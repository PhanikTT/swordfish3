﻿namespace Swordfish.Businessobjects.Easyjet
{
    public class EasyJetRouteModel
    {
        public string departureDateTime { get; set; }
        public string arrivalDateTime { get; set; }
        public string flightNumber { get; set; }
        public string operatingAirline { get; set; }
        public string arrivalAirport { get; set; }
        public string departureAirport { get; set; }
    }
}