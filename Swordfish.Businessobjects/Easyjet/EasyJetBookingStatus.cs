﻿namespace Swordfish.Businessobjects.Easyjet
{
    public class EasyJetBookingStatus
    {
        public int componentTypeOptionId { get; set; }
        public string eJReturnBookingRefId { get; set; }
        public string eJInBoundBookingRefId { get; set; }
        public string eJOutBoundBookingRefId { get; set; }
        public bool? eJReturnBookingSuccess { get; set; }
        public bool? eJInBoundBookingSuccess { get; set; }
        public bool? eJOutBoundBookingSuccess { get; set; }
        public decimal totalPriceOnConfirmStage { get; set; }
    }
}
