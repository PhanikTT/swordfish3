﻿namespace Swordfish.Businessobjects.Easyjet
{
    public class EasyJetSupplementsModel
    {
        public string type { get; set; }
        public string description { get; set; }
        public int adultsCount { get; set; }
        public int childrenCount { get; set; }
        public int infantsCount { get; set; }
        public decimal adultPricePricePerUnit { get; set; }
        public decimal childPricePricePerUnit { get; set; }
        public decimal infantPricePricePerUnit { get; set; }
    }
}