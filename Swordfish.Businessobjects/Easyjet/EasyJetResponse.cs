﻿using System;
using System.Collections.Generic;
using System.Web.Mvc;

namespace Swordfish.Businessobjects.Easyjet
{
    public class EasyJetResponse
    {
        public string quoteHeaderId { get; set; }
        public List<EasyJetRouteModel> route { get; set; }
        public int adultsCount { get; set; }
        public int childrenCount { get; set; }
        public int infantsCount { get; set; }
        public decimal adultPricePP { get; set; }
        public decimal childPricePP { get; set; }
        public decimal infantPricePP { get; set; }
        public string currencyCode { get; set; }
        public string priceType { get; set; }
        public List<EasyJetSupplementsModel> supplements { get; set; }
        public string supplierRef { get; set; }
        public decimal totalPriceOnConfirmStage { get; set; }
        public decimal cardFeeOnConfirmStage { get; set; }
        [AllowHtml]
        public string confirmationPage { get; set; }
        [AllowHtml]
        public string unexpectedCnfPage { get; set; }
        public int ComponentTypeOptionId { get; set; }
        public DateTime CreatedDate { get; set; }
    }
}
