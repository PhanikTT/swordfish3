﻿using Swordfish.Businessobjects.Sesame;

namespace Swordfish.Businessobjects.Bookings
{
    public class BookCreateRequest : SesameRequestBase
    {
        public string FlightQuoteId { get; set; }
        public AccoDetail AccoQuotes { get; set; }
        public BookCreateRequest()
        {
            AccoQuotes = new AccoDetail();
        }
    }
}
