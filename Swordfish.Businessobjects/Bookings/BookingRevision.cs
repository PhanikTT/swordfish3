﻿namespace Swordfish.Businessobjects.Bookings
{
    public class BookingRevision
    {
        public int AgentId { get; set; }
        public int CustomerId { get; set; }
        public string QuoteReference { get; set; }
        public string FlightSupplierId { get; set; }
        public string FlightStatusCode { get; set; }
        public decimal FlightFare { get; set; }
        public decimal HotelFare { get; set; }
        public string HotelSupplierId { get; set; }
        public string HotelStatusCode { get; set; }
        public string CurrencyCode { get; set; }
        public string IpAddress { get; set; }
        public string CwtSessionId { get; set; }
        public bool IsTestBooking { get; set; }
        public CustomerRequest CustomerDetails { get; set; }
    }
}
