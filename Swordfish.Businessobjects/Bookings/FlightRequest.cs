﻿using System;

namespace Swordfish.Businessobjects.Bookings
{
    public class FlightRequest
    {
        public string url { get; set; }
        public string source { get; set; }
        public string destination { get; set; }
        public DateTime inboundDate { get; set; }
        public DateTime outboundDate { get; set; }
        public int passengers { get; set; }
        public int adults { get; set; }
        public int child { get; set; }
        public int infants { get; set; }
        public int Bags { get; set; }
        public string FlightWay { get; set; }
        public string ForCompany { get; set; }
    }
}
