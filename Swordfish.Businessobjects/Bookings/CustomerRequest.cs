﻿namespace Swordfish.Businessobjects.Bookings
{
    public class CustomerRequest
    {
        public string Title { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Address1 { get; set; }
        public string Address2 { get; set; }
        public string Address3 { get; set; }
        public string Country { get; set; }
        public string PostCode { get; set; }
        public string Email { get; set; }
        public string PhoneDay { get; set; }
        public string PhoneEve { get; set; }
    }
}
