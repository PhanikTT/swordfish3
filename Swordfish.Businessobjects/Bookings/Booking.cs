﻿using Swordfish.Businessobjects.Passengers;
using Swordfish.Businessobjects.Payments;
using System.Collections.Generic;

namespace Swordfish.Businessobjects.Bookings
{
    public class Booking
    {
        public string QuoteReference { get; set; }
        public int UserId { get; set; }
        public int BasketId { get; set; }
        public string ForClient { get; set; }
        public List<Passenger> PassengerDetails { get; set; }
        public PaymentDetails PaymentInfo { get; set; }
    }
}
