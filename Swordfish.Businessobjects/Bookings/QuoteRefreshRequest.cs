﻿using Swordfish.Businessobjects.Passengers;
using Swordfish.Businessobjects.Quotes;
using System.Collections.Generic;

namespace Swordfish.Businessobjects.Bookings
{
    public class QuoteRefreshRequest
    {
        public List<QuoteRefBasket> Basket { get; set; }
        public List<Passenger> PassengerDetails { get; set; }
    }
}
