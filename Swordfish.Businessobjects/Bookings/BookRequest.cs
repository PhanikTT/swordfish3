﻿namespace Swordfish.Businessobjects.Bookings
{
    public class BookRequest
    {
        public string BasketReference { get; set; }
        public CustomerRequest CustomerDetails { get; set; }
    }
}
