﻿namespace Swordfish.Businessobjects.Bookings
{
    public class AccoDetail
    {
        public string AccoQuoteId { get; set; }
        public string RoomTypeCode { get; set; }
        public string RateQuoteId { get; set; }
    }
}
