﻿namespace Swordfish.Businessobjects.Bookings
{
    public class BasketRequest
    {
        public string FlightQuoteId { get; set; }
        public string AccommodationQuoteId { get; set; }
        public string RoomTypeQuote { get; set; }
        public string RoomRateQuote { get; set; }
    }
}