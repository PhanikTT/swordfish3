﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Swordfish.Businessobjects.Sesame
{
    public class SesameAccoDetail
    {
        public string AccoQuoteId { get; set; }
        public string RoomTypeCode { get; set; }
        public string RateQuoteId { get; set; }
    }
}
