﻿using System.Collections.Generic;

namespace Swordfish.Businessobjects.Sesame
{
    public class SesameBasket
    {
        public int StatusCode { get; set; }
        public string StatusMessage { get; set; }
        public string BasketId { get; set; }
        public string CurrencyCode { get; set; }
        public string ResponseXml { get; set; }
        public int QuoteHeaderId { get; set; }
        public int AgentId { get; set; }
        public int DepositDays { get; set; }
        public string SourceUrl { get; set; }
        public List<SesameComponent> BasketComponents { get; set; }
        public SesameBasket()
        {
            BasketComponents = new List<SesameComponent>();
        }
    }
}
