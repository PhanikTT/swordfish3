﻿namespace Swordfish.Businessobjects.Sesame
{
    public class SesameAddress
    {
        public string Line1 { get; set; }
        public string Line2 { get; set; }
        public string Line3 { get; set; }
        public string CountryCode { get; set; }
        public string PostCode { get; set; }
    }
}
