﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Swordfish.Businessobjects.Sesame
{
    public class SesameBasketResponse
    {
        public SesameBasket ResponseBasket { get; set; }

        public SesameBasketResponse()
        {
            ResponseBasket = new SesameBasket();
        }
    }
}
