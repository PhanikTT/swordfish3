﻿using System.Collections.Generic;

namespace Swordfish.Businessobjects.Sesame
{
    public class SesameComponent
    {
        public string ComponentID { get; set; }
        public string ProductTypeCode { get; set; }
        public string SupplierCode { get; set; }
        public string BookingAttempted { get; set; }
        public string BookingSucceeded { get; set; }
        public string BookingCancelled { get; set; }
        public string StatusCode { get; set; }
        public string SupplierBookingId { get; set; }
        public string SupplierComponentId { get; set; }
        public int DirectionOptionId { get; set; }
        public int Type { get; set; }
        public string  SourceUrl { get; set; }
        public List<SesameDetail> ComponentDetails { get; set; }
        public List<SesameOption> ComponentOptions { get; set; }
        public List<string> ComponentErrors { get; set; }
        public List<SesameHotelInformation> ComponentHotelInformation { get; set; }
        public List<SesameFlightInformation> ComponentFlightInformation { get; set; }
        public List<SesameVCN> ComponentVCNDetials { get; set; }
        public List<string> ComponentMessages { get; set; }
        public SesameComponent()
        {
            ComponentDetails = new List<SesameDetail>();
            ComponentOptions = new List<SesameOption>();
            ComponentErrors = new List<string>();
            ComponentHotelInformation = new List<SesameHotelInformation>();
            ComponentFlightInformation = new List<SesameFlightInformation>();
            ComponentVCNDetials = new List<SesameVCN>();
            ComponentMessages = new List<string>();
        }
    }
}