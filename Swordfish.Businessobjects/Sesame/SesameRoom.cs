﻿using System.Collections.Generic;

namespace Swordfish.Businessobjects.Sesame
{
    public class SesameRoom
    {
        public List<SesamePaxDetail> Adults { get; set; }
        public List<SesamePaxDetail> Children { get; set; }
        public List<SesamePaxDetail> Infants { get; set; }
        public SesameRoom()
        {
            this.Adults = new List<SesamePaxDetail>();
            this.Children = new List<SesamePaxDetail>();
            this.Infants = new List<SesamePaxDetail>();
        }
    }
}
