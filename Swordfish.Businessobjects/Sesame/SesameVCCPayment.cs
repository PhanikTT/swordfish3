﻿namespace Swordfish.Businessobjects.Sesame
{
    public class SesameVCCPayment
    {
        public string SupplierCode { get; set; }
        public string CurrencyCode { get; set; }
    }
}
