﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Swordfish.Businessobjects.Sesame
{
    public class SesameBasketCreateRequest:SesameRequestBase
    {
        public string SourceUrl { get; set; }
        public List<string> FlightQuotes { get; set; }
        public SesameAccoDetail AccoQuotes { get; set; }
        public SesameBasketCreateRequest()
        {
            AccoQuotes = new SesameAccoDetail();
        }
    }
}
