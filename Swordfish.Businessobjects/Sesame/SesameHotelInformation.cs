﻿namespace Swordfish.Businessobjects.Sesame
{
    public class SesameHotelInformation: SesameRequestBase
    {
        public string Description { get; set; }
        public string Code { get; set; }
        public string BoardCode { get; set; }
    }
}
