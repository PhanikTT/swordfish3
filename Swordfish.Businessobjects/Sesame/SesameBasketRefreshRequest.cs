﻿using System.Collections.Generic;

namespace Swordfish.Businessobjects.Sesame
{
    public class SesameBasketRefreshRequest : SesameRequestBase
    {
        public string BasketID { get; set; }
        public string SourceUrl { get; set; }
        public List<SesameBookComponent> Components { get; set; }
        public List<SesameRoom> Occupancy { get; set; }
        public SesameBasketRefreshRequest()
        {

            this.Components = new List<SesameBookComponent>();
            this.Occupancy = new List<SesameRoom>();
        }
    }
}
