﻿using System.Collections.Generic;

namespace Swordfish.Businessobjects.Sesame
{
    public class SesameFlightInformation
    {
        public string FromAirportCode { get; set; }
        public string ToAirportCode { get; set; }
        public string DepartureDateTime { get; set; }
        public string ArrivalDateTime { get; set; }
        public List<SesameLeg> Legs { get; set; }
        public SesameFlightInformation()
        {
            Legs = new List<SesameLeg>();
        }
    }
}
