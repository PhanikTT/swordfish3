﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Swordfish.Businessobjects.Sesame
{
    public class SesameDetail
    {
        public string Type { get; set; }
        public string Description { get; set; }
        public string SourceCurrency { get; set; }
        public string SourcePricePence { get; set; }
        public string SellingPricePence { get; set; }
        public string Quantity { get; set; }
        public double? ExchangeRateUsed { set; get; }

    }
}
