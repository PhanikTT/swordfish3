﻿namespace Swordfish.Businessobjects.Sesame
{
    public class SesameLeg
    {
        public string FromAirportCode { get; set; }
        public string ToAirportCode { get; set; }
        public string DepartureDateTime { get; set; }
        public string ArrivalDateTime { get; set; }
        public string FlightNumber { get; set; }
        public string MarketingAirline { get; set; }
        public string CabinClass { get; set; }
    }
}
