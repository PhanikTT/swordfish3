﻿using System.Collections.Generic;

namespace Swordfish.Businessobjects.Sesame
{
    public class SesameBookComponent
    {
        public string ComponentID { get; set; }
        public List<SesameBookOption> Options { get; set; }
        public SesameBookComponent()
        {
            this.Options = new List<SesameBookOption>();
        }
    }
}