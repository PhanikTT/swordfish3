﻿namespace Swordfish.Businessobjects.Sesame
{
    public class SesameBookOption
    {
        public string OptionID { get; set; }
        public string QuantityRequired { get; set; }
    }
}
