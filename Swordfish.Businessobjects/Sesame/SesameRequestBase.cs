﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace Swordfish.Businessobjects.Sesame
{
    public class SesameRequestBase
    {
        public string SessionID { get; set; }
        public SesameRequestBase()
        {
            if (HttpContext.Current != null)
            {
                SessionID = HttpContext.Current.Session.SessionID;
            }
        }
    }
}
