﻿namespace Swordfish.Businessobjects.Sesame
{
    public class SesameOption
    {
        public string Description { get; set; }
        public string Code { get; set; }
        public string Type { get; set; }
        public string QuantityAvailable { get; set; }
        public string QuantityRequired { get; set; }
        public string BuyingPricePence { get; set; }
        public string SellingPricePence { get; set; }
        public string Items { get; set; }
    }
}
