﻿namespace Swordfish.Businessobjects.Sesame
{
    public class SesameVCN
    {
        public string PAN { get; set; }
        public string CardType { get; set; }
        public string ExpiryDate { get; set; }
        public string CardHolderName { get; set; }
        public string Reference { get; set; }
    }
}
