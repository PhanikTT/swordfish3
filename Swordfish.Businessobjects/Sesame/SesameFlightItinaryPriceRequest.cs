﻿using System.Runtime.Serialization;

namespace Swordfish.Businessobjects.Sesame
{
    [DataContract]
    public class SesameFlightItinaryPriceRequest : SesameRequestBase
    {
        [DataMember]
        public string quoteId { get; set; }
    }
}
