﻿namespace Swordfish.Businessobjects.Sesame
{
    public class SesamePaxDetail
    {
        public string Title { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string DOB { get; set; }
        public string Email { get; set; }
        public string PhoneDay { get; set; }
        public string PhoneEve { get; set; }
        public SesameAddress PAXAddress { get; set; }
        public SesamePaxDetail()
        {
            PAXAddress = new SesameAddress();
        }
    }
}
