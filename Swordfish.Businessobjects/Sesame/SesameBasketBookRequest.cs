﻿namespace Swordfish.Businessobjects.Sesame
{
    public class SesameBasketBookRequest : SesameRequestBase
    {
        public string BasketID { get; set; }
        public string SourceUrl { get; set; }
        public SesamePaxDetail CardHolderDetail { get; set; }
        public SesameVCCPayment VCCPaymentDetails { get; set; }
        public SesameBasketBookRequest()
        {
            CardHolderDetail = new SesamePaxDetail();
            VCCPaymentDetails = new SesameVCCPayment();
        }
    }
}
