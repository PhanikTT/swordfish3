﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Swordfish.Businessobjects.Payments
{
    public class Card
    {
        public string cardtype { get; set; }
        public string cardnumber { get; set; }
        public string cv2 { get; set; }
        public string nameoncard { get; set; }
        public string expirydate { get; set; }
    }
}
