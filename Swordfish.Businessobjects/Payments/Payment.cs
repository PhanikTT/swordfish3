﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Swordfish.Businessobjects.Payments
{
    public class Payment
    {
        public string title { get; set; }
        public string forename { get; set; }
        public string surname { get; set; }
        public string addressline1 { get; set; }
        public string addressline2 { get; set; }
        public string postcode { get; set; }
        public Card card { get; set; }
    }
}
