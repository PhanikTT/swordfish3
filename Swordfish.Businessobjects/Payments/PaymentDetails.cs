﻿using System;

namespace Swordfish.Businessobjects.Payments
{
    public class PaymentDetails
    {
        public string quoteHeaderId { get; set; }
        public string cardType { get; set; }
        public string cardNumber { get; set; }
        public string name { get; set; }
        public string address { get; set; }
        public string city { get; set; }
        public string country { get; set; }
        public string postCode { get; set; }
        public decimal amount { get; set; }
        public decimal cost { get; set; }
        public decimal cardCharges { get; set; }
        public decimal totalCost { get; set; }
        public string merchantRefrence { get; set; }
        public string transactionId { get; set; }
        public string resultDatetime { get; set; }
        public string processingdb { get; set; }
        public string errormsg { get; set; }
        public string merchantNumber { get; set; }
        public string tid { get; set; }
        public string schemeName { get; set; }
        public string messageNumber { get; set; }
        public string authCode { get; set; }
        public string authMessage { get; set; }
        public string vertel { get; set; }
        public string txnResult { get; set; }
        public string pcavsResult { get; set; }
        public string ad1avsResult { get; set; }
        public string cvcResult { get; set; }
        public string arc { get; set; }
        public string iadarc { get; set; }
        public string iadoad { get; set; }
        public string cardExpiry { get; set; }
        public string isd { get; set; }
        public string authorisingentity { get; set; }
        public string houseNumber { get; set; }
        public int agentId { get; set; }
        public string cardEasyResponse { get; set; }
        #region old 
        public decimal Amount { get; set; }
        public string NameOnCard { get; set; }
        public string PaymentEmail { get; set; }
        public string IsPaymentSuccess { get; set; }
        public string PaymentCountry { get; set; }
        public string CardType { get; set; }
        public string CardNumber { get; set; }
        public string CardValidFrom { get; set; }
        public string CardExpiry { get; set; }
        public string CardIssueNumber { get; set; }
        public string Status { get; set; }
        public string TransactionReference { get; set; }
        public DateTime PaymentDate { get; set; }
        public string PaymentPostCode { get; set; }
        public string PaymentAddress1 { get; set; }
        public string PaymentAddress2 { get; set; }
        public string PaymentAddress3 { get; set; }
        #endregion
    }
}
