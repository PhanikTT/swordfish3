﻿using System;

namespace Swordfish.Businessobjects.Passengers
{
    public class Passenger
    {
        public string Title { get; set; }
        public string FirstName { get; set; }
        public string MiddleName { get; set; }
        public string LastName { get; set; }
        public string IsLeadPassenger { get; set; }
        public string DateOfBirth { get; set; }
        public string DefaultDateOfBirth { get; set; }

        public string Nationality { get; set; }
        public string Gender { get; set; }
        public string HouseNumber { get; set; }
        public string Address1 { get; set; }
        public string Address2 { get; set; }
        public string Address3 { get; set; }
        public string Mobile { get; set; }
        public string Telephone { get; set; }
        public string PostCode { get; set; }
        public string Country { get; set; }
        public string City { get; set; }
        public string Email { get; set; }
        public string PassportNumber { get; set; }
        public string Type { get; set; }
        public DateTime? LapsDays { set; get; }
        public int StatusOptionId { get; set; }
        public string StatusDescription { get; set; }
        public int PassengerTypeOptionId { get; set; }
    }
}
