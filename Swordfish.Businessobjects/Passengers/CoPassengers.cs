﻿namespace Swordfish.Businessobjects.Passengers
{
    public class CoPassengers
    {
        public string Title { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Email { get; set; }
        public string DateOfBirth { get; set; }
        public string PassportNumber { get; set; }
        public string PassengerType { get; set; }
        public string IsAdultOrChild { get; set; }
    }
}
