﻿namespace Swordfish.Businessobjects.Basket
{
    public class Quote
    {
        public string BasketQuoteReference { get; set; }
        public string QuoteRef { get; set; }
        public int CustomerId { get; set; }
        public string QuoteStatus { get; set; }
        public Agent Agent { get; set; }

    }
}
