﻿using System;
using System.Collections.Generic;

namespace Swordfish.Businessobjects.Basket
{
    public class FlyingDetails
    {
        public string FlightNumber { get; set; }
        public DateTime DepartureTime { get; set; }
        public DateTime ArrivalTime { get; set; }
        public string DepartureAirport { get; set; }
        public string ArrivalAirport { get; set; }
        public decimal TotalSellingPrice { get; set; }
        public int NoOfSeats { get; set; }
        public string OperatorCode { get; set; }//Added
        public string AirlineCode { get; set; }//Added
        public string FareType { get; set; }
        public List<FlightSegments> FlightSegments { get; set; }
    }
}
