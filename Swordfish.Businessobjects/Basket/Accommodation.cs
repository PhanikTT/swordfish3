﻿using System;
using System.Collections.Generic;

namespace Swordfish.Businessobjects.Basket
{
    public class Accommodation
    {
        public string HotelId { get; set; }
        public string HotelName { get; set; }
        public string ResortName { get; set; }
        public string Address { get; set; }
        public string PhoneNo { get; set; }
        public decimal Rating { get; set; }
        public decimal UserScore { get; set; }
        public string BoardCode { get; set; }
        public string BoardDesc { get; set; }
        public string SupplierCode { get; set; }
        public string SupplierBasketId { get; set; }
        public decimal AmountAfterTax { get; set; }
        public DateTime StartDate { get; set; }
        public int NoOfDaysRequired { get; set; }
        public int NoOfRooms { get; set; }
        public string TTICode { get; set; }
        public string HotelCityCode { get; set; }
        public string HotelCity { get; set; }
        public string HotelCodeContext { get; set; }

        #region For TopDog
        public string BookingReference { get; set; }
        public string Description { get; set; }
        public string ExtraDescription { get; set; }
        public string AirportCode { get; set; }
        public string AirportDescription { get; set; }
        public string Country { get; set; }
        public string PostalCode { get; set; }
        #endregion 


        public List<AccommodationRooms> AccommodationRoomInfo { get; set; }
    }
}
