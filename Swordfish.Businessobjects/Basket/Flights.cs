﻿using System.Collections.Generic;

namespace Swordfish.Businessobjects.Basket
{
    public class Flights
    {
        public string Operator { get; set; } //Delete
        public string FareType { get; set; }
        public string SupplierCode { get; set; } //Delete
        public string SupplierBasketId { get; set; }
        public string Errata { get; set; }

        #region For TopDog
        public string OperatorCode { get; set; }
        public string BookingReference { get; set; }
        public string Description { get; set; }
        public string ServiceClass { get; set; }
        #endregion

        public List<FlyingDetails> FlightDetails { get; set; }
    }
}
