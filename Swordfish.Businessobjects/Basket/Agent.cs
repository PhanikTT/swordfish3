﻿using System;

namespace Swordfish.Businessobjects.Basket
{
    public class Agent
    {
        public string Email { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Password { get; set; }
        public string Initial { get; set; }
        public string Title { get; set; }
        public string AddressType { get; set; }
        public string Address1 { get; set; }
        public string Address2 { get; set; }
        public string PostCode { get; set; }
        public string EmailType { get; set; }
        public string PhoneType { get; set; }
        public string Phone { get; set; }
        public int CreatedBy { get; set; }
        public DateTime CreatedOn { get; set; }


    }
}
