﻿namespace Swordfish.Businessobjects.Basket
{
    public class AccommodationRooms
    {
        public string RoomCode { get; set; }
        public string RoomRateQuote { get; set; }
        public int NoOfAdult { get; set; }
        public int NoOfChild { get; set; }
        public int NoOfInfants { get; set; }
        public int Quantity { get; set; }
        public string Description { get; set; }
    }
}
