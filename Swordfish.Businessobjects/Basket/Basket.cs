﻿namespace Swordfish.Businessobjects.Basket
{
    public class Basket
    {
        public string SessionId { get; set; }
        public string UserId { get; set; }
        public string Currency { get; set; }
        public int NoOfAdults { get; set; }
        public int NoOfChilds { get; set; }
        public int NoOfInfants { get; set; }
        public Quote Quote { get; set; }
    }
}
