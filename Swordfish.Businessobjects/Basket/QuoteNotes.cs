﻿namespace Swordfish.Businessobjects.Basket
{
    public class QuoteNotes
    {
        public string QuoteRef { get; set; }
        public string Notes { get; set; }
    }
}
