﻿namespace Swordfish.Businessobjects.Transfers
{
    public class Todetails
    {
        public string codetype { get; set; }
        public ToAccommodation accommodation { get; set; }
        public ToFlight flight { get; set; }
    }
}
