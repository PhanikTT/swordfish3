﻿namespace Swordfish.Businessobjects.Transfers
{
    public class TransferComponent
    {
        public string ProductId { get; set; }
        public string BookingTypeId { get; set; }
        public int TransferTime { get; set; }
        public string ProductTypeId { get; set; }
        public string ProductType { get; set; }
        public int MinPax { get; set; }
        public int MaxPax { get; set; }
        public int Luggage { get; set; }
        public string perPerson { get; set; }
        public string productDescription { get; set; }
        public decimal oldPrice { get; set; }
        public decimal totalPrice { get; set; }
        public string Currency { get; set; }
        public string PriceType { get; set; }
        public string TypeCode { get; set; }
        public string PriceDescription { get; set; }
        public int Units { get; set; }
        public decimal PricePerPersonOldPrice { get; set; }
        public int CreatedBy { get; set; }
        public int ModifiedBy { get; set; }
        public string TransferType { get; set; }
        public int TransferTypeOptionId { get; set; }


    }
}
