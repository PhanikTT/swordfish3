﻿using System.Collections.Generic;

namespace Swordfish.Businessobjects.Transfers
{
    public class TransferBookResponse
    {
        public int StatusCode { get; set; }
        public string StatusMessage { get; set; }
        public List<TransfersRoot> Rootobject { get; set; }
        public TransferBookResponse()
        {
            Rootobject = new List<TransfersRoot>();
        }
        public class TransfersRoot
        {
            public Response response { get; set; }
        }

        public class TransferBooking
        {
            public List<TransfersBookGeneral> general { get; set; }
            public List<TransfersBookContact> contact { get; set; }
            public List<TransfersBookPricing> pricing { get; set; }
            public List<TransfersData> transfers { get; set; }

        }

        public class Response
        {
            public TransferBooking booking { get; set; }
        }
    }
}
