﻿using System.Collections.Generic;

namespace Swordfish.Businessobjects.Transfers
{
    public class TransfersData
    {
        public string transferOrder { get; set; }
        public string transferId { get; set; }
        public string transferStatus { get; set; }
        public string dateBooked { get; set; }
        public string dateAmended { get; set; }
        public string dateCancelled { get; set; }
        public string bookingTypeId { get; set; }
        public string description { get; set; }
        public string productId { get; set; }
        public string productType { get; set; }
        public string productCategory { get; set; }
        public string transportProvider { get; set; }
        public string transferTime { get; set; }
        public string noofPax { get; set; }
        public string minimumPax { get; set; }
        public string maximumPax { get; set; }
        public string lagguage { get; set; }
        public string noofVehicles { get; set; }
        public string noofAdults { get; set; }
        public string noofChildren { get; set; }
        public string noofInfants { get; set; }
        public List<TransfersBookFromDetail> fromDetails { get; set; }
        public List<TransfersBookToDetail> toDetails { get; set; }
        public List<TransfersBookInstruction> instructions { get; set; }
        public List<TransfersBookPricing> pricings { get; set; }
    }
}
