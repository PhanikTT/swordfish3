﻿namespace Swordfish.Businessobjects.Transfers
{
    public class Contact
    {
        public string email { get; set; }
        public string title { get; set; }
        public string firstname { get; set; }
        public string lastname { get; set; }
        public string telephone { get; set; }
    }
}
