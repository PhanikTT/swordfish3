﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Swordfish.Businessobjects.Transfers
{
    public class TransfersBookFlight
    {
        public string flightNumber { get; set; }
        public string terminal { get; set; }
        public string arrivalDateTime { get; set; }
        public string departureDateTime { get; set; }
    }
}
