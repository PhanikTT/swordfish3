﻿namespace Swordfish.Businessobjects.Transfers
{
    public class TransfersBookContact
    {
        public string title { get; set; }
        public string firstName { get; set; }
        public string lastName { get; set; }
        public string email { get; set; }
        public string telephone { get; set; }

    }
}
