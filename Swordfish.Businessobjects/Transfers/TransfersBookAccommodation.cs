﻿namespace Swordfish.Businessobjects.Transfers
{
    public class TransfersBookAccommodation
    {
        public string name { get; set; }
        public string addressLine1 { get; set; }
        public string addressLine2 { get; set; }
        public string addressLine3 { get; set; }
        public string telephone { get; set; }
        public string pickupDateTime { get; set; }
        public string supplierPickupDateTime { get; set; }
        public string supplierName { get; set; }
        public string supplierAddressline1 { get; set; }
        public string supplierAddressline2 { get; set; }
        public string supplierAddressline3 { get; set; }
        public string pickupTimeViewed { get; set; }
        public string reconfirmationRequired { get; set; }

    }
}
