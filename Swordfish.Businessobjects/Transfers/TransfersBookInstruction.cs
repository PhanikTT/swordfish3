﻿namespace Swordfish.Businessobjects.Transfers
{
    public class TransfersBookInstruction
    {
        public string instruction { get; set; }
        public string instructionNoMarkup { get; set; }
        public string reconfirmationTelephone { get; set; }
        public string emergencyTelephone { get; set; }
        public string hoursMondayFriday { get; set; }
        public string hourSaturday { get; set; }
        public string hoursSunday { get; set; }
        public string ukEmergencyTelephone { get; set; }
    }
}
