﻿using System.Collections.Generic;

namespace Swordfish.Businessobjects.Transfers
{
    public class Transfer
    {
        public int productid { get; set; }
        public int bookingtypeid { get; set; }
        public decimal saleprice { get; set; }
        public int noadults { get; set; }
        public int nochildren { get; set; }
        public int noinfants { get; set; }
        public int nopax { get; set; }
        public int novehicles { get; set; }
        public Fromdetails fromdetails { get; set; }
        public Todetails todetails { get; set; }
        public List<Extra> extras { get; set; }
    }
}
