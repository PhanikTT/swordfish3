﻿namespace Swordfish.Businessobjects.Transfers
{
    public class ToAccommodation
    {
        public string name { get; set; }
        public string addressline1 { get; set; }
        public string addressline2 { get; set; }
        public string addressline3 { get; set; }
        public string telephone { get; set; }
    }
}
