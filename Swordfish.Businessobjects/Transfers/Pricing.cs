﻿namespace Swordfish.Businessobjects.Transfers
{
    public class Pricing
    {
        public decimal saleprice { get; set; }
        public string currency { get; set; }
    }
}
