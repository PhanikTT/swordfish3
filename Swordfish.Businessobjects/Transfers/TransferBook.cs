﻿using System;

namespace Swordfish.Businessobjects.Transfers
{
    public class TransferBook
    {
        public string transferOrder { get; set; }
        public int transferId { get; set; }
        public string transferStatus { get; set; }
        public DateTime? dateBooked { get; set; }
        public DateTime? dateAmended { get; set; }
        public DateTime? dateCancelled { get; set; }
        public int bookingTypeId { get; set; }
        public int productId { get; set; }
        public string productType { get; set; }
        public string productCategory { get; set; }
        public string transportProvider { get; set; }
        public string transferTime { get; set; }
        public int noofPax { get; set; }
        public int minimumPax { get; set; }
        public int maximumPax { get; set; }
        public string lagguage { get; set; }
        public int noofVehicles { get; set; }
        public int noofAdults { get; set; }
        public int noofChildren { get; set; }
        public int noofInfants { get; set; }
        public string airportcodeType { get; set; }
        public string airportCode { get; set; }
        public string airportCodeId { get; set; }
        public string airportCodeName { get; set; }
        public string flightName { get; set; }
        public string flightNumber { get; set; }
        public string terminal { get; set; }
        public DateTime? arrivalDateTime { get; set; }
        public DateTime? departureDateTime { get; set; }
        public string hotelName { get; set; }
        public string addressLine1 { get; set; }
        public string addressLine2 { get; set; }
        public string addressLine3 { get; set; }
        public string telephone { get; set; }
        public DateTime? pickupDateTime { get; set; }
        public DateTime? supplierPickupDateTime { get; set; }
        public string supplierName { get; set; }
        public string supplierAddressline1 { get; set; }
        public string supplierAddressline2 { get; set; }
        public string supplierAddressline3 { get; set; }
        public string pickupTimeViewed { get; set; }
        public string reconfirmationRequired { get; set; }
        public string instruction { get; set; }
        public string instructionNoMarkup { get; set; }
        public string reconfirmationTelephone { get; set; }
        public string emergencyTelephone { get; set; }
        public string hoursMondayFriday { get; set; }
        public string hourSaturday { get; set; }
        public string hoursSunday { get; set; }
        public string ukEmergencyTelephone { get; set; }
        public decimal netPrice { get; set; }
        public decimal totalPrice { get; set; }
        public decimal transactionfee { get; set; }
        public decimal carbonoffset { get; set; }
        public decimal optionalExtras { get; set; }
        public decimal creditAmount { get; set; }
        public decimal salePrice { get; set; }
        public string currency { get; set; }
        public string vehicleTypeCode { get; set; }
        public string vehicleType { get; set; }
        public string description { get; set; }
        public string units { get; set; }
        public decimal vehicleNetPrice { get; set; }
        public decimal vehicleTotalPrice { get; set; }
    }
}
