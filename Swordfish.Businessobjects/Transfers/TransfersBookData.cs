﻿using System.Collections.Generic;

namespace Swordfish.Businessobjects.Transfers
{
    public class TransfersBookData
    {
        public TransfersBookGeneral general { get; set; }
        public TransfersBookContact contact { get; set; }
        public TransfersBookPricing pricing { get; set; }
        public List<TransferBook> transferbookdata { get; set; }
    }
}
