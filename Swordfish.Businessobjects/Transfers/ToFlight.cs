﻿namespace Swordfish.Businessobjects.Transfers
{
    public class ToFlight
    {
        public string departuredatetime { get; set; }
        public string flightnumber { get; set; }
        public string terminal { get; set; }
        public string destinationairport { get; set; }
    }
}
