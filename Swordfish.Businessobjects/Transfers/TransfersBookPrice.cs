﻿namespace Swordfish.Businessobjects.Transfers
{
    public class TransfersBookPrice
    {
        public string typeCode { get; set; }
        public string type { get; set; }
        public string description { get; set; }
        public string units { get; set; }
        public string netPrice { get; set; }
        public string totalPrice { get; set; }
    }
}
