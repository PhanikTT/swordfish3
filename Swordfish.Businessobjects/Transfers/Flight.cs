﻿namespace Swordfish.Businessobjects.Transfers
{
    public class Flight
    {
        public string arrivaldatetime { get; set; }
        public string flightnumber { get; set; }
        public string terminal { get; set; }
        public string originairport { get; set; }
    }
}
