﻿namespace Swordfish.Businessobjects.Transfers
{
    public class Extra
    {
        public string Typecode { get; set; }
        public int Units { get; set; }
    }
}
