﻿using System.Collections.Generic;

namespace Swordfish.Businessobjects.Transfers
{
    public class TransfersBookFromDetail
    {
        public string airportcodeType { get; set; }
        public string airportCode { get; set; }
        public string airportCodeId { get; set; }
        public string airportCodeName { get; set; }
        public List<TransfersBookFlight> flights { get; set; }
        public List<TransfersBookAccommodation> accommodations { get; set; }
    }
}
