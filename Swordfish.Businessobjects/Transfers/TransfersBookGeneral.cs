﻿using System;

namespace Swordfish.Businessobjects.Transfers
{
    public class TransfersBookGeneral
    {
        public int quoteHeaderId { get; set; }
        public string affiliateCode { get; set; }
        public string agency { get; set; }
        public string clerk { get; set; }
        public string bookingReference { get; set; }
        public string bookingStatusId { get; set; }
        public string orderReference { get; set; }
        public string noOfTransfers { get; set; }
        public string paymentType { get; set; }
        public DateTime dateEntered { get; set; }
        public string pdf { get; set; }
        public string html { get; set; }
    }
}
