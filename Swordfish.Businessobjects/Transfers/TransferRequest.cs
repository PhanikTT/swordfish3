﻿namespace Swordfish.Businessobjects.Transfers
{
    public class TransferRequest
    {
        public string FromDate { get; set; }
        public string ToDate { get; set; }
        public int AdultCount { get; set; }
        public int ChildrenCount { get; set; }
        public int InfantCount { get; set; }
    }
}
