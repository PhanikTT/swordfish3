﻿namespace Swordfish.Businessobjects.Transfers
{
    public class Fromdetails
    {

        public string codetype { get; set; }
        public Flight flight { get; set; }
        public FromAccommodation accommodation { get; set; }
    }
}
