﻿using System.Collections.Generic;

namespace Swordfish.Businessobjects.Transfers
{
    public class TransfersBookToDetail
    {
        public string codeType { get; set; }
        public string code { get; set; }
        public string id { get; set; }
        public string name { get; set; }
        public List<TransfersBookAccommodation> accommodations { get; set; }
        public List<TransfersBookFlight> flights { get; set; }
    }
}
