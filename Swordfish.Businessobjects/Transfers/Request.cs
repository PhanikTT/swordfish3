﻿using System.Collections.Generic;

namespace Swordfish.Businessobjects.Transfers
{
    public class Request
    {
        public General general { get; set; }
        public Contact contact { get; set; }
        public Pricing pricing { get; set; }
        public List<Transfer> transfers { get; set; }
        public Payments.Payment payment { get; set; }
    }
}
