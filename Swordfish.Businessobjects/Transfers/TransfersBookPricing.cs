﻿using System.Collections.Generic;

namespace Swordfish.Businessobjects.Transfers
{
    public class TransfersBookPricing
    {
        public string netPrice { get; set; }
        public string totalPrice { get; set; }
        public string transactionfee { get; set; }
        public string carbonoffset { get; set; }
        public string optionalExtras { get; set; }
        public string creditAmount { get; set; }
        public string salePrice { get; set; }
        public string currency { get; set; }
        public List<TransfersBookPrice> Prices { get; set; }
    }
}
