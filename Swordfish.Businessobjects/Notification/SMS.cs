﻿namespace Swordfish.Businessobjects.Notification
{
    public class SMS
    {
        public string AccountSid { get; set; }
        public string AuthToken { get; set; }
        public string FromPhoneNumber { get; set; }
        public string ToPhoneNumber { get; set; }
        public string Message { get; set; }
    }
}
