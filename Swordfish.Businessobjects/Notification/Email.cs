﻿namespace Swordfish.Businessobjects.Notification
{
    public class Email
    {
        public string SmtpServer { get; set; }
        public string Port { get; set; }
        public string SenderEmail { get; set; }
        public string SenderPassword { get; set; }
        public string ToEmail { get; set; }
        public string FromEmail { get; set; }
        public bool EnableSsl { get; set; }
        public string Subject { get; set; }
        public string Body { get; set; }
        public string AttachementFileString { get; set; }
        public string FileType { get; set; }
    }
}
