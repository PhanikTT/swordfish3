﻿using System.Collections.Generic;

namespace Swordfish.Businessobjects.Notification
{
    public class SmsResponse
    {
        public string Messagecount { get; set; }
        public List<Message> Messages { get; set; }
    }
}