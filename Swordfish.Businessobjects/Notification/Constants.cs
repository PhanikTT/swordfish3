﻿namespace Swordfish.Businessobjects.Notification
{
    public class Constants
    {
        public enum EmailTypes
        {
            QuickQuoteEmail,
            BookQuoteEmail,
            SendLiveShareEmail,
            PassWordRecoveryEMail,
            TopDogFailureEmail,
            BookingFullFillmentFailureEmail,
            BookingAcknowledgementEmail,
        };

        public enum SmsTypes
        {
            QuickQuoteSMS,
            BookQuoteSMS,
        };

        public enum ServerEnvironments
        {
            sandbox,
            dev,
            sqa,
            uat
        };
    }
}
