﻿using System.Collections.Generic;

namespace Swordfish.Businessobjects.Quotes
{
    public class Occupancy
    {
        public Occupancy()
        {
            ChildAge = new List<int>();
            InfantAge = new List<int>();
        }
        public int RoomNo { get; set; }
        public int AdultsCount { get; set; }
        public int ChildCount { get; set; }
        public List<int> ChildAge { get; set; }
        public int InfantCount { get; set; }
        public List<int> InfantAge { get; set; }
    }
}
