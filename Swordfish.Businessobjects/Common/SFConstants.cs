﻿namespace Swordfish.Businessobjects.Common
{
    public class SFConstants
    {
        //TSOption
        public static int QuoteType_Quickquote = 1;
        public static int QuoteType_Bookquote = 2;
        public static int QuoteComponent_FlightReturn = 3;
        public static int QuoteComponent_Flightoneway = 4;
        public static int QuoteComponent_Accommodation = 5;
        public static int QuoteComponent_Transfer = 6;
        public static int QuoteComponent_Bags = 7;
        public static int QuoteStatus_Success = 9;
        public static int QuoteStatus_Failure = 10;
        public static int QuoteStatus_New = 8;
        public static int Direction_Inbound = 11;
        public static int Direction_Outbound = 12;
        public static int PassengerType_Adult = 13;
        public static int PassengerType_Child = 14;
        public static int PassengerType_Infant = 15;
        public static int CobrowsStatus_New = 16;
        public static int CobrowsStatus_Approved = 17;
        public static int CobrowsStatus_Rejected = 18;
        public static int CobrowsStatus_Expired = 19;
        public static int SesameBasket_Create = 20;
        public static int SesameBasket_Refresh = 21;
        public static int SesameBasket_Book = 22;
        //public static int Direction_RoundTrip = 23;
        public static int Transfers_Returning = 24;
        public static int Transfers_Travelling = 25;
        public static int Flights = 41;
        public static int Resorts = 42;


        // TSValue
        public static int LowCostAirlineValueId = 9;
        public static int QuoteStatus = 1;
        public static int QuoteComponent = 2;
        public static int QuoteType = 3;
        public static int Direction = 4;
        public static int PassengerType = 5;
        public static int CobrowsStatus = 6;
        public static int SesameQuoteRequests = 7;
        public static int Transfers = 8;
        public static int LCA = 9;
        public static int Title = 10;
        public static int MappingRecordType = 12;
        public static int ITSystems = 11;
        public static int CountryMaster = 13;


        public static string EasyJetMarketingAirlineCode = "U2";
        public static string EasyJetMarketingAirline = "EasyJet";
        public static string EasyJetBookingType = "EasyJet";
        public static string NonEasyJetBookingType = "Non-EasyJet";
        public static string HybridBookingType = "Hybrid";
        public static string EUROCurrencyCode = "EUR";
        public static string GBPCurrencyCode = "GBP";
        public static string FlightReturn = "Flight Return";

        public static string BagsTypeBaggage = "baggage";
        public static string BagsTypeAdditionalWeight = "additional_weight";
        public static int EzJetDefaultWeight = 20;
        public static string EzJetDefaultWeightKey = "EzJetDefaultWeightKey";

        public static string SesameFlightComponentIdentifier = "FLI";
        public static string EasyJetBaggageType_Baggage = "baggage";
        public static string EasyJetBaggageType_Additional_Weight = "additional_weight";
        public static string EasyJetBaggageType_SportsEquipment = "equipment";
        public static string NoHotelsFound = "Hotel not found";
        public static string TopDogResponse = "TopDog Response";
        public static string LatLongType = "GEO";


        public enum ItenaryType
        {
            Returns = 3,
            OneWay = 4
        }

        
    }
}
