﻿using Swordfish.Businessobjects.Quotes;
using System;
using System.Collections.Generic;

namespace Swordfish.Businessobjects.Common
{
    public class SearchData
    {
        public string TTSSQuoteId { get; set; }

        public string sfQuoteId { get; set; }

        public string sourceName { get; set; }

        public string destinationName { get; set; }

        public DateTime startDate { get; set; }

        public DateTime endDate { get; set; }

        public string sourceCode { get; set; }

        public string destinationCode { get; set; }

        public string hotelName { get; set; }

        public decimal starRating { get; set; }

        public bool BoardTypeAI { get; set; }
        public bool BoardTypeFB { get; set; }
        public bool BoardTypeHB { get; set; }
        public bool BoardTypeBB { get; set; }
        public bool BoardTypeSC { get; set; }
        public bool BoardTypeRO { get; set; }
        public int CreatedBy { get; set; }
        public int CustomerId { get; set; }
        public int RoomNo { get; set; }
        public int AdultsCount { get; set; }
        public int ChildrenCount { get; set; }
        public int InfantsCount { get; set; }
        public int Nights { get; set; }
        public List<Occupancy> OccupancyRooms { get; set; }
        public SearchData()
        {
            OccupancyRooms = new List<Occupancy>();
        }
    }

}
