﻿namespace Swordfish.Businessobjects.Hotels
{
    public class HotelDetailRequest
    {
        public string SupplierCode { get; set; }
        public string SupplierAccommodationId { get; set; }
    }
}
