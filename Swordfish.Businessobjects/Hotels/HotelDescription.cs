﻿namespace Swordfish.Businessobjects.Hotels
{
    public class HotelDescription
    {
        public string DescriptionCode { get; set; }
        public string DescriptionTitle { get; set; }
        public string Description { get; set; }
    }
}
