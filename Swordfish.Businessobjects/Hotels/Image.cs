﻿namespace Swordfish.Businessobjects.Hotels
{
    public class Image
    {
        public string url { get; set; }
        public string caption { get; set; }
    }
}
