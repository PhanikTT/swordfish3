﻿using System.Collections.Generic;

namespace Swordfish.Businessobjects.Hotels
{
    public class HotelInfo
    {
        public List<DestinationHotels> Hotel { get; set; }
        public List<HotelDescription> HotelDescription { get; set; }
        public List<HotelImages> HotelImages { get; set; }
        public List<HotelAddress> HotelAddress { get; set; }
    }
}
