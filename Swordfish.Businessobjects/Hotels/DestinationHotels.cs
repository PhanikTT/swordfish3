﻿namespace Swordfish.Businessobjects.Hotels
{
    public class DestinationHotels
    {
        public string Name { get; set; }
        public string Rating { get; set; }
        public string TTICode { get; set; }
        public string SupplierAccommId { get; set; }
        public string SupplierCode { get; set; }
        public string SupplierResortId { get; set; }
        public string TripAdvisorScore { get; set; }
    }
}
