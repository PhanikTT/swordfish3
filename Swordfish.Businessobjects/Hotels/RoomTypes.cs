﻿namespace Swordfish.Businessobjects.Hotels
{
    public class RoomTypes
    {
        public string RoomTypeCode { get; set; }
        public string RoomDescription { get; set; }
    }
}
