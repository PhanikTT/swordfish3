﻿namespace Swordfish.Businessobjects.Hotels
{
    public class Resort
    {
        public int ResortId { get; set; }
        public string SupplierResortId { get; set; }
        public string Name { get; set; }
        public string AirportCodes { get; set; }
        public string SubResortId { get; set; }
    }
}
