﻿using System;

namespace Swordfish.Businessobjects.Hotels
{
    public class HotelRequest
    {
        public string destination { get; set; }
        public int HotelQty { get; set; }
        public int guests { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public int nights { get; set; }
        public string TTICode { get; set; }
        public decimal Rating { get; set; }
        public int Rooms { get; set; }
        public int Adults { get; set; }
        public int Child { get; set; }
        public int Infants { get; set; }
        public int Nights { get; set; }
        public string ForCompany { get; set; }
    }
}
