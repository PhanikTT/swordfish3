﻿using System.Collections.Generic;

namespace Swordfish.Businessobjects.Hotels
{
    public class Hotel
    {
        //public string Name { get; set; }
        //public double Price { get; set; }
        //public string Star { get; set; }
        //public string Desc { get; set; }
        //public string TRating { get; set; } //Tripadvisor Rating
        //public string StartDate { get; set; }
        //public double Days { get; set; }

        public List<RoomTypes> RoomTypes { get; set; }
        public List<RoomPlans> RoomPlans { get; set; }
        public List<RoomRates> RoomRates { get; set; }
        public List<BasicPropertyInfo> BasicPropertyInfo { get; set; }
        public List<HotelRating> Rating { get; set; }
    }
}
