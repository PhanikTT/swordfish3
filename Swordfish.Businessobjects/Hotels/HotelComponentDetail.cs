﻿namespace Swordfish.Businessobjects.Hotels
{
    public class HotelComponentDetail
    {
        public string RoomTypeCode { get; set; }
        public string RatePlanCode { get; set; }
        public string RatePlanName { get; set; }
        public string RateQuoteId { get; set; }
        public decimal AmountAfterTax { get; set; }
        public int CreatedBy { get; set; }
        public int ModifiedBy { get; set; }
        public string RoomDescription { get; set; }
        public bool? NonRefundable { get; set; }
        public bool? CancelPolicyIndicator { get; set; }
    }
}
