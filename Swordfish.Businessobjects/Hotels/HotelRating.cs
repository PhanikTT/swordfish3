﻿namespace Swordfish.Businessobjects.Hotels
{
    public class HotelRating
    {
        public decimal Rating { get; set; }
        public string HotelCode { get; set; }
        public decimal TripAdvisor { get; set; }
    }
}
