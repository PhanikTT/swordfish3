﻿namespace Swordfish.Businessobjects.Hotels
{
    public class BasicPropertyInfo
    {
        public string TTIcode { get; set; }
        public string HotelCodeContext { get; set; }
        public string HotelCode { get; set; }
        public string HotelName { get; set; }
        public string HotelCity { get; set; }
        public string ReferenceId { get; set; }
        public string ReferenceType { get; set; }
    }
}
