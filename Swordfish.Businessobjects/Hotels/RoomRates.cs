﻿namespace Swordfish.Businessobjects.Hotels
{
    public class RoomRates
    {
        public string RoomTypeCode { get; set; }
        public string RatePlanCode { get; set; }
        public string QuoteId { get; set; }
        public string AmountAfterTax { get; set; }
    }
}
