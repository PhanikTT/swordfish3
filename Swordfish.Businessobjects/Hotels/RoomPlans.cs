﻿namespace Swordfish.Businessobjects.Hotels
{
    public class RoomPlans
    {
        public string RatePlanName { get; set; }
        public string RatePlanCode { get; set; }
    }
}
