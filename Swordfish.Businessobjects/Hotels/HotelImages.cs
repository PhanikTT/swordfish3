﻿namespace Swordfish.Businessobjects.Hotels
{
    public class HotelImages
    {
        public string Caption { get; set; }
        public string Url { get; set; }
    }
}
