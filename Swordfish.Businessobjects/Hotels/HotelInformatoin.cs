﻿using System.Collections.Generic;

namespace Swordfish.Businessobjects.Hotels
{
    public class HotelInformatoin
    {
        public string Name { get; set; }
        public string Rating { get; set; }
        public string TTICode { get; set; }
        public string SupplierAccommId { get; set; }
        public string SupplierCode { get; set; }
        public string SupplierResortId { get; set; }
        public string TripAdvisorScore { get; set; }
        public HotelAddress Address { get; set; }
        public List<Description> Descriptions { get; set; }
        public List<Image> Images { get; set; }
    }
}
