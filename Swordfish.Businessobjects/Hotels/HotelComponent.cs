﻿using System;
using System.Collections.Generic;

namespace Swordfish.Businessobjects.Hotels
{
    public class HotelComponent
    {
        public string HotelName { get; set; }
        public string HotelCode { get; set; }
        public string HotelCodeContext { get; set; }
        public string SupplierResortId { get; set; }

        public string SupplierResortName { get; set; }
        public string ComponentType { get; set; }
        public int ComponentTypeOptionId { get; set; }

        public int TTICode { get; set; }
        public decimal StarRating { get; set; }
        public decimal TripAdvisorRating { get; set; }

        public int HotelTypeId { get; set; }
        public string HotelQuoteId { get; set; }
        public int CreatedBy { get; set; }
        public int ModifiedBy { get; set; }
        public DateTime? CheckInDate { get; set; }
        public DateTime? CheckOutDate { get; set; }
        public HotelInformatoin HotelInformatoin { get; set; }
        public List<HotelComponentDetail> hotelComponentsDetails { get; set; }
    }
}
