﻿namespace Swordfish.Businessobjects.Baggage
{
    public class BagComponentEZJet
    {
        public int PassengerNumber { get; set; }
        public int BagsCount { get; set; }
        public decimal BagsCost { get; set; }
        public int AdditionalWeight { get; set; }
        public decimal AdditionalWeightCost { get; set; }
        public int DefaultWeight { get; set; }
        public int TotalWeight { get; set; }
        public decimal TotalBagsCost { get; set; }
        public int DirectionOptionId { get; set; }
    }
}
