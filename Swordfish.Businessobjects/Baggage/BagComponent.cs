﻿namespace Swordfish.Businessobjects.Baggage
{
    public class BagComponent
    {
        public string BagsType { get; set; }
        public string BagsDescription { get; set; }
        public string BagsCode { get; set; }
        public int QuantityAvailable { get; set; }
        public int QuantityRequired { get; set; }
        public decimal BuyingPrice { get; set; }
        public decimal SellingPrice { get; set; }
        public decimal Price { get; set; }

        public string Items { get; set; }
        public int CreatedBy { get; set; }
        public int ModifiedBy { get; set; }
        public string Direction { get; set; }
        public int DirectionOptionId { get; set; }
        public int PassengerIndex { get; set; }
    }
}
