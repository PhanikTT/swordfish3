﻿using System;

namespace Swordfish.Businessobjects.Flights
{
    public class FlightSegments
    {
        public string FlightNumber { get; set; }
        public DateTime DepartureTime { get; set; }
        public DateTime ArrivalTime { get; set; }
        public string DepartureAirport { get; set; }
        public string ArrivalAirport { get; set; }
        public int NoOfSeats { get; set; }
        public string OperatorCode { get; set; }//Added
        public string AirlineCode { get; set; }//Added
    }
}
