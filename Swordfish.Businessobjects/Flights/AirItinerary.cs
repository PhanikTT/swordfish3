﻿namespace Swordfish.Businessobjects.Flights
{
    public class AirItinerary
    {
        public string FlightNumber { get; set; }
        public string MarketingAirline { get; set; }
        public string MarketingAirlineCode { get; set; }
        public string ArrivalDateTime { get; set; }
        public string DepartureDateTime { get; set; }
        public string DepartureAirportLocationCode { get; set; }
        public string ArrivalAirportLocationCode { get; set; }
        public string JourneyIndicator { get; set; }
        public string FlightType { get; set; }
        public string CabinClass { get; set; }
    }
}
