﻿namespace Swordfish.Businessobjects.Flights
{
    public class AirItineraryPricingInfo
    {
        public string QuoteId { get; set; }
        public string TotalFareCurrencyCode { get; set; }
        public string TotalFareAmount { get; set; }
        public string BaseFareCurrencyCode { get; set; }
        public string BaseFareAmount { get; set; }
    }
}
