﻿using System.Collections.Generic;

namespace Swordfish.Businessobjects.Flights
{
    public class FlightComponent
    {
        public string CurrencyCode { get; set; }
        public string Amount { get; set; }
        public string Source { get; set; }
        public string Arrival { get; set; }
        public int ComponentTypeOptionId { get; set; }
        public string componentType { get; set; }
        public int CreatedBy { get; set; }
        public int ModifiedBy { get; set; }
        public string FlightQuoteId { get; set; }
        public decimal? BaseFareAmount { get; set; }
        public string BaseFareCurrencyCode { get; set; }
        public double? ExchangeRateUsed { get; set; }
        public string SourceUrl { get; set; }
        public List<FlightComponentDetail> FlightComponentDetails { get; set; }
    }
}
