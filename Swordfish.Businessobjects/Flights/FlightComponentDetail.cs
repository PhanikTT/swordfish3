﻿using System;

namespace Swordfish.Businessobjects.Flights
{
    public class FlightComponentDetail
    {
        public DateTime DepartureDateTime { get; set; }
        public DateTime ArrivalDateTime { get; set; }
        public string FlightNumber { get; set; }
        public string JourneyIndicator { get; set; }
        public string DepartureAirportCode { get; set; }
        public string DepartureAirportName { get; set; }
        public string ArrivalAirportCode { get; set; }
        public string ArrivalAirportName { get; set; }
        public string MarketingAirLine { get; set; }
        public string MarketingAirlineCode { get; set; }
        public string Direction { get; set; }
        public int DirectionOptionId { get; set; }
        public int CreatedBy { get; set; }
        public int ModifiedBy { get; set; }
        public string TravelDuration { get; set; }
        public string CabinClass { get; set; }
    }
}
