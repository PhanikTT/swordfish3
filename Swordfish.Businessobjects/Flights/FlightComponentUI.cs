﻿using System.Collections.Generic;

namespace Swordfish.Businessobjects.Flights
{
    public class FlightComponentUI
    {
        public int flightTypeID { get; set; }
        public string flightType { get; set; }

        public List<FlightComponentDetail> flightComponentsOutbound { get; set; }
        public List<FlightComponentDetail> flightComponentsInbound { get; set; }

        public FlightComponentUI()
        {
            flightComponentsOutbound = new List<FlightComponentDetail>();
            flightComponentsInbound = new List<FlightComponentDetail>();
        }
    }
}
