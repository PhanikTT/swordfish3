﻿using System.Collections.Generic;

namespace Swordfish.Businessobjects.Flights
{
    public class Flight
    {
        public List<AirItinerary> AirItinerary { get; set; }
        public List<AirItineraryPricingInfo> AirItineraryPricingInfo { get; set; }
        public string SourceURL { get; set; }
    }
}
