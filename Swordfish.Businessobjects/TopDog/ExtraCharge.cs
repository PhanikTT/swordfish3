﻿namespace Swordfish.Businessobjects.TopDog
{
    public class ExtraCharge
    {
        public string EC_CD { get; set; }
        public float EC_Price { get; set; }
        public float EC_Price_Rate { get; set; }
        public string EC_Price_Rule { get; set; }
        public float EC_Cost { get; set; }
        public float EC_Cost_Rate { get; set; }
        public string EC_Cost_Rule { get; set; }
        public string EC_PerPerson { get; set; }
        public string EC_Dtl { get; set; }
        public string EC_AppRule { get; set; }
        public string EC_Hide { get; set; }
    }
}

