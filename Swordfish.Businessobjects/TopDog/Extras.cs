﻿namespace Swordfish.Businessobjects.TopDog
{
    public class Extras
    {
        public string ProductType { get; set; }
        public string ProductTypeCode { get; set; }
        public string ProductLabel { get; set; }
        public string ProductReference { get; set; }
        public string ProductReferenceDescription { get; set; }
        public int Quantity { get; set; }
        public decimal Fare { get; set; }
    }
}