﻿namespace Swordfish.Businessobjects.TopDog
{
    public class TopDogInfo
    {
        public string Quote_id { get; set; }
        public string TOPDogStatus { get; set; }
        public string TOPDogRefNo { get; set; }
        public string TOPDogInfo { get; set; }
    }
}