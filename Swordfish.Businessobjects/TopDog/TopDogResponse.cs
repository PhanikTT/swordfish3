﻿namespace Swordfish.Businessobjects.TopDog
{
    public class TopDogResponse
    {
        public string TopDogRefNum { get; set; }
        public string VerbName { get; set; }
        public string VerbResponse { get; set; }
        public bool IsVisible { get; set; }
        public bool IsError { get; set; }
    }
}
