﻿using System;

namespace Swordfish.Businessobjects.TTSS
{
    public class TTSSFlightDetails
    {
        public DateTime DepartureDate { get; set; }
        public string Fare { get; set; }
    }
}
