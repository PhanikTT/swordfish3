﻿namespace Swordfish.Businessobjects.TTSS
{
    public class BoardCodePrices
    {
        public string BoardCode { get; set; }
        public string Fare { get; set; }
    }
}