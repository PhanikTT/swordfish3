﻿using System;
using System.Collections.Generic;

namespace Swordfish.Businessobjects.TTSS
{
    public class TTSSPackage
    {
        public string QuoteRef { get; set; }
        public string Accommodation { get; set; }
        public int Duration { get; set; }
        public string BoardType { get; set; }
        public string AccommodationFare { get; set; }
        public string SourceFrom { get; set; }
        public string DestinationTo { get; set; }
        public DateTime? OutboundArrivalTime { get; set; }
        public DateTime? InboundArrivalTime { get; set; }
        public string MarketingAirLane { get; set; }
        public string Rating { get; set; }
        public DateTime? OutboundDepartureDtTime { get; set; }
        public DateTime? InboundDepartureDtTime { get; set; }
        public List<BoardCodePrices> BoardTypePrices { get; set; }
    }
}
