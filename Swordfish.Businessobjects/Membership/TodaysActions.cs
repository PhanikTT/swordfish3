﻿using System;

namespace Swordfish.Businessobjects.Membership
{
    public class TodaysActions
    {
        public int TaskId { get; set; }
        public int UserId { get; set; }
        public string TaskText { get; set; }
        public string TodaysActionDate { get; set; }
        public int TaskPrimaryKeyid { get; set; }
        public int[] TaskPKeyid { get; set; }
        public string Type { get; set; }
        public string remindersdate { get; set; }
        public Int16 Is_deleted { get; set; }
        public string reminder_status { get; set; }
        public int reminder { get; set; }
    }
}
