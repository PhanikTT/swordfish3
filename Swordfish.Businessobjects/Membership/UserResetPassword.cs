﻿namespace Swordfish.Businessobjects.Membership
{
    public class UserResetPassword
    {
        public string Username { get; set; }
        public string NewPassword { get; set; }
    }
}
