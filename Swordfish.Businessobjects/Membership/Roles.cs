﻿using System;

namespace Swordfish.Businessobjects.Membership
{
    public class Roles
    {
        public int RoleId { get; set; }
        public string Description { get; set; }
        public string RoleName { get; set; }
        public int CreatedBy { get; set; }
        public DateTime CreatedOn { get; set; }
        public int? UpdatedBy { get; set; }
        public DateTime? UpdatedOn { get; set; }
        public string Action { get; set; }
    }
}
