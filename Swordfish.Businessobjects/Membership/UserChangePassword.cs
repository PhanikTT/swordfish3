﻿namespace Swordfish.Businessobjects.Membership
{
    public class UserChangePassword
    {
        public string Username { get; set; }
        public string OldPassword { get; set; }
        public string NewPassword { get; set; }
    }
}
