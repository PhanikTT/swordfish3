﻿using System;

namespace Swordfish.Businessobjects.Membership
{
    public class Agent
    {
        public string Email { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Password { get; set; }
        public int CreatedBy { get; set; }
        public DateTime CreatedOn { get; set; }
        public string syntecAccountID { get; set; }
    }
}
