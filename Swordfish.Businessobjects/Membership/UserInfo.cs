﻿using System;
using System.Collections.Generic;

namespace Swordfish.Businessobjects.Membership
{
    public class UserInfo
    {
        public int UserId { get; set; }
        public string Username { get; set; }
        public string UserEmailAddress { get; set; }
        public string Password { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Address { get; set; }
        public int TeamId { get; set; }
        public bool IsActive { get; set; }
        public DateTime ValidFrom { get; set; }
        public int CreatedBy { get; set; }
        public DateTime CreatedOn { get; set; }
        public System.Int32? UpdatedBy { get; set; }
        public System.DateTime? UpdatedOn { get; set; }
        public string TeamName { get; set; }
        public string TierName { get; set; }
        public string TierDescription { get; set; }
        public string Imagepath { get; set; }
        public DateTime? LastLoginDate { get; set; }
        public string MobileNumber { get; set; }
        public string Morgin { get; set; }
        public string Special { get; set; }
        public string ReportsTo { get; set; }
        public int MarginFlightMin { get; set; }
        public int MarginFlightMax { get; set; }
        public int MarginHotelMin { get; set; }
        public int MarginHotelMax { get; set; }
        public int MarginExtraMin { get; set; }
        public int MarginExtraMax { get; set; }
        public virtual ICollection<UserRoles> UserAssignedRoles { get; set; }

        public string Initial { get; set; }
        public string Title { get; set; }
        public string AddressType { get; set; }
        public string Address1 { get; set; }
        public string Address2 { get; set; }
        public string Address3 { get; set; }
        public string State { get; set; }
        public string Country { get; set; }
        public string PostCode { get; set; }
        public string EmailType { get; set; }
        public string PhoneType { get; set; }
        public string Phone { get; set; }
        public string SessionId { get; set; }
        public string Environment { get; set; }
        public string SyntechAccountId { get; set; }
        public int TopDogId { get; set; }
    }
}
