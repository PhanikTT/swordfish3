﻿namespace Swordfish.Businessobjects.Membership
{
    public class GraphData
    {

        public string WeekDate { get; set; }
        public string Count { get; set; }
        public long? Week1 { get; set; }
        public long? Week2 { get; set; }
        public long? Week3 { get; set; }
        public long? Week4 { get; set; }
    }
}