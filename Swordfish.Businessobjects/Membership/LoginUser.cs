﻿namespace Swordfish.Businessobjects.Membership
{
    public class LoginUser
    {
        public string Username { get; set; }
        public string Password { get; set; }
    }
}
