﻿using System;

namespace Swordfish.Businessobjects.Customers
{
    public class CustomerQuotes
    {
        public DateTime QuotedDate { set; get; }
        public string QuoteRef { set; get; }
        public string Source { get; set; }
        public string Destination { get; set; }
        public string HotelName { get; set; }
        public DateTime FormDate { get; set; }
        public DateTime ToDate { get; set; }
        public int NumberOfAdults { get; set; }
        public int NumberOfChilds { get; set; }
        public int NumberOfInfants { get; set; }
        public int NumberOfNights { get; set; }
        public decimal TotalPrice { get; set; }
        public decimal PricePerPerson { get; set; }
    }
}
