﻿using System;

namespace Swordfish.Businessobjects.Customers
{
    public class CustomerPhoneNumbers
    {
        public int PhoneNumberId { get; set; }
        public int CustomerId { get; set; }
        public string PhoneNumberType { get; set; }
        public string PhoneNumber { get; set; }
        //Changes by Himansu on mar 1st
        public DateTime CreatedOn { get; set; }
        public string CreatedBy { get; set; }
    }
}
