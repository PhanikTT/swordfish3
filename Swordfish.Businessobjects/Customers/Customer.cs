﻿using System;
using System.Collections.Concurrent;

namespace Swordfish.Businessobjects.Customers
{
    public class Customer
    {
        public int CustomerId { get; set; }
        public string Title { get; set; }
        public string email_address { get; set; }
        public string Password { get; set; }
        public string first_name { get; set; }
        public string last_name { get; set; }
        public string CreatedBy { get; set; }
        public DateTime CreatedOn { get; set; }
        public string UpdatedBy { get; set; }
        public string UpdatedOn { get; set; }

        //Changes by Himansu on mar 1st
        public string EmailType { get; set; }
        public string City { get; set; }
        public string Country { get; set; }
        public string Postcode { get; set; }
        public string MembershipNo { get; set; }
        public bool emailConsent { get; set; }
        public bool OptedForSMS { get; set; }
        public bool OptedForEmail { get; set; }
        //for CustomerAddress table
        public string AddressType { get; set; }
        public string Address { get; set; }
        public string address_line1 { get; set; }
        public string address_line2 { get; set; }
        public string HouseNumber { get; set; }

        //for CustomerPhoneNumbers table
        public string PhoneNumberType { get; set; }
        public string phone_number { get; set; }
        public string telephone_number { get; set; }
        public string CountryCode { get; set; }
        public int numberOfBookedQuotes { get; set; }
        public int totalQuotes { get; set; }
        public bool IsEmail { set; get; }
        public bool IsSms { set; get; }
        public ConcurrentBag<CustomerAddress> CustomerAddressInfo { get; set; }
        public ConcurrentBag<CustomerPhoneNumbers> CustomerPhoneNumbersInfo { get; set; }
    }
}
