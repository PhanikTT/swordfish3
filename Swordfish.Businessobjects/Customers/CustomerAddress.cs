﻿using System;

namespace Swordfish.Businessobjects.Customers
{
    public class CustomerAddress
    {
        public int AddressId { get; set; }
        public int CustomerId { get; set; }
        public string AddressType { get; set; }
        public string Address { get; set; }
        //Changes by Himansu on mar 1st
        public string address_line1 { get; set; }
        public string address_line2 { get; set; }
        public DateTime CreatedOn { get; set; }
        public string CreatedBy { get; set; }
    }
}
