﻿namespace Swordfish.Businessobjects.Customers
{
    public class IncomingCustomer
    {
        public int CustomerId { get; set; }
        public int Userid { get; set; }
        public string QuoteId { get; set; }
        public string LoginDateTime { get; set; }
        public string LogoutDateTime { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string EmailAddress { get; set; }
        public string PhoneNumber { get; set; }
        public string Address { get; set; }
    }
}
