﻿namespace Swordfish.Businessobjects.Quotes
{
    public class QuoteRefHotel
    {
        public string Description { get; set; }
        public string Code { get; set; }
        public string BoardCode { get; set; }
    }
}
