﻿using System.Collections.Generic;

namespace Swordfish.Businessobjects.Quotes
{
    public class QuoteRefComponent
    {
        public string ComponentId { get; set; }
        public string ProductTypeCode { get; set; }
        public string SupplierCode { get; set; }
        public string Type { get; set; }
        public string Description { get; set; }
        public string SourceCurrency { get; set; }
        public string SourcePricePence { get; set; }
        public string SellingPricePence { get; set; }
        public bool BookingAttempted { get; set; }
        public bool BookingSucceeded { get; set; }
        public bool BookingCancelled { get; set; }
        public string StatusCode { get; set; }
        public string SupplierBookingId { get; set; }
        public string ErrataInformation { set; get; }
        public List<QuoteRefHotel> QuoteHotel { get; set; }
        public List<QuoteRefFlight> Flight { get; set; }
    }
}
