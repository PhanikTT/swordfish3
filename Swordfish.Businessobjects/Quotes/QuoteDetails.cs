﻿using System;

namespace Swordfish.Businessobjects.Quotes
{
    public class QuoteDetails
    {
        public int BasketId { get; set; }
        public string QuoteRef { get; set; }

        public string QuoteStatus { get; set; }
        public string ConsumerBookingReference { get; set; }
        public string CustomerFirstName { get; set; }
        public string CustomerLastName { get; set; }
        public string CustomerEmailAddress { get; set; }
        public string CustomerAddress { get; set; }
        public string CustomerPhoneNUmber { get; set; }
        public DateTime? QuoteDate { get; set; }
        public DateTime? BookingDate { get; set; }
        public string CurrencyCode { get; set; }
        public int NoOfAdults { get; set; }
        public int NoOfChildren { get; set; }
        public int NoOfInfants { get; set; }

        public FlightComponent FlightDetails { get; set; }
        public HotelComponent HotelDetails { get; set; }
        public BagsComponent BagsDetails { get; set; }
        public TransfersComponent TransfersDetails { get; set; }
    }
}
