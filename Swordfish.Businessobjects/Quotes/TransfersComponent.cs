﻿namespace Swordfish.Businessobjects.Quotes
{
    public class TransfersComponent
    {
        public int TransfersComponentId { get; set; }
        public string TransfersDescription { get; set; }
        public string TransfersProductId { get; set; }
        public decimal TransfersFare { get; set; }
        public int VehicalsQuantity { get; set; }
    }
}
