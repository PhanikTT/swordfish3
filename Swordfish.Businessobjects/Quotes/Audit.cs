﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Swordfish.Businessobjects.Quotes
{
    public class Audit
    {
        public string Error { get; set; }
        public string Duration { get; set; }
        public string Node { get; set; }
    }
}
