﻿using System;

namespace Swordfish.Businessobjects.Quotes
{
    public class QuoteOverride
    {
        public bool IsOverride { get; set; }
        public int quoteHeaderId { get; set; }
        public decimal totalAmount { get; set; }
        public decimal depositAmount { get; set; }
        public decimal amountPaid { get; set; }
        public decimal amountDue { get; set; }
        public DateTime dateOfTravel { get; set; }
        public int agentId { get; set; }
        public string approverId { get; set; }
        public string comments { get; set; }
        public int createBy { get; set; }
    }
}
