﻿using System;

namespace Swordfish.Businessobjects.Quotes
{
    public class HotelComponent
    {
        public int HotelComponentId { get; set; }
        public string AccommodationSupplierId { get; set; }
        public string AccommodationSupplierCode { get; set; }
        public string AccommodationName { get; set; }
        public string HotelCode { get; set; }
        public string HotelCodeContext { get; set; }
        public string HotelCityCode { get; set; }
        public string HotelCity { get; set; }
        public string TTICode { get; set; }
        public string Address { get; set; }
        public decimal Rating { get; set; }
        public decimal UserScore { get; set; }
        public string BoardCode { get; set; }
        public int RoomsRequired { get; set; }
        public int NoOfNights { get; set; }
        public string RoomDescription { get; set; }
        public string RoomCode { get; set; }
        public string RoomRateQuote { get; set; }
        public decimal HotelFare { get; set; }
        public DateTime CheckIn { get; set; }
        public DateTime CheckOut { get; set; }
    }
}
