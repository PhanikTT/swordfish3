﻿namespace Swordfish.Businessobjects.Quotes
{
    public class QuoteHeader
    {
        public int QuoteHeadderId { get; set; }
        public string CustomerReferenceCode { get; set; }
        public string quotestatus { get; set; }
        public int AgentId { get; set; }
        public int CustomerId { get; set; }
        public string CustomerTitle { get; set; }
        public string CustomerFirstName { get; set; }
        public string CustomerLastName { get; set; }
        public string CustomerEmailAddress { get; set; }
        public string MobileNumber { get; set; }
        public string TelephoneNumber { get; set; }
        public string CountryCode { get; set; }
        public string HouseNumber { get; set; }
        public string PostCode { get; set; }
        public string Address { get; set; }
        public string City { get; set; }
        public string Country { get; set; }
        public int quoteStatusOptionId { get; set; }
        public decimal HotelCost { get; set; }
        public decimal BaggageCost { get; set; }
        public decimal TransfersCost { get; set; }
        public decimal FidelityCost { get; set; }
        public decimal TotalCost { get; set; }
        public decimal FlightCost { get; set; }
        public decimal GrandTotal { get; set; }
        public decimal PricePerPerson { get; set; }
        public string InternalNotes { get; set; }
        public string SentToEmail { get; set; }
        public string SentToSms { get; set; }
        public int CreatedBy { get; set; }
        public int ModifiedBy { get; set; }
        public decimal MarginHotel { get; set; }
        public decimal MarginFlight { get; set; }
        public decimal MarginExtras { get; set; }
    }
}
