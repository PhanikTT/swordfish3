﻿using System.Collections.Generic;

namespace Swordfish.Businessobjects.Quotes
{
    public class QuoteRefBasket
    {
        public string BasketId { get; set; }
        public string CurrencyCode { get; set; }
        public List<QuoteRefComponent> ComponentHotel { get; set; }
        public List<QuoteRefComponent> ComponentFlight { get; set; }
    }
}
