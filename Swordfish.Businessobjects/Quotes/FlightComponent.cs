﻿using Swordfish.Businessobjects.Flights;
using System.Collections.Generic;

namespace Swordfish.Businessobjects.Quotes
{
    public class FlightComponent
    {
        public int FlightComponentId { get; set; }
        public string FlightSupplierId { get; set; }
        public string FlightSupplierCode { get; set; }
        public int NoOfAdults { get; set; }
        public int NoOfChildren { get; set; }
        public int NoOfInfants { get; set; }
        public List<FlyingDetails> FlyingDetails { get; set; }
    }
}
