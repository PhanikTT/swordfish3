﻿using Swordfish.Businessobjects.Baggage;
using Swordfish.Businessobjects.Common;
using Swordfish.Businessobjects.Easyjet;
using Swordfish.Businessobjects.Flights;
using Hotels = Swordfish.Businessobjects.Hotels;
using Swordfish.Businessobjects.Sesame;
using Swordfish.Businessobjects.Transfers;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using Flights = Swordfish.Businessobjects.Flights;
using Swordfish.Businessobjects.Membership;

namespace Swordfish.Businessobjects.Quotes
{
    public class Quote
    {

        public SearchData searchModel { get; set; }

        public QuoteHeader quoteHeader { get; set; }
        public string PageExpireMsg { get; set; }
        public List<Flights.FlightComponent> flightComponents { get; set; }

        public FlightComponentUI flightComponentUI { get; set; }

        public Swordfish.Businessobjects.Hotels.HotelComponent hotelComponent { get; set; }

        public List<TransferComponent> transferComponents { get; set; }

        public List<BagComponent> bagsComponents { get; set; }

        public List<BagComponentEZJet> bagComponentEZJets { get; set; }
        public SesameBasket SesameBasket { get; set; }
        public SesameBasket SesameBookBasket { get; set; }
        public TransfersBookData BookedHolidayTaxiData { get; set; }
        public List<EasyJetBookingStatus> EasyJetBookingStatus { get; set; }
        public List<EasyJetResponse> EasyJetBookings { get; set; }
        public UserInfo UserInfo { set; get; }
        #region Topdog Static Fields
        public string TransferOperatorCode { get; set; }
        public string HotelOperatorCode { get; set; }
        public string FlightOperatorCode { get; set; }
        public string FlightBookingReference { get; set; }
        public string FlightDescription { get; set; }
        public string FlightServiceClass { get; set; }
        public string FlightOperator { get; set; }
        public string Currency { get; set; }
        public string TopDogUserId { get; set; }
        public string TopDogPassword { get; set; }
        public string TopDogLanguage { get; set; }
        public string CCNumber { get; set; }
        public string CCType { get; set; }
        public string CCStartDate { get; set; }
        public string CCEndDate { get; set; }
        public string SecurityNum { get; set; }
        public int ArrivalPickUp { get; set; }
        public int DeparturePickUp { get; set; }
        public string AddressType { get; set; }
        public string PhoneType { get; set; }
        public string EmailType { get; set; }
        public decimal TopDogHotelCommision { get; set; }
        public decimal TopDogTransferCommision { get; set; }
        public string TopDogPassengerNationality { get; set; }        
        #endregion
        #region CardEasy Fields
        public string CardEasyEndPoint { get; set; } 
        #endregion
        public Quote()
        {
            searchModel = new SearchData();
            quoteHeader = new QuoteHeader();
            flightComponents = new List<Flights.FlightComponent>();
            hotelComponent = new Hotels.HotelComponent();
            transferComponents = new List<TransferComponent>();
            bagsComponents = new List<BagComponent>();
            flightComponentUI = new FlightComponentUI();
            SesameBasket = new SesameBasket();
            bagComponentEZJets = new List<BagComponentEZJet>();
            SesameBookBasket = new SesameBasket();
            BookedHolidayTaxiData = new TransfersBookData();
            EasyJetBookingStatus = new List<EasyJetBookingStatus>();
        }

        public void PopulateFlightComponentUI()
        {
            List<FlightComponentDetail> flightComponentsDetails = new List<FlightComponentDetail>();
            flightComponentUI.flightType = flightComponents[0].componentType;
            foreach (Flights.FlightComponent flightComponent in flightComponents)
            {
                foreach (var details in flightComponent.FlightComponentDetails)
                {
                    FlightComponentDetail FlightComponentDetail = new FlightComponentDetail();
                    FlightComponentDetail.DepartureDateTime = details.DepartureDateTime;
                    FlightComponentDetail.ArrivalDateTime = details.ArrivalDateTime;
                    FlightComponentDetail.FlightNumber = details.FlightNumber;
                    FlightComponentDetail.JourneyIndicator = details.JourneyIndicator;
                    FlightComponentDetail.DepartureAirportCode = details.DepartureAirportCode;
                    FlightComponentDetail.ArrivalAirportCode = details.ArrivalAirportCode;
                    FlightComponentDetail.MarketingAirLine = details.MarketingAirLine;
                    FlightComponentDetail.MarketingAirlineCode = details.MarketingAirlineCode;
                    FlightComponentDetail.Direction = details.Direction;
                    FlightComponentDetail.DirectionOptionId = details.DirectionOptionId;
                    var estHours = (Convert.ToDateTime(details.ArrivalDateTime) - Convert.ToDateTime(details.DepartureDateTime));
                    FlightComponentDetail.TravelDuration = estHours.Hours.ToString("00") + ":" + estHours.Minutes.ToString("00");
                    if (details.DirectionOptionId == SFConstants.Direction_Outbound)
                    {
                        flightComponentUI.flightComponentsOutbound.Add(FlightComponentDetail);
                    }
                    else if (details.DirectionOptionId == SFConstants.Direction_Inbound)
                    {
                        flightComponentUI.flightComponentsInbound.Add(FlightComponentDetail);
                    }
                }
            }
        }
        public void PopulateBagsComponentForEzJet()
        {
            // case 1 Returns with EzJet // TSoption 23 is returns
            if (flightComponents.Count == 1 && flightComponents[0].ComponentTypeOptionId == SFConstants.QuoteComponent_FlightReturn)
            {
                // Check if the marketing airline is ezjet
                if (flightComponents[0].FlightComponentDetails[0].MarketingAirlineCode.Trim() == SFConstants.EasyJetMarketingAirlineCode)
                {
                    // As this is a returns there will only ezJet bags.
                    // TBD move the bags to EzJet bags structure
                    AddBagsComponentForEzJet(SFConstants.QuoteComponent_FlightReturn);
                }
            }
            else// Case oneway
            {
                foreach (Flights.FlightComponent tempFlightComponent in flightComponents)
                {// Case:Outbound ID = 11 and Airline is EzJet
                    if (tempFlightComponent.FlightComponentDetails[0].DirectionOptionId == SFConstants.Direction_Outbound &&
                        tempFlightComponent.FlightComponentDetails[0].MarketingAirlineCode.Trim() == SFConstants.EasyJetMarketingAirlineCode)
                    {
                        AddBagsComponentForEzJet(SFConstants.Direction_Outbound);
                    }
                    else
                    {
                        AddBagsComponentForEzJet(SFConstants.Direction_Inbound);

                    }

                }
            }
        }

        private void AddBagsComponentForEzJet(int DirectionOptionId)
        {
            try
            {
                BagComponent TempBagsCount = null;
                BagComponent TempBagsWeight = null;
                int bagSetCount = 0;
                for (int i = 0; i < bagsComponents.Count; i++)
                {
                    if (bagsComponents[i].DirectionOptionId == DirectionOptionId && bagsComponents[i].BagsType == SFConstants.BagsTypeBaggage)
                    {
                        TempBagsCount = bagsComponents[i];
                        bagSetCount++;
                    }
                    if (bagsComponents[i].DirectionOptionId == DirectionOptionId && bagsComponents[i].BagsType == SFConstants.BagsTypeAdditionalWeight)
                    {
                        TempBagsWeight = bagsComponents[i];
                        bagSetCount++;
                    }

                    if (bagSetCount == 2)
                    {// now we got both the hand bags and weight item
                        BagComponentEZJet tempBagComponentEZJet = new BagComponentEZJet();

                        tempBagComponentEZJet.DirectionOptionId = DirectionOptionId;
                        tempBagComponentEZJet.AdditionalWeight = int.Parse(TempBagsWeight.BagsDescription.ToLower().Replace("kg", "").Trim());
                        tempBagComponentEZJet.AdditionalWeightCost = TempBagsWeight.SellingPrice;
                        tempBagComponentEZJet.DefaultWeight = ConfigurationManager.AppSettings[SFConstants.EzJetDefaultWeightKey] == null ? SFConstants.EzJetDefaultWeight : int.Parse(ConfigurationManager.AppSettings[SFConstants.EzJetDefaultWeightKey].ToString());
                        int startPosition = TempBagsCount.BagsDescription.IndexOf("x");
                        tempBagComponentEZJet.BagsCount = int.Parse(TempBagsCount.BagsDescription.Substring(0, startPosition).Trim());
                        tempBagComponentEZJet.BagsCost = TempBagsCount.SellingPrice;

                        tempBagComponentEZJet.TotalWeight = tempBagComponentEZJet.DefaultWeight + tempBagComponentEZJet.AdditionalWeight;
                        //Case 1: No additional bags but additional weight
                        if(tempBagComponentEZJet.BagsCount == 0)
                            tempBagComponentEZJet.TotalBagsCost = tempBagComponentEZJet.AdditionalWeightCost;
                        else
                            tempBagComponentEZJet.TotalBagsCost = tempBagComponentEZJet.AdditionalWeightCost + (tempBagComponentEZJet.BagsCost / tempBagComponentEZJet.BagsCount);
                        //Case 2: additional bags with additional weight
                        bagComponentEZJets.Add(tempBagComponentEZJet);
                        bagSetCount = 0;
                    }
                }
            }
            catch (Exception)
            {
            }
        }
    }
}
