﻿using System.Collections.Generic;

namespace Swordfish.Businessobjects.Quotes
{
    public class QuoteRef
    {
        public List<Audit> Audit { get; set; }
        public List<QuoteRefBasket> Basket { get; set; }
    }
}
