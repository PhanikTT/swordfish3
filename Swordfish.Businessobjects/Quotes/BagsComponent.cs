﻿namespace Swordfish.Businessobjects.Quotes
{
    public class BagsComponent
    {
        public int BagComponentId { get; set; }
        public int BagsCount { get; set; }
        public decimal BagsFare { get; set; }
    }
}
