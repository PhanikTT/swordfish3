﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Swordfish.Businessobjects.Quotes
{
    public class QuoteRefBaggage
    {
        public string Description { get; set; }
        public string Code { get; set; }
        public string Type { get; set; }
        public string QuantityAvailable { get; set; }
        public string QuantityRequired { get; set; }
        public string BuyingPricePence { get; set; }
        public string SellingPricePence { get; set; }
    }
}
