﻿using System;

namespace Swordfish.Businessobjects.Quotes
{
    public class AgentQuote
    {
        public int QuoteHeaderID { get; set; }
        public string CustomerReferenceCode { get; set; }
        public DateTime QuotedOn { get; set; }
        public string CustomerTitle { get; set; }
        public string CustomerFirstName { get; set; }
        public string CustomerLastName { get; set; }
        public string SourceCode { get; set; }
        public string DestinationCode { get; set; }
        public DateTime FromtDate { get; set; }
        public DateTime ToDate { get; set; }
        public string HotelName{ get; set; }
        public int? AdultsCount { get; set; }
        public int? ChildrenCount { get; set; }
        public int? InfantsCount { get; set; }
        public double GrandTotal { get; set; }
        public double PricePerPerson { get; set; }
        public int NumberOfNights { get; set; }
        public string AgentName { get; set; }

    }
}
