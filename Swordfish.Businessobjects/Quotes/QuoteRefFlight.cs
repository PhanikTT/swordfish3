﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Swordfish.Businessobjects.Quotes
{
    public class QuoteRefFlight
    {
        public string FromAirportCode { get; set; }
        public string ToAirportCode { get; set; }
        public string DepartureDateTime { get; set; }
        public string ArrivalDateTime { get; set; }
        public List<QuoteRefBaggage> Baggage { get; set; }
    }
}
