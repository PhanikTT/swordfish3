﻿using Swordfish.Businessobjects.TTSS;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using Swordfish.Utilities.ELKLog;
using System.Diagnostics;
using System.Xml.Linq;
using System.Web.Script.Serialization;

namespace Swordfish.Components.TTSSService
{
    public class TTSSService : ITTSSService
    {
        public async Task<TTSSPackage> GetTtssQuoteDetails(string requestUrl)
        {
            TTSSPackage ttssPackage = new TTSSPackage();
            var ELKLogret = WriteTopDogLog("Info", "GetTtssQuoteDetails() calling Started!!. at " + DateTime.Now.ToString());
            try
            {
                List<BoardCodePrices> lstBoardTypesFare = new List<BoardCodePrices>();
                TTSSQuote lstTripDetails = new TTSSQuote();
                string tripDetailsFromCache = null;//CacheRepository.GetData(requestUrl);

                if (!string.IsNullOrEmpty(tripDetailsFromCache) && tripDetailsFromCache.Length > 2)
                {
                    var serializ = new JavaScriptSerializer();
                    lstTripDetails = serializ.Deserialize<TTSSQuote>(tripDetailsFromCache);
                }
                else
                {
                    lstTripDetails = await Task.Run(() => LoadXMLFromGetRequest(requestUrl));
                    // var serializer = new JavaScriptSerializer();
                    //var serializedResult = serializer.Serialize(lstTripDetails);
                    //CacheRepository.SetData(requestUrl, serializedResult);
                }

                if (lstTripDetails.Adults != null)
                {
                    TTSSQuote leastFarePackage = new TTSSQuote();
                    leastFarePackage = (TTSSQuote)lstTripDetails;

                    ttssPackage.QuoteRef = string.IsNullOrEmpty(leastFarePackage.QuoteRef) ? "N/A" : leastFarePackage.QuoteRef;
                    ttssPackage.Accommodation = string.IsNullOrEmpty(leastFarePackage.Accommodation) ? "N/A" : leastFarePackage.Accommodation;
                    ttssPackage.AccommodationFare = string.IsNullOrEmpty(leastFarePackage.AccomPrice) ? "N/A" : leastFarePackage.AccomPrice;
                    ttssPackage.BoardType = leastFarePackage.BoardTypeCode.Trim();
                    ttssPackage.DestinationTo = string.IsNullOrEmpty(leastFarePackage.ToAirport) ? "N/A" : leastFarePackage.ToAirport;
                    ttssPackage.SourceFrom = string.IsNullOrEmpty(leastFarePackage.FromAirport) ? "N/A" : leastFarePackage.FromAirport;

                    ttssPackage.Duration = Convert.ToInt32(leastFarePackage.Duration);
                    ttssPackage.MarketingAirLane = string.IsNullOrEmpty(leastFarePackage.FlightOperator) ? "N/A" : leastFarePackage.FlightOperator;
                    ttssPackage.OutboundDepartureDtTime = Convert.ToDateTime(leastFarePackage.DepartureDate);
                    ttssPackage.OutboundArrivalTime = Convert.ToDateTime(leastFarePackage.OutboundArrivalDate);
                    ttssPackage.InboundDepartureDtTime = Convert.ToDateTime(leastFarePackage.InboundDepartureDate);
                    ttssPackage.InboundArrivalTime = Convert.ToDateTime(leastFarePackage.ReturnDate);
                    ttssPackage.Rating = leastFarePackage.Rating.ToString();
                    lstBoardTypesFare.Add(new BoardCodePrices { BoardCode = GetBoardCode(ttssPackage.BoardType), Fare = ttssPackage.AccommodationFare });
                    ttssPackage.BoardTypePrices = lstBoardTypesFare;
                }
            }
            catch (Exception ex)
            {
                ELKLogret = WriteTopDogLog("Error", "GetTtssQuoteDetails() calling Error Occured::" + ex.Message.ToString() + " at " + DateTime.Now.ToString());
                throw ex;
            }
            ELKLogret = WriteTopDogLog("Info", "GetTtssQuoteDetails() calling completed!!. at " + DateTime.Now.ToString());
            return ttssPackage;
        }

        public async Task<TTSSQuote> GetQuote(string requestUrl)
        {
            TTSSQuote tripDetails = new TTSSQuote();
            var ELKLogret = WriteTopDogLog("Info", "GetQuote() calling Started!!. at " + DateTime.Now.ToString());
            try
            {
                string tripDetailsFromCache = null;//CacheRepository.GetData(requestUrl);
                if (!string.IsNullOrEmpty(tripDetailsFromCache) && tripDetailsFromCache.Length > 2)
                {
                    var serializ = new JavaScriptSerializer();
                    tripDetails = serializ.Deserialize<TTSSQuote>(tripDetailsFromCache);
                }
                else
                {
                    var lstTripDetails = await Task.Run(() => LoadXMLFromGetRequest(requestUrl));


                    if (lstTripDetails != null)
                    {
                        tripDetails = (TTSSQuote)lstTripDetails;
                    }
                }
            }
            catch (Exception ex)
            {
                ELKLogret = WriteTopDogLog("Error", "GetQuote() calling Error Occured::" + ex.Message.ToString() + " at " + DateTime.Now.ToString());
                throw ex;
            }
            ELKLogret = WriteTopDogLog("Info", "GetQuote() calling end!!. at " + DateTime.Now.ToString());
            return tripDetails;
        }

        private TTSSQuote LoadXMLFromGetRequest(string requestUrl)
        {
            TTSSQuote lstTripDetails = new TTSSQuote();
            var ELKLogret = WriteTopDogLog("Info", "LoadXMLFromGetRequest() calling Started!!. at " + DateTime.Now.ToString());
            try
            {
                string userID = "travelclub";
                string password = "xmlfeed";

                HttpWebRequest webRequest = (HttpWebRequest)HttpWebRequest.Create(requestUrl);
                webRequest.Headers.Add("UserID:" + userID + ";");
                webRequest.Headers.Add("Password:" + password + ";");
                webRequest.Method = "POST";
                webRequest.ContentType = "text/xml";
                HttpWebResponse webResponse = (HttpWebResponse)webRequest.GetResponse();
                if (webResponse.StatusCode != HttpStatusCode.OK)
                {
                    StringBuilder sbErrorMessage = new StringBuilder();
                    sbErrorMessage.Append("Status Code = " + webResponse.StatusCode);
                    sbErrorMessage.Append(Environment.NewLine);
                    sbErrorMessage.Append("Status Description = " + webResponse.StatusDescription.ToString());
                    sbErrorMessage.Append(Environment.NewLine);
                    sbErrorMessage.Append("RequestXML = " + requestUrl);
                    sbErrorMessage.Append(Environment.NewLine);
                    sbErrorMessage.Append("ResponseXML = " + webResponse.ToString());
                    throw new Exception(sbErrorMessage.ToString());
                }

                //Get the response stream
                System.IO.Stream ResponseStream = webResponse.GetResponseStream();
                System.IO.StreamReader sr = new System.IO.StreamReader(ResponseStream);
                var response = sr.ReadToEnd();
                sr.Close();

                XDocument xdoc = XDocument.Parse(response);

                lstTripDetails = xdoc.Element("Container").Element("Results")
                                .Elements("Result")
                                .Select(e => new TTSSQuote()
                                {
                                    Id = (string)e.Attribute("id"),
                                    DestinationId = (string)e.Attribute("destinationId"),
                                    TradingNameId = (string)e.Attribute("tradingNameId"),
                                    ClientId = (string)e.Attribute("clientId"),
                                    TradingName = (string)e.Attribute("tradingName"),
                                    TradingNameImage = (string)e.Attribute("tradingNameImage"),
                                    Phone = (string)e.Attribute("phone"),
                                    DepartureDate = (string)e.Attribute("departureDate"),
                                    OutboundArrivalDate = (string)e.Attribute("outboundArrivalDate"),
                                    InboundDepartureDate = (string)e.Attribute("inboundDepartureDate"),
                                    ReturnDate = (string)e.Attribute("returnDate"),
                                    Duration = (string)e.Attribute("duration"),
                                    DepartureId = (string)e.Attribute("departureId"),
                                    Departure = (string)e.Attribute("departure"),
                                    BoardTypeId = (string)e.Attribute("boardTypeId"),
                                    BoardTypeCode = (string)e.Attribute("boardTypeCode"),
                                    Adults = (string)e.Attribute("adults"),
                                    Children = (string)e.Attribute("children"),
                                    Rating = (string)e.Attribute("rating"),
                                    Price = (string)e.Attribute("price"),
                                    IsPreferential = (string)e.Attribute("isPreferential"),
                                    Allocation = (string)e.Attribute("allocation"),
                                    GatewayCode = (string)e.Attribute("gatewayCode"),
                                    GatewayName = (string)e.Attribute("gatewayName"),
                                    Accommodation = (string)e.Attribute("accommodation"),
                                    ContentOverview = (string)e.Attribute("contentOverview"),
                                    ContentImageSrc = (string)e.Attribute("contentImageSrc"),
                                    Updated = (string)e.Attribute("updated"),
                                    ContentSource = (string)e.Attribute("contentSource"),
                                    Source = (string)e.Attribute("source"),
                                    FlightOperator = (string)e.Attribute("flightOperator"),
                                    FromAirport = (string)e.Attribute("fromAirport"),
                                    ToAirport = (string)e.Attribute("toAirport"),
                                    FlightPrice = (string)e.Attribute("flightPrice"),
                                    AccomPrice = (string)e.Attribute("accomPrice"),
                                    Destination = (string)e.Attribute("destination"),
                                    Glong = (string)e.Attribute("glong"),
                                    Glat = (string)e.Attribute("glat"),
                                    HotelOperator = (string)e.Attribute("hotelOperator"),
                                    ContentId = (string)e.Attribute("contentId"),
                                    HotelKey = (string)e.Attribute("hotelKey"),
                                    QuoteRef = (string)e.Attribute("quoteRef"),
                                    F1 = (string)e.Attribute("f1"),
                                    F2 = (string)e.Attribute("f2"),
                                    F3 = (string)e.Attribute("f3"),
                                    F4 = (string)e.Attribute("f4"),
                                    F5 = (string)e.Attribute("f5"),
                                    F6 = (string)e.Attribute("f6"),
                                    F7 = (string)e.Attribute("f7"),
                                    F8 = (string)e.Attribute("f8"),
                                    F9 = (string)e.Attribute("f9"),
                                    F10 = (string)e.Attribute("f10"),
                                    F11 = (string)e.Attribute("f11"),
                                    F12 = (string)e.Attribute("f12"),
                                    F13 = (string)e.Attribute("f13"),
                                    F14 = (string)e.Attribute("f14"),
                                    F15 = (string)e.Attribute("f15"),
                                    F16 = (string)e.Attribute("f16"),
                                    F17 = (string)e.Attribute("f17"),
                                    F18 = (string)e.Attribute("f18"),
                                    F19 = (string)e.Attribute("f19"),
                                    F20 = (string)e.Attribute("f20"),
                                    OtherBonding = (string)e.Attribute("otherBonding"),
                                    HotelOperatorId = (string)e.Attribute("hotelOperatorId"),
                                    OperatorsRating = (string)e.Attribute("operatorsRating"),
                                    AccommodationType = (string)e.Attribute("accommodationType"),
                                    OutboundFlight = (string)e.Attribute("outboundFlight"),
                                    InboundFlight = (string)e.Attribute("inboundFlight"),
                                    FlightSupplierId = (string)e.Attribute("flightSupplierId"),
                                    AirportFromId = (string)e.Attribute("airportFromId"),
                                    AirportToId = (string)e.Attribute("airportToId"),
                                    AccomMargin = (string)e.Attribute("accomMargin"),
                                    HotelMargin = (string)e.Attribute("hotelMargin"),
                                    OrigAccomPrice = (string)e.Attribute("origAccomPrice"),
                                    BookingFee = (string)e.Attribute("bookingFee"),
                                    OrigFlightPrice = (string)e.Attribute("origFlightPrice"),
                                    GatewayMargin = (string)e.Attribute("gatewayMargin"),
                                    OrigPkgPrice = (string)e.Attribute("origPkgPrice")
                                }).FirstOrDefault();
            }

            catch (WebException ex)
            {
                var resp = new StreamReader(ex.Response.GetResponseStream()).ReadToEnd();
                var log = Logger.Log("Error occured at Teletext.RequestSesame() for requestUrl=" + requestUrl + " ;<br/> and URL=" + requestUrl, ";<br/> Response XML= " + resp + "; StackTrace=" + ex.StackTrace + "; <br/>InnerException=" + ex.InnerException + ";<br/>Error Message" + ex.Message.ToString());

                ELKLogret = WriteTopDogLog("Error", "LoadXMLFromGetRequest() calling Error Occured::" + ex.Message.ToString() + " at " + DateTime.Now.ToString());
            }
            catch (Exception ex)
            {
                var log = Logger.Log("Error occured at Teletext.RequestSesame() for requestXml=" + requestUrl + " ;<br/> and URL=" + requestUrl, ";<br/>  StackTrace=" + ex.StackTrace + "; <br/>InnerException=" + ex.InnerException == null ? "" : ex.InnerException + ";<br/>Error Message" + ex.Message.ToString());
                throw ex;
            }
            ELKLogret = WriteTopDogLog("Info", "LoadXMLFromGetRequest() calling Started!!. at " + DateTime.Now.ToString());
            return lstTripDetails;
        }

        private string GetBoardCode(string BoardType)
        {
            string boardCode = string.Empty;
            var ELKLogret = WriteTopDogLog("Info", "GetBoardCode() calling Started!!. at " + DateTime.Now.ToString());
            switch (BoardType)
            {
                case "Self Catering":
                    boardCode = "SC";
                    break;
                case "Room Only":
                    boardCode = "RO";
                    break;
                case "Bed &amp; Breakfast":
                    boardCode = "BB";
                    break;
                case "All Inclusive":
                    boardCode = "AI";
                    break;
                case "Full Board":
                    boardCode = "FB";
                    break;
                case "Half Board":
                    boardCode = "HB";
                    break;
            }
            ELKLogret = WriteTopDogLog("Info", "GetBoardCode() calling end!!. at " + DateTime.Now.ToString());
            return boardCode;
        }


        public static Task WriteTopDogLog(string LogType, string LogMessage)
        {
            try
            {
                Task.Run(async () =>
                {
                    await Logger.Log(LogType, LogMessage);
                }).ConfigureAwait(false);

            }
            catch (Exception ex)
            {
                WriteEVWLog(LogMessage + "::" + ex.Message, "E");
            }

            return Task.Delay(1);
        }

        public static Task WriteEVWLog(string LogMessage, string LogType)
        {
            try
            {
                if (!EventLog.SourceExists("Chilli"))
                    EventLog.CreateEventSource("Chilli", LogMessage);

                if (LogType == "I")
                    EventLog.WriteEntry("Chilli", LogMessage + " at " + DateTime.Now.ToString(), EventLogEntryType.Information, 100);
                else if (LogType == "E")
                    EventLog.WriteEntry("Chilli", LogMessage + " at " + DateTime.Now.ToString(), EventLogEntryType.Error, 100);
            }
            catch
            {
                //Consume error
            }
            return Task.Delay(1);

        }

    }
}
