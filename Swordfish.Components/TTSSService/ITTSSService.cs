﻿using Swordfish.Businessobjects.TTSS;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Swordfish.Components.TTSSService
{
    public interface ITTSSService
    {
        Task<TTSSPackage> GetTtssQuoteDetails(string requestUrl);
        Task<TTSSQuote> GetQuote(string requestUrl);
    }
}
