﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Swordfish.Components.BookingFulfilment
{
    public interface IBookingFulfilment
    {
        void FulfilBooking();
    }
}
