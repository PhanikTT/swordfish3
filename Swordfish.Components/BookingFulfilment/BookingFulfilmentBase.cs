﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Swordfish.Components.BookingFulfilment
{
    public class BookingFulfilmentBase
    {
        public IBookingFulfilment CreateBookingFulfilment()
        {
            return new BookingFulfilmentGeneralReturn();
        }

        protected void LoadQuote() { }
        protected void LoadSFBasket() { }
        protected void LoadSesameBasket() { }
        protected void LoadPassengers() { }
        protected void CreateBasket() { }
        protected void RefreshBasket() { }
        protected void BookBasket() { }

        //protected void CreateEUROasket() { }
        //protected void RefreshEUROBasket() { }
        //protected void BookEUROBasket() { }

        protected void BookTransfers() { }

        protected void RedirectTOEasyJetBooking() { }


    }
}
