﻿using Newtonsoft.Json;
using Swordfish.Businessobjects.Customers;
using Swordfish.Businessobjects.Passengers;
using Swordfish.Businessobjects.Quotes;
using Swordfish.Businessobjects.TopDog;
using Swordfish.Utilities.DataAccess;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Threading.Tasks;
using System.Linq;
using System.Collections.Concurrent;
using Swordfish.Businessobjects.Common;
using System.Globalization;
using Swordfish.Businessobjects.Payments;
using Swordfish.Businessobjects.Sesame;
using Swordfish.Businessobjects.Transfers;
using Swordfish.Businessobjects.Flights;
using Newtonsoft.Json.Linq;
using Swordfish.Businessobjects.Hotels;
using Swordfish.Businessobjects.Baggage;

namespace Swordfish.Components.BookingServices.DataAccess
{
    public class BookingServiceDL
    {
        public string SaveBasket(Quote basket)
        {
            string result = string.Empty;
            try
            {
                var jsonInput = JsonConvert.SerializeObject(basket);

                var db = new CommandRunner(ConfigurationManager.ConnectionStrings["chilliDataBase"].ConnectionString.ToString());
                string sql = "select * from save_basket_and_generate_quote('" + jsonInput.ToString() + "') as quotestatus";
                var res = db.ExecuteDynamic(sql);

                foreach (var x in res)
                {
                    result = x.quotestatus;
                }
            }
            catch (Exception ex)
            {
                var log = Utilities.ELKLog.Logger.Log("Error", ex.Message.ToString());
                throw ex;
            }

            return result.ToString();
        }
        public bool SaveTopDogResponse(List<TopDogResponse> topDogResponse, int quoteheaderId, int userid)
        {
            var result = false;
            var jsonInput = JsonConvert.SerializeObject(topDogResponse);
            var db = new CommandRunner(ConfigurationManager.ConnectionStrings["chilliDataBase"].ConnectionString.ToString());
            string sql = "select * from savetopdogresponse('" + jsonInput.Replace("'", "''").ToString() + "'," + quoteheaderId + "," + userid + ") as topdogresponsesavestatus";
            var res = db.ExecuteDynamic(sql);

            foreach (var x in res)
            {
                result = Convert.ToBoolean(x.topdogresponsesavestatus);
            }
            return result;
        }
        public List<ExtraCharge> GetExtraCharges()
        {
            List<ExtraCharge> ExtraChargeLst = new List<ExtraCharge>();
            try
            {
                var db = new CommandRunner(ConfigurationManager.ConnectionStrings["chilliDataBase"].ConnectionString.ToString());
                string sql = "select * from get_TopDog_ExtCharges(1)";
                var res = db.ExecuteDynamic(sql);
                foreach (var x in res)
                {
                    ExtraChargeLst.Add(new ExtraCharge()
                    {
                        EC_CD = x.ec_cd,
                        EC_Price = (float)x.ec_price,
                        EC_Price_Rate = (float)x.ec_price_rate,
                        EC_Price_Rule = x.ec_price_rule,
                        EC_Cost = (float)x.ec_cost,
                        EC_Cost_Rate = (float)x.ec_cost_rate,
                        EC_Cost_Rule = x.ec_cost_rule,
                        EC_PerPerson = x.ec_perperson,
                        EC_Dtl = x.ec_dtl,
                        EC_AppRule = x.ec_apprule,
                        EC_Hide = x.ec_hide,
                    });
                }
            }
            catch (Exception ex)
            {
                var ELKLogstr = Utilities.ELKLog.Logger.Log("Error", ex.Message.ToString());
            }
            return ExtraChargeLst;
        }
        public Task<string> SaveCoBrowsDetails(string QuoteId, string ClientId, string AgentId, string Url_Id, Passenger Leadpassenger, List<CoPassengers> CoPassengers, string LeadEmail, string LeadMob)
        {
            string result = null;
            string CoPassInput;
            string sql = string.Empty;
            try
            {
                var LeadPassInput = JsonConvert.SerializeObject(Leadpassenger);
                if (CoPassengers.Count > 0)
                    CoPassInput = JsonConvert.SerializeObject(CoPassengers);
                else
                {
                    CoPassInput = "N";
                }

                var db = new CommandRunner(ConfigurationManager.ConnectionStrings["chilliDataBase"].ConnectionString.ToString());
                sql = "select * from insert_CoBrows_data('" + QuoteId + "','" + AgentId + "','" + ClientId + "','" + Url_Id + "','" + LeadPassInput + "','" + CoPassInput + "','" + LeadEmail + "','" + LeadMob + "') as cobrowsstatus";

                var res = db.ExecuteDynamic(sql);

                foreach (var x in res)
                {
                    result = Convert.ToString(x.cobrowsstatus);
                }
                if (result != null)
                {
                    return Task.FromResult(result);
                }
            }
            catch (Exception ex)
            {
                Utilities.ELKLog.Logger.Log("Error", "Error in SaveCoBrowsDetails()::" + ex.Message.ToString());
            }
            return Task.FromResult(result);
        }
        public async Task<List<CoBrows>> GetCoBrowsDetails(string Brows_key)
        {
            try
            {
                bool IsDataFound = false;
                List<CoBrows> CoBrowsDataLst = new List<CoBrows>();
                CoBrows CoBrowsData = new CoBrows();
                List<CoPassengers> CoPassenger = new List<CoPassengers>();
                var db = new CommandRunner(ConfigurationManager.ConnectionStrings["chilliDataBase"].ConnectionString.ToString());
                string sql = "select * from get_Passenger_Info('" + Brows_key + "')";
                var res = db.ExecuteDynamic(sql);
                foreach (var pax in res)
                {
                    if (pax.leadpassenger == "Y")
                    {
                        CoBrowsData.CoBrows_Id = Convert.ToString(pax.brows_id);
                        CoBrowsData.Client_Id = pax.client_id;
                        CoBrowsData.Agent_Id = pax.agent_id;
                        CoBrowsData.Quote_Id = pax.quote_id;
                        CoBrowsData.Title = pax.title;
                        CoBrowsData.FirstName = pax.firstname;
                        CoBrowsData.LastName = pax.lastname;
                        CoBrowsData.DateOfBirth = (pax.dateofbirth != null && pax.dateofbirth != "") ? pax.dateofbirth : null;
                        CoBrowsData.Gender = pax.sex;
                        CoBrowsData.Address1 = pax.address1;
                        CoBrowsData.Address2 = pax.address2;
                        CoBrowsData.Address3 = pax.address3;
                        CoBrowsData.Mobile = pax.mobile;
                        CoBrowsData.Telephone = pax.telephone;
                        CoBrowsData.PostCode = pax.postcode;
                        CoBrowsData.Email = pax.email;
                        CoBrowsData.PassportNumber = pax.passportnumber;
                    }
                    else
                    {
                        CoPassengers CoPassengers = new CoPassengers();
                        CoPassengers.Title = pax.title;
                        CoPassengers.FirstName = pax.firstname;
                        CoPassengers.LastName = pax.lastname;
                        CoPassengers.DateOfBirth = (pax.dateofbirth != null && pax.dateofbirth != "") ? pax.dateofbirth : null;
                        CoPassengers.PassengerType = pax.sex;
                        CoPassengers.Email = pax.email;
                        CoPassengers.PassportNumber = pax.passportnumber;
                        CoPassengers.IsAdultOrChild = pax.isadultorchild;
                        CoPassenger.Add(CoPassengers);
                    }
                    IsDataFound = true;
                }
                if (IsDataFound == true)
                {
                    CoBrowsData.CoPassengersList = CoPassenger;
                    CoBrowsDataLst.Add(CoBrowsData);
                }
                return CoBrowsDataLst;
            }
            catch (Exception ex)
            {
                var ELKLogstr = Utilities.ELKLog.Logger.Log("Error", ex.Message.ToString());
            }
            return null;
        }
        public async Task<bool> COBrowsDataStatus(string CoBrowID, string confm)
        {
            bool result = false;
            try
            {
                var db = new CommandRunner(ConfigurationManager.ConnectionStrings["chilliDataBase"].ConnectionString.ToString());
                string sql = "select * from Upd_CoBrows_Status(" + CoBrowID + ",'" + confm + "') as cobrowsstatus";
                var res = db.ExecuteDynamic(sql);

                foreach (var x in res)
                {
                    result = Convert.ToBoolean(x.cobrowsstatus);
                }
            }
            catch (Exception ex)
            {
                var ELKLogstr = Utilities.ELKLog.Logger.Log("Error", ex.Message.ToString());
            }

            return result;
        }
        public async Task<string> SaveCustomerDetails(Customer customer)
        {
            string result = string.Empty;
            try
            {

                var dbCmd = new CommandRunner(ConfigurationManager.ConnectionStrings["chilliDataBase"].ConnectionString.ToString());
                string sqlString = "select * from  insert_customer_details('" + customer.email_address + "','" + customer.first_name + "','" + customer.last_name + "','" + null + "','" + null + "','" + customer.EmailType + "','" + customer.City + "','" + customer.Country + "','" + customer.Postcode + "','" + customer.HouseNumber + "','" + customer.MembershipNo + "','" + customer.CreatedBy + "','" + false + "','" + null + "','" + null + "','" + customer.address_line1 + "','" + customer.address_line2 + "','" + null + "','" + customer.phone_number + "','" + customer.telephone_number + "','" + customer.CountryCode + "','" + customer.emailConsent + "','" + customer.Title + "','" + customer.OptedForEmail + "','" + customer.OptedForSMS + "','Insert')as myoutput";
                var DBresponse = dbCmd.ExecuteDynamic(sqlString);

                foreach (var x in DBresponse)
                {
                    result = x.myoutput;
                }
            }
            catch (Exception ex)
            {
                result = "f";
                var log = Utilities.ELKLog.Logger.Log("Error", ex.Message.ToString());

            }
            return result.ToString();
        }
        public async Task<List<Customer>> SearchCustomerDetails(Customer customer)
        {
            var CustDetails = new List<Customer>();
            string result = string.Empty;
            try
            {
                var dbCmd = new CommandRunner(ConfigurationManager.ConnectionStrings["chilliDataBase"].ConnectionString.ToString());
                string sqlString = "select* from get_customer('" + customer.MembershipNo + "','" + customer.first_name + "','" + customer.EmailType + "','" + customer.email_address + "','" + customer.phone_number + "','" + customer.Postcode + "')";

                var DBresponse = dbCmd.ExecuteDynamic(sqlString);
                foreach (var item in DBresponse.ToList())
                {
                    var custobj = new Customer();
                    custobj.email_address = item.email_address;
                    custobj.MembershipNo = item.membership_no;
                    custobj.first_name = item.first_name;
                    custobj.last_name = item.last_name;
                    custobj.phone_number = item.phone_number;
                    custobj.telephone_number = item.telephone_number;
                    custobj.CountryCode = item.country_code;
                    custobj.HouseNumber = item.house_number;
                    custobj.Postcode = item.postcode;
                    custobj.CustomerId = item.customer_id;
                    custobj.numberOfBookedQuotes = Convert.ToInt32(item.numberofbookedquotes);
                    custobj.totalQuotes = Convert.ToInt32(item.totalquotes);
                    custobj.Title = item.title;
                    CustDetails.Add(custobj);
                }
            }
            catch (Exception ex)
            {
                var log = Utilities.ELKLog.Logger.Log("Error", ex.Message.ToString());
            }
            return CustDetails;
        }
        public async Task<List<Customer>> GetCustomerDatabyMembershipNo(Customer customer)
        {
            List<Customer> CustDetails = new List<Customer>();
            string result = string.Empty;
            try
            {
                var dbCmd = new CommandRunner(ConfigurationManager.ConnectionStrings["chilliDataBase"].ConnectionString.ToString());
                string sqlString = "select* from get_customer('" + customer.MembershipNo + "','','','','','')";

                var DBresponse = dbCmd.ExecuteDynamic(sqlString);
                foreach (var item in DBresponse.ToList())
                {
                    Customer custobj = new Customer();
                    custobj.email_address = item.email_address;
                    custobj.MembershipNo = item.membership_no;
                    custobj.first_name = item.first_name;
                    custobj.last_name = item.last_name;
                    custobj.phone_number = item.phone_number;
                    custobj.telephone_number = item.telephone_number;
                    custobj.CountryCode = item.country_code;
                    custobj.Postcode = item.postcode;
                    custobj.address_line1 = item.address_line1;
                    custobj.address_line2 = item.address_line2;
                    custobj.HouseNumber = item.house_number;
                    custobj.City = item.city;
                    custobj.Country = item.country;
                    custobj.EmailType = item.emailtype;
                    custobj.CustomerId = item.customer_id;
                    custobj.emailConsent = item.email_consent ?? false;
                    custobj.Title = item.title;
                    custobj.OptedForSMS = item.opted_for_sms ?? false;
                    custobj.OptedForEmail = item.opted_for_email ?? false;
                    CustDetails.Add(custobj);
                }
            }
            catch (Exception ex)
            {
                var log = Utilities.ELKLog.Logger.Log("Error", ex.Message.ToString());
            }
            return CustDetails;
        }
        public async Task<string> UpdateCustomerDetails(Customer customer)
        {
            string result = string.Empty;
            try
            {
                var dbCmd = new CommandRunner(ConfigurationManager.ConnectionStrings["chilliDataBase"].ConnectionString.ToString());
                string sqlString = "select * from  insert_customer_details('" + customer.email_address + "','" + customer.first_name + "','" + customer.last_name + "','" + null + "','" + null + "','" + customer.EmailType + "','" + customer.City + "','" + customer.Country + "','" + customer.Postcode + "','" + customer.HouseNumber + "','" + customer.MembershipNo + "','" + customer.CreatedBy + "','" + false + "','" + null + "','" + null + "','" + customer.address_line1 + "','" + customer.address_line2 + "','" + null + "','" + customer.phone_number + "','" + customer.telephone_number + "','" + customer.CountryCode + "','" + customer.emailConsent + "','" + customer.Title + "','" + customer.OptedForEmail + "','" + customer.OptedForSMS + "','Update')as myoutput";
                var DBresponse = dbCmd.ExecuteDynamic(sqlString);

                foreach (var x in DBresponse)
                {
                    result = x.myoutput;
                }
            }
            catch (Exception ex)
            {
                result = "f";
                var log = Utilities.ELKLog.Logger.Log("Error", ex.Message.ToString());
            }
            return result.ToString();
        }
        public async Task<ConcurrentBag<CustomerQuotes>> getCustomerQuotes(int customerId)
        {
            var result = new ConcurrentBag<CustomerQuotes>();
            try
            {
                var dbCmd = new CommandRunner(ConfigurationManager.ConnectionStrings["chilliDataBase"].ConnectionString.ToString());
                var sqlString = "select * from get_customer_quotes(" + customerId + ")";
                var DBResponce = dbCmd.ExecuteDynamic(sqlString);
                foreach (var item in DBResponce)
                {
                    var customerQuote = new CustomerQuotes();
                    customerQuote.QuotedDate = item.quoteddate;
                    customerQuote.QuoteRef = item.quoteref;
                    customerQuote.HotelName = item.hotelname;
                    customerQuote.TotalPrice = (decimal)item.totalprice;
                    customerQuote.PricePerPerson = (decimal)item.priceperperson;
                    customerQuote.Source = item.source;
                    customerQuote.Destination = item.destination;
                    customerQuote.FormDate = item.formdate;
                    customerQuote.ToDate = item.todate;
                    customerQuote.NumberOfAdults = item.numberofadults;
                    customerQuote.NumberOfChilds = item.numberofchilds;
                    customerQuote.NumberOfInfants = item.numberofinfants;
                    result.Add(customerQuote);
                }
            }
            catch (Exception ex)
            {
                Utilities.ELKLog.Logger.Log("Error", ex.Message.ToString());
            }
            return result;
        }
        public async Task<int> SaveAgentJournal(int AgentId, string QuoteRef, string JournalType, string JournalNotes)
        {
            int result = 0;
            try
            {
                var dbCmd = new CommandRunner(ConfigurationManager.ConnectionStrings["chilliDataBase"].ConnectionString.ToString());
                string sqlString = "select * from  insert_agent_journals(" + AgentId + ",'" + QuoteRef + "','" + JournalType + "','" + JournalNotes + "' ) as journalId";
                var DBresponse = dbCmd.ExecuteDynamic(sqlString);

                foreach (var x in DBresponse)
                {
                    result = x.journalid;
                }
            }
            catch (Exception ex)
            {
                Utilities.ELKLog.Logger.Log("Error", ex.Message.ToString());
            }
            return result;

        }
        public SearchData getsearchModel(string sfQuoteId)
        {
            SearchData searchmodel = new SearchData();
            var dbCmd = new CommandRunner(ConfigurationManager.ConnectionStrings["chilliDataBase"].ConnectionString.ToString());
            var sqlString = "select * from getsearchdetails('" + sfQuoteId + "')";
            var DBResponse = dbCmd.ExecuteDynamic(sqlString);
            foreach (var searchdata in DBResponse.ToList())
            {
                searchmodel.TTSSQuoteId = searchdata.ttss_quote_id;
                searchmodel.sfQuoteId = searchdata.sf_quote_id;
                searchmodel.sourceName = searchdata.source_name;
                searchmodel.sourceCode = searchdata.source_code;
                searchmodel.destinationName = searchdata.destination_name;
                searchmodel.destinationCode = searchdata.destination_code;
                searchmodel.startDate = DateTime.ParseExact(searchdata.start_date, "d/M/yyyy", new CultureInfo("en-US"));
                searchmodel.endDate = DateTime.ParseExact(searchdata.end_date, "d/M/yyyy", new CultureInfo("en-US"));
                searchmodel.hotelName = searchdata.hotel_name;
                searchmodel.starRating = Convert.ToDecimal(searchdata.star_rating);
                searchmodel.BoardTypeAI = Convert.ToBoolean(searchdata.board_type_ai);
                searchmodel.BoardTypeFB = Convert.ToBoolean(searchdata.board_type_fb);
                searchmodel.BoardTypeHB = Convert.ToBoolean(searchdata.board_type_hb);
                searchmodel.BoardTypeBB = Convert.ToBoolean(searchdata.board_type_bb);
                searchmodel.BoardTypeSC = Convert.ToBoolean(searchdata.board_type_sc);
                searchmodel.BoardTypeRO = Convert.ToBoolean(searchdata.board_type_ro);
                var s = Convert.ToString(searchdata.created_by);
                searchmodel.CreatedBy = Convert.ToInt32(Convert.ToString(searchdata.created_by), 16);
                searchmodel.RoomNo = (int)(searchdata.room_no);
                searchmodel.AdultsCount = (int)(searchdata.adults_count);
                searchmodel.ChildrenCount = (int)(searchdata.children_count);
                searchmodel.InfantsCount = (int)(searchdata.infants_count);
            }
            return searchmodel;
        }
        public async Task<string> CheckEmailExist(string emailId, string memberShipNo)
        {
            string result = "";
            try
            {
                var dbCmd = new CommandRunner(ConfigurationManager.ConnectionStrings["chilliDataBase"].ConnectionString.ToString());
                string sqlString = "select exists(select 1 from customers where email_address='" + emailId + "' and (membership_no !='" + memberShipNo + "')) as email_address";
                var DBresponse = dbCmd.ExecuteDynamic(sqlString);
                foreach (var x in DBresponse)
                {
                    result = Convert.ToString(x.email_address);
                }
            }
            catch (Exception ex)
            {
                result = "f";
                Utilities.ELKLog.Logger.Log("Error", ex.Message.ToString());
            }
            return result;
        }
        public async Task<string> CheckPhoneNumberExist(string phone_number, string memberShipNo)
        {
            string result = "";
            try
            {
                var dbCmd = new CommandRunner(ConfigurationManager.ConnectionStrings["chilliDataBase"].ConnectionString.ToString());
                string sqlString = "select exists(select 1 from customers_phone_numbers where phone_number='" + phone_number + "' and  (customer_id !=  (select customer_id from customers where membership_no ='" + memberShipNo + "' limit 1) or ''='" + memberShipNo + "')) as phone_number";
                var DBresponse = dbCmd.ExecuteDynamic(sqlString);
                foreach (var x in DBresponse)
                {
                    result = Convert.ToString(x.phone_number);
                }
            }
            catch (Exception ex)
            {
                result = "f";
                Utilities.ELKLog.Logger.Log("Error", ex.Message.ToString());
            }
            return result;
        }
        public int SavePassengerDetails(string passengerDetails)
        {
            int status = 0;
            try
            {
                var db = new CommandRunner(ConfigurationManager.ConnectionStrings["chilliDataBase"].ConnectionString.ToString());
                string sql = "select * from SavePassengerDetails('" + passengerDetails + "')";
                var res = db.ExecuteDynamic(sql);
                foreach (var record in res)
                {
                    status = Convert.ToInt32(record.savepassengerdetails);
                }
            }
            catch (Exception ex)
            {
                Utilities.ELKLog.Logger.Log("Error", ex.Message.ToString());
                throw ex;
            }
            return status;
        }
        public int SaveCoBrowseDetails(string cobrowseModel)
        {
            int status = 0;
            try
            {
                var db = new CommandRunner(ConfigurationManager.ConnectionStrings["chilliDataBase"].ConnectionString.ToString());
                string sql = "select * from SaveCobrowsDetails('" + cobrowseModel + "')";
                var res = db.ExecuteDynamic(sql);
                foreach (var record in res)
                {
                    status = Convert.ToInt32(record.savecobrowsdetails);
                }
            }
            catch (Exception ex)
            {
                Utilities.ELKLog.Logger.Log("Error", ex.Message.ToString());
                throw ex;
            }
            return status;
        }
        public List<Passenger> Getpassenger(string quoteHeaderId)
        {
            var passengers = new List<Passenger>();
            Passenger passenger = null;
            try
            {
                var db = new CommandRunner(ConfigurationManager.ConnectionStrings["chilliDataBase"].ConnectionString.ToString());
                string sql = "select * from getpassengers_info_for_quote('" + quoteHeaderId + "')";
                var res = db.ExecuteDynamic(sql);
                foreach (var record in res)
                {
                    passenger = new Passenger();
                    passenger.Address1 = record.address1;
                    passenger.Address2 = record.address2;
                    passenger.Address3 = record.address3;
                    passenger.HouseNumber = record.housenumber;
                    passenger.Title = record.title;
                    passenger.FirstName = record.firstname;
                    passenger.LastName = record.lastname;
                    passenger.DateOfBirth = Convert.ToString(record.dob);
                    passenger.Email = record.email;
                    passenger.Type = record.passengertype;
                    passenger.PassengerTypeOptionId = record.passengertypeoptionid;
                    passenger.Country = record.country;
                    passenger.City = record.city;
                    passenger.PostCode = record.postcode;
                    passenger.Mobile = record.mobile;
                    passenger.Telephone = record.telephone;
                    passenger.IsLeadPassenger = Convert.ToString(record.isleadpassenger);
                    passenger.LapsDays = record.lapsdays != null ? Convert.ToDateTime(record.lapsdays) : null;
                    passenger.StatusOptionId = record.statusoptionid != null ? Convert.ToInt32(record.statusoptionid) : 0;
                    passenger.StatusDescription = record.statusdescription;
                    passenger.PassengerTypeOptionId = Convert.ToInt32(record.passengertypeoptionid);
                    passengers.Add(passenger);
                }
            }
            catch (Exception ex)
            {
                Utilities.ELKLog.Logger.Log("Error", ex.Message.ToString());
                throw ex;
            }
            return passengers;
        }
        public int SavePaymentDetails(List<PaymentDetail> payments)
        {
            int result = 0;
            try
            {
                var jsonInput = JsonConvert.SerializeObject(payments);
                var db = new CommandRunner(ConfigurationManager.ConnectionStrings["chilliDataBase"].ConnectionString.ToString());
                string sql = "select * from save_booking_payment_details('" + jsonInput.ToString() + "') as payment";
                var res = db.ExecuteDynamic(sql);
                foreach (var x in res)
                {
                    result = Convert.ToInt32(x.payment);
                }

            }
            catch (Exception ex)
            {
                Utilities.ELKLog.Logger.Log("Error", ex.Message.ToString());
                throw ex;
            }
            return result;
        }
        public bool SaveQuoteOverrideDetails(QuoteOverride quoteOverride)
        {
            bool result = false;
            try
            {
                var jsonInput = JsonConvert.SerializeObject(quoteOverride);
                var db = new CommandRunner(ConfigurationManager.ConnectionStrings["chilliDataBase"].ConnectionString.ToString());
                string sql = "select * from save_quote_override_details('" + jsonInput.ToString() + "') as quote";
                var res = db.ExecuteDynamic(sql);

                foreach (var x in res)
                {
                    result = Convert.ToBoolean(x.quote);
                }

            }
            catch (Exception ex)
            {
                Utilities.ELKLog.Logger.Log("Error", ex.Message.ToString());
                throw ex;
            }
            return result;
        }
        public bool SaveSesameQuoteForCreateRefreshBook(SesameBasket sesameBasket)
        {
            bool result = false;
            try
            {
                var jsonInput = JsonConvert.SerializeObject(sesameBasket);
                var db = new CommandRunner(ConfigurationManager.ConnectionStrings["chilliDataBase"].ConnectionString.ToString());
                string sql = "select * from save_quote_crb_details('" + jsonInput.ToString() + "') as quote";
                var res = db.ExecuteDynamic(sql);

                foreach (var x in res)
                {
                    result = Convert.ToBoolean(x.quote);
                }
            }
            catch (Exception ex)
            {
                Utilities.ELKLog.Logger.Log("Error", ex.Message.ToString());
                throw ex;
            }
            return result;
        }
        public List<KeyValuePair<string, string>> GetOptionsForValue(int valueId)
        {
            List<KeyValuePair<string, string>> tsOptions = new List<KeyValuePair<string, string>>();
            var db = new CommandRunner(ConfigurationManager.ConnectionStrings["chilliDataBase"].ConnectionString.ToString());
            string sql = "select * from get_tsoptions_for_tsvalueid(" + valueId + ")";
            var res = db.ExecuteDynamic(sql);
            foreach (var record in res)
            {
                tsOptions.Add(new KeyValuePair<string, string>(record.id, record.description));
            }
            return tsOptions;
        }
        public bool SaveTransfersResponseData(TransfersBookData transfersBookData)
        {
            bool result = false;
            try
            {
                var jsonInputTransfersBookData = JsonConvert.SerializeObject(transfersBookData);
                var db = new CommandRunner(ConfigurationManager.ConnectionStrings["chilliDataBase"].ConnectionString.ToString());
                string sql = "select * from save_holidaybooking_transfer_details('" + jsonInputTransfersBookData.Replace("'", "''").ToString() + "' )as holidayTaxis";
                var res = db.ExecuteDynamic(sql);

                foreach (var x in res)
                {
                    result = Convert.ToBoolean(x.holidaytaxis);
                }
            }
            catch (Exception ex)
            {
                Utilities.ELKLog.Logger.Log("Error", ex.Message.ToString());
                throw ex;
            }
            return result;
        }
        public List<PaymentDetail> GetCompletePaymentDetails(int quoteHeaderID)
        {
            List<PaymentDetail> paymentDetails = new List<PaymentDetail>();
            if (quoteHeaderID > 0)
            {
                try
                {
                    var db = new CommandRunner(ConfigurationManager.ConnectionStrings["chilliDataBase"].ConnectionString.ToString());
                    string sql = "select * from get_payment_Details(" + quoteHeaderID + " ) ";
                    var res = db.ExecuteDynamic(sql);
                    foreach (var record in res)
                    {
                        if (record.get_payment_details != null)
                        {
                            paymentDetails = JsonConvert.DeserializeObject<List<PaymentDetail>>(Convert.ToString(record.get_payment_details));
                        }
                    }
                }
                catch (Exception ex)
                {
                    Utilities.ELKLog.Logger.Log("Error", ex.Message.ToString());
                    throw ex;
                }
            }
            return paymentDetails;
        }
        public Quote LoadQuoteData(string QuoteHeaderId)
        {
            Quote quote = new Quote();
            dynamic quoteData = string.Empty;
            var db = new CommandRunner(ConfigurationManager.ConnectionStrings["chilliDataBase"].ConnectionString.ToString());
            string sql = "select * from getallquoteinfo('" + QuoteHeaderId + "')";
            var result = db.ExecuteDynamic(sql);
            foreach (var record in result)
            {
                string data = record.getallquoteinfo;
                quoteData = JsonConvert.DeserializeObject<dynamic>(data);
            }
            var basketHeader = quoteData.basketheader != null ? quoteData.basketheader : string.Empty;
            quote.quoteHeader = LoadbasketHeader(basketHeader);
            var SearchData = quoteData.searchmodel != null ? quoteData.searchmodel : string.Empty;
            quote.searchModel = LoadSearchData(SearchData);
            var flightComponentData = quoteData.flightcomponentdata != null ? quoteData.flightcomponentdata : string.Empty;
            quote.flightComponents = LoadFlights(flightComponentData);
            quote.PopulateFlightComponentUI();
            var hotelComponentDetails = quoteData.hotelcomponentdata != null ? quoteData.hotelcomponentdata : string.Empty;
            quote.hotelComponent = LoadHotelData(hotelComponentDetails);
            var bagComponentDetails = quoteData.bagcomponentdetails != null ? quoteData.bagcomponentdetails : string.Empty;
            quote.bagsComponents = LoadBagsData(bagComponentDetails);
            var transferComponentDetails = quoteData.transfercomponentdetails != null ? quoteData.transfercomponentdetails : string.Empty;
            quote.transferComponents = LoadTranfersData(transferComponentDetails);
            return quote;
        }
        private static QuoteHeader LoadbasketHeader(dynamic basketHeader)
        {
            QuoteHeader quoteHeader = new QuoteHeader();
            if (basketHeader.quoteheaderid != null)
            {
                quoteHeader.QuoteHeadderId = Convert.ToInt32(basketHeader.quoteheaderid);
                quoteHeader.CustomerReferenceCode = basketHeader.customerreferencecode;
                quoteHeader.AgentId = Convert.ToInt32(basketHeader.agentid);
                quoteHeader.CustomerId = Convert.ToInt32(basketHeader.customerid);
                quoteHeader.CustomerTitle = basketHeader.customertitle != null ? basketHeader.customertitle : string.Empty;
                quoteHeader.CustomerFirstName = basketHeader.customerfirstname != null ? basketHeader.customerfirstname : string.Empty;
                quoteHeader.CustomerLastName = basketHeader.customerlastname != null ? basketHeader.customerlastname : string.Empty;
                quoteHeader.CustomerEmailAddress = basketHeader.customeremailaddress != null ? basketHeader.customeremailaddress : string.Empty;
                quoteHeader.MobileNumber = basketHeader.phonenumber != null ? basketHeader.phonenumber : string.Empty;
                quoteHeader.TelephoneNumber = basketHeader.telephonenumber != null ? basketHeader.telephonenumber : string.Empty;
                quoteHeader.CountryCode = basketHeader.countrycode != null ? basketHeader.countrycode : string.Empty;
                quoteHeader.PostCode = basketHeader.postcode != null ? basketHeader.postcode : string.Empty;
                quoteHeader.Address = basketHeader.address + " " + basketHeader.address_line1 + " " + basketHeader.address_line2;
                quoteHeader.City = basketHeader.city != null ? basketHeader.city : string.Empty;
                quoteHeader.Country = basketHeader.country != null ? basketHeader.country : string.Empty;
                quoteHeader.HouseNumber = basketHeader.housenumber != null ? basketHeader.housenumber : string.Empty;
                quoteHeader.quotestatus = basketHeader.quotetype;
                quoteHeader.quoteStatusOptionId = Convert.ToInt32(basketHeader.quotestatusoptionid);
                quoteHeader.HotelCost = basketHeader.hotelcost;
                quoteHeader.BaggageCost = basketHeader.baggagecost;
                quoteHeader.TransfersCost = basketHeader.transferscost;
                quoteHeader.FidelityCost = basketHeader.fidelitycost;
                quoteHeader.TotalCost = basketHeader.totalcost;
                quoteHeader.MarginHotel = basketHeader.marginhotel;
                quoteHeader.MarginFlight = basketHeader.marginflight;
                quoteHeader.MarginExtras = basketHeader.marginextra;
                quoteHeader.GrandTotal = basketHeader.grandtotal;
                quoteHeader.PricePerPerson = basketHeader.priceperperson;
                quoteHeader.InternalNotes = basketHeader.internalnotes != null ? basketHeader.internalnotes : string.Empty;
                quoteHeader.SentToEmail = basketHeader.senttoemail;
                quoteHeader.SentToSms = basketHeader.senttosms;
                quoteHeader.FlightCost = basketHeader.flightcost;
            }
            return quoteHeader;
        }
        private static SearchData LoadSearchData(dynamic searchData)
        {
            SearchData searchModel = new SearchData();
            if (searchData.sourcename != null)
            {
                searchModel.TTSSQuoteId = searchData.ttssquoteid != null ? searchData.ttssquoteid : string.Empty;
                searchModel.sfQuoteId = searchData.customerreferencecode != null ? searchData.customerreferencecode : string.Empty;
                searchModel.sourceName = searchData.sourcename;
                searchModel.sourceCode = searchData.sourcecode;
                searchModel.destinationName = searchData.destinationname;
                searchModel.destinationCode = searchData.destinationcode;
                searchModel.startDate = Convert.ToDateTime(searchData.startdate);
                searchModel.endDate = Convert.ToDateTime(searchData.enddate);
                searchModel.hotelName = searchData.hotelname;
                searchModel.Nights = Convert.ToInt32(searchData.nights != null ? searchData.nights : 0);
                searchModel.starRating = Convert.ToDecimal(searchData.starrating != null ? searchData.starrating : 0);
                searchModel.BoardTypeAI = Convert.ToBoolean(searchData.boardtypeai);
                searchModel.BoardTypeBB = Convert.ToBoolean(searchData.boardtypefb);
                searchModel.BoardTypeFB = Convert.ToBoolean(searchData.boardtypehb);
                searchModel.BoardTypeHB = Convert.ToBoolean(searchData.boardtypebb);
                searchModel.BoardTypeRO = Convert.ToBoolean(searchData.boardtypesc);
                searchModel.BoardTypeSC = Convert.ToBoolean(searchData.boardtypero);
                searchModel.RoomNo = Convert.ToInt32(searchData.roomno != null ? searchData.roomno : 0);
                searchModel.AdultsCount = Convert.ToInt32(searchData.adultscount != null ? searchData.adultscount : 0);
                searchModel.InfantsCount = Convert.ToInt32(searchData.infantscount != null ? searchData.infantscount : 0);
                searchModel.ChildrenCount = Convert.ToInt32(searchData.childrencount != null ? searchData.childrencount : 0);
                string occupancyStr = Convert.ToString(searchData.occupancyrooms);
                searchModel.OccupancyRooms = Newtonsoft.Json.JsonConvert.DeserializeObject<List<Occupancy>>(occupancyStr);
            }
            return searchModel;
        }
        private static List<Businessobjects.Flights.FlightComponent> LoadFlights(dynamic flightcomponentdata)
        {
            List<Businessobjects.Flights.FlightComponent> FlightComponents = new List<Businessobjects.Flights.FlightComponent>();
            List<FlightComponentDetail> flightComponentsDetails = new List<FlightComponentDetail>();
            int count = ((JToken)(flightcomponentdata[0])).Count();
            int index = 0;
            for (int i = 0; i < count; i++)
            {
                if (i == 0 || (i + 1) % 2 != 0)
                {
                    if (((JToken)(flightcomponentdata[0])).ElementAt(i).Children().ElementAt(0).HasValues)
                    {
                        FlightComponents.Add(LoadflightsComponentData(((JToken)(flightcomponentdata[0])).ElementAt(i).Children().ElementAt(0)));
                        int flightDetailsCount = (((JToken)(flightcomponentdata[0])).ElementAt(i + 1).Children().ToArray())[0].ToArray().Count();
                        for (int j = 0; j < flightDetailsCount; j++)
                        {
                            var flightcomponent = (((JToken)(flightcomponentdata[0])).ElementAt(i + 1).Children().ToArray())[0].ToArray().ElementAt(j);
                            FlightComponents.ElementAt(index).FlightComponentDetails.Add(LoadFlightsDetailsData(flightcomponent));
                        }
                        index++;
                    }
                }
            }
            return FlightComponents;
        }
        private static Businessobjects.Flights.FlightComponent LoadflightsComponentData(JToken flightcomponent)
        {
            Businessobjects.Flights.FlightComponent flightComponent = new Businessobjects.Flights.FlightComponent();
            List<FlightComponentDetail> flightComponentsDetails = new List<FlightComponentDetail>();
            flightComponent.Amount = flightcomponent["amount"].ToString();
            flightComponent.CurrencyCode = flightcomponent["currencycode"].ToString();
            flightComponent.Source = flightcomponent["source"].ToString();
            flightComponent.Arrival = flightcomponent["arrival"].ToString();
            flightComponent.FlightQuoteId = flightcomponent["flightquoteid"].ToString();
            flightComponent.componentType = flightcomponent["flighttype"].ToString();
            flightComponent.ComponentTypeOptionId = Convert.ToInt32(flightcomponent["flighttypeoptionid"].ToString());
            flightComponent.SourceUrl= flightcomponent["source_url"].ToString();
            flightComponent.BaseFareCurrencyCode = flightcomponent["base_fare_currency_code"].ToString();
            flightComponent.BaseFareAmount =  !String.IsNullOrEmpty(flightcomponent["base_fare_amount"].ToString())?(decimal?)flightcomponent["base_fare_amount"]:null;
            flightComponent.SourceUrl = flightcomponent["source_url"].ToString();
            flightComponent.FlightComponentDetails = flightComponentsDetails;
            return flightComponent;
        }
        private static FlightComponentDetail LoadFlightsDetailsData(JToken flightDetails)
        {
            FlightComponentDetail flightComponentsDetails = new FlightComponentDetail();
            flightComponentsDetails.DepartureDateTime = Convert.ToDateTime(flightDetails["departuredatetime"].ToString());
            flightComponentsDetails.ArrivalDateTime = Convert.ToDateTime(flightDetails["arrivaldatetime"].ToString());
            flightComponentsDetails.FlightNumber = flightDetails["flightnumber"].ToString();
            flightComponentsDetails.JourneyIndicator = flightDetails["journeyindicator"].ToString();
            flightComponentsDetails.DepartureAirportCode = flightDetails["departureairportcode"].ToString();
            flightComponentsDetails.ArrivalAirportCode = flightDetails["arrivalairportcode"].ToString();
            flightComponentsDetails.MarketingAirLine = flightDetails["marketingairline"].ToString();
            flightComponentsDetails.MarketingAirlineCode = flightDetails["marketingairlinecode"].ToString();
            flightComponentsDetails.Direction = flightDetails["flightdirection"].ToString();
            flightComponentsDetails.DirectionOptionId = Convert.ToInt32(flightDetails["flightdirectionoptionid"].ToString());
            return flightComponentsDetails;

        }
        private static Businessobjects.Hotels.HotelComponent LoadHotelData(dynamic hotelcomponentdetails)
        {
            Businessobjects.Hotels.HotelComponent hotelComponents = new Businessobjects.Hotels.HotelComponent();
            List<HotelComponentDetail> hotelComponentDetails = new List<HotelComponentDetail>();
            var Hotelcomponent = hotelcomponentdetails.hotelcomponent;
            var hotelDetails = hotelcomponentdetails.hotelcomponentdetails;
            hotelComponents.HotelName = Hotelcomponent.hotelname;
            hotelComponents.HotelCode = Hotelcomponent.hotelcode;
            hotelComponents.HotelCodeContext = Hotelcomponent.hotelcodecontext;
            hotelComponents.SupplierResortId = Hotelcomponent.supplierresortid;
            hotelComponents.CheckInDate = Hotelcomponent.checkindate != null ? Convert.ToDateTime(Hotelcomponent.checkindate) : null;
            hotelComponents.StarRating = Convert.ToDecimal(Hotelcomponent.starrating != null ? Hotelcomponent.starrating : 0);
            hotelComponents.TripAdvisorRating = Convert.ToDecimal(Hotelcomponent.tripadvisorrating != null ? Hotelcomponent.tripadvisorrating : 0);
            hotelComponents.CheckOutDate = Hotelcomponent.checkoutdate != null ? Convert.ToDateTime(Hotelcomponent.checkoutdate) : null;
            hotelComponents.TTICode = Convert.ToInt32(Hotelcomponent.tticode != null ? Hotelcomponent.tticode : 0);
            hotelComponents.HotelTypeId = Convert.ToInt32(Hotelcomponent.hoteltypeid != null ? Hotelcomponent.hoteltypeid : 0);
            hotelComponents.HotelQuoteId = Hotelcomponent.hotelquoteid;
            hotelComponents.hotelComponentsDetails = hotelComponentDetails;
            int count = ((JToken)(hotelcomponentdetails.hotelcomponentdetails)).Count();
            for (int i = 0; i < count; i++)
            {
                HotelComponentDetail hotelComponentsDetails = new HotelComponentDetail();
                var hotelcomDetails = ((JToken)(hotelDetails)).ElementAt(i);
                hotelComponentsDetails.AmountAfterTax = Convert.ToDecimal(hotelcomDetails["amountaftertax"].ToString());
                hotelComponentsDetails.RoomTypeCode = hotelcomDetails["roomtypecode"].ToString();
                hotelComponentsDetails.RatePlanCode = hotelcomDetails["rateplancode"].ToString();
                hotelComponentsDetails.RatePlanName = hotelcomDetails["rateplanname"].ToString();
                hotelComponentsDetails.RateQuoteId = hotelcomDetails["ratequoteid"].ToString();
                hotelComponentsDetails.RoomDescription = hotelcomDetails["roomdescription"].ToString();
                hotelComponentsDetails.CancelPolicyIndicator = (!string.IsNullOrEmpty(Convert.ToString(hotelcomDetails["canclepolicyindicator"])) ? Convert.ToBoolean(hotelcomDetails["canclepolicyindicator"]) : false);
                hotelComponentsDetails.NonRefundable = (!string.IsNullOrEmpty(Convert.ToString(hotelcomDetails["nonrefundable"])) ? Convert.ToBoolean(hotelcomDetails["nonrefundable"]) : false);
                hotelComponents.hotelComponentsDetails.Add(hotelComponentsDetails);
            }

            return hotelComponents;
        }
        private static dynamic LoadBagsData(dynamic bagcomponentdetails)
        {
            List<BagComponent> bagsData = new List<BagComponent>();
            if (((JToken)(bagcomponentdetails)).Count() > 0)
            {
                for (int i = 0; i < ((JToken)(bagcomponentdetails)).Count(); i++)
                {
                    BagComponent bag = new BagComponent();
                    var bags = ((JToken)(bagcomponentdetails)).ElementAt(i);
                    bag.BagsType = bags["bagtype"].ToString();
                    bag.BagsDescription = bags["bagdescription"].ToString();
                    bag.BagsCode = bags["bagcode"].ToString();
                    bag.QuantityAvailable = Convert.ToInt32(bags["quantityavailable"].ToString());
                    bag.QuantityRequired = Convert.ToInt32(bags["quantityrequired"].ToString());
                    bag.BuyingPrice = Convert.ToDecimal(bags["buyingprice"].ToString());
                    bag.SellingPrice = Convert.ToDecimal(bags["sellingprice"].ToString());
                    bag.Items = bags["items"]!=null? bags["items"].ToString():string.Empty;
                    bag.Price = Convert.ToDecimal(bags["price"].ToString());
                    bag.Direction = bags["bagdirection"] != null ? bags["bagdirection"].ToString() : string.Empty;
                    bag.DirectionOptionId = bags["bagdirectionoptionid"] != null ? Convert.ToInt32(bags["bagdirectionoptionid"].ToString()) : 0;
                    bag.PassengerIndex = bags["passengerindex"] != null ? Convert.ToInt32(bags["passengerindex"]) : 0;
                    bagsData.Add(bag);
                }
            }
            return bagsData;
        }
        private static dynamic LoadTranfersData(dynamic transfercomponentdetails)
        {
            List<TransferComponent> transfersComponents = new List<TransferComponent>();
            if (((JToken)(transfercomponentdetails)).Count() > 0)
            {
                for (int i = 0; i < ((JToken)(transfercomponentdetails)).Count(); i++)
                {
                    TransferComponent transfersComponent = new TransferComponent();
                    var transfers = ((JToken)(transfercomponentdetails)).ElementAt(i);
                    transfersComponent.ProductId = transfers["productid"].ToString();
                    transfersComponent.BookingTypeId = transfers["bookingtypeid"].ToString();
                    transfersComponent.TransferTime = Convert.ToInt32(transfers["transfertime"].ToString());
                    transfersComponent.ProductTypeId = transfers["producttypeid"].ToString();
                    transfersComponent.ProductType = transfers["producttype"].ToString();
                    transfersComponent.MinPax = Convert.ToInt32(transfers["minpax"].ToString());
                    transfersComponent.MaxPax = Convert.ToInt32(transfers["maxpax"].ToString());
                    transfersComponent.Luggage = Convert.ToInt32(transfers["luggage"].ToString());
                    transfersComponent.productDescription = transfers["productdescription"].ToString();
                    transfersComponent.perPerson = transfers["perperson"].ToString();
                    transfersComponent.oldPrice = Convert.ToDecimal(transfers["oldprice"].ToString());
                    transfersComponent.totalPrice = Convert.ToDecimal(transfers["totalprice"].ToString());
                    transfersComponent.Currency = transfers["currency"].ToString();
                    transfersComponent.PriceType = transfers["pricetype"].ToString() != null ? transfers["pricetype"].ToString() : string.Empty;
                    transfersComponent.TypeCode = transfers["typecode"].ToString();
                    transfersComponent.PriceDescription = transfers["pricedescription"].ToString();
                    transfersComponent.Units = Convert.ToInt32(transfers["units"].ToString());
                    transfersComponent.PricePerPersonOldPrice = Convert.ToDecimal(transfers["priceperpersonoldprice"].ToString());
                    transfersComponent.TransferType = transfers["trasfertype"].ToString();
                    transfersComponent.TransferTypeOptionId = Convert.ToInt32(transfers["trasfertypeoptionid"].ToString());
                    transfersComponents.Add(transfersComponent);
                }
            }
            return transfersComponents;
        }
        public Task<int> SaveCoBrowseConformationResponse(int quoteHeaderId, string guId, string responseMsg)
        {
            int status = 0;
            try
            {
                var db = new CommandRunner(ConfigurationManager.ConnectionStrings["chilliDataBase"].ConnectionString.ToString());
                string sql = "select * from save_Cobrows_Response(" + quoteHeaderId + ",'" + guId + "','" + responseMsg + "')";
                var res = db.ExecuteDynamic(sql);
                foreach (var record in res)
                {
                    status = Convert.ToInt32(record.save_cobrows_response);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return Task.FromResult(status);
        }
        public SesameBasket GetComponentDetails(int quoteHeaderId, string basketId, int typeId)
        {
            SesameBasket sesameBasket = new SesameBasket();
            try
            {
                var db = new CommandRunner(ConfigurationManager.ConnectionStrings["chilliDataBase"].ConnectionString.ToString());
                string sql = "select * from get_componentsDetails(" + quoteHeaderId + ",'" + basketId + "'," + typeId + ")";
                dynamic res = db.ExecuteDynamic(sql);

                foreach (var record in res)
                {
                    sesameBasket.BasketId = record.supplier_basket_id;
                    sesameBasket.CurrencyCode = record.custom_field_1;
                    sesameBasket.QuoteHeaderId = quoteHeaderId;
                    sesameBasket.SourceUrl = record.sourceurl;
                    var component = new SesameComponent();
                    component.ComponentID = Convert.ToString(record.component_id);
                    component.DirectionOptionId = Convert.ToInt32(record.direction_option_id);
                    component.BookingAttempted = record.custom_field_2;
                    component.BookingCancelled = record.custom_field_4;
                    component.BookingSucceeded = record.custom_field_3;
                    component.ProductTypeCode = record.product_type_code;
                    component.StatusCode = record.status_code;
                    component.SupplierBookingId = record.supplier_booking_id;
                    component.SupplierComponentId = record.supplier_component_id;
                    component.SupplierCode = record.status_code;
                    component.Type = record.sesame_basket_type_option_id;
                    component.SourceUrl = record.sourceurl;
                    if (!string.IsNullOrEmpty(record.component_detail_info))
                        component.ComponentDetails = JsonConvert.DeserializeObject<List<SesameDetail>>(Convert.ToString(record.component_detail_info));

                    if (!string.IsNullOrEmpty(record.component_option_info))
                        component.ComponentOptions = JsonConvert.DeserializeObject<List<SesameOption>>(Convert.ToString(record.component_option_info));

                    if (!string.IsNullOrEmpty(record.flights_details))
                    {
                        List<SesameFlightInformation> flightDetails = new List<SesameFlightInformation>();
                        var flightInformation = JsonConvert.DeserializeObject(Convert.ToString(record.flights_details));

                        if (flightInformation != null)
                        {
                            foreach (var segment in flightInformation)
                            {
                                var flight = JsonConvert.DeserializeObject<SesameFlightInformation>(Convert.ToString(segment));
                                if (segment.flight_legs != null)
                                {
                                    flight.Legs = JsonConvert.DeserializeObject<List<SesameLeg>>(Convert.ToString(segment.flight_legs));
                                }

                                flightDetails.Add(flight);
                            }
                        }

                        component.ComponentFlightInformation = flightDetails;
                    }
                    if (!string.IsNullOrEmpty(record.accommodation_details))
                        component.ComponentHotelInformation = JsonConvert.DeserializeObject<List<SesameHotelInformation>>(Convert.ToString(record.accommodation_details));
                    if (!string.IsNullOrEmpty(record.accommodation_details))
                        component.ComponentHotelInformation = JsonConvert.DeserializeObject<List<SesameHotelInformation>>(Convert.ToString(record.accommodation_details));
                    if (!string.IsNullOrEmpty(record.componenterrors))
                    {
                        var result = Convert.ToString(record.componenterrors);
                        foreach (var item in result.Split('|'))
                        {
                            component.ComponentErrors.Add(item);
                        }
                    }
                    if (!string.IsNullOrEmpty(record.componentvcndetials))
                        component.ComponentVCNDetials = JsonConvert.DeserializeObject<List<SesameVCN>>(Convert.ToString(record.componentvcndetials));
                    if (!string.IsNullOrEmpty(record.component_messages))
                    {
                        var compnentMessages = Convert.ToString(record.component_messages);
                        foreach (var componentMessage in compnentMessages.Split('|'))
                        {
                            component.ComponentMessages.Add(componentMessage);
                        }
                    }
                    sesameBasket.BasketComponents.Add(component);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return sesameBasket;
        }
        public TransfersBookData GetHolidayTaxisData(int quoteHeaderId)
        {
            TransfersBookData transfersbookdata = new TransfersBookData();
            try
            {
                var db = new CommandRunner(ConfigurationManager.ConnectionStrings["chilliDataBase"].ConnectionString.ToString());
                string sql = "select * from get_holidaytaxidetails(" + quoteHeaderId + " ) as holidayTaxis";
                var res = db.ExecuteDynamic(sql);

                foreach (var record in res)
                {
                    TransfersBookGeneral general = new TransfersBookGeneral();
                    general.affiliateCode = record.affiliate_code;
                    general.agency = record.agency;
                    general.clerk = record.clerk;
                    general.bookingReference = record.booking_reference;
                    general.bookingStatusId = Convert.ToString(record.booking_status_id);
                    general.orderReference = record.order_reference;
                    general.noOfTransfers = Convert.ToString(record.no_of_transfers);
                    general.paymentType = record.payment_type;
                    general.dateEntered = Convert.ToDateTime(record.date_entered);
                    general.pdf = record.pdf;
                    general.html = record.html;
                    transfersbookdata.general = general;

                    TransfersBookContact contact = new TransfersBookContact();
                    contact.title = record.title;
                    contact.firstName = record.first_name;
                    contact.lastName = record.last_name;
                    contact.email = record.email;
                    contact.telephone = record.telephonenum;
                    transfersbookdata.contact = contact;

                    TransfersBookPricing pricing = new TransfersBookPricing();
                    pricing.netPrice = Convert.ToString(record.net_prices);
                    pricing.totalPrice = Convert.ToString(record.total_prices);
                    pricing.transactionfee = Convert.ToString(record.transcation_fee);
                    pricing.carbonoffset = Convert.ToString(record.carbon_offsets);
                    pricing.optionalExtras = Convert.ToString(record.optionals_extras);
                    pricing.creditAmount = Convert.ToString(record.credit_amout);
                    pricing.salePrice = Convert.ToString(record.sale_prices);
                    pricing.currency = record.currencys;
                    transfersbookdata.pricing = pricing;

                    if (!string.IsNullOrEmpty(record.transferbookingdetails))
                        transfersbookdata.transferbookdata = JsonConvert.DeserializeObject<List<TransferBook>>(Convert.ToString(record.transferbookingdetails));
                }
            }
            catch (Exception ex)
            {
                Utilities.ELKLog.Logger.Log("Error", ex.Message.ToString());
                throw ex;
            }
            return transfersbookdata;
        }

        public List<AgentQuote> GetAgentQuotes(string quoteRef, string customerName,string fromDate, string toDate,int agentid,bool isLast24Hours)
        {
            var agentQuotes = new List<AgentQuote>();
            AgentQuote agentQuote = null;
            var db = new CommandRunner(ConfigurationManager.ConnectionStrings["chilliDataBase"].ConnectionString.ToString());
            string sql = "select * from get_agent_quotes('" + quoteRef + "','"+ customerName +"','"+ fromDate + "','"+ toDate +"',"+agentid+","+ isLast24Hours+") as agentQuotes";
            var res = db.ExecuteDynamic(sql);
            foreach (var item in res)
            {
                agentQuote = new AgentQuote();
                agentQuote.QuoteHeaderID = Convert.ToInt32(item.quote_header_id);
                agentQuote.CustomerReferenceCode = item.customer_reference_code;
                agentQuote.QuotedOn = item.quotedon;
                agentQuote.CustomerFirstName = item.first_name;
                agentQuote.CustomerLastName = item.last_name;
                agentQuote.SourceCode = item.source_code;
                agentQuote.DestinationCode = item.destination_code;
                agentQuote.FromtDate = item.start_date;
                agentQuote.ToDate = item.end_date;
                agentQuote.HotelName = item.hotel_name;
                agentQuote.AdultsCount =  item.adults_count;
                agentQuote.ChildrenCount =  item.children_count;
                agentQuote.InfantsCount =  item.infants_count;
                agentQuote.GrandTotal = Convert.ToDouble(item.grand_total);
                agentQuote.PricePerPerson = Convert.ToDouble(item.price_per_person);
                agentQuote.NumberOfNights = agentQuote.ToDate.Subtract(agentQuote.FromtDate).Days;
                agentQuotes.Add(agentQuote);
            }
            return agentQuotes;
        }


        public bool UpdateAgentNotes(string quoteHeaderID, string agentNotes)
        {
            bool isAgentNotesUpdated = false;
            try {
                var db = new CommandRunner(ConfigurationManager.ConnectionStrings["chilliDataBase"].ConnectionString.ToString());
                string sql = "select * from update_agentnotes('" + quoteHeaderID + "','" + agentNotes + "')";
                var res = db.ExecuteDynamic(sql);
                foreach(var item in res)
                {
                    isAgentNotesUpdated = item.update_agentnotes;
                }

            }
            catch(Exception ex)
            {

            }
            return isAgentNotesUpdated;
        }
    }
}
