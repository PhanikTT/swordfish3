﻿using Swordfish.Businessobjects.Common;
using Swordfish.Businessobjects.Customers;
using Swordfish.Businessobjects.Hotels;
using Swordfish.Businessobjects.Passengers;
using Swordfish.Businessobjects.Payments;
using Swordfish.Businessobjects.Quotes;
using Swordfish.Businessobjects.Sesame;
using Swordfish.Businessobjects.TopDog;
using Swordfish.Businessobjects.Transfers;
using Swordfish.Components.BookingServices.BusinessManagers;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Swordfish.Components.BookingServices
{
    public class BookingService:IBookingService
    {
        BookingManager bookingManager = null;
        public BookingService()
        {
            bookingManager = new BookingManager();
        }
        public string SaveBasket(Quote basket)
        {
            var result = bookingManager.SaveBasket(basket);
            return result.ToString();
        }
        public bool SaveTopDogResponse(List<TopDogResponse> topDogResponse, int quoteheaderId, int userid)
        {
            return bookingManager.SaveTopDogResponse(topDogResponse, quoteheaderId, userid);
        }
        public List<ExtraCharge> GetExtraCharges()
        {
            return bookingManager.GetExtraCharges();
        }
        public async Task<ConcurrentBag<DestinationHotels>> GetHotelList(string HotelSupplierCode, string DestinationCode)
        {
            ConcurrentBag<DestinationHotels> hotelList = new ConcurrentBag<DestinationHotels>();
            try
            {
                var s = await bookingManager.GetHotelList(HotelSupplierCode, DestinationCode).ConfigureAwait(false);
                hotelList = new ConcurrentBag<DestinationHotels>(s);
            }
            catch (Exception ex)
            {
                var log = Utilities.ELKLog.Logger.Log("Error", ex.Message);
            }
            return hotelList;
        }
        public async Task<string> SaveCoBrowsDetails(string QuoteId, string ClientId, string AgentId, string Url_Id, Passenger Leadpassenger, List<CoPassengers> CoPassengers, string LeadEmail, string LeadMob)
        {
            return await bookingManager.SaveCoBrowsDetails(QuoteId, ClientId, AgentId, Url_Id, Leadpassenger, CoPassengers, LeadEmail, LeadMob).ConfigureAwait(false);
        }
        public async Task<List<CoBrows>> GetCoBrowsDetails(string Brows_Key)
        {
            return await bookingManager.GetCoBrowsDetails(Brows_Key).ConfigureAwait(false);
        }
        public async Task<bool> COBrowsDataStatus(string CoBrowID, string confm)
        {
            return await bookingManager.COBrowsDataStatus(CoBrowID, confm).ConfigureAwait(false);
        }
        public async Task<ConcurrentBag<Resort>> GetResorts(string SupplierCode)
        {
            ConcurrentBag<Resort> resortList = new ConcurrentBag<Resort>();
            try
            {
                var resorts = await bookingManager.GetResorts(SupplierCode).ConfigureAwait(false);
                resortList = new ConcurrentBag<Resort>(resorts);
            }
            catch (Exception ex)
            {
                Utilities.ELKLog.Logger.Log("Error", ex.Message);
                throw ex;
            }
            return resortList;
        }
        public async Task<string> SaveCustomerDetails(Customer customer)
        {
            return await bookingManager.SaveCustomerDetails(customer).ConfigureAwait(false);
        }
        public async Task<List<Customer>> SearchCustomerDetails(Customer customer)
        {
            return await bookingManager.SearchCustomerDetails(customer).ConfigureAwait(false);
        }
        public async Task<List<Customer>> GetCustomerDatabyMembershipNo(Customer customer)
        {
            return await bookingManager.GetCustomerDatabyMembershipNo(customer).ConfigureAwait(false);
        }
        public async Task<string> UpdateCustomerDetails(Customer customer)
        {
            return await bookingManager.UpdateCustomerDetails(customer).ConfigureAwait(false);
        }
        public async Task<ConcurrentBag<CustomerQuotes>> GetCustomerQuotes(int customerId)
        {
            return await bookingManager.GetCustomerQuotes(customerId).ConfigureAwait(false);
        }
        public async Task<int> SaveAgentJournal(int AgentId, string QuoteRef, string JournalType, string JournalNotes)
        {
            return await bookingManager.SaveAgentJournal(AgentId, QuoteRef, JournalType, JournalNotes).ConfigureAwait(false);
        }
        public SearchData getsearchModel(string sfquoteId)
        {
            return bookingManager.getsearchModel(sfquoteId);
        }
        public async Task<string> CheckEmailExist(string emailId, string memberShipNo)
        {
            return await bookingManager.CheckEmailExist(emailId, memberShipNo).ConfigureAwait(false);
        }
        public async Task<string> CheckPhoneNumberExist(string phone_number, string memberShipNo)
        {
            return await bookingManager.CheckPhoneNumberExist(phone_number, memberShipNo).ConfigureAwait(false);
        }
        public int SavePassengerDetails(string passengerDetails)
        {
            return bookingManager.SavePassengerDetails(passengerDetails);
        }
        public async Task<int> SaveCoBrowseDetails(string cobrowsModel)
        {
            return await Task.Run(() => bookingManager.SaveCoBrowseDetails(cobrowsModel)).ConfigureAwait(false);
        }
        public List<Passenger> Getpassenger(string quoteHeaderId)
        {
            return bookingManager.Getpassenger(quoteHeaderId);
        }
        public int SavePaymentDetails(List<PaymentDetail> payments)
        {
            return bookingManager.SavePaymentDetails(payments);
        }
        public bool SaveQuoteOverrideDetails(QuoteOverride quoteOverride)
        {
            return bookingManager.SaveQuoteOverrideDetails(quoteOverride);
        }
        public bool SaveSesameQuoteForCreateRefreshBook(SesameBasket sesameBasket)
        {
            return bookingManager.SaveSesameQuoteForCreateRefreshBook(sesameBasket);
        }
        public List<KeyValuePair<string, string>> GetOptionsForValue(int valueID)
        {
            return bookingManager.GetOptionsForValue(valueID);
        }
        public bool SaveTransfersResponseData(TransfersBookData transfersBookData)
        {
            return bookingManager.SaveTransfersResponseData(transfersBookData);
        }
        public List<PaymentDetail> GetCompletePaymentDetails(int quoteHeaderID)
        {
            return bookingManager.GetCompletePaymentDetails(quoteHeaderID);
        }
        public Quote loadQuoteData(string QuoteHeaderId)
        {
            var quoteData = new Quote();
            quoteData = bookingManager.LoadQuoteData(QuoteHeaderId);
            return quoteData;
        }
        public Task<int> SaveCoBrowseConformationResponse(int quoteHeaderId, string guId, string responseMsg)
        {
            return bookingManager.SaveCoBrowseConformationResponse(quoteHeaderId, guId, responseMsg);
        }
        public SesameBasket GetComponentDetails(int quoteHeaderId, string basketId, int typeId)
        {
            return bookingManager.GetComponentDetails(quoteHeaderId, basketId, typeId);
        }
        public TransfersBookData GetHolidayTaxisData(int quoteHeaderId)
        {
            return bookingManager.GetHolidayTaxisData(quoteHeaderId);
        }
        public List<AgentQuote> GetAgentQuotes(string quoteRef, string customerName, string fromDate, string toDate, int agentid, bool isLast24Hours)
        {
            return bookingManager.GetAgentQuotes(quoteRef, customerName, fromDate, toDate, agentid, isLast24Hours);
        }
        public bool UpdateAgentNotes(string quoteHeaderID, string agentNotes)
        {
            return bookingManager.UpdateAgentNotes(quoteHeaderID, agentNotes);
        }
    }
}
