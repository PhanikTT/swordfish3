﻿using Swordfish.Businessobjects.Common;
using Swordfish.Businessobjects.Customers;
using Swordfish.Businessobjects.Hotels;
using Swordfish.Businessobjects.Passengers;
using Swordfish.Businessobjects.Payments;
using Swordfish.Businessobjects.Quotes;
using Swordfish.Businessobjects.Sesame;
using Swordfish.Businessobjects.TopDog;
using Swordfish.Businessobjects.Transfers;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Swordfish.Components.BookingServices
{
    public interface IBookingService
    {
        //Task<ConcurrentBag<Flight>> GetFlights(FlightRequest flightRequest);
        //Task<ConcurrentBag<Hotel>> GetHotels(HotelRequest hotelRequest);
        //Task<PackageData> GetPackage(FlightRequest flightRequest, HotelRequest hotelRequest);
        string SaveBasket(Quote basket);
        //Task<QuoteRef> GetCreateBookInformation(BookRequest bookRequest);
        //Task<bool> SaveRevision(BookingRevision bookingRevision);
        //void SaveTopDogStatus(string Quote, string Status, string TD_Ref_No, string Error_dtls);
        bool SaveTopDogResponse(List<TopDogResponse> topDogResponse, int quoteheaderId, int userid);
        //dynamic getTopDogResponse(int QuoteHeaderId, int VerbOPtionId);
        List<ExtraCharge> GetExtraCharges();
        //Task<bool> SaveBookingDetails(Booking booking, Basket ChilliBasket, PaymentDetails PaymentDtl);
        Task<ConcurrentBag<DestinationHotels>> GetHotelList(string HotelSupplierCode, string DestinationCode);
        //Task<HotelInfo> GetHotelDetails(HotelDetailRequest hotelDetailRequest);
        //Task<int> DeleteQuoteReference(string QuoteRefNumber);
        //Task<QuoteRef> GetCreateQuoteInformation(BasketRequest quoteRequest);
        //Task<QuoteRef> GetQuoteRefresh(QuoteRefreshRequest basketRefresh);
        Task<string> SaveCoBrowsDetails(string QuoteId, string ClientId, string AgentId, string Url_Id, Passenger Leadpassenger, List<CoPassengers> CoPassengers, string LeadEmail, string LeadMob);
        Task<List<CoBrows>> GetCoBrowsDetails(string Brows_Key);
        Task<bool> COBrowsDataStatus(string CoBrowID, string confm);
        Task<ConcurrentBag<Resort>> GetResorts(string SupplierCode);
        //Task<TopDogInfo> GetTopDogStatus(string QuoteID);
        //Task<int> GetQuoteCount(int CustomerID);
        //Task<List<QuoteDetails>> GetQuotes(int agentId, int customerId, DateTime? fromDate, DateTime? toDate, string searchValue);
        Task<string> SaveCustomerDetails(Customer customer);
        Task<List<Customer>> SearchCustomerDetails(Customer customer);
        Task<List<Customer>> GetCustomerDatabyMembershipNo(Customer customer);
        Task<string> UpdateCustomerDetails(Customer customer);
        Task<ConcurrentBag<CustomerQuotes>> GetCustomerQuotes(int customerId);
        Task<int> SaveAgentJournal(int AgentId, string QuoteRef, string JournalType, string JournalNotes);
        SearchData getsearchModel(string sfquoteId);
        Task<string> CheckEmailExist(string emailId, string memberShipNo);
        Task<string> CheckPhoneNumberExist(string phone_number, string memberShipNo);
        int SavePassengerDetails(string PassengersModel);
        Task<int> SaveCoBrowseDetails(string cobrowsModel);
        List<Passenger> Getpassenger(string quoteHeaderId);
        int SavePaymentDetails(List<PaymentDetail> payments);
        bool SaveQuoteOverrideDetails(QuoteOverride quoteOverride);
        bool SaveSesameQuoteForCreateRefreshBook(SesameBasket sesameBasket);
        List<KeyValuePair<string, string>> GetOptionsForValue(int valueID);
        bool SaveTransfersResponseData(TransfersBookData transfersBookData);
        List<PaymentDetail> GetCompletePaymentDetails(int quoteHeaderID);
        Quote loadQuoteData(string QuoteHeaderId);
        Task<int> SaveCoBrowseConformationResponse(int quoteHeaderId, string guId, string responseMsg);
        SesameBasket GetComponentDetails(int quoteHeaderId, string basketId, int typeId);
        TransfersBookData GetHolidayTaxisData(int quoteHeaderId);
        List<AgentQuote> GetAgentQuotes(string quoteRef, string customerName, string fromDate, string toDate, int agentid, bool isLast24Hours);
        bool UpdateAgentNotes(string quoteHeaderID, string agentNotes);
        
    }
}
