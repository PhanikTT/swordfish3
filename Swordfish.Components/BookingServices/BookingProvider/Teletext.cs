﻿using Swordfish.Businessobjects.Hotels;
using Swordfish.Utilities.Cache;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Web.Script.Serialization;
using System.Xml;
using System.Web;
using System.Data;
using System.Linq;
using System.Collections.Concurrent;

namespace Swordfish.Components.BookingServices.BookingProvider
{
    public class Teletext
    {
        public async Task<List<DestinationHotels>> GetHotelList(string HotelSupplierCode, string DestinationCode)
        {
            var hotelsList = new List<DestinationHotels>();
            try
            {
                hotelsList = await Task.Run(() => GetHotelsAtDestination(HotelSupplierCode, DestinationCode));
            }
            catch (Exception ex)
            {
                var log = Utilities.ELKLog.Logger.Log("Error", ex.Message.ToString());
            }
            return hotelsList;
        }
        private async static Task<List<DestinationHotels>> GetHotelsAtDestination(string HotelSupplierCode, string DestinationCode)
        {
            string hotelDestinationsCacheKey = GetHoteDestinationCacheKey(HotelSupplierCode, DestinationCode);
            var getDataFromCache = DeserializeHotelDestination(hotelDestinationsCacheKey);
            if ((getDataFromCache == null || getDataFromCache.Result == null || getDataFromCache.Result.Count == 0))
            {
                string requestHotelXML = "<SSM_HotelListRQ xmlns=" + "\"http://www.cwtdigital.com/Sesame/2015\"" + "> <Resort SupplierCode =\"" + HotelSupplierCode + "\"" + " SupplierResortId =\"" + DestinationCode + "\" /> </SSM_HotelListRQ > ";

                string response = await Task.Run(() => RequestSesame(requestHotelXML));
                var hotelsSestinations = await Task.Run(() => GetHotelsListDestination(response));
                SetHotelDestination(hotelDestinationsCacheKey, hotelsSestinations);
                return hotelsSestinations;
            }

            return getDataFromCache.Result;
        }
        private static string GetHoteDestinationCacheKey(string hotelSupplierCode, string destinationCode)
        {
            return hotelSupplierCode + "_" + destinationCode;
        }
        private static Task<List<DestinationHotels>> DeserializeHotelDestination(string hotelDetailCacheKey)
        {
            List<DestinationHotels> cacheHotelInfo = null;

            try
            {
                var hotelCacheInfo = CacheRepository.GetData(hotelDetailCacheKey);

                if (hotelCacheInfo != null)
                {
                    var serialize = new JavaScriptSerializer();
                    
                    var data = serialize.Deserialize<List<DestinationHotels>>(hotelCacheInfo);
                    cacheHotelInfo = data;
                }
            }
            catch (Exception ex)
            {
                var log = Utilities.ELKLog.Logger.Log("Error", ex.Message.ToString());
            }

            return Task.FromResult(cacheHotelInfo);
        }
        private static string RequestSesame(string requestXml, string url = null)
        {
            string outputResult = string.Empty;
            string destinationUrl = ConfigurationManager.AppSettings["Sesame.DestinationURL"] == null ? "http://sesame.francisco.uat.cwtdigital.com" : ConfigurationManager.AppSettings["Sesame.DestinationURL"].ToString();

            try
            {
                HttpWebRequest request = HttpWebRequest.Create(destinationUrl) as HttpWebRequest;
                request.Method = "POST";
                request.ContentType = "application/x-www-form-urlencoded; charset=UTF-8";

                Encoding e = new UTF8Encoding(false);
                XmlDocument doc = new XmlDocument();
                doc.LoadXml(requestXml);
                string rawXml = doc.OuterXml;
                string encodedXML = HttpUtility.UrlEncode(rawXml, e);
                string requestText = string.Format("request={0}", encodedXML);

                Stream requestStream = request.GetRequestStream();
                StreamWriter requestWriter = new StreamWriter(requestStream, e);
                requestWriter.Write(requestText);
                requestWriter.Close();

                HttpWebResponse response;
                response = (HttpWebResponse)request.GetResponse();

                Dictionary<string, string> _dictionary = new Dictionary<string, string>();
                Stream responseStream = response.GetResponseStream();
                var responseStr = new StreamReader(responseStream).ReadToEnd();
                outputResult = responseStr.ToString();
                if (response.StatusCode == HttpStatusCode.OK)
                {
                    outputResult = responseStr.ToString();
                }
            }
            catch (WebException ex)
            {
                var resp = new StreamReader(ex.Response.GetResponseStream()).ReadToEnd();
                System.Xml.XmlDocument doc = new System.Xml.XmlDocument();
                doc.LoadXml(resp.ToString());

                // Root element
                System.Xml.XmlElement root = doc.DocumentElement;

                System.Xml.XmlElement conditie = (System.Xml.XmlElement)root.ChildNodes[0];

                string Message = root.ChildNodes[0].InnerText;
                string SessionId = root.ChildNodes[1].InnerText;
                string Backtrace = root.ChildNodes[2].InnerText;
                var log = Utilities.ELKLog.Logger.Log("Error occured at Teletext.RequestSesame() for requestXml=" + requestXml + " ;<br/> and URL=" + url, ";<br/> Response XML= " + resp + "; StackTrace=" + ex.StackTrace + "; <br/>InnerException=" + ex.InnerException + ";<br/>Error Message" + ex.Message.ToString());
                throw ex;
            }
            catch (Exception ex)
            {
                var log = Utilities.ELKLog.Logger.Log("Error occured at Teletext.RequestSesame() for requestXml=" + requestXml + " ;<br/> and URL=" + url, ";<br/>  StackTrace=" + ex.StackTrace + "; <br/>InnerException=" + ex.InnerException == null ? "" : ex.InnerException + ";<br/>Error Message" + ex.Message.ToString());
                throw ex;
            }

            return outputResult;
        }
        public static List<DestinationHotels> GetHotelsListDestination(string responseStr)
        {
            List<DestinationHotels> lstHotelInfo = new List<DestinationHotels>();
            XmlDocument xmlDoc = new XmlDocument();
            xmlDoc.LoadXml(responseStr.ToString());
            XmlReader xmlReader = new XmlNodeReader(xmlDoc);
            DataSet xmlDS = new DataSet();
            xmlDS.ReadXml(xmlReader);

            if (xmlDS.Tables.Count > 0)
            {
                lstHotelInfo = (from tab1 in xmlDS.Tables["Hotel"].AsEnumerable()
                                select new DestinationHotels
                                {
                                    Name = Convert.ToString(tab1["Name"]),
                                    SupplierAccommId = Convert.ToString(tab1["SupplierAccommId"]),
                                    Rating = Convert.ToString(tab1["Rating"]),
                                    SupplierCode = Convert.ToString(tab1["SupplierCode"]),
                                    SupplierResortId = Convert.ToString(tab1["SupplierResortId"]),
                                    TTICode = Convert.ToString(tab1["TTICode"])
                                }).ToList<DestinationHotels>();
            }
            return new List<DestinationHotels>(lstHotelInfo);
        }
        private static void SetHotelDestination(string hotelCacheKey, List<DestinationHotels> hotels)
        {
            try
            {
                if (CacheRepository.CheckKeyExists(hotelCacheKey) == false)
                {
                    var serializer = new JavaScriptSerializer();
                    var serializedResult = serializer.Serialize(hotels);
                    CacheRepository.SetData(hotelCacheKey, serializedResult.ToString());
                }
            }
            catch (Exception ex)
            {
                var log = Utilities.ELKLog.Logger.Log("Error", ex.Message.ToString());
            }
        }
        public async Task<List<Resort>> GetResorts(string SupplierCode)
        {
            var resortList = new List<Resort>();
            try
            {
                resortList = await Task.Run(() => GetResortDetails(SupplierCode));
            }
            catch (Exception ex)
            {
                var log = Utilities.ELKLog.Logger.Log("Error", ex.Message.ToString());
            }
            return resortList;
        }
        private async static Task<List<Resort>> GetResortDetails(string SupplierCode)
        {
            string resortsCacheKey = GetResortCacheKey(SupplierCode);
            var getDataFromCache = DeserializeResorts(resortsCacheKey);
            if ((getDataFromCache == null || getDataFromCache.Result == null || getDataFromCache.Result.Count == 0))
            {
                string requestResortXML = "<SSM_ResortTreeRQ xmlns=" + "\"http://www.cwtdigital.com/Sesame/2015\"" + ">" + "<SupplierCode>" + SupplierCode + "</SupplierCode></SSM_ResortTreeRQ>";

                string response = await Task.Run(() => RequestSesame(requestResortXML));
                var resorts = await Task.Run(() => GetResortList(response));
                SetResort(resortsCacheKey, resorts);
                return resorts;
            }
            return getDataFromCache.Result;
        }
        private static string GetResortCacheKey(string SupplierCode)
        {
            return SupplierCode;
        }
        private static Task<List<Resort>> DeserializeResorts(string resortCacheKey)
        {
            List<Resort> resorts = null;
            try
            {
                var resortCache = CacheRepository.GetData(resortCacheKey);
                if (resortCache != null)
                {
                    var serialize = new JavaScriptSerializer();

                    var data = serialize.Deserialize<List<Resort>>(resortCache);
                    resorts = data;
                }
            }
            catch (Exception ex)
            {
                var log = Utilities.ELKLog.Logger.Log("Error", ex.Message.ToString());
            }
            return Task.FromResult(resorts);
        }
        public static List<Resort> GetResortList(string responseStr)
        {
            List<Resort> lstResort = new List<Resort>();
            XmlDocument xmlDoc = new XmlDocument();
            xmlDoc.LoadXml(responseStr.ToString());

            XmlReader xmlReader = new XmlNodeReader(xmlDoc);
            DataSet xmlDS = new DataSet();

            xmlDS.ReadXml(xmlReader);
            lstResort = (from tab1 in xmlDS.Tables["Resort"].AsEnumerable()
                         select new Resort
                         {
                             ResortId = Convert.ToInt32(tab1["Resort_Id"]),
                             Name = Convert.ToString(tab1["Name"]),
                             AirportCodes = Convert.ToString(tab1["AirportCodes"]),
                             SupplierResortId = Convert.ToString(tab1["SupplierResortId"]),
                             SubResortId = Convert.ToString(tab1["Resort_Id_0"])
                         }).ToList();
            return new List<Resort>(lstResort);

        }
        private static void SetResort(string resortsCacheKey, List<Resort> resorts)
        {
            try
            {
                if (CacheRepository.CheckKeyExists(resortsCacheKey) == false)
                {
                    var serializer = new JavaScriptSerializer();
                    var serializedResult = serializer.Serialize(resorts);

                    CacheRepository.SetData(resortsCacheKey, serializedResult.ToString());
                }
            }
            catch (Exception ex)
            {
                var log = Utilities.ELKLog.Logger.Log("Error", ex.Message.ToString());
            }
        }
    }
}
