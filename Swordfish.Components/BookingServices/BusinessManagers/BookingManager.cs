﻿using Swordfish.Businessobjects.Common;
using Swordfish.Businessobjects.Customers;
using Swordfish.Businessobjects.Hotels;
using Swordfish.Businessobjects.Passengers;
using Swordfish.Businessobjects.Payments;
using Swordfish.Businessobjects.Quotes;
using Swordfish.Businessobjects.Sesame;
using Swordfish.Businessobjects.TopDog;
using Swordfish.Businessobjects.Transfers;
using Swordfish.Components.BookingServices.BookingProvider;
using Swordfish.Components.BookingServices.DataAccess;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Swordfish.Components.BookingServices.BusinessManagers
{
    public class BookingManager
    {
        BookingServiceDL bookingServiceDL = null;
        public BookingManager()
        {
            bookingServiceDL = new BookingServiceDL();
        }
        public string SaveBasket(Quote basket)
        {
            return bookingServiceDL.SaveBasket(basket);
        }
        public bool SaveTopDogResponse(List<TopDogResponse> topDogResponse, int quoteheaderId, int userid)
        {
            return bookingServiceDL.SaveTopDogResponse(topDogResponse, quoteheaderId, userid);
        }
        public List<ExtraCharge> GetExtraCharges()
        {
            return bookingServiceDL.GetExtraCharges();
        }
        public async Task<List<DestinationHotels>> GetHotelList(string HotelSupplierCode, string DestinationCode)
        {
            Teletext multicom = new Teletext();
            return await multicom.GetHotelList(HotelSupplierCode, DestinationCode);
        }
        public async Task<string> SaveCoBrowsDetails(string QuoteId, string ClientId, string AgentId, string Url_Id, Passenger Leadpassenger, List<CoPassengers> CoPassengers, string LeadEmail, string LeadMob)
        {
            return await bookingServiceDL.SaveCoBrowsDetails(QuoteId, ClientId, AgentId, Url_Id, Leadpassenger, CoPassengers, LeadEmail, LeadMob).ConfigureAwait(false);
        }
        public async Task<List<CoBrows>> GetCoBrowsDetails(string Brows_Key)
        {
            return await bookingServiceDL.GetCoBrowsDetails(Brows_Key).ConfigureAwait(false);
        }
        public async Task<bool> COBrowsDataStatus(string CoBrowID, string confm)
        {
            return await bookingServiceDL.COBrowsDataStatus(CoBrowID, confm).ConfigureAwait(false);
        }
        public async Task<List<Resort>> GetResorts(string SupplierCode)
        {
            Teletext multicom = new Teletext();
            return await multicom.GetResorts(SupplierCode);
        }
        public async Task<string> SaveCustomerDetails(Customer customer)
        {
            return await bookingServiceDL.SaveCustomerDetails(customer).ConfigureAwait(false);
        }
        public async Task<List<Customer>> SearchCustomerDetails(Customer customer)
        {
            return await bookingServiceDL.SearchCustomerDetails(customer).ConfigureAwait(false);
        }
        public async Task<List<Customer>> GetCustomerDatabyMembershipNo(Customer customer)
        {
            return await bookingServiceDL.GetCustomerDatabyMembershipNo(customer).ConfigureAwait(false);
        }
        public async Task<string> UpdateCustomerDetails(Customer customer)
        {
            return await bookingServiceDL.UpdateCustomerDetails(customer).ConfigureAwait(false);
        }
        public async Task<ConcurrentBag<CustomerQuotes>> GetCustomerQuotes(int customerId)
        {
            var cutomerQuotes = Task.Run(() => bookingServiceDL.getCustomerQuotes(customerId)).Result;
            return cutomerQuotes;
        }
        public async Task<int> SaveAgentJournal(int AgentId, string QuoteRef, string JournalType, string JournalNotes)
        {
            return await Task.Run(() => bookingServiceDL.SaveAgentJournal(AgentId, QuoteRef, JournalType, JournalNotes)).ConfigureAwait(false);
        }
        public SearchData getsearchModel(string sfQuoteId)
        {
            var searchdata = bookingServiceDL.getsearchModel(sfQuoteId);
            return searchdata;
        }
        public async Task<string> CheckEmailExist(string email, string memberShipNo)
        {
            return await Task.Run(() => bookingServiceDL.CheckEmailExist(email, memberShipNo)).ConfigureAwait(false);
        }
        public async Task<string> CheckPhoneNumberExist(string phone_number, string memberShipNo)
        {
            return await Task.Run(() => bookingServiceDL.CheckPhoneNumberExist(phone_number, memberShipNo)).ConfigureAwait(false);
        }
        public int SavePassengerDetails(string passengerDetails)
        {
            return bookingServiceDL.SavePassengerDetails(passengerDetails);
        }
        public int SaveCoBrowseDetails(string cobrowsModel)
        {
            return bookingServiceDL.SaveCoBrowseDetails(cobrowsModel);
        }
        public List<Passenger> Getpassenger(string quoteHeaderId)
        {
            return bookingServiceDL.Getpassenger(quoteHeaderId);
        }
        public int SavePaymentDetails(List<PaymentDetail> payments)
        {
            return bookingServiceDL.SavePaymentDetails(payments);
        }
        public bool SaveQuoteOverrideDetails(QuoteOverride quoteOverride)
        {
            return bookingServiceDL.SaveQuoteOverrideDetails(quoteOverride);
        }
        public bool SaveSesameQuoteForCreateRefreshBook(SesameBasket sesameBasket)
        {
            return bookingServiceDL.SaveSesameQuoteForCreateRefreshBook(sesameBasket);
        }
        public List<KeyValuePair<string, string>> GetOptionsForValue(int valueId)
        {
            return bookingServiceDL.GetOptionsForValue(valueId);
        }
        public bool SaveTransfersResponseData(TransfersBookData transfersBookData)
        {
            return bookingServiceDL.SaveTransfersResponseData(transfersBookData);
        }
        public List<PaymentDetail> GetCompletePaymentDetails(int quoteHeaderID)
        {
            return bookingServiceDL.GetCompletePaymentDetails(quoteHeaderID);
        }
        public Quote LoadQuoteData(string QuoteHeaderId)
        {
            try
            {
                Quote quoteData = new Quote();
                quoteData = bookingServiceDL.LoadQuoteData(QuoteHeaderId);
                return quoteData;
            }
            catch (Exception EX)
            {
                throw EX;
            }
        }
        public Task<int> SaveCoBrowseConformationResponse(int quoteHeaderId, string guId, string responseMsg)
        {
            return bookingServiceDL.SaveCoBrowseConformationResponse(quoteHeaderId, guId, responseMsg);
        }
        public SesameBasket GetComponentDetails(int quoteHeaderId, string basketId, int typeId)
        {
            return bookingServiceDL.GetComponentDetails(quoteHeaderId, basketId, typeId);
        }
        public TransfersBookData GetHolidayTaxisData(int quoteHeaderId)
        {
            return bookingServiceDL.GetHolidayTaxisData(quoteHeaderId);
        }

        public List<AgentQuote> GetAgentQuotes(string quoteRef, string customerName, string fromDate, string toDate, int agentid, bool isLast24Hours)
        {
            return bookingServiceDL.GetAgentQuotes(quoteRef, customerName, fromDate, toDate, agentid, isLast24Hours);
        }

        public bool UpdateAgentNotes(string quoteHeaderID,string agentNotes)
        {
            return bookingServiceDL.UpdateAgentNotes(quoteHeaderID, agentNotes);
        }
    }
}
