﻿using Newtonsoft.Json;
using Swordfish.Businessobjects.Baggage;
using Swordfish.Businessobjects.Common;
using Swordfish.Businessobjects.Hotels;
using Swordfish.Businessobjects.Membership;
using Swordfish.Businessobjects.Payments;
using Swordfish.Businessobjects.Quotes;
using Swordfish.Businessobjects.Sesame;
using Swordfish.Businessobjects.TopDog;
using Swordfish.Components.DataAccessLayer;
using Swordfish.Components.TopDogBasketService;
using Swordfish.Utilities.DataAccess;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Swordfish.Components.TopDogServices
{
    public class TopDogOperations
    {
        private static string clientIP = string.Empty;
        public TopDogOperations(string clientIp)
        {
            clientIP = clientIp;
        }
        public static string sessionID = string.Empty;
        public static string userSessionID = string.Empty;
        public static string Environment = string.Empty;
        public static int userId = 0;
        public static int TopdogId = 0;
        public static List<TopDogResponse> topDogResponses = new List<TopDogResponse>();
        public static string refNum = string.Empty;
        public static string surName = string.Empty;
        public static string departureDate = string.Empty;

        public List<TopDogResponse> Start(Quote quote, List<Swordfish.Businessobjects.Passengers.Passenger> passengers, UserInfo agent, List<PaymentDetail> payment, List<ExtraCharge> extraCharge)
        {
            if (agent != null)
            {
                userId = agent.UserId;
                userSessionID = agent.SessionId;
                Environment = agent.Environment;
                TopdogId = agent.TopDogId;
            }

            LogToDB("TopDogOperations:Start(): Started!!", "TopDogMessage", "Start");
            topDogResponses = new List<TopDogResponse>();
            TopDogBlackBox.BlackBoxPortTypeClient blackBox = new TopDogBlackBox.BlackBoxPortTypeClient();
            try
            {
                TopDogBasketService.ExternalProductReservationRequest ExternalProductReservation = new TopDogBasketService.ExternalProductReservationRequest();
                TopDogBlackBox.StartSessionResponse sessionResponse = StartSession(blackBox, quote);

                if (sessionResponse != null && sessionResponse.Errors == null && sessionResponse.Warnings == null)
                {
                    LogTopDogResponse("StartSession", "Session created successfully.", false, false, sessionResponse.SessionId);
                    TopDogBasketService.BasketOperationsPortTypeClient basketClient = new TopDogBasketService.BasketOperationsPortTypeClient();

                    sessionID = sessionResponse.SessionId;
                    ExternalProductReservation.SessionId = sessionID;

                    //Add Passenger
                    WriteTopDogLog("Info", "AddPassenger() calling Started!!.");
                    AddPassengersResponse passengerResponse = AddPassenger(basketClient, passengers, quote);
                    if (passengerResponse.Errors != null && passengerResponse.Errors.Count() >= 1)
                    {
                        LogTopDogResponse("Passengers", passengerResponse.Errors[0], true, true);
                        return topDogResponses;
                    }
                    else
                        LogTopDogResponse("Passengers", "Success", true, false);
                    WriteTopDogLog("Info", "AddPassenger() calling completed successfully!!.");

                    //Add External components
                    WriteTopDogLog("Info", "ExternalProductReservation() calling Started!!.");
                    ExternalProductReservationResponse externalProductReservationresponse = TopDogOperations.ExternalProductReservation(ExternalProductReservation, basketClient, quote, passengers, passengerResponse);
                    if (externalProductReservationresponse.Errors != null && externalProductReservationresponse.Errors.Count() >= 1)
                    {
                        LogTopDogResponse("ExternalProductReservation", externalProductReservationresponse.Errors[0], false, true);
                        return topDogResponses;
                    }
                    else
                        LogTopDogResponse("ExternalProductReservation", "Success", false, false, externalProductReservationresponse.BookCode);
                    WriteTopDogLog("Info", "ExternalProductReservation() calling completed successfully!!.");

                    //Add Agent Details
                    WriteTopDogLog("Info", "AddClient() calling Started!!.");
                    AddClientResponse addClientRequestResponse = AddClient(basketClient, quote, passengers);
                    if (addClientRequestResponse.Errors != null && addClientRequestResponse.Errors.Count() >= 1)
                    {
                        LogTopDogResponse("Agent", addClientRequestResponse.Errors[0], true, true);
                        return topDogResponses;
                    }
                    else
                        LogTopDogResponse("Agent", "Success", false, false);
                    WriteTopDogLog("Info", "AddClient() calling completed successfully!!. at " + DateTime.Now.ToString());

                    //Add Charges Details
                    WriteTopDogLog("Info", "AddExtraChargeSInfo() calling Started!!.");
                    if (quote.quoteHeader.MarginHotel > 0 || quote.quoteHeader.FidelityCost > 0)
                    {
                        AddExtChargesResponse extraChargeResponse = AddExtraChargesInfo(basketClient, extraCharge, quote, passengers, payment);
                        if (extraChargeResponse.Errors != null && extraChargeResponse.Errors.Count() >= 1)
                        {
                            LogTopDogResponse("Extra Charges", extraChargeResponse.Errors[0], true, true);
                            return topDogResponses;
                        }
                        else
                            LogTopDogResponse("Extra Charges", "Success", true, false);
                        WriteTopDogLog("Info", "AddExtraChargeSInfo() calling completed successfully!!.");
                    }

                    //Add ExtraBasket Info Details
                    WriteTopDogLog("Info", "AddExtraBasketInfo() calling Started!!.");
                    AddExtraBasketInfoResponse addExternalBasketRequestResponse = AddExtraBasketInfo(basketClient, quote, payment);
                    if (addExternalBasketRequestResponse.Errors != null && addExternalBasketRequestResponse.Errors.Count() >= 1)
                    {
                        LogTopDogResponse("Extra Basket Info", addExternalBasketRequestResponse.Errors[0], true, true);
                        return topDogResponses;
                    }
                    else
                        LogTopDogResponse("Extra Basket Info", "Success", true, false);
                    WriteTopDogLog("Info", "AddExtraBasketInfo() calling completed successfully!!.");

                    //Start Booking 
                    WriteTopDogLog("Info", "StartBooking() calling Started!!.");
                    StartBookingProcessResponse startBookingProcessResponse = StartBooking(basketClient);
                    if (startBookingProcessResponse.Errors != null && startBookingProcessResponse.Errors.Count() >= 1)
                    {
                        LogTopDogResponse("Start Booking", startBookingProcessResponse.Errors[0], false, true);
                        return topDogResponses;
                    }
                    else
                        LogTopDogResponse("Start Booking", "Success", false, false);
                    WriteTopDogLog("Info", "StartBooking() calling completed successfully!!.");

                    //Finish Booking
                    WriteTopDogLog("Info", "FinishBooking() calling Started!!.");
                    FinishBookingProcessResponse finishBookingProcessResponse = FinishBooking(basketClient);
                    if (finishBookingProcessResponse.Errors != null && finishBookingProcessResponse.Errors.Count() >= 1)
                    {
                        LogTopDogResponse("Finish Booking", finishBookingProcessResponse.Errors[0], false, true);
                        return topDogResponses;
                    }
                    else
                        LogTopDogResponse("Finish Booking", "Success", false, false);
                    WriteTopDogLog("Info", "FinishBooking() calling completed successfully!!.");

                    //Get Basket Response
                    WriteTopDogLog("Info", "GetMyBasket() calling Started!!.");
                    GetBasketResponse getBasketResponse = GetMyBasket(basketClient);
                    if (getBasketResponse.Errors != null && getBasketResponse.Errors.Count() >= 1)
                    {
                        LogTopDogResponse("TopDog Response", getBasketResponse.Errors[0], true, true);
                        return topDogResponses;
                    }
                    else
                    {
                        var response = getBasketResponse.Basket;
                        LogTopDogResponse("TopDog Response", "Success", true, false, getBasketResponse.Basket.BookingReference.ToString());
                        refNum = response.BookingReference.ToString();
                        surName = response.Passengers.Where(a => a.BasketLeadPassenger == true).FirstOrDefault().Surname;
                        departureDate = response.Products.OrderBy(a => a.FromDate).FirstOrDefault().FromDate;
                    }
                    WriteTopDogLog("Info", "GetMyBasket() calling completed successfully!!.");
                }
                else
                    LogTopDogResponse("Start Session", "Unable to create TopDog session.", true, false);
            }
            catch (Exception ex)
            {
                LogTopDogResponse("TopDog response", ex.Message, true, true);
                LogToDB("TopDogOperations:Start():" + ex.Message, "TopDogError", "Start", "", ex);
            }
            finally
            {
                if (!string.IsNullOrEmpty(sessionID))
                {
                    TopDogBlackBox.EndSessionRequest1 endSessionRequest1 = new TopDogBlackBox.EndSessionRequest1();
                    TopDogBlackBox.EndSessionRequest endSessionRequest = new TopDogBlackBox.EndSessionRequest();
                    endSessionRequest.SessionId = sessionID;
                    endSessionRequest1.EndSessionRequest = endSessionRequest;
                    var endSessionResponse = blackBox.EndSession(endSessionRequest1);
                    if (endSessionResponse.EndSessionResponse.Errors == null && endSessionResponse.EndSessionResponse.Warnings == null)
                    {
                        LogTopDogResponse("EndSession", "Session ended successfully.", false, false, endSessionResponse.EndSessionResponse.SessionId);
                        LogToDB("TopDogOperations:Start(): Session ended successfully.", "TopDogMessage", "Start");
                        var sessionIDEND = endSessionResponse.EndSessionResponse.SessionId;
                    }
                }
            }
            //Add Payment Details
            WriteTopDogLog("Info", "AddPayment() calling Started!!.");
            if (!string.IsNullOrEmpty(sessionID) && payment.Count > 0)
            {
                foreach (var pmt in payment)
                {
                    AddPaymentResponse addPaymentRequestResponse = AddPayment(quote, pmt, passengers);
                    if (addPaymentRequestResponse.Errors != null && addPaymentRequestResponse.Errors.Count() >= 1)
                    {
                        LogTopDogResponse("Payments", addPaymentRequestResponse.Errors[0], true, true);
                        return topDogResponses;
                    }
                    else
                        LogTopDogResponse("Payments", "Success", true, false);
                }
            }
            WriteTopDogLog("Info", "AddPayment() calling completed successfully!!.");
            LogToDB("TopDogOperations:Start(): Ended!!", "TopDogMessage", "Start");
            return topDogResponses;
        }
        private static void LogTopDogResponse(string verbName, string verdResponse, bool visbile, bool error, string topDogRef = "")
        {
            var topDagResponse = new TopDogResponse();
            topDagResponse.VerbName = verbName;
            topDagResponse.VerbResponse = verdResponse;
            topDagResponse.IsVisible = visbile;
            topDagResponse.IsError = error;
            topDagResponse.TopDogRefNum = topDogRef;
            topDogResponses.Add(topDagResponse);
        }
        private static ExternalProductReservationResponse ExternalProductReservation(ExternalProductReservationRequest ExternalProductReservation,
            BasketOperationsPortTypeClient basketClient, Quote quote, List<Swordfish.Businessobjects.Passengers.Passenger> passengers, AddPassengersResponse passengerResponse)
        {
            LogToDB("TopDogOperations:ExternalProductReservation(): Started!!", "TopDogMessage", "ExternalProductReservation");
            ExternalProductReservationResponse externalProductRes = null;
            try
            {
                ExternalProductDetails PKGDtls = new ExternalProductDetails();
                PassengerTypeQuantity[] passengerTypeQuantity = new PassengerTypeQuantity[3];

                //Topdog Hotel Integration.
                externalProductRes = HotelExternalProductReservation(ExternalProductReservation, externalProductRes, PKGDtls, quote,
                    passengerTypeQuantity, passengers, basketClient, passengerResponse);
                if (externalProductRes.Errors != null && externalProductRes.Errors.Count() >= 1)
                    return externalProductRes;
                if (quote.transferComponents.Count > 0)
                {
                    externalProductRes = TransferExternalProductReservation(ExternalProductReservation, externalProductRes, PKGDtls, quote, passengerTypeQuantity, passengers, basketClient);
                    if (externalProductRes.Errors != null && externalProductRes.Errors.Count() >= 1)
                    {
                        LogTopDogResponse("Transfers", externalProductRes.Errors[0], true, true);
                        return externalProductRes;
                    }
                    else
                    {
                        LogTopDogResponse("Transfers", "Success", true, false, externalProductRes.BookCode);
                        // LinkPassengersToProductRequest
                        WriteTopDogLog("Info", "LinkPassengerToProduct() calling Started!!.");
                        LinkPassengersToProductResponse linkResponse = LinkPassengerToProduct(basketClient, externalProductRes, passengerResponse);
                        if (linkResponse.Errors != null && linkResponse.Errors.Count() >= 1)
                            LogTopDogResponse("Link passengers to transfer", linkResponse.Errors[0], true, true);
                        else
                            LogTopDogResponse("Link passengers to transfer", "Success", false, false);
                        WriteTopDogLog("Info", "LinkPassengerToProduct() calling completed successfully!!.");
                    }
                }

                //Topdog Flight with Hops Integration.
                externalProductRes = FlightExternalProductReservation(ExternalProductReservation, externalProductRes, PKGDtls,
                    quote, passengerTypeQuantity, passengers, basketClient, passengerResponse);
                if (externalProductRes.Errors != null && externalProductRes.Errors.Count() >= 1)
                    return externalProductRes;

            }
            catch (Exception ex)
            {
                LogTopDogResponse("External Product", ex.Message, false, true);
                LogToDB("TopDogOperations:ExternalProductReservation(): " + ex.Message, "TopDogError", "ExternalProductReservation");
                throw;
            }
            LogToDB("TopDogOperations:ExternalProductReservation(): Ended!!", "TopDogMessage", "ExternalProductReservation");
            return externalProductRes;
        }
        private static ExternalProductReservationResponse HotelExternalProductReservation(ExternalProductReservationRequest ExternalProductReservation,
            ExternalProductReservationResponse externalProductRes, ExternalProductDetails PKGDtls, Quote quote,
            PassengerTypeQuantity[] passengerTypeQuantity, List<Swordfish.Businessobjects.Passengers.Passenger> passengers, BasketOperationsPortTypeClient basketClient, AddPassengersResponse passengerResponse)
        {
            LogToDB("TopDogOperations:HotelExternalProductReservation(): Started!!", "TopDogMessage", "HotelExternalProductReservation");
            try
            {
                if (quote.hotelComponent != null)
                {
                    ExternalProduct HotelProduct = new ExternalProduct();
                    HotelProduct.OperatorCode = quote.HotelOperatorCode;
                    HotelProduct.BookingReference = string.IsNullOrEmpty(quote.SesameBookBasket.BasketComponents.FindAll(bc => bc.ProductTypeCode.Equals("ACO"))
                        .Select(y => y.SupplierBookingId).FirstOrDefault()) ? string.Empty : quote.SesameBookBasket.BasketComponents
                        .FindAll(bc => bc.ProductTypeCode.Equals("ACO")).Select(y => y.SupplierBookingId).FirstOrDefault();
                    HotelProduct.Description = quote.hotelComponent.HotelName;
                    HotelProduct.Status = ProductStatusCode.BKG_ACCOMPLISHED.ToString();
                    HotelProduct.ExternalProductType = ProductType.HotelProduct;

                    HotelExternalProduct HotelDtls = new HotelExternalProduct();
                    HotelDtls.CheckinDate = string.Format("{0:yyyy-MM-dd}", quote.hotelComponent.CheckInDate);
                    HotelDtls.CheckoutDate = string.Format("{0:yyyy-MM-dd}", quote.hotelComponent.CheckOutDate);
                    TopDogBasketService.CodeAndDescription BoradCDDescription = new TopDogBasketService.CodeAndDescription();
                    TopDogBasketService.CodeAndDescription codeAndDescription = new TopDogBasketService.CodeAndDescription();
                    if (quote.hotelComponent.hotelComponentsDetails.Count > 0)
                    {
                        foreach (HotelComponentDetail hotelcomponentdetail in quote.hotelComponent.hotelComponentsDetails)
                        {
                            codeAndDescription.Code = hotelcomponentdetail.RoomTypeCode;
                            codeAndDescription.Description = hotelcomponentdetail.RoomDescription;
                            BoradCDDescription.Code = hotelcomponentdetail.RatePlanCode;
                            BoradCDDescription.Description = hotelcomponentdetail.RatePlanName;
                        }
                    }
                    HotelDtls.RoomType = codeAndDescription;
                    HotelDtls.BoardType = BoradCDDescription;

                    //HotelSellingPrice

                    ExtHotelPrices HotelPrices = new ExtHotelPrices();
                    HotelPrices.HotelRateType = HotelRateType.PerUnit;
                    //HotelPrices.PerPersonPrices = new PerPersonPrice[] { new PerPersonPrice() { AgeType = "A", Price = (float)quote.quoteHeader.HotelCost / passengers.Count }, new PerPersonPrice() { AgeType = "C", Price = (float)quote.quoteHeader.HotelCost / passengers.Count }, new PerPersonPrice() { AgeType = "I", Price = (float)quote.quoteHeader.HotelCost / passengers.Count } };
                    // HotelPrices.PerUnitPrice = (float)(quote.quoteHeader.HotelCost + quote.quoteHeader.MarginHotel);
                    var hotelPrice = Convert.ToDecimal(quote.SesameBookBasket.BasketComponents.FindAll(x => x.ProductTypeCode == "ACO")
                        .Select(y => y.ComponentDetails.Sum(x => Convert.ToDecimal(x.SellingPricePence))).FirstOrDefault()) / 100;
                    HotelPrices.PerUnitPrice = (float)(hotelPrice);
                    HotelPrices.CurrencyCode = quote.SesameBasket.CurrencyCode;
                    HotelPrices.PerUnitPriceSpecified = true;
                    HotelDtls.HotelPrices = HotelPrices;

                    //Hotel Cost Price.
                    ExtHotelPrices HotelCosts = new ExtHotelPrices();
                    HotelCosts.HotelRateType = HotelRateType.PerUnit;
                    //HotelCosts.PerPersonPrices = new PerPersonPrice[] { new PerPersonPrice() { AgeType = "A", Price = (float)quote.quoteHeader.HotelCost/passengers.Count }, new PerPersonPrice() { AgeType = "C", Price = (float)quote.quoteHeader.HotelCost / passengers.Count }, new PerPersonPrice() { AgeType = "I", Price = (float)quote.quoteHeader.HotelCost / passengers.Count } };
                    //HotelCosts.PerUnitPrice = (float)quote.quoteHeader.HotelCost;
                    HotelCosts.PerUnitPrice = (float)(hotelPrice + (hotelPrice * quote.TopDogHotelCommision / 100));
                    HotelCosts.CurrencyCode = quote.SesameBasket.CurrencyCode;
                    HotelCosts.PerUnitPriceSpecified = true;
                    HotelDtls.HotelCosts = HotelCosts;

                    ExtProdHotelDetailedInfo ExtraHotelDtls = new ExtProdHotelDetailedInfo();
                    ExtraHotelDtls.Code = quote.hotelComponent.HotelCode.ToString();
                    ExtraHotelDtls.Name = quote.hotelComponent.HotelName;
                    TopDogBasketService.CodeAndDescription HtlExtaCatInfo = new TopDogBasketService.CodeAndDescription();
                    HtlExtaCatInfo.Code = quote.hotelComponent.StarRating.ToString();// To do
                    HtlExtaCatInfo.Description = quote.hotelComponent.StarRating.ToString() + " Star"; //To do;
                    ExtraHotelDtls.Category = HtlExtaCatInfo;
                    ExtraHotelDtls.Address = new ExtHotelAddress();
                    if (quote.hotelComponent.HotelInformatoin.Address != null)
                    {
                        ExtraHotelDtls.Address.AddressLine = quote.hotelComponent.HotelInformatoin.Address.Address1 + "," + quote.hotelComponent.HotelInformatoin.Address.Address2;
                        ExtraHotelDtls.Address.CityName = quote.hotelComponent.HotelInformatoin.Address.City;
                        ExtraHotelDtls.Address.Country = quote.hotelComponent.HotelInformatoin.Address.Country;
                        ExtraHotelDtls.Address.PostalCode = quote.hotelComponent.HotelInformatoin.Address.Postcode;
                        ExtraHotelDtls.TelephoneNumber = quote.hotelComponent.HotelInformatoin.Address.Tel;
                        ExtraHotelDtls.FaxNumber = quote.hotelComponent.HotelInformatoin.Address.Fax;
                    }
                    TopDogBasketService.CodeAndDescription HtlNearByAirport = new TopDogBasketService.CodeAndDescription();
                    HtlNearByAirport.Code = quote.searchModel.destinationCode;
                    HtlNearByAirport.Description = quote.searchModel.destinationName;
                    ExtraHotelDtls.Address.Airport = HtlNearByAirport;

                    ExtraHotelDtls.Address.Resort = new TopDogBasketService.CodeAndDescription();
                    List<TopDogSupplierMapping> tdSupplierMapping = new List<TopDogSupplierMapping>();
                    if (!string.IsNullOrEmpty(quote.hotelComponent.SupplierResortId))
                        tdSupplierMapping = GetTopDogSupplierMapping(quote.hotelComponent.SupplierResortId.Contains('-') ? quote.hotelComponent.SupplierResortId.Split('-')[1] : quote.hotelComponent.SupplierResortId, SFConstants.Resorts);

                    if (tdSupplierMapping != null && tdSupplierMapping.Count > 0)
                        ExtraHotelDtls.Address.Resort.Code = tdSupplierMapping.Select(y => y.MappedSystemCode).FirstOrDefault();

                    ExtraHotelDtls.Address.Resort.Description = quote.hotelComponent.SupplierResortName;
                    if (quote.hotelComponent.HotelInformatoin.Descriptions != null && quote.hotelComponent.HotelInformatoin.Descriptions.Count > 0)
                    {
                        ExtHotelFacility[] ExtHotelFacility = new ExtHotelFacility[quote.hotelComponent.HotelInformatoin.Descriptions.Count];

                        for (int j = 0; j < quote.hotelComponent.HotelInformatoin.Descriptions.Count; j++)
                        {
                            ExtHotelFacility ExtHotelFacilityobj = new ExtHotelFacility();
                            ExtHotelFacilityobj.Code = quote.hotelComponent.HotelInformatoin.Descriptions[j].Code;
                            ExtHotelFacilityobj.Description = quote.hotelComponent.HotelInformatoin.Descriptions[j].Value;
                            ExtHotelFacilityobj.Available = true;
                            ExtHotelFacility[j] = ExtHotelFacilityobj;
                        }
                        ExtraHotelDtls.HotelFacilities = ExtHotelFacility;
                    }
                    ExtHotelCharacteristics hotelCharacteristics = new ExtHotelCharacteristics();
                    if (quote.hotelComponent.HotelInformatoin.Images != null && quote.hotelComponent.HotelInformatoin.Images.Count > 0)
                    {
                        NameValuePair[] photos = new NameValuePair[quote.hotelComponent.HotelInformatoin.Images.Count];
                        for (int i = 0; i < quote.hotelComponent.HotelInformatoin.Images.Count; i++)
                        {
                            NameValuePair photo = new NameValuePair();
                            photo.Name = i.ToString();
                            photo.Value = quote.hotelComponent.HotelInformatoin.Images[i].url;
                            photos[i] = photo;
                        }
                        hotelCharacteristics.Photos = photos;
                    }
                    ExtraHotelDtls.Characteristics = hotelCharacteristics;
                    HotelDtls.HotelDetailedInfo = ExtraHotelDtls;

                    if (passengerTypeQuantity[0] != null || passengerTypeQuantity[1] != null || passengerTypeQuantity[2] != null)
                        HotelProduct.TravelerInformation = passengerTypeQuantity;
                    else
                    {
                        passengerTypeQuantity = CountPassengerType(passengers);
                        HotelProduct.TravelerInformation = passengerTypeQuantity;
                    }
                    var clientNotes = quote.SesameBookBasket.BasketComponents.FindAll(x => x.ProductTypeCode == "ACO").FirstOrDefault().ComponentMessages;
                    StringBuilder clientMessage = new StringBuilder();
                    if (clientNotes != null && clientNotes.Count > 0)
                    {
                        foreach (var item in clientNotes)
                        {
                            clientMessage.Append(item + "\n\n ");
                        }
                    }
                    if (clientMessage.Length > 0)
                        HotelProduct.ClientNotes = clientMessage.ToString();
                    //HotelProduct.InternalNotes = "Hotel internal notes";
                    //HotelProduct.ImportantClientNotes = "Hotel importent client notes";                  
                    PKGDtls.HotelExternalProduct = HotelDtls;
                    ExternalProductReservation.ExternalProduct = HotelProduct;
                    ExternalProductReservation.ExternalProduct.ExternalProductDetails = PKGDtls;

                    LogToDB("TopDogOperations:HotelExternalProductReservation(): Started!!", "TopDogMessage:ExternalProductReservationRequest", "HotelExternalProductReservation", JsonConvert.SerializeObject(ExternalProductReservation));
                    externalProductRes = basketClient.ExternalProductReservation(ExternalProductReservation);
                    LogToDB("TopDogOperations:HotelExternalProductReservation(): Ended!!", "TopDogMessage:ExternalProductReservationResponse", "HotelExternalProductReservation", JsonConvert.SerializeObject(externalProductRes));
                    if (externalProductRes.Errors != null && externalProductRes.Errors.Count() >= 1)
                        LogTopDogResponse("Accommodation", externalProductRes.Errors[0], true, true);
                    else
                    {
                        LogTopDogResponse("Accommodation", "Success", true, false, externalProductRes.BookCode);
                        ///LinkPassengersToProductRequest
                        WriteTopDogLog("Info", "LinkPassengerToProduct() calling Started!!.");
                        LinkPassengersToProductResponse linkResponse = LinkPassengerToProduct(basketClient, externalProductRes, passengerResponse);
                        if (linkResponse.Errors != null && linkResponse.Errors.Count() >= 1)
                            LogTopDogResponse("Link passengers to accommodation", linkResponse.Errors[0], true, true);
                        else
                            LogTopDogResponse("Link passengers to accommodation", "Success", false, false);
                        WriteTopDogLog("Info", "LinkPassengerToProduct() calling completed successfully!!.");
                    }
                }
                LogToDB("TopDogOperations:HotelExternalProductReservation(): Ended!!", "TopDogMessage", "HotelExternalProductReservation");
                return externalProductRes;
            }
            catch (Exception ex)
            {
                LogTopDogResponse("Accommodation", ex.Message, true, true);
                LogToDB("TopDogOperations:HotelExternalProductReservation():" + ex.Message, "TopDogError", "HotelExternalProductReservation", "", ex);
                throw;
            }

        }
        private static ExternalProductReservationResponse TransferExternalProductReservation(ExternalProductReservationRequest ExternalProductReservation,
            ExternalProductReservationResponse externalProductRes, ExternalProductDetails PKGDtls, Quote quote,
            PassengerTypeQuantity[] passengerTypeQuantity, List<Swordfish.Businessobjects.Passengers.Passenger> passengers, BasketOperationsPortTypeClient basketClient)
        {
            LogToDB("TopDogOperations:TransferExternalProductReservation(): Started!!", "TopDogMessage", "TransferExternalProductReservation");
            try
            {
                ExternalProduct TransfersProduct = new ExternalProduct();
                TransfersProduct.OperatorCode = quote.TransferOperatorCode;
                TransfersProduct.BookingReference = quote.BookedHolidayTaxiData.general.bookingReference == null ? quote.quoteHeader.CustomerReferenceCode : quote.BookedHolidayTaxiData.general.bookingReference;
                TransfersProduct.Description = quote.transferComponents[0].productDescription;
                TransfersProduct.Status = ProductStatusCode.BKG_ACCOMPLISHED.ToString();
                TransfersProduct.ExternalProductType = ProductType.TransferProduct;

                TransferExternalProduct TransfersExternalProd = new TransferExternalProduct();
                TransfersExternalProd.Vehicle = new ExtTransferVehicle();
                TransfersExternalProd.Vehicle.Name = quote.transferComponents[0].ProductType;
                TransfersExternalProd.VehiclesQuantity = quote.transferComponents[0].Units;

                TransfersExternalProd.Hotel = new ExtTransferHtl();
                TransfersExternalProd.Hotel.Name = quote.hotelComponent.HotelName;
                TransfersExternalProd.Hotel.Address = new ExtTransferHtlAddr();
                if (quote.hotelComponent.HotelInformatoin.Address != null)
                {
                    TransfersExternalProd.Hotel.Address.AddressLine1 = quote.hotelComponent.HotelInformatoin.Address.Address1;
                    TransfersExternalProd.Hotel.Address.AddressLine2 = quote.hotelComponent.HotelInformatoin.Address.Address2;
                    TransfersExternalProd.Hotel.Address.CountryCode = quote.hotelComponent.HotelInformatoin.Address.Country;
                    TransfersExternalProd.Hotel.Address.PostalCode = quote.hotelComponent.HotelInformatoin.Address.Postcode;
                }
                TransfersExternalProd.HotelLocation = new Location();
                TransfersExternalProd.HotelLocation.AirportCode = quote.searchModel.destinationCode;

                if (quote.hotelComponent != null)
                {
                    if (!string.IsNullOrEmpty(quote.hotelComponent.SupplierResortId) && !string.IsNullOrEmpty(quote.hotelComponent.SupplierResortName))
                    {
                        List<TopDogSupplierMapping> tdSupplierMapping = new List<TopDogSupplierMapping>();
                        if (!string.IsNullOrEmpty(quote.hotelComponent.SupplierResortId))
                            tdSupplierMapping = GetTopDogSupplierMapping(quote.hotelComponent.SupplierResortId.Contains('-') ? quote.hotelComponent.SupplierResortId.Split('-')[1] : quote.hotelComponent.SupplierResortId, SFConstants.Resorts);
                        if (tdSupplierMapping != null && tdSupplierMapping.Count > 0)
                            TransfersExternalProd.HotelLocation.ResortCode = tdSupplierMapping.Select(y => y.MappedSystemCode).FirstOrDefault();

                        TransfersExternalProd.HotelLocation.ResortName = quote.hotelComponent.SupplierResortName; //ToDo
                    }
                }
                else
                {
                    TransfersExternalProd.HotelLocation.ResortCode = null;
                    TransfersExternalProd.HotelLocation.ResortName = null;
                }
                TransfersExternalProd.ArrivalJourney = new ExtTransferlJourney();
                TransfersExternalProd.ArrivalJourney.FlightInfo = new TransferFlightInfo();

                if (quote.flightComponents != null && quote.flightComponents.Count > 0)
                {
                    if (quote.flightComponents[0].ComponentTypeOptionId == SFConstants.QuoteComponent_FlightReturn)
                    {
                        TransfersExternalProd.ArrivalJourney.FlightInfo.ArrivalAirportCode = quote.flightComponents[0].FlightComponentDetails.Where(x => x.DirectionOptionId.Equals(SFConstants.Direction_Outbound)).Last().ArrivalAirportCode; //SowrdFishBasket.Quote.PackageInformation.FlightInfo.FlightDetails[0].ArrivalAirport;
                        TransfersExternalProd.ArrivalJourney.FlightInfo.ArrivalDateTime = Convert.ToString(quote.flightComponents[0].FlightComponentDetails.Where(x => x.DirectionOptionId.Equals(SFConstants.Direction_Outbound)).Last().ArrivalDateTime.ToString("yyyy-MM-ddTHH:mm:ss"));
                        TransfersExternalProd.ArrivalJourney.FlightInfo.FlightNumber = Convert.ToString(quote.flightComponents[0].FlightComponentDetails.Where(x => x.DirectionOptionId.Equals(SFConstants.Direction_Outbound)).Last().FlightNumber);
                        TransfersExternalProd.ArrivalJourney.PickupDateTime = Convert.ToString(quote.flightComponents[0].FlightComponentDetails.Where(x => x.DirectionOptionId.Equals(SFConstants.Direction_Outbound)).Last().ArrivalDateTime.AddMinutes(quote.ArrivalPickUp).ToString("yyyy-MM-ddTHH:mm:ss"));

                    }
                    else
                    {
                        var flights = quote.flightComponents.Where(x => x.FlightComponentDetails.Select(y => y.DirectionOptionId).FirstOrDefault() == SFConstants.Direction_Outbound).ToList();
                        TransfersExternalProd.ArrivalJourney.FlightInfo.ArrivalAirportCode = flights.Last().FlightComponentDetails.Where(x => x.DirectionOptionId.Equals(SFConstants.Direction_Outbound)).Last().ArrivalAirportCode; //SowrdFishBasket.Quote.PackageInformation.FlightInfo.FlightDetails[0].ArrivalAirport;
                        TransfersExternalProd.ArrivalJourney.FlightInfo.ArrivalDateTime = Convert.ToString(flights.Last().FlightComponentDetails.Where(x => x.DirectionOptionId.Equals(SFConstants.Direction_Outbound)).Last().ArrivalDateTime.ToString("yyyy-MM-ddTHH:mm:ss"));
                        TransfersExternalProd.ArrivalJourney.FlightInfo.FlightNumber = Convert.ToString(flights.Last().FlightComponentDetails.Where(x => x.DirectionOptionId.Equals(SFConstants.Direction_Outbound)).Last().FlightNumber);
                        TransfersExternalProd.ArrivalJourney.PickupDateTime = Convert.ToString(flights.Last().FlightComponentDetails.Where(x => x.DirectionOptionId.Equals(SFConstants.Direction_Outbound)).Last().ArrivalDateTime.AddMinutes(quote.ArrivalPickUp).ToString("yyyy-MM-ddTHH:mm:ss"));
                    }
                }
                else
                {
                    TransfersExternalProd.ArrivalJourney.FlightInfo.ArrivalAirportCode = null;
                    TransfersExternalProd.ArrivalJourney.FlightInfo.ArrivalDateTime = null;
                    TransfersExternalProd.ArrivalJourney.FlightInfo.FlightNumber = null;
                    TransfersExternalProd.ArrivalJourney.PickupDateTime = null;
                }

                TransfersExternalProd.TransferMinutes = quote.transferComponents[0].TransferTime;
                TransfersExternalProd.DepartureJourney = new ExtTransferlJourney();
                TransfersExternalProd.DepartureJourney.FlightInfo = new TransferFlightInfo();

                if (quote.flightComponents != null && quote.flightComponents.Count > 0)
                {
                    if (quote.flightComponents[0].ComponentTypeOptionId == SFConstants.QuoteComponent_FlightReturn)
                    {
                        TransfersExternalProd.DepartureJourney.FlightInfo.DepartureAirportCode = quote.flightComponents[0].FlightComponentDetails.Where(x => x.DirectionOptionId.Equals(SFConstants.Direction_Inbound)).First().DepartureAirportCode;
                        TransfersExternalProd.DepartureJourney.FlightInfo.DepartureDateTime = Convert.ToString(quote.flightComponents[0].FlightComponentDetails.Where(x => x.DirectionOptionId.Equals(SFConstants.Direction_Inbound)).First().DepartureDateTime.ToString("yyyy-MM-ddTHH:mm:ss"));
                        TransfersExternalProd.DepartureJourney.FlightInfo.FlightNumber = Convert.ToString(quote.flightComponents[0].FlightComponentDetails.Where(x => x.DirectionOptionId.Equals(SFConstants.Direction_Inbound)).First().FlightNumber);
                        TransfersExternalProd.DepartureJourney.PickupDateTime = Convert.ToString(quote.flightComponents[0].FlightComponentDetails.Where(x => x.DirectionOptionId.Equals(SFConstants.Direction_Inbound)).First().DepartureDateTime.AddMinutes(-quote.DeparturePickUp).ToString("yyyy-MM-ddTHH:mm:ss"));
                    }
                    else
                    {
                        var flights = quote.flightComponents.Where(x => x.FlightComponentDetails.Select(y => y.DirectionOptionId).FirstOrDefault() == SFConstants.Direction_Inbound).ToList();
                        TransfersExternalProd.DepartureJourney.FlightInfo.DepartureAirportCode = flights.First().FlightComponentDetails.Where(x => x.DirectionOptionId.Equals(SFConstants.Direction_Inbound)).First().DepartureAirportCode;
                        TransfersExternalProd.DepartureJourney.FlightInfo.DepartureDateTime = Convert.ToString(flights.First().FlightComponentDetails.Where(x => x.DirectionOptionId.Equals(SFConstants.Direction_Inbound)).First().DepartureDateTime.ToString("yyyy-MM-ddTHH:mm:ss"));
                        TransfersExternalProd.DepartureJourney.FlightInfo.FlightNumber = Convert.ToString(flights.First().FlightComponentDetails.Where(x => x.DirectionOptionId.Equals(SFConstants.Direction_Inbound)).First().FlightNumber);
                        TransfersExternalProd.DepartureJourney.PickupDateTime = Convert.ToString(flights.First().FlightComponentDetails.Where(x => x.DirectionOptionId.Equals(SFConstants.Direction_Inbound)).First().DepartureDateTime.AddMinutes(-quote.DeparturePickUp).ToString("yyyy-MM-ddTHH:mm:ss"));
                    }
                }
                else
                {
                    TransfersExternalProd.DepartureJourney.FlightInfo.DepartureAirportCode = null;
                    TransfersExternalProd.DepartureJourney.FlightInfo.DepartureDateTime = null;
                    TransfersExternalProd.DepartureJourney.FlightInfo.FlightNumber = null;
                    TransfersExternalProd.DepartureJourney.PickupDateTime = null;
                }

                ExtProdPrice TransferPrices = new ExtProdPrice();
                TransferPrices.PriceBasisType = ExtProdPriceBasis.PerPerson;
                TransferPrices.PerBookingPriceSpecified = false;
                var transferPrice = (quote.transferComponents.Sum(t => t.totalPrice)) / passengers.Count;
                var transferCost = (quote.transferComponents.Sum(t => t.totalPrice) + (quote.transferComponents.Sum(t => t.totalPrice) * quote.TopDogTransferCommision / 100)) / passengers.Count;

                TransferPrices.PerPersonPrices = new ExtProdPerPersonPrice[] {
                new ExtProdPerPersonPrice() {
                    PersonAgeCode = PersonAgeCode.A,
                    Price =  (float)transferPrice
                    ,PriceSpecified=true},
                new ExtProdPerPersonPrice() {
                    PersonAgeCode = PersonAgeCode.C,
                    Price = (float)transferPrice
                    ,PriceSpecified=true },
                new ExtProdPerPersonPrice() {
                    PersonAgeCode = PersonAgeCode.I,
                    Price = (float)transferPrice
                    ,PriceSpecified=true} };
                TransferPrices.CurrencyCode = quote.transferComponents[0].Currency;
                TransfersExternalProd.TransferPrices = TransferPrices;

                ExtProdPrice TransferCosts = new ExtProdPrice();
                TransferCosts.PriceBasisType = ExtProdPriceBasis.PerPerson;
                TransferCosts.PerBookingPriceSpecified = false;
                TransferCosts.PerPersonPrices = new ExtProdPerPersonPrice[] {
                new ExtProdPerPersonPrice() {
                    PersonAgeCode = PersonAgeCode.A,
                    Price = (float)transferCost,PriceSpecified=true},
                new ExtProdPerPersonPrice() {
                    PersonAgeCode = PersonAgeCode.C,
                    Price = (float)transferCost,PriceSpecified=true },
                new ExtProdPerPersonPrice() {
                    PersonAgeCode = PersonAgeCode.I,
                    Price = (float)transferCost,PriceSpecified=true} };
                TransferCosts.CurrencyCode = quote.transferComponents[0].Currency;
                TransfersExternalProd.TransferCosts = TransferCosts;

                if (passengerTypeQuantity[0] != null || passengerTypeQuantity[1] != null || passengerTypeQuantity[2] != null)
                    TransfersProduct.TravelerInformation = passengerTypeQuantity;
                else
                {
                    passengerTypeQuantity = CountPassengerType(passengers);
                    TransfersProduct.TravelerInformation = passengerTypeQuantity;
                }

                TransfersProduct.ClientNotes = quote.BookedHolidayTaxiData.general.html + "\n\nARRIVAL INSTRUCTIONS \n\n" + quote.BookedHolidayTaxiData.transferbookdata[0].instruction
                    + "\n\nEMERGENCY AND RECONFIRMATION CONTACT DETAILS\n\n" + "Supplier: " + "\nReconfirmation Telephone Number: " + quote.BookedHolidayTaxiData.transferbookdata[0].reconfirmationTelephone
                    + "\n24 Hour Emergency Number: " + quote.BookedHolidayTaxiData.transferbookdata[0].emergencyTelephone + "\n24 Hour UK Emergency Number: " + quote.BookedHolidayTaxiData.transferbookdata[0].ukEmergencyTelephone
                    + "\n\nOffice Hours (local time)\nMonday-Friday: " + quote.BookedHolidayTaxiData.transferbookdata[0].hoursMondayFriday + "\nSaturday: " + quote.BookedHolidayTaxiData.transferbookdata[0].hourSaturday
                    + "\nSunday: " + quote.BookedHolidayTaxiData.transferbookdata[0].hoursSunday + "\n\nDEPARTURE INSTRUCTIONS\n\n" + quote.BookedHolidayTaxiData.transferbookdata[1].instruction
                    + "\n\nEMERGENCY AND RECONFIRMATION CONTACT DETAILS\n\n" + "Supplier: " + "\nReconfirmation Telephone Number: " + quote.BookedHolidayTaxiData.transferbookdata[1].reconfirmationTelephone
                    + "\n24 Hour Emergency Number: " + quote.BookedHolidayTaxiData.transferbookdata[1].emergencyTelephone + "\n24 Hour UK Emergency Number: " + quote.BookedHolidayTaxiData.transferbookdata[1].ukEmergencyTelephone
                    + "\n\nOffice Hours (local time)\nMonday-Friday: " + quote.BookedHolidayTaxiData.transferbookdata[1].hoursMondayFriday + "\nSaturday: " + quote.BookedHolidayTaxiData.transferbookdata[1].hourSaturday
                    + "\nSunday: " + quote.BookedHolidayTaxiData.transferbookdata[1].hoursSunday;
                // TransfersProduct.InternalNotes = quote.BookedHolidayTaxiData.transferbookdata[1].instruction;
                //TransfersExternalProd.Shuttle = true;
                PKGDtls.TransferExternalProduct = TransfersExternalProd;
                ExternalProductReservation.ExternalProduct = TransfersProduct;
                ExternalProductReservation.ExternalProduct.ExternalProductDetails = PKGDtls;
                LogToDB("TopDogOperations:TransferExternalProductReservation(): Started!!", "TopDogMessage:ExternalProductReservationRequest", "TransferExternalProductReservation", JsonConvert.SerializeObject(ExternalProductReservation));
                externalProductRes = basketClient.ExternalProductReservation(ExternalProductReservation);
                LogToDB("TopDogOperations:TransferExternalProductReservation(): Ended!!", "TopDogMessage:ExternalProductReservationRequest", "TransferExternalProductReservation", JsonConvert.SerializeObject(externalProductRes));
                return externalProductRes;
            }
            catch (Exception ex)
            {
                LogTopDogResponse("Transfers", ex.Message, true, true);
                LogToDB("TopDogOperations:TransferExternalProductReservation(): " + ex.Message, "TopDogError", "TransferExternalProductReservation", "", ex);
                throw;
            }
        }
        private static ExternalProductReservationResponse FlightExternalProductReservation(ExternalProductReservationRequest ExternalProductReservation,
            ExternalProductReservationResponse externalProductRes, ExternalProductDetails PKGDtls, Quote quote,
            PassengerTypeQuantity[] passengerTypeQuantity, List<Swordfish.Businessobjects.Passengers.Passenger> passengers, BasketOperationsPortTypeClient basketClient, AddPassengersResponse passengerResponse)
        {
            LogToDB("TopDogOperations:FlightExternalProductReservation(): Strated!!", "TopDogMessage", "FlightExternalProductReservation");
            try
            {
                if (quote != null)
                {
                    var sesameFlightComponent = new List<SesameComponent>();
                    if (quote.SesameBasket.BasketComponents != null)
                        sesameFlightComponent = quote.SesameBookBasket.BasketComponents.FindAll(bc => bc.ProductTypeCode.Equals("FLI"));

                    if (quote.flightComponents != null && quote.flightComponents.Count > 1 && quote.flightComponents[0].ComponentTypeOptionId == SFConstants.QuoteComponent_Flightoneway)
                    {
                        foreach (var flight in quote.flightComponents)
                        {
                            string currency = string.Empty;
                            var FlightProduct = new ExternalProduct();
                            List<TopDogSupplierMapping> tdSupplierMapping = new List<TopDogSupplierMapping>();
                            if (!string.IsNullOrEmpty(flight.FlightComponentDetails.Select(fl => fl.MarketingAirlineCode).FirstOrDefault()))
                                tdSupplierMapping = GetTopDogSupplierMapping(flight.FlightComponentDetails.Select(fl => fl.MarketingAirlineCode).FirstOrDefault(), SFConstants.Flights);
                            if (tdSupplierMapping != null && tdSupplierMapping.Count > 0)
                                FlightProduct.OperatorCode = tdSupplierMapping.Select(y => y.MappedSystemCode).FirstOrDefault();

                            var supplierId = string.Empty;
                            if (flight.FlightComponentDetails.Select(x => x.DirectionOptionId).FirstOrDefault().Equals(SFConstants.Direction_Outbound))
                            {
                                if (sesameFlightComponent != null && sesameFlightComponent.Count > 0)
                                    supplierId = sesameFlightComponent.Where(x => x.DirectionOptionId == SFConstants.Direction_Outbound).Select(y => y.SupplierBookingId).FirstOrDefault();
                                else
                                    supplierId = quote.EasyJetBookingStatus.Where(x => x.componentTypeOptionId == SFConstants.Direction_Outbound).Select(y => y.eJOutBoundBookingRefId).FirstOrDefault();
                            }
                            else
                            {
                                if (sesameFlightComponent != null && sesameFlightComponent.Count > 0 && flight.FlightComponentDetails.First().MarketingAirlineCode != SFConstants.EasyJetMarketingAirlineCode)
                                    supplierId = sesameFlightComponent.Where(x => x.DirectionOptionId == SFConstants.Direction_Inbound).Select(y => y.SupplierBookingId).FirstOrDefault();
                                else
                                    supplierId = quote.EasyJetBookingStatus.Where(x => x.componentTypeOptionId == SFConstants.Direction_Inbound).Select(y => y.eJInBoundBookingRefId).FirstOrDefault();
                            }
                            FlightProduct.BookingReference = supplierId;
                            FlightProduct.Description = flight.componentType;
                            FlightProduct.Status = ProductStatusCode.BKG_ACCOMPLISHED.ToString();
                            FlightProduct.ExternalProductType = ProductType.FlightProduct;

                            var FlightDtls = new FlightExternalProduct();
                            var flightSector = new ExtFlight[1][];
                            var flightSectorCount = 0;
                            var flightDetails = new ExtFlight[flight.FlightComponentDetails.Count];
                            foreach (var fligthSegment in flight.FlightComponentDetails)
                            {

                                var ExtraFlightDtls = new ExtFlight();
                                ExtraFlightDtls.FlightNumber = fligthSegment.FlightNumber;
                                ExtraFlightDtls.OperatingAirlineCode = fligthSegment.MarketingAirlineCode;
                                ExtraFlightDtls.DepartureDateTime = fligthSegment.DepartureDateTime.ToString("yyyy-MM-ddTHH:mm:ss");
                                ExtraFlightDtls.ArrivalDateTime = fligthSegment.ArrivalDateTime.ToString("yyyy-MM-ddTHH:mm:ss");
                                ExtraFlightDtls.ServiceClass = fligthSegment.CabinClass;
                                ExtraFlightDtls.DepartureAirportCode = fligthSegment.DepartureAirportCode;
                                ExtraFlightDtls.ArrivalAirportCode = fligthSegment.ArrivalAirportCode;
                                ExtraFlightDtls.EnRouteStops = 0;
                                ExtraFlightDtls.EnRouteStopsSpecified = false;
                                flightDetails[flightSectorCount] = ExtraFlightDtls;
                                flightSectorCount++;
                            }

                            flightSector[0] = flightDetails;

                            FlightDtls.Itinerary = flightSector;

                            //var seameFlight = sesameFlightComponent.FirstOrDefault(s => s.DirectionOptionId == flight.FlightComponentDetails.First().DirectionOptionId);
                            float adultGBPPrice = 0, childGBPPrice = 0, infantGBPPrice = 0, adultEURPrice = 0, childEURPrice = 0, infantEURPrice = 0;
                            if (flight.FlightComponentDetails.First().MarketingAirlineCode == SFConstants.EasyJetMarketingAirlineCode)
                            {
                                if (quote.EasyJetBookings != null && quote.EasyJetBookings.Count > 0)
                                {
                                    if (flight.FlightComponentDetails.Select(x => x.DirectionOptionId).FirstOrDefault().Equals(SFConstants.Direction_Outbound))
                                    {
                                        adultGBPPrice = (float)quote.EasyJetBookings.FirstOrDefault(x => x.ComponentTypeOptionId.Equals(SFConstants.Direction_Outbound)).adultPricePP;
                                        childGBPPrice = (float)quote.EasyJetBookings.FirstOrDefault(x => x.ComponentTypeOptionId.Equals(SFConstants.Direction_Outbound)).childPricePP;
                                        infantGBPPrice = (float)quote.EasyJetBookings.FirstOrDefault(x => x.ComponentTypeOptionId.Equals(SFConstants.Direction_Outbound)).infantPricePP;
                                        adultEURPrice = (float)quote.EasyJetBookings.FirstOrDefault(x => x.ComponentTypeOptionId.Equals(SFConstants.Direction_Outbound)).adultPricePP;
                                        childEURPrice = (float)quote.EasyJetBookings.FirstOrDefault(x => x.ComponentTypeOptionId.Equals(SFConstants.Direction_Outbound)).childPricePP;
                                        infantEURPrice = (float)quote.EasyJetBookings.FirstOrDefault(x => x.ComponentTypeOptionId.Equals(SFConstants.Direction_Outbound)).infantPricePP;
                                        currency = quote.EasyJetBookings.FirstOrDefault(x => x.ComponentTypeOptionId.Equals(SFConstants.Direction_Outbound)).currencyCode;
                                    }
                                    else
                                    {
                                        adultGBPPrice = (float)quote.EasyJetBookings.FirstOrDefault(x => x.ComponentTypeOptionId.Equals(SFConstants.Direction_Inbound)).adultPricePP;
                                        childGBPPrice = (float)quote.EasyJetBookings.FirstOrDefault(x => x.ComponentTypeOptionId.Equals(SFConstants.Direction_Inbound)).childPricePP;
                                        infantGBPPrice = (float)quote.EasyJetBookings.FirstOrDefault(x => x.ComponentTypeOptionId.Equals(SFConstants.Direction_Inbound)).infantPricePP;
                                        adultEURPrice = (float)quote.EasyJetBookings.FirstOrDefault(x => x.ComponentTypeOptionId.Equals(SFConstants.Direction_Inbound)).adultPricePP;
                                        childEURPrice = (float)quote.EasyJetBookings.FirstOrDefault(x => x.ComponentTypeOptionId.Equals(SFConstants.Direction_Inbound)).childPricePP;
                                        infantEURPrice = (float)quote.EasyJetBookings.FirstOrDefault(x => x.ComponentTypeOptionId.Equals(SFConstants.Direction_Inbound)).infantPricePP;
                                        currency = quote.EasyJetBookings.FirstOrDefault(x => x.ComponentTypeOptionId.Equals(SFConstants.Direction_Inbound)).currencyCode;
                                    }
                                }
                            }
                            else
                            {
                                var flightPrices = quote.SesameBookBasket.BasketComponents.FindAll(bc => bc.ProductTypeCode.Equals("FLI"))
                                       .FirstOrDefault(s => s.DirectionOptionId == flight.FlightComponentDetails.First().DirectionOptionId).ComponentDetails.ToList();
                                currency = flightPrices.Select(x => x.SourceCurrency).FirstOrDefault();

                                var flightAdultPrice = passengers.Where(x => x.Type == "Adult").Count() > 0 ? flightPrices.Where(x => x.Type.Contains("adult")).Sum(x => Convert.ToDecimal(x.SellingPricePence)) / passengers.Where(x => x.Type == "Adult").Count() : 0;
                                var flightChildPrice = passengers.Where(x => x.Type == "Child").Count() > 0 ? flightPrices.Where(x => x.Type.Contains("child")).Sum(x => Convert.ToDecimal(x.SellingPricePence)) / passengers.Where(x => x.Type == "Child").Count() : 0;
                                var flightInfantPrice = passengers.Where(x => x.Type == "Infant").Count() > 0 ? flightPrices.Where(x => x.Type.Contains("infant")).Sum(x => Convert.ToDecimal(x.SellingPricePence)) / passengers.Where(x => x.Type == "Infant").Count() : 0;

                                if (flightAdultPrice > 0)
                                {
                                    adultGBPPrice = (float)(flightAdultPrice / 100);
                                }
                                if (flightChildPrice > 0)
                                {
                                    childGBPPrice = (float)(flightChildPrice / 100);
                                }
                                if (flightInfantPrice > 0)
                                {
                                    infantGBPPrice = (float)(flightInfantPrice / 100);
                                }

                                var EURAdultPrice = passengers.Where(x => x.Type == "Adult").Count() > 0 ? flightPrices.Where(x => x.Type.Contains("adult")).Sum(x => Convert.ToDecimal(x.SourcePricePence)) / passengers.Where(x => x.Type == "Adult").Count() : 0;
                                var EURChildPrice = passengers.Where(x => x.Type == "Child").Count() > 0 ? flightPrices.Where(x => x.Type.Contains("child")).Sum(x => Convert.ToDecimal(x.SourcePricePence)) / passengers.Where(x => x.Type == "Child").Count() : 0;
                                var EURInfantPrice = passengers.Where(x => x.Type == "Infant").Count() > 0 ? flightPrices.Where(x => x.Type.Contains("infant")).Sum(x => Convert.ToDecimal(x.SourcePricePence)) / passengers.Where(x => x.Type == "Infant").Count() : 0;

                                if (flightAdultPrice > 0)
                                {
                                    adultEURPrice = (float)(EURAdultPrice / 100);
                                }
                                if (flightChildPrice > 0)
                                {
                                    childEURPrice = (float)(EURChildPrice / 100);
                                }
                                if (flightInfantPrice > 0)
                                {
                                    infantEURPrice = (float)(EURInfantPrice / 100);
                                }
                            }
                            var FlightPrice = new ExtProdPrice();
                            FlightPrice.PriceBasisType = ExtProdPriceBasis.PerPerson;
                            FlightPrice.PerPersonPrices = new ExtProdPerPersonPrice[] {
                                new ExtProdPerPersonPrice() {
                                    PersonAgeCode = PersonAgeCode.A,
                                    Price =  adultGBPPrice,PriceSpecified=true },
                                new ExtProdPerPersonPrice() {
                                    PersonAgeCode = PersonAgeCode.C,
                                    Price = childGBPPrice,PriceSpecified=true },
                                new ExtProdPerPersonPrice() {
                                    PersonAgeCode = PersonAgeCode.I,
                                    Price = infantGBPPrice,PriceSpecified=true} };
                            FlightPrice.PerBookingPriceSpecified = false;
                            FlightPrice.CurrencyCode = quote.Currency;
                            FlightDtls.FlightPrices = FlightPrice;

                            var FlightCost = new ExtProdPrice();
                            FlightCost.PriceBasisType = ExtProdPriceBasis.PerPerson;
                            FlightCost.PerBookingPriceSpecified = false;
                            if (flight.FlightComponentDetails.Select(x => x.DirectionOptionId).FirstOrDefault().Equals(SFConstants.Direction_Outbound))
                            {
                                FlightCost.PerPersonPrices = new ExtProdPerPersonPrice[] {
                                new ExtProdPerPersonPrice() {
                                    PersonAgeCode = PersonAgeCode.A,
                                    Price =  adultGBPPrice ,PriceSpecified=true},
                                new ExtProdPerPersonPrice() {
                                    PersonAgeCode = PersonAgeCode.C,
                                    Price = childGBPPrice,PriceSpecified=true },
                                new ExtProdPerPersonPrice() {
                                    PersonAgeCode = PersonAgeCode.I,
                                    Price = infantGBPPrice,PriceSpecified=true} };
                                FlightCost.CurrencyCode = quote.Currency;
                            }
                            else
                            {
                                FlightCost.PerPersonPrices = new ExtProdPerPersonPrice[] {
                                new ExtProdPerPersonPrice() {
                                    PersonAgeCode = PersonAgeCode.A,Price =  adultEURPrice,PriceSpecified=true },

                                new ExtProdPerPersonPrice() {
                                    PersonAgeCode = PersonAgeCode.C,Price = childEURPrice ,PriceSpecified=true},

                                new ExtProdPerPersonPrice() {
                                    PersonAgeCode = PersonAgeCode.I,Price = infantEURPrice,PriceSpecified=true} };
                                FlightCost.CurrencyCode = currency;
                            }

                            FlightDtls.FlightCosts = FlightCost;
                            FlightDtls.TicketOnDeparture = false;
                            FlightDtls.TicketOnDepartureText = null;
                            FlightDtls.PlatingAirlineCode = flight.FlightComponentDetails.Last().MarketingAirlineCode;
                            FlightDtls.PlatingAirlineName = flight.FlightComponentDetails.Last().MarketingAirLine;
                            FlightDtls.ProduceTickets = new TopDogBasketService.BoolWrapper() { Value = false };

                            if (passengerTypeQuantity[0] != null || passengerTypeQuantity[1] != null || passengerTypeQuantity[2] != null)
                                FlightProduct.TravelerInformation = passengerTypeQuantity;
                            else
                            {
                                passengerTypeQuantity = CountPassengerType(passengers);
                                FlightProduct.TravelerInformation = passengerTypeQuantity;
                            }

                            if (flight.FlightComponentDetails.Select(x => x.DirectionOptionId).FirstOrDefault().Equals(SFConstants.Direction_Outbound)
                                && flight.FlightComponentDetails.Exists(fl => fl.MarketingAirlineCode.Equals("FR"))
                                && (sesameFlightComponent.FindAll(p => p.DirectionOptionId == SFConstants.Direction_Outbound).Where(v => v.ComponentVCNDetials.Count > 0)).Count() > 0)
                                FlightDtls.ConfirmationCardLastFourDigits = sesameFlightComponent.FindAll(p => p.DirectionOptionId == SFConstants.Direction_Outbound).Select(v => v.ComponentVCNDetials.Select(y => y.PAN.Substring(y.PAN.Length - 4, 4)).FirstOrDefault()).FirstOrDefault();
                            else if (flight.FlightComponentDetails.Select(x => x.DirectionOptionId).FirstOrDefault().Equals(SFConstants.Direction_Inbound)
                                && flight.FlightComponentDetails.Exists(fl => fl.MarketingAirlineCode.Equals("FR")) && (sesameFlightComponent.FindAll(p => p.DirectionOptionId == SFConstants.Direction_Inbound).Where(v => v.ComponentVCNDetials.Count > 0)).Count() > 0)
                                FlightDtls.ConfirmationCardLastFourDigits = sesameFlightComponent.FindAll(p => p.DirectionOptionId == SFConstants.Direction_Inbound).Select(v => v.ComponentVCNDetials.Select(y => y.PAN.Substring(y.PAN.Length - 4, 4)).FirstOrDefault()).FirstOrDefault();

                            PKGDtls.FlightExternalProduct = FlightDtls;
                            ExternalProductReservation.ExternalProduct = FlightProduct;
                            ExternalProductReservation.ExternalProduct.ExternalProductDetails = PKGDtls;
                            LogToDB("TopDogOperations:FlightExternalProductReservation(): Strated!!", "TopDogMessage:ExternalProductReservationRequest", "FlightExternalProductReservation", JsonConvert.SerializeObject(ExternalProductReservation));
                            externalProductRes = basketClient.ExternalProductReservation(ExternalProductReservation);
                            LogToDB("TopDogOperations:FlightExternalProductReservation(): Ended!!", "TopDogMessage:ExternalProductReservationResponse", "FlightExternalProductReservation", JsonConvert.SerializeObject(externalProductRes));
                            if (externalProductRes.Errors != null && externalProductRes.Errors.Count() >= 1)
                            {
                                if (flight.FlightComponentDetails.Select(x => x.DirectionOptionId).FirstOrDefault().Equals(SFConstants.Direction_Outbound))
                                    LogTopDogResponse("Outbound Flights", externalProductRes.Errors[0], true, true);
                                else
                                    LogTopDogResponse("Inbound Flights", externalProductRes.Errors[0], true, true);
                                return externalProductRes;
                            }
                            else
                            {
                                if (flight.FlightComponentDetails.Select(x => x.DirectionOptionId).FirstOrDefault().Equals(SFConstants.Direction_Outbound))
                                    LogTopDogResponse("Outbound Flights", "Success", true, false, externalProductRes.BookCode);
                                else
                                    LogTopDogResponse("Inbound Flights", "Success", true, false, externalProductRes.BookCode);
                                ///LinkPassengersToProductRequest
                                WriteTopDogLog("Info", "LinkPassengerToProduct() calling Started!!.");
                                LinkPassengersToProductResponse linkResponse = LinkPassengerToProduct(basketClient, externalProductRes, passengerResponse);
                                if (linkResponse.Errors != null && linkResponse.Errors.Count() >= 1)
                                    LogTopDogResponse("Link passengers to flight", linkResponse.Errors[0], true, true);
                                else
                                    LogTopDogResponse("Link passengers to flight", "Success", false, false);
                                WriteTopDogLog("Info", "LinkPassengerToProduct() calling completed successfully!!.");

                                AddExternalSupplementsResponse supplementResponse = new AddExternalSupplementsResponse();
                                if (quote.bagsComponents.Count > 0)
                                {
                                    if (flight.FlightComponentDetails.Select(x => x.DirectionOptionId).FirstOrDefault().Equals(SFConstants.Direction_Outbound))
                                    {
                                        if (flight.FlightComponentDetails.First().MarketingAirlineCode == SFConstants.EasyJetMarketingAirlineCode)
                                            supplementResponse = AddSupplementToProductForEasyJet(externalProductRes.BookCode, basketClient, quote, SFConstants.Direction_Outbound, passengers);
                                        else
                                            supplementResponse = AddSupplementToProduct(externalProductRes.BookCode, basketClient, quote, SFConstants.Direction_Outbound, passengers);
                                    }
                                    else
                                    {
                                        if (flight.FlightComponentDetails.First().MarketingAirlineCode == SFConstants.EasyJetMarketingAirlineCode)
                                            supplementResponse = AddSupplementToProductForEasyJet(externalProductRes.BookCode, basketClient, quote, SFConstants.Direction_Inbound, passengers);
                                        else
                                            supplementResponse = AddSupplementToProduct(externalProductRes.BookCode, basketClient, quote, SFConstants.Direction_Inbound, passengers);
                                    }

                                    if (supplementResponse.Errors != null && supplementResponse.Errors.Count() >= 1)
                                        LogTopDogResponse("Add Supplements to flight", supplementResponse.Errors[0], true, true);
                                    else
                                        LogTopDogResponse("Add Supplements to flight", "Success", false, false);
                                }
                            }
                        }
                    }
                    else if (quote.flightComponents != null && quote.flightComponents.Count > 0 && quote.flightComponents[0].ComponentTypeOptionId == SFConstants.QuoteComponent_FlightReturn)
                    {
                        foreach (var flight in quote.flightComponents)
                        {
                            var FlightProduct = new ExternalProduct();
                            List<TopDogSupplierMapping> tdSupplierMapping = new List<TopDogSupplierMapping>();
                            if (!string.IsNullOrEmpty(flight.FlightComponentDetails.Select(fl => fl.MarketingAirlineCode).FirstOrDefault()))
                                tdSupplierMapping = GetTopDogSupplierMapping(flight.FlightComponentDetails.Select(fl => fl.MarketingAirlineCode).FirstOrDefault(), SFConstants.Flights);
                            if (tdSupplierMapping != null && tdSupplierMapping.Count > 0)
                                FlightProduct.OperatorCode = tdSupplierMapping.Select(y => y.MappedSystemCode).FirstOrDefault();

                            if (sesameFlightComponent != null && sesameFlightComponent.Count > 0)
                                FlightProduct.BookingReference = sesameFlightComponent[0].SupplierBookingId;
                            else
                                FlightProduct.BookingReference = quote.EasyJetBookingStatus[0].eJReturnBookingRefId;
                            FlightProduct.BookingReference = sesameFlightComponent[0].SupplierBookingId;
                            FlightProduct.Description = flight.componentType;
                            FlightProduct.Status = ProductStatusCode.BKG_ACCOMPLISHED.ToString();
                            FlightProduct.ExternalProductType = ProductType.FlightProduct;

                            var FlightDtls = new FlightExternalProduct();
                            var flightSector = new ExtFlight[2][];
                            var flightOBSectorCount = 0;
                            var flightIBSectorCount = 0;

                            var outBoundFlights = flight.FlightComponentDetails.Where(f => f.DirectionOptionId == SFConstants.Direction_Outbound).ToList();
                            var inboundFlights = flight.FlightComponentDetails.Where(f => f.DirectionOptionId == SFConstants.Direction_Inbound).ToList();

                            if (outBoundFlights != null && outBoundFlights.Count > 0)
                            {
                                var flightOBDetails = new ExtFlight[outBoundFlights.Count];
                                foreach (var fligthSegment in outBoundFlights)
                                {
                                    var ExtraFlightDtls = new ExtFlight();
                                    ExtraFlightDtls.FlightNumber = fligthSegment.FlightNumber;
                                    ExtraFlightDtls.OperatingAirlineCode = fligthSegment.MarketingAirlineCode;
                                    ExtraFlightDtls.DepartureDateTime = fligthSegment.DepartureDateTime.ToString("yyyy-MM-ddTHH:mm:ss");
                                    ExtraFlightDtls.ArrivalDateTime = fligthSegment.ArrivalDateTime.ToString("yyyy-MM-ddTHH:mm:ss");
                                    ExtraFlightDtls.ServiceClass = fligthSegment.CabinClass;
                                    ExtraFlightDtls.DepartureAirportCode = fligthSegment.DepartureAirportCode;
                                    ExtraFlightDtls.ArrivalAirportCode = fligthSegment.ArrivalAirportCode;
                                    ExtraFlightDtls.EnRouteStops = 0;
                                    ExtraFlightDtls.EnRouteStopsSpecified = false;
                                    flightOBDetails[flightOBSectorCount] = ExtraFlightDtls;
                                    flightOBSectorCount++;
                                }

                                flightSector[0] = flightOBDetails;
                            }
                            if (inboundFlights != null && inboundFlights.Count > 0)
                            {
                                var flightIBDetails = new ExtFlight[inboundFlights.Count];
                                foreach (var fligthSegment in inboundFlights)
                                {
                                    var ExtraFlightDtls = new ExtFlight();
                                    ExtraFlightDtls.FlightNumber = fligthSegment.FlightNumber;
                                    ExtraFlightDtls.OperatingAirlineCode = fligthSegment.MarketingAirlineCode;
                                    ExtraFlightDtls.DepartureDateTime = fligthSegment.DepartureDateTime.ToString("yyyy-MM-ddTHH:mm:ss");
                                    ExtraFlightDtls.ArrivalDateTime = fligthSegment.ArrivalDateTime.ToString("yyyy-MM-ddTHH:mm:ss");
                                    ExtraFlightDtls.ServiceClass = fligthSegment.CabinClass;
                                    ExtraFlightDtls.DepartureAirportCode = fligthSegment.DepartureAirportCode;
                                    ExtraFlightDtls.ArrivalAirportCode = fligthSegment.ArrivalAirportCode;
                                    ExtraFlightDtls.EnRouteStops = 0;
                                    ExtraFlightDtls.EnRouteStopsSpecified = false;
                                    flightIBDetails[flightIBSectorCount] = ExtraFlightDtls;
                                    flightIBSectorCount++;
                                }
                                flightSector[1] = flightIBDetails;
                            }
                            FlightDtls.Itinerary = flightSector;
                            float adultGBPPrice = 0, childGBPPrice = 0, infantGBPPrice = 0;
                            if (flight.FlightComponentDetails.First().MarketingAirlineCode == SFConstants.EasyJetMarketingAirlineCode)
                            {
                                adultGBPPrice = (float)quote.EasyJetBookings.FirstOrDefault().adultPricePP;
                                childGBPPrice = (float)quote.EasyJetBookings.FirstOrDefault().childPricePP;
                                infantGBPPrice = (float)quote.EasyJetBookings.FirstOrDefault().infantPricePP;
                            }
                            else
                            {
                                var flightPrices = quote.SesameBookBasket.BasketComponents.FindAll(bc => bc.ProductTypeCode.Equals("FLI"))
                                    .FirstOrDefault().ComponentDetails.ToList();
                                var flightAdultPrice = passengers.Where(x => x.Type == "Adult").Count() > 0 ? flightPrices.Where(x => x.Type.Contains("adult")).Sum(x => Convert.ToDecimal(x.SellingPricePence)) / passengers.Where(x => x.Type == "Adult").Count() : 0;
                                var flightChildPrice = passengers.Where(x => x.Type == "Child").Count() > 0 ? flightPrices.Where(x => x.Type.Contains("child")).Sum(x => Convert.ToDecimal(x.SellingPricePence)) / passengers.Where(x => x.Type == "Child").Count() : 0;
                                var flightInfantPrice = passengers.Where(x => x.Type == "Infant").Count() > 0 ? flightPrices.Where(x => x.Type.Contains("infant")).Sum(x => Convert.ToDecimal(x.SellingPricePence)) / passengers.Where(x => x.Type == "Infant").Count() : 0;

                                if (flightAdultPrice > 0)
                                {
                                    adultGBPPrice = (float)(flightAdultPrice / 100);
                                }
                                if (flightChildPrice > 0)
                                {
                                    childGBPPrice = (float)(flightChildPrice / 100);
                                }
                                if (flightInfantPrice > 0)
                                {
                                    infantGBPPrice = (float)(flightInfantPrice / 100);
                                }
                            }
                            var FlightPrice = new ExtProdPrice();
                            FlightPrice.PriceBasisType = ExtProdPriceBasis.PerPerson;
                            FlightPrice.PerPersonPrices = new ExtProdPerPersonPrice[] {
                                new ExtProdPerPersonPrice() {
                                    PersonAgeCode = PersonAgeCode.A,
                                    Price =  adultGBPPrice,PriceSpecified=true },
                                new ExtProdPerPersonPrice() {
                                    PersonAgeCode = PersonAgeCode.C,
                                    Price = childGBPPrice,PriceSpecified=true },
                                new ExtProdPerPersonPrice() {
                                    PersonAgeCode = PersonAgeCode.I,
                                    Price = infantGBPPrice,PriceSpecified=true} };
                            FlightPrice.PerBookingPriceSpecified = false;
                            FlightPrice.CurrencyCode = quote.Currency;
                            FlightDtls.FlightPrices = FlightPrice;

                            var FlightCost = new ExtProdPrice();
                            FlightCost.PriceBasisType = ExtProdPriceBasis.PerPerson;
                            FlightCost.PerBookingPriceSpecified = false;
                            FlightCost.PerPersonPrices = new ExtProdPerPersonPrice[] {
                                new ExtProdPerPersonPrice() {
                                    PersonAgeCode = PersonAgeCode.A,
                                    Price =  adultGBPPrice ,PriceSpecified=true},
                                new ExtProdPerPersonPrice() {
                                    PersonAgeCode = PersonAgeCode.C,
                                    Price = childGBPPrice,PriceSpecified=true },
                                new ExtProdPerPersonPrice() {
                                    PersonAgeCode = PersonAgeCode.I,
                                    Price = infantGBPPrice,PriceSpecified=true} };
                            FlightCost.CurrencyCode = quote.Currency;
                            FlightDtls.FlightCosts = FlightCost;

                            if (passengerTypeQuantity[0] != null || passengerTypeQuantity[1] != null || passengerTypeQuantity[2] != null)
                                FlightProduct.TravelerInformation = passengerTypeQuantity;
                            else
                            {
                                passengerTypeQuantity = CountPassengerType(passengers);
                                FlightProduct.TravelerInformation = passengerTypeQuantity;
                            }

                            if (flight.FlightComponentDetails.Exists(fl => fl.MarketingAirlineCode.Equals("FR")) && (sesameFlightComponent.Where(v => v.ComponentVCNDetials.Count > 0).Count() > 0))
                                FlightDtls.ConfirmationCardLastFourDigits = sesameFlightComponent.Select(v => v.ComponentVCNDetials.Select(y => y.PAN.Substring(y.PAN.Length - 4, 4)).FirstOrDefault()).FirstOrDefault();

                            PKGDtls.FlightExternalProduct = FlightDtls;
                            ExternalProductReservation.ExternalProduct = FlightProduct;
                            ExternalProductReservation.ExternalProduct.ExternalProductDetails = PKGDtls;
                            LogToDB("TopDogOperations:FlightExternalProductReservation(): Strated!!", "TopDogMessage:ExternalProductReservationRequest", "FlightExternalProductReservation", JsonConvert.SerializeObject(ExternalProductReservation));
                            externalProductRes = basketClient.ExternalProductReservation(ExternalProductReservation);
                            LogToDB("TopDogOperations:FlightExternalProductReservation(): Ended!!", "TopDogMessage:ExternalProductReservationResponse", "FlightExternalProductReservation", JsonConvert.SerializeObject(externalProductRes));
                            if (externalProductRes.Errors != null && externalProductRes.Errors.Count() >= 1)
                            {
                                LogTopDogResponse("Flights", externalProductRes.Errors[0], true, true);
                                return externalProductRes;
                            }
                            else
                            {
                                LogTopDogResponse("Flights", "Success", true, false);
                                ///LinkPassengersToProductRequest
                                WriteTopDogLog("Info", "LinkPassengerToProduct() calling Started!!.");
                                LinkPassengersToProductResponse linkResponse = LinkPassengerToProduct(basketClient, externalProductRes, passengerResponse);
                                if (linkResponse.Errors != null && linkResponse.Errors.Count() >= 1)
                                    LogTopDogResponse("Link passengers to flight", linkResponse.Errors[0], true, true);
                                else
                                    LogTopDogResponse("Link passengers to flight", "Success", false, false, externalProductRes.BookCode);
                                WriteTopDogLog("Info", "LinkPassengerToProduct() calling completed successfully!!.");

                                //Add Supplements      
                                AddExternalSupplementsResponse supplementResponse = new AddExternalSupplementsResponse();
                                if (quote.bagsComponents.Count > 0)
                                {
                                    if (flight.FlightComponentDetails.First().MarketingAirlineCode == SFConstants.EasyJetMarketingAirlineCode)
                                        supplementResponse = AddSupplementToProductForEasyJet(externalProductRes.BookCode, basketClient, quote, 0, passengers);
                                    else
                                        supplementResponse = AddSupplementToProduct(externalProductRes.BookCode, basketClient, quote, 0, passengers);
                                    if (supplementResponse.Errors != null && supplementResponse.Errors.Count() >= 1)
                                        LogTopDogResponse("Add Supplements to flight", supplementResponse.Errors[0], true, true);
                                    else
                                        LogTopDogResponse("Add Supplements to flight", "Success", false, false);
                                }
                            }
                        }
                    }
                }

                LogToDB("TopDogOperations:FlightExternalProductReservation(): Ended!!", "TopDogMessage", "FlightExternalProductReservation");
                return externalProductRes;
            }
            catch (Exception ex)
            {
                LogTopDogResponse("Flights", ex.Message, true, true);
                LogToDB("TopDogOperations:FlightExternalProductReservation():" + ex.Message, "TopDogError", "FlightExternalProductReservation", "", ex);
                throw;
            }
        }
        private static AddExternalSupplementsResponse AddSupplementToProduct(string bookCode, BasketOperationsPortTypeClient basketClient, Quote quote, int type, List<Swordfish.Businessobjects.Passengers.Passenger> passengers)
        {
            LogToDB("TopDogOperations:AddSupplementToProduct(): Started!!", "TopDogMessage", "AddSupplementToProduct");
            AddExternalSupplementsResponse supplementResponse = new AddExternalSupplementsResponse();
            List<SesameDetail> componentDetails = new List<SesameDetail>();
            AddExternalSupplementsRequest externalSupplementsRequest = new AddExternalSupplementsRequest();
            if (type == SFConstants.Direction_Outbound)
                componentDetails = quote.SesameBookBasket.BasketComponents.FindAll(bc => bc.ProductTypeCode.Equals("FLI"))
                                          .FirstOrDefault(s => s.DirectionOptionId == type).ComponentDetails.Skip(passengers.Count).ToList();
            else if (type == SFConstants.Direction_Inbound)
                componentDetails = quote.SesameBookBasket.BasketComponents.FindAll(bc => bc.ProductTypeCode.Equals("FLI"))
                                      .FirstOrDefault(s => s.DirectionOptionId == type).ComponentDetails.Skip(passengers.Count).ToList();
            else if (quote.flightComponents != null && quote.flightComponents.Count > 0 && quote.flightComponents[0].ComponentTypeOptionId == SFConstants.QuoteComponent_FlightReturn)
                componentDetails = quote.SesameBookBasket.BasketComponents.FindAll(bc => bc.ProductTypeCode.Equals("FLI"))
                                           .FirstOrDefault().ComponentDetails.Skip(passengers.Count).ToList();
            if (componentDetails.Count > 0)
            {
                externalSupplementsRequest.SessionId = sessionID;
                externalSupplementsRequest.BookCode = bookCode;
                externalSupplementsRequest.ProductType = ProductType.FlightProduct;
                ExternalSupplement[] lstExternalSupplement = new ExternalSupplement[componentDetails.Count];
                int index = 0;
                foreach (var item in componentDetails)
                {
                    var externalSupplement = new ExternalSupplement();
                    externalSupplement.Code = "Baggage";
                    externalSupplement.Description = "Baggage: " + item.Description;
                    ExtProdSupplPrice extProdSupplPrice = new ExtProdSupplPrice();
                    extProdSupplPrice.Quantity = Convert.ToInt32(item.Quantity);
                    extProdSupplPrice.EachPrice = (float)((Convert.ToDecimal(item.SellingPricePence) / extProdSupplPrice.Quantity) / 100);
                    extProdSupplPrice.HowRule = HowRuleType.FLAT;
                    extProdSupplPrice.EachPriceSpecified = true;
                    extProdSupplPrice.QuantitySpecified = true;
                    extProdSupplPrice.RateSpecified = true;
                    externalSupplement.SupplementPrice = extProdSupplPrice;
                    ExtProdSupplPrice extProdSupplCost = new ExtProdSupplPrice();
                    extProdSupplCost.Quantity = Convert.ToInt32(item.Quantity);
                    if (type == SFConstants.Direction_Inbound)
                        extProdSupplCost.EachPrice = (float)((Convert.ToDecimal(item.SourcePricePence) / extProdSupplCost.Quantity) / 100);
                    else
                        extProdSupplCost.EachPrice = (float)((Convert.ToDecimal(item.SellingPricePence) / extProdSupplCost.Quantity) / 100);
                    extProdSupplCost.HowRule = HowRuleType.FLAT;
                    extProdSupplCost.EachPriceSpecified = true;
                    extProdSupplCost.QuantitySpecified = true;
                    extProdSupplCost.RateSpecified = true;
                    externalSupplement.SupplementCost = extProdSupplCost;
                    externalSupplement.Mandatory = true;
                    externalSupplement.Commissionable = true;
                    externalSupplement.AllowQuantityEdit = false;
                    externalSupplement.BookViaXML = false;
                    externalSupplement.ApplyRuleSpecified = true;
                    externalSupplement.ApplyRule = ApplyRule.PER_BOOKING;
                    externalSupplement.BookingDateTime = DateTime.Now.ToString("yyyy-MM-ddTHH:mm:ss");
                    lstExternalSupplement[index] = externalSupplement;
                    index++;
                }
                externalSupplementsRequest.ExternalSupplements = lstExternalSupplement;
                LogToDB("TopDogOperations:AddSupplementToProduct(): Started!!", "TopDogMessage:AddExternalSupplementsRequest", "AddSupplementToProduct", JsonConvert.SerializeObject(externalSupplementsRequest));
                supplementResponse = basketClient.AddExternalSupplements(externalSupplementsRequest);
                LogToDB("TopDogOperations:AddSupplementToProduct(): Ended!!", "TopDogMessage:AddExternalSupplementsRequest", "AddSupplementToProduct", JsonConvert.SerializeObject(supplementResponse));
            }
            LogToDB("TopDogOperations:AddSupplementToProduct(): Ended!!", "TopDogMessage", "AddSupplementToProduct");
            return supplementResponse;
        }
        private static AddExternalSupplementsResponse AddSupplementToProductForEasyJet(string bookCode, BasketOperationsPortTypeClient basketClient, Quote quote, int type, List<Swordfish.Businessobjects.Passengers.Passenger> passengers)
        {
            LogToDB("TopDogOperations:AddSupplementToProduct(): Started!!", "TopDogMessage", "AddSupplementToProduct");
            AddExternalSupplementsResponse supplementResponse = new AddExternalSupplementsResponse();
            List<BagComponent> bags = new List<BagComponent>();
            AddExternalSupplementsRequest externalSupplementsRequest = new AddExternalSupplementsRequest();
            if (type == SFConstants.Direction_Outbound)
                bags = quote.bagsComponents.Where(x => x.DirectionOptionId == SFConstants.Direction_Outbound).ToList();
            else if (type == SFConstants.Direction_Inbound)
                bags = quote.bagsComponents.Where(x => x.DirectionOptionId == SFConstants.Direction_Inbound).ToList();
            else
                bags = quote.bagsComponents.ToList();
            if (bags.Count > 0)
            {
                externalSupplementsRequest.SessionId = sessionID;
                externalSupplementsRequest.BookCode = bookCode;
                externalSupplementsRequest.ProductType = ProductType.FlightProduct;
                ExternalSupplement[] lstExternalSupplement = new ExternalSupplement[bags.GroupBy(x => x.BagsCode).Select(y => y.First()).Count()];
                int index = 0;
                foreach (var item in bags.GroupBy(x => x.BagsCode).Select(y => y.First()))
                {
                    var externalSupplement = new ExternalSupplement();
                    externalSupplement.Code = item.BagsType;
                    externalSupplement.Description = item.BagsType + ": " + item.BagsDescription;
                    ExtProdSupplPrice extProdSupplPrice = new ExtProdSupplPrice();
                    extProdSupplPrice.Quantity = bags.Where(y => y.BagsCode.Equals(item.BagsCode)).Count();
                    extProdSupplPrice.EachPrice = (float)item.SellingPrice;
                    extProdSupplPrice.HowRule = HowRuleType.FLAT;
                    extProdSupplPrice.EachPriceSpecified = true;
                    extProdSupplPrice.QuantitySpecified = true;
                    extProdSupplPrice.RateSpecified = true;
                    externalSupplement.SupplementPrice = extProdSupplPrice;
                    ExtProdSupplPrice extProdSupplCost = new ExtProdSupplPrice();
                    extProdSupplCost.Quantity = bags.Where(y => y.BagsCode.Equals(item.BagsCode)).Count();
                    if (type == SFConstants.Direction_Inbound)
                        extProdSupplCost.EachPrice = (float)item.BuyingPrice;
                    else
                        extProdSupplCost.EachPrice = (float)item.SellingPrice;
                    extProdSupplCost.HowRule = HowRuleType.FLAT;
                    extProdSupplCost.EachPriceSpecified = true;
                    extProdSupplCost.QuantitySpecified = true;
                    extProdSupplCost.RateSpecified = true;
                    externalSupplement.SupplementCost = extProdSupplCost;
                    externalSupplement.Mandatory = true;
                    externalSupplement.Commissionable = true;
                    externalSupplement.AllowQuantityEdit = false;
                    externalSupplement.BookViaXML = false;
                    externalSupplement.ApplyRuleSpecified = true;
                    externalSupplement.ApplyRule = ApplyRule.PER_BOOKING;
                    externalSupplement.BookingDateTime = DateTime.Now.ToString("yyyy-MM-ddTHH:mm:ss");
                    lstExternalSupplement[index] = externalSupplement;
                    index++;
                }
                externalSupplementsRequest.ExternalSupplements = lstExternalSupplement;
                LogToDB("TopDogOperations:AddSupplementToProduct(): Started!!", "TopDogMessage:AddExternalSupplementsRequest", "AddSupplementToProduct", JsonConvert.SerializeObject(externalSupplementsRequest));
                supplementResponse = basketClient.AddExternalSupplements(externalSupplementsRequest);
                LogToDB("TopDogOperations:AddSupplementToProduct(): Ended!!", "TopDogMessage:AddExternalSupplementsRequest", "AddSupplementToProduct", JsonConvert.SerializeObject(supplementResponse));
            }
            LogToDB("TopDogOperations:AddSupplementToProduct(): Ended!!", "TopDogMessage", "AddSupplementToProduct");
            return supplementResponse;
        }

        private static AddPassengersResponse AddPassenger(BasketOperationsPortTypeClient basketClient, List<Swordfish.Businessobjects.Passengers.Passenger> passengers, Quote quote)
        {
            LogToDB("TopDogOperations:AddPassenger(): Started!!", "TopDogMessage", "AddPassenger");
            try
            {
                TopDogBasketService.AddPassengersRequest addPassengerRequest = new TopDogBasketService.AddPassengersRequest();
                TopDogBasketService.Passenger[] PassengerInfo = new TopDogBasketService.Passenger[passengers.Count];

                for (int i = 0; i <= passengers.Count - 1; i++)
                {
                    TopDogBasketService.Passenger PassengerInfoDtls = new TopDogBasketService.Passenger();
                    if (passengers[i].Title == "Master")
                    {
                        passengers[i].Title = "MSTR";
                    }
                    PassengerInfoDtls.Title = passengers[i].Title;
                    PassengerInfoDtls.FirstName = passengers[i].FirstName;
                    PassengerInfoDtls.SecondName = passengers[i].MiddleName;
                    PassengerInfoDtls.Surname = passengers[i].LastName;
                    if (passengers[i].Title == "Mr" || passengers[i].Title == "Master")
                        PassengerInfoDtls.Gender = Gender.M;
                    else
                        PassengerInfoDtls.Gender = Gender.F;

                    //PassengerAddress passengerAddress = new PassengerAddress();
                    //passengerAddress.AddressLine1 = passengers[i].Address1;
                    //passengerAddress.AddressLine2 = passengers[i].Address2;
                    //passengerAddress.Country = passengers[i].Country;
                    //passengerAddress.PostCode = passengers[i].PostCode;

                    //PassengerInfoDtls.Address = passengerAddress;

                    TopDogBasketService.IntWrapper intWrapper = new TopDogBasketService.IntWrapper();
                    if ((passengers[i].DateOfBirth != null && passengers[i].DateOfBirth != "") || (passengers[i].DefaultDateOfBirth != null && passengers[i].DefaultDateOfBirth != ""))
                    {
                        string PassengerAge = !string.IsNullOrEmpty(passengers[i].DateOfBirth) ? GetTodaysAge(DateTime.ParseExact(passengers[i].DateOfBirth, "dd/MM/yyyy", System.Globalization.CultureInfo.InvariantCulture), DateTime.Now).ToString() : GetTodaysAge(DateTime.ParseExact(passengers[i].DefaultDateOfBirth, "dd/MM/yyyy", System.Globalization.CultureInfo.InvariantCulture), DateTime.Now).ToString();
                        intWrapper.Value = Convert.ToInt32(PassengerAge);
                    }
                    else { intWrapper.Value = 0; }

                    PassengerInfoDtls.Age = intWrapper;

                    if (passengers[i].IsLeadPassenger == "True")
                        PassengerInfoDtls.BasketLeadPassenger = true;
                    else
                        PassengerInfoDtls.BasketLeadPassenger = false;
                    if ((passengers[i].DateOfBirth != null && passengers[i].DateOfBirth != "") || (passengers[i].DefaultDateOfBirth != null && passengers[i].DefaultDateOfBirth != ""))
                    {
                        PassengerInfoDtls.BirthDate = !string.IsNullOrEmpty(passengers[i].DateOfBirth) ? String.Format("{0:yyyy-MM-dd}", DateTime.ParseExact(passengers[i].DateOfBirth, "dd/MM/yyyy", System.Globalization.CultureInfo.InvariantCulture)) : String.Format("{0:yyyy-MM-dd}", DateTime.ParseExact(passengers[i].DefaultDateOfBirth, "dd/MM/yyyy", System.Globalization.CultureInfo.InvariantCulture));
                    }
                    else
                    {
                        PassengerInfoDtls.BirthDate = null;

                    }
                    //  PassengerInfoDtls.EmailAddress = passengers[i].Email;
                    PassengerInfoDtls.GenderSpecified = true;
                    //   PassengerInfoDtls.Initials = passengers[i].Title;
                    // PassengerInfoDtls.MobileNumber = passengers[i].Mobile;
                    PassengerInfoDtls.Nationality = quote.TopDogPassengerNationality;
                    //  PassengerInfoDtls.PassportNumber = passengers[i].PassportNumber;
                    PassengerInfoDtls.PersonAgeCode = GetPassengerType(passengers[i].Type);
                    //  PassengerInfoDtls.PhoneNumber = passengers[i].Telephone;
                    PassengerInfo[i] = PassengerInfoDtls;
                }
                addPassengerRequest.Passengers = PassengerInfo;
                addPassengerRequest.SessionId = sessionID;
                LogToDB("TopDogOperations:AddPassenger(): Started!!", "TopDogMessage:AddPassengersRequest", "AddPassenger", JsonConvert.SerializeObject(addPassengerRequest));
                var passengerResponse = basketClient.AddPassenger(addPassengerRequest);
                LogToDB("TopDogOperations:AddPassenger(): Ended!!", "TopDogMessage:AddPassengersResponse", "AddPassenger", JsonConvert.SerializeObject(passengerResponse));
                return passengerResponse;
            }
            catch (Exception ex)
            {
                LogTopDogResponse("Passengers", ex.Message, true, true);
                LogToDB("TopDogOperations:AddPassenger(): " + ex.Message, "TopDogError", "AddPassenger", "", ex);
            }
            return null;
        }
        private static PassengerTypeQuantity[] CountPassengerType(List<Swordfish.Businessobjects.Passengers.Passenger> passengers)
        {
            LogToDB("TopDogOperations:CountPassengerType(): Started!!", "TopDogMessage", "CountPassengerType");
            PassengerTypeQuantity[] passengerTypeQuantity = new PassengerTypeQuantity[3];
            if (passengers.Count != 0)
            {
                int AdultPsssenger = 0;
                int ChildPsssenger = 0;
                int InfantPsssenger = 0;
                for (int i = 0; i < passengers.Count; i++)
                {
                    if (GetPassengerType(passengers[i].Type) == "A" && AdultPsssenger == 0)
                    {
                        PassengerTypeQuantity passengerTypeQty = new PassengerTypeQuantity();
                        AdultPsssenger = passengers.Where(x => GetPassengerType(x.Type) == "A").Count();
                        passengerTypeQty.PersonAgeCode = PersonAgeCode.A;
                        passengerTypeQty.Quantity = AdultPsssenger;
                        passengerTypeQuantity[0] = passengerTypeQty;
                    }
                    else if (GetPassengerType(passengers[i].Type) == "C" && ChildPsssenger == 0)
                    {
                        PassengerTypeQuantity passengerTypeQty = new PassengerTypeQuantity();
                        ChildPsssenger = passengers.Where(x => GetPassengerType(x.Type) == "C").Count();
                        passengerTypeQty.PersonAgeCode = PersonAgeCode.C;
                        passengerTypeQty.Quantity = ChildPsssenger;
                        passengerTypeQuantity[1] = passengerTypeQty;
                    }
                    else if (GetPassengerType(passengers[i].Type) == "I" && InfantPsssenger == 0)
                    {
                        PassengerTypeQuantity passengerTypeQty = new PassengerTypeQuantity();
                        InfantPsssenger = passengers.Where(x => GetPassengerType(x.Type) == "I").Count();
                        passengerTypeQty.PersonAgeCode = PersonAgeCode.I;
                        passengerTypeQty.Quantity = InfantPsssenger;
                        passengerTypeQuantity[2] = passengerTypeQty;
                    }
                }
            }
            LogToDB("TopDogOperations:CountPassengerType(): Ended!!", "TopDogMessage", "CountPassengerType");
            return passengerTypeQuantity;
        }
        private static string GetPassengerType(string type)
        {
            LogToDB("TopDogOperations:GetPassengerType(): Started!!", "TopDogMessage", "GetPassengerType");
            string passengerCode = string.Empty;
            switch (type)
            {
                case "Adult":
                    passengerCode = "A";
                    break;
                case "Child":
                    passengerCode = "C";
                    break;
                case "Infant":
                    passengerCode = "I";
                    break;
            }
            LogToDB("TopDogOperations:GetPassengerType(): Ended!!", "TopDogMessage", "GetPassengerType");
            return passengerCode;
        }
        private static int GetTodaysAge(DateTime birthDate, DateTime now)
        {
            int age = Convert.ToInt32(now.Year.ToString()) - Convert.ToInt32(birthDate.Year.ToString());
            return age;
        }
        private static TopDogBlackBox.StartSessionResponse StartSession(TopDogBlackBox.BlackBoxPortTypeClient blackBox, Quote quote)
        {
            LogToDB("TopDogOperations:StartSession(): Started!!", "TopDogMessage", "StartSession");
            try
            {
                TopDogBlackBox.StartSessionRequest1 startSession1 = new TopDogBlackBox.StartSessionRequest1();
                TopDogBlackBox.StartSessionRequest startSession = new TopDogBlackBox.StartSessionRequest();
                startSession.UserId = quote.TopDogUserId;
                startSession.Password = quote.TopDogPassword;
                startSession.LanguageCode = quote.TopDogLanguage;
                startSession1.StartSessionRequest = startSession;
                LogToDB("TopDogOperations:StartSession(): Started!!", "TopDogMessage:StartSessionRequest", "StartSession", JsonConvert.SerializeObject(startSession));
                TopDogBlackBox.StartSessionResponse1 sessionResponse1 = blackBox.StartSession(startSession1);
                TopDogBlackBox.StartSessionResponse sessionResponse = sessionResponse1.StartSessionResponse;
                LogToDB("TopDogOperations:StartSession(): Ended!!", "TopDogMessage:StartSessionResponse", "StartSession", JsonConvert.SerializeObject(sessionResponse));
                return sessionResponse;
            }
            catch (Exception ex)
            {
                LogTopDogResponse("Start Session", ex.Message, true, true);
                LogToDB("TopDogOperations:StartSession(): " + ex.Message, "TopDogError", "StartSession", "", ex);
            }
            return null;
        }
        private static AddClientResponse AddClient(BasketOperationsPortTypeClient basketClient, Quote quote, List<Swordfish.Businessobjects.Passengers.Passenger> passengers)
        {
            LogToDB("TopDogOperations:AddClient(): Started!!", "TopDogMessage", "AddClient");
            try
            {
                if (passengers != null && passengers.Count > 0)
                {
                    AddClientRequest addClientRequest = new AddClientRequest();
                    addClientRequest.SessionId = sessionID;
                    addClientRequest.FirstName = passengers[0].FirstName;
                    addClientRequest.Initials = "";
                    addClientRequest.Surname = passengers[0].LastName;
                    addClientRequest.Title = passengers[0].Title;
                    addClientRequest.Addresses = new ClientAddress[] { new ClientAddress(), };
                    addClientRequest.Addresses[0].Type = quote.AddressType;
                    addClientRequest.Addresses[0].Address1 = passengers[0].Address1;
                    addClientRequest.Addresses[0].Address2 = passengers[0].Address2;
                    addClientRequest.Addresses[0].HouseNumber = passengers[0].HouseNumber;
                    addClientRequest.Addresses[0].Country = passengers[0].Country;
                    addClientRequest.Addresses[0].PostCode = passengers[0].PostCode;
                    addClientRequest.Emails = new ClientEmailType[] { new ClientEmailType(), };
                    addClientRequest.Emails[0].Value = passengers[0].Email;
                    addClientRequest.Emails[0].Type = quote.EmailType;
                    if (Convert.ToBoolean(quote.quoteHeader.SentToEmail))
                        addClientRequest.Emails[0].AgreedToReceiveMarketingEmails = "YES";
                    else
                        addClientRequest.Emails[0].AgreedToReceiveMarketingEmails = "NO";

                    if (!string.IsNullOrEmpty(passengers[0].Telephone))
                    {
                        var phones = new ClientPhoneType[2];
                        ClientPhoneType phone = new ClientPhoneType();
                        phone.Value = passengers[0].Telephone;
                        phone.Type = "HOMEP";
                        phones[0] = phone;
                        ClientPhoneType mobile = new ClientPhoneType();
                        mobile.Value = passengers[0].Mobile;
                        mobile.Type = quote.PhoneType;
                        if (Convert.ToBoolean(quote.quoteHeader.SentToSms))
                            mobile.AgreedToReceiveMarketingSms = "YES";
                        else
                            mobile.AgreedToReceiveMarketingSms = "NO";
                        phones[1] = mobile;
                        addClientRequest.Phones = phones;
                    }
                    else
                    {
                        var phones = new ClientPhoneType[1];
                        ClientPhoneType mobile = new ClientPhoneType();
                        mobile.Value = passengers[0].Mobile;
                        mobile.Type = quote.PhoneType;
                        if (Convert.ToBoolean(quote.quoteHeader.SentToSms))
                            mobile.AgreedToReceiveMarketingSms = "YES";
                        else
                            mobile.AgreedToReceiveMarketingSms = "NO";
                        phones[0] = mobile;
                        addClientRequest.Phones = phones;
                    }
                    LogToDB("TopDogOperations:AddClient(): Started!!", "TopDogMessage:AddClientRequest", "AddClient", JsonConvert.SerializeObject(addClientRequest));
                    var addClientRequestResponse = basketClient.AddClient(addClientRequest);
                    LogToDB("TopDogOperations:AddClient(): Ended!!", "TopDogMessage:AddClientResponse", "AddClient", JsonConvert.SerializeObject(addClientRequestResponse));
                    return addClientRequestResponse;
                }
            }
            catch (Exception ex)
            {
                LogTopDogResponse("Agent", ex.Message, true, true);
                LogToDB("TopDogOperations:AddClient(): " + ex.Message, "TopDogError", "AddClient", "", ex);
            }
            return null;
        }
        private static LinkPassengersToProductResponse LinkPassengerToProduct(BasketOperationsPortTypeClient basketClient, ExternalProductReservationResponse externalProductReservationresponse, AddPassengersResponse passengerResponse)
        {
            LogToDB("TopDogOperations:LinkPassengerToProduct(): Started!!", "TopDogMessage", "LinkPassengerToProduct");
            LinkPassengersToProductRequest linkPtoPR = new LinkPassengersToProductRequest();
            try
            {
                linkPtoPR.SessionId = sessionID;
                linkPtoPR.BookCode = externalProductReservationresponse.BookCode;
                PassengerLink[] passengersLink = new PassengerLink[passengerResponse.Passengers.Count()];

                for (int i = 0; i < passengerResponse.Passengers.Count(); i++)
                {
                    PassengerLink passengerLink = new PassengerLink();
                    passengerLink.PassengerId = passengerResponse.Passengers[i].PassengerId;
                    passengerLink.LeadPassenger = passengerResponse.Passengers[i].BasketLeadPassenger;
                    passengerLink.PersonAgeCode = passengerResponse.Passengers[i].PersonAgeCode;

                    //if (i == 0)
                    //{
                    //    PassengerLinkSpecificProductLnk AddBaggage = new PassengerLinkSpecificProductLnk();
                    //    PassengerLinkSpecificProductLnkFlightProdLnk supplimentBaggage = new PassengerLinkSpecificProductLnkFlightProdLnk();
                    //    supplimentBaggage.Baggage = "2P";
                    //    AddBaggage.FlightProdLnk = supplimentBaggage;
                    //    passengerLink.SpecificProductLnk = AddBaggage;
                    //}
                    passengersLink[i] = passengerLink;
                }
                linkPtoPR.PassengerRefs = passengersLink;
                LogToDB("TopDogOperations:LinkPassengerToProduct(): Started!!", "TopDogMessage:LinkPassengersToProductRequest", "LinkPassengerToProduct", JsonConvert.SerializeObject(linkPtoPR));
                var linkResponse = basketClient.LinkPassengersToProduct(linkPtoPR);
                LogToDB("TopDogOperations:LinkPassengerToProduct(): Ended!!", "TopDogMessage:LinkPassengersToProductResponse", "LinkPassengerToProduct", JsonConvert.SerializeObject(linkPtoPR));
                return linkResponse;
            }
            catch (Exception ex)
            {
                LogTopDogResponse("Link passenger", ex.Message, true, true);
                LogToDB("TopDogOperations:LinkPassengerToProduct(): " + ex.Message, "TopDogError", "LinkPassengerToProduct", "", ex);
            }
            return null;
        }
        private static AddExtChargesResponse AddExtraChargeSInfo(BasketOperationsPortTypeClient basketClient, List<ExtraCharge> ExtraCharges, Quote quote, List<Businessobjects.Passengers.Passenger> passengers)
        {
            LogToDB("TopDogOperations:AddExtraChargeSInfo(): Started!!", "TopDogMessage", "AddExtraChargeSInfo");
            AddExtChargesRequest addExternalCharge = new AddExtChargesRequest();

            if (ExtraCharges.Count > 0)
            {
                ExtCharge[] Extcharges = new ExtCharge[ExtraCharges.Count];
                for (int i = 0; i < ExtraCharges.Count; i++)
                {
                    ExtCharge Charges = new ExtCharge();
                    ChargePrice Chargesprice = new ChargePrice();
                    ChargePrice ChargesCost = new ChargePrice();

                    addExternalCharge.SessionId = sessionID;

                    Charges.Code = ExtraCharges[i].EC_CD.ToString();
                    Chargesprice.CurrencyCode = quote.Currency;
                    Chargesprice.Price = (float)ExtraCharges[i].EC_Price;
                    Chargesprice.PriceSpecified = true;
                    Chargesprice.Rate = (float)ExtraCharges[i].EC_Price_Rate;
                    Chargesprice.RateSpecified = true;
                    if (ExtraCharges[i].EC_Price_Rule.ToString() == "F")
                    {
                        Chargesprice.Rule = HowRuleType.FLAT;
                    }
                    else
                    {
                        Chargesprice.Rule = HowRuleType.PERCENT;
                    }
                    Chargesprice.RuleSpecified = true;
                    Charges.Price = Chargesprice;

                    ChargesCost.CurrencyCode = quote.Currency;
                    ChargesCost.Price = (float)ExtraCharges[i].EC_Cost;
                    ChargesCost.PriceSpecified = true;
                    ChargesCost.Rate = (float)ExtraCharges[i].EC_Cost_Rate;
                    ChargesCost.RateSpecified = true;
                    if (ExtraCharges[i].EC_Cost_Rule.ToString() == "F")
                    {
                        ChargesCost.Rule = HowRuleType.FLAT;
                    }
                    else
                    {
                        ChargesCost.Rule = HowRuleType.PERCENT;
                    }
                    ChargesCost.RuleSpecified = true;
                    Charges.Cost = ChargesCost;

                    if (ExtraCharges[i].EC_PerPerson.ToString() == "Y")
                    {
                        Charges.PerPerson = true;
                    }
                    else
                    {
                        Charges.PerPerson = false;
                    }

                    Charges.Quantity = passengers.Count;
                    Charges.Description = ExtraCharges[i].EC_Dtl.ToString();
                    Charges.ApplyRule = ExtraCharges[i].EC_AppRule.ToString();
                    Charges.Excluded = false;
                    Charges.ClientCommissionRate = 0;
                    Charges.ClientCommissionRateSpecified = false;
                    if (ExtraCharges[i].EC_AppRule.ToString() == "Y")
                        Charges.Hidden = true;
                    else
                        Charges.Hidden = false;

                    Extcharges[i] = Charges;
                }
                addExternalCharge.Charges = Extcharges;
            }
            LogToDB("TopDogOperations:AddExtraChargeSInfo(): Ended!!", "TopDogMessage:AddExtChargesRequest", "AddExtraChargeSInfo", JsonConvert.SerializeObject(addExternalCharge));
            var AddExtChargesRes = basketClient.AddExternalCharges(addExternalCharge);
            LogToDB("TopDogOperations:AddExtraChargeSInfo(): Ended!!", "TopDogMessage:AddExtChargesResponse", "AddExtraChargeSInfo", JsonConvert.SerializeObject(AddExtChargesRes));
            return AddExtChargesRes;
        }
        private static AddExtChargesResponse AddExtraChargesInfo(BasketOperationsPortTypeClient basketClient, List<ExtraCharge> ExtraCharges, Quote quote, List<Businessobjects.Passengers.Passenger> passengers, List<PaymentDetail> payments)
        {
            LogToDB("TopDogOperations:AddExtraChargeSInfo(): Started!!", "TopDogMessage", "AddExtraChargeSInfo");
            AddExtChargesRequest addExternalCharge = new AddExtChargesRequest();
            List<string> lstCharges = new List<string>();
            addExternalCharge.SessionId = sessionID;
            if (quote.quoteHeader.FidelityCost > 0)
                lstCharges.Add("Fidelity");
            if (quote.quoteHeader.MarginHotel > 0)
                lstCharges.Add("Hotel");
            //   if (payments.Count > 0)
            //      lstCharges.Add("Payment");
            if (lstCharges.Count > 0)
            {
                ExtCharge[] Extcharges = new ExtCharge[lstCharges.Count];
                for (int i = 0; i < lstCharges.Count; i++)
                {
                    ExtCharge Charges = new ExtCharge();
                    if (lstCharges[i] == "Fidelity")
                        Charges = Extracharge(lstCharges[i], quote);
                    if (lstCharges[i] == "Hotel")
                        Charges = Extracharge(lstCharges[i], quote);
                    if (lstCharges[i] == "Payment")
                        Charges = Extracharge(lstCharges[i], quote, payments.Sum(x => Convert.ToDecimal(x.surcharge) > 0 ? Convert.ToDecimal(x.surcharge) : x.cardCharges));
                    Extcharges[i] = Charges;
                }
                addExternalCharge.Charges = Extcharges;
            }
            LogToDB("TopDogOperations:AddExtraChargeSInfo(): Ended!!", "TopDogMessage:AddExtChargesRequest", "AddExtraChargeSInfo", JsonConvert.SerializeObject(addExternalCharge));
            var AddExtChargesRes = basketClient.AddExternalCharges(addExternalCharge);
            LogToDB("TopDogOperations:AddExtraChargeSInfo(): Ended!!", "TopDogMessage:AddExtChargesResponse", "AddExtraChargeSInfo", JsonConvert.SerializeObject(AddExtChargesRes));
            return AddExtChargesRes;
        }

        private static ExtCharge Extracharge(string chargeType, Quote quote, decimal paymentCharge = 0)
        {
            ExtCharge extCharge = new ExtCharge();
            ChargePrice chargesPrice = new ChargePrice();
            ChargePrice chargesCost = new ChargePrice();
            switch (chargeType)
            {
                case "Fidelity":
                    extCharge.Code = "MISCELLANEOUS";
                    extCharge.Description = "Fidelity Charge Truly Fidelity Charge";
                    chargesPrice = GetChargePrice(quote.Currency, 0);
                    chargesCost = GetChargePrice(quote.Currency, quote.quoteHeader.FidelityCost);
                    break;
                case "Hotel":
                    extCharge.Code = "NSP";
                    extCharge.Description = "New Selling Price Teletext Holidays";
                    chargesPrice = GetChargePrice(quote.Currency, quote.quoteHeader.MarginHotel);
                    chargesCost = GetChargePrice(quote.Currency, 0);
                    break;
                case "Payment":
                    extCharge.Code = "PC";
                    extCharge.Description = "Payment Charges";
                    chargesPrice = GetChargePrice(quote.Currency, paymentCharge);
                    chargesCost = GetChargePrice(quote.Currency, paymentCharge);
                    break;
            }
            extCharge.Price = chargesPrice;
            extCharge.Cost = chargesCost;
            extCharge.ApplyRule = "PB";
            extCharge.Excluded = false;
            extCharge.ClientCommissionRate = 0;
            return extCharge;
        }

        private static ChargePrice GetChargePrice(string currencyCode, decimal price)
        {
            ChargePrice chargePrice = new ChargePrice();
            chargePrice.CurrencyCode = currencyCode;
            chargePrice.Price = (float)price;
            chargePrice.PriceSpecified = true;
            chargePrice.Rule = HowRuleType.FLAT;
            chargePrice.RuleSpecified = true;
            return chargePrice;
        }
        private static AddExtraBasketInfoResponse AddExtraBasketInfo(BasketOperationsPortTypeClient basketClient, Quote quote, List<PaymentDetail> payments)
        {
            LogToDB("TopDogOperations:AddExtraBasketInfo(): Started!!", "TopDogMessage", "AddExtraBasketInfo");

            //////////////////////AddExtraBasketInfoRequest///////////////////////////////////////////////////////

            AddExtraBasketInfoRequest addExtraBasketInfoRequest = new AddExtraBasketInfoRequest();
            addExtraBasketInfoRequest.SessionId = sessionID;
            //addExtraBasketInfoRequest.BookingReference = DateTime.Now.ToString("yyyyMMddHHmmss");
            addExtraBasketInfoRequest.CreationDateTime = DateTime.Now.ToString("yyyy-MM-ddTHH:mm:ss");//TimeZoneInfo.ConvertTime(DateTime.Now, TimeZoneInfo.Local, TimeZoneInfo.FindSystemTimeZoneById("GMT Standard Time")).ToString("yyyy-MM-ddTHH:mm:ss"); 
            addExtraBasketInfoRequest.AffiliateReference = quote.quoteHeader.CustomerReferenceCode;
            addExtraBasketInfoRequest.InternalNotes = quote.quoteHeader.InternalNotes;
            addExtraBasketInfoRequest.MediaCode = "SF";
            //addExtraBasketInfoRequest.ThirdPartyReference = quote.quoteHeader.CustomerReferenceCode;
            //addExtraBasketInfoRequest.InstallmentPlanApplied = true;
            //addExtraBasketInfoRequest.InstallmentPlanAppliedSpecified = true;
            if (payments != null && payments.Count > 0 && payments[0].depositType.ToUpper() == "DEPOSIT")
            {
                addExtraBasketInfoRequest.Deposit = (float)payments.Sum(p => p.cost);
                addExtraBasketInfoRequest.DepositDue = DateTime.Now.ToString("yyyy-MM-ddTHH:mm:ss");
                addExtraBasketInfoRequest.DepositSpecified = true;
                addExtraBasketInfoRequest.BalanceDue = Convert.ToDateTime(payments.Select(x => x.depositDuedate).FirstOrDefault()).ToString("yyyy-MM-ddTHH:mm:ss");// quote.searchModel.startDate.ToString("yyyy-MM-ddTHH:mm:ss");
            }
            else
            {
                addExtraBasketInfoRequest.Deposit = 0;
                addExtraBasketInfoRequest.DepositDue = DateTime.Now.ToString("yyyy-MM-ddTHH:mm:ss");
                addExtraBasketInfoRequest.DepositSpecified = false;
                addExtraBasketInfoRequest.BalanceDue = DateTime.Now.ToString("yyyy-MM-ddTHH:mm:ss");
            }
            addExtraBasketInfoRequest.LoadedByCode = TopdogId;
            addExtraBasketInfoRequest.LoadedByCodeSpecified = true;
            addExtraBasketInfoRequest.BookedByCode = TopdogId;
            addExtraBasketInfoRequest.BookedByCodeSpecified = true;

            //addExtraBasketInfoRequest.LowDeposit = (float)payments.Sum(p => p.cost);
            //addExtraBasketInfoRequest.LowDepositDue = quote.searchModel.startDate.ToString("yyyy-MM-ddTHH:mm:ss");

            //addExtraBasketInfoRequest.OverriddenPaymentPlans = new OverriddenPaymentPlan[] { new OverriddenPaymentPlan(), new OverriddenPaymentPlan() };
            //addExtraBasketInfoRequest.OverriddenPaymentPlans[0].Amount = (float)payments.Sum(p => p.cost);
            //addExtraBasketInfoRequest.OverriddenPaymentPlans[0].PaymentDate = DateTime.Now.ToString("yyyy-MM-dd");


            //addExtraBasketInfoRequest.OverriddenPaymentPlans[1].Amount =(float)(1595.86)- (float)payments.Sum(p => p.cost);
            //addExtraBasketInfoRequest.OverriddenPaymentPlans[1].PaymentDate = DateTime.Now.AddDays(10).ToString("yyyy-MM-dd");

            // addExtraBasketInfoRequest.InstallmentPlanApplied = true;
            // addExtraBasketInfoRequest.InstallmentPlanAppliedSpecified = true;
            //addExtraBasketInfoRequest.OverriddenPaymentPlans[0].PaymentDate = "2015-09-29";
            //addExtraBasketInfoRequest.OverriddenPaymentPlans[1].Amount = 100;
            //addExtraBasketInfoRequest.OverriddenPaymentPlans[1].PaymentDate = "2015-09-30";

            // addExtraBasketInfoRequest.InstallmentPlanApplied = true;
            // addExtraBasketInfoRequest.InstallmentPlanAppliedSpecified = true;
            //   addExtraBasketInfoRequest.OverriddenPaymentPlans[1].PaymentDate = "2015-09-30";
            LogToDB("TopDogOperations:AddExtraBasketInfo(): Started!!", "TopDogMessage:AddExtraBasketInfoRequest", "AddExtraBasketInfo", JsonConvert.SerializeObject(addExtraBasketInfoRequest));
            var addExtraBasketInfoResponse = basketClient.AddExtraBasketInfo(addExtraBasketInfoRequest);
            LogToDB("TopDogOperations:AddExtraBasketInfo(): Started!!", "TopDogMessage:AddExtraBasketInfoResponse", "AddExtraBasketInfo", JsonConvert.SerializeObject(addExtraBasketInfoResponse));
            return addExtraBasketInfoResponse;
        }

        private static AddPaymentResponse AddPayment(Quote quote, PaymentDetail payment, List<Businessobjects.Passengers.Passenger> passengers)
        {
            LogToDB("TopDogOperations:AddPayment(): Started!!", "TopDogMessage", "AddPayment");
            TopDogBlackBox.BlackBoxPortTypeClient blackBox = null;
            TopDogBlackBox.StartSessionResponse sessionResponse = null;
            bool succeeded = false;
            string topDogError = string.Empty;
            Swordfish.Components.TopDogBasketService.AddPaymentResponse addPaymentRequestResponse = null;

            for (int index = 0; index < 3 && !succeeded; index++)
            {
                try
                {
                    blackBox = new TopDogBlackBox.BlackBoxPortTypeClient();
                    sessionResponse = StartSession(blackBox, quote);
                    LogTopDogResponse("StartSession", "Session created successfully.", false, false, sessionResponse.SessionId);
                    TopDogBasketService.BasketOperationsPortTypeClient basketClient = new TopDogBasketService.BasketOperationsPortTypeClient();
                    GetBasketRequest getBasketRequest = new GetBasketRequest();
                    getBasketRequest.SessionId = sessionResponse.SessionId;
                    getBasketRequest.BookingReference = refNum;
                    getBasketRequest.ClientSurname = surName;
                    getBasketRequest.DepartDate = departureDate;
                    LogToDB("TopDogOperations:GetMyBasket(): Started!!", "TopDogMessage:GetBasketRequest", "GetMyBasket", JsonConvert.SerializeObject(getBasketRequest));
                    var getBasketResponse = basketClient.GetBasket(getBasketRequest);
                    LogToDB("TopDogOperations:GetMyBasket(): Started!!", "TopDogMessage:GetBasketResponse", "GetMyBasket", JsonConvert.SerializeObject(getBasketResponse));

                    sessionID = sessionResponse.SessionId;
                    AddPaymentRequest addPaymentRequest = new AddPaymentRequest();
                    addPaymentRequest.SessionId = sessionID;
                    addPaymentRequest.Amount = (float)payment.cost;
                    addPaymentRequest.PaymentType = TopDogBasketService.PaymentType.CREDIT_CARD;
                    addPaymentRequest.PaymentReference = "SFPayRefNo" + DateTime.Now.ToString("yyyyMMddHHmmss");//"SFPayRefNo" + quote.quoteHeader.CustomerReferenceCode + payment.transactionId;//ToDo
                    addPaymentRequest.PaymentDate = DateTime.Now.ToString("yyyy-MM-ddTHH:mm:ss");
                    addPaymentRequest.AmountSpecified = true;
                    addPaymentRequest.AuthorisationCode = payment.authCode;
                    addPaymentRequest.Notes = string.Empty;
                    addPaymentRequest.Manual = new TopDogBasketService.BoolWrapper();
                    addPaymentRequest.Manual.Value = true;
                    CreditCard cc = new CreditCard();
                    cc.CardNumber = string.IsNullOrEmpty(payment.pan) ? quote.CCNumber : payment.pan.Replace('*', '1');
                    cc.CardType = payment.cardTypeCode;
                    cc.ExpireDate = payment.cardExpiry.Substring(0, 2) + payment.cardExpiry.Substring(payment.cardExpiry.Length - 2, 2);
                    cc.NameOnCard = payment.name;
                    cc.SecurityNumber = quote.SecurityNum;

                    CardDetails cd = new CardDetails();
                    cd.CardHolder = new CardHolder();
                    cd.CardHolder.AddressLine1 = payment.address;
                    cd.CardHolder.AddressLine2 = payment.address;
                    cd.CardHolder.City = payment.city;
                    cd.CardHolder.Country = payment.country;
                    cd.CardHolder.EmailAddress = passengers[0].Email;
                    cd.CardHolder.FirstName = payment.firstName;
                    cd.CardHolder.Surname = payment.lastName;
                    cd.CardHolder.Title = passengers[0].Title;
                    cd.CardHolder.Initials = "";
                    cd.CardHolder.PhoneNumber = passengers[0].Mobile;
                    cd.CardHolder.PostCode = payment.postCode;
                    cd.CardHolder.SpokenToCardHolder = new TopDogBasketService.BoolWrapper() { Value = true };
                    cd.CardHolder.SendMeInvoice = new TopDogBasketService.BoolWrapper() { Value = true };
                    cd.CardHolder.SendMeTickets = new TopDogBasketService.BoolWrapper() { Value = true };
                    cd.CreditCard = cc;
                    addPaymentRequest.CardDetails = cd;
                    LogToDB("TopDogOperations:AddPayment(): Started!!", "TopDogMessage:AddPaymentRequest", "AddPayment", JsonConvert.SerializeObject(addPaymentRequest));
                    addPaymentRequestResponse = basketClient.AddPayment(addPaymentRequest);
                    LogToDB("TopDogOperations:AddPayment(): Started!!", "TopDogMessage:AddPaymentResponse", "AddPayment", JsonConvert.SerializeObject(addPaymentRequestResponse));
                    var addPaymentResponseStatus = addPaymentRequestResponse.Status;

                    if (addPaymentRequestResponse.Errors == null)
                    {
                        //StartBookingProcess                        
                        TopDogBasketService.StartBookingProcessRequest1 startBookingProcessRequest1 = new TopDogBasketService.StartBookingProcessRequest1();
                        TopDogBasketService.StartBookingProcessRequest startBookingProcessRequest = new TopDogBasketService.StartBookingProcessRequest();
                        startBookingProcessRequest.SessionId = sessionID;
                        startBookingProcessRequest1.StartBookingProcessRequest = startBookingProcessRequest;
                        TopDogBasketService.StartBookingProcessResponse startBookingProcessResponse = basketClient.StartBookingProcess(startBookingProcessRequest);

                        //ContinueBookingProcess
                        string continueBookingProcessedMessage = string.Empty;
                        TopDogBasketService.ContinueBookingProcessRequest1 continueBookingProcessRequest1 = new TopDogBasketService.ContinueBookingProcessRequest1();
                        TopDogBasketService.ContinueBookingProcessRequest continueBookingProcessRequest = new TopDogBasketService.ContinueBookingProcessRequest();
                        continueBookingProcessRequest.SessionId = sessionID;
                        continueBookingProcessRequest1.ContinueBookingProcessRequest = continueBookingProcessRequest;

                        for (int index1 = 0; !continueBookingProcessedMessage.Contains("All operations are completed") && index1 < 150; index1++)
                        {
                            TopDogBasketService.ContinueBookingProcessResponse continueBookingProcessResponse = basketClient.ContinueBookingProcess(continueBookingProcessRequest);
                            continueBookingProcessedMessage = continueBookingProcessResponse.BookingProcessStatus.StatusMessage;
                            System.Threading.Thread.Sleep(1000);
                        }

                        if (!continueBookingProcessedMessage.Contains("All operations are completed"))
                        {
                            topDogError = continueBookingProcessedMessage;
                        }

                        //FinishBookingProcess                        
                        string finishBookingProcessedMessage = string.Empty;
                        TopDogBasketService.FinishBookingProcessRequest1 finishBookingProcessRequest1 = new TopDogBasketService.FinishBookingProcessRequest1();
                        TopDogBasketService.FinishBookingProcessRequest finishBookingProcessRequest = new TopDogBasketService.FinishBookingProcessRequest();
                        finishBookingProcessRequest.SessionId = sessionID;
                        finishBookingProcessRequest1.FinishBookingProcessRequest = finishBookingProcessRequest;

                        TopDogBasketService.FinishBookingProcessResponse finishBookingProcessResponse = null;
                        finishBookingProcessResponse = basketClient.FinishBookingProcess(finishBookingProcessRequest);
                        finishBookingProcessedMessage = finishBookingProcessResponse.BookingProcessStatus.StatusMessage;

                        if (finishBookingProcessedMessage.Contains("Booking process is finished"))
                        {
                            succeeded = true;
                            topDogError = "Successfully Posted";
                        }
                        else
                        {
                            topDogError = finishBookingProcessedMessage;
                        }
                    }
                    else
                    {
                        topDogError = addPaymentRequestResponse.Errors != null && addPaymentRequestResponse.Errors.Count() > 0 ? string.Join(",", addPaymentRequestResponse.Errors) : "Failed Posting to TopDog";
                    }

                    index++;
                }
                catch (Exception ex)
                {
                    topDogError = ex.InnerException != null ? ex.InnerException.ToString() : ex.Message;
                }
                finally
                {
                    if (!string.IsNullOrEmpty(sessionID))
                    {
                        TopDogBlackBox.EndSessionRequest1 endSessionRequest1 = new TopDogBlackBox.EndSessionRequest1();
                        TopDogBlackBox.EndSessionRequest endSessionRequest = new TopDogBlackBox.EndSessionRequest();
                        endSessionRequest.SessionId = sessionID;
                        endSessionRequest1.EndSessionRequest = endSessionRequest;
                        var endSessionResponse = blackBox.EndSession(endSessionRequest1);
                        var sessionIDEND = endSessionResponse.EndSessionResponse.SessionId;
                    }
                }
            }

            LogToDB("TopDogOperations:AddPayment(): End.", "TopDogMessage:AddPaymentResponse", "AddPayment", JsonConvert.SerializeObject(addPaymentRequestResponse));
            return addPaymentRequestResponse;
        }
        private static StartBookingProcessResponse StartBooking(BasketOperationsPortTypeClient basketClient)
        {
            LogToDB("TopDogOperations:StartBooking(): Started!!", "TopDogMessage", "StartBooking");
            try
            {
                StartBookingProcessRequest startBookingProcessRequest = new StartBookingProcessRequest();
                startBookingProcessRequest.SessionId = sessionID;
                LogToDB("TopDogOperations:StartBooking(): Started!!", "TopDogMessage:StartBookingProcessRequest", "StartBooking", JsonConvert.SerializeObject(startBookingProcessRequest));
                var startBookProcessResponse = basketClient.StartBookingProcess(startBookingProcessRequest);
                LogToDB("TopDogOperations:StartBooking(): Ended!!", "TopDogMessage:StartBookingProcessResponse", "StartBooking", JsonConvert.SerializeObject(startBookProcessResponse));
                return startBookProcessResponse;
            }
            catch (Exception ex)
            {
                LogTopDogResponse("Start Booking", ex.Message, false, true);
                LogToDB("TopDogOperations:StartBooking(): " + ex.Message, "TopDogError", "StartBooking", "", ex);
            }
            return null;
        }
        private static FinishBookingProcessResponse FinishBooking(BasketOperationsPortTypeClient basketClient)
        {
            LogToDB("TopDogOperations:FinishBooking(): Started!!", "TopDogMessage", "FinishBooking");
            try
            {
                FinishBookingProcessRequest finishBookingProcessRequest = new FinishBookingProcessRequest();
                finishBookingProcessRequest.SessionId = sessionID;
                LogToDB("TopDogOperations:FinishBooking(): Started!!", "TopDogMessage:FinishBookingProcessRequest", "FinishBooking", JsonConvert.SerializeObject(finishBookingProcessRequest));
                var finishBookProcessResponse = basketClient.FinishBookingProcess(finishBookingProcessRequest);
                LogToDB("TopDogOperations:FinishBooking(): Ended!!", "TopDogMessage:FinishBookingProcessResponse", "FinishBooking", JsonConvert.SerializeObject(finishBookProcessResponse));
                return finishBookProcessResponse;
            }
            catch (Exception ex)
            {
                LogTopDogResponse("Finish Booking", ex.Message, false, true);
                LogToDB("TopDogOperations:FinishBooking(): " + ex.Message, "TopDogError", "FinishBooking");
            }
            return null;
        }
        private static GetBasketResponse GetMyBasket(BasketOperationsPortTypeClient basketClient)
        {
            LogToDB("TopDogOperations:GetMyBasket(): Started!!", "TopDogMessage", "GetMyBasket");
            try
            {
                GetBasketRequest getBasketRequest = new GetBasketRequest();
                getBasketRequest.SessionId = sessionID;
                LogToDB("TopDogOperations:GetMyBasket(): Started!!", "TopDogMessage:GetBasketRequest", "GetMyBasket", JsonConvert.SerializeObject(getBasketRequest));
                var getBasketResponse = basketClient.GetBasket(getBasketRequest);
                LogToDB("TopDogOperations:GetMyBasket(): Started!!", "TopDogMessage:GetBasketResponse", "GetMyBasket", JsonConvert.SerializeObject(getBasketResponse));
                return getBasketResponse;
            }
            catch (Exception ex)
            {
                LogTopDogResponse("TopDog Basket", ex.Message, true, true);
                LogToDB("TopDogOperations:GetMyBasket(): " + ex.Message, "TopDogError", "GetMyBasket", "", ex);
            }
            return null;
        }

        private static GetBasketResponse GetMyBasket(string sessionId)
        {
            LogToDB("TopDogOperations:GetMyBasket(): Started!!", "TopDogMessage", "GetMyBasket");
            try
            {
                TopDogBasketService.BasketOperationsPortTypeClient basketClient = new TopDogBasketService.BasketOperationsPortTypeClient();
                GetBasketRequest getBasketRequest = new GetBasketRequest();
                getBasketRequest.SessionId = sessionID;
                getBasketRequest.BookingReference = refNum;
                getBasketRequest.ClientSurname = surName;
                getBasketRequest.DepartDate = departureDate;
                LogToDB("TopDogOperations:GetMyBasket(): Started!!", "TopDogMessage:GetBasketRequest", "GetMyBasket", JsonConvert.SerializeObject(getBasketRequest));
                var getBasketResponse = basketClient.GetBasket(getBasketRequest);
                LogToDB("TopDogOperations:GetMyBasket(): Started!!", "TopDogMessage:GetBasketResponse", "GetMyBasket", JsonConvert.SerializeObject(getBasketResponse));
                return getBasketResponse;
            }
            catch (Exception ex)
            {
                LogTopDogResponse("TopDog Basket", ex.Message, true, true);
                LogToDB("TopDogOperations:GetMyBasket(): " + ex.Message, "TopDogError", "GetMyBasket", "", ex);
            }
            return null;
        }
        public static Task WriteTopDogLog(string LogType, string LogMessage)
        {
            try
            {
                Task.Run(async () =>
                {
                    await Utilities.ELKLog.Logger.Log(LogType, LogMessage);
                }).ConfigureAwait(false);
                LogToDB(LogMessage, "TopDogMessage");
            }
            catch (Exception ex)
            {
                LogToDB(LogMessage, "TopDogError", "", "", ex);
            }

            return Task.Delay(1);
        }
        private static void LogToDB(string Message, string LogType, string functionName = "", string payload = "", Exception ex = null)
        {
            string message = string.Empty;
            if (!string.IsNullOrEmpty(Message))
                message = Message + "," + GetExceptionMessage(ex);
            else
                message = GetExceptionMessage(ex);
            Utilities.ELKLog.DataAccess.SaveSFLog(LogType, userId, userSessionID, Environment, System.Threading.Thread.CurrentThread.ManagedThreadId.ToString(), functionName, message, payload.Replace("'", "''"), GetStackTrace(ex), userId, clientIP);
        }
        private static string GetExceptionMessage(Exception ex)
        {
            string retValue = string.Empty;
            if (ex != null)
            {
                StringBuilder stringBuilder = new StringBuilder();
                stringBuilder.Append(ex.Message);
                if (ex.InnerException != null)
                {
                    stringBuilder.Append(ex.InnerException.Message);
                }
                retValue = stringBuilder.ToString();
            }
            return retValue.Replace("'", "");
        }
        private static string GetStackTrace(Exception ex)
        {
            string retVal = string.Empty;
            if (ex != null)
            {
                StringBuilder stringBuilder = new StringBuilder();
                stringBuilder.Append(ex.StackTrace);
                if (ex.InnerException != null)
                {
                    stringBuilder.Append(ex.InnerException.StackTrace);
                }
                retVal = stringBuilder.ToString();
            }
            return retVal;
        }

        private static List<TopDogSupplierMapping> GetTopDogSupplierMapping(string SourceSystemCode, int RecordTypeOptionId)
        {
            List<TopDogSupplierMapping> supplierMapping = new List<TopDogSupplierMapping>();
            try
            {
                var db = new CommandRunner(ConfigurationManager.ConnectionStrings["chilliDataBase"].ConnectionString.ToString());
                string sql = "select * from get_topdog_supplier_mapping('" + SourceSystemCode + "'," + RecordTypeOptionId + " ) ";
                var res = db.ExecuteDynamic(sql);
                foreach (var record in res)
                {
                    if (record.get_topdog_supplier_mapping != null)
                    {
                        supplierMapping = JsonConvert.DeserializeObject<List<TopDogSupplierMapping>>(record.get_topdog_supplier_mapping);
                    }
                }
            }
            catch (Exception ex)
            {
                LogTopDogResponse("Mapping", ex.Message, true, true);
                LogToDB("TopDogOperations:GetTopDogSupplierMapping(): " + ex.Message, "TopDogError", "GetTopDogSupplierMapping", "", ex);
            }
            return supplierMapping;
        }

        private static string GetCountryCode(string CountryName)
        {
            string result = string.Empty;
            try
            {
                var db = new CommandRunner(ConfigurationManager.ConnectionStrings["chilliDataBase"].ConnectionString.ToString());
                string sql = "select * from get_country_code('" + CountryName + "')";
                var res = db.ExecuteDynamic(sql);
                foreach (var record in res)
                {
                    if (!string.IsNullOrEmpty(record.get_country_code))
                    {
                        result = record.get_country_code;
                    }
                }
            }
            catch (Exception ex)
            {
                LogTopDogResponse("Mapping", ex.Message, true, true);
                LogToDB("TopDogOperations:GetTopDogSupplierMapping(): " + ex.Message, "TopDogError", "GetTopDogSupplierMapping", "", ex);
            }
            return result;
        }
    }
}
