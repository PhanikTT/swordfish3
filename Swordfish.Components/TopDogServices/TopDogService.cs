﻿using System;
using System.Collections.Generic;
using Swordfish.Businessobjects.Membership;
using Swordfish.Businessobjects.Passengers;
using Swordfish.Businessobjects.Payments;
using Swordfish.Businessobjects.Quotes;
using Swordfish.Businessobjects.TopDog;

namespace Swordfish.Components.TopDogServices
{
    public class TopDogService : ITopDogService
    {
        public List<TopDogResponse> SaveDataInTopDog(Quote quote, List<Passenger> passengers, UserInfo agent, List<PaymentDetail> payment, List<ExtraCharge> extraCharge,string clientIp)
        {
            TopDogOperations topDogPeration = new TopDogOperations(clientIp);
            return topDogPeration.Start(quote, passengers, agent, payment, extraCharge);
        }
    }
}
