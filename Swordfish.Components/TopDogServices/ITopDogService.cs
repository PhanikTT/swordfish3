﻿using Swordfish.Businessobjects.TopDog;
using Swordfish.Businessobjects.Quotes;
using Swordfish.Businessobjects.Passengers;
using Swordfish.Businessobjects.Membership;
using Swordfish.Businessobjects.Payments;
using System.Collections.Generic;

namespace Swordfish.Components.TopDogServices
{
    public interface ITopDogService
    {
        List<TopDogResponse> SaveDataInTopDog(Quote quote, List<Passenger> passengers, UserInfo agent, List<PaymentDetail> payment, List<ExtraCharge> extraCharge, string clientIp);
    }
}
