﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Swordfish.Components.DataAccessLayer
{
   public class TopDogSupplierMapping
    {
        public string SourceSystemCode { get; set; }
        public string SourceSystemDescription { get; set; }
        public string SSCustomColumn1 { get; set; }
        public string SSCustomColumn2 { get; set; }
        public int RecordTypeOptionID { get; set; }
        public int SourceSystemOptionID { get; set; }
        public string MappedSystemCode { get; set; }
        public string MappedSystemDescription { get; set; }
        public string MSCustomColumn1 { get; set; }
        public string MSCustomColumn2 { get; set; }
        public int MappedSystemOptionID { get; set; }
    }
}
