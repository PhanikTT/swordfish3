﻿using Swordfish.Businessobjects.Customers;
using Swordfish.Businessobjects.Membership;
using Swordfish.Utilities.DataAccess;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Configuration;

namespace Swordfish.Components.MembershipServices.DataAccess
{
    public static class DataAccess
    {
        public static ConcurrentBag<GraphData> GetGraphData(int UserId, string currentdate)
        {

            ConcurrentBag<GraphData> graphdata = new ConcurrentBag<GraphData>();
            List<object> newdata = new List<object>();
            var db = new CommandRunner(ConfigurationManager.ConnectionStrings["chilliDataBase"].ConnectionString.ToString());
            string sql = "select * from get_agents_graphdata(" + UserId + ",date('" + currentdate + "'))";
            var res = db.ExecuteDynamic(sql);
            foreach (var record in res)
            {
                GraphData graphobj = new GraphData();
                graphobj.WeekDate = Convert.ToDateTime(record.paymentdate).ToString("MM/dd/yyyy");
                graphobj.Count = record.count.ToString();
                graphdata.Add(graphobj);
            }
            return graphdata;
        }

        public static ConcurrentBag<GraphData> GetGraphDataforfourWeeks(int UserId, string currentdate)
        {
            ConcurrentBag<GraphData> graphdataforfourweeks = new ConcurrentBag<GraphData>();
            var db = new CommandRunner(ConfigurationManager.ConnectionStrings["chilliDataBase"].ConnectionString.ToString());
            string sql = "select * from get_agents_graphdata_basedon_weeks(" + UserId + ",date('" + currentdate + "'))";
            var res = db.ExecuteDynamic(sql);
            foreach (var record in res)
            {
                GraphData graphobj = new GraphData();
                graphobj.Week1 = record.week1;
                graphobj.Week2 = record.week2;
                graphobj.Week3 = record.week3;
                graphobj.Week4 = record.week4;
                graphdataforfourweeks.Add(graphobj);
            }
            return graphdataforfourweeks;
        }
        public static ConcurrentBag<TodaysActions> InsertUpdateTodaysAction(TodaysActions todaysactions, string type)
        {

            ConcurrentBag<TodaysActions> todaysactiondata = new ConcurrentBag<TodaysActions>();
            List<object> todaysAction = new List<object>();
            List<object> newid = new List<object>();
            string actiontext = todaysactions.TaskText;
            int userid = todaysactions.UserId;
            int actionstatusid = todaysactions.TaskId;
            string actiondate = todaysactions.TodaysActionDate;
            int[] TodaysPKid = todaysactions.TaskPKeyid;
            if (todaysactions.remindersdate == null)
            {
                todaysactions.remindersdate = String.Format("{0:yyyy/M/d HH:mm:ss}", DateTime.Now);
            }
            if (type == "Insert")
            {
                var db = new CommandRunner(ConfigurationManager.ConnectionStrings["chilliDataBase"].ConnectionString.ToString());
                string sql = "select * from InsertUpdate_TodaysAction(null," + actionstatusid + "," + userid + ",'" + actiontext.Replace("'", "''") + "','" + actiondate + "',null,'" + type + "','" + todaysactions.remindersdate + "','" + todaysactions.reminder_status + "','" + todaysactions.Is_deleted + "','" + todaysactions.reminder + "')";
                var res = db.ExecuteDynamic(sql);
                foreach (var record in res)
                {

                    var exactdata = record.todaystext;
                    var id = record.todyasid;
                    newid.Add(id);
                    todaysAction.Add(exactdata);
                }

            }
            else
            {
                foreach (int Pk in TodaysPKid)
                {
                    var db = new CommandRunner(ConfigurationManager.ConnectionStrings["chilliDataBase"].ConnectionString.ToString());
                    string sql = "select * from InsertUpdate_TodaysAction(" + Pk + ",null,null,null,null,'" + actiondate + "','" + type + "','" + todaysactions.remindersdate + "','" + todaysactions.reminder_status + "','" + todaysactions.Is_deleted + "','" + todaysactions.reminder + "')";
                    var res = db.ExecuteDynamic(sql);
                    foreach (var record in res)
                    {

                        var exactdata = record.todaystext;
                        var id = record.todyasid;
                        newid.Add(id);
                        todaysAction.Add(exactdata);
                    }

                }
            }
            todaysactiondata = GetTodaysActionData(userid);
            return todaysactiondata;
        }
        public static ConcurrentBag<TodaysActions> GetTodaysActionData(int UserId)
        {
            ConcurrentBag<TodaysActions> todaysactiondata = new ConcurrentBag<TodaysActions>();
            var db = new CommandRunner(ConfigurationManager.ConnectionStrings["chilliDataBase"].ConnectionString.ToString());
            string sql = "select * from get_todayactiondata(" + UserId + ")";
            var res = db.ExecuteDynamic(sql);
            foreach (var record in res)
            {
                TodaysActions todaysdata = new TodaysActions();
                todaysdata.TaskText = record.todaystext;
                todaysdata.TaskPrimaryKeyid = record.todyasid;
                todaysdata.remindersdate = record.remindersdate;
                todaysdata.reminder = Convert.ToInt32(record.reminders);
                todaysactiondata.Add(todaysdata);
            }
            return todaysactiondata;
        }

        public static IncomingCustomer InsertIncomingCustomerData(IncomingCustomer customerdata)
        {
            IncomingCustomer Incustomerdata = new IncomingCustomer();
            int userid = customerdata.Userid;
            string quoteid = customerdata.QuoteId;
            string logindatetime = customerdata.LoginDateTime;
            string logoutdatetime = customerdata.LogoutDateTime;
            string firstname = customerdata.FirstName;
            string lastname = customerdata.LastName;
            string email_address = customerdata.EmailAddress;
            string phone_number = customerdata.PhoneNumber;
            string address = customerdata.Address;
            var db = new CommandRunner(ConfigurationManager.ConnectionStrings["chilliDataBase"].ConnectionString.ToString());
            string sql = "select * from insertCust_ChroniclesData(" + userid + ",'" + quoteid + "','" + logindatetime + "','" + logoutdatetime + "','" + firstname + "','" + lastname + "','" + email_address + "','" + phone_number + "','" + address + "')";
            var res = db.ExecuteDynamic(sql);
            foreach (var record in res)
            {
                Incustomerdata.CustomerId = Convert.ToInt32(record.customerid);
                Incustomerdata.Userid = Convert.ToInt32(record.user_id);
                Incustomerdata.QuoteId = record.quote_id.ToString();
                Incustomerdata.LoginDateTime = record.login_datetime.ToString();
                Incustomerdata.LogoutDateTime = record.logout_datetime.ToString();
                Incustomerdata.FirstName = record.first_name.ToString();
                Incustomerdata.LastName = record.last_name.ToString();
                Incustomerdata.EmailAddress = record.emailaddress.ToString();
                Incustomerdata.PhoneNumber = record.phonenumber.ToString();
                Incustomerdata.Address = record.cust_address.ToString();
            }
            return Incustomerdata;
        }
    }
}
