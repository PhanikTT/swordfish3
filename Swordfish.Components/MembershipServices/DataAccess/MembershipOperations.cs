﻿using Swordfish.Businessobjects.Membership;
using Swordfish.Utilities.DataAccess;
using Swordfish.Utilities.ELKLog;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Configuration;

namespace Swordfish.Components.MembershipServices.DataAccess
{
    public class MembershipOperations
    {
        public MembershipOperations()
        {

        }
        /// <summary>
        /// To Do: Creating the user
        /// </summary>
        /// <param name="user"></param>
        /// <returns>integer</returns>
        public int CreateAgent(Agent agent)
        {
            return CreateNewUser(agent);
        }

        /// <summary>
        /// To Do: Validating the user
        /// </summary>
        /// <param name="user"></param>
        /// <returns>int</returns>
        public int ValidateUser(LoginUser user)
        {
            return CheckUserValidate(user);
        }

        /// <summary>
        /// To Do: Forgot Email Request
        /// </summary>
        /// <param name="email"></param>
        /// <returns>int</returns>
        public int ForgotPassword(string email)
        {
            return CheckIsValidUser(email);
        }

        /// <summary>
        /// To Do: Reset Password
        /// </summary>
        /// <param name="password"></param>
        public bool ResetPassword(UserResetPassword password)
        {
            return ResetUserPassword(password);
        }

        /// <summary>
        /// To Do: CRUD Operations on Role Table
        /// </summary>
        /// <param name="role"></param>
        /// <returns>int</returns>
        public int RoleOperations(Roles role)
        {
            return RoleCrud(role);
        }

        /// <summary>
        /// TO DO : Get logged in user is exists in the given role or not
        /// </summary>
        /// <param name="userid"></param>
        /// <param name="role"></param>
        /// <returns>bool</returns>
        public bool IsUserInRole(string userid, string role)
        {
            return CheckUserIsInRole(userid, role);
        }

        /// <summary>
        /// TO Do : Get Logged in user details
        /// </summary>
        /// <param name="username"></param>
        /// <returns>UserInfo Object</returns>
        public UserInfo GetUserDetails(string username)
        {
            return GetUserInfo(username);
        }

        /// <summary>
        ///  To Do: Get all roles in the database
        /// </summary>
        /// <returns>roles List</returns>
        public ConcurrentBag<Roles> GetAllRoles()
        {
            return GetRoles();
        }

        /// <summary>
        /// To Do: Get user assigned all roles
        /// </summary>
        /// <param name="username"></param>
        /// <returns>list of roles</returns>
        public IEnumerable<string> GetAllUserAssignedRoles(string username)
        {
            return GetAssignedRolesByUsername(username);
        }
        /// <summary>
        /// To Do: Enable the reset password option to the user
        /// </summary>
        /// <param name="email"></param>
        /// <returns>bool</returns>
        public bool EnableUserResetPassword(string email, bool isEnable)
        {
            return EnableResetPasswordToUser(email, isEnable);
        }

        /// <summary>
        /// To Do: Checking the reset password option enabled
        /// </summary>
        /// <param name="email"></param>
        /// <returns>bool</returns>
        public bool IsEnableResetPasswordToUser(string email)
        {
            return CheckEnableResetPasswordToUser(email);
        }

        public bool UpdateAgentLoginDate(int userId)
        {
            return updateLoginDateForAgent(userId);
        }

        private bool updateLoginDateForAgent(int userId)
        {

            bool result = false;
            try
            {
                var db = new CommandRunner(ConfigurationManager.ConnectionStrings["MembershipDataBase"].ConnectionString.ToString());
                string sql = "select * from f_update_agentlastLogin(" + userId + ") as status";
                var res = db.ExecuteDynamic(sql);

                foreach (var x in res)
                {
                    result = x.status;
                }
            }
            catch (Exception ex)
            {
                Logger.Log("Error", ex.Message.ToString());
            }

            return result;

        }

        private bool CheckEnableResetPasswordToUser(string email)
        {
            bool result = false;

            try
            {
                var db = new CommandRunner(ConfigurationManager.ConnectionStrings["MembershipDataBase"].ConnectionString.ToString());
                string sql = "select * from check_user_reset_password_link_enable('" + email + "') as enable_status";
                var res = db.ExecuteDynamic(sql);

                foreach (var x in res)
                {
                    result = x.enable_status;
                }
            }
            catch (Exception ex)
            {
                Logger.Log("Error", ex.Message.ToString());
            }

            return result;
        }

        private bool EnableResetPasswordToUser(string email, bool isEnable)
        {
            bool result = false;
            string valEnable = isEnable == true ? "1" : "0";

            try
            {
                var db = new CommandRunner(ConfigurationManager.ConnectionStrings["MembershipDataBase"].ConnectionString.ToString());
                string sql = "select * from enable_user_reset_password_link('" + email + "','" + valEnable + "') as enable_status";
                var res = db.ExecuteDynamic(sql);

                foreach (var x in res)
                {
                    result = x.enable_status;
                }
            }
            catch (Exception ex)
            {
                Logger.Log("Error", ex.Message.ToString());
            }

            return result;
        }

        private IEnumerable<string> GetAssignedRolesByUsername(string username)
        {
            List<string> rolesList = new List<string>();

            try
            {
                var db = new CommandRunner(ConfigurationManager.ConnectionStrings["MembershipDataBase"].ConnectionString.ToString());
                string sql = "select * from get_assigned_roles_by_username('" + username + "') as role_name";
                var res = db.ExecuteDynamic(sql);

                foreach (var x in res)
                {
                    rolesList.Add(x.role_name);
                }
            }
            catch (Exception ex)
            {
                Logger.Log("Error", ex.Message.ToString());
            }

            return rolesList;
        }

        private ConcurrentBag<Roles> GetRoles()
        {
            ConcurrentBag<Roles> allRoles = new ConcurrentBag<Roles>();

            try
            {
                var db = new CommandRunner(ConfigurationManager.ConnectionStrings["MembershipDataBase"].ConnectionString.ToString());
                string sql = "select * from get_all_roles()";
                var res = db.ExecuteDynamic(sql);

                foreach (var x in res)
                {
                    Roles role = new Roles();
                    role.RoleId = Convert.ToInt32(x.role_id);
                    role.RoleName = Convert.ToString(x.role_name);
                    role.Description = x.role_description == null ? null : Convert.ToString(x.role_description);
                    role.CreatedBy = Convert.ToInt32(x.created_by);
                    role.CreatedOn = Convert.ToDateTime(x.created_on);
                    role.UpdatedBy = x.updated_by == null ? null : Convert.ToInt32(x.updated_by);
                    role.UpdatedOn = x.updated_on == null ? null : Convert.ToDateTime(x.updated_on);

                    allRoles.Add(role);
                }
            }
            catch (Exception ex)
            {
                Logger.Log("Error", ex.Message.ToString());
            }

            return allRoles;
        }

        private UserInfo GetUserInfo(string username)
        {
            UserInfo user = new UserInfo();

            try
            {
                var db = new CommandRunner(ConfigurationManager.ConnectionStrings["MembershipDataBase"].ConnectionString.ToString());
                string sql = "select * from get_user_details('" + username + "')";
                var res = db.ExecuteDynamic(sql);

                foreach (var data in res)
                {
                    user.UserId = Convert.ToInt32(data.userid);
                    user.Username = Convert.ToString(data.username);
                    user.UserEmailAddress = Convert.ToString(data.email);
                    user.TeamId = Convert.ToInt32(data.teamid);
                    user.FirstName = data.firstname == null ? null : Convert.ToString(data.firstname);
                    user.LastName = data.lastname == null ? null : Convert.ToString(data.lastname);
                    user.ValidFrom = Convert.ToDateTime(data.validfrom);
                    user.CreatedBy = Convert.ToInt32(data.createdby);
                    user.CreatedOn = Convert.ToDateTime(data.createdon);
                    user.UpdatedBy = data.updatedby == null ? null : Convert.ToInt32(data.updatedby);
                    user.UpdatedOn = data.updatedon == null ? null : Convert.ToDateTime(data.updatedon);
                    user.LastLoginDate = data.last_login_date == null ? null : Convert.ToDateTime(data.last_login_date);
                    user.TeamName = data.teamname == null ? null : Convert.ToString(data.teamname);
                    user.TierName = data.tiercode == null ? null : Convert.ToString(data.tiercode);
                    user.TierDescription = data.tierdescription == null ? null : Convert.ToString(data.tierdescription);
                    user.Imagepath = data.imagepath == null ? null : Convert.ToString(data.imagepath);
                    user.MobileNumber = data.mobile_no == null ? null : Convert.ToString(data.mobile_no);
                    user.MarginFlightMin = data.margin_flight_min == null ? 0 : Convert.ToInt32(data.margin_flight_min);
                    user.MarginFlightMax = data.margin_flight_max == null ? 0 : Convert.ToInt32(data.margin_flight_max);
                    user.MarginHotelMin = data.margin_hotel_min == null ? 0 : Convert.ToInt32(data.margin_hotel_min);
                    user.MarginHotelMax = data.margin_hotel_max == null ? 0 : Convert.ToInt32(data.margin_hotel_max);
                    user.MarginExtraMin = data.margin_extra_min == null ? 0 : Convert.ToInt32(data.margin_extra_min);
                    user.MarginExtraMax = data.margin_extra_max == null ? 0 : Convert.ToInt32(data.margin_extra_max);
                    user.Initial = Convert.ToString(data.initial);
                    user.Title = Convert.ToString(data.title);
                    user.AddressType = Convert.ToString(data.address_type);
                    user.Address1 = Convert.ToString(data.address1);
                    user.Address2 = Convert.ToString(data.address2);
                    user.Address3 = Convert.ToString(data.address3);
                    user.State = Convert.ToString(data.state);
                    user.Country = Convert.ToString(data.country);
                    user.PostCode = Convert.ToString(data.postcode);
                    user.EmailType = Convert.ToString(data.email_type);
                    user.PhoneType = Convert.ToString(data.phone_type);
                    user.Phone = Convert.ToString(data.phone);
                    user.SyntechAccountId = data.syntec_account_id;
                    user.TopDogId = data.topdog_id;
                    // Getting UserAssignedRoles
                    List<UserRoles> userInRoles = new List<UserRoles>();
                    userInRoles = GetUserAssignedRoles(user.UserId);

                    user.UserAssignedRoles = userInRoles;
                }
            }
            catch (Exception ex)
            {
                Logger.Log("Error", ex.Message.ToString());
            }

            return user;
        }

        private List<UserRoles> GetUserAssignedRoles(int UserId)
        {
            List<UserRoles> userInRoles = new List<UserRoles>();

            try
            {
                var db = new CommandRunner(ConfigurationManager.ConnectionStrings["MembershipDataBase"].ConnectionString.ToString());
                string sql = "select * from get_user_assigned_roles('" + UserId + "')";
                var res = db.ExecuteDynamic(sql);

                foreach (var x in res)
                {
                    userInRoles.Add(new UserRoles { UserId = UserId, RoleId = Convert.ToInt32(x.role_id) });
                }
            }
            catch (Exception ex)
            {
                Logger.Log("Error", ex.Message.ToString());
            }

            return userInRoles;
        }

        private bool CheckUserIsInRole(string userid, string role)
        {
            bool result = false;

            try
            {
                var db = new CommandRunner(ConfigurationManager.ConnectionStrings["MembershipDataBase"].ConnectionString.ToString());
                string sql = "select * from is_user_in_role('" + userid + "','" + role + "') as rolestatus";
                var res = db.ExecuteDynamic(sql);

                foreach (var x in res)
                {
                    result = Convert.ToBoolean(x.rolestatus);
                }
            }
            catch (Exception ex)
            {
                Logger.Log("Error", ex.Message.ToString());
            }

            return result;
        }

        private bool ResetUserPassword(UserResetPassword password)
        {
            bool result = false;

            try
            {
                var db = new CommandRunner(ConfigurationManager.ConnectionStrings["MembershipDataBase"].ConnectionString.ToString());
                string sql = "select * from reset_user_password('" + password.Username + "','" + password.NewPassword + "') as changestatus";
                var res = db.ExecuteDynamic(sql);

                foreach (var x in res)
                {
                    result = Convert.ToBoolean(x.changestatus);
                }
            }
            catch (Exception ex)
            {
                Logger.Log("Error", ex.Message.ToString());
            }

            return result;
        }

        private int RoleCrud(Roles role)
        {
            int result = 0;

            try
            {
                var db = new CommandRunner(ConfigurationManager.ConnectionStrings["MembershipDataBase"].ConnectionString.ToString());
                string sql = "select * from role_operations('" + role.RoleId + "','" + role.RoleName + "','" + role.Description + "','" + role.CreatedBy + "','" + role.CreatedOn + "','" + role.UpdatedBy + "','" + role.UpdatedOn + "','" + role.Action + "') as RoleStatus";
                var res = db.ExecuteDynamic(sql);

                foreach (var x in res)
                {
                    result = Convert.ToInt32(x.RoleStatus);
                }
            }
            catch (Exception ex)
            {
                Logger.Log("Error", ex.Message.ToString());
            }

            return result;
        }

        private int CheckIsValidUser(string email)
        {
            int result = 0;

            try
            {
                var db = new CommandRunner(ConfigurationManager.ConnectionStrings["MembershipDataBase"].ConnectionString.ToString());
                string sql = "select * from cu_check_user_exists('" + email + "') as userexists";
                var res = db.ExecuteDynamic(sql);

                foreach (var x in res)
                {
                    result = x.userexists;
                }
            }
            catch (Exception ex)
            {
                Logger.Log("Error", ex.Message.ToString());
            }

            return result;
        }

        // Connection string error
        // datatype conversione rror

        private int CreateNewUser(Agent agent)
        {
            int result = 0;

            try
            {
                var db = new CommandRunner(ConfigurationManager.ConnectionStrings["MembershipDataBase"].ConnectionString.ToString());
                string sql = "select * from create_new_customer_care_agent('" + agent.Email + "','" + agent.FirstName + "','" + agent.LastName + "','" + agent.Password + "','" + agent.CreatedBy + "','" + agent.syntecAccountID + "') as LoginStatus";
                var res = db.ExecuteDynamic(sql);

                foreach (var x in res)
                {
                    result = x.loginstatus;
                }
            }
            catch (Exception ex)
            {
                Logger.Log("Error", ex.Message.ToString());
            }

            return result;
        }

        private int CheckUserValidate(LoginUser user)
        {
            int result = 0;

            try
            {
                var db = new CommandRunner(ConfigurationManager.ConnectionStrings["MembershipDataBase"].ConnectionString.ToString());
                string sql = "select * from cu_user_validate('" + user.Username + "','" + user.Password + "') as LoginStatus";
                var res = db.ExecuteDynamic(sql);

                foreach (var x in res)
                {
                    result = x.loginstatus;
                }
            }
            catch (Exception ex)
            {
                Logger.Log("Error", ex.Message.ToString());
            }

            return result;
        }
    }
}
