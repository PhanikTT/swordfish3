﻿using Swordfish.Businessobjects.Customers;
using Swordfish.Businessobjects.Membership;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Swordfish.Components.MembershipServices
{
    public interface IMembershipService
    {
        int CreateAgent(Agent agent);
        int ValidateUser(LoginUser user);
        int ForgotPassword(string email);
        bool ResetPassword(UserResetPassword password);
        int RoleOperations(Roles role);
        bool IsUserInRole(string userid, string role);
        UserInfo GetUserDetails(string username);
        ConcurrentBag<Roles> GetAllRoles();
        IEnumerable<string> GetUserAssignedRoles(string username);
        ConcurrentBag<GraphData> GetGraphData(int UserId, string graphDate);
        ConcurrentBag<GraphData> GetGraphDataforfourWeeks(int UserId, string date);
        ConcurrentBag<TodaysActions> InsertUpdateTodaysAction(TodaysActions todaysaction, string type);
        ConcurrentBag<TodaysActions> GetTodaysActionData(int UserId);
        bool EnableUserResetPassword(string email, bool isEnable);
        bool IsEnableResetPasswordToUser(string email);
        IncomingCustomer InsertIncomingCustomerData(IncomingCustomer customerdata);
        bool UpdateAgentLoginDate(int userId);
    }
}
