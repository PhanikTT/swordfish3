﻿using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Threading.Tasks;
using Swordfish.Businessobjects.Membership;
using Swordfish.Components.MembershipServices.BusinessManagers;
using Swordfish.Businessobjects.Customers;

namespace Swordfish.Components.MembershipServices
{
    public class MembershipService : IMembershipService
    {
        public int CreateAgent(Agent agent)
        {
            BusinessManager bmLayer = new BusinessManager();
            return bmLayer.CreateAgent(agent);
        }

        public int ValidateUser(LoginUser user)
        {
            BusinessManager bmLayer = new BusinessManager();
            return bmLayer.ValidateUser(user);
        }

        public int ForgotPassword(string email)
        {
            BusinessManager bmLayer = new BusinessManager();
            return bmLayer.ForgotPassword(email);
        }

        public bool ResetPassword(UserResetPassword password)
        {
            BusinessManager bmLayer = new BusinessManager();
            return bmLayer.ResetPassword(password);
        }

        public bool IsEnableResetPasswordToUser(string email)
        {
            BusinessManager bmLayer = new BusinessManager();
            return bmLayer.IsEnableResetPasswordToUser(email);
        }

        public int RoleOperations(Roles role)
        {
            BusinessManager bmLayer = new BusinessManager();
            return bmLayer.RoleOperations(role);
        }

        public bool IsUserInRole(string userid, string role)
        {
            BusinessManager bmLayer = new BusinessManager();
            return bmLayer.IsUserInRole(userid, role);
        }

        public UserInfo GetUserDetails(string username)
        {
            BusinessManager bmLayer = new BusinessManager();
            return bmLayer.GetUserDetails(username);
        }

        public ConcurrentBag<Roles> GetAllRoles()
        {
            BusinessManager bmLayer = new BusinessManager();
            return bmLayer.GetAllRoles();
        }

        public IEnumerable<string> GetUserAssignedRoles(string username)
        {
            BusinessManager bmLayer = new BusinessManager();
            return bmLayer.GetUserAssignedRoles(username);
        }

        public ConcurrentBag<GraphData> GetGraphData(int UserId, string date)
        {
            BusinessManager bmLayer = new BusinessManager();
            return bmLayer.GetGraphData(UserId, date);
        }

        public ConcurrentBag<GraphData> GetGraphDataforfourWeeks(int UserId, string date)
        {
            BusinessManager bmLayer = new BusinessManager();
            return bmLayer.GetGraphDataforfourWeeks(UserId, date);
        }

        public ConcurrentBag<TodaysActions> GetTodaysActionData(int UserId)
        {
            BusinessManager bmLayer = new BusinessManager();
            return bmLayer.GetTodaysActionData(UserId);
        }

        public ConcurrentBag<TodaysActions> InsertUpdateTodaysAction(TodaysActions todaysaction, string type)
        {
            BusinessManager bmLayer = new BusinessManager();
            return bmLayer.InsertUpdateTodaysAction(todaysaction, type);
        }
        public IncomingCustomer InsertIncomingCustomerData(IncomingCustomer customerdata)
        {
            BusinessManager bmLayer = new BusinessManager();
            return bmLayer.InsertIncomingCustomerData(customerdata);
        }

        public bool EnableUserResetPassword(string email, bool isEnable)
        {
            BusinessManager bmLayer = new BusinessManager();
            return bmLayer.EnableUserResetPassword(email, isEnable);
        }

        public bool UpdateAgentLoginDate(int userId)
        {
            BusinessManager bmLayer = new BusinessManager();
            return bmLayer.UpdateAgentLoginDate(userId);
        }
    }
}
