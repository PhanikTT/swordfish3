﻿using Swordfish.Businessobjects.Customers;
using Swordfish.Businessobjects.Membership;
using System.Collections.Concurrent;
using System.Threading.Tasks;

namespace Swordfish.Components.MembershipServices.BusinessManagers
{
    public class TodaysActionDetails
    {
        public Task<ConcurrentBag<TodaysActions>> InsertUpdateTodaysAction(TodaysActions todaysaction, string type)
        {
            return Task.FromResult(DataAccess.DataAccess.InsertUpdateTodaysAction(todaysaction, type));
        }

        public Task<IncomingCustomer> InsertIncomingCustomerData(IncomingCustomer customerdata)
        {
            return Task.FromResult(DataAccess.DataAccess.InsertIncomingCustomerData(customerdata));
        }

        public ConcurrentBag<TodaysActions> GetTodaysActionData(int UserId)
        {
            return DataAccess.DataAccess.GetTodaysActionData(UserId);
        }

    }
}