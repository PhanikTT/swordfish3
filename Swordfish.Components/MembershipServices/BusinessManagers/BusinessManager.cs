﻿using Swordfish.Businessobjects.Customers;
using Swordfish.Businessobjects.Membership;
using Swordfish.Components.MembershipServices.DataAccess;
using Swordfish.Utilities.ELKLog;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Swordfish.Components.MembershipServices.BusinessManagers
{
    public class BusinessManager
    {
        public int CreateAgent(Agent agent)
        {
            int result = 0;

            try
            {
                MembershipOperations memberOperation = new MembershipOperations();
                result = memberOperation.CreateAgent(agent);
            }
            catch (Exception ex)
            {
                Logger.Log("Error", ex.Message.ToString());
            }

            return result;
        }

        public int ValidateUser(LoginUser user)
        {
            int result = 0;

            try
            {
                MembershipOperations memberOperation = new MembershipOperations();
                result = memberOperation.ValidateUser(user);
            }
            catch (Exception ex)
            {
                Logger.Log("Error", ex.Message.ToString());
            }

            return result;
        }

        public int ForgotPassword(string email)
        {
            int result = 0;

            try
            {
                MembershipOperations memberOperation = new MembershipOperations();
                result = memberOperation.ForgotPassword(email);
            }
            catch (Exception ex)
            {
                Logger.Log("Error", ex.Message.ToString());
            }

            return result;
        }

        public bool ResetPassword(UserResetPassword password)
        {
            bool result = false;
            try
            {
                MembershipOperations memberOperation = new MembershipOperations();
                result = memberOperation.ResetPassword(password);
            }
            catch (Exception ex)
            {
                Logger.Log("Error", ex.Message.ToString());
            }

            return result;
        }

        public int RoleOperations(Roles role)
        {
            int result = 0;

            try
            {
                MembershipOperations memberOperation = new MembershipOperations();
                result = memberOperation.RoleOperations(role);
            }
            catch (Exception ex)
            {
                Logger.Log("Error", ex.Message.ToString());
            }

            return result;

        }

        public bool IsUserInRole(string userid, string role)
        {
            bool result = false;

            try
            {
                MembershipOperations memberOperation = new MembershipOperations();
                result = memberOperation.IsUserInRole(userid, role);
            }
            catch (Exception ex)
            {
                Logger.Log("Error", ex.Message.ToString());
            }

            return result;
        }

        public UserInfo GetUserDetails(string username)
        {
            UserInfo user = new UserInfo();

            try
            {
                MembershipOperations memberOperation = new MembershipOperations();
                user = memberOperation.GetUserDetails(username);
            }
            catch (Exception ex)
            {
                Logger.Log("Error", ex.Message.ToString());
            }

            return user;
        }

        public ConcurrentBag<Roles> GetAllRoles()
        {
            ConcurrentBag<Roles> allroles = null;

            try
            {
                MembershipOperations memberOperation = new MembershipOperations();
                allroles = memberOperation.GetAllRoles();
            }
            catch (Exception ex)
            {
                Logger.Log("Error", ex.Message.ToString());
            }

            return allroles;
        }

        public IEnumerable<string> GetUserAssignedRoles(string username)
        {
            IEnumerable<string> userroles = null;

            try
            {
                MembershipOperations memberOperation = new MembershipOperations();
                userroles = memberOperation.GetAllUserAssignedRoles(username);
            }
            catch (Exception ex)
            {
                Logger.Log("Error", ex.Message.ToString());
            }

            return userroles;
        }


        public ConcurrentBag<GraphData> GetGraphData(int UserId, string currentdate)
        {
            var graphdata = new ConcurrentBag<GraphData>();

            try
            {
                TeamPerformance teamperformance = new TeamPerformance();
                graphdata = Task.Run(() => teamperformance.GetGraphData(UserId, currentdate)).Result;

            }
            catch (Exception ex)
            {
                Logger.Log("Error", ex.Message.ToString());
            }
            ConcurrentBag<GraphData> g = new ConcurrentBag<GraphData>(graphdata);

            return g;
        }
        public ConcurrentBag<GraphData> GetGraphDataforfourWeeks(int UserId, string currentdate)
        {
            ConcurrentBag<GraphData> graphdata = new ConcurrentBag<GraphData>();

            try
            {
                TeamPerformance teamperformance = new TeamPerformance();
                graphdata = Task.Run(() => teamperformance.GetGraphDataforfourWeeks(UserId, currentdate)).Result;

            }
            catch (Exception ex)
            {
                Logger.Log("Error", ex.Message.ToString());
            }
            ConcurrentBag<GraphData> g = new ConcurrentBag<GraphData>(graphdata);

            return g;

        }

        public ConcurrentBag<TodaysActions> InsertUpdateTodaysAction(TodaysActions todaysaction, string type)
        {
            ConcurrentBag<TodaysActions> todaysactiondata = new ConcurrentBag<TodaysActions>();

            try
            {
                TodaysActionDetails todaysactions = new TodaysActionDetails();
                todaysactiondata = Task.Run(() => todaysactions.InsertUpdateTodaysAction(todaysaction, type)).Result;
            }
            catch (Exception ex)
            {
                Logger.Log("Error", ex.Message.ToString());
            }
            ConcurrentBag<TodaysActions> g = new ConcurrentBag<TodaysActions>(todaysactiondata);

            return g;
        }

        public IncomingCustomer InsertIncomingCustomerData(IncomingCustomer customerdata)
        {
            IncomingCustomer CustomerData = new IncomingCustomer();
            try
            {
                TodaysActionDetails todaysactions = new TodaysActionDetails();
                CustomerData = Task.Run(() => todaysactions.InsertIncomingCustomerData(customerdata)).Result;
            }
            catch (Exception ex)
            {
                Logger.Log("Error", ex.Message.ToString());
            }

            return CustomerData;
        }

        public ConcurrentBag<TodaysActions> GetTodaysActionData(int UserId)
        {
            ConcurrentBag<TodaysActions> todaysactiondata = new ConcurrentBag<TodaysActions>();

            try
            {
                TodaysActionDetails todaysactions = new TodaysActionDetails();
                todaysactiondata = todaysactions.GetTodaysActionData(UserId);

            }
            catch (Exception ex)
            {
                Logger.Log("Error", ex.Message.ToString());
            }
            ConcurrentBag<TodaysActions> g = new ConcurrentBag<TodaysActions>(todaysactiondata);

            return g;

        }

        public bool EnableUserResetPassword(string email, bool isEnable)
        {
            bool result = false;

            try
            {
                MembershipOperations memberOperation = new MembershipOperations();
                result = memberOperation.EnableUserResetPassword(email, isEnable);
            }
            catch (Exception ex)
            {
                Logger.Log("Error", ex.Message.ToString());
            }

            return result;
        }

        public bool IsEnableResetPasswordToUser(string email)
        {
            bool result = false;

            try
            {
                MembershipOperations memberOperation = new MembershipOperations();
                result = memberOperation.IsEnableResetPasswordToUser(email);
            }
            catch (Exception ex)
            {
                Logger.Log("Error", ex.Message.ToString());
            }

            return result;
        }

        public bool UpdateAgentLoginDate(int userId)
        {
            bool result = false;

            try
            {
                MembershipOperations memberOperation = new MembershipOperations();
                result = memberOperation.UpdateAgentLoginDate(userId);
            }
            catch (Exception ex)
            {
                Logger.Log("Error", ex.Message.ToString());
            }

            return result;

        }
    }
}
