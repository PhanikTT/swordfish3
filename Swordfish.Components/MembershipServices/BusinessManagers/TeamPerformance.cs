﻿using Swordfish.Businessobjects.Membership;
using System.Collections.Concurrent;
using System.Threading.Tasks;

namespace Swordfish.Components.MembershipServices.BusinessManagers
{
    public class TeamPerformance
    {
        public Task<ConcurrentBag<GraphData>> GetGraphData(int UserId, string currentdate)
        {
            return Task.FromResult(DataAccess.DataAccess.GetGraphData(UserId, currentdate));
        }

        public Task<ConcurrentBag<GraphData>> GetGraphDataforfourWeeks(int UserId, string currentdate)
        {
            return Task.FromResult(DataAccess.DataAccess.GetGraphDataforfourWeeks(UserId, currentdate));
        }
    }
}