﻿using Swordfish.UI.Helpers;
using Swordfish.Utilities.ELKLog;
using System;
using System.Configuration;
using System.Linq;
using System.ServiceModel;
using System.Xml;
using System.Xml.Linq;

namespace HotelCodes
{
    class Program
    {
        static void Main(string[] args)
        {
            try
            {
                Swordfish.HotelCodes.BackGroundJobs.HotelCodes hotelCodes = new Swordfish.HotelCodes.BackGroundJobs.HotelCodes();
                hotelCodes.GetAllHotelCodeFromHC();
                Swordfish.HotelCodes.BackGroundJobs.ResortTree resortTree = new Swordfish.HotelCodes.BackGroundJobs.ResortTree();
                resortTree.GetAllResortListFromSesame();
                Swordfish.HotelCodes.BackGroundJobs.HotelList hotelList = new Swordfish.HotelCodes.BackGroundJobs.HotelList();
                hotelList.GetAllHotelListFromSesame();
               
               
            }
            catch (Exception EX)
            {
                Logger.Log("Error", "Stack Trace" + EX.StackTrace + " ;InnerException = " + EX.InnerException + " ;Source = " + EX.Source + " ;TargetSite =" + EX.TargetSite + " on date=" + DateTime.Now);

            }
        }
    }
}
