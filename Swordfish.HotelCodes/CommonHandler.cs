﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Text.RegularExpressions;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Swordfish.HotelCodes
{
   public class CommonHandler
    {
        public static string GetStaticPath()
        {
           return LoadKey("StaticData.Path");

        }
        public static void createDirectory(string path)
        {
            bool exists = Directory.Exists(path);
            if (!exists)
            {
                Directory.CreateDirectory(path);
            }
        }
        public static void createArchive(string Absolutepath,string fileName)
        {
            if (File.Exists(Absolutepath+ fileName))
            {           
            string archivePath = Absolutepath + "Archive\\" + DateTime.Now.Date.Day + "_" + DateTime.Now.Month + "_" + DateTime.Now.Year + "_" + DateTime.Now.Hour + "\\";
            CommonHandler.createDirectory(archivePath);
                if (!File.Exists(archivePath + fileName))
                {
                    File.Create(archivePath + fileName).Close();
                   
                }
                System.IO.File.Copy(Absolutepath + fileName, archivePath + fileName, true);

            }

        }
        public static string StripNSAndAlias(string responseXML)
        {
            string filter = @"xmlns(:\w+)?=""([^""]+)""|xsi(:\w+)?=""([^""]+)""";
            string s = Regex.Replace(responseXML, filter, "");
            return Regex.Replace(responseXML, filter, "");
        }
        public static string GetReleativePath()
        {
            string directory = string.Empty;
            if (Environment.CurrentDirectory.Contains("bin\\Debug"))
            {
                directory = Environment.CurrentDirectory.Replace("bin\\Debug", "");
            }
            else if (Environment.CurrentDirectory.Contains("bin\\Release"))
            {
                directory = Environment.CurrentDirectory.Replace("bin\\Release", "");
            }
            else
            {
                directory = Environment.CurrentDirectory;
            }
            return directory;

        }

        public static string HotelcodesPath()
        {
            return LoadKey("HotelCodes.Path");
        }
        public static string HotelListPath()
        {
            return LoadKey("HotelList.Path");
        }
        public static string ResortTreePath()
        {
            return LoadKey("ResortTree.Path");
        }

        public static bool GetAbsolute()
        {
            bool s = Convert.ToBoolean(LoadKey("Absolute"));
          return Convert.ToBoolean(LoadKey("Absolute"));
        }

        public static string HotelcodesFileName()
        {
            return LoadKey("Hotelcodes.FileName");

        }
        public static string HotelcodesURL()
        {
            return LoadKey("Hotelcodes.URL");

        }
        public static string ResortTreeFileName()
        {
            return LoadKey("ResortTree.FileName");

        }
        public static string ResortTreeXmlpath()
        {
            return LoadKey("ResortTreeXml.path");

        }
        public static string HotelListArchiveFileName()
        {
            return LoadKey("HotelListArchive.FileName");

        }
        public static string HotelListFileName()
        {
            return LoadKey("HotelList.FileName");

        }
        public static string HotelListRequestxmlpath()
        {
            return LoadKey("HotelListRequestxml.path");

        }
        public static string HotelListDestinationCodes()
        {
            return LoadKey("HotelList.DestinationCodes");

        }
        public static string LoadKey(string key)
        {
            return ConfigurationManager.AppSettings.Get(key).ToString();

        }
    }
}
