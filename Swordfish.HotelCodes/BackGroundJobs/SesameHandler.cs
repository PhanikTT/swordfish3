﻿using Swordfish.Utilities.ELKLog;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Net;
using System.Text;
using System.Xml;
using System.Web;
using Swordfish.UI.Helpers;

namespace Swordfish.HotelCodes.BackGroundJobs
{
    class SesameHandler
    {
       

        public string CallSesame(string requestXML, string sessionId = null)
        {
            string strResponseXML = string.Empty;
            string destinationUrl = string.Empty;
            destinationUrl = ConfigurationManager.AppSettings.Get("Sesame.Url").ToString(); ;
            SFLogger.logBackGroundjobsMessage("Calling service method in CallSesame and Destination url :" + destinationUrl);
            try
            {
                HttpWebRequest request = HttpWebRequest.Create(destinationUrl) as HttpWebRequest;
                request.Method = "POST";
                request.ContentType = "application/x-www-form-urlencoded; charset=UTF-8";
                Encoding encoding = new UTF8Encoding(false);
                XmlDocument xmlDOC = new XmlDocument();
                xmlDOC.LoadXml(requestXML.ToString());
                string rawXml = xmlDOC.OuterXml;
                string encodedXML = HttpUtility.UrlEncode(rawXml, encoding);
                string requestText = string.Format("request={0}", encodedXML);
                Stream requestStream = request.GetRequestStream();
                StreamWriter requestWriter = new StreamWriter(requestStream, encoding);
                requestWriter.Write(requestText);
                requestWriter.Close();
                HttpWebResponse response;
                response = (HttpWebResponse)request.GetResponse();
                Dictionary<string, string> _dictionary = new Dictionary<string, string>();
                Stream responseStream = response.GetResponseStream();
                var responseStr = new StreamReader(responseStream).ReadToEnd();
                strResponseXML = responseStr.ToString();
                if (response.StatusCode == HttpStatusCode.OK)
                {
                    strResponseXML = responseStr.ToString();
                    SFLogger.logBackGroundjobsMessage(string.Format("ProcessName ={0}\n Request={1};\n Response={2}", "callsesame", requestXML, strResponseXML));
                }
                else
                {
                    StringBuilder sbErrorMessage = new StringBuilder();
                    sbErrorMessage.Append("Status Code = " + response.StatusCode);
                    sbErrorMessage.Append(Environment.NewLine);
                    sbErrorMessage.Append("RequestXML = " + requestXML);
                    sbErrorMessage.Append(Environment.NewLine);
                    sbErrorMessage.Append("ResponseXML = " + responseStr.ToString());
                    SFLogger.logBackGroundjobsMessage("sbErrorMessage=" + sbErrorMessage.ToString());
                    throw new Exception(sbErrorMessage.ToString());
                }
            }
            catch (WebException ex)
            {
                if (ex.Response == null)
                    throw ex;
                var resp = new StreamReader(ex.Response.GetResponseStream()).ReadToEnd();
                System.Xml.XmlDocument doc = new System.Xml.XmlDocument();
                try
                {
                    doc.LoadXml(resp.ToString());
                }
                catch (Exception)
                {
                    SFLogger.logBackGroundjobsMessage("Error occured at CallService() for requestXml=" + requestXML + " ; and exception message:" + ex.InnerException != null ? ex.InnerException.Message : ex.Message);
                    throw ex;
                }
                // Root element
                System.Xml.XmlElement root = doc.DocumentElement;
                System.Xml.XmlElement conditie = (System.Xml.XmlElement)root.ChildNodes[0];
                string Message = root.ChildNodes[0].InnerText;
                string SessionId = root.ChildNodes[1].InnerText;
                string Backtrace = root.ChildNodes[2].InnerText;
                SFLogger.logBackGroundjobsMessage("Sesame response :" + resp + "; Sesame exception :" + Message);
                throw ex;

            }
            catch (Exception ex)
            {
                SFLogger.logBackGroundjobsMessage("Error occured at CallService() for requestXml=" + requestXML + " ; inner excetiion:and exception message:" + ex.InnerException != null ? ex.InnerException.Message : ex.Message);
                throw ex;
            }
            SFLogger.logBackGroundjobsMessage("RequestXml=" + requestXML + " ; and Response xml:" + strResponseXML);
            return strResponseXML;
        }
    }
}
