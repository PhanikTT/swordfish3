﻿using Swordfish.Utilities.ELKLog;
using System;
using Swordfish.UI.Helpers;
using System.Configuration;
using System.Linq;
using System.ServiceModel;
using System.Xml;
using System.Xml.Linq;

namespace Swordfish.HotelCodes.BackGroundJobs
{
    public class HotelCodes
    {
        public void GetAllHotelCodeFromHC()
        {
            try
            {
                SFLogger.logBackGroundjobsMessage("Fetching the hotel codes has started");
               string FileName = CommonHandler.HotelcodesFileName();
                string path = string.Empty;
                if (CommonHandler.GetAbsolute())
                    path = CommonHandler.HotelcodesPath();
                else
                    path = CommonHandler.GetReleativePath() + CommonHandler.GetStaticPath();
                CommonHandler.createDirectory(path);
                string HotelcodesURL = CommonHandler.HotelcodesURL();
                string filePath = path + FileName;
                CommonHandler.createArchive(path, FileName);
                BasicHttpBinding httpBinding = new BasicHttpBinding();
                httpBinding.MaxBufferSize = 2147483647;
                httpBinding.MaxReceivedMessageSize = 2147483647;
                EndpointAddress endPointAddress = new EndpointAddress(HotelcodesURL);
                StaticDataClient client = new StaticDataClient(httpBinding, endPointAddress); //new StaticDataClient("BasicHttpBinding_IStaticData");
                XElement xml = XElement.Parse(client.GetHotelIffCode(0).OuterXml); 
                xml.Save(filePath);
                SFLogger.logBackGroundjobsMessage("Fetching the hotel codes has ended , total number of records fetched" + xml.Elements().Count() + " on" + DateTime.Now+ "and codes is stored in Path :"+ path);
            }
            catch (Exception EX)
            {
                SFLogger.logBackGroundjobsMessage("Stack Trace" + EX.StackTrace + " ;InnerException = " + EX.InnerException + " ;Source = " + EX.Source + " ;TargetSite =" + EX.TargetSite + " on date=" + DateTime.Now);

            }
        }
    }
}