﻿using Swordfish.UI.Helpers;
using Swordfish.Utilities.ELKLog;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using System.Xml.Linq;

namespace Swordfish.HotelCodes.BackGroundJobs
{
    class ResortTree
    {
        public void GetAllResortListFromSesame()
        {
            try
            {
                SFLogger.logBackGroundjobsMessage("fetching the hotel codes has started on" + DateTime.Now);
                string FileName = CommonHandler.ResortTreeFileName();
                string path = string.Empty;
                if (CommonHandler.GetAbsolute())
                    path = CommonHandler.HotelcodesPath();
                else
                    path = CommonHandler.GetReleativePath() + CommonHandler.GetStaticPath();
                CommonHandler.createDirectory(path);
                string filePath = path + FileName;
                CommonHandler.createArchive(path, FileName);
                string ResortTreeRequestXML = CommonHandler.GetReleativePath() + CommonHandler.ResortTreeXmlpath();
                SesameHandler sesameHandler = new SesameHandler();
                string requestXML = File.ReadAllText(ResortTreeRequestXML);
                var response = sesameHandler.CallSesame(requestXML);
                XmlDocument xmldoc = new XmlDocument();                                
                xmldoc.LoadXml(CommonHandler.StripNSAndAlias(response));               
                xmldoc.Save(filePath);
                SFLogger.logBackGroundjobsMessage("Total number of records fetched " + xmldoc.ChildNodes[1].ChildNodes[0].ChildNodes.Count + " on" + DateTime.Now + "and codes are stored into Path :" + filePath);
               
            }
            catch (Exception EX)
            {
                SFLogger.logBackGroundjobsMessage("Stack Trace" + EX.StackTrace + " ;InnerException = " + EX.InnerException + " ;Source = " + EX.Source + " ;TargetSite =" + EX.TargetSite + " on date=" + DateTime.Now);

            }
        }
      


    }
}
