﻿using Swordfish.UI.Helpers;
using Swordfish.Utilities.ELKLog;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Xml;
using System.Xml.Linq;

namespace Swordfish.HotelCodes.BackGroundJobs
{
    public class HotelList
    {
        public void GetAllHotelListFromSesame()
        {
            try
            {
                SFLogger.logBackGroundjobsMessage("fetching the hotel codes has started on" + DateTime.Now);
               string FileName = CommonHandler.HotelListFileName();                
                string path = string.Empty;
                bool s = CommonHandler.GetAbsolute();
                if (CommonHandler.GetAbsolute())
                    path = CommonHandler.HotelcodesPath();
                else
                    path = CommonHandler.GetReleativePath()+ CommonHandler.GetStaticPath();
                CommonHandler.createDirectory(path);
                string Archive = CommonHandler.HotelListArchiveFileName();
                string filePath = path + FileName;
                string ArchivefilePath = path + Archive;
                CommonHandler.createArchive(path, FileName);
                
                if (!File.Exists(filePath))
                {
                    File.Create(filePath);
                }
                string HotelListRequestXML = CommonHandler.GetReleativePath() + CommonHandler.HotelListRequestxmlpath();
                string HotelList = CommonHandler.HotelListDestinationCodes();
                string[] list;
                list = HotelList.Split('|');
                string resortTreePath = path + CommonHandler.ResortTreeFileName();
                XmlDocument resorts = new XmlDocument();
                resorts.Load(resortTreePath);
                List<string> resortIds = new List<string>();
                foreach (XmlElement resort in resorts.ChildNodes[1].ChildNodes[0].ChildNodes)
                {
                    foreach (XmlElement resortid in resort.ChildNodes)
                    {
                        foreach (XmlAttribute attribute in resortid.Attributes)
                        {
                            if (attribute.Name == "SupplierResortId")
                            {
                                resortIds.Add(attribute.Value);
                                break;
                            }
                        }
                       
                    }
                }
                SesameHandler sesameHandler = new SesameHandler();
                string firstline = string.Empty;
                string Secondline = string.Empty;
                string Lastline = string.Empty;
                File.WriteAllText(filePath, "");
                foreach (var DestinationCode in resortIds)
                {
                    try
                    {
                        SFLogger.logBackGroundjobsMessage("fetching the hotel list has started on" + DateTime.Now + "and hotellist.destination code is " + DestinationCode);
                        string requestXML = string.Format(File.ReadAllText(HotelListRequestXML), DestinationCode);
                        var response = sesameHandler.CallSesame(requestXML);
                        XmlDocument xmldoc = new XmlDocument();                        
                        xmldoc.LoadXml(response);
                        xmldoc.Save(ArchivefilePath);
                        var archive = new List<string>(System.IO.File.ReadAllLines(ArchivefilePath));
                        if (archive.Count > 3)
                        {
                            firstline = archive.ElementAt(0);
                            Secondline = archive.ElementAt(1);
                            Lastline = archive.ElementAt(archive.Count - 1);
                            archive.RemoveAt(0);
                            archive.RemoveAt(0);
                            archive.RemoveAt(archive.Count - 1);
                            File.WriteAllText(ArchivefilePath, string.Empty);
                            File.AppendAllLines(filePath, archive.ToArray());
                            SFLogger.logBackGroundjobsMessage("Total number of records fetched " + archive.Count + " on" + DateTime.Now + "and codes are stored into Path :" + filePath + " With destination code " + DestinationCode);
                        }
                        else
                            SFLogger.logBackGroundjobsMessage("Total number of records fetched 0"  + " on" + DateTime.Now + " With destination code "+DestinationCode);
                        
                    }
                    catch (Exception Ex)
                    {
                        SFLogger.logBackGroundjobsMessage("Stack Trace" + Ex.StackTrace + " ;InnerException = " + Ex.InnerException + " ;Source = " + Ex.Source + " ;TargetSite =" + Ex.TargetSite + " on date=" + DateTime.Now);
                    }
                    SFLogger.logBackGroundjobsMessage("fetching the hotel codes has ended for destination code "+ DestinationCode);
                }
                var file= new List<string>(System.IO.File.ReadAllLines(filePath));
                file.Insert(0, CommonHandler.StripNSAndAlias(Secondline));
                file.Insert(0, firstline);
                file.Insert(file.Count, Lastline);
                File.WriteAllLines(filePath, file.ToArray());
            }
            catch (Exception EX)
            {
                SFLogger.logBackGroundjobsMessage("Stack Trace" + EX.StackTrace + " ;InnerException = " + EX.InnerException + " ;Source = " + EX.Source + " ;TargetSite =" + EX.TargetSite + " on date=" + DateTime.Now);

            }
        }
    }
}
