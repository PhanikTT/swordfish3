﻿using StackExchange.Redis;
using System;
using System.Configuration;

namespace Swordfish.Utilities.Cache
{
    public static class CacheRepository
    {
        static readonly ConnectionMultiplexer Redis = ConnectionMultiplexer.Connect(ConfigurationSettings.AppSettings["Cache.IP"]);
        static readonly IDatabase Db = Redis.GetDatabase();

        public static void SetData(string key, string data)
        {
            Db.StringSet(key, data);
            Db.KeyExpire(key, new TimeSpan(Convert.ToInt32(ConfigurationSettings.AppSettings["Cache.Hours"].ToString()), Convert.ToInt32(ConfigurationSettings.AppSettings["Cache.Minutes"].ToString()), Convert.ToInt32(ConfigurationSettings.AppSettings["Cache.Seconds"].ToString()))); //15 Min
        }

        public static void SetDataForever(string key, string data)
        {
            Db.StringSet(key, data);

        }
        public static string GetData(string key)
        {
            if (CheckKeyExists(key))
                return Db.StringGet(key);
            else
                return string.Empty;
        }
        public static bool CheckKeyExists(string key)
        {
            return Db.KeyExists(key);
        }

        public static bool RemoveKey(string key)
        { return Db.KeyDelete(key); }
    }
}
