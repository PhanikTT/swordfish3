﻿using System;

namespace Swordfish.Utilities.WEXCard
{
    public class CreateCardResponse
    {
        public string requestReference { get; set; }
        public string signature { get; set; }
        public string success { get; set; }
        public string resultCode { get; set; }
        public string message { get; set; }
        public string PAN { get; set; }
        public string cv2 { get; set; }
        public string expiryDate { get; set; }
        public string cardReference { get; set; }
        public string QuoteHeaderId { get; set; }
        public DateTime CreatedDate { get; set; }
    }
}
