﻿using Swordfish.Utilities.DataAccess;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Net;
using System.Security.Cryptography;
using System.Text;
using System.Xml;
using System.Xml.Linq;
using System.Xml.Serialization;

namespace Swordfish.Utilities.WEXCard
{
    public class CardCreator
    {
        #region Declarations

        private string finalSignature = string.Empty;
        private string signature = string.Empty;
        static readonly string password = "VZzvQHSw";
        private string hashStringWithRALN = string.Empty;
        private string hashStringWithPass = string.Empty;
        private GBP.createCardRequest GBPCardRequest = null;
        private EURO.createCard EUROCardRequest = null;
        private string connectionString = string.Empty;
        private string quoteHeaderId = string.Empty;
        private bool enableLogging = true;
        public string WEXURL = ConfigurationManager.AppSettings["WEX.URL"].ToString();

        #endregion
        /// <summary>
        /// Use to Get Wex Card
        /// </summary>
        /// <param name="connectionString"></param>
        public CardCreator(string connectionString)
        {
            this.connectionString = connectionString;
        }
        /// <summary>
        /// Creates Card Creator Object and store the requred data locally
        /// </summary>
        /// <param name="GBPCardRequest">request data to create GBP Card</param>
        /// <param name="connectionString">Database Connection string to log request and response objects</param>
        /// <param name="enableLogging">Enable or Disable Logging</param>
        public CardCreator(GBP.createCardRequest GBPCardRequest, string connectionString, string quoteHeaderId = null, bool enableLogging = true)
        {
            this.GBPCardRequest = GBPCardRequest;
            this.connectionString = connectionString;
            this.quoteHeaderId = quoteHeaderId;
            this.enableLogging = enableLogging;
        }

        /// <summary>
        /// Creates Card Creator Object and store the requred data locally
        /// </summary>
        /// <param name="EUROCardRequest">request data to create EURO Card</param>
        /// <param name="connectionString">Database Connection string to log request and response objects</param>
        /// <param name="enableLogging">Enable or Disable Logging</param>
        public CardCreator(EURO.createCard EUROCardRequest, string connectionString, string quoteHeaderId = null, bool enableLogging = true)
        {
            this.EUROCardRequest = EUROCardRequest;
            this.connectionString = connectionString;
            this.quoteHeaderId = quoteHeaderId;
            this.enableLogging = enableLogging;
        }
        /// <summary>
        /// Creates WEX Card - GBP
        /// </summary>
        public CreateCardResponse CreateGBPCard()
        {
            string header = "";
            string footer = "";
            CreateCardResponse objResponse = null;
            CardCreatorLogger logger = new CardCreatorLogger();
            try
            {
                //Soap Header And Footter
                header = "<?xml version='1.0' encoding='utf - 8'?><soapenv:Envelope xmlns:soapenv='http://schemas.xmlsoap.org/soap/envelope/' xmlns:ws='http://ws.integration.services.prepaid.corporatepay.com/'><soapenv:Header/><soapenv:Body><ws:createCard>";
                footer = "</ws:createCard></soapenv:Body ></soapenv:Envelope > ";

                if (GBPCardRequest == null)
                    throw new ArgumentNullException("cardRequest is Empty");
                if (GBPCardRequest.Request == null)
                    throw new ArgumentNullException("cardRequest.Request is Empty");
                if (GBPCardRequest.Request.customer == null)
                    throw new ArgumentNullException("cardRequest.Request.customer is Empty");
                if (GBPCardRequest.Request.customer.info == null)
                    throw new ArgumentNullException("cardRequest.Request.customer.info is Empty");
                if (GBPCardRequest.Request.customer.permanentAddress == null)
                    throw new ArgumentNullException("cardRequest.Request.customer.permanentAddress is Empty");
                if (GBPCardRequest != null)
                    GBPCardRequest.QuoteHeaderId = quoteHeaderId;
                //Signature Strings
                signature = GBPCardRequest.Request.requestReference + "." + GBPCardRequest.Request.loadAmount + "." + GBPCardRequest.Request.customer.info.lastName;
                hashStringWithRALN = GetHashString(signature).ToString();
                hashStringWithPass = hashStringWithRALN.ToLower() + "." + password.ToString();
                finalSignature = GetHashString(hashStringWithPass).ToString();

                GBPCardRequest.Request.signature = finalSignature.ToLower();

                if (enableLogging)
                {
                    //Log Request
                    logger.LogRequest(GBPCardRequest, connectionString);
                }

                string rawXml = null;
                HttpWebRequest httpWebRequest = null;
                HttpWebResponse response;
                string destinationUrl = null;
                destinationUrl = ConfigurationManager.AppSettings["WEX.URL"].ToString();

                string xmlString = ConvertObjectToXMLString(GBPCardRequest.Request).ToString();
                string actualTrasferXml = RemoveAllNamespaces(xmlString).Replace(System.Environment.NewLine, string.Empty);
                XmlDocument doc1 = new XmlDocument();
                doc1.PreserveWhitespace = false;
                doc1.LoadXml(actualTrasferXml.ToString());
                rawXml = doc1.InnerXml.ToString();

                httpWebRequest = (HttpWebRequest)WebRequest.Create(destinationUrl);
                httpWebRequest.Method = "POST";
                httpWebRequest.Accept = "application/xml";

                string postData = rawXml.ToString();
                string request = header + postData + footer;

                byte[] bytes = Encoding.UTF8.GetBytes(request);
                httpWebRequest.ContentLength = bytes.Length;
                Stream requestStream = httpWebRequest.GetRequestStream();
                requestStream.Write(bytes, 0, bytes.Length);

                response = (HttpWebResponse)httpWebRequest.GetResponse();
                var encoding = ASCIIEncoding.ASCII;
                string responseText = string.Empty;
                using (var reader = new System.IO.StreamReader(response.GetResponseStream(), encoding))
                {
                    responseText = reader.ReadToEnd();
                }
                objResponse = GetResponse(responseText);
                if (objResponse != null)
                    objResponse.QuoteHeaderId = quoteHeaderId;
                //Log Request
                logger.LogResponse(objResponse, connectionString);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return objResponse;
        }
        /// <summary>
        /// Creates WEX Card - EURO
        /// </summary>
        public CreateCardResponse CreateEUROCard()
        {
            string header = "";
            string footer = "";
            CreateCardResponse objResponse = null;
            CardCreatorLogger logger = new CardCreatorLogger();
            try
            {
                //Soap Header And Footter
                header = "<?xml version='1.0' encoding='utf - 8'?><soapenv:Envelope xmlns:soapenv='http://schemas.xmlsoap.org/soap/envelope/' xmlns:ws='http://ws.integration.services.prepaid.corporatepay.com/'><soapenv:Header/><soapenv:Body><ws:createCard>";
                footer = "</ws:createCard></soapenv:Body ></soapenv:Envelope > ";

                if (EUROCardRequest == null)
                    throw new ArgumentNullException("EUROCardRequest is Empty");
                if (EUROCardRequest.request == null)
                    throw new ArgumentNullException("EUROCardRequest.request is Empty");
                if (EUROCardRequest.request.customer == null)
                    throw new ArgumentNullException("EUROCardRequest.request.customer is Empty");
                if (EUROCardRequest.request.customer.info == null)
                    throw new ArgumentNullException("EUROCardRequest.request.customer.info is Empty");
                if (EUROCardRequest.request.customer.permanentAddress == null)
                    throw new ArgumentNullException("EUROCardRequest.request.customer.permanentAddress is Empty");
                if (EUROCardRequest != null)
                    EUROCardRequest.QuoteHeaderId = quoteHeaderId;
                //Signature Strings
                signature = EUROCardRequest.request.requestReference + "." + EUROCardRequest.request.loadAmount + "." + EUROCardRequest.request.customer.info.lastName;
                hashStringWithRALN = GetHashString(signature).ToString();
                hashStringWithPass = hashStringWithRALN.ToLower() + "." + password.ToString();
                finalSignature = GetHashString(hashStringWithPass).ToString();

                EUROCardRequest.request.signature = finalSignature.ToLower();

                if (enableLogging)
                {
                    //Log Request
                    logger.LogRequest(EUROCardRequest, connectionString);
                }

                string rawXml = null;
                HttpWebRequest httpWebRequest = null;
                HttpWebResponse response;
                string destinationUrl = null;
                destinationUrl = ConfigurationManager.AppSettings["WEX.URL"].ToString();

                string xmlString = ConvertObjectToXMLString(EUROCardRequest.request).ToString();
                string actualTrasferXml = RemoveAllNamespaces(xmlString).Replace(System.Environment.NewLine, string.Empty);
                XmlDocument doc1 = new XmlDocument();
                doc1.PreserveWhitespace = false;
                doc1.LoadXml(actualTrasferXml.ToString());
                rawXml = doc1.InnerXml.ToString();

                httpWebRequest = (HttpWebRequest)WebRequest.Create(destinationUrl);
                httpWebRequest.Method = "POST";
                httpWebRequest.Accept = "application/xml";

                string postData = rawXml.ToString();
                string request = header + postData + footer;

                byte[] bytes = Encoding.UTF8.GetBytes(request);
                httpWebRequest.ContentLength = bytes.Length;
                Stream requestStream = httpWebRequest.GetRequestStream();
                requestStream.Write(bytes, 0, bytes.Length);

                response = (HttpWebResponse)httpWebRequest.GetResponse();
                var encoding = ASCIIEncoding.ASCII;
                string responseText = string.Empty;
                using (var reader = new System.IO.StreamReader(response.GetResponseStream(), encoding))
                {
                    responseText = reader.ReadToEnd();
                }
                objResponse = GetResponse(responseText);
                if (objResponse != null)
                    objResponse.QuoteHeaderId = quoteHeaderId;
                //Log Request
                logger.LogResponse(objResponse, connectionString);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return objResponse;
        }
        public CreateCardResponse GetWexCard(string quoteHeaderId)
        {
            if (string.IsNullOrEmpty(quoteHeaderId))
                throw new ArgumentNullException("quoteHeaderId is empty or null");
            if (string.IsNullOrEmpty(connectionString))
                throw new ArgumentNullException("connectionString is empty or null");

            List<CreateCardResponse> cardsList = new List<CreateCardResponse>();
            if (!string.IsNullOrEmpty(connectionString))
            {
                var db = new CommandRunner(connectionString);
                string sql = "select * from get_wexcard('" + quoteHeaderId + "')";
                var res = db.ExecuteDynamic(sql);
                foreach (var item in res)
                {
                    CreateCardResponse card = new CreateCardResponse();
                    card.cardReference = item.cardreference;
                    card.cv2 = item.cv2;
                    card.expiryDate = item.expirydate;
                    card.message = item.message;
                    card.PAN = item.pan;
                    card.QuoteHeaderId = item.quoteheaderid;
                    card.requestReference = item.requestreference;
                    card.resultCode = item.resultcode;
                    card.signature = item.signature;
                    card.success = item.success;
                    card.CreatedDate = item.createddate;
                    cardsList.Add(card);
                }
            }
            return cardsList.OrderByDescending(n => n.CreatedDate).FirstOrDefault();
        }
        //generatehash
        private string GetHashString(string inputString)
        {
            StringBuilder sb = new StringBuilder();
            foreach (byte b in GetHash(inputString))
                sb.Append(b.ToString("X2"));

            return sb.ToString().ToLower();
        }
        private byte[] GetHash(string inputString)
        {
            HashAlgorithm algorithm = SHA1.Create();  // SHA1.Create()
            return algorithm.ComputeHash(Encoding.UTF8.GetBytes(inputString));
        }
        private string ConvertObjectToXMLString(object classObject)
        {
            string xmlString = null;
            XmlSerializer xmlSerializer = new XmlSerializer(classObject.GetType());
            using (MemoryStream memoryStream = new MemoryStream())
            {
                xmlSerializer.Serialize(memoryStream, classObject);
                memoryStream.Position = 0;
                xmlString = new StreamReader(memoryStream).ReadToEnd();

            }
            return xmlString;
        }
        private string RemoveAllNamespaces(string xmlDocument)
        {
            XElement xmlDocumentWithoutNs = RemoveAllNamespaces(XElement.Parse(xmlDocument));

            return xmlDocumentWithoutNs.ToString();
        }
        private XElement RemoveAllNamespaces(XElement xmlDocument)
        {
            if (!xmlDocument.HasElements)
            {
                XElement xElement = new XElement(xmlDocument.Name.LocalName);
                xElement.Value = xmlDocument.Value;

                foreach (XAttribute attribute in xmlDocument.Attributes())
                    xElement.Add(attribute);

                return xElement;
            }
            return new XElement(xmlDocument.Name.LocalName, xmlDocument.Elements().Select(el => RemoveAllNamespaces(el)));
        }
        private CreateCardResponse GetResponse(string responseXML)
        {
            string x = RemoveAllNamespaces(responseXML);
            CreateCardResponse objCreateCardResponse = new CreateCardResponse();
            XDocument doc = XDocument.Parse(x);
            objCreateCardResponse.requestReference = doc.Root.Descendants("requestReference").FirstOrDefault() != null ? doc.Root.Descendants("requestReference").FirstOrDefault().Value : string.Empty;
            objCreateCardResponse.signature = doc.Root.Descendants("signature").FirstOrDefault() != null ? doc.Root.Descendants("signature").FirstOrDefault().Value : string.Empty;
            objCreateCardResponse.success = doc.Root.Descendants("success").FirstOrDefault() != null ? doc.Root.Descendants("success").FirstOrDefault().Value : string.Empty;
            objCreateCardResponse.resultCode = doc.Root.Descendants("resultCode").FirstOrDefault() != null ? doc.Root.Descendants("resultCode").FirstOrDefault().Value : string.Empty;
            objCreateCardResponse.message = doc.Root.Descendants("message").FirstOrDefault() != null ? doc.Root.Descendants("message").FirstOrDefault().Value : string.Empty;
            objCreateCardResponse.PAN = doc.Root.Descendants("PAN").FirstOrDefault() != null ? doc.Root.Descendants("PAN").FirstOrDefault().Value : string.Empty;
            objCreateCardResponse.cv2 = doc.Root.Descendants("cv2").FirstOrDefault() != null ? doc.Root.Descendants("cv2").FirstOrDefault().Value : string.Empty;
            objCreateCardResponse.expiryDate = doc.Root.Descendants("expiryDate").FirstOrDefault() != null ? doc.Root.Descendants("expiryDate").FirstOrDefault().Value : string.Empty;
            objCreateCardResponse.cardReference = doc.Root.Descendants("cardReference").FirstOrDefault() != null ? doc.Root.Descendants("cardReference").FirstOrDefault().Value : string.Empty;
            return objCreateCardResponse;
        }
    }
}
