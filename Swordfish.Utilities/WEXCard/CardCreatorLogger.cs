﻿using Swordfish.Utilities.DataAccess;
namespace Swordfish.Utilities.WEXCard
{
    internal class CardCreatorLogger
    {
        public void LogRequest(GBP.createCardRequest GBPCardRequest, string connectionString)
        {
            if (!string.IsNullOrEmpty(connectionString))
            {
                var db = new CommandRunner(connectionString);
                string sql = "select * from save_wex_request(" + GBPCardRequest.Request.loadAmount + ",'" + GBPCardRequest.QuoteHeaderId + "','" + GBPCardRequest.Request.requestReference + "','" + GBPCardRequest.Request.username + "','" + GBPCardRequest.Request.signature + "','" + GBPCardRequest.Request.account + "','" + GBPCardRequest.Request.expiry + "','" + GBPCardRequest.Request.instant + "','" + GBPCardRequest.Request.activated + "','" + GBPCardRequest.Request.description + "','" + GBPCardRequest.Request.efftvStartDate + "','" + GBPCardRequest.Request.customer.info.title + "','" + GBPCardRequest.Request.customer.info.firstName + "','" + GBPCardRequest.Request.customer.info.lastName + "','" + GBPCardRequest.Request.customer.info.gender + "','" + GBPCardRequest.Request.customer.info.dateOfBirth + "','" + GBPCardRequest.Request.customer.info.email + "','" + GBPCardRequest.Request.customer.permanentAddress.id + "','" + GBPCardRequest.Request.customer.permanentAddress.street + "','" + GBPCardRequest.Request.customer.permanentAddress.postcode + "','" + GBPCardRequest.Request.customer.permanentAddress.city + "','" + GBPCardRequest.Request.customer.permanentAddress.country + "','" + GBPCardRequest.Request.customer.permanentAddress.houseNumber + "')";
                var res = db.ExecuteDynamic(sql);
                foreach (var r in res)
                {
                    var s = r;
                }
            }
        }
        public void LogRequest(EURO.createCard EUROCardRequest, string connectionString)
        {
            if (!string.IsNullOrEmpty(connectionString))
            {
                var db = new CommandRunner(connectionString);
                string sql = "select * from save_wex_request(" + EUROCardRequest.request.loadAmount + ",'" + EUROCardRequest.QuoteHeaderId + "','" + EUROCardRequest.request.requestReference + "','" + EUROCardRequest.request.username + "','" + EUROCardRequest.request.signature + "','" + EUROCardRequest.request.account + "','" + EUROCardRequest.request.expiry + "','" + EUROCardRequest.request.instant + "','" + EUROCardRequest.request.activated + "','" + EUROCardRequest.request.description + "','','" + EUROCardRequest.request.customer.info.title + "','" + EUROCardRequest.request.customer.info.firstName + "','" + EUROCardRequest.request.customer.info.lastName + "','" + EUROCardRequest.request.customer.info.gender + "','" + EUROCardRequest.request.customer.info.dateOfBirth + "','" + EUROCardRequest.request.customer.info.email + "','" + EUROCardRequest.request.customer.permanentAddress.id + "','" + EUROCardRequest.request.customer.permanentAddress.street + "','" + EUROCardRequest.request.customer.permanentAddress.postcode + "','" + EUROCardRequest.request.customer.permanentAddress.city + "','" + EUROCardRequest.request.customer.permanentAddress.country + "','" + EUROCardRequest.request.customer.permanentAddress.houseName + "')";
                var res = db.ExecuteDynamic(sql);
                foreach (var r in res)
                {
                    var s = r;
                }
            }
        }
        public void LogResponse(CreateCardResponse cardResponse, string connectionString)
        {
            if (!string.IsNullOrEmpty(connectionString))
            {
                var db = new CommandRunner(connectionString);
                string sql = "select * from save_wex_response('" + cardResponse.requestReference + "','" + cardResponse.QuoteHeaderId + "','" + cardResponse.signature + "','" + cardResponse.success + "','" + cardResponse.resultCode + "','" + cardResponse.message + "','" + cardResponse.PAN + "','" + cardResponse.cv2 + "','" + cardResponse.expiryDate + "','" + cardResponse.cardReference + "')";
                var res = db.ExecuteDynamic(sql);
                foreach (var r in res)
                {
                    var s = r;
                }
            }
        }
    }
}
