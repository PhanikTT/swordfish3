﻿using System.Xml.Serialization;

namespace Swordfish.Utilities.WEXCard.GBP
{
    public class webServiceRequest
    {

        public string requestReference { get; set; }
        public string username { get; set; }
        public string signature { get; set; }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://schemas.xmlsoap.org/soap/envelope/")]
        [System.Xml.Serialization.XmlRootAttribute(Namespace = "http://schemas.xmlsoap.org/soap/envelope/", IsNullable = false)]
        public partial class Envelope
        {

            private object headerField;

            private EnvelopeBody bodyField;

            /// <remarks/>
            public object Header
            {
                get
                {
                    return this.headerField;
                }
                set
                {
                    this.headerField = value;
                }
            }

            /// <remarks/>
            public EnvelopeBody Body
            {
                get
                {
                    return this.bodyField;
                }
                set
                {
                    this.bodyField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://schemas.xmlsoap.org/soap/envelope/")]
        public partial class EnvelopeBody
        {

            private createCard createCardField;

            /// <remarks/>
            [System.Xml.Serialization.XmlElementAttribute(Namespace = "http://ws.integration.services.prepaid.corporatepay.com/")]
            public createCard createCard
            {
                get
                {
                    return this.createCardField;
                }
                set
                {
                    this.createCardField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://ws.integration.services.prepaid.corporatepay.com/")]
        [System.Xml.Serialization.XmlRootAttribute(Namespace = "http://ws.integration.services.prepaid.corporatepay.com/", IsNullable = false)]
        public partial class createCard
        {

            private request requestField;

            /// <remarks/>
            [System.Xml.Serialization.XmlElementAttribute(Namespace = "")]
            public request request
            {
                get
                {
                    return this.requestField;
                }
                set
                {
                    this.requestField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        [System.Xml.Serialization.XmlRootAttribute(Namespace = "", IsNullable = false)]
        public partial class request
        {

            private string requestReferenceField;

            private string usernameField;

            private string signatureField;

            private uint accountField;

            private byte expiryField;

            private bool instantField;

            private bool activatedField;

            private ushort loadAmountField;

            private string descriptionField;

            private System.DateTime efftvStartDateField;

            private requestCustomer customerField;

            /// <remarks/>
            public string requestReference
            {
                get
                {
                    return this.requestReferenceField;
                }
                set
                {
                    this.requestReferenceField = value;
                }
            }

            /// <remarks/>
            public string username
            {
                get
                {
                    return this.usernameField;
                }
                set
                {
                    this.usernameField = value;
                }
            }

            /// <remarks/>
            public string signature
            {
                get
                {
                    return this.signatureField;
                }
                set
                {
                    this.signatureField = value;
                }
            }

            /// <remarks/>
            public uint account
            {
                get
                {
                    return this.accountField;
                }
                set
                {
                    this.accountField = value;
                }
            }

            /// <remarks/>
            public byte expiry
            {
                get
                {
                    return this.expiryField;
                }
                set
                {
                    this.expiryField = value;
                }
            }

            /// <remarks/>
            public bool instant
            {
                get
                {
                    return this.instantField;
                }
                set
                {
                    this.instantField = value;
                }
            }

            /// <remarks/>
            public bool activated
            {
                get
                {
                    return this.activatedField;
                }
                set
                {
                    this.activatedField = value;
                }
            }

            /// <remarks/>
            public ushort loadAmount
            {
                get
                {
                    return this.loadAmountField;
                }
                set
                {
                    this.loadAmountField = value;
                }
            }

            /// <remarks/>
            public string description
            {
                get
                {
                    return this.descriptionField;
                }
                set
                {
                    this.descriptionField = value;
                }
            }

            /// <remarks/>
            [System.Xml.Serialization.XmlElementAttribute(DataType = "date")]
            public System.DateTime efftvStartDate
            {
                get
                {
                    return this.efftvStartDateField;
                }
                set
                {
                    this.efftvStartDateField = value;
                }
            }

            /// <remarks/>
            public requestCustomer customer
            {
                get
                {
                    return this.customerField;
                }
                set
                {
                    this.customerField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        public partial class requestCustomer
        {

            private requestCustomerInfo infoField;

            private requestCustomerPermanentAddress permanentAddressField;

            /// <remarks/>
            public requestCustomerInfo info
            {
                get
                {
                    return this.infoField;
                }
                set
                {
                    this.infoField = value;
                }
            }

            /// <remarks/>
            public requestCustomerPermanentAddress permanentAddress
            {
                get
                {
                    return this.permanentAddressField;
                }
                set
                {
                    this.permanentAddressField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        public partial class requestCustomerInfo
        {

            private string titleField;

            private string firstNameField;

            private string lastNameField;

            private string genderField;

            private System.DateTime dateOfBirthField;

            private string emailField;

            /// <remarks/>
            public string title
            {
                get
                {
                    return this.titleField;
                }
                set
                {
                    this.titleField = value;
                }
            }

            /// <remarks/>
            public string firstName
            {
                get
                {
                    return this.firstNameField;
                }
                set
                {
                    this.firstNameField = value;
                }
            }

            /// <remarks/>
            public string lastName
            {
                get
                {
                    return this.lastNameField;
                }
                set
                {
                    this.lastNameField = value;
                }
            }

            /// <remarks/>
            public string gender
            {
                get
                {
                    return this.genderField;
                }
                set
                {
                    this.genderField = value;
                }
            }

            /// <remarks/>
            public System.DateTime dateOfBirth
            {
                get
                {
                    return this.dateOfBirthField;
                }
                set
                {
                    this.dateOfBirthField = value;
                }
            }

            /// <remarks/>
            public string email
            {
                get
                {
                    return this.emailField;
                }
                set
                {
                    this.emailField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        public partial class requestCustomerPermanentAddress
        {

            private byte idField;

            private string streetField;

            private string postcodeField;

            private string cityField;

            private string countryField;

            private byte houseNumberField;

            /// <remarks/>
            public byte id
            {
                get
                {
                    return this.idField;
                }
                set
                {
                    this.idField = value;
                }
            }

            /// <remarks/>
            public string street
            {
                get
                {
                    return this.streetField;
                }
                set
                {
                    this.streetField = value;
                }
            }

            /// <remarks/>
            public string postcode
            {
                get
                {
                    return this.postcodeField;
                }
                set
                {
                    this.postcodeField = value;
                }
            }

            /// <remarks/>
            public string city
            {
                get
                {
                    return this.cityField;
                }
                set
                {
                    this.cityField = value;
                }
            }

            /// <remarks/>
            public string country
            {
                get
                {
                    return this.countryField;
                }
                set
                {
                    this.countryField = value;
                }
            }

            /// <remarks/>
            public byte houseNumber
            {
                get
                {
                    return this.houseNumberField;
                }
                set
                {
                    this.houseNumberField = value;
                }
            }
        }


    }
    public class udfRequest : webServiceRequest
    {
        public string udf1 { get; set; }
        public string udf2 { get; set; }
        public string udf3 { get; set; }
        public string udf4 { get; set; }
        public string udf5 { get; set; }
        public string udf6 { get; set; }
        public string udf7 { get; set; }
        public string udf8 { get; set; }
        public string udf9 { get; set; }
        public string udf10 { get; set; }
        public string udf11 { get; set; }
        public string udf12 { get; set; }
        public string udf13 { get; set; }
        public string udf14 { get; set; }
        public string udf15 { get; set; }
        public string udf16 { get; set; }
        public string udf17 { get; set; }
        public string udf18 { get; set; }
        public string udf19 { get; set; }
        public string udf20 { get; set; }
    }

    public class request : udfRequest
    {
        public string account { get; set; }
        public bool instant { get; set; }
        public int expiry { get; set; }
        public bool activated { get; set; }
        public int loadAmount { get; set; }
        public string efftvStartDate { get; set; }
        public string description { get; set; }
        public customer customer { get; set; }
        public string customData1 { get; set; }
        public string customData2 { get; set; }
        public string faxNumber { get; set; }
    }
    public class customer
    {
        public string reference { get; set; }

        public string ip { get; set; }

        public personalInfo info { get; set; }

        public addressDTO permanentAddress { get; set; }
    }
    public class personalInfo
    {
        public string title { get; set; }

        public string firstName { get; set; }

        public string middleName { get; set; }

        public string lastName { get; set; }

        public string gender { get; set; }

        public string homePhone { get; set; }

        public string mobilePhone { get; set; }

        public string email { get; set; }

        public string dateOfBirth { get; set; }

        public string placeOfBirth { get; set; }

        public string marketingOptIn { get; set; }
    }
    public class addressDTO
    {
        public int id { get; set; }

        public string flatNumber { get; set; }

        public string houseNumber { get; set; }

        public string houseName { get; set; }

        public string street { get; set; }

        public string line1 { get; set; }

        public string line2 { get; set; }

        public string line3 { get; set; }

        public string city { get; set; }

        public string state { get; set; }

        public string postcode { get; set; }

        public string country { get; set; }
    }


    [XmlRoot(ElementName = "createCard", Namespace = "http://ws.integration.services.prepaid.corporatepay.com/")]
    public class createCardRequest
    {
        [XmlElement(ElementName = "request")]
        public request Request { get; set; }
        public string QuoteHeaderId { get; set; }
    }

    [XmlRoot(ElementName = "Body", Namespace = "http://schemas.xmlsoap.org/soap/envelope/")]
    public class Body
    {
        [XmlElement(ElementName = "createCard", Namespace = "http://ws.integration.services.prepaid.corporatepay.com/")]
        public createCardRequest createCard { get; set; }
    }

    [XmlRoot(ElementName = "Envelope", Namespace = "http://schemas.xmlsoap.org/soap/envelope/")]
    public class Envelope
    {
        [XmlElement(ElementName = "Header", Namespace = "http://schemas.xmlsoap.org/soap/envelope/")]
        public string Header { get; set; }
        [XmlElement(ElementName = "Body", Namespace = "http://schemas.xmlsoap.org/soap/envelope/")]
        public Body Body { get; set; }
        [XmlAttribute(AttributeName = "soapenv", Namespace = "http://www.w3.org/2000/xmlns/")]
        public string Soapenv { get; set; }
        [XmlAttribute(AttributeName = "ws", Namespace = "http://www.w3.org/2000/xmlns/")]
        public string Ws { get; set; }
    }
}
