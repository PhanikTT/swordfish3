﻿using System.Collections.Generic;
using Npgsql;

namespace Swordfish.Utilities.DataAccess
{
    public interface ICommandRunner
    {
        NpgsqlCommand BuildCommand(string sql, params object[] args);
        string ConnectionString { get; set; }
        IEnumerable<T> Execute<T>(string sql, params object[] args) where T : new();
        IEnumerable<dynamic> ExecuteDynamic(string sql, params object[] args);
        T ExecuteSingle<T>(string sql, params object[] args) where T : new();
        dynamic ExecuteSingleDynamic(string sql, params object[] args);
        NpgsqlDataReader OpenReader(string sql, params object[] args);
        List<int> Transact(params NpgsqlCommand[] cmds);
    }
}
