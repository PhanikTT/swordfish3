﻿using Swordfish.Utilities.DataAccess;
using System;
using System.Configuration;

namespace Swordfish.Utilities.ELKLog
{
    public static class DataAccess
    {
        public static bool SaveSFLog(string logType, int userId, string userSession, string environment, string thread, string functionName, string message, string payload, string stackTrace, int createdBy, string clientIP)
        {
            bool result = false;
            try
            {
                var db = new CommandRunner(ConfigurationManager.ConnectionStrings["chilliDataBase"].ConnectionString.ToString());
                string sql = "select * from save_sf_log('" + logType + "'," + userId + ",'" + userSession + "','" + environment + "','" + thread + "','" + functionName + "','" + message.Replace("'", "") + "','" + payload + "','" + stackTrace.Replace("'", "") + "'," + createdBy + ",'" + clientIP + "') as sflog";
                var res = db.ExecuteDynamic(sql);
                foreach (var x in res)
                {
                    result = Convert.ToBoolean(x.sflog);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return result;
        }
    }
}
