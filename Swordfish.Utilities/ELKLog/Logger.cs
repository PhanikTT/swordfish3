﻿using log4net;
using System;
using System.Threading.Tasks;

namespace Swordfish.Utilities.ELKLog
{
    public class Logger
    {
        private static ILog logger = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);


        public static Task Log(string MessageType, string Message, Exception ex)
        {

            switch (MessageType)
            {
                case "Debug":
                    logger.Debug(DateTime.Now.ToString() + "-" + Message, ex == null ? null : ex);
                    break;
                case "Info":
                    logger.Info(DateTime.Now.ToString() + "-" + Message, ex == null ? null : ex);
                    break;
                case "Warn":
                    logger.Warn(DateTime.Now.ToString() + "-" + Message, ex == null ? null : ex);
                    break;
                case "Error":
                    logger.Error(DateTime.Now.ToString() + "-" + Message, ex == null ? null : ex);
                    break;
                case "Fatal":
                    logger.Fatal(DateTime.Now.ToString() + "-" + Message, ex == null ? null : ex);
                    break;
                default:
                    logger.Error(DateTime.Now.ToString() + "-" + Message, ex == null ? null : ex);
                    break;
            }

            return Task.Delay(1);

        }
        public static Task Log(string MessageType, string Message)
        {
            Log(MessageType, Message, null);
            return Task.Delay(1);
        }
    }
}
