﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(SwordFish.Startup))]
namespace SwordFish
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
            app.MapSignalR();
        }
    }
}
