﻿using Swordfish.Components.MembershipServices;
using Swordfish.UI.Helpers;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using System.Web.Script.Serialization;
namespace SwordFish.App_Start
{
    public class SFAuthorizeAttribute : AuthorizeAttribute
    {
        //public override void OnAuthorization(AuthorizationContext filterContext)
        //{
        //    if (filterContext.HttpContext.Request.IsAuthenticated)
        //    {
        //        //var authorizedUsers = ConfigurationManager.AppSettings[UsersConfigKey];
        //        //var authorizedRoles = ConfigurationManager.AppSettings[RolesConfigKey];

        //        //Users = String.IsNullOrEmpty(Users) ? authorizedUsers : Users;
        //        //Roles = String.IsNullOrEmpty(Roles) ? authorizedRoles : Roles;

        //        //if (!String.IsNullOrEmpty(Roles))
        //        //{
        //        //    if (!CurrentUser.IsInRole(Roles))
        //        //    {
        //        //        filterContext.Result = new RedirectToRouteResult(new
        //        //        RouteValueDictionary(new { controller = "Error", action = "AccessDenied" }));

        //        //        // base.OnAuthorization(filterContext); //returns to login url
        //        //    }
        //        //}

        //        //if (!String.IsNullOrEmpty(Users))
        //        //{
        //        //    if (!Users.Contains(CurrentUser.UserId.ToString()))
        //        //    {
        //        //        filterContext.Result = new RedirectToRouteResult(new
        //        //        RouteValueDictionary(new { controller = "Error", action = "AccessDenied" }));

        //        //        // base.OnAuthorization(filterContext); //returns to login url
        //        //    }
        //        //}
        //    }

        //}

        protected override bool AuthorizeCore(HttpContextBase httpContext)
        {

            //var isAuthorized = false;
            //if (httpContext != null)
            //{ 
            //    isAuthorized = base.AuthorizeCore(httpContext);
            //    if (isAuthorized)
            //    {
            //        var authCookie = HttpContext.Current.Request.Cookies[FormsAuthentication.FormsCookieName];
            //        if (authCookie != null)
            //        {
            //            string encTicket = authCookie.Value;
            //            if (!String.IsNullOrEmpty(encTicket))
            //            {
            //                var ticket = FormsAuthentication.Decrypt(encTicket);
            //                var id = new UserIdentity(ticket);
            //                MembershipRepositoryService.ServiceRepository serviceRepository = new MembershipRepositoryService.ServiceRepository();
            //                var userRoles = GetUserRoles(serviceRepository, httpContext.User.Identity.Name);
            //                var prin = new GenericPrincipal(id, userRoles.ToArray());
            //                HttpContext.Current.User = prin;

            //            }
            //        }
            //    }
            //}
            // return isAuthorized;
            return true;
        }

        private IEnumerable<string> GetUserRoles(MembershipService serviceRepository, string username)
        {
            MembershipHelper userModel = new MembershipHelper(serviceRepository);
            IEnumerable<string> allRoles = null;
            if (HttpContext.Current.Cache.Get(username) == null)
            {


                var allUserRoles = Task.Run(() => userModel.GetUserRoles(username));
                Task.WhenAll(allUserRoles);
                allRoles = allUserRoles.Result;
                var serializer = new JavaScriptSerializer();
                var serializedResult = serializer.Serialize(allRoles);
                HttpContext.Current.Cache.Insert(username, serializedResult);
            }
            else
            {
                var serializ = new JavaScriptSerializer();


                allRoles = serializ.Deserialize<IEnumerable<string>>(HttpContext.Current.Cache[username].ToString());

            }
            return allRoles;
        }
    }
}