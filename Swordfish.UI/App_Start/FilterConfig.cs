﻿using SwordFish.App_Start;
using SwordFish.Filters;
using System.Web.Mvc;

namespace SwordFish
{
    public class FilterConfig
    {
        public static void RegisterGlobalFilters(GlobalFilterCollection filters)
        {

            //filters.Add(new CustomHandleErrorAttribute());
       //     filters.Add(new HandleErrorAttribute());
            filters.Add(new SFAuthorizeAttribute());

        }
    }
}
