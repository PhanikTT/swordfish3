﻿using System.Web;
using System.Web.Optimization;

namespace SwordFish
{
    public class BundleConfig
    {
        // For more information on bundling, visit http://go.microsoft.com/fwlink/?LinkId=301862
        public static void RegisterBundles(BundleCollection bundles)
        {
            // Use the development version of Modernizr to develop with and learn from. Then, when you're
            // ready for production, use the build tool at http://modernizr.com to pick only the tests you need.
            bundles.Add(new ScriptBundle("~/bundles/jquery").Include(
                        "~/Scripts/JQuery/jquery-{version}.js",
                        "~/Scripts/JQuery/jquery-ui.min.js",
                        "~/Scripts/JQuery/jquery.validate*",
                        "~/Scripts/JQuery/plugins/canvasjs/jquery.canvasjs.min.js",
                        "~/Scripts/JQuery/jquery.bootpag.min.js"
                        ));

            // Use the development version of Modernizr to develop with and learn from. Then, when you're
            // ready for production, use the build tool at http://modernizr.com to pick only the tests you need.
            bundles.Add(new ScriptBundle("~/bundles/modernizr").Include(
                        "~/Scripts/modernizr-*"));

            bundles.Add(new ScriptBundle("~/bundles/tinymce").Include(
                      "~/Scripts/tinymce/tinymce.min.js"));

            bundles.Add(new ScriptBundle("~/bundles/chartsScript").Include(
                   "~/Scripts/barchart.js"));

            bundles.Add(new ScriptBundle("~/bundles/dashBoardScript").Include(
                   "~/Scripts/dashboardscript.js", "~/Scripts/CommonScript.js"));


            bundles.Add(new ScriptBundle("~/bundles/roomTypeScript").Include(
                  "~/Scripts/roomtypescript.js"));

            bundles.Add(new ScriptBundle("~/bundles/bootPage").Include(
                ));

            bundles.Add(new ScriptBundle("~/bundles/paymentscript").Include(
                  "~/Scripts/paymentscript.js"));

            bundles.Add(new ScriptBundle("~/bundles/GridMVC").Include(
                   "~/Scripts/gridmvc.js",
                    "~/Scripts/gridmvc.lang.ru.js",
                    "~/Scripts/gridmvc.min.js"));

            bundles.Add(new ScriptBundle("~/bundles/Slider").Include(
                "~/Scripts/ion.rangeSlider.min.js"));

            bundles.Add(new ScriptBundle("~/bundles/daterangepicker").Include(
            "~/Scripts/JQuery/plugins/momentjs/moment.min.js",
            "~/Scripts/JQuery/plugins/daterangepicker/daterangepicker.min.js",
                    "~/Scripts/JQuery/plugins/datetimepicker/jquery.datetimepicker.full.min.js",
                    "~/Scripts/Bootstrap/bootstrap-datetimepicker.min.js"));


            bundles.Add(new ScriptBundle("~/bundles/bootstrap").Include(
                    "~/Scripts/Bootstrap/bootstrap.min.js",
                    "~/Scripts/Bootstrap/bootstrap.touchspin.js",
                    "~/Scripts/respond.js",
                    "~/Scripts/starrating.js",
                    "~/Scripts/html5shiv.js",
                    "~/Scripts/jssor.slider.mini.js"
                    ));

            bundles.Add(new ScriptBundle("~/bundles/menuscript").Include(
                "~/Scripts/menuscript.js", "~/Scripts/CommonScript.js"
                ));

            bundles.Add(new ScriptBundle("~/bundles/signalR").Include(
                "~/Scripts/JQuery/plugins/signalr/jquery.signalR-2.2.0.min.js",
                "~/Scripts/JQuery/plugins/signalr/hubs.js",
                "~/Scripts/JQuery/plugins/signalr/hubsConnect.js"));

            bundles.Add(new ScriptBundle("~/bundles/quotefilter").Include(
                "~/Scripts/filterscript.js",
                "~/Scripts/transfers.js",
                "~/Scripts/basketactions.js",
                "~/Scripts/quotesearchactions.js"));

            //bundles.Add(new ScriptBundle("~/bundles/transferjs").Include("~/Scritps/transfers.js"));

            bundles.Add(new ScriptBundle("~/bundles/customerquotes").Include(
            "~/Scripts/customerquotes.js",
            "~/Scripts/transfers.js"));

            bundles.Add(new ScriptBundle("~/bundles/agentquotes").Include(
         "~/Scripts/agentquotes.js"));

            //CSS Bundling
            bundles.Add(new StyleBundle("~/Content/Chillicss").Include(
                     "~/Content/bootstrap.min.css",
                     "~/Content/fonts.css",
                     "~/Content/jqueryui.css",
                     "~/Content/bootstrap-theme.min.css",
                     "~/Content/ion.rangeSlider.css",
                     "~/Content/Gridmvc.css",
                     "~/Content/daterangepicker.css",
                     "~/Content/ion.rangeSlider.skinFlat.css",
                     "~/Content/normalize.css",
                     "~/Content/Styles.css",
                     "~/Content/datepicker.css",
                     "~/Content/autocomplete.css",
                     "~/Content/AutoExtender.css",
                     "~/Content/starrating.css",
                     "~/Content/fontawesome.css"
                      ));

            //Angular JS
            bundles.Add(new ScriptBundle("~/bundles/angularCore").Include(
                                        "~/Scripts/AngularJS/angular.min.js",
                                        "~/Scripts/AngularJS/angular-messages.min.js",
                                        "~/Scripts/AngularJS/angular-*",
                                        "~/Scripts/AngularJS/directives/ngtouchspin/ngTouchSpin.js",
                                        "~/Scripts/AngularJS/angular-sanitize.min.js"
                                        ));

            bundles.Add(new StyleBundle("~/Content/angularCSS").Include(
                                        "~/Content/font-awesome.min.css",
                                        "~/Content/rzslider.css",
                                        "~/Content/ui-grid.min.css",
                                        "~/Content/daterangepicker.min.css",
                                        "~/Content/bootstrap.min.css",
                                        "~/Content/fonts.css",
                                        "~/Content/perfect-scrollbar.min.css",
                                        "~/Content/ngStyles.css"
                                        ));

            bundles.Add(new ScriptBundle("~/bundles/angularCustom").Include(
                                        "~/Scripts/AngularJS/directives/rzslider/rzslider.js",
                                        "~/Scripts/Bootstrap/ui-grid.js",
                                        "~/Scripts/Bootstrap/ui-bootstrap-tpls.min.js",
                                        "~/Scripts/Bootstrap/bootstrap.min.js",
                                        "~/Scripts/JQuery/plugins/momentjs/moment.min.js",
                                        "~/Scripts/AngularJS/directives/ngscrollbar/ngScrollbar.js",
                                        "~/Scripts/JQuery/plugins/scrollbars/perfect-scrollbar.jquery.min.js",
                                        "~/Scripts/JQuery/plugins/scrollbars/perfect-scrollbar.min.js",
                                        "~/Scripts/JQuery/plugins/daterangepicker/daterangepicker.min.js",
                                        "~/Scripts/Custom/*.js",
                                        "~/Scripts/Custom/Common/Controllers/*.js",
                                        "~/Scripts/Custom/Common/Directives/*.js",
                                        "~/Scripts/Custom/Common/Services/*.js",
                                        "~/Scripts/Custom/Common/Filters/*.js",
                                        "~/Scripts/AngularJS/directives/sticky/sticky.js",
                                        "~/Scripts/Custom/Customer/*.js",
                                        "~/Scripts/Custom/Customer/Directives/*.js",
                                        "~/Scripts/Custom/Filters/*.js",
                                        "~/Scripts/Custom/Filters/Directives/*.js",
                                        "~/Scripts/Custom/Flights/*.js",
                                        "~/Scripts/Custom/Flights/Directives/*.js",
                                        "~/Scripts/Custom/Hotels/*.js",
                                        "~/Scripts/Custom/Hotels/Directives/*.js",
                                        "~/Scripts/Custom/Transfers/*.js",
                                        "~/Scripts/Custom/Transfers/Directives/*.js",
                                        "~/Scripts/Custom/Basket/*.js",
                                        "~/Scripts/Custom/Basket/Directives/*.js",
                                        "~/Scripts/Custom/Baggage/*.js",
                                        "~/Scripts/Custom/Baggage/Directives/*.js",
                                        "~/Scripts/Custom/Booking/*.js",
                                        "~/Scripts/Custom/Booking/Directives/*.js",
                                        "~/Scripts/Custom/AgentQuotes/*.js",
                                        "~/Scripts/AngularJS/directives/pagenation/*.js"
                                        ));

            // Enable Optimization
            BundleTable.EnableOptimizations = false;
        }
    }
}
