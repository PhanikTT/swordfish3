﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;
using System.Threading.Tasks;
using System.IO;
using Swordfish.Components.MembershipServices;
using Swordfish.UI.Helpers;

namespace SwordFish
{
    public class MvcApplication : System.Web.HttpApplication
    {
        protected void Application_Start()
        {
            AreaRegistration.RegisterAllAreas();
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);

            log4net.Config.XmlConfigurator.Configure(new FileInfo(Server.MapPath("~/Web.config")));
        }
        void Application_Error(object sender, EventArgs e)
        {
            Exception exc = Server.GetLastError();

            if (exc is HttpUnhandledException)
            {
            }
            SFLogger.logError("" , exc);
        }
        protected void Application_BeginRequest(object sender, EventArgs e)
        {
            System.Globalization.CultureInfo.DefaultThreadCurrentCulture = new System.Globalization.CultureInfo("en-GB");
            System.Globalization.CultureInfo.DefaultThreadCurrentUICulture = new System.Globalization.CultureInfo("en-GB");
        }

        protected void Session_End(object sender, EventArgs e)
        {
        }
        private IEnumerable<string> GetUserRoles(MembershipService serviceRepository, string username)
        {
            MembershipHelper userModel = new MembershipHelper(serviceRepository);
            IEnumerable<string> allRoles = null;
            if (TempStorage_OLD.roles == null)
            {


                var allUserRoles = Task.Run(() => userModel.GetUserRoles(username));
                Task.WhenAll(allUserRoles);
                allRoles = allUserRoles.Result;
                TempStorage_OLD.roles = allRoles;
            }
            else
            {
                allRoles = TempStorage_OLD.roles;
            }
            return allRoles;
        }
    }
}
