﻿using System;
using System.Runtime.Serialization;

namespace Swordfish.UI.Helpers
{
    public class SwordfishAjaxException : Exception
    {
        public SwordfishAjaxException()
        : base() { }

        public SwordfishAjaxException(string message)
        : base(message) { }

        public SwordfishAjaxException(string format, params object[] args)
        : base(string.Format(format, args)) { }

        public SwordfishAjaxException(string message, Exception innerException)
        : base(message, innerException) { }

        public SwordfishAjaxException(string format, Exception innerException, params object[] args)
        : base(string.Format(format, args), innerException) { }

        protected SwordfishAjaxException(SerializationInfo info, StreamingContext context)
        : base(info, context) { }

    }
}