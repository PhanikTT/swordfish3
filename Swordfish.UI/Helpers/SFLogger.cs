﻿using System;
using System.Text;
using System.Web;
using Swordfish.Utilities.ELKLog;

namespace Swordfish.UI.Helpers
{
    public static class SFLogger
    {
        private static log4net.ILog logger = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        public static void logMessage(string Message)
        {
            logger.Info(DateTime.Now.ToString() + "-" + Message);
            LogToDB(Message, "Message", null);
        }
        public static void logBackGroundjobsMessage(string Message)
        {
             logger.Info(DateTime.Now.ToString() + "-" + Message);
           
        }
        public static void logMessage(string Message,string functionName, string RequestOrResponse)
        {
            logger.Info(DateTime.Now.ToString() + "-" + Message);
            LogTravelSAASLogsToDB(Message, functionName, "TravelSAASLogs", RequestOrResponse);
        }
        public static void logError(string Message, Exception ex)
        {
            logger.Error(DateTime.Now.ToString() + "-" + Message, ex == null ? null : ex);
            LogToEventLog(Message, ex);
            LogToDB(Message, "Error", ex);
        }
        public static void logDebug(string Message)
        {
            logger.Debug(DateTime.Now.ToString() + "-" + Message);
            LogToDB(Message, "Debug", null);
        }
        public static void logWarn(string Message)
        {
            logger.Warn(DateTime.Now.ToString() + "-" + Message);
            LogToDB(Message, "Warning", null);
        }
        public static void logFatal(string Message, Exception ex)
        {
            logger.Fatal(DateTime.Now.ToString() + "-" + Message, ex == null ? null : ex);
            LogToEventLog(Message, ex);
            LogToDB(Message, "Fatal", ex);
        }

        private static void LogToDB(string Message, string LogType, Exception ex)
        {
            string logType = LogType;
            int userId = SFConfiguration.GetUserID();
            string userSession = GetSessionID();
            string environment = SFConfiguration.GetEnvironment();
            string thread = System.Threading.Thread.CurrentThread.ManagedThreadId.ToString();
            string functionName = "";
            string message = string.Empty;
            if (!string.IsNullOrEmpty(Message))
                message = Message + "," + GetExceptionMessage(ex);
            else
                message = GetExceptionMessage(ex);
            string payload = "";
            string stackTrace = GetStackTrace(ex);
            string clientIP = GetClientIP();
            int createdBy = SFConfiguration.GetUserID();

           DataAccess.SaveSFLog(logType, userId, userSession, environment, thread, functionName, message, payload, stackTrace, createdBy, clientIP);
        }

        private static void LogTravelSAASLogsToDB(string Message, string functionName, string LogType, string RequestOrResponse)
        {
            string logType = LogType;
            int userId = SFConfiguration.GetUserID();
            string userSession = GetSessionID();
            string environment = SFConfiguration.GetEnvironment();
            string thread = System.Threading.Thread.CurrentThread.ManagedThreadId.ToString();

            string clientIP = GetClientIP();
            int createdBy = SFConfiguration.GetUserID();

            DataAccess.SaveSFLog(logType, userId, userSession, environment, thread, functionName, Message, RequestOrResponse.Replace("'",""), string.Empty, createdBy, clientIP);
        }

        private static string GetClientIP()
        {
            string ip = "UnKnown";
            if (System.Web.HttpContext.Current != null)
            {
                ip = System.Web.HttpContext.Current.Request.ServerVariables["HTTP_X_FORWARDED_FOR"];
                if (string.IsNullOrEmpty(ip))
                {
                    ip = System.Web.HttpContext.Current.Request.ServerVariables["REMOTE_ADDR"];
                }
            }
            return ip;
        }

        public static string GetSessionID()
        {
            string strSession = string.Empty;
            if (HttpContext.Current != null)
            {
                strSession = HttpContext.Current.Session.SessionID;
            }
            return strSession;
        }

        private static string GetEnvironment()
        {
            return SFConfiguration.GetEnvironment();
        }

        private static void LogToEventLog(string Mesasge, Exception ex)
        {
            StringBuilder stringBuilder = new StringBuilder();
            stringBuilder.Append(ex.Message);
            if (ex.InnerException != null)
            {
                stringBuilder.Append(ex.InnerException.Message);
            }
            stringBuilder.Append(ex.StackTrace);
            if (!System.Diagnostics.EventLog.SourceExists("SwordFish"))
                System.Diagnostics.EventLog.CreateEventSource("SwordFish", "SwordFish");
            System.Diagnostics.EventLog eventLog = new System.Diagnostics.EventLog("SwordFish");
            eventLog.Source = "SwordFish";
            if (stringBuilder != null && stringBuilder.ToString() == "")
            {
                eventLog.WriteEntry(stringBuilder.ToString());
            }


        }

        private static string GetExceptionMessage(Exception ex)
        {
            string retValue = string.Empty;
            if (ex != null)
            {
                StringBuilder stringBuilder = new StringBuilder();
                stringBuilder.Append(ex.Message);
                if (ex.InnerException != null)
                {
                    stringBuilder.Append(ex.InnerException.Message);
                }
                retValue = stringBuilder.ToString();
            }
            return retValue.Replace("'", "");
        }

        private static string GetStackTrace(Exception ex)
        {
            string retVal = string.Empty;
            if (ex != null)
            {
                StringBuilder stringBuilder = new StringBuilder();
                stringBuilder.Append(ex.StackTrace);
                if (ex.InnerException != null)
                {
                    stringBuilder.Append(ex.InnerException.StackTrace);
                }
                retVal = stringBuilder.ToString();
            }

            return retVal;
        }

    }
}