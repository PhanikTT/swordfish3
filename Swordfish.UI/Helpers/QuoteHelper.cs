﻿using Swordfish.Businessobjects.Customers;
using Swordfish.Businessobjects.Quotes;
using System;
using System.Collections.Concurrent;
using System.Linq;
using System.Threading.Tasks;
using Swordfish.Businessobjects.Flights;
using Swordfish.Businessobjects.Hotels;
using Common = Swordfish.Businessobjects.Common;
using Swordfish.Components.BookingServices;

namespace Swordfish.UI.Helpers
{
    public class QuoteHelper
    {
        private IBookingService PackagingRepository;
        public QuoteHelper(IBookingService packagingRepository)
        {
            PackagingRepository = packagingRepository;
        }
        public string SaveQuote(Swordfish.Businessobjects.Quotes.Quote basket)
        {          
            return PackagingRepository.SaveBasket(basket);

        }
        public ConcurrentBag<CustomerQuotes> GetCustomerQuotes(int CustomerId)
        {
            return GetCustomerQuoteDetails(PackagingRepository, CustomerId).Result;
        }
        private Task<ConcurrentBag<CustomerQuotes>> GetCustomerQuoteDetails(IBookingService packagingRepository, int CustomerId)
        {
            return GetCustomerRecentQuotes(packagingRepository, CustomerId);
        }
        private async Task<ConcurrentBag<CustomerQuotes>> GetCustomerRecentQuotes(IBookingService packagingRepository, int CustomerId)
        {
            var cutomerQuotes = await packagingRepository.GetCustomerQuotes(CustomerId).ConfigureAwait(false);
            return cutomerQuotes;
        }
        public Swordfish.Businessobjects.Quotes.Quote LoadQuoteData(Swordfish.Businessobjects.Quotes.Quote QuoteObject)
        {
            try
            {
                if (SFConfiguration.GetTTSSQSearch() != null)
                    QuoteObject.searchModel.TTSSQuoteId = SFConfiguration.GetTTSSQSearch();
                int agentId = SFConfiguration.GetUserID();
                QuoteObject.searchModel.CreatedBy = agentId;
                if (QuoteObject.transferComponents != null)
                {
                    foreach (var transferComponent in QuoteObject.transferComponents)
                    {
                        transferComponent.CreatedBy = agentId;
                        transferComponent.ModifiedBy = agentId;
                        if (transferComponent != null)
                            foreach (var transfercomponentsdetails in QuoteObject.transferComponents)
                            {
                                transfercomponentsdetails.CreatedBy = agentId;
                                transfercomponentsdetails.ModifiedBy = agentId;

                            }
                    }
                }

                QuoteObject.quoteHeader.CreatedBy = agentId;
                if (QuoteObject.bagsComponents != null)
                    QuoteObject.bagsComponents.Where(x => x.CreatedBy == agentId);
                if (QuoteObject.hotelComponent != null)
                {
                    QuoteObject.hotelComponent.CreatedBy = agentId;
                    QuoteObject.hotelComponent.ModifiedBy = agentId;
                    if (QuoteObject.hotelComponent.hotelComponentsDetails != null)
                        foreach (var hotelComponentsDetails in QuoteObject.hotelComponent.hotelComponentsDetails)
                        {
                            hotelComponentsDetails.CreatedBy = agentId;
                            hotelComponentsDetails.ModifiedBy = agentId;
                        }

                }
                if (QuoteObject.flightComponents != null)
                {
                    foreach (var FlightComponent in QuoteObject.flightComponents)
                    {
                        FlightComponent.CreatedBy = agentId;
                        FlightComponent.ModifiedBy = agentId;
                        if (FlightComponent.FlightComponentDetails != null)
                            foreach (var flightcomponentsdetails in FlightComponent.FlightComponentDetails)
                            {
                                flightcomponentsdetails.CreatedBy = agentId;
                                flightcomponentsdetails.ModifiedBy = agentId;

                            }
                    }

                }               
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return QuoteObject;

        }       
        public Common.SearchData getsearchdata(string sfquote)
        {
            Common.SearchData searchmodel = new Common.SearchData();
            searchmodel = PackagingRepository.getsearchModel(sfquote);
            return searchmodel;
        }
        public ConcurrentBag<DestinationHotels> GetDestinationHotels(string supplierCode, string destinationCode)
        {
            return GetDestinationsHotelsList(PackagingRepository, supplierCode, destinationCode).Result;
        }
        private Task<ConcurrentBag<DestinationHotels>> GetDestinationsHotelsList(IBookingService packagingRepository, string supplierCode, string destinationCode)
        {
            return GetListofDestinationHotels(packagingRepository, supplierCode, destinationCode);
        }
        private async Task<ConcurrentBag<DestinationHotels>> GetListofDestinationHotels(IBookingService packagingRepository, string supplierCode, string destinationCode)
        {
            var hotelDetails = await packagingRepository.GetHotelList(supplierCode, destinationCode).ConfigureAwait(false);
            return hotelDetails;
        }
    }
}