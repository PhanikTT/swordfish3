﻿using Swordfish.Businessobjects.Quotes;
using Swordfish.Components.BookingServices;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace Swordfish.UI.Helpers
{
    public class CoBrowsingHelper
    {
        public Quote LoadQuoteData(string quoteHeaderID)
        {
           var quoteComponents =  new CommonHelper().LoadQuoteData(quoteHeaderID);
            return quoteComponents;
        }

        public Task<int> SaveCoBrowseConformationResponce(int quoteHeaderID, string guId, string responseMsg)
        {
             SFLogger.logMessage( "SaveConformationResponce() calling started!!.For User " + SFConfiguration.GetUserID().ToString() + " at " + DateTime.Now.ToString());
            var serviceRepository = new BookingService();
            var result = Task.Run(() => serviceRepository.SaveCoBrowseConformationResponse(quoteHeaderID,guId,responseMsg));
            SFLogger.logMessage( "SaveConformationResponce() calling Ended!!.For User " + SFConfiguration.GetUserID().ToString() + " at " + DateTime.Now.ToString());
            return result;
        }

        public void ConvertBaggageSlabs(Quote quote)
        {
            if (quote.bagsComponents.Count > 0)
            {
                var baggages = quote.bagsComponents.Where(x => x.BagsType.ToLower() == "baggage").ToList();
                if (baggages.Count > 0)
                {
                    foreach (var item in baggages)
                    {
                        var additionWeight = quote.bagsComponents.Where(x => x.BagsType.ToLower() == "additional_weight" && x.Direction == item.Direction && x.PassengerIndex == item.PassengerIndex).FirstOrDefault();
                        if (additionWeight != null)
                        {
                            item.BagsDescription += "(" + additionWeight.BagsDescription + ")";
                            item.Price += additionWeight.Price;
                            item.SellingPrice += additionWeight.SellingPrice;
                            item.BuyingPrice += additionWeight.BuyingPrice;
                            item.Items += additionWeight.Items;
                        }
                        quote.bagsComponents.Remove(additionWeight);
                    }
                }
            }
        }
    }
}