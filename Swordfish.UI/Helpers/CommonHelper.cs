﻿using Swordfish.Businessobjects.Common;
using Swordfish.Businessobjects.Passengers;
using Swordfish.Businessobjects.Payments;
using Swordfish.Businessobjects.Quotes;
using Swordfish.Businessobjects.Sesame;
using Swordfish.Businessobjects.TopDog;
using Swordfish.Businessobjects.Transfers;
using Swordfish.Components.BookingServices;
using Swordfish.UI.Models.Hotel;
using System;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Web;

namespace Swordfish.UI.Helpers
{
    public class CommonHelper
    {

        private IBookingService PackagingRepository;
        public CommonHelper()
        {
            PackagingRepository = new BookingService();
        }
        public List<Passenger> GetPassengers(string quoteHeaderID)
        {
            SFLogger.logMessage ( "GetPassengersData() calling started!!.For User " + SFConfiguration.GetUserID().ToString() + " at " + DateTime.Now.ToString());
            var result = PackagingRepository.Getpassenger(quoteHeaderID);
            result = DateOfBirthManipulation(result);
            SFLogger.logMessage( "GetPassengersData() calling Ended!!.For User " + SFConfiguration.GetUserID().ToString() + " at " + DateTime.Now.ToString());
            return result;
        }
        public List<Passenger> DateOfBirthManipulation(List<Passenger> passengers)
        {
            SFLogger.logMessage("DateOfBirthManipulation() calling started!!.For User " + SFConfiguration.GetUserID().ToString() + " at " + DateTime.Now.ToString());            
            foreach (var item in passengers)
            {
                if (string.IsNullOrEmpty(item.DateOfBirth))
                {
                    if (item.Type == "Adult")
                    {
                        item.DefaultDateOfBirth = SFConfiguration.GetTopDogDefaultAdultAge();
                    }
                    if (item.Type == "Child")
                    {
                        item.DefaultDateOfBirth = SFConfiguration.GetTopDogDefaultChildAge();
                    }
                    if (item.Type == "Infant")
                    {
                        item.DefaultDateOfBirth = SFConfiguration.GetTopDogDefaultInfantAge();
                    }
                }
            }
            SFLogger.logMessage("DateOfBirthManipulation() calling Ended!!.For User " + SFConfiguration.GetUserID().ToString() + " at " + DateTime.Now.ToString());
            return passengers;
        }
        public Quote LoadQuoteData(string QuoteHeaderId)
        {
            SFLogger.logMessage("SearchResultsHelper:GetQuoteComponentsData(): started!!.");
            var srcJsonPath = HttpContext.Current.Server.MapPath("~/JsonData/Source.json");
            var destJsonPath = HttpContext.Current.Server.MapPath("~/JsonData/destination.json");
            Quote quoteData = new Quote();
            BookingService serviceRepository = new BookingService();
            quoteData = serviceRepository.loadQuoteData(QuoteHeaderId);
            quoteData.UserInfo = SFConfiguration.GetUserInfo();
            if (quoteData.flightComponentUI.flightComponentsInbound != null && quoteData.flightComponentUI.flightComponentsInbound.Count > 0)
            {
                foreach (var flightInBound in quoteData.flightComponentUI.flightComponentsInbound)
                {
                    flightInBound.DepartureAirportName = SFConfiguration.GetAirportName(flightInBound.DepartureAirportCode, destJsonPath, srcJsonPath);
                    flightInBound.ArrivalAirportName = SFConfiguration.GetAirportName(flightInBound.ArrivalAirportCode, destJsonPath, srcJsonPath);
                }
            }
            if (quoteData.flightComponentUI.flightComponentsOutbound != null && quoteData.flightComponentUI.flightComponentsOutbound.Count > 0)
            {
                foreach (var flightOutBound in quoteData.flightComponentUI.flightComponentsOutbound)
                {
                    flightOutBound.DepartureAirportName = SFConfiguration.GetAirportName(flightOutBound.DepartureAirportCode, destJsonPath, srcJsonPath);
                    flightOutBound.ArrivalAirportName = SFConfiguration.GetAirportName(flightOutBound.ArrivalAirportCode, destJsonPath, srcJsonPath);
                }
            }
            if (quoteData.hotelComponent != null)
            {
                quoteData.hotelComponent.SupplierResortName = SFConfiguration.GetResortAndHotelRatings("", "", quoteData.hotelComponent.SupplierResortId).ResortName;
            }
            quoteData.SesameBasket = GetComponentDetails(Convert.ToInt32(QuoteHeaderId), string.Empty, SFConstants.SesameBasket_Create);
            quoteData.hotelComponent.HotelInformatoin = SearchResultsHelper.GetHotelInfo(new HotelInfoRequest { SupplierAccommId = quoteData.hotelComponent.HotelCode, TTICode = Convert.ToString(quoteData.hotelComponent.TTICode) });
            quoteData.HotelOperatorCode = SFConfiguration.GetHotelOperatorCode();
            quoteData.TransferOperatorCode = SFConfiguration.GetTransferOperatorCode();
            quoteData.FlightOperatorCode = SFConfiguration.GetFlightOperatorCode();
            quoteData.FlightServiceClass = SFConfiguration.GetFlightServiceClass();
            quoteData.Currency = SFConfiguration.GetCurrency();
            quoteData.TopDogUserId = SFConfiguration.GetTopDogUserId();
            quoteData.TopDogPassword = SFConfiguration.GetTopDogPassword();
            quoteData.TopDogLanguage = SFConfiguration.GetTopDogLanguage();
            quoteData.CCNumber = SFConfiguration.GetCCNumber();
            quoteData.CCType = SFConfiguration.GetCCType();
            quoteData.CCStartDate = SFConfiguration.GetCCStartDate();
            quoteData.CCEndDate = SFConfiguration.GetCCEndDate();
            quoteData.SecurityNum = SFConfiguration.GetSecurityNum();
            quoteData.ArrivalPickUp = SFConfiguration.GetArrivalPickUp();
            quoteData.DeparturePickUp = SFConfiguration.GetDeparturePickUp();
            quoteData.AddressType = SFConfiguration.GetAddressType();
            quoteData.PhoneType = SFConfiguration.GetPhoneType();
            quoteData.EmailType = SFConfiguration.GetEmailType();
            quoteData.TopDogHotelCommision = SFConfiguration.GetTopDogHotelCommision();
            quoteData.TopDogTransferCommision = SFConfiguration.GetTopDogTransferCommision();
            quoteData.TopDogPassengerNationality = SFConfiguration.GetTopDogPassengerNationality();
            quoteData.SesameBookBasket.DepositDays = SFConfiguration.GetDepositDays();
            quoteData.CardEasyEndPoint = SFConfiguration.GetCardEasyEndPoint();
            SFLogger.logMessage("SearchResultsHelper:GetQuoteComponentsData(): end.");
            return quoteData;
        }
        public SesameBasket GetComponentDetails(int quoteHeaderId, string basketId, int typeId)
        {
            SFLogger.logMessage("SearchResultsHelper:GetResort(): started!!. at :" + DateTime.Now);
            var serviceRepository = new BookingService();
            var result = serviceRepository.GetComponentDetails(quoteHeaderId, basketId, typeId);
            return result;
        }

        public TransfersBookData GetHolidayTaxisData(int quoteHeaderId)
        {
            SFLogger.logMessage("SearchResultsHelper:GetHolidayTaxisData(): started!!. at :" + DateTime.Now);
            var serviceRepository = new BookingService();
            var result = serviceRepository.GetHolidayTaxisData(quoteHeaderId);
            return result;
        }
        public List<KeyValuePair<string, string>> GetLowCostAirLines()
        {
            return PackagingRepository.GetOptionsForValue(SFConstants.LowCostAirlineValueId);
        }
        public List<KeyValuePair<string, string>> GetOptionsForValue(int valueId)
        {
            return PackagingRepository.GetOptionsForValue(valueId);
        }
        public int GetAgeFromDateOfBirth(DateTime birthDay)
        {
            try
            {
                DateTime today = DateTime.Today;
                int age = today.Year - birthDay.Year;

                if (birthDay > today.AddYears(-age))
                    age--;

                return age;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public int GetAgeFromDateOfBirth(string birthDay)
        {
            if (!string.IsNullOrEmpty(birthDay))
            {
                try
                {
                    DateTime bDay = new DateTime();
                    var converted = DateTime.TryParse(birthDay,out bDay);
                    DateTime today = DateTime.Today;
                    int age = today.Year - bDay.Year;

                    if (bDay > today.AddYears(-age))
                        age--;

                    return age;
                }
                catch (Exception ex)
                {
                    throw ex;
                } 
            }
            else
            {
                throw new ArgumentNullException("birthDay is null: GetAgeFromDateOfBirth");
            }
        }

        public Quote LoadQuoteDataWithHotelInformation(string quoteHeaderId)
        {
            Quote result = null;
            try
            {
                if (!string.IsNullOrEmpty(quoteHeaderId))
                {
                    result = LoadQuoteData(quoteHeaderId);
                    result.hotelComponent.HotelInformatoin = SearchResultsHelper.GetHotelInfo(new HotelInfoRequest { SupplierAccommId = result.hotelComponent.HotelCode, TTICode = Convert.ToString(result.hotelComponent.TTICode) });
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return result;
        }

        public List<PaymentDetail> GetCompletePaymentDetails(int quoteHeaderID)
        {
            SFLogger.logMessage("SearchResultsHelper:GetCompletePaymentDetails(): started!!. at :" + DateTime.Now);
            var serviceRepository = new BookingService();
            var result = serviceRepository.GetCompletePaymentDetails(quoteHeaderID);
            SFLogger.logMessage("SearchResultsHelper:GetCompletePaymentDetails(): ended!!. at :" + DateTime.Now);
            return result;
        }

        public string getPostCode(string postCode)
        {
            string responseText = string.Empty;
            HttpStatusCode wRespStatusCode;
            try {
                string url = SFConfiguration.GetopenPostCodeUrl();
                string key = SFConfiguration.GetopenPostCodeKey();
                string postcode = postCode;
                string requestURL = url + "/" + postcode + "?api_key=" + key;
                var httpWebRequest = (HttpWebRequest)WebRequest.Create(requestURL);
                httpWebRequest.Method = "GET";
                httpWebRequest.Accept = "application/json";
                var httpResponse = (HttpWebResponse)httpWebRequest.GetResponse();
                wRespStatusCode = httpResponse.StatusCode;
                using (var streamReader = new StreamReader(httpResponse.GetResponseStream()))
                {
                    responseText = streamReader.ReadToEnd();
                }
            }
            catch( WebException webex)
            {
                wRespStatusCode = ((HttpWebResponse)webex.Response).StatusCode;
                responseText = wRespStatusCode.ToString();
            }
            return responseText;

        }
        public List<ExtraCharge> GetExtraCharges()
        {
            List<ExtraCharge> ExtraChargeLst = new List<ExtraCharge>();
            return PackagingRepository.GetExtraCharges();
        }
    }
}