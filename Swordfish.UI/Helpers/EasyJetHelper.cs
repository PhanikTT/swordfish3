﻿using Swordfish.Businessobjects.Common;
using Swordfish.Businessobjects.Easyjet;
using Swordfish.Businessobjects.Passengers;
using Swordfish.Businessobjects.Quotes;
using Swordfish.UI.Models.Easyjet;
using Swordfish.Utilities.DataAccess;
using Swordfish.Utilities.WEXCard;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Globalization;
using System.Linq;

namespace Swordfish.UI.Helpers
{
    public static class EasyJetHelper
    {
        public static EasyJetBasketModel GetEJQuoteData(string QuoteHeaderId, int ComponentTypeOptionId, bool PriceCalculation = false)
        {
            CommonHelper commonHelper = new CommonHelper();
            Quote quoteData = new Quote();
            CreateCardResponse WEXCard = null;
            quoteData = commonHelper.LoadQuoteData(QuoteHeaderId);
            quoteData.PopulateBagsComponentForEzJet();
            List<Passenger> pax = commonHelper.GetPassengers(QuoteHeaderId);
            string connectionString = SFConfiguration.GetchilliDataBase() != null ? SFConfiguration.GetchilliDataBase() : null;
            string wexCardHolderName = SFConfiguration.GetWEXCardHolderName() != null ? SFConfiguration.GetWEXCardHolderName() : null;
            string cardType = SFConfiguration.GetWEXCardType() != null ? SFConfiguration.GetWEXCardType() : null;
            string defaultAdultAge = SFConfiguration.GetEJAdultAge() != null ? SFConfiguration.GetEJAdultAge() : null;
            string defaultChildAge = SFConfiguration.GetEJChildAge() != null ? SFConfiguration.GetEJChildAge() : null;
            string priceType = SFConfiguration.GetEJPriceType() != null ? SFConfiguration.GetEJPriceType() : null;
            string demoMode = SFConfiguration.GetEJDemoMode() != null ? SFConfiguration.GetEJDemoMode() : null;
            string currency = SFConfiguration.GetEJCurrency() != null ? SFConfiguration.GetEJCurrency() : null;
            string operatingAirline = SFConfiguration.GetEJOperatingAirline() != null ? SFConfiguration.GetEJOperatingAirline() : null;
            string email = SFConfiguration.GetEJEmail() != null ? SFConfiguration.GetEJEmail() : null;
            string password = SFConfiguration.GetEJPassword() != null ? SFConfiguration.GetEJPassword() : null;
            string EJpaxflightadult = SFConfiguration.GetEJPaxFlightAdult() != null ? SFConfiguration.GetEJPaxFlightAdult() : null;
            string EJpaxflightchild = SFConfiguration.GetEJPaxFlightChild() != null ? SFConfiguration.GetEJPaxFlightChild() : null;
            string EJpaxflightinfant = SFConfiguration.GetEJPaxFlightInfant() != null ? SFConfiguration.GetEJPaxFlightInfant() : null;
            EasyJetBasketModel basket = null;
            if (!string.IsNullOrEmpty(connectionString))
            {
                decimal AdultPrice = 0;
                decimal ChildPrice = 0;
                decimal InfantPrice = 0;
                //Filtering for Only Flight components and easyjet components
                var easyJetFlightComponents = quoteData.SesameBasket.BasketComponents
                    .Where(n => n.ProductTypeCode.Equals(SFConstants.SesameFlightComponentIdentifier) &&
                    n.ComponentFlightInformation.SelectMany(s => s.Legs)
                    .Where(l => l.MarketingAirline.Equals(SFConstants.EasyJetMarketingAirline)).Any());
                //Calculating Adult,Child,Infant prices
                var easyJetComponents = easyJetFlightComponents.Where(d => ComponentTypeOptionId == SFConstants.QuoteComponent_FlightReturn ? true : d.DirectionOptionId == ComponentTypeOptionId).SelectMany(n => n.ComponentDetails);
                if (easyJetComponents != null && easyJetComponents.Count() > 0)
                {
                    if (easyJetComponents.First().SourceCurrency.Equals(SFConstants.EUROCurrencyCode))
                    {
                        currency = SFConstants.EUROCurrencyCode;
                    }
                    foreach (var ComponentDetails in easyJetComponents)
                    {
                        if (!string.IsNullOrEmpty(ComponentDetails.Type))
                        {
                            if (ComponentDetails.SourceCurrency.Equals(SFConstants.EUROCurrencyCode))
                            {
                                if (ComponentDetails.Type.Contains(EJpaxflightadult))
                                {
                                    AdultPrice += Convert.ToDecimal(ComponentDetails.SourcePricePence) / 100;
                                }
                                if (ComponentDetails.Type.Contains(EJpaxflightchild))
                                {
                                    ChildPrice += Convert.ToDecimal(ComponentDetails.SourcePricePence) / 100;
                                }
                                if (ComponentDetails.Type.Contains(EJpaxflightinfant))
                                {
                                    InfantPrice += Convert.ToDecimal(ComponentDetails.SourcePricePence) / 100;
                                }
                            }
                            else if (ComponentDetails.SourceCurrency.Equals(SFConstants.GBPCurrencyCode))
                            {
                                if (ComponentDetails.Type.Contains(EJpaxflightadult))
                                {
                                    AdultPrice += Convert.ToDecimal(ComponentDetails.SellingPricePence) / 100;
                                }
                                if (ComponentDetails.Type.Contains(EJpaxflightchild))
                                {
                                    ChildPrice += Convert.ToDecimal(ComponentDetails.SellingPricePence) / 100;
                                }
                                if (ComponentDetails.Type.Contains(EJpaxflightinfant))
                                {
                                    InfantPrice += Convert.ToDecimal(ComponentDetails.SellingPricePence) / 100;
                                }
                            }
                        }
                    }
                }
                AdultPrice /= quoteData.searchModel.AdultsCount;
                if (quoteData.searchModel.ChildrenCount > 0)
                    ChildPrice /= quoteData.searchModel.ChildrenCount;
                if (quoteData.searchModel.InfantsCount > 0)
                    InfantPrice /= quoteData.searchModel.InfantsCount;
                if (!PriceCalculation)
                {
                    CardCreator cardCreator = new CardCreator(connectionString);
                    WEXCard = cardCreator.GetWexCard(QuoteHeaderId);
                }
                basket = new EasyJetBasketModel()
                {
                    quoteHeaderId = QuoteHeaderId,
                    ComponentTypeOptionId = ComponentTypeOptionId,
                    adultPrice = AdultPrice,
                    childPrice = ChildPrice,
                    infantPrice = InfantPrice,
                    adultsCount = quoteData.searchModel.AdultsCount,
                    childrenCount = quoteData.searchModel.ChildrenCount,
                    infantsCount = quoteData.searchModel.InfantsCount,
                    priceType = priceType,
                    demoMode = false,
                    currency = currency,
                    cardType = cardType,
                    cardHolderName = wexCardHolderName,
                    priceMatching = false,
                    accountInfo = new List<EasyJetLoginModel>()
                                        {
                                            new EasyJetLoginModel()
                                            {
                                                email = email,
                                                password = password
                                            }
                                        }
                };
                if (WEXCard != null)
                {
                    basket.cv2 = WEXCard.cv2;
                    basket.expireDate = Convert.ToDateTime(WEXCard.expiryDate).ToString("MMyy");
                    basket.cardNumber = WEXCard.PAN;
                }
                else
                {
                    if (!PriceCalculation)
                    {
                        throw new Exception("WEX Card Not found");
                    }
                }
                if (!PriceCalculation)
                {
                    List<EasyJetRouteModel> routes = new List<EasyJetRouteModel>();
                    foreach (var flightComponent in quoteData.flightComponents)
                    {
                        foreach (var details in flightComponent.FlightComponentDetails.Where(c => ComponentTypeOptionId == SFConstants.QuoteComponent_FlightReturn ? true : c.DirectionOptionId == ComponentTypeOptionId))
                        {
                            if (details.MarketingAirlineCode.Equals(SFConstants.EasyJetMarketingAirlineCode))
                            {
                                EasyJetRouteModel route = new EasyJetRouteModel();
                                route.departureAirport = details.DepartureAirportCode;
                                route.arrivalAirport = details.ArrivalAirportCode;
                                route.departureDateTime = details.DepartureDateTime.ToString("yyyy-MM-ddTHH:mm:ss", CultureInfo.InvariantCulture);
                                route.arrivalDateTime = details.ArrivalDateTime.ToString("yyyy-MM-ddTHH:mm:ss", CultureInfo.InvariantCulture);
                                route.operatingAirline = operatingAirline;
                                route.flightNumber = details.FlightNumber;
                                if (flightComponent.Source.Equals(details.DepartureAirportCode))
                                {
                                    if (details.DirectionOptionId == SFConstants.Direction_Outbound)
                                        routes.Insert(0, route);
                                    else if (details.DirectionOptionId == SFConstants.Direction_Inbound)
                                        routes.Add(route);
                                }
                                else if (flightComponent.Arrival.Equals(details.ArrivalAirportCode))
                                {
                                    if (details.DirectionOptionId == SFConstants.Direction_Outbound)
                                        routes.Insert(0, route);
                                    else if (details.DirectionOptionId == SFConstants.Direction_Inbound)
                                        routes.Add(route);
                                }
                                else if (flightComponent.ComponentTypeOptionId == SFConstants.QuoteComponent_FlightReturn)
                                {
                                    if (flightComponent.Source.Equals(details.ArrivalAirportCode))
                                    {
                                        if (details.DirectionOptionId == SFConstants.Direction_Outbound)
                                            routes.Insert(0, route);
                                        else if (details.DirectionOptionId == SFConstants.Direction_Inbound)
                                            routes.Add(route);
                                    }
                                    else if (flightComponent.Arrival.Equals(details.DepartureAirportCode))
                                    {
                                        if (details.DirectionOptionId == SFConstants.Direction_Outbound)
                                            routes.Insert(0, route);
                                        else if (details.DirectionOptionId == SFConstants.Direction_Inbound)
                                            routes.Add(route);
                                    }
                                }
                            }
                        }
                    }
                    basket.route = routes;
                }
                if (!PriceCalculation)
                {
                    List<EasyJetPassengersModel> passengers = new List<EasyJetPassengersModel>();
                    List<int> childAges = new List<int>();
                    if (pax != null && pax.Count > 0)
                    {
                        foreach (var item in pax)
                        {
                            EasyJetPassengersModel passenger = new EasyJetPassengersModel();
                            passenger.title = item.Title;
                            passenger.firstName = item.FirstName;
                            if (item.PassengerTypeOptionId == SFConstants.PassengerType_Adult)
                            {
                                if (!string.IsNullOrEmpty(item.DateOfBirth) || !string.IsNullOrEmpty(item.DefaultDateOfBirth))
                                    passenger.age = commonHelper.GetAgeFromDateOfBirth(item.DateOfBirth ?? item.DefaultDateOfBirth).ToString();
                                else
                                    passenger.age = defaultAdultAge;
                                passenger.personAgeCode = "A";
                            }
                            else if (item.PassengerTypeOptionId == SFConstants.PassengerType_Child)
                            {
                                if (!string.IsNullOrEmpty(item.DateOfBirth) || !string.IsNullOrEmpty(item.DefaultDateOfBirth))
                                    passenger.age = commonHelper.GetAgeFromDateOfBirth(item.DateOfBirth ?? item.DefaultDateOfBirth).ToString();
                                else
                                    passenger.age = defaultChildAge;
                                passenger.personAgeCode = "C";
                                int childAge = 0;
                                int.TryParse(passenger.age, out childAge);
                                childAges.Add(childAge);
                            }
                            else if (item.PassengerTypeOptionId == SFConstants.PassengerType_Infant)
                            {
                                passenger.personAgeCode = "I";
                            }

                            passenger.surname = item.LastName;
                            passengers.Add(passenger);
                        }
                    }
                    basket.passengers = passengers;
                    basket.childAges = childAges;
                }
                List<EasyJetSupplementsModel> supplements = new List<EasyJetSupplementsModel>();
                foreach (var item in quoteData.bagComponentEZJets.Where(n => ComponentTypeOptionId == SFConstants.QuoteComponent_FlightReturn ? true : n.DirectionOptionId == ComponentTypeOptionId))
                {
                    EasyJetSupplementsModel supplement = new EasyJetSupplementsModel();
                    //Not checking for bags or sportitems
                    //Because we are not supporting sports items in SF
                    supplement.adultsCount = item.BagsCount;
                    supplement.childrenCount = 0;
                    supplement.infantsCount = 0;
                    supplement.adultPricePricePerUnit = item.TotalBagsCost;
                    supplement.childPricePricePerUnit = item.TotalBagsCost;
                    supplement.infantPricePricePerUnit = item.TotalBagsCost;
                    supplement.description = item.TotalWeight + "kg hold bag";
                    supplement.type = "BAGGAGE";
                    supplements.Add(supplement);
                }
                basket.supplements = supplements;
            }
            return basket;
        }
        public static decimal GetTotalAmount(EasyJetBasketModel model)
        {
            decimal totalAmount = default(decimal);
            totalAmount += (model.adultPrice * model.adultsCount) + (model.childPrice * model.childrenCount) + (model.infantPrice * model.infantsCount);
            foreach (var item in model.supplements)
            {
                totalAmount += (item.adultPricePricePerUnit * item.adultsCount);
            }
            return totalAmount;
        }
        public static void SaveEasyJetBooking(EasyJetResponse easyJetBooking)
        {
            string connectionString = ConfigurationManager.ConnectionStrings["chilliDataBase"] != null ? ConfigurationManager.ConnectionStrings["chilliDataBase"].ToString() : null;
            if (!string.IsNullOrEmpty(connectionString))
            {
                var db = new CommandRunner(connectionString);
                string sql = "select * from save_easy_jet_bookings(" + easyJetBooking.ComponentTypeOptionId + ",'" + easyJetBooking.quoteHeaderId + "'," + easyJetBooking.adultsCount + "," + easyJetBooking.childrenCount + "," + easyJetBooking.infantsCount + ",'" + easyJetBooking.currencyCode + "','" + easyJetBooking.priceType + "','" + easyJetBooking.supplierRef + "'," + easyJetBooking.adultPricePP + "," + easyJetBooking.childPricePP + "," + easyJetBooking.infantPricePP + "," + easyJetBooking.totalPriceOnConfirmStage + "," + easyJetBooking.cardFeeOnConfirmStage + ",'" + easyJetBooking.confirmationPage.Replace("'", "").Replace(":", "") + "','" + easyJetBooking.unexpectedCnfPage + "')";
                var res = db.ExecuteDynamic(sql);
                foreach (var r in res)
                {
                    var s = r;
                }
            }
        }
        public static EasyJetResponse GetEasyJetBooking(string quoteHeaderId, int componentTypeOptionId)
        {
            string connectionString = ConfigurationManager.ConnectionStrings["chilliDataBase"] != null ? ConfigurationManager.ConnectionStrings["chilliDataBase"].ToString() : null;
            EasyJetResponse card = null;
            if (!string.IsNullOrEmpty(connectionString))
            {
                var db = new CommandRunner(connectionString);
                string sql = "select * from get_easyjet_booking('" + quoteHeaderId + "'," + componentTypeOptionId + ")";
                var res = db.ExecuteDynamic(sql);

                foreach (var item in res)
                {
                    card = new EasyJetResponse();
                    card.ComponentTypeOptionId = item.componenttypeoptionid;
                    card.quoteHeaderId = item.quoteheaderid;
                    card.adultsCount = item.adultscount;
                    card.childrenCount = item.childrencount;
                    card.infantsCount = item.infantscount;
                    card.currencyCode = item.currencycode;
                    card.priceType = item.pricetype;
                    card.supplierRef = item.supplierref;
                    card.adultPricePP = item.adultpricepp;
                    card.childPricePP = item.childpricepp;
                    card.infantPricePP = item.infantpricepp;
                    card.totalPriceOnConfirmStage = item.totalpriceonconfirmstage;
                    card.cardFeeOnConfirmStage = item.cardfeeonconfirmstage;
                    card.confirmationPage = item.confirmationpage;
                    card.unexpectedCnfPage = item.unexpectedcnfpage;
                    card.CreatedDate = item.createddate;
                }
            }
            return card;
        }
        public static void FillEasyJetResponses(List<EasyJetBookingStatus> model, string quoteHeaderId, int componentTypeOptionId)
        {
            if (componentTypeOptionId == SFConstants.QuoteComponent_FlightReturn)
            {
                var response = EasyJetHelper.GetEasyJetBooking(quoteHeaderId, SFConstants.QuoteComponent_FlightReturn);
                EasyJetBookingStatus result = new EasyJetBookingStatus();
                result.componentTypeOptionId = SFConstants.QuoteComponent_FlightReturn;
                if (response != null)
                    result.totalPriceOnConfirmStage = response.totalPriceOnConfirmStage;
                if (response != null && !string.IsNullOrEmpty(response.supplierRef))
                {
                    result.eJReturnBookingSuccess = true;
                    result.eJReturnBookingRefId = response.supplierRef;
                    model.Add(result);
                }
                else if (response != null)
                {
                    result.eJReturnBookingSuccess = false;
                    result.eJReturnBookingRefId = null;
                    model.Add(result);
                }
            }
            else
            {
                var OutboundResponse = EasyJetHelper.GetEasyJetBooking(quoteHeaderId, SFConstants.Direction_Outbound);
                EasyJetBookingStatus OutboundResult = new EasyJetBookingStatus();
                OutboundResult.componentTypeOptionId = SFConstants.Direction_Outbound;
                if (OutboundResponse != null)
                    OutboundResult.totalPriceOnConfirmStage = OutboundResponse.totalPriceOnConfirmStage;
                if (OutboundResponse != null && !string.IsNullOrEmpty(OutboundResponse.supplierRef))
                {
                    OutboundResult.eJOutBoundBookingSuccess = true;
                    OutboundResult.eJOutBoundBookingRefId = OutboundResponse.supplierRef;
                    model.Add(OutboundResult);
                }
                else if (OutboundResponse != null)
                {
                    OutboundResult.eJOutBoundBookingSuccess = false;
                    OutboundResult.eJOutBoundBookingRefId = null;
                    model.Add(OutboundResult);
                }
                EasyJetResponse InboundResponse = EasyJetHelper.GetEasyJetBooking(quoteHeaderId, SFConstants.Direction_Inbound);
                EasyJetBookingStatus InboundResult = new EasyJetBookingStatus();
                InboundResult.componentTypeOptionId = SFConstants.Direction_Inbound;
                if (InboundResponse != null)
                    InboundResult.totalPriceOnConfirmStage = InboundResponse.totalPriceOnConfirmStage;
                if (InboundResponse != null && !string.IsNullOrEmpty(InboundResponse.supplierRef))
                {
                    InboundResult.eJInBoundBookingSuccess = true;
                    InboundResult.eJInBoundBookingRefId = InboundResponse.supplierRef;
                    model.Add(InboundResult);
                }
                else if (InboundResponse != null)
                {
                    InboundResult.eJInBoundBookingSuccess = false;
                    InboundResult.eJInBoundBookingRefId = null;
                    model.Add(InboundResult);
                }
            }
        }
    }
}