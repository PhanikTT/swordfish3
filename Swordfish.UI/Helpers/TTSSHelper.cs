﻿using Swordfish.Businessobjects.TTSS;
using Swordfish.Components.TTSSService;
using System.Threading.Tasks;

namespace Swordfish.UI.Helpers
{
    public class TTSSHelper
    {
        private ITTSSService Repository;

        public TTSSHelper(ITTSSService ttssRepository)
        {
            Repository = ttssRepository;
        }

        public TTSSQuote GetQuote(string requestUrl)
        {
            var ttssPackage = new TTSSQuote();
            var quoteList = Task.Run(() => GetQuoteDetails(Repository, requestUrl));
            Task.WhenAll(quoteList);
            ttssPackage = quoteList.Result;
            if (!string.IsNullOrEmpty(ttssPackage.HotelKey))
            {
                var hotelCodes = SFConfiguration.GetHotelCodesForIFFCode(ttssPackage.HotelKey);
                if (hotelCodes != null)
                {
                    ttssPackage.MasterHotelId = hotelCodes.Masterhotelid;
                    ttssPackage.TTICode = hotelCodes.TTICode;
                }
            }
            return ttssPackage;
        }
        private async Task<TTSSQuote> GetQuoteDetails(ITTSSService ttssRepository, string requestUrl)
        {
            TTSSQuote package = await ttssRepository.GetQuote(requestUrl).ConfigureAwait(false);
            return package;
        }
    }
}