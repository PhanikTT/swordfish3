﻿using System.Web;
using iTextSharp.text;
using iTextSharp.text.pdf;

namespace Swordfish.UI.Helpers
{
    public class AddHeaderFooterHelper : PdfPageEventHelper
    {
        public override void OnEndPage(PdfWriter writer, Document doc)
        {
            Paragraph footerAdd1 = new Paragraph("\nTeletext Holidays is a trading name of Truly Travel Ltd. ATOL No:T7300, TTA No:U6466.\n Address: 9th Floor, Holborn Tower 137-144 High Holborn London WC1V 6PW United Kingdom\n", FontFactory.GetFont("Verdana", 10, iTextSharp.text.Font.NORMAL, BaseColor.BLACK));
            string imageFilePath =HttpContext.Current.Server.MapPath(SFConfiguration.GetTTSSFooter());
            float cellHeight = doc.TopMargin;
            BaseColor Magenta = new BaseColor(116, 57, 122);
            iTextSharp.text.Image jpg = iTextSharp.text.Image.GetInstance(imageFilePath);
            jpg.ScaleToFit(80f, 200f);
            jpg.Alignment = Element.ALIGN_TOP;
          
            footerAdd1.Alignment = Element.ALIGN_CENTER;
            PdfPTable footerTbl = new PdfPTable(2);
            footerTbl.SetWidths(new int[] { 1, 4 });
            footerTbl.TotalWidth = 580;
            footerTbl.HorizontalAlignment = Element.ALIGN_CENTER;
            footerTbl.WidthPercentage = 50;
            PdfPCell cellAdd1 = new PdfPCell(footerAdd1);
            PdfPCell cellAdd2 = new PdfPCell(jpg); 
            cellAdd1.Border = 0;            
            cellAdd1.HorizontalAlignment = Element.ALIGN_LEFT;
            cellAdd2.Border = 0;

            cellAdd2.HorizontalAlignment = Element.ALIGN_CENTER;
            footerTbl.AddCell(cellAdd2);
            footerTbl.AddCell(cellAdd1);
            
            footerTbl.WriteSelectedRows(0, -1, doc.LeftMargin, doc.BottomMargin, writer.DirectContent);
            base.OnEndPage(writer, doc);
        }

        public override void OnStartPage(PdfWriter writer, Document doc)
        {
            string imageFilePath = HttpContext.Current.Server.MapPath(SFConfiguration.GetTTSSLogo());
            float cellHeight = doc.TopMargin;
            BaseColor Magenta = new BaseColor(116, 57, 122);
            iTextSharp.text.Image jpg = iTextSharp.text.Image.GetInstance(imageFilePath);
            jpg.ScaleToFit(150f, 200f);
            jpg.Alignment = Element.ALIGN_TOP;

            PdfPTable HeaderTbl = new PdfPTable(1);
            HeaderTbl.TotalWidth = doc.PageSize.Width;
            HeaderTbl.HorizontalAlignment = Element.ALIGN_RIGHT;
            HeaderTbl.WidthPercentage = 50;
            PdfPCell cellHder = new PdfPCell(jpg);
            cellHder.Border = 0;
            cellHder.PaddingLeft = 20;
            cellHder.PaddingRight = 10;
            cellHder.PaddingTop = 10;
            cellHder.HorizontalAlignment = Element.ALIGN_RIGHT;
            cellHder.VerticalAlignment = Element.ALIGN_RIGHT;
            HeaderTbl.AddCell(cellHder);
            doc.Add(HeaderTbl);
        }
    }
}