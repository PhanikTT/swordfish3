﻿using System;
using System.Collections;
using Newtonsoft.Json;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Text.RegularExpressions;
using System.Web.Mvc;
using System.Web;
using Swordfish.Businessobjects.TopDog;
using Swordfish.Utilities.WEXCard;
using Swordfish.Businessobjects.Sesame;
using Swordfish.Businessobjects.Common;
using Swordfish.Businessobjects.Quotes;
using Swordfish.Businessobjects.Transfers;
using Swordfish.Businessobjects.Passengers;
using Swordfish.Components.BookingServices;
using Swordfish.Components.TopDogServices;
using Swordfish.UI.Models.Booking;
using System.Linq;
using GBP = Swordfish.Utilities.WEXCard.GBP;
using EURO = Swordfish.Utilities.WEXCard.EURO;
using Swordfish.Businessobjects.Easyjet;

namespace Swordfish.UI.Helpers
{
    public class BookingFulfilmentHelper
    {
        private string sessionID = "Not Available";
        NotificationHelper notificationHelper = new NotificationHelper();
        string toMailList = string.Empty;
        public BookingFulfilmentHelper()
        {
            if (HttpContext.Current != null)
                this.sessionID = HttpContext.Current.Session.SessionID;

        }
        public BookingFulfillmentModel GetBookingFulfillmentModel(string quoteHeaderId)
        {
            BookingFulfillmentModel bookingFulfillmentModel = new BookingFulfillmentModel();
            bookingFulfillmentModel.LoadQuote(quoteHeaderId);

            int easyJetCount = 0, nonEasyJetCount = 0;
            if (bookingFulfillmentModel.QuoteData.hotelComponent != null)
            {
                bookingFulfillmentModel.Hotel = new BookingFulfillmentHotel()
                {
                    HotelName = quoteData.hotelComponent.HotelName,
                    HotelDescription = quoteData.hotelComponent.hotelComponentsDetails[0].RoomDescription,
                    ResortName = quoteData.hotelComponent.SupplierResortName,
                    StarRating = quoteData.hotelComponent.StarRating
                };
            }

            
            foreach (var flightComponent in quoteData.flightComponents)
            {
                bookingFulfillmentModel.ComponentTypeOptionId = flightComponent.ComponentTypeOptionId;
                foreach (var details in flightComponent.FlightComponentDetails)
                {
                    if (details.MarketingAirlineCode.Equals(SFConstants.EasyJetMarketingAirlineCode))
                    {
                        if (flightComponent.Source.Equals(details.DepartureAirportCode) || flightComponent.Arrival.Equals(details.ArrivalAirportCode))
                        {
                            easyJetCount++;
                            bookingFulfillmentModel.Flights.Add(new BookingFulfillmentFlight()
                            {
                                DepartureAirport = details.DepartureAirportCode,
                                ArrivalAirport = details.ArrivalAirportCode,
                                MarketingAirline = details.MarketingAirLine,
                                MarketingAirlineCode = details.MarketingAirlineCode,
                                ComponentTypeOptionId = flightComponent.ComponentTypeOptionId,
                                DirectionOptionId = details.DirectionOptionId
                            });
                        }
                        else if (flightComponent.ComponentTypeOptionId == SFConstants.QuoteComponent_FlightReturn)
                        {
                            if (flightComponent.Source.Equals(details.ArrivalAirportCode) || flightComponent.Arrival.Equals(details.DepartureAirportCode))
                            {
                                easyJetCount++;
                                bookingFulfillmentModel.Flights.Add(new BookingFulfillmentFlight()
                                {
                                    DepartureAirport = details.DepartureAirportCode,
                                    ArrivalAirport = details.ArrivalAirportCode,
                                    MarketingAirline = details.MarketingAirLine,
                                    MarketingAirlineCode = details.MarketingAirlineCode,
                                    ComponentTypeOptionId = flightComponent.ComponentTypeOptionId,
                                    DirectionOptionId = details.DirectionOptionId
                            });
                            }
                        }
                    }
                    else
                    {
                        if (flightComponent.Source.Equals(details.DepartureAirportCode) || flightComponent.Arrival.Equals(details.ArrivalAirportCode))
                        {
                            nonEasyJetCount++;
                            bookingFulfillmentModel.Flights.Add(new BookingFulfillmentFlight()
                            {
                                DepartureAirport = details.DepartureAirportCode,
                                ArrivalAirport = details.ArrivalAirportCode,
                                MarketingAirline = details.MarketingAirLine,
                                MarketingAirlineCode = details.MarketingAirlineCode,
                                ComponentTypeOptionId = flightComponent.ComponentTypeOptionId,
                                DirectionOptionId = details.DirectionOptionId
                        });
                        }
                        else if (flightComponent.ComponentTypeOptionId == SFConstants.QuoteComponent_FlightReturn)
                        {
                            if (flightComponent.Source.Equals(details.ArrivalAirportCode) || flightComponent.Arrival.Equals(details.DepartureAirportCode))
                            {
                                nonEasyJetCount++;
                                bookingFulfillmentModel.Flights.Add(new BookingFulfillmentFlight()
                                {
                                    DepartureAirport = details.DepartureAirportCode,
                                    ArrivalAirport = details.ArrivalAirportCode,
                                    MarketingAirline = details.MarketingAirLine,
                                    MarketingAirlineCode = details.MarketingAirlineCode,
                                    ComponentTypeOptionId = flightComponent.ComponentTypeOptionId,
                                    DirectionOptionId = details.DirectionOptionId
                                });
                            }
                        }
                    }
                }
            }
            bookingFulfillmentModel.BookingType = easyJetCount == 0 ? SFConstants.NonEasyJetBookingType : nonEasyJetCount == 0 ? SFConstants.EasyJetBookingType : SFConstants.HybridBookingType;
            return bookingFulfillmentModel;
        }
        public void BookingFulfilmentForNONEzJetQuote(BookingFulfillmentModel bookingFulFillmentmodel, string quoteHeaderID)
        {
            BookingFulfilmentHelper bookingFulfilment = new BookingFulfilmentHelper();
            var sesameBookBasket = bookingFulfilment.GetSesameComponent(Convert.ToInt32(quoteHeaderID), "", SFConstants.SesameBasket_Book);
            BookFlightsHotelsWithSesame(bookingFulFillmentmodel, quoteHeaderID, sesameBookBasket);
            FillNonEzJetModel(bookingFulFillmentmodel, quoteHeaderID);
        }
        public void FillNonEzJetModel(BookingFulfillmentModel bookingFulFillmentmodel, string quoteHeaderID)
        {
            SesameBasket sesameBookBasket = GetSesameComponent(Convert.ToInt32(quoteHeaderID), "", SFConstants.SesameBasket_Book);
            List<NonEjStatusCodes> flightSuccessQuotes = new List<NonEjStatusCodes>();
            List<NonEjStatusCodes> hotelSuccessQuotes = new List<NonEjStatusCodes>();
            if (sesameBookBasket.BasketComponents != null && sesameBookBasket.BasketComponents.Count > 0)
            {
                bookingFulFillmentmodel.QuoteData.SesameBookBasket = sesameBookBasket;
                var flightComponents = sesameBookBasket.BasketComponents.FindAll(bc => bc.ProductTypeCode.Equals("FLI"));
                foreach (var flightComponent in flightComponents)
                {
                    NonEjStatusCodes nonEjStatusCode = new NonEjStatusCodes();
                    if (flightComponent.ComponentErrors != null && flightComponent.ComponentErrors.Count > 0)
                    {
                        nonEjStatusCode.StatusCode = (int)System.Net.HttpStatusCode.InternalServerError;
                        nonEjStatusCode.RefCode = flightComponent.ComponentErrors[0];
                        nonEjStatusCode.DirectionOptionId = flightComponent.DirectionOptionId;
                    }
                    else if (!string.IsNullOrEmpty(flightComponent.BookingSucceeded) && !Convert.ToBoolean(flightComponent.BookingSucceeded))
                    {
                        nonEjStatusCode.StatusCode = (int)System.Net.HttpStatusCode.InternalServerError;
                        nonEjStatusCode.DirectionOptionId = flightComponent.DirectionOptionId;
                    }
                    else
                    {
                        nonEjStatusCode.StatusCode = (int)System.Net.HttpStatusCode.OK;
                        nonEjStatusCode.RefCode = flightComponent.SupplierBookingId;
                        nonEjStatusCode.DirectionOptionId = flightComponent.DirectionOptionId;
                    }
                    flightSuccessQuotes.Add(nonEjStatusCode);
                }

               if(flightComponents.Select(x=>x.BookingSucceeded =true.ToString()).Count() == flightComponents.Count())
                    bookingFulFillmentmodel.FlightFailureStatus = false;
               else
                    bookingFulFillmentmodel.FlightFailureStatus = true;

                var hotelComponents = sesameBookBasket.BasketComponents.FindAll(bc => bc.ProductTypeCode.Equals("ACO"));
                foreach (var hotelComponent in hotelComponents)
                {
                    NonEjStatusCodes nonEjStatusCode = new NonEjStatusCodes();
                    if ((hotelComponent.ComponentErrors != null && hotelComponent.ComponentErrors.Count > 0) || hotelComponent.BookingSucceeded == "false")
                    {
                        nonEjStatusCode.StatusCode = (int)System.Net.HttpStatusCode.InternalServerError;
                        if (hotelComponent.ComponentErrors.Count > 0)
                            nonEjStatusCode.RefCode = hotelComponent.ComponentErrors[0];
                        nonEjStatusCode.ComponentError = "Booking failed.";
                        bookingFulFillmentmodel.HotelFailureStatus = true;
                    }
                    else
                    {
                        nonEjStatusCode.StatusCode = (int)System.Net.HttpStatusCode.OK;
                        nonEjStatusCode.RefCode = hotelComponent.SupplierBookingId;
                        bookingFulFillmentmodel.HotelFailureStatus = false;
                    }
                    hotelSuccessQuotes.Add(nonEjStatusCode);
                }

                //Fetching Tranfers booking status code
                if (bookingFulFillmentmodel.QuoteData.transferComponents.Count > 0)
                {
                    CommonHelper objcommonHelper = new CommonHelper();
                    bookingFulFillmentmodel.QuoteData.BookedHolidayTaxiData = objcommonHelper.GetHolidayTaxisData(Convert.ToInt32(quoteHeaderID));
                    if (bookingFulFillmentmodel.QuoteData.BookedHolidayTaxiData != null && bookingFulFillmentmodel.QuoteData.BookedHolidayTaxiData.general != null)
                        bookingFulFillmentmodel.TransfersFailureStatus = false;
                    else
                        bookingFulFillmentmodel.TransfersFailureStatus = true;
                }
                else
                {
                    bookingFulFillmentmodel.TransfersFailureStatus = false;
                }
            }
            else
            {
                NonEjStatusCodes nonEjStatusCode = new NonEjStatusCodes();
                nonEjStatusCode.StatusCode = (int)System.Net.HttpStatusCode.InternalServerError;
                bookingFulFillmentmodel.TransfersFailureStatus = true;
                bookingFulFillmentmodel.HotelFailureStatus = true;
                bookingFulFillmentmodel.FlightFailureStatus = true;
                if (!string.IsNullOrEmpty(sesameBookBasket.StatusMessage))
                    nonEjStatusCode.RefCode = sesameBookBasket.StatusMessage;
                else
                    nonEjStatusCode.RefCode = bookingFulFillmentmodel.QuoteData.SesameBasket.StatusMessage;
                flightSuccessQuotes.Add(nonEjStatusCode);
                hotelSuccessQuotes.Add(nonEjStatusCode);
            }
            bookingFulFillmentmodel.NonEjFlightStatusCodes = flightSuccessQuotes;
            bookingFulFillmentmodel.NonEjHotelStatusCodes = hotelSuccessQuotes;
        }
        private void BookFlightsHotelsWithSesame(BookingFulfillmentModel bookingFulFillmentmodel, string quoteHeaderID, SesameBasket sesameBookBasket)
        {
            var gbpSesameBasket = sesameBookBasket.BasketComponents.FindAll(bc => bc.ProductTypeCode.Equals("FLI")
            && (bc.DirectionOptionId == SFConstants.Direction_Outbound || bc.DirectionOptionId == SFConstants.QuoteComponent_FlightReturn));
            if ((gbpSesameBasket == null || gbpSesameBasket.Count == 0) && sesameBookBasket.BasketComponents.FindAll(bc => bc.ProductTypeCode.Equals("ACO")).Count == 0)
                BookWithGBPSesame(bookingFulFillmentmodel, quoteHeaderID);

            var eurSesameBasket = sesameBookBasket.BasketComponents.FindAll(bc => bc.ProductTypeCode.Equals("FLI")
            && (bc.DirectionOptionId == SFConstants.Direction_Inbound));
            if (eurSesameBasket == null || eurSesameBasket.Count == 0)
                BookWithEURSesame(bookingFulFillmentmodel, quoteHeaderID);
        }
        private void BookWithGBPSesame(BookingFulfillmentModel bookingFulFillmentmodel, string quoteHeaderID)
        {
            string basketCreateId = string.Empty;
            string basketRefreshId = string.Empty;
            string basketBookId = string.Empty;
            basketCreateId = CreateGBPBasketWithSesame(bookingFulFillmentmodel, quoteHeaderID);
            if (!string.IsNullOrEmpty(basketCreateId))
                basketRefreshId = RefreshBasketWithSesame(bookingFulFillmentmodel, quoteHeaderID, basketCreateId);
            if (!string.IsNullOrEmpty(basketRefreshId))
                basketBookId = BookBasketWithSesame(bookingFulFillmentmodel, quoteHeaderID, basketRefreshId);
        }
        private void BookWithEURSesame(BookingFulfillmentModel bookingFulFillmentmodel, string quoteHeaderID)
        {
            string basketCreateId = string.Empty;
            string basketRefreshId = string.Empty;
            string basketBookId = string.Empty;
            basketCreateId = CreateEURBasketWithSesame(bookingFulFillmentmodel, quoteHeaderID);
            if (!string.IsNullOrEmpty(basketCreateId))
                basketRefreshId = RefreshBasketWithSesame(bookingFulFillmentmodel, quoteHeaderID, basketCreateId);
            if (!string.IsNullOrEmpty(basketRefreshId))
                basketBookId = BookBasketWithSesame(bookingFulFillmentmodel, quoteHeaderID, basketRefreshId);
        }
        private string CreateGBPBasketWithSesame(BookingFulfillmentModel bookingFulFillmentmodel, string quoteHeaderID)
        {
            string basketId = string.Empty;
            SFLogger.logMessage("BookingFulfilment:CreateFlightHotelBasketWithSesame(): Start!!.");
            List<string> flighQuotes = new List<string>();
            int flightTypeId = 0;
            int directionOptionId = 0;
            SesameBasketCreateRequest basketCreateRequest = new SesameBasketCreateRequest();
            if (bookingFulFillmentmodel.QuoteData.flightComponents != null && bookingFulFillmentmodel.QuoteData.flightComponents.Count > 0)
            {
                foreach (var flight in bookingFulFillmentmodel.QuoteData.flightComponents)
                {
                    flightTypeId = flight.ComponentTypeOptionId;
                    if (flight.FlightComponentDetails != null && flight.FlightComponentDetails.Count > 0 && flightTypeId == SFConstants.QuoteComponent_FlightReturn)
                    {
                        foreach (var flightComp in flight.FlightComponentDetails)
                        {
                            if (!flightComp.MarketingAirlineCode.Equals(SFConfiguration.GetEjMarketingAirlineCode()))
                            {
                                if (flighQuotes != null && flighQuotes.Count > 0)
                                {
                                    if (!flighQuotes.Exists(f => flight.FlightQuoteId.Contains(f)))
                                        flighQuotes.Add(flight.FlightQuoteId);
                                }
                                else
                                    flighQuotes.Add(flight.FlightQuoteId);
                                directionOptionId = flightComp.DirectionOptionId;
                                basketCreateRequest.SourceUrl = flight.SourceUrl;
                            }
                        }
                    }
                    if (flight.FlightComponentDetails != null && flight.FlightComponentDetails.Count > 0 && flightTypeId == SFConstants.QuoteComponent_Flightoneway)
                    {
                        foreach (var flightComp in flight.FlightComponentDetails)
                        {
                            if (!flightComp.MarketingAirlineCode.Equals(SFConfiguration.GetEjMarketingAirlineCode())
                                && flightComp.DirectionOptionId == SFConstants.Direction_Outbound)
                            {
                                if (flighQuotes != null && flighQuotes.Count > 0)
                                {
                                    if (!flighQuotes.Exists(f => flight.FlightQuoteId.Contains(f)))
                                        flighQuotes.Add(flight.FlightQuoteId);
                                }
                                else
                                    flighQuotes.Add(flight.FlightQuoteId);
                                directionOptionId = flightComp.DirectionOptionId;
                                basketCreateRequest.SourceUrl = flight.SourceUrl;
                            }
                        }
                    }
                }
            }
            SesameAccoDetail accoDetail = new SesameAccoDetail();
            if (bookingFulFillmentmodel.QuoteData.hotelComponent != null)
            {
                accoDetail.AccoQuoteId = bookingFulFillmentmodel.QuoteData.hotelComponent.HotelQuoteId;
                if (bookingFulFillmentmodel.QuoteData.hotelComponent.hotelComponentsDetails != null && bookingFulFillmentmodel.QuoteData.hotelComponent.hotelComponentsDetails.Count > 0)
                {
                    accoDetail.RateQuoteId = bookingFulFillmentmodel.QuoteData.hotelComponent.hotelComponentsDetails[0].RateQuoteId;
                    accoDetail.RoomTypeCode = bookingFulFillmentmodel.QuoteData.hotelComponent.hotelComponentsDetails[0].RoomTypeCode;
                }
            }
            if (flighQuotes != null && flighQuotes.Count > 0)
                basketCreateRequest.FlightQuotes = flighQuotes;
            basketCreateRequest.AccoQuotes = accoDetail;
            string url = string.Format(SFConfiguration.GetTravelSaasCreateBasket(), this.sessionID);
            Task<SesameBasket> taskCreateBasket = Task.Factory.StartNew(() => GetCreateSesameResponseFromServer(basketCreateRequest, url));
            Task.WaitAll(taskCreateBasket);
            if (taskCreateBasket.Result != null && taskCreateBasket.Result.StatusCode == (int)System.Net.HttpStatusCode.OK)
            {
                FliggingDirectionForSesameBasket(taskCreateBasket.Result, bookingFulFillmentmodel.QuoteData, directionOptionId);
                basketId = SaveSesameResponse(taskCreateBasket.Result, SFConstants.SesameBasket_Create, quoteHeaderID);
            }
            else
            {
                bookingFulFillmentmodel.QuoteData.SesameBasket = taskCreateBasket.Result;
                throw new SwordfishAjaxException(bookingFulFillmentmodel.QuoteData.SesameBasket.StatusMessage);
            }
            SFLogger.logMessage("BookingFulfilment:CreateFlightHotelBasketWithSesame(): end.");
            return basketId;
        }
        private string CreateEURBasketWithSesame(BookingFulfillmentModel bookingFulFillmentmodel, string quoteHeaderID)
        {
            string basketId = string.Empty;
            SFLogger.logMessage("BookingFulfilment:CreateFlightHotelBasketWithSesame(): Start!!.");
            List<string> flighQuotes = new List<string>();
            int flightTypeId = 0;
            int directionOptionId = 0;
            SesameBasketCreateRequest basketCreateRequest = new SesameBasketCreateRequest();
            if (bookingFulFillmentmodel.QuoteData.flightComponents != null && bookingFulFillmentmodel.QuoteData.flightComponents.Count > 0)
            {
                foreach (var flight in bookingFulFillmentmodel.QuoteData.flightComponents)
                {
                    flightTypeId = flight.ComponentTypeOptionId;
                    if (flight.FlightComponentDetails != null && flight.FlightComponentDetails.Count > 0 && flightTypeId == SFConstants.QuoteComponent_Flightoneway)
                    {
                        foreach (var flightComp in flight.FlightComponentDetails)
                        {
                            if (!flightComp.MarketingAirlineCode.Equals(SFConfiguration.GetEjMarketingAirlineCode()) && flightComp.DirectionOptionId == SFConstants.Direction_Inbound)
                            {
                                if (flighQuotes != null && flighQuotes.Count > 0)
                                {
                                    if (!flighQuotes.Exists(f => flight.FlightQuoteId.Contains(f)))
                                        flighQuotes.Add(flight.FlightQuoteId);
                                }
                                else
                                    flighQuotes.Add(flight.FlightQuoteId);
                                directionOptionId = flightComp.DirectionOptionId;
                                basketCreateRequest.SourceUrl = flight.SourceUrl;
                            }
                        }
                    }
                }
            }

            if (flighQuotes != null && flighQuotes.Count > 0)
            {
                basketCreateRequest.FlightQuotes = flighQuotes;
                string url = string.Format(SFConfiguration.GetTravelSaasCreateBasket(), this.sessionID);
                Task<SesameBasket> taskCreateBasket = Task.Factory.StartNew(() => GetCreateSesameResponseFromServer(basketCreateRequest, url));
                Task.WaitAll(taskCreateBasket);
                if (taskCreateBasket.Result != null && taskCreateBasket.Result.StatusCode == (int)System.Net.HttpStatusCode.OK)
                {
                    FliggingDirectionForSesameBasket(taskCreateBasket.Result, bookingFulFillmentmodel.QuoteData, directionOptionId);
                    basketId = SaveSesameResponse(taskCreateBasket.Result, SFConstants.SesameBasket_Create, quoteHeaderID);
                }
                else
                {
                    bookingFulFillmentmodel.QuoteData.SesameBasket = taskCreateBasket.Result;
                    throw new SwordfishAjaxException(bookingFulFillmentmodel.QuoteData.SesameBasket.StatusMessage);
                }
            }
            SFLogger.logMessage("BookingFulfilment:CreateFlightHotelBasketWithSesame(): end.");
            return basketId;
        }
        private void FliggingDirectionForSesameBasket(SesameBasket sesameBasket, Quote quoteData, int directionOptionId)
        {
            var flightData = sesameBasket.BasketComponents.FindAll(bc => bc.ProductTypeCode.Equals("FLI"));
            //flightData is null
            if (quoteData.flightComponents.Count > 1)
            {
                if (flightData.Count > 0)
                {
                    if (flightData[0] != null)
                        flightData[0].DirectionOptionId = directionOptionId;
                    if (flightData.Count > 1 && flightData[1] != null)
                        flightData[1].DirectionOptionId = directionOptionId;
                }
            }
            if (quoteData.flightComponents.Count == 1)
            {
                if (flightData.Count > 0)
                {
                    if (flightData[0] != null)
                        flightData[0].DirectionOptionId = SFConstants.QuoteComponent_FlightReturn;
                }
            }
        }
        private string RefreshBasketWithSesame(BookingFulfillmentModel bookingFulFillmentmodel, string quoteHeaderID, string basketId)
        {
            SFLogger.logMessage("BookingFulfilment:RefreshFlightHotelBasketWithSesame(): Start.");
            SesameBasket sesameBasket = GetSesameComponent(Convert.ToInt32(quoteHeaderID), basketId, SFConstants.SesameBasket_Create);
            SesameBasketRefreshRequest sesameBasketRefreshRequest = new SesameBasketRefreshRequest();
            sesameBasketRefreshRequest.SourceUrl = sesameBasket.SourceUrl;
            sesameBasketRefreshRequest.BasketID = basketId;
            sesameBasketRefreshRequest.Components = BuildSesameBookComponent(sesameBasket, bookingFulFillmentmodel.QuoteData);
            sesameBasketRefreshRequest.Occupancy = BindSesameRooms(quoteHeaderID);
            string basketRId = string.Empty;
            string url = string.Format(SFConfiguration.GetTravelSaasRefreshBasket(), this.sessionID);
            Task<SesameBasket> taskCreateBasket = Task.Factory.StartNew(() => GetRefreshSesameResponseFromServer(sesameBasketRefreshRequest, url));
            Task.WaitAll(taskCreateBasket);
            if (taskCreateBasket.Result != null && taskCreateBasket.Result.StatusCode == (int)System.Net.HttpStatusCode.OK)
            {
                if (sesameBasket.BasketComponents.Where(c => c.ProductTypeCode.Equals("FLI")).FirstOrDefault() != null)
                    FliggingDirectionForSesameBasket(taskCreateBasket.Result, bookingFulFillmentmodel.QuoteData, sesameBasket.BasketComponents.Where(c => c.ProductTypeCode.Equals("FLI")).FirstOrDefault().DirectionOptionId);
                basketRId = SaveSesameResponse(taskCreateBasket.Result, SFConstants.SesameBasket_Refresh, quoteHeaderID);
            }
            else
            {
                bookingFulFillmentmodel.QuoteData.SesameBasket = taskCreateBasket.Result;
                throw new SwordfishAjaxException(bookingFulFillmentmodel.QuoteData.SesameBasket.StatusMessage);
            }
            SFLogger.logMessage("BookingFulfilment:RefreshFlightHotelBasketWithSesame(): end.");
            return basketRId;
        }
        private List<SesameBookComponent> BuildSesameBookComponent(SesameBasket sesameBasket, Quote quoteData)
        {
            List<SesameBookComponent> sesameBookComponents = new List<SesameBookComponent>();
            if (sesameBasket.BasketComponents != null && sesameBasket.BasketComponents.Count > 0)
            {
                foreach (var basketComponent in sesameBasket.BasketComponents)
                {
                    SesameBookComponent sesameComponent = new SesameBookComponent();
                    sesameComponent.ComponentID = basketComponent.SupplierComponentId;
                    if (quoteData.bagsComponents != null && quoteData.bagsComponents.Count > 0 && basketComponent.ProductTypeCode.Equals("FLI"))
                    {
                        List<SesameBookOption> sesameBookOptions = new List<SesameBookOption>();
                        if (basketComponent.DirectionOptionId == SFConstants.Direction_Outbound)
                        {
                            var obBags = quoteData.bagsComponents.FindAll(bc => bc.DirectionOptionId == SFConstants.Direction_Outbound).ToList();
                            foreach (var obBag in obBags.GroupBy(x => x.BagsCode).Select(y => y.First()))
                            {
                                SesameBookOption sesameBookOption = new SesameBookOption();
                                sesameBookOption.OptionID = obBag.BagsCode;
                                sesameBookOption.QuantityRequired = obBags.Where(y => y.BagsCode.Equals(obBag.BagsCode)).Count().ToString();
                                sesameBookOptions.Add(sesameBookOption);
                            }
                        }
                        if (basketComponent.DirectionOptionId == SFConstants.Direction_Inbound)
                        {
                            var ibBags = quoteData.bagsComponents.FindAll(bc => bc.DirectionOptionId == SFConstants.Direction_Inbound).ToList();
                            foreach (var ibBag in ibBags.GroupBy(x => x.BagsCode).Select(y => y.First()))
                            {
                                SesameBookOption sesameBookOption = new SesameBookOption();
                                sesameBookOption.OptionID = ibBag.BagsCode;
                                sesameBookOption.QuantityRequired = ibBags.Where(y => y.BagsCode.Equals(ibBag.BagsCode)).Count().ToString();
                                sesameBookOptions.Add(sesameBookOption);
                            }
                        }
                        if (basketComponent.DirectionOptionId == SFConstants.QuoteComponent_FlightReturn)
                        {
                            var rndBags = quoteData.bagsComponents.FindAll(bc => bc.DirectionOptionId == SFConstants.QuoteComponent_FlightReturn).ToList();
                            foreach (var rndBag in rndBags.GroupBy(x => x.BagsCode).Select(y => y.First()))
                            {
                                SesameBookOption sesameBookOption = new SesameBookOption();
                                sesameBookOption.OptionID = rndBag.BagsCode;
                                sesameBookOption.QuantityRequired = rndBags.Where(y => y.BagsCode.Equals(rndBag.BagsCode)).Count().ToString();
                                sesameBookOptions.Add(sesameBookOption);
                            }
                        }
                        sesameComponent.Options = sesameBookOptions;
                    }
                    sesameBookComponents.Add(sesameComponent);
                }
            }
            return sesameBookComponents;
        }
        private List<SesameRoom> BindSesameRooms(string quoteHeaderID)
        {
            List<SesameRoom> sesameRooms = new List<SesameRoom>();
            CommonHelper commonHelper = new CommonHelper();
            List<Passenger> passengers = commonHelper.GetPassengers(quoteHeaderID);
            if (passengers != null && passengers.Count > 0)
            {
                SesameRoom sesameRoom = new SesameRoom();
                List<SesamePaxDetail> audltPaxs = new List<SesamePaxDetail>();
                List<SesamePaxDetail> childPaxs = new List<SesamePaxDetail>();
                List<SesamePaxDetail> infantPaxs = new List<SesamePaxDetail>();
                foreach (var passenger in passengers)
                {
                    SesamePaxDetail pax = new SesamePaxDetail();
                    if (passenger.PassengerTypeOptionId == SFConstants.PassengerType_Adult)
                    {
                        pax.Title = passenger.Title;
                        pax.FirstName = passenger.FirstName;
                        pax.LastName = passenger.LastName;
                        var date = !string.IsNullOrEmpty(passenger.DateOfBirth) ? passenger.DateOfBirth.Split('/') : passenger.DefaultDateOfBirth.Split('/');
                        pax.DOB = date[2] + '-' + date[1] + '-' + date[0];
                        audltPaxs.Add(pax);
                    }
                    if (passenger.PassengerTypeOptionId == SFConstants.PassengerType_Child)
                    {
                        pax.Title = passenger.Title;
                        pax.FirstName = passenger.FirstName;
                        pax.LastName = passenger.LastName;
                        var date = !string.IsNullOrEmpty(passenger.DateOfBirth) ? passenger.DateOfBirth.Split('/') : passenger.DefaultDateOfBirth.Split('/');
                        pax.DOB = date[2] + '-' + date[1] + '-' + date[0];
                        childPaxs.Add(pax);
                    }
                    if (passenger.PassengerTypeOptionId == SFConstants.PassengerType_Infant)
                    {
                        pax.Title = passenger.Title;
                        pax.FirstName = passenger.FirstName;
                        pax.LastName = passenger.LastName;
                        var date = !string.IsNullOrEmpty(passenger.DateOfBirth) ? passenger.DateOfBirth.Split('/') : passenger.DefaultDateOfBirth.Split('/');
                        pax.DOB = date[2] + '-' + date[1] + '-' + date[0];
                        infantPaxs.Add(pax);
                    }
                }
                sesameRoom.Adults = audltPaxs;
                sesameRoom.Children = childPaxs;
                sesameRoom.Infants = infantPaxs;

                sesameRooms.Add(sesameRoom);
            }
            return sesameRooms;
        }
        private string BookBasketWithSesame(BookingFulfillmentModel bookingFulFillmentmodel, string quoteHeaderID, string basketId)
        {
            SFLogger.logMessage("BookingFulfilment:BookFlightHotelBasketWithSesame(): start.");
            SesameBasket sesameBasket = GetSesameComponent(Convert.ToInt32(quoteHeaderID), basketId, SFConstants.SesameBasket_Refresh);
            SesameBasketBookRequest sesameBasketBookRequest = new SesameBasketBookRequest();
            sesameBasketBookRequest.BasketID = basketId;
            sesameBasketBookRequest.SourceUrl = sesameBasket.SourceUrl;
            sesameBasketBookRequest.CardHolderDetail = GetLeadPassengerDetails(quoteHeaderID);
            sesameBasketBookRequest.VCCPaymentDetails = GetVCCPayment(bookingFulFillmentmodel.QuoteData);
            string basketBId = string.Empty;
            string url = string.Format(SFConfiguration.GetTravelSaasBookBasket(), this.sessionID);
            Task<SesameBasket> taskCreateBasket = Task.Factory.StartNew(() => GetBookSesameResponseFromServer(sesameBasketBookRequest, url));
            Task.WaitAll(taskCreateBasket);
            if (taskCreateBasket.Result != null && taskCreateBasket.Result.StatusCode == (int)System.Net.HttpStatusCode.OK)
            {
                if (sesameBasket.BasketComponents.Where(c => c.ProductTypeCode.Equals("FLI")).FirstOrDefault() != null)
                    FliggingDirectionForSesameBasket(taskCreateBasket.Result, bookingFulFillmentmodel.QuoteData, sesameBasket.BasketComponents.Where(c => c.ProductTypeCode.Equals("FLI")).FirstOrDefault().DirectionOptionId);
                basketBId = SaveSesameResponse(taskCreateBasket.Result, SFConstants.SesameBasket_Book, quoteHeaderID);
            }
            else
            {
                bookingFulFillmentmodel.QuoteData.SesameBasket = taskCreateBasket.Result;
                throw new SwordfishAjaxException(bookingFulFillmentmodel.QuoteData.SesameBasket.StatusMessage);
            }
            SFLogger.logMessage("BookingFulfilment:BookFlightHotelBasketWithSesame(): end.");
            return string.Empty;
        }
        private SesamePaxDetail GetLeadPassengerDetails(string quoteHeaderID)
        {
            SesamePaxDetail sesameLeadPassengerDetails = new SesamePaxDetail();
            CommonHelper commonHelper = new CommonHelper();
            List<Passenger> passengers = commonHelper.GetPassengers(quoteHeaderID);
            foreach (var passenger in passengers)
            {
                if (passenger.IsLeadPassenger.Equals("True"))
                {
                    sesameLeadPassengerDetails.Title = passenger.Title;
                    sesameLeadPassengerDetails.FirstName = passenger.FirstName;
                    sesameLeadPassengerDetails.LastName = passenger.LastName;
                    sesameLeadPassengerDetails.Email = passenger.Email;
                    sesameLeadPassengerDetails.PhoneDay = passenger.Mobile;
                    sesameLeadPassengerDetails.PhoneEve = passenger.Telephone;
                    var date = !string.IsNullOrEmpty(passenger.DateOfBirth) ? passenger.DateOfBirth.Split('/') : passenger.DefaultDateOfBirth.Split('/');
                    sesameLeadPassengerDetails.DOB = date[2] + '-' + date[1] + '-' + date[0];

                    SesameAddress address = new SesameAddress();
                    address.Line1 = passenger.Address1;
                    address.Line2 = passenger.Address2;
                    address.Line3 = passenger.Address3;
                    address.CountryCode = "GB";
                    address.PostCode = passenger.PostCode;

                    sesameLeadPassengerDetails.PAXAddress = address;
                }
            }
            return sesameLeadPassengerDetails;
        }
        private SesameVCCPayment GetVCCPayment(Quote quoteData)
        {
            string optionValue = string.Empty;
            SesameVCCPayment sesameVCCPayment = new SesameVCCPayment();
            CommonHelper commonHelper = new CommonHelper();
            var lowCostAirlines = commonHelper.GetLowCostAirLines();
            if (lowCostAirlines != null && lowCostAirlines.Count > 0)
            {
                if (quoteData.flightComponents != null && quoteData.flightComponents.Count > 0)
                {
                    foreach (var flight in quoteData.flightComponents)
                    {
                        var marketAirLineCode = flight.FlightComponentDetails.Find(fc => lowCostAirlines.Exists(l => l.Key.Equals(fc.MarketingAirlineCode)));
                        if (marketAirLineCode != null)
                        {
                            optionValue = lowCostAirlines.Find(lc => lc.Key.Equals(marketAirLineCode.MarketingAirlineCode)).Value;
                        }
                    }
                }
            }
            if (!string.IsNullOrEmpty(optionValue))
            {
                sesameVCCPayment.SupplierCode = optionValue.Split('|')[1].ToString();
                sesameVCCPayment.CurrencyCode = SFConfiguration.GetCurrency();
            }
            return sesameVCCPayment;
        }
        public SesameBasket GetSesameComponent(int quoteHeaderId, string basketId, int typeId)
        {
            CommonHelper commonHelper = new CommonHelper();
            return commonHelper.GetComponentDetails(quoteHeaderId, basketId, typeId);
        }
        private static string SaveSesameResponse(SesameBasket sesameBasket, int basketTypeOptionId, string quoteHeaderID)
        {
            SFLogger.logMessage("BookingFulfilment:SaveSesameResponse(): end.");
            bool status = false;
            BookingService serviceRepository = new BookingService();
            BookingHelper bookingHelper = new BookingHelper(serviceRepository);
            sesameBasket.AgentId = SFConfiguration.GetUserID();
            sesameBasket.QuoteHeaderId = Convert.ToInt32(quoteHeaderID);
            if (sesameBasket.BasketComponents != null && sesameBasket.BasketComponents.Count > 0)
            {
                foreach (var basketComponent in sesameBasket.BasketComponents)
                {
                    basketComponent.Type = basketTypeOptionId;
                }
                foreach (var BasketComponent in sesameBasket.BasketComponents)
                {
                    if(BasketComponent.ComponentMessages != null && BasketComponent.ComponentMessages.Count > 0)
                    {
                        for(int index=0; index< BasketComponent.ComponentMessages.Count; index++)
                        {
                            BasketComponent.ComponentMessages[index] = BasketComponent.ComponentMessages[index].Replace(",", SFConfiguration.GetSpecialKeyValue());
                        }
                    }
                } 
            }
            if (bookingHelper.SaveSesameQuoteForCreateRefreshBook(sesameBasket))
                status = true;
            else
                status = false;
            SFLogger.logMessage("BookingFulfilment:SaveSesameResponse(): end.");
            if (status)
                return sesameBasket.BasketId;
            else
                return string.Empty;
        }
        private static SesameBasket GetCreateSesameResponseFromServer(SesameBasketCreateRequest basketCreateRequest, string url)
        {
            SFLogger.logMessage("BookingFulfilment:GetCreateSesameResponseFromServer(): started!!.");
            var list = new ArrayList();
            list.Add(basketCreateRequest);
            var travelSAASClient = new TravelSAASRestClient();
            var sesameResponse = new SesameBasketResponse();
            var result = travelSAASClient.ExecuteRequest(url, RequestType.Post, list);
            if (result.StatusCode == (int)System.Net.HttpStatusCode.OK)
                sesameResponse = JsonConvert.DeserializeObject<SesameBasketResponse>(result.Response);
            else
                sesameResponse.ResponseBasket.StatusMessage = result.Response;
            sesameResponse.ResponseBasket.StatusCode = result.StatusCode;
            SFLogger.logMessage("BookingFulfilment:GetFlightListFromService(): end.");
            return sesameResponse.ResponseBasket;
        }
        private static SesameBasket GetRefreshSesameResponseFromServer(SesameBasketRefreshRequest basketRefreshRequest, string url)
        {
            SFLogger.logMessage("BookingFulfilment:GetCreateSesameResponseFromServer(): started!!.");
            var list = new ArrayList();
            list.Add(basketRefreshRequest);
            var travelSAASClient = new TravelSAASRestClient();
            var sesameBasketResponse = new SesameBasketResponse();
            var result = travelSAASClient.ExecuteRequest(url, RequestType.Post, list);

            if (result.StatusCode == (int)System.Net.HttpStatusCode.OK)
                sesameBasketResponse = JsonConvert.DeserializeObject<SesameBasketResponse>(result.Response);
            else
                sesameBasketResponse.ResponseBasket.StatusMessage = result.Response;
            sesameBasketResponse.ResponseBasket.StatusCode = result.StatusCode;
            SFLogger.logMessage("BookingFulfilment:GetFlightListFromService(): end.");
            return sesameBasketResponse.ResponseBasket;
        }
        private static SesameBasket GetBookSesameResponseFromServer(SesameBasketBookRequest basketBookRequest, string url)
        {
            SFLogger.logMessage("BookingFulfilment:GetCreateSesameResponseFromServer(): started!!.");
            var list = new ArrayList();
            list.Add(basketBookRequest);
            var travelSAASClient = new TravelSAASRestClient();
            var sesameBasketResponse = new SesameBasketResponse();
            var result = travelSAASClient.ExecuteRequest(url, RequestType.Post, list);

            if (result.StatusCode == (int)System.Net.HttpStatusCode.OK)
                sesameBasketResponse = JsonConvert.DeserializeObject<SesameBasketResponse>(result.Response);
            else
                sesameBasketResponse.ResponseBasket.StatusMessage = result.Response;
            sesameBasketResponse.ResponseBasket.StatusCode = result.StatusCode;
            SFLogger.logMessage("BookingFulfilment:GetFlightListFromService(): end.");
            return sesameBasketResponse.ResponseBasket;
        }
        public bool BookTransfersWithHolidayTaxi(BookingFulfillmentModel bookingFulFillmentmodel, string quoteHeaderID)
        {
            if (bookingFulFillmentmodel.QuoteData.transferComponents != null && bookingFulFillmentmodel.QuoteData.transferComponents.Count > 0)
            {
                Request bookRequest = new Request();
                General general = new General();
                general.orderreference = quoteHeaderID;
                general.paymenttype = SFConfiguration.GetPaymentType();
                bookRequest.general = general;

                Contact contact = new Contact();
                contact.email = SFConfiguration.GetHolidayTaxiDefaultEmailId();
                contact.title = bookingFulFillmentmodel.QuoteData.quoteHeader.CustomerTitle;
                contact.firstname = bookingFulFillmentmodel.QuoteData.quoteHeader.CustomerFirstName;
                contact.lastname = bookingFulFillmentmodel.QuoteData.quoteHeader.CustomerLastName;
                contact.telephone = bookingFulFillmentmodel.QuoteData.quoteHeader.MobileNumber;
                bookRequest.contact = contact;

                Pricing transferPricing = new Pricing();
                transferPricing.saleprice = bookingFulFillmentmodel.QuoteData.transferComponents[0].totalPrice + bookingFulFillmentmodel.QuoteData.transferComponents[1].totalPrice;
                transferPricing.currency = bookingFulFillmentmodel.QuoteData.transferComponents[0].Currency.ToString();
                bookRequest.pricing = transferPricing;

                List<Transfer> transfersflist = new List<Transfer>();
                Transfer transfertravelling = new Transfer();
                transfertravelling.productid = Convert.ToInt32(bookingFulFillmentmodel.QuoteData.transferComponents[0].ProductId);
                transfertravelling.bookingtypeid = Convert.ToInt32(bookingFulFillmentmodel.QuoteData.transferComponents[0].BookingTypeId);
                transfertravelling.saleprice = bookingFulFillmentmodel.QuoteData.transferComponents[0].totalPrice;
                transfertravelling.noadults = bookingFulFillmentmodel.QuoteData.searchModel.AdultsCount;
                transfertravelling.nochildren = bookingFulFillmentmodel.QuoteData.searchModel.ChildrenCount;
                transfertravelling.noinfants = bookingFulFillmentmodel.QuoteData.searchModel.InfantsCount;
                transfertravelling.nopax = bookingFulFillmentmodel.QuoteData.searchModel.AdultsCount + bookingFulFillmentmodel.QuoteData.searchModel.ChildrenCount + bookingFulFillmentmodel.QuoteData.searchModel.InfantsCount;
                transfertravelling.novehicles = bookingFulFillmentmodel.QuoteData.transferComponents[0].Units;

                Fromdetails fromdetailstravelling = new Fromdetails();
                fromdetailstravelling.codetype = SFConfiguration.GetIataCode();

                Flight flightfromdetails = new Flight();
                flightfromdetails.arrivaldatetime = String.Format("{0:s}", bookingFulFillmentmodel.QuoteData.flightComponentUI.flightComponentsOutbound.Last().ArrivalDateTime);
                flightfromdetails.flightnumber = bookingFulFillmentmodel.QuoteData.flightComponentUI.flightComponentsOutbound[0].FlightNumber;
                flightfromdetails.terminal = SFConfiguration.GetTerminal();

                flightfromdetails.originairport = bookingFulFillmentmodel.QuoteData.flightComponentUI.flightComponentsOutbound[0].ArrivalAirportCode;

                fromdetailstravelling.flight = flightfromdetails;
                transfertravelling.fromdetails = fromdetailstravelling;

                Todetails todetails = new Todetails();
                todetails.codetype = SFConfiguration.GetGIataCode();

                transfertravelling.todetails = todetails;

                ToAccommodation accommodationtoDetailstravelling = new ToAccommodation();
                accommodationtoDetailstravelling.name = bookingFulFillmentmodel.QuoteData.hotelComponent.HotelName;
                accommodationtoDetailstravelling.addressline1 = !string.IsNullOrEmpty(bookingFulFillmentmodel.QuoteData.hotelComponent.HotelInformatoin.Address.Address1) ? bookingFulFillmentmodel.QuoteData.hotelComponent.HotelInformatoin.Address.Address1 : SFConfiguration.GetAddressLine();
                accommodationtoDetailstravelling.addressline2 = !string.IsNullOrEmpty(bookingFulFillmentmodel.QuoteData.hotelComponent.HotelInformatoin.Address.City) ? bookingFulFillmentmodel.QuoteData.hotelComponent.HotelInformatoin.Address.City : SFConfiguration.GetAddressLine();
                accommodationtoDetailstravelling.addressline3 = !string.IsNullOrEmpty(bookingFulFillmentmodel.QuoteData.hotelComponent.HotelInformatoin.Address.Country) ? bookingFulFillmentmodel.QuoteData.hotelComponent.HotelInformatoin.Address.Country : SFConfiguration.GetAddressLine();
                accommodationtoDetailstravelling.telephone = !string.IsNullOrEmpty(bookingFulFillmentmodel.QuoteData.hotelComponent.HotelInformatoin.Address.Tel) ? bookingFulFillmentmodel.QuoteData.hotelComponent.HotelInformatoin.Address.Tel : SFConfiguration.GetHotelPhoneNumber();
                todetails.accommodation = accommodationtoDetailstravelling;

                transfersflist.Add(transfertravelling);

                bookRequest.transfers = transfersflist;

                Transfer transfersReturning = new Transfer();
                transfersReturning.productid = Convert.ToInt32(bookingFulFillmentmodel.QuoteData.transferComponents[1].ProductId);
                transfersReturning.bookingtypeid = Convert.ToInt32(bookingFulFillmentmodel.QuoteData.transferComponents[1].BookingTypeId);
                transfersReturning.saleprice = bookingFulFillmentmodel.QuoteData.transferComponents[1].totalPrice;
                transfersReturning.noadults = bookingFulFillmentmodel.QuoteData.searchModel.AdultsCount;
                transfersReturning.nochildren = bookingFulFillmentmodel.QuoteData.searchModel.ChildrenCount;
                transfersReturning.noinfants = bookingFulFillmentmodel.QuoteData.searchModel.InfantsCount;
                transfersReturning.nopax = bookingFulFillmentmodel.QuoteData.searchModel.AdultsCount + bookingFulFillmentmodel.QuoteData.searchModel.ChildrenCount + bookingFulFillmentmodel.QuoteData.searchModel.InfantsCount;
                transfersReturning.novehicles = bookingFulFillmentmodel.QuoteData.transferComponents[1].Units;

                Fromdetails fromdetailsreturning = new Fromdetails();
                fromdetailsreturning.codetype = SFConfiguration.GetGIataCode();

                FromAccommodation accommodation2 = new FromAccommodation();
                accommodation2.pickupdatetime = String.Format("{0:s}", bookingFulFillmentmodel.QuoteData.flightComponentUI.flightComponentsInbound[0].DepartureDateTime.AddMinutes(-bookingFulFillmentmodel.QuoteData.DeparturePickUp).ToString("yyyy-MM-ddTHH:mm:ss"));
                accommodation2.name = bookingFulFillmentmodel.QuoteData.hotelComponent.HotelName;
                accommodation2.addressline1 = !string.IsNullOrEmpty(bookingFulFillmentmodel.QuoteData.hotelComponent.HotelInformatoin.Address.Address1) ? bookingFulFillmentmodel.QuoteData.hotelComponent.HotelInformatoin.Address.Address1 : SFConfiguration.GetAddressLine();
                accommodation2.addressline2 = !string.IsNullOrEmpty(bookingFulFillmentmodel.QuoteData.hotelComponent.HotelInformatoin.Address.City) ? bookingFulFillmentmodel.QuoteData.hotelComponent.HotelInformatoin.Address.City : SFConfiguration.GetAddressLine();
                accommodation2.addressline3 = !string.IsNullOrEmpty(bookingFulFillmentmodel.QuoteData.hotelComponent.HotelInformatoin.Address.Country) ? bookingFulFillmentmodel.QuoteData.hotelComponent.HotelInformatoin.Address.Country : SFConfiguration.GetAddressLine();
                accommodation2.telephone = !string.IsNullOrEmpty(bookingFulFillmentmodel.QuoteData.hotelComponent.HotelInformatoin.Address.Tel) ? bookingFulFillmentmodel.QuoteData.hotelComponent.HotelInformatoin.Address.Tel : SFConfiguration.GetAddressLine();
                fromdetailsreturning.accommodation = accommodation2;
                transfersReturning.fromdetails = fromdetailsreturning;

                Todetails todetailsreturning = new Todetails();
                todetailsreturning.codetype = SFConfiguration.GetIataCode();
                transfersReturning.todetails = todetailsreturning;

                ToFlight flightreturning = new ToFlight();
                flightreturning.departuredatetime = String.Format("{0:s}", bookingFulFillmentmodel.QuoteData.flightComponentUI.flightComponentsInbound[0].DepartureDateTime);
                flightreturning.flightnumber = bookingFulFillmentmodel.QuoteData.flightComponentUI.flightComponentsInbound[0].FlightNumber;
                flightreturning.terminal = SFConfiguration.GetTerminal();
                flightreturning.destinationairport = bookingFulFillmentmodel.QuoteData.flightComponentUI.flightComponentsInbound[0].ArrivalAirportCode;
                todetailsreturning.flight = flightreturning;
                transfersflist.Add(transfersReturning);

                bookRequest.transfers = transfersflist;
                TransferBookResponse transferData = new TransferBookResponse();
                SFLogger.logMessage("Holiday Taxis Request: Start.", "BookingFulfilment: BookTransfersWithHolidayTaxi()", JsonConvert.SerializeObject(bookRequest));
                transferData = BookingHelper.BookedTransfersDataFromService(bookRequest);
                SFLogger.logMessage("Holiday Taxis Response: End.", "BookingFulfilment: BookTransfersWithHolidayTaxi()", JsonConvert.SerializeObject(transferData));
                bookingFulFillmentmodel.TransferStatusCodes = transferData.StatusCode;
                if (transferData != null && transferData.Rootobject.Count > 0 && transferData.StatusCode == (int)System.Net.HttpStatusCode.OK)
                {
                    if (GetTransfersBookModelStructure(transferData, quoteHeaderID))
                    {
                        bookingFulFillmentmodel.TransferStatusMessage = GetTransferBookingReference(quoteHeaderID);
                        CommonHelper objcommonHelper = new CommonHelper();
                        bookingFulFillmentmodel.QuoteData.BookedHolidayTaxiData = objcommonHelper.GetHolidayTaxisData(Convert.ToInt32(quoteHeaderID));
                    }
                    else
                    {
                        bookingFulFillmentmodel.TransferStatusCodes = (int)System.Net.HttpStatusCode.InternalServerError;
                        bookingFulFillmentmodel.TransferStatusMessage = "Failed to save transfers to database.";
                    }
                }
                else
                {
                    bookingFulFillmentmodel.TransferStatusMessage = transferData.StatusMessage;
                }
                return true;
            }
            else
                return false;
        }
        private bool GetTransfersBookModelStructure(TransferBookResponse transferData, string quoteHeaderID)
        {
            List<TransfersData> transfers = transferData.Rootobject[0].response.booking.transfers;
            var transferBookDetails = new List<TransferBook>();
            TransfersBookData transferBookDetail = new TransfersBookData();

            TransfersBookGeneral general = new TransfersBookGeneral();
            general.quoteHeaderId = Convert.ToInt32(quoteHeaderID);
            general.affiliateCode = transferData.Rootobject[0].response.booking.general[0].affiliateCode;
            general.agency = transferData.Rootobject[0].response.booking.general[0].agency;
            general.clerk = transferData.Rootobject[0].response.booking.general[0].clerk;
            general.bookingReference = transferData.Rootobject[0].response.booking.general[0].bookingReference;
            general.bookingStatusId = transferData.Rootobject[0].response.booking.general[0].bookingStatusId;
            general.orderReference = transferData.Rootobject[0].response.booking.general[0].orderReference;
            general.noOfTransfers = transferData.Rootobject[0].response.booking.general[0].noOfTransfers;
            general.paymentType = transferData.Rootobject[0].response.booking.general[0].paymentType;
            general.dateEntered = transferData.Rootobject[0].response.booking.general[0].dateEntered;
            general.pdf = transferData.Rootobject[0].response.booking.general[0].pdf;
            general.html = transferData.Rootobject[0].response.booking.general[0].html;
            transferBookDetail.general = general;

            TransfersBookContact contact = new TransfersBookContact();
            contact.title = transferData.Rootobject[0].response.booking.contact[0].title;
            contact.firstName = transferData.Rootobject[0].response.booking.contact[0].firstName;
            contact.lastName = transferData.Rootobject[0].response.booking.contact[0].lastName;
            contact.email = transferData.Rootobject[0].response.booking.contact[0].email;
            contact.telephone = transferData.Rootobject[0].response.booking.contact[0].telephone;
            transferBookDetail.contact = contact;

            TransfersBookPricing pricing = new TransfersBookPricing();
            pricing.netPrice = transferData.Rootobject[0].response.booking.pricing[0].netPrice;
            pricing.totalPrice = transferData.Rootobject[0].response.booking.pricing[0].totalPrice;
            pricing.transactionfee = transferData.Rootobject[0].response.booking.pricing[0].transactionfee;
            pricing.carbonoffset = transferData.Rootobject[0].response.booking.pricing[0].carbonoffset;
            pricing.optionalExtras = transferData.Rootobject[0].response.booking.pricing[0].optionalExtras;
            pricing.creditAmount = transferData.Rootobject[0].response.booking.pricing[0].creditAmount;
            pricing.salePrice = transferData.Rootobject[0].response.booking.pricing[0].salePrice;
            pricing.currency = transferData.Rootobject[0].response.booking.pricing[0].currency;
            transferBookDetail.pricing = pricing;

            foreach (var transfersobjects in transfers)
            {
                var transferdata = new TransferBook();
                transferdata.transferOrder = transfersobjects.transferOrder;
                transferdata.transferId = Convert.ToInt32(transfersobjects.transferId);
                transferdata.transferStatus = transfersobjects.transferStatus;

                if (!string.IsNullOrEmpty(transfersobjects.dateBooked))
                {
                    transferdata.dateBooked = Convert.ToDateTime(transfersobjects.dateBooked);
                }
                if (!string.IsNullOrEmpty(transfersobjects.dateAmended))
                {
                    transferdata.dateAmended = Convert.ToDateTime(transfersobjects.dateAmended);
                }
                if (!string.IsNullOrEmpty(transfersobjects.dateCancelled))
                {
                    transferdata.dateCancelled = Convert.ToDateTime(transfersobjects.dateCancelled);
                }
                transferdata.bookingTypeId = Convert.ToInt32(transfersobjects.bookingTypeId);
                transferdata.productId = Convert.ToInt32(transfersobjects.productId);
                transferdata.description = transfersobjects.description;
                transferdata.productType = transfersobjects.productType;
                transferdata.productCategory = transfersobjects.productCategory;
                transferdata.transportProvider = transfersobjects.transportProvider;
                transferdata.transferTime = transfersobjects.transferTime;
                transferdata.minimumPax = Convert.ToInt32(transfersobjects.minimumPax);
                transferdata.maximumPax = Convert.ToInt32(transfersobjects.maximumPax);
                transferdata.lagguage = transfersobjects.lagguage;
                transferdata.noofPax = Convert.ToInt32(transfersobjects.noofPax);
                transferdata.noofVehicles = Convert.ToInt32(transfersobjects.noofVehicles);
                transferdata.noofAdults = Convert.ToInt32(transfersobjects.noofAdults);
                transferdata.noofChildren = Convert.ToInt32(transfersobjects.noofChildren);
                transferdata.noofInfants = Convert.ToInt32(transfersobjects.noofInfants);
                transferdata.airportcodeType = transfersobjects.fromDetails[0].airportcodeType;
                transferdata.airportCode = transfersobjects.fromDetails[0].airportCode;
                transferdata.airportCodeId = transfersobjects.fromDetails[0].airportCodeId;
                transferdata.airportCodeName = Regex.Replace(transfersobjects.fromDetails[0].airportCodeName, @"([^0-9A-Za-z ,]|^\s)", string.Empty);
                transferdata.flightNumber = transfersobjects.fromDetails[0].flights.Count > 0 ? transfersobjects.fromDetails[0].flights[0].flightNumber : string.Empty;
                transferdata.terminal = transfersobjects.fromDetails[0].flights.Count > 0 ? transfersobjects.fromDetails[0].flights[0].terminal : string.Empty;
                if (transfersobjects.fromDetails[0].flights.Count > 0 && !string.IsNullOrEmpty(transfersobjects.fromDetails[0].flights[0].arrivalDateTime))
                {
                    transferdata.arrivalDateTime = Convert.ToDateTime(transfersobjects.fromDetails[0].flights[0].arrivalDateTime);
                }
                if (transfersobjects.fromDetails[0].flights.Count > 0 && !string.IsNullOrEmpty(transfersobjects.fromDetails[0].flights[0].departureDateTime))
                {
                    transferdata.departureDateTime = Convert.ToDateTime(transfersobjects.fromDetails[0].flights[0].departureDateTime);
                }

                transferdata.hotelName = transfersobjects.toDetails[0].accommodations.Count > 0 ? transfersobjects.toDetails[0].accommodations[0].name : string.Empty;
                transferdata.addressLine1 = transfersobjects.toDetails[0].accommodations.Count > 0 ? transfersobjects.toDetails[0].accommodations[0].addressLine1 : string.Empty;
                transferdata.addressLine2 = transfersobjects.toDetails[0].accommodations.Count > 0 ? transfersobjects.toDetails[0].accommodations[0].addressLine2 : string.Empty;
                transferdata.addressLine3 = transfersobjects.toDetails[0].accommodations.Count > 0 ? transfersobjects.toDetails[0].accommodations[0].addressLine3 : string.Empty;
                transferdata.telephone = transfersobjects.toDetails[0].accommodations.Count > 0 ? transfersobjects.toDetails[0].accommodations[0].telephone : string.Empty;
                transferdata.supplierName = transfersobjects.toDetails[0].accommodations.Count > 0 ? transfersobjects.toDetails[0].accommodations[0].supplierName : string.Empty;
                transferdata.supplierAddressline1 = transfersobjects.toDetails[0].accommodations.Count > 0 ? transfersobjects.toDetails[0].accommodations[0].supplierAddressline1 : string.Empty;
                transferdata.supplierAddressline2 = transfersobjects.toDetails[0].accommodations.Count > 0 ? transfersobjects.toDetails[0].accommodations[0].supplierAddressline2 : string.Empty;
                transferdata.supplierAddressline3 = transfersobjects.toDetails[0].accommodations.Count > 0 ? transfersobjects.toDetails[0].accommodations[0].supplierAddressline3 : string.Empty;
                transferdata.pickupTimeViewed = transfersobjects.toDetails[0].accommodations.Count > 0 ? transfersobjects.toDetails[0].accommodations[0].pickupTimeViewed : string.Empty;
                transferdata.reconfirmationRequired = transfersobjects.toDetails[0].accommodations.Count > 0 ? transfersobjects.toDetails[0].accommodations[0].reconfirmationRequired : string.Empty;

                if (transfersobjects.fromDetails[0].accommodations.Count > 0 && !string.IsNullOrEmpty(transfersobjects.fromDetails[0].accommodations[0].pickupDateTime))
                {
                    transferdata.pickupDateTime = Convert.ToDateTime(transfersobjects.fromDetails[0].accommodations[0].pickupDateTime);
                }

                if (transfersobjects.toDetails[0].accommodations.Count > 0 && !string.IsNullOrEmpty(transfersobjects.toDetails[0].accommodations[0].supplierPickupDateTime))
                {
                    transferdata.supplierPickupDateTime = Convert.ToDateTime(transfersobjects.toDetails[0].accommodations[0].supplierPickupDateTime);
                }
                if (transfersobjects.toDetails[0].flights.Count > 0 && !string.IsNullOrEmpty(transfersobjects.toDetails[0].flights[0].departureDateTime))
                {
                    transferdata.departureDateTime = Convert.ToDateTime(transfersobjects.toDetails[0].flights[0].departureDateTime);
                }
                if (transfersobjects.toDetails[0].accommodations.Count > 0 && !string.IsNullOrEmpty(transfersobjects.toDetails[0].accommodations[0].pickupDateTime))
                {
                    transferdata.pickupDateTime = Convert.ToDateTime(transfersobjects.toDetails[0].accommodations[0].pickupDateTime);
                }
                transferdata.netPrice = Convert.ToDecimal(String.Format("{0:0.00}", transfersobjects.pricings[0].netPrice));
                transferdata.totalPrice = Convert.ToDecimal(String.Format("{0:0.00}", transfersobjects.pricings[0].totalPrice));
                transferdata.transactionfee = Convert.ToDecimal(String.Format("{0:0.00}", transfersobjects.pricings[0].transactionfee));
                transferdata.carbonoffset = Convert.ToDecimal(String.Format("{0:0.00}", transfersobjects.pricings[0].carbonoffset));
                transferdata.optionalExtras = Convert.ToDecimal(String.Format("{0:0.00}", transfersobjects.pricings[0].optionalExtras));
                transferdata.creditAmount = Convert.ToDecimal(String.Format("{0:0.00}", transfersobjects.pricings[0].creditAmount));
                transferdata.salePrice = Convert.ToDecimal(transfersobjects.pricings[0].salePrice);
                transferdata.currency = transfersobjects.pricings[0].currency;
                transferdata.vehicleTypeCode = transfersobjects.pricings[0].Prices[0].typeCode;
                transferdata.vehicleType = transfersobjects.pricings[0].Prices[0].type;
                transferdata.description = transfersobjects.pricings[0].Prices[0].description;
                transferdata.units = transfersobjects.pricings[0].Prices[0].units;
                transferdata.vehicleNetPrice = Convert.ToDecimal(String.Format("{0:0.00}", transfersobjects.pricings[0].Prices[0].netPrice));
                transferdata.vehicleTotalPrice = Convert.ToDecimal(String.Format("{0:0.00}", transfersobjects.pricings[0].Prices[0].totalPrice));
                transferdata.instruction = Regex.Replace(transfersobjects.instructions[0].instruction, @"([^0-9A-Za-z ,]|^\s)", string.Empty);
                transferdata.instructionNoMarkup = Regex.Replace(transfersobjects.instructions[0].instructionNoMarkup, @"([^0-9A-Za-z ,]|^\s)", string.Empty);
                transferdata.reconfirmationTelephone = transfersobjects.instructions[0].reconfirmationTelephone;
                transferdata.emergencyTelephone = transfersobjects.instructions[0].emergencyTelephone;
                transferdata.hoursMondayFriday = transfersobjects.instructions[0].hoursMondayFriday;
                transferdata.hourSaturday = transfersobjects.instructions[0].hourSaturday;
                transferdata.hoursSunday = transfersobjects.instructions[0].hoursSunday;
                transferdata.ukEmergencyTelephone = transfersobjects.instructions[0].ukEmergencyTelephone;
                transferBookDetails.Add(transferdata);

            }
            BookingService serviceRepository = new BookingService();
            BookingHelper bookingHelper = new BookingHelper(serviceRepository);
            transferData.Rootobject[0].response.booking.transfers = null;
            transferBookDetail.transferbookdata = transferBookDetails;
            return bookingHelper.SaveTransfersResponseData(transferBookDetail);
        }
        private string GetTransferBookingReference(string quoteHeaderID)
        {
            CommonHelper objcommonHelper = new CommonHelper();
            int quoteHeaderid = Convert.ToInt32(quoteHeaderID);
            return objcommonHelper.GetHolidayTaxisData(quoteHeaderid).general.bookingReference;
        }
        public CreateCardResponse CreateWEXCard(string quoteHeaderId, decimal totalAmount, bool IsGBP)
        {
            string connectionString = SFConfiguration.GetchilliDataBase() != null ? SFConfiguration.GetchilliDataBase() : null;
            string wexCardHolderName = SFConfiguration.GetWEXCardHolderName() != null ? SFConfiguration.GetWEXCardHolderName() : null;
            string requestReference = SFConfiguration.GetWEXrequestReference() != null ? SFConfiguration.GetWEXrequestReference() : null;
            string username = SFConfiguration.GetWEXusername() != null ? SFConfiguration.GetWEXusername() : null;
            string lastName = SFConfiguration.GetWEXLastName() != null ? SFConfiguration.GetWEXLastName() : null;
            string GBPAccount = SFConfiguration.GetWEXGBPaccount() != null ? SFConfiguration.GetWEXGBPaccount() : null;
            string EUROAccount = SFConfiguration.GetWEXEUROaccount() != null ? SFConfiguration.GetWEXEUROaccount() : null;
            string expiry = SFConfiguration.GetWEXexpiry() != null ? SFConfiguration.GetWEXexpiry() : null;
            string instant = SFConfiguration.GetWEXinstant() != null ? SFConfiguration.GetWEXinstant() : null;
            string activated = SFConfiguration.GetWEXactivated() != null ? SFConfiguration.GetWEXactivated() : null;
            string description = SFConfiguration.GetWEXdescription() != null ? SFConfiguration.GetWEXdescription() : null;
            string title = SFConfiguration.GetWEXtitle() != null ? SFConfiguration.GetWEXtitle() : null;
            string firstName = SFConfiguration.GetWEXfirstName() != null ? SFConfiguration.GetWEXfirstName() : null;
            string gender = SFConfiguration.GetWEXGender() != null ? SFConfiguration.GetWEXGender() : null;
            string dateOfBirth = SFConfiguration.GetWEXDateOfBirth() != null ? SFConfiguration.GetWEXDateOfBirth() : null;
            string email = SFConfiguration.GetWEXEmail() != null ? SFConfiguration.GetWEXEmail() : null;
            string street = SFConfiguration.GetWEXStreet() != null ? SFConfiguration.GetWEXStreet() : null;
            string postcode = SFConfiguration.GetWEXPostcode() != null ? SFConfiguration.GetWEXPostcode() : null;
            string city = SFConfiguration.GetWEXCity() != null ? SFConfiguration.GetWEXCity() : null;
            string country = SFConfiguration.GetWEXCountry() != null ? SFConfiguration.GetWEXCountry() : null;
            string houseNumber = SFConfiguration.GetWEXHouseNumber() != null ? SFConfiguration.GetWEXHouseNumber() : null;
            string cardType = SFConfiguration.GetWEXCardType() != null ? SFConfiguration.GetWEXCardType() : null;
            string houseName = SFConfiguration.GetWEXHouseName() != null ? SFConfiguration.GetWEXHouseName() : null;
            if (IsGBP)
            {
                //Create GBP Card Request
                GBP.createCardRequest objcreateCard = new GBP.createCardRequest();
                GBP.request objrequest = new GBP.request();
                GBP.personalInfo objpersonalInfo = new GBP.personalInfo();
                objpersonalInfo.lastName = lastName;
                objrequest.loadAmount = (int)Math.Ceiling(totalAmount); ;
                objrequest.requestReference = requestReference;
                objrequest.username = username;

                objrequest.account = GBPAccount;
                objrequest.expiry = Convert.ToInt32(expiry);
                objrequest.instant = Convert.ToBoolean(instant);
                objrequest.activated = Convert.ToBoolean(activated);
                objrequest.description = description;
                objrequest.efftvStartDate = DateTime.Now.ToString("yyyy-MM-dd");
                objcreateCard.Request = objrequest;

                GBP.customer objcustomer = new GBP.customer();
                objcreateCard.Request.customer = objcustomer;

                objpersonalInfo.title = title;
                objpersonalInfo.firstName = firstName;

                objpersonalInfo.gender = gender;
                objpersonalInfo.dateOfBirth = dateOfBirth;
                objpersonalInfo.email = email;
                objcreateCard.Request.customer.info = objpersonalInfo;

                GBP.addressDTO objaddressDTO = new GBP.addressDTO();
                objaddressDTO.id = 0;
                objaddressDTO.street = street;
                objaddressDTO.postcode = postcode;
                objaddressDTO.city = city;
                objaddressDTO.country = country;
                objaddressDTO.houseNumber = houseNumber;
                objcreateCard.Request.customer.permanentAddress = objaddressDTO;

                CardCreator obj = new CardCreator(objcreateCard, connectionString, quoteHeaderId);
                return obj.CreateGBPCard();
            }
            else
            {
                //Create EURO Card Request
                EURO.createCard objcreateCard = new EURO.createCard();
                EURO.request objrequest = new EURO.request();
                EURO.requestCustomerInfo objpersonalInfo = new EURO.requestCustomerInfo();
                objpersonalInfo.lastName = lastName;
                objrequest.loadAmount = (ushort)Math.Ceiling(totalAmount); ;
                objrequest.requestReference = requestReference;
                objrequest.username = username;

                objrequest.account = GBPAccount;
                objrequest.expiry = 1;
                objrequest.instant = Convert.ToBoolean(instant);
                objrequest.activated = Convert.ToBoolean(activated);
                objrequest.description = description;
                objcreateCard.request = objrequest;

                EURO.requestCustomer objcustomer = new EURO.requestCustomer();
                objcreateCard.request.customer = objcustomer;

                objpersonalInfo.title = title;
                objpersonalInfo.firstName = firstName;

                objpersonalInfo.gender = gender;
                objpersonalInfo.dateOfBirth = dateOfBirth;
                objpersonalInfo.email = email;
                objcreateCard.request.customer.info = objpersonalInfo;

                EURO.requestCustomerPermanentAddress objaddressDTO = new EURO.requestCustomerPermanentAddress();
                objaddressDTO.id = 0;
                objaddressDTO.street = street;
                objaddressDTO.postcode = postcode;
                objaddressDTO.city = city;
                objaddressDTO.country = country;
                objaddressDTO.houseName = houseName;
                objcreateCard.request.customer.permanentAddress = objaddressDTO;

                CardCreator obj = new CardCreator(objcreateCard, connectionString, quoteHeaderId);
                return obj.CreateEUROCard();
            }
        }
        public bool saveTopDogResponse(List<TopDogResponse> topDogResponse, int quoteheaderId)
        {
            BookingService PackagingRepository = new BookingService();
            return PackagingRepository.SaveTopDogResponse(topDogResponse, quoteheaderId, SFConfiguration.GetUserID());
        }
        public List<TopDogResponse> PostDataToTopDog(int quoteHeaderId, BookingFulfillmentModel bookingFulFillmentmodel, ControllerContext controllerContext)
        {
            var quoteData = bookingFulFillmentmodel.QuoteData;
            List<TopDogResponse> topDogResponse = new List<TopDogResponse>();
            CommonHelper commonHelper = new CommonHelper();
            BookingFulfilmentHelper bookingFulfilment = new BookingFulfilmentHelper();
            var extraCharges = commonHelper.GetExtraCharges();
            var passengers = commonHelper.GetPassengers(quoteHeaderId.ToString());
            var userInfo = SFConfiguration.GetUserInfo();
            List<EasyJetResponse> easyJetBookings = new List<EasyJetResponse>();
            if ((quoteData.EasyJetBookingStatus != null && quoteData.EasyJetBookingStatus.Count > 0) && quoteData.flightComponents
                .Where(f => f.ComponentTypeOptionId.Equals(SFConstants.QuoteComponent_FlightReturn)).Count() > 0)
                easyJetBookings.Add(EasyJetHelper.GetEasyJetBooking(quoteHeaderId.ToString(), SFConstants.QuoteComponent_FlightReturn));
            else if ((quoteData.EasyJetBookingStatus != null && quoteData.EasyJetBookingStatus.Count > 0) && quoteData.flightComponents
                .Where(f => f.ComponentTypeOptionId.Equals(SFConstants.QuoteComponent_Flightoneway)).Count() > 0)
            {
                easyJetBookings.Add(EasyJetHelper.GetEasyJetBooking(quoteHeaderId.ToString(), SFConstants.Direction_Inbound));
                easyJetBookings.Add(EasyJetHelper.GetEasyJetBooking(quoteHeaderId.ToString(), SFConstants.Direction_Outbound));
            }
            quoteData.EasyJetBookings = easyJetBookings;
            userInfo.SessionId = sessionID;
            userInfo.Environment = SFConfiguration.GetEnvironment();
            var paymentList = commonHelper.GetCompletePaymentDetails(quoteHeaderId);
            TopDogService topDog = new TopDogService();
            topDogResponse = topDog.SaveDataInTopDog(quoteData, passengers, userInfo, paymentList, extraCharges, SFLogger.GetSessionID());
            if (topDogResponse == null || (topDogResponse.Count > 0 && topDogResponse.FindAll(x => x.IsError == true).Count > 0))
            {
                notificationHelper.SendTopDogFailureEMail(controllerContext, quoteHeaderId, quoteData, out toMailList);
                bookingFulFillmentmodel.FulfillmentTransactionSummary = "Topdog posting failed, email has been sent to " + toMailList + " " + " with QuoteRef:" + " " + quoteData.quoteHeader.CustomerReferenceCode.ToUpper().ToString();
            }
            return topDogResponse;
        }
        public bool BookingFullfillmentFailureEmail(int quoteHeaderId, Quote quoteData, ControllerContext controllerContext, out string emailList)
        {
            NotificationHelper notificationHelper = new NotificationHelper();
            return notificationHelper.SendBookingFullFillmentFailureEmail(controllerContext, quoteHeaderId, quoteData, out emailList);
        }
        public bool BookingAcknowledgementEmail(int quoteHeaderId, Quote quoteData, ControllerContext controllerContext, out string emailList)
        {
            NotificationHelper notificationHelper = new NotificationHelper();
            return notificationHelper.SendBookingAcknowledgementEmail(controllerContext, quoteHeaderId, quoteData, out emailList);
        }

        public BookingFulfillmentModel BookingFullfillmentAccess(BookingFulfillmentModel model, ControllerContext controllerContext)
        {
            try
            {
                string quoteHeaderId = model.QuoteHeaderId;
                model = GetBookingFulfillmentModel(model.QuoteHeaderId);
                model.IsFromSubmitted = 1;
                model.QuoteHeaderId = quoteHeaderId;
                //NonEzJet booking
                BookingFulfilmentForNONEzJetQuote(model, model.QuoteHeaderId);
                if (model.QuoteData.SesameBasket != null && model.QuoteData.SesameBasket.StatusCode != 0 && model.QuoteData.SesameBasket.StatusCode != (int)System.Net.HttpStatusCode.OK)
                {
                    FillNonEzJetModel(model, model.QuoteHeaderId);
                }

                if (model.BookingType.Equals(SFConstants.NonEasyJetBookingType))
                {
                    if (model.QuoteData.SesameBasket != null && model.QuoteData.SesameBasket.BasketComponents != null && model.QuoteData.SesameBasket.BasketComponents.Count > 0)
                    {
                        if (model.QuoteData.transferComponents != null && model.QuoteData.transferComponents.Count > 0 && model.QuoteData.BookedHolidayTaxiData.transferbookdata == null
                            && model.FlightFailureStatus == false && model.HotelFailureStatus == false)
                        {
                            if (!BookTransfersWithHolidayTaxi(model, model.QuoteHeaderId))
                                return model;
                        }
                        else
                        {
                            if (model.QuoteData.BookedHolidayTaxiData.general != null)
                            {
                                model.TransferStatusMessage = model.QuoteData.BookedHolidayTaxiData.general.bookingReference;
                                model.TransferStatusCodes = (int)System.Net.HttpStatusCode.OK;
                            }

                        }
                    }

                }
                else if (model.BookingType.Equals(SFConstants.EasyJetBookingType))
                {
                    if (model.QuoteData.SesameBasket != null)
                    {
                        if (model.ComponentTypeOptionId > 0)
                        {
                            EasyJetHelper.FillEasyJetResponses(model.EasyJetBookingStatus, model.QuoteHeaderId, model.ComponentTypeOptionId);
                            if (model.ComponentTypeOptionId == SFConstants.QuoteComponent_FlightReturn && model.EasyJetBookingStatus.Count == 1)
                            {
                                if (model.EasyJetBookingStatus[0].eJReturnBookingSuccess.HasValue && model.EasyJetBookingStatus[0].eJReturnBookingSuccess.Value)
                                {
                                    model.QuoteData.EasyJetBookingStatus = model.EasyJetBookingStatus;
                                    if (model.QuoteData.transferComponents != null && model.QuoteData.transferComponents.Count > 0 && model.QuoteData.BookedHolidayTaxiData.transferbookdata == null
                            && model.FlightFailureStatus == false && model.HotelFailureStatus == false)
                                    {
                                        if (!BookTransfersWithHolidayTaxi(model, model.QuoteHeaderId))
                                            return model;
                                    }
                                    else
                                    {
                                        if (model.QuoteData.BookedHolidayTaxiData.general != null)
                                        {
                                            model.TransferStatusMessage = model.QuoteData.BookedHolidayTaxiData.general.bookingReference;
                                            model.TransferStatusCodes = (int)System.Net.HttpStatusCode.OK;
                                        }

                                    }
                                }

                            }
                            else if ((model.ComponentTypeOptionId == SFConstants.Direction_Inbound || model.ComponentTypeOptionId == SFConstants.Direction_Outbound) && model.EasyJetBookingStatus.Count == 2)
                            {
                                if ((model.EasyJetBookingStatus[0].eJReturnBookingSuccess.HasValue && model.EasyJetBookingStatus[0].eJReturnBookingSuccess.Value) &&
                                    ((model.EasyJetBookingStatus[1].eJReturnBookingSuccess.HasValue && model.EasyJetBookingStatus[1].eJReturnBookingSuccess.Value)))
                                {
                                    model.QuoteData.EasyJetBookingStatus = model.EasyJetBookingStatus;
                                    if (model.QuoteData.transferComponents != null && model.QuoteData.transferComponents.Count > 0 && model.QuoteData.BookedHolidayTaxiData.transferbookdata == null
                                      && model.FlightFailureStatus == false && model.HotelFailureStatus == false)
                                    {
                                        if (!BookTransfersWithHolidayTaxi(model, model.QuoteHeaderId))
                                            return model;
                                    }
                                    else
                                    {
                                        if (model.QuoteData.BookedHolidayTaxiData.general != null)
                                        {
                                            model.TransferStatusMessage = model.QuoteData.BookedHolidayTaxiData.general.bookingReference;
                                            model.TransferStatusCodes = (int)System.Net.HttpStatusCode.OK;
                                        }

                                    }
                                }

                            }

                        }
                    }
                }
                else if (model.BookingType.Equals(SFConstants.HybridBookingType))
                {
                    if (model.QuoteData.SesameBasket != null && model.QuoteData.SesameBasket.BasketComponents != null && model.QuoteData.SesameBasket.BasketComponents.Count > 0)
                    {
                        if (model.ComponentTypeOptionId > 0)
                        {
                            EasyJetHelper.FillEasyJetResponses(model.EasyJetBookingStatus, model.QuoteHeaderId, model.ComponentTypeOptionId);
                            if (model.EasyJetBookingStatus != null && model.EasyJetBookingStatus.Count > 0)
                            {
                                if (model.QuoteData.transferComponents != null && model.QuoteData.transferComponents.Count > 0 && model.QuoteData.BookedHolidayTaxiData.transferbookdata == null
                                    && model.FlightFailureStatus == false && model.HotelFailureStatus == false)
                                {
                                    if (!BookTransfersWithHolidayTaxi(model, model.QuoteHeaderId))
                                        return model;
                                }
                                else
                                {
                                    if (model.QuoteData.BookedHolidayTaxiData.general != null)
                                    {
                                        model.TransferStatusMessage = model.QuoteData.BookedHolidayTaxiData.general.bookingReference;
                                        model.TransferStatusCodes = (int)System.Net.HttpStatusCode.OK;
                                    }

                                }
                            }

                        }
                    }
                }


            }
            catch (Exception ex)
            {
                FillNonEzJetModel(model, model.QuoteHeaderId);
                BookingFailureEmails( model, controllerContext, "BookingFullfillmentFailure");
            }
            return model;
        }
        public void BookingFailureEmails(BookingFulfillmentModel model, ControllerContext controllerContext, string failureType)
        {
            GetEasyjetModelData(model);
            if (failureType == "TopDogFailure")
            {
                notificationHelper.SendTopDogFailureEMail(controllerContext, Convert.ToInt32(model.QuoteHeaderId), model.QuoteData, out toMailList);
                model.FulfillmentTransactionSummary = "Topdog posting failed, email has been sent to " + toMailList + " " + " with QuoteRef:" + " " + model.QuoteData.quoteHeader.CustomerReferenceCode.ToUpper().ToString();
            }
            else
            {
                BookingFullfillmentFailureEmail(Convert.ToInt32(model.QuoteHeaderId), model.QuoteData, controllerContext, out toMailList);
                model.FulfillmentTransactionSummary = "Booking fulfillment failure email has been sent to " + toMailList + " " + " with QuoteRef:" + " " + model.QuoteData.quoteHeader.CustomerReferenceCode.ToUpper().ToString();
            }
        }
        public void GetEasyjetModelData(BookingFulfillmentModel model)
        {
            List<EasyJetResponse> easyJetBookings = new List<EasyJetResponse>();
            if ((model.QuoteData.EasyJetBookingStatus != null && model.QuoteData.EasyJetBookingStatus.Count > 0) && model.QuoteData.flightComponents
                .Where(f => f.ComponentTypeOptionId.Equals(SFConstants.QuoteComponent_FlightReturn)).Count() > 0)
                easyJetBookings.Add(EasyJetHelper.GetEasyJetBooking(model.QuoteData.quoteHeader.QuoteHeadderId.ToString(), SFConstants.QuoteComponent_FlightReturn));
            else if ((model.QuoteData.EasyJetBookingStatus != null && model.QuoteData.EasyJetBookingStatus.Count > 0) && model.QuoteData.flightComponents
                .Where(f => f.ComponentTypeOptionId.Equals(SFConstants.QuoteComponent_Flightoneway)).Count() > 0)
            {
                easyJetBookings.Add(EasyJetHelper.GetEasyJetBooking(model.QuoteData.quoteHeader.QuoteHeadderId.ToString(), SFConstants.Direction_Inbound));
                easyJetBookings.Add(EasyJetHelper.GetEasyJetBooking(model.QuoteData.quoteHeader.QuoteHeadderId.ToString(), SFConstants.Direction_Outbound));
            }
            model.QuoteData.EasyJetBookings = easyJetBookings;
        }
        public void UpdateAgentNotes(string quoteHeaderID, string agentNotes)
        {
            BookingService serviceRepository = new BookingService();
            if(agentNotes.Length>0)
            agentNotes = agentNotes.Replace("\n", ",").Substring(0,agentNotes.Length-1);
            serviceRepository.UpdateAgentNotes(quoteHeaderID, agentNotes);
        }

        public BookingFulfillmentModel PostToTopDog(string quoteHeaderID, ControllerContext controllerContext)
        {
            BookingFulfillmentModel model = null;

            try
            {
                model = GetBookingFulfillmentModel(quoteHeaderID);
                FillNonEzJetModel(model, quoteHeaderID);
                model.QuoteHeaderId = quoteHeaderID;
                if (model.FlightFailureStatus || model.HotelFailureStatus || model.TransfersFailureStatus)
                {
                    GetEasyjetModelData(model);
                    BookingFullfillmentFailureEmail(Convert.ToInt32(model.QuoteHeaderId), model.QuoteData, controllerContext, out toMailList);
                    model.FulfillmentTransactionSummary = "Booking fulfillment failure email has been sent to " + toMailList + " " + " with QuoteRef:" + " " + model.QuoteData.quoteHeader.CustomerReferenceCode.ToUpper().ToString();
                }
                else // post to topdog
                {
                    BookingAcknowledgementEmail(Convert.ToInt32(model.QuoteHeaderId), model.QuoteData, controllerContext, out toMailList);
                    model.TopDogResponse = PostDataToTopDog(Convert.ToInt32(quoteHeaderID), model, controllerContext);
                    bool topDogSaveRes = saveTopDogResponse(model.TopDogResponse, Convert.ToInt32(model.QuoteHeaderId));
                }
            }
            catch (Exception ex)
            {
                BookingFailureEmails( model, controllerContext, "TopDogFailure");
            }
            return model;
        }
    }
}