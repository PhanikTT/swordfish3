﻿using System.Collections.Generic;
using System.Web.Mvc;
using System.Collections;
using Newtonsoft.Json;
using Swordfish.Businessobjects.Sesame;
using Swordfish.Businessobjects.Payments;
using Quotes = Swordfish.Businessobjects.Quotes;
using Swordfish.Businessobjects.Transfers;
using Swordfish.Components.BookingServices;
using Swordfish.Businessobjects.Notification;
using System.Linq;
using System.Xml;
using System.Xml.Linq;
using System;

namespace Swordfish.UI.Helpers
{
    public class BookingHelper
    {

        private IBookingService PackagingRepository;

        public BookingHelper(IBookingService packagingRepository)
        {
            PackagingRepository = packagingRepository;
        }

        public int SavePaymentDetails(List<PaymentDetail> payments)
        {
            return PackagingRepository.SavePaymentDetails(payments);
        }
        public int SaveCardEasyResponse(PaymentDetail paymentDetail)
        {
            var payments = new List<PaymentDetail>();
            payments.Add(paymentDetail);
            return PackagingRepository.SavePaymentDetails(payments);
        }
        public PaymentDetail TransformResponse(string response)
        {
            PaymentDetail paymentDetail = new PaymentDetail();
            if (!string.IsNullOrEmpty(response))
            {
                XmlDocument xdoc = new XmlDocument();//xml doc used for xml parsing
                xdoc.LoadXml(response);
                XmlNode basketNode = xdoc.SelectSingleNode("/postResult");
                XmlNode recurNode = xdoc.SelectSingleNode("/postResult/recur");
                XmlNode cardTypeNode = xdoc.SelectSingleNode("/postResult/cardType");
                XmlNode surchargeNode = xdoc.SelectSingleNode("/postResult/surcharge");
                XmlNode pspResponseNode = xdoc.SelectSingleNode("/postResult/pspResponse");
                if (pspResponseNode != null && !string.IsNullOrEmpty(pspResponseNode.InnerText))
                {
                    XDocument pspResponseDoc = XDocument.Parse(pspResponseNode.InnerText);
                    XNamespace xmlns1 = "TXN";
                    var transResNodes = pspResponseDoc.Descendants(xmlns1 + "transactionresponse").Nodes().ToList();
                    if (transResNodes != null && transResNodes.Count > 0)
                    {
                        paymentDetail = (from o in pspResponseDoc.Descendants(xmlns1 + "transactionresponse")
                                                 select new PaymentDetail
                                                 {
                                                     merchantRefrence = o.Element(xmlns1 + "merchantreference").Value,
                                                     transactionId = o.Element(xmlns1 + "transactionid").Value,
                                                     resultDatetime = o.Element(xmlns1 + "resultdatetimestring").Value,
                                                     processingdb = o.Element(xmlns1 + "processingdb").Value,
                                                     errormsg = o.Element(xmlns1 + "errormsg").Value,
                                                     merchantNumber = o.Element(xmlns1 + "merchantnumber").Value,
                                                     tid = o.Element(xmlns1 + "tid").Value,
                                                     schemeName = o.Element(xmlns1 + "schemename").Value,
                                                     messageNumber = o.Element(xmlns1 + "messagenumber").Value,
                                                     authCode = o.Element(xmlns1 + "authcode").Value,
                                                     authMessage = o.Element(xmlns1 + "authmessage").Value,
                                                     vertel = o.Element(xmlns1 + "vrtel").Value,
                                                     txnResult = o.Element(xmlns1 + "txnresult").Value,
                                                     pcavsResult = o.Element(xmlns1 + "pcavsresult").Value,
                                                     ad1avsResult = o.Element(xmlns1 + "ad1avsresult").Value,
                                                     cvcResult = o.Element(xmlns1 + "cvcresult").Value,
                                                     arc = o.Element(xmlns1 + "arc").Value,
                                                     iadarc = o.Element(xmlns1 + "iadarc").Value,
                                                     iadoad = o.Element(xmlns1 + "iadoad").Value,
                                                     isd = o.Element(xmlns1 + "isd").Value,
                                                     authorisingentity = o.Element(xmlns1 + "authorisingentity").Value
                                                 }).FirstOrDefault();
                        paymentDetail.quoteHeaderId = paymentDetail.merchantRefrence.Split('-')[1];
                        paymentDetail.id = Convert.ToInt32(paymentDetail.merchantRefrence.Split('-')[0]);
                        paymentDetail.recur = recurNode.InnerText;
                        paymentDetail.cardType = cardTypeNode.InnerText;
                        paymentDetail.surcharge = surchargeNode.InnerText;
                    }
                }
                XmlNode panNode = xdoc.SelectSingleNode("/postResult/pan");
                paymentDetail.pan = panNode.InnerText;
                XmlNode manualNode = xdoc.SelectSingleNode("/postResult/manual");
                paymentDetail.manual = manualNode.InnerText;
            }
            return paymentDetail;
        }
        public bool SaveQuoteOverrideDetails(Quotes.QuoteOverride quoteOverride)
        {
            return PackagingRepository.SaveQuoteOverrideDetails(quoteOverride);
        }

        public string SendQuoteAsEmailToLeadPassenger(string mobile, string quoteHeaderID, string email, ControllerContext controllerContext)
        {
            var notificationHelper = new NotificationHelper();
            return notificationHelper.SendSmsAndEmail(mobile, email, controllerContext, quoteHeaderID, Constants.SmsTypes.BookQuoteSMS.ToString());
        }

        public bool SaveSesameQuoteForCreateRefreshBook(SesameBasket sesameBasket)
        {
            return PackagingRepository.SaveSesameQuoteForCreateRefreshBook(sesameBasket);
        }

        public static TransferBookResponse BookedTransfersDataFromService(Request transferBookRequest)
        {
            SFLogger.logMessage("SearchResultsHelper:BookedTransfersDataFromService(): started!!.");
            string url = SFConfiguration.GetTravelSaasBookTransfers();
            var lstTransfersData = new TransferBookResponse();
            var list = new ArrayList();
            list.Add(transferBookRequest);
            var travelSAASClient = new TravelSAASRestClient();
            var result = travelSAASClient.ExecuteRequest(url, RequestType.Post, list);
            if (result.StatusCode == (int)System.Net.HttpStatusCode.OK)
                lstTransfersData = JsonConvert.DeserializeObject<TransferBookResponse>(result.Response);
            else
                lstTransfersData.StatusMessage = result.Response;
            lstTransfersData.StatusCode = result.StatusCode;
            SFLogger.logMessage("SearchResultsHelper:GetTransfersDataFromService(): end.");
            return lstTransfersData;
        }

        public bool SaveTransfersResponseData(TransfersBookData transfersBookData)
        {
            return PackagingRepository.SaveTransfersResponseData(transfersBookData);

        }
    }
}