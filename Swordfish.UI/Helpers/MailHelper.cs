﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Net.Mail;
using System.Net;
using System.Web.Script.Serialization;
using System.IO;
using System.Web.UI;
using iTextSharp.text;
using iTextSharp.text.html.simpleparser;
using iTextSharp.text.pdf;
using System.Text;
using System.Web.Mvc;
using System.Xml;
using Swordfish.Businessobjects.Notification;
using Swordfish.Businessobjects.Quotes;
using Swordfish.Businessobjects.Passengers;
using Swordfish.Businessobjects.Common;
using Swordfish.Businessobjects.Easyjet;

namespace Swordfish.UI.Helpers
{
    public class MailHelper
    {
        Email email = new Email();
        internal bool SendMail(Email email)
        {
            SmtpClient SmtpServer = new SmtpClient(SFConfiguration.getSMTPServer());
            SmtpServer.Port = Convert.ToInt32(SFConfiguration.getPort());
            SmtpServer.Credentials = new System.Net.NetworkCredential(SFConfiguration.getSenderEmail(), SFConfiguration.getSenderPassword());
            SmtpServer.EnableSsl = Convert.ToBoolean(SFConfiguration.getSsl());
            MailMessage message = new MailMessage();
            if (string.IsNullOrEmpty(email.FromEmail))
            {
                message.From = new MailAddress(SFConfiguration.getSenderEmail(), SFConfiguration.GetEmailDisplayName());
            }
            else
            {
                message.From = new MailAddress(email.FromEmail, SFConfiguration.GetEmailDisplayName());
            }
            if (email.FileType != null)
            {
                message.Attachments.Add(new Attachment(new MemoryStream(ConvertHtmlToPdf(email.AttachementFileString)), "QuoteDetails" + FileName(email.FileType)));
            }
            message.To.Add(email.ToEmail);
            message.Subject = email.Subject.ToString();
            message.Body = email.Body;
            message.IsBodyHtml = true;
            SmtpServer.Send(message);
            return true;
        }

        private byte[] ConvertHtmlToPdf(string htmlcode)
        {
            byte[] bytes = null;
            using (StringWriter sw = new StringWriter())
            {
                using (HtmlTextWriter hw = new HtmlTextWriter(sw))
                {
                    StringReader sr = new StringReader(htmlcode);

                    Document pdfDoc = new Document(PageSize.A4, 5f, 5f, 5f, 45f);
                    HTMLWorker htmlparser = new HTMLWorker(pdfDoc);
                    using (MemoryStream memoryStream = new MemoryStream())
                    {
                        PdfWriter writer = PdfWriter.GetInstance(pdfDoc, memoryStream);
                        pdfDoc.Open();
                        writer.PageEvent = new AddHeaderFooterHelper();
                        htmlparser.Parse(sr);
                        pdfDoc.Close();
                        bytes = memoryStream.ToArray();
                        memoryStream.Close();
                    }
                }
            }
            return bytes;
        }
        private string FileName(string fileType)
        {
            string fileName = string.Empty;
            DateTime fileCreationDatetime = DateTime.Now;
            switch (fileType)
            {
                case ".pdf":
                    fileName = string.Format("{0}.pdf", fileCreationDatetime.ToString(@"yyyyMMdd") + "_" + fileCreationDatetime.ToString(@"HHmmss"));
                    break;
                case ".doc":
                    fileName = string.Format("{0}.doc", fileCreationDatetime.ToString(@"yyyyMMdd") + "_" + fileCreationDatetime.ToString(@"HHmmss"));
                    break;
                case ".xls":
                    fileName = string.Format("{0}.xls", fileCreationDatetime.ToString(@"yyyyMMdd") + "_" + fileCreationDatetime.ToString(@"HHmmss"));
                    break;
            }
            return fileName;
        }

        internal bool SendSMS(SMS sms)
        {
            string globalSMS_Url = SFConfiguration.GetGlobalSMSUrl();
            string globalSMS_Key = SFConfiguration.GetGlobalSMSAccountSid();
            string globalSMS_Secret = SFConfiguration.GetGlobalSMSAuthToken();

            string Uri = string.Format(globalSMS_Url, globalSMS_Key, globalSMS_Secret, sms.ToPhoneNumber, sms.Message);

            var json = new WebClient().DownloadString(Uri);
            var GlobalSMSResponse = ParseSmsResponseJson(json);

            if (GlobalSMSResponse.Messages[0].Status == "0")
                return true;
            else
                return false;
        }

        private SmsResponse ParseSmsResponseJson(string json)
        {
            json = json.Replace("message-count", "MessageCount");
            json = json.Replace("message-price", "MessagePrice");
            json = json.Replace("message-id", "MessageId");
            json = json.Replace("remaining-balance", "RemainingBalance");
            return new JavaScriptSerializer().Deserialize<SmsResponse>(json);
        }
        private string CheckAndReplaceReservedWords(Dictionary<string, string> quoteData, string emailContent)
        {
            string outvalue = string.Empty;
            if (emailContent.Contains("<![CDATA["))
                emailContent = emailContent.Replace("<![CDATA[", "").Replace("]]>", "");
            if (emailContent.Contains("@@"))
            {
                string[] separators = new string[] { "!!" };
                string[] split = emailContent.Split(separators, StringSplitOptions.None);
                foreach (string s in split)
                {
                    if (s != null && s.Contains("@@"))
                    {
                        int index1 = s.IndexOf("@@");
                        string reservedword = s.Substring(s.IndexOf("@@"));
                        quoteData.TryGetValue(reservedword.Substring(2), out outvalue);
                        if (outvalue != null)
                        {
                            emailContent = emailContent.Replace(reservedword + "!!", outvalue);
                        }
                    }
                }
            }
            if (emailContent.Contains("@@bagQuantity!!"))
            { emailContent = "<b>Baggage is not added to this Quote</b>"; }
            if (emailContent.Contains("@@Pickupdate_AirportToHotel!!"))
            { emailContent = "<table width='95%' align='center'><tbody><tr><td><b>Transfers is not added to this Quote</b></td></tr></tbody></table>"; }
            if (emailContent.Contains("@@TransferType_FromAirportToHotel!!") && emailContent.Contains("OB and IB included"))
            {
                int index = emailContent.IndexOf("@@TransferType_FromAirportToHotel!!");
                int index1 = emailContent.IndexOf("All elements");
                emailContent = emailContent.Remove(index, index1 - index);
            }
            return emailContent;
        }
        public bool SendQuoteByEMail(string quoteHeaderId, ControllerContext controllerContext, Constants.EmailTypes EmailType, string Email)
        {
            string path = "~/Xmldata/CustomerCommunication.xml";
            string bodyContent = string.Empty;
            string fromEmailContent = string.Empty;
            string toEmailContent = string.Empty;
            string attachement = string.Empty;
            string filePath = string.Empty;
            if (HttpContext.Current != null)
            {
                filePath = HttpContext.Current.Server.MapPath(@"" + path + "");
            }
            else {
                filePath = HttpRuntime.AppDomainAppPath + path.Replace("~", string.Empty).Replace('/', '\\');
            }
            QuoteCommunication QuoteCommunication = new QuoteCommunication();
            Dictionary<string, string> dictionaryEmailContent = new Dictionary<string, string>();
            XmlDocument document = new XmlDocument();
            document.Load(filePath);
            var quoteData = GetQuote(quoteHeaderId);
            var QuoteData = LoadQuoteDataIntoDictionary(quoteHeaderId, EmailType.ToString(), quoteData);
            string quoteReference = QuoteData["quoteReference"];
            XmlNode quickQuoteBodyEMailNodes = document.SelectSingleNode("/Communications/EmailCmmunication/" + EmailType + "/Body");
            XmlNode quickQuoteBodyEMailSubjectNodes = document.SelectSingleNode("/Communications/EmailCmmunication/" + EmailType + "/Subject");
            XmlNode quickQuoteEMailNodes = document.SelectSingleNode("/Communications/EmailCmmunication/" + EmailType + "/Attachment");
            XmlNode quickQuoteFromEmailNodes = document.SelectSingleNode("/Communications/EmailCmmunication/" + EmailType + "/FromEmailAddress");
            XmlNode quickQuoteToEmailNodes = document.SelectSingleNode("/Communications/EmailCmmunication/" + EmailType + "/ToEmailAddress");
            XmlNodeList nodes = Manipulatenodes(quickQuoteEMailNodes.ChildNodes, QuoteData);
            foreach (XmlNode node in nodes)
            {
                string TempComtent = node.InnerXml;
                dictionaryEmailContent.Add(node.Name, CheckAndReplaceReservedWords(QuoteData, TempComtent));
            }
            if (quickQuoteBodyEMailNodes.InnerText != string.Empty)
            {
                bodyContent = quickQuoteBodyEMailNodes.ChildNodes[0].Value.Replace("\r\n", string.Empty).Trim();
            }
            if (quickQuoteFromEmailNodes.InnerText != string.Empty)
            {
                fromEmailContent = quickQuoteFromEmailNodes.ChildNodes[0].Value.Replace("\r\n", string.Empty).Trim();
            }
            if (quickQuoteToEmailNodes.InnerText != string.Empty)
            {
                toEmailContent = quickQuoteToEmailNodes.ChildNodes[0].Value.Replace("\r\n", string.Empty).Trim() + ";" + Email;
            }
            else
            {
                toEmailContent = Email;
            }
            string subject = quickQuoteBodyEMailSubjectNodes.ChildNodes[0].Value.Replace("\r\n", string.Empty).Trim();
            if (subject.Contains("@@"))
            {
                string[] separators = new string[] { "!!" };
                string[] split = subject.Split(separators, StringSplitOptions.None);
                foreach (string s in split)
                {
                    if (s != null && s.Contains("@@"))
                    {
                        int index1 = s.IndexOf("@@");
                        string reservedword = s.Substring(s.IndexOf("@@"));
                        if (!string.IsNullOrEmpty(quoteReference))
                        {
                            subject = subject.Replace(reservedword + "!!", quoteReference);
                        }
                    }
                }
            }
            QuoteCommunication.quoteDictionary = dictionaryEmailContent;
            if (quickQuoteEMailNodes.InnerText != string.Empty)
            {
                attachement = ViewRenderer.RenderPartialView("GenericEmailTemplate", QuoteCommunication, controllerContext);
            }
            email.Body = bodyContent;
            email.FromEmail = fromEmailContent;
            email.ToEmail = toEmailContent.Replace(";", ",");
            email.Subject = subject;
            if (!string.IsNullOrEmpty(attachement))
            {
                email.FileType = ".pdf";
                email.AttachementFileString = attachement;
            }
            bool result = SendMail(email);
            return result;
        }
        public bool SendLiveShareEMail(string leadPassingerName, string url, ControllerContext controllerContext, Guid guid, Constants.EmailTypes SendLiveShareEmail, int quoteHeaderId, string Email, string quoteReference)
        {
            string path = "~/Xmldata/CustomerCommunication.xml";
            string filePath = string.Empty;
            string fromEmailContent = string.Empty;
            string toEmailContent = string.Empty;
            string attachement = string.Empty;
            if (HttpContext.Current != null)
            {
                filePath = HttpContext.Current.Server.MapPath(@"" + path + "");
            }
            else {
                filePath = HttpRuntime.AppDomainAppPath + path.Replace("~", string.Empty).Replace('/', '\\');
            }
            QuoteCommunication QuoteCommunication = new QuoteCommunication();
            Dictionary<string, string> dictionaryEmailContent = new Dictionary<string, string>();
            XmlDocument document = new XmlDocument();
            document.Load(filePath);
            string[] replaceobjects = { "leadPassingerName::" + leadPassingerName, "url::" + url, "guid::" + guid.ToString(), "quoteHeaderId::" + quoteHeaderId.ToString(), "Email::" + Email };
            var QuoteData = LoadDataIntoDictionary(replaceobjects);
            XmlNode SendLiveShareEMailNodes = document.SelectSingleNode("/Communications/EmailCmmunication/" + SendLiveShareEmail + "/Body");
            XmlNode SendLiveShareEMailSubjectNodes = document.SelectSingleNode("/Communications/EmailCmmunication/" + SendLiveShareEmail + "/Subject");
            XmlNode LiveShareFromEmailNodes = document.SelectSingleNode("/Communications/EmailCmmunication/" + SendLiveShareEmail + "/FromEmailAddress");
            XmlNode LiveShareToEmailNodes = document.SelectSingleNode("/Communications/EmailCmmunication/" + SendLiveShareEmail + "/ToEmailAddress");
            XmlNode LiveShareAttachement = document.SelectSingleNode("/Communications/EmailCmmunication/" + SendLiveShareEmail + "/Attachment");
            foreach (XmlNode node in SendLiveShareEMailNodes.ChildNodes)
            {
                string TempComtent = node.Value;
                dictionaryEmailContent.Add(node.Name, CheckAndReplaceReservedWords(QuoteData, TempComtent));
            }
            QuoteCommunication.quoteDictionary = dictionaryEmailContent;
            if (LiveShareFromEmailNodes.InnerText != string.Empty)
            {
                fromEmailContent = LiveShareFromEmailNodes.ChildNodes[0].Value.Replace("\r\n", string.Empty).Trim();
            }
            if (LiveShareToEmailNodes.InnerText != string.Empty)
            {
                toEmailContent = LiveShareToEmailNodes.ChildNodes[0].Value.Replace("\r\n", string.Empty).Trim() + ";" + Email;
            }
            else
            {
                toEmailContent = Email;
            }
            string subject = SendLiveShareEMailSubjectNodes.ChildNodes[0].Value.Replace("\r\n", string.Empty).Trim();
            if (subject.Contains("@@"))
            {
                string[] separators = new string[] { "!!" };
                string[] split = subject.Split(separators, StringSplitOptions.None);
                foreach (string s in split)
                {
                    if (s != null && s.Contains("@@"))
                    {
                        int index1 = s.IndexOf("@@");
                        string reservedword = s.Substring(s.IndexOf("@@"));
                        if (!string.IsNullOrEmpty(quoteReference))
                        {
                            subject = subject.Replace(reservedword + "!!", quoteReference);
                        }
                    }
                }
            }
            if (LiveShareAttachement.InnerText != string.Empty)
            {
                attachement = ViewRenderer.RenderPartialView("GenericEmailTemplate", QuoteCommunication, controllerContext);
            }
            string emailString = ViewRenderer.RenderPartialView("GenericEmailTemplate", QuoteCommunication, controllerContext);
            Email email = new Email();
            email.FromEmail = fromEmailContent;
            email.ToEmail = toEmailContent.Replace(";", ",");
            email.Subject = subject;
            email.Body = emailString;
            if (!string.IsNullOrEmpty(attachement))
            {
                email.FileType = ".pdf";
                email.AttachementFileString = emailString;
            }
            bool isMailSent = SendMail(email);
            return isMailSent;
        }
        public bool SendTopDogFailureEMail(ControllerContext controllerContext, int quoteHeaderId, Swordfish.Businessobjects.Quotes.Quote quoteData, Constants.EmailTypes TopDogFailureEmail, out string emailList)
        {
            string path = "~/Xmldata/CustomerCommunication.xml";
            string bodyContent = string.Empty;
            string fromEmailContent = string.Empty;
            string toEmailContent = string.Empty;
            string attachement = string.Empty;
            string filePath = string.Empty;
            if (HttpContext.Current != null)
            {
                filePath = HttpContext.Current.Server.MapPath(@"" + path + "");
            }
            else {
                filePath = HttpRuntime.AppDomainAppPath + path.Replace("~", string.Empty).Replace('/', '\\');
            }
            QuoteCommunication TopDogCommunication = new QuoteCommunication();
            Dictionary<string, string> dictionaryEmailContent = new Dictionary<string, string>();
            XmlDocument document = new XmlDocument();
            document.Load(filePath);
            var QuoteData = LoadQuoteDataIntoDictionary(quoteHeaderId.ToString(), TopDogFailureEmail.ToString(), quoteData);
            string agentEmailAddress = QuoteData["Agent.userEmailAddress"];
            XmlNode quickQuoteBodyEMailNodes = document.SelectSingleNode("/Communications/EmailCmmunication/" + TopDogFailureEmail + "/Body");
            XmlNode quickQuoteBodyEMailSubjectNodes = document.SelectSingleNode("/Communications/EmailCmmunication/" + TopDogFailureEmail + "/Subject");
            XmlNode quickQuoteEMailNodes = document.SelectSingleNode("/Communications/EmailCmmunication/" + TopDogFailureEmail + "/Attachment");
            XmlNode quickQuoteFromEmailNodes = document.SelectSingleNode("/Communications/EmailCmmunication/" + TopDogFailureEmail + "/FromEmailAddress");
            XmlNode quickQuoteToEmailNodes = document.SelectSingleNode("/Communications/EmailCmmunication/" + TopDogFailureEmail + "/ToEmailAddress");
            XmlNodeList nodes = Manipulatenodes(quickQuoteEMailNodes.ChildNodes, QuoteData);
            foreach (XmlNode node in nodes)
            {
                string TempComtent = node.InnerXml;
                dictionaryEmailContent.Add(node.Name, CheckAndReplaceReservedWords(QuoteData, TempComtent));
            }
            if (quickQuoteBodyEMailNodes.InnerText != string.Empty)
            {
                bodyContent = quickQuoteBodyEMailNodes.ChildNodes[0].Value.Replace("\r\n", string.Empty).Trim();
            }
            if (quickQuoteFromEmailNodes.InnerText != string.Empty)
            {
                fromEmailContent = quickQuoteFromEmailNodes.ChildNodes[0].Value.Replace("\r\n", string.Empty).Trim();
            }
            if (quickQuoteToEmailNodes.InnerText != string.Empty)
            {
                string serverEnvironment = SFConfiguration.GetEnvironment();
                if (!serverEnvironment.Equals(Constants.ServerEnvironments.uat.ToString()))
                {
                    toEmailContent = quickQuoteToEmailNodes.ChildNodes[0].Value.Replace("\r\n", string.Empty).Trim() + ";" + agentEmailAddress;
                }
                else
                {
                    toEmailContent = SFConfiguration.GetTopDogToEmailList() + ";" + agentEmailAddress;
                }

            }
            else
            {
                toEmailContent = agentEmailAddress;
            }
            string subject = quickQuoteBodyEMailSubjectNodes.ChildNodes[0].Value.Replace("\r\n", string.Empty).Trim();
            if (subject.Contains("@@"))
            {
                string[] separators = new string[] { "!!" };
                string[] split = subject.Split(separators, StringSplitOptions.None);
                foreach (string s in split)
                {
                    if (s != null && s.Contains("@@"))
                    {
                        int index1 = s.IndexOf("@@");
                        string reservedword = s.Substring(s.IndexOf("@@"));
                        if (quoteHeaderId.ToString() != null)
                        {
                            subject = subject.Replace(reservedword + "!!", quoteData.quoteHeader.CustomerReferenceCode);
                        }
                    }
                }
            }
            TopDogCommunication.quoteDictionary = dictionaryEmailContent;
            if (quickQuoteEMailNodes.InnerText != string.Empty)
            {
                attachement = ViewRenderer.RenderPartialView("GenericEmailTemplate", TopDogCommunication, controllerContext);
            }
            email.Body = bodyContent;
            email.FromEmail = fromEmailContent;
            email.ToEmail = toEmailContent.Replace(";", ",");
            emailList = toEmailContent.Replace(";", ",");
            email.Subject = subject;
            if (!string.IsNullOrEmpty(attachement))
            {
                email.FileType = ".pdf";
                email.AttachementFileString = attachement;
            }
            bool result = SendMail(email);
            return result;
        }
        public bool SendBookingFullFillmentFailureEmail(ControllerContext controllerContext, int quoteHeaderId, Swordfish.Businessobjects.Quotes.Quote quoteData, Constants.EmailTypes BookingFullFillmentFailureEmail, out string emailList)
        {
            string path = "~/Xmldata/CustomerCommunication.xml";
            string bodyContent = string.Empty;
            string fromEmailContent = string.Empty;
            string toEmailContent = string.Empty;
            string attachement = string.Empty;
            string filePath = string.Empty;
            if (HttpContext.Current != null)
            {
                filePath = HttpContext.Current.Server.MapPath(@"" + path + "");
            }
            else {
                filePath = HttpRuntime.AppDomainAppPath + path.Replace("~", string.Empty).Replace('/', '\\');
            }
            QuoteCommunication TopDogCommunication = new QuoteCommunication();
            Dictionary<string, string> dictionaryEmailContent = new Dictionary<string, string>();
            XmlDocument document = new XmlDocument();
            document.Load(filePath);
            var QuoteData = LoadQuoteDataIntoDictionary(quoteHeaderId.ToString(), BookingFullFillmentFailureEmail.ToString(), quoteData);
            string agentEmailAddress = QuoteData["Agent.userEmailAddress"];
            XmlNode quickQuoteBodyEMailNodes = document.SelectSingleNode("/Communications/EmailCmmunication/" + BookingFullFillmentFailureEmail + "/Body");
            XmlNode quickQuoteBodyEMailSubjectNodes = document.SelectSingleNode("/Communications/EmailCmmunication/" + BookingFullFillmentFailureEmail + "/Subject");
            XmlNode quickQuoteEMailNodes = document.SelectSingleNode("/Communications/EmailCmmunication/" + BookingFullFillmentFailureEmail + "/Attachment");
            XmlNode quickQuoteFromEmailNodes = document.SelectSingleNode("/Communications/EmailCmmunication/" + BookingFullFillmentFailureEmail + "/FromEmailAddress");
            XmlNode quickQuoteToEmailNodes = document.SelectSingleNode("/Communications/EmailCmmunication/" + BookingFullFillmentFailureEmail + "/ToEmailAddress");
            XmlNodeList nodes = Manipulatenodes(quickQuoteEMailNodes.ChildNodes, QuoteData);
            foreach (XmlNode node in nodes)
            {
                string TempComtent = node.InnerXml;
                dictionaryEmailContent.Add(node.Name, CheckAndReplaceReservedWords(QuoteData, TempComtent));
            }
            if (quickQuoteBodyEMailNodes.InnerText != string.Empty)
            {
                bodyContent = quickQuoteBodyEMailNodes.ChildNodes[0].Value.Replace("\r\n", string.Empty).Trim();
            }
            if (quickQuoteFromEmailNodes.InnerText != string.Empty)
            {
                fromEmailContent = quickQuoteFromEmailNodes.ChildNodes[0].Value.Replace("\r\n", string.Empty).Trim();
            }
            if (quickQuoteToEmailNodes.InnerText != string.Empty)
            {
                string serverEnvironment = SFConfiguration.GetEnvironment();
                if (!serverEnvironment.Equals(Constants.ServerEnvironments.uat.ToString()))
                {
                    toEmailContent = quickQuoteToEmailNodes.ChildNodes[0].Value.Replace("\r\n", string.Empty).Trim() + ";" + agentEmailAddress;
                }
                else
                {
                    toEmailContent = SFConfiguration.GetTopDogToEmailList() + ";" + agentEmailAddress;
                }

            }
            else
            {
                toEmailContent = agentEmailAddress;
            }
            string subject = quickQuoteBodyEMailSubjectNodes.ChildNodes[0].Value.Replace("\r\n", string.Empty).Trim();
            if (subject.Contains("@@"))
            {
                string[] separators = new string[] { "!!" };
                string[] split = subject.Split(separators, StringSplitOptions.None);
                foreach (string s in split)
                {
                    if (s != null && s.Contains("@@"))
                    {
                        int index1 = s.IndexOf("@@");
                        string reservedword = s.Substring(s.IndexOf("@@"));
                        if (quoteHeaderId.ToString() != null)
                        {
                            subject = subject.Replace(reservedword + "!!", quoteData.quoteHeader.CustomerReferenceCode);
                        }
                    }
                }
            }
            TopDogCommunication.quoteDictionary = dictionaryEmailContent;
            if (quickQuoteEMailNodes.InnerText != string.Empty)
            {
                attachement = ViewRenderer.RenderPartialView("GenericEmailTemplate", TopDogCommunication, controllerContext);
            }
            email.Body = bodyContent;
            email.FromEmail = fromEmailContent;
            email.ToEmail = toEmailContent.Replace(";", ",");
            emailList = toEmailContent.Replace(";", ",");
            email.Subject = subject;
            if (!string.IsNullOrEmpty(attachement))
            {
                email.FileType = ".pdf";
                email.AttachementFileString = attachement;
            }
            bool result = SendMail(email);
            return result;
        }

        public bool SendBookingAcknowledgementEmail(ControllerContext controllerContext, int quoteHeaderId, Swordfish.Businessobjects.Quotes.Quote quoteData, Constants.EmailTypes BookingAcknowledgementEmail, out string emailList)
        {
            string path = "~/Xmldata/CustomerCommunication.xml";
            string bodyContent = string.Empty;
            string fromEmailContent = string.Empty;
            string toEmailContent = string.Empty;
            string attachement = string.Empty;
            string filePath = string.Empty;
            if (HttpContext.Current != null)
            {
                filePath = HttpContext.Current.Server.MapPath(@"" + path + "");
            }
            else {
                filePath = HttpRuntime.AppDomainAppPath + path.Replace("~", string.Empty).Replace('/', '\\');
            }
            QuoteCommunication TopDogCommunication = new QuoteCommunication();
            Dictionary<string, string> dictionaryEmailContent = new Dictionary<string, string>();
            XmlDocument document = new XmlDocument();
            document.Load(filePath);
            var QuoteData = LoadQuoteDataIntoDictionary(quoteHeaderId.ToString(), BookingAcknowledgementEmail.ToString(), quoteData);
            string customerEmailAddress = QuoteData["Email1"];
            XmlNode quickQuoteBodyEMailNodes = document.SelectSingleNode("/Communications/EmailCmmunication/" + BookingAcknowledgementEmail + "/Body");
            XmlNode quickQuoteBodyEMailSubjectNodes = document.SelectSingleNode("/Communications/EmailCmmunication/" + BookingAcknowledgementEmail + "/Subject");
            XmlNode quickQuoteEMailNodes = document.SelectSingleNode("/Communications/EmailCmmunication/" + BookingAcknowledgementEmail + "/Attachment");
            XmlNode quickQuoteFromEmailNodes = document.SelectSingleNode("/Communications/EmailCmmunication/" + BookingAcknowledgementEmail + "/FromEmailAddress");
            XmlNode quickQuoteToEmailNodes = document.SelectSingleNode("/Communications/EmailCmmunication/" + BookingAcknowledgementEmail + "/ToEmailAddress");
            XmlNodeList nodes = Manipulatenodes(quickQuoteEMailNodes.ChildNodes, QuoteData);
            foreach (XmlNode node in nodes)
            {
                string TempComtent = node.InnerXml;
                dictionaryEmailContent.Add(node.Name, CheckAndReplaceReservedWords(QuoteData, TempComtent));
            }
            if (quickQuoteBodyEMailNodes.InnerText != string.Empty)
            {
                bodyContent = quickQuoteBodyEMailNodes.ChildNodes[0].Value.Replace("\r\n", string.Empty).Trim();
            }
            if (quickQuoteFromEmailNodes.InnerText != string.Empty)
            {
                fromEmailContent = quickQuoteFromEmailNodes.ChildNodes[0].Value.Replace("\r\n", string.Empty).Trim();
            }
            if (quickQuoteToEmailNodes.InnerText != string.Empty)
            {
                string serverEnvironment = SFConfiguration.GetEnvironment();
                if (!serverEnvironment.Equals(Constants.ServerEnvironments.uat.ToString()))
                {
                    toEmailContent = quickQuoteToEmailNodes.ChildNodes[0].Value.Replace("\r\n", string.Empty).Trim() + ";" + customerEmailAddress;
                }
                else
                {
                    toEmailContent = SFConfiguration.GetTopDogToEmailList() + ";" + customerEmailAddress;
                }

            }
            else
            {
                toEmailContent = customerEmailAddress;
            }
            string subject = quickQuoteBodyEMailSubjectNodes.ChildNodes[0].Value.Replace("\r\n", string.Empty).Trim();
            if (subject.Contains("@@"))
            {
                string[] separators = new string[] { "!!" };
                string[] split = subject.Split(separators, StringSplitOptions.None);
                foreach (string s in split)
                {
                    if (s != null && s.Contains("@@"))
                    {
                        int index1 = s.IndexOf("@@");
                        string reservedword = s.Substring(s.IndexOf("@@"));
                        if (quoteHeaderId.ToString() != null)
                        {
                            subject = subject.Replace(reservedword + "!!", quoteData.quoteHeader.CustomerReferenceCode);
                        }
                    }
                }
            }
            TopDogCommunication.quoteDictionary = dictionaryEmailContent;
            if (quickQuoteEMailNodes.InnerText != string.Empty)
            {
                attachement = ViewRenderer.RenderPartialView("GenericEmailTemplate", TopDogCommunication, controllerContext);
            }
            email.Body = bodyContent;
            email.FromEmail = fromEmailContent;
            email.ToEmail = toEmailContent.Replace(";", ",");
            emailList = toEmailContent.Replace(";", ",");
            email.Subject = subject;
            if (!string.IsNullOrEmpty(attachement))
            {
                email.FileType = ".pdf";
                email.AttachementFileString = attachement;
            }
            bool result = SendMail(email);
            return result;
        }
        private string manipulatesmsstring(string from, string to, int count, string nodevalue, Dictionary<string, string> DictionaryData)
        {

            int fromIndex = nodevalue.IndexOf(from);
            int toIndex = nodevalue.IndexOf(to);
            string manipulatedstring = nodevalue.Substring(fromIndex, (toIndex - fromIndex));
            string result = appendtexttonode(manipulatedstring, count.ToString(), from, DictionaryData);
            nodevalue = nodevalue.Replace(manipulatedstring, result);
            return nodevalue;
        }
        public XmlNodeList Manipulatenodes(XmlNodeList childNodes, Dictionary<string, string> quoteData)
        {
            XmlNodeList nodes = childNodes;
            string flightnode = string.Empty;
            string baggagenode = string.Empty;
            string baggageemptynode1 = string.Empty;
            string baggageemptynode2 = string.Empty;
            int flightKey = 0;
            int PassengerKey = 0;
            int BaggageKey = 0;
            int BaggageKey1 = 0;
            int BaggageKey2 = 0;
            int count = 0;
            int paymentKey = 0;
            string passengerNode = string.Empty;
            string paymentnode = string.Empty;
            string passengerCount = string.Empty;
            string flightCount = string.Empty;
            string baggageCount = string.Empty;
            string paymentCount = string.Empty;
            quoteData.TryGetValue("PassengerCount", out passengerCount);
            quoteData.TryGetValue("FlightCount", out flightCount);
            quoteData.TryGetValue("BaggageCount", out baggageCount);
            quoteData.TryGetValue("PaymentCount", out paymentCount);
            foreach (XmlNode node in nodes)
            {
                count++;
                if (node.InnerText.Contains("FlightData"))
                {
                    flightKey = count;
                    if (Convert.ToInt32(flightCount) > 0)
                        flightnode = appendtexttonode(node.InnerText, flightCount, "FlightData", quoteData);
                }
                else if (node.InnerText.Contains("Passenger Names"))
                {
                    PassengerKey = count;
                    if (Convert.ToInt32(passengerCount) > 1)
                        passengerNode = appendtexttonode(node.InnerText, passengerCount, "Passenger Names", quoteData);
                }
                else if (node.InnerText.Contains("BaggageData"))
                {
                    BaggageKey = count;
                    if (Convert.ToInt32(baggageCount) > 1)
                        baggagenode = appendtexttonode(node.InnerText, baggageCount, "BaggageData", quoteData);

                }
                else if (node.InnerText.Contains("bagsempty1"))
                {
                    BaggageKey1 = count;

                    baggageemptynode1 = appendtexttonode(node.InnerText, baggageCount, "bagsempty1", quoteData);

                }
                else if (node.InnerText.Contains("bagsempty2"))
                {
                    BaggageKey2 = count;

                    baggageemptynode2 = appendtexttonode(node.InnerText, baggageCount, "bagsempty2", quoteData);

                }
                else if (node.InnerText.Contains("PaymentData"))
                {
                    paymentKey = count;
                    if (Convert.ToInt32(paymentCount) >= 0)
                        paymentnode = appendtexttonode(node.InnerText, paymentCount, "PaymentData", quoteData);
                }
            }
            if (flightKey != 0)
                nodes.Item(flightKey - 1).InnerText = flightnode;
            if (PassengerKey != 0)
                nodes.Item(PassengerKey - 1).InnerText = passengerNode;
            if (BaggageKey != 0)
                nodes.Item(BaggageKey - 1).InnerText = baggagenode;
            if (paymentKey != 0)
                nodes.Item(paymentKey - 1).InnerText = paymentnode;
            if (BaggageKey1 != 0)
                nodes.Item(BaggageKey1 - 1).InnerText = baggageemptynode1;
            if (BaggageKey2 != 0)
                nodes.Item(BaggageKey2 - 1).InnerText = baggageemptynode2;
            return nodes;
        }

        private string appendtexttonode(string InnerText, string count, string reservedword, Dictionary<string, string> DictionaryData)
        {
            if ((reservedword == "bagsempty1" || reservedword == "bagsempty2") && Convert.ToInt32(count) == 0)
            {
                return string.Empty;
            }
            else if ((reservedword == "bagsempty1" || reservedword == "bagsempty2") && Convert.ToInt32(count) > 0)
            {
                return InnerText.Replace(reservedword, "");
            }
            string nodetext = InnerText.Replace(reservedword, "");
            string outvalue = string.Empty;

            string quoteoutboundCount = string.Empty;
            string quoteinboundCount = string.Empty;
            string quoteoutboundValue = string.Empty;
            string quoteinboundValue = string.Empty;

            string outboundCount = string.Empty;
            string outboundValue = string.Empty;
            string inboundCount = string.Empty;
            string inboundValue = string.Empty;

            DictionaryData.TryGetValue("flightarrival", out outvalue);
            DictionaryData.TryGetValue("Direction_QuoteOutboundCount", out quoteoutboundCount);
            DictionaryData.TryGetValue("Direction_QuoteInboundCount", out quoteinboundCount);
            DictionaryData.TryGetValue("Direction_OutboundCount", out outboundCount);
            DictionaryData.TryGetValue("Direction_InboundCount", out inboundCount);


            for (int i = 1; i <= Convert.ToInt32(count); i++)
            {
                string pass = InnerText.Replace(reservedword, "");
                if (reservedword == "Flights data" && Convert.ToInt32(outvalue) <= i)
                {
                    pass = pass.Replace("Departing", "Arriving");
                }
                pass = pass.Replace("1", i.ToString());
                if (i != 1)
                    nodetext += pass;
            }
            if (outboundCount != null && inboundCount != null)
            {
                if (outboundCount.Contains('_'))
                    outboundValue = outboundCount.Split('_')[1];
                if (inboundCount.Contains('_'))
                    inboundValue = inboundCount.Split('_')[1];
                if (Convert.ToInt32(outboundValue) > Convert.ToInt32(inboundValue))
                    outboundValue = outboundCount.Split('_')[1];
                else
                    outboundValue = inboundCount.Split('_')[1];

                if (reservedword == "FlightData" && Convert.ToInt32(count) > 2)
                {
                    string RemoveWord = "@@SupplierCost1!!";
                    for (int j = 2; j <= Convert.ToInt32(count); j++)
                    {
                        string AUditWord = RemoveWord;
                        AUditWord = AUditWord.Replace("1", j.ToString());

                        if (j != (Convert.ToInt32(outboundValue)))
                            nodetext = nodetext.Replace(AUditWord, "");

                    }
                }
            }
            if (quoteoutboundCount != null && quoteinboundCount != null)
            {
                if (quoteoutboundCount.Contains('_'))
                    quoteoutboundValue = quoteoutboundCount.Split('_')[1];
                if (quoteinboundCount.Contains('_'))
                    quoteinboundValue = quoteinboundCount.Split('_')[1];
                if (Convert.ToInt32(quoteoutboundValue) > Convert.ToInt32(quoteinboundValue))
                    quoteoutboundValue = quoteoutboundCount.Split('_')[1];
                else
                    quoteoutboundValue = quoteinboundCount.Split('_')[1];

                if (reservedword == "FlightData" && Convert.ToInt32(count) > 2)
                {
                    string RemoveWord = "@@QuotePrice1!!";
                    for (int j = 2; j <= Convert.ToInt32(count); j++)
                    {
                        string AUditWord = RemoveWord;
                        AUditWord = AUditWord.Replace("1", j.ToString());

                        if (j != (Convert.ToInt32(quoteoutboundValue)))
                            nodetext = nodetext.Replace(AUditWord, "");

                    }
                }
            }

            if (reservedword == "BaggageData" && Convert.ToInt32(count) > 2)
            {
                string RemoveWord = "@@baggageTotalPrice1!!";
                for (int j = 2; j <= Convert.ToInt32(count); j++)
                {

                    string AUditWord = RemoveWord;
                    AUditWord = AUditWord.Replace("1", j.ToString());

                    if (j == (Convert.ToInt32(count) / 2) + 1)
                    {
                        nodetext = nodetext.Replace(AUditWord, "@@baggageTotalPrice2!!");
                    }
                    else {
                        nodetext = nodetext.Replace(AUditWord, "");
                    }
                }
            }
            return nodetext;
        }

        public bool sendQuoteBySms(string quoteHeaderId, Constants.SmsTypes SMSType, string mobileNumber)
        {
            string filePath = HttpContext.Current.Server.MapPath(@"~/xmldata/CustomerCommunication.xml");
            string TempComtent = string.Empty;
            string flightcount = string.Empty;
            string passengerCount = string.Empty;
            string manipulatedString = string.Empty;
            QuoteCommunication QuoteCommunication = new QuoteCommunication();
            Dictionary<string, string> dictionaryEmailContent = new Dictionary<string, string>();
            XmlDocument document = new XmlDocument();
            document.Load(filePath);
            var quoteData = GetQuote(quoteHeaderId);
            var QuoteData = LoadQuoteDataIntoDictionary(quoteHeaderId, SMSType.ToString(), quoteData);
            XmlNode quickQuoteEMailNodes = document.SelectSingleNode("/Communications/SMSCommunication/" + SMSType);
            QuoteData.TryGetValue("FlightCount", out flightcount);
            QuoteData.TryGetValue("PassengerCount", out passengerCount);
            if (Convert.ToInt32(flightcount) > 0)
            {
                manipulatedString = manipulatesmsstring("Flights data", "Staying at", Convert.ToInt32(flightcount), quickQuoteEMailNodes.InnerText, QuoteData);
            }
            if (Convert.ToInt32(passengerCount) > 0)
            {
                manipulatedString = manipulatesmsstring("Passenger data", "Adults:", Convert.ToInt32(passengerCount), quickQuoteEMailNodes.InnerText, QuoteData);
            }
            TempComtent = CheckAndReplaceReservedWords(QuoteData, manipulatedString);
            dictionaryEmailContent.Add("sms", TempComtent);
            QuoteCommunication.quoteDictionary = dictionaryEmailContent;
            SMS SMSInfo = new SMS();
            SMSInfo.FromPhoneNumber = null;
            SMSInfo.ToPhoneNumber = mobileNumber;
            SMSInfo.Message = TempComtent;
            bool status = SendSMS(SMSInfo);
            return status;
        }
        private Quote GetQuote(string quoteHeaderId)
        {
            return new CommonHelper().LoadQuoteData(quoteHeaderId);
        }
        private Dictionary<string, string> LoadQuoteDataIntoDictionary(string quoteHeaderId, string EmailSmsType, Quote quoteData)
        {
            var Quote = quoteData;
            int passengerCount = 0;
            int flightCount = 0;
            int paymentListCount = 0;
            int baggageCount = 0;
            var AgentDetails = SFConfiguration.GetUserInfo();
            List<Passenger> Passengers = new List<Passenger>();
            CommonHelper commonHelper = new CommonHelper();
            Passengers = commonHelper.GetPassengers(quoteHeaderId);
            int bagQuantity = 0;
            string baggageDescription = string.Empty;
            decimal bagPrice = 0;
            string bagDirection = string.Empty;
            int flightarrival = 0;
            var srcJsonPath = HttpContext.Current.Server.MapPath("~/JsonData/Source.json");
            var destJsonPath = HttpContext.Current.Server.MapPath("~/JsonData/destination.json");
            QuoteCommunication quoteCommunicationClass = new QuoteCommunication();
            string imageFilePath = HttpContext.Current.Server.MapPath(SFConfiguration.GetTTSSLogo());
            quoteCommunicationClass.quoteDictionary.Add("Image", imageFilePath);
            quoteCommunicationClass.quoteDictionary.Add("Search.DestinationCode", Quote.searchModel.destinationCode);
            quoteCommunicationClass.quoteDictionary.Add("Search.sourceCode", Quote.searchModel.sourceCode);
            quoteCommunicationClass.quoteDictionary.Add("Search.sourceName", Quote.searchModel.sourceName);
            quoteCommunicationClass.quoteDictionary.Add("Search.starRating", Quote.searchModel.starRating.ToString());
            quoteCommunicationClass.quoteDictionary.Add("Search.startDate", Quote.searchModel.startDate.ToString());
            quoteCommunicationClass.quoteDictionary.Add("Search.endDate", Quote.searchModel.endDate.ToString());
            quoteCommunicationClass.quoteDictionary.Add("Agent.agentName", AgentDetails.FirstName + " " + AgentDetails.LastName);
            quoteCommunicationClass.quoteDictionary.Add("Agent.agentId", AgentDetails.UserId.ToString());
            SFLogger.logMessage("Agent.telephoneNumber" + AgentDetails.MobileNumber);
            quoteCommunicationClass.quoteDictionary.Add("Agent.telephoneNumber", AgentDetails.MobileNumber != null ? AgentDetails.MobileNumber.ToString() : "0402080953");
            quoteCommunicationClass.quoteDictionary.Add("Agent.userEmailAddress", AgentDetails.UserEmailAddress);
            quoteCommunicationClass.quoteDictionary.Add("quoteType", "QuickQuote");
            quoteCommunicationClass.quoteDictionary.Add("supportEmail", "Support@teletext.co.uk");
            quoteCommunicationClass.quoteDictionary.Add("quoteReference", Quote.quoteHeader.CustomerReferenceCode.ToUpper().ToString());
            quoteCommunicationClass.quoteDictionary.Add("quoteDate", String.Format("{0:dd/MM/yy}", DateTime.Now.Date));
            quoteCommunicationClass.quoteDictionary.Add("customerName", Quote.quoteHeader.CustomerFirstName + " " + Quote.quoteHeader.CustomerLastName);
            quoteCommunicationClass.quoteDictionary.Add("marginHotel", Quote.quoteHeader.MarginHotel.ToString());
            quoteCommunicationClass.quoteDictionary.Add("marginFlight", Quote.quoteHeader.MarginFlight.ToString());
            quoteCommunicationClass.quoteDictionary.Add("marginExtra", Quote.quoteHeader.MarginExtras.ToString());
            quoteCommunicationClass.quoteDictionary.Add("bagsPrice", "£" + Quote.quoteHeader.BaggageCost.ToString("0.00"));
            quoteCommunicationClass.quoteDictionary.Add("totalPrice", "£" + (Quote.quoteHeader.FlightCost + Quote.quoteHeader.HotelCost + Quote.quoteHeader.TransfersCost + Quote.quoteHeader.FidelityCost + Quote.quoteHeader.MarginExtras + Quote.quoteHeader.MarginFlight + Quote.quoteHeader.MarginHotel + Quote.quoteHeader.BaggageCost).ToString("0.00"));
            quoteCommunicationClass.quoteDictionary.Add("flightPrice", "£" + Quote.quoteHeader.FlightCost.ToString("0.00"));
            quoteCommunicationClass.quoteDictionary.Add("accomodationPrice", "£" + (Quote.quoteHeader.HotelCost + Quote.quoteHeader.FidelityCost + Quote.quoteHeader.MarginExtras + Quote.quoteHeader.MarginFlight + Quote.quoteHeader.MarginHotel).ToString("0.00"));
            quoteCommunicationClass.quoteDictionary.Add("transferPrice", "£" + Quote.quoteHeader.TransfersCost.ToString("0.00"));
            quoteCommunicationClass.quoteDictionary.Add("grandTotal", "£" + Quote.quoteHeader.GrandTotal.ToString("0.00"));
            quoteCommunicationClass.quoteDictionary.Add("internalNotes", Quote.quoteHeader.InternalNotes.ToString());
            quoteCommunicationClass.quoteDictionary.Add("sentToSms", Quote.quoteHeader.SentToSms.ToString());
            quoteCommunicationClass.quoteDictionary.Add("sentToEmail", Quote.quoteHeader.SentToEmail);
            quoteCommunicationClass.quoteDictionary.Add("fidelityCost", Quote.quoteHeader.FidelityCost.ToString("0.00"));
            quoteCommunicationClass.quoteDictionary.Add("AdultsCount", Quote.searchModel.AdultsCount.ToString());
            quoteCommunicationClass.quoteDictionary.Add("ChildCount", Quote.searchModel.ChildrenCount.ToString());
            quoteCommunicationClass.quoteDictionary.Add("InfantCount", Quote.searchModel.InfantsCount.ToString());
            quoteCommunicationClass.quoteDictionary.Add("HotelName", Quote.hotelComponent.HotelName);
            if (Passengers.Count() > 0)
            {
                foreach (var passenger in Passengers)
                {
                    passengerCount++;
                    quoteCommunicationClass.quoteDictionary.Add("Title" + passengerCount, passenger.Title);
                    quoteCommunicationClass.quoteDictionary.Add("FirstName" + passengerCount, passenger.FirstName);
                    quoteCommunicationClass.quoteDictionary.Add("LastName" + passengerCount, passenger.LastName);
                    quoteCommunicationClass.quoteDictionary.Add("PassportNumber" + passengerCount, passenger.PassportNumber);
                    quoteCommunicationClass.quoteDictionary.Add("IsLeadPassenger" + passengerCount, passenger.IsLeadPassenger);
                    quoteCommunicationClass.quoteDictionary.Add("Telephone" + passengerCount, passenger.Telephone);
                    quoteCommunicationClass.quoteDictionary.Add("Gender" + passengerCount, passenger.Gender);
                    if (EmailSmsType == Constants.EmailTypes.TopDogFailureEmail.ToString() || EmailSmsType == Constants.EmailTypes.BookingFullFillmentFailureEmail.ToString())
                    {
                        quoteCommunicationClass.quoteDictionary.Add("Type" + passengerCount, passenger.Type);
                        if (passenger.IsLeadPassenger == "True")
                        {
                            quoteCommunicationClass.quoteDictionary.Add("PassengerType" + passengerCount, "Lead Passenger");
                        }
                        else
                        {
                            quoteCommunicationClass.quoteDictionary.Add("PassengerType" + passengerCount, "Co Passenger");
                        }
                    }
                    else
                    {
                        quoteCommunicationClass.quoteDictionary.Add("Type" + passengerCount, passenger.Type.Substring(0, 1));
                    }
                    quoteCommunicationClass.quoteDictionary.Add("Email" + passengerCount, passenger.Email);
                    quoteCommunicationClass.quoteDictionary.Add("DateofBirth" + passengerCount, !string.IsNullOrEmpty(passenger.DateOfBirth) ? passenger.DateOfBirth : "");
                }
                if (EmailSmsType == Constants.EmailTypes.TopDogFailureEmail.ToString() || EmailSmsType == Constants.EmailTypes.BookingFullFillmentFailureEmail.ToString())
                {
                    quoteCommunicationClass.quoteDictionary.Add("Address1", string.IsNullOrEmpty(Passengers[0].Address1) ? "" : Passengers[0].Address1);
                    quoteCommunicationClass.quoteDictionary.Add("Address2", string.IsNullOrEmpty(Passengers[0].Address2) ? "" : Passengers[0].Address2);
                    quoteCommunicationClass.quoteDictionary.Add("Address3", string.IsNullOrEmpty(Passengers[0].Address3) ? "" : Passengers[0].Address3);
                    quoteCommunicationClass.quoteDictionary.Add("PostCode", string.IsNullOrEmpty(Passengers[0].PostCode) ? "" : Passengers[0].PostCode);
                    quoteCommunicationClass.quoteDictionary.Add("EmailAddress1", string.IsNullOrEmpty(Passengers[0].Email) ? "" : Passengers[0].Email);
                    quoteCommunicationClass.quoteDictionary.Add("TelePhoneNumber", string.IsNullOrEmpty(Passengers[0].Telephone) ? "" : Passengers[0].Telephone);
                    quoteCommunicationClass.quoteDictionary.Add("MobileNumber", string.IsNullOrEmpty(Passengers[0].Mobile) ? "" : Passengers[0].Mobile);
                    quoteCommunicationClass.quoteDictionary.Add("Numberofnights", Quote.searchModel.Nights.ToString());
                }
            }
            if (EmailSmsType != Constants.EmailTypes.BookingAcknowledgementEmail.ToString())
            {
                foreach (var flights in Quote.flightComponents)
                {
                    foreach (var details in flights.FlightComponentDetails)
                    {
                        flightCount++;
                        if (flights.Arrival == details.DepartureAirportCode)
                        {
                            flightarrival = flightCount;
                            quoteCommunicationClass.quoteDictionary.Add("flightarrival", flightarrival.ToString());
                        }
                        details.DepartureAirportName = SFConfiguration.GetAirportName(details.DepartureAirportCode, destJsonPath, srcJsonPath);
                        details.ArrivalAirportName = SFConfiguration.GetAirportName(details.ArrivalAirportCode, destJsonPath, srcJsonPath);
                        if (details.DepartureAirportName.Contains(','))
                            details.DepartureAirportName = details.DepartureAirportName.Split(',')[0] + " Airport";
                        if (details.ArrivalAirportName.Contains(','))
                            details.ArrivalAirportName = details.ArrivalAirportName.Split(',')[0] + " Airport";
                        if (EmailSmsType == Constants.SmsTypes.QuickQuoteSMS.ToString())
                        {
                            quoteCommunicationClass.quoteDictionary.Add("FlightRoute" + flightCount, details.DepartureAirportName);
                            quoteCommunicationClass.quoteDictionary.Add("DepDate" + flightCount, String.Format("{0:dd/MM/yy}", details.DepartureDateTime));
                            quoteCommunicationClass.quoteDictionary.Add("DepTime" + flightCount, String.Format("{0:HH:mm}", details.DepartureDateTime));
                            quoteCommunicationClass.quoteDictionary.Add("ArrivalAirPortName" + flightCount, details.ArrivalAirportName);
                            quoteCommunicationClass.quoteDictionary.Add("ArrDate" + flightCount, String.Format("{0:dd/MM/yy}", details.ArrivalDateTime));
                            quoteCommunicationClass.quoteDictionary.Add("ArrTime" + flightCount, String.Format("{0:HH:mm}", details.ArrivalDateTime));
                            quoteCommunicationClass.quoteDictionary.Add("FlightNo" + flightCount, details.FlightNumber.ToString());
                            quoteCommunicationClass.quoteDictionary.Add("marketingAirline" + flightCount, details.MarketingAirLine.ToString());
                            quoteCommunicationClass.quoteDictionary.Add("marketingAirlineCode" + flightCount, details.MarketingAirlineCode.ToString());
                        }
                        else
                        {
                            quoteCommunicationClass.quoteDictionary.Add("FlightRoute" + flightCount, details.DepartureAirportName + " > " + details.ArrivalAirportName);
                            quoteCommunicationClass.quoteDictionary.Add("DepDate" + flightCount, String.Format("{0:dd/MM/yy}", details.DepartureDateTime));
                            quoteCommunicationClass.quoteDictionary.Add("DepTime" + flightCount, String.Format("{0:HH:mm}", details.DepartureDateTime));
                            quoteCommunicationClass.quoteDictionary.Add("ArrDate" + flightCount, String.Format("{0:dd/MM/yy}", details.ArrivalDateTime));
                            quoteCommunicationClass.quoteDictionary.Add("ArrTime" + flightCount, String.Format("{0:HH:mm}", details.ArrivalDateTime));
                            quoteCommunicationClass.quoteDictionary.Add("FlightNo" + flightCount, details.FlightNumber.ToString());
                            quoteCommunicationClass.quoteDictionary.Add("marketingAirline" + flightCount, details.MarketingAirLine.ToString());
                            quoteCommunicationClass.quoteDictionary.Add("marketingAirlineCode" + flightCount, details.MarketingAirlineCode.ToString());
                        }
                        if (EmailSmsType == Constants.EmailTypes.TopDogFailureEmail.ToString() || EmailSmsType == Constants.EmailTypes.BookingFullFillmentFailureEmail.ToString())
                        {

                            var sesameBookFlights = quoteData.SesameBookBasket.BasketComponents.Where(x => x.ProductTypeCode == "FLI");
                            if (details.DirectionOptionId == SFConstants.Direction_Outbound && flights.componentType != SFConstants.FlightReturn)
                            {
                                if (!quoteCommunicationClass.quoteDictionary.ContainsKey("Direction_OutboundCount"))
                                    quoteCommunicationClass.quoteDictionary.Add("Direction_OutboundCount", flights.FlightComponentDetails.Select(x => x.DirectionOptionId == SFConstants.Direction_Outbound).Count().ToString() + "_" + flightCount.ToString());
                                if (!quoteCommunicationClass.quoteDictionary.ContainsKey("Direction_QuoteOutboundCount"))
                                    quoteCommunicationClass.quoteDictionary.Add("Direction_QuoteOutboundCount", flightCount.ToString() + "_" + flightCount.ToString());
                                PopulateFlightOutBoundInformation(quoteData, quoteCommunicationClass, sesameBookFlights, flightCount, passengerCount, flights);

                            }
                            else if (details.DirectionOptionId == SFConstants.Direction_Inbound && flights.componentType != SFConstants.FlightReturn)
                            {
                                if (!quoteCommunicationClass.quoteDictionary.ContainsKey("Direction_InboundCount"))
                                    quoteCommunicationClass.quoteDictionary.Add("Direction_InboundCount", flights.FlightComponentDetails.Select(x => x.DirectionOptionId == SFConstants.Direction_Inbound).Count().ToString() + "_" + flightCount.ToString());
                                if (!quoteCommunicationClass.quoteDictionary.ContainsKey("Direction_QuoteInboundCount"))
                                    quoteCommunicationClass.quoteDictionary.Add("Direction_QuoteInboundCount", flightCount.ToString() + "_" + flightCount.ToString());
                                PopulateFlightInBoundInformation(quoteData, quoteCommunicationClass, sesameBookFlights, flightCount, passengerCount, flights);
                            }
                            else if(flights.componentType == SFConstants.FlightReturn)
                            {
                                if (!quoteCommunicationClass.quoteDictionary.ContainsKey("Direction_OutboundCount"))
                                    quoteCommunicationClass.quoteDictionary.Add("Direction_OutboundCount", flights.FlightComponentDetails.Select(j => j.DirectionOptionId == SFConstants.Direction_Outbound).Count().ToString() + "_" + flightCount.ToString());
                                if (!quoteCommunicationClass.quoteDictionary.ContainsKey("Direction_QuoteOutboundCount"))
                                    quoteCommunicationClass.quoteDictionary.Add("Direction_QuoteOutboundCount", flightCount.ToString() + "_" + flightCount.ToString());
                                if (!quoteCommunicationClass.quoteDictionary.ContainsKey("Direction_InboundCount"))
                                    quoteCommunicationClass.quoteDictionary.Add("Direction_InboundCount", sesameBookFlights.Where(y => y.DirectionOptionId == SFConstants.Direction_Inbound).Count().ToString() + "_" + flightCount.ToString());
                                if (!quoteCommunicationClass.quoteDictionary.ContainsKey("Direction_QuoteInboundCount"))
                                    quoteCommunicationClass.quoteDictionary.Add("Direction_QuoteInboundCount", flightCount.ToString() + "_" + flightCount.ToString());
                                PopulateFlightReturnInformation(quoteData, quoteCommunicationClass, sesameBookFlights, flightCount, passengerCount, flights);
                            }

                        }

                    }

                }
                if (Quote.hotelComponent != null)
                {
                    quoteCommunicationClass.quoteDictionary.Add("AccomodationName", Quote.hotelComponent.HotelName);
                    quoteCommunicationClass.quoteDictionary.Add("ResortName", Quote.hotelComponent.SupplierResortName.ToString());
                    quoteCommunicationClass.quoteDictionary.Add("CheckInDate", String.Format("{0:dd/MM/yy}", Quote.hotelComponent.CheckInDate));
                    quoteCommunicationClass.quoteDictionary.Add("CheckOutDate", String.Format("{0:dd/MM/yy}", Quote.hotelComponent.CheckOutDate));
                    quoteCommunicationClass.quoteDictionary.Add("starRating", Quote.hotelComponent.StarRating.ToString());
                    if (EmailSmsType == Constants.EmailTypes.TopDogFailureEmail.ToString() || EmailSmsType == Constants.EmailTypes.BookingFullFillmentFailureEmail.ToString())
                    {
                        var quoteAccomPrice = (Quote.quoteHeader.HotelCost + Quote.quoteHeader.FidelityCost + Quote.quoteHeader.MarginExtras + Quote.quoteHeader.MarginFlight + Quote.quoteHeader.MarginHotel).ToString("0.00");
                        quoteCommunicationClass.quoteDictionary.Add("HotelAddress", Quote.hotelComponent.HotelInformatoin.Address.Address1 + " " + Quote.hotelComponent.HotelInformatoin.Address.Address2 + " " + Quote.hotelComponent.HotelInformatoin.Address.Address3);
                        var sesameBookAccomodation = quoteData.SesameBookBasket.BasketComponents.Where(x => x.ProductTypeCode == "ACO");
                        if (sesameBookAccomodation != null && sesameBookAccomodation.Count() > 0)
                        {
                            var sesameAccoGrossPrice = sesameBookAccomodation.FirstOrDefault();
                            if (sesameAccoGrossPrice != null)
                            {
                                var hotelCompanyRefNumber = sesameBookAccomodation.Select(g => g.SupplierBookingId).FirstOrDefault();
                                var sesameAccoGrossCost = (sesameAccoGrossPrice.ComponentDetails.Select(y => Convert.ToDecimal(y.SellingPricePence)).FirstOrDefault() / 100).ToString();
                                var sesameAccoGrossValue = (Convert.ToDecimal(sesameAccoGrossCost) + Quote.quoteHeader.FidelityCost + Quote.quoteHeader.MarginExtras + Quote.quoteHeader.MarginFlight + Quote.quoteHeader.MarginHotel).ToString();

                                if (sesameAccoGrossValue != null)
                                {
                                    quoteCommunicationClass.quoteDictionary.Add("AccomodationGrossPrice", !string.IsNullOrEmpty(sesameAccoGrossValue) ? "£" + sesameAccoGrossValue : "Prices Not Available");
                                    quoteCommunicationClass.quoteDictionary.Add("AccomodationNetPrice", !string.IsNullOrEmpty(sesameAccoGrossValue) ? "£" + sesameAccoGrossValue : "Prices Not Available");
                                }
                                else
                                {
                                    quoteCommunicationClass.quoteDictionary.Add("AccomodationGrossPrice", "£" + quoteAccomPrice);
                                }
                                if (hotelCompanyRefNumber != null && hotelCompanyRefNumber != string.Empty)
                                {
                                    quoteCommunicationClass.quoteDictionary.Add("HotelCompanyRefNumber", hotelCompanyRefNumber);
                                }
                                else
                                {
                                    quoteCommunicationClass.quoteDictionary.Add("HotelCompanyRefNumber", "HotelCompany Failed");
                                    quoteCommunicationClass.quoteDictionary.Add("HotelCompanyFailed", "style=\"color: red;\"");
                                }

                            }
                        }
                        else
                        {
                            quoteCommunicationClass.quoteDictionary.Add("HotelCompanyRefNumber", "HotelCompany Failed");
                            quoteCommunicationClass.quoteDictionary.Add("AccomodationGrossPrice", quoteAccomPrice);
                            quoteCommunicationClass.quoteDictionary.Add("AccomodationNetPrice", quoteAccomPrice);
                        }

                    }
                    foreach (var details in Quote.hotelComponent.hotelComponentsDetails)
                    {
                        quoteCommunicationClass.quoteDictionary.Add("RoomType", details.RoomDescription);
                        quoteCommunicationClass.quoteDictionary.Add("Board Basis", details.RatePlanName);
                        quoteCommunicationClass.quoteDictionary.Add("amountAfterTax", "£" + details.AmountAfterTax.ToString());
                        break;
                    }
                }

                if (Quote.transferComponents.Count > 0)
                {
                    string pickupPlace = SFConfiguration.GetAirportName(Quote.flightComponents[0].Arrival, destJsonPath, srcJsonPath);
                    var totalTransferPrice = Quote.quoteHeader.TransfersCost.ToString();
                    if (pickupPlace.Contains(','))
                        pickupPlace = pickupPlace.Split(',')[0];
                    quoteCommunicationClass.quoteDictionary.Add("Pickupdate_AirportToHotel", String.Format("{0:dd/MM/yy HH:mm:ss}", Quote.flightComponentUI.flightComponentsOutbound.Last().ArrivalDateTime.AddMinutes(Quote.ArrivalPickUp)));
                    quoteCommunicationClass.quoteDictionary.Add("Pickupdate_HotelToAirport", String.Format("{0:dd/MM/yy HH:mm:ss}", Quote.flightComponentUI.flightComponentsInbound[0].DepartureDateTime.AddMinutes(-Quote.DeparturePickUp)));
                    quoteCommunicationClass.quoteDictionary.Add("PickUpPlace_AirportToHotel", pickupPlace);
                    quoteCommunicationClass.quoteDictionary.Add("DropOff_AtHotel", Quote.hotelComponent.HotelName);
                    quoteCommunicationClass.quoteDictionary.Add("DropOff_AtAirport", pickupPlace);
                    quoteCommunicationClass.quoteDictionary.Add("PickUpPlace_HotelToFlight", Quote.hotelComponent.HotelName);
                    quoteCommunicationClass.quoteDictionary.Add("TransferApproxTransferTime_FromAirportToHotel", Quote.transferComponents[0].TransferTime.ToString());
                    quoteCommunicationClass.quoteDictionary.Add("TransferType_FromAirportToHotel", Quote.transferComponents[0].ProductType.ToString());
                    quoteCommunicationClass.quoteDictionary.Add("TransferApproxTransferTime_FromHotelToAirport", Quote.transferComponents[1].TransferTime.ToString());
                    quoteCommunicationClass.quoteDictionary.Add("TransferTypeFromHotelToAirport", Quote.transferComponents[1].ProductType.ToString());
                    if (EmailSmsType == Constants.EmailTypes.TopDogFailureEmail.ToString() || EmailSmsType == Constants.EmailTypes.BookingFullFillmentFailureEmail.ToString())
                    {
                        if (Quote.BookedHolidayTaxiData.transferbookdata != null)
                        {
                            quoteCommunicationClass.quoteDictionary.Add("HolidayTaxiReferenceNo", !string.IsNullOrEmpty(Quote.BookedHolidayTaxiData.general.bookingReference) ? Quote.BookedHolidayTaxiData.general.bookingReference : "HolidayTaxi Failed");
                            quoteCommunicationClass.quoteDictionary.Add("HolidayTaxiFailed", !string.IsNullOrEmpty(Quote.BookedHolidayTaxiData.general.bookingReference) ? "" : "style=\"color: red;\"");
                            var transfersGrossPrice = Quote.BookedHolidayTaxiData.pricing.totalPrice.ToString();
                            var transfersNetPrice = Quote.BookedHolidayTaxiData.pricing.netPrice.ToString();
                            if (transfersGrossPrice != null || transfersGrossPrice != string.Empty && transfersNetPrice != null || transfersNetPrice != string.Empty)
                            {
                                quoteCommunicationClass.quoteDictionary.Add("HolidayTaxiGrossPrice", "£" + transfersGrossPrice.ToString());
                                quoteCommunicationClass.quoteDictionary.Add("HolidayTaxiNetPrice", "£" + transfersNetPrice.ToString());
                            }
                            else
                            {
                                quoteCommunicationClass.quoteDictionary.Add("HolidayTaxiGrossPrice", "£" + totalTransferPrice);
                                quoteCommunicationClass.quoteDictionary.Add("HolidayTaxiNetPrice", "£" + totalTransferPrice);
                            }
                        }
                        else
                        {
                            quoteCommunicationClass.quoteDictionary.Add("HolidayTaxiReferenceNo", "HolidayTaxi Failed");
                            quoteCommunicationClass.quoteDictionary.Add("HolidayTaxiFailed", "style=\"color: red;\"");
                            quoteCommunicationClass.quoteDictionary.Add("HolidayTaxiGrossPrice", "£" + totalTransferPrice);
                            quoteCommunicationClass.quoteDictionary.Add("HolidayTaxiNetPrice", "£" + totalTransferPrice);
                        }
                    }
                }
                if (Quote.bagsComponents != null && Quote.bagsComponents.Count > 0)
                {
                    quoteCommunicationClass.quoteDictionary.Add("bagsvalidationShow", "");
                    if (EmailSmsType == Constants.EmailTypes.TopDogFailureEmail.ToString() || EmailSmsType == Constants.EmailTypes.BookingFullFillmentFailureEmail.ToString())
                    {
                        quoteCommunicationClass.quoteDictionary.Add("bagsvalidation", string.Empty);
                        foreach (var bags in Quote.bagsComponents)
                        {
                            baggageCount++;
                            bagDirection = bags.Direction;
                            if (bags.Direction == "Flight Return")
                            {
                                quoteCommunicationClass.quoteDictionary.Add("baggageDescription" + baggageCount, bags.BagsDescription.ToString());
                                quoteCommunicationClass.quoteDictionary.Add("baggagePrice" + baggageCount, "£" + bags.SellingPrice.ToString());
                                quoteCommunicationClass.quoteDictionary.Add("baggageCount" + baggageCount, bags.Items.ToString());
                                quoteCommunicationClass.quoteDictionary.Add("baggageDirection" + baggageCount, "RoundTrip");
                            }
                            else
                            {
                                quoteCommunicationClass.quoteDictionary.Add("baggageDescription" + baggageCount, bags.BagsDescription);
                                quoteCommunicationClass.quoteDictionary.Add("baggagePrice" + baggageCount, "£" + bags.SellingPrice.ToString());
                                quoteCommunicationClass.quoteDictionary.Remove("baggageDirection" + baggageCount);
                                quoteCommunicationClass.quoteDictionary.Add("baggageCount" + baggageCount, bags.Items.ToString());
                                quoteCommunicationClass.quoteDictionary.Add("baggageDirection" + baggageCount, bags.Direction);

                            }

                        }
                        if (Quote.bagsComponents[0].Direction != "Flight Return")
                        {
                            var outboundprice = Quote.bagsComponents.Where(x => x.DirectionOptionId == SFConstants.Direction_Outbound).Sum(x => x.SellingPrice);
                            var inboundprice = Quote.bagsComponents.Where(x => x.DirectionOptionId == SFConstants.Direction_Inbound).Sum(x => x.SellingPrice);
                            if (outboundprice != 0 && Quote.bagsComponents[0].DirectionOptionId == SFConstants.Direction_Outbound)
                            {
                                quoteCommunicationClass.quoteDictionary.Add("baggageTotalPrice1", "£" + outboundprice.ToString());
                            }
                            else
                            {
                                quoteCommunicationClass.quoteDictionary.Add("baggageTotalPrice2", "£" + outboundprice.ToString());
                            }
                            if (inboundprice != 0 && Quote.bagsComponents[0].DirectionOptionId == SFConstants.Direction_Inbound)
                            {
                                quoteCommunicationClass.quoteDictionary.Add("baggageTotalPrice1", "£" + inboundprice.ToString());
                            }
                            else
                            {
                                quoteCommunicationClass.quoteDictionary.Add("baggageTotalPrice2", "£" + inboundprice.ToString());
                            }
                        }
                        else
                        {
                            var RoundTripPrice = Quote.bagsComponents.Where(x => x.DirectionOptionId == SFConstants.QuoteComponent_FlightReturn).Sum(x => x.SellingPrice);
                            quoteCommunicationClass.quoteDictionary.Add("baggageTotalPrice1", "£" + RoundTripPrice.ToString());
                            quoteCommunicationClass.quoteDictionary.Add("baggageTotalPrice2", "");
                        }

                    }
                    else
                    {
                        foreach (var bags in Quote.bagsComponents)
                        {
                            baggageDescription += bags.BagsDescription + ",";
                            bagQuantity += !string.IsNullOrEmpty(bags.Items) ? Convert.ToInt32(bags.Items) : 0;
                            bagPrice += bags.SellingPrice;
                        }
                        baggageDescription = baggageDescription.Remove(baggageDescription.LastIndexOf(','), 1);
                        quoteCommunicationClass.quoteDictionary.Add("bagQuantity", bagQuantity.ToString());
                        quoteCommunicationClass.quoteDictionary.Add("baggageDescription", baggageDescription.ToString());
                    }
                }
                else
                {
                    quoteCommunicationClass.quoteDictionary.Add("bagsvalidation", "Baggage is not added to this Quote");
                    quoteCommunicationClass.quoteDictionary.Add("bagsvalidationShow", "none");
                }
                if (EmailSmsType == Constants.EmailTypes.TopDogFailureEmail.ToString() || EmailSmsType == Constants.EmailTypes.BookingFullFillmentFailureEmail.ToString())
                {
                    var paymentList = commonHelper.GetCompletePaymentDetails(Convert.ToInt32(quoteHeaderId));
                    if (paymentList.Count() > 0)
                    {
                        quoteCommunicationClass.quoteDictionary.Add("PaymentType1", paymentList[0].depositType);
                        
                        decimal receivedAmount = 0;
                        decimal totoalDueAmount = 0;
                        decimal totalCardCharges = 0;
                        foreach (var paymentlist in paymentList)
                        {
                            totoalDueAmount = paymentList.Take(paymentListCount).Sum(x => x.cost);
                            paymentListCount++;
                            receivedAmount += paymentlist.cost;
                            totalCardCharges += paymentlist.cardCharges;
                            var balanceDueAmount = Quote.quoteHeader.GrandTotal - (receivedAmount - totalCardCharges);
                            var cardMemberAddress = string.Format("{0},<br/>{1},<br/>{2},<br/>{3},<br/>{4}", paymentlist.houseNumber, paymentlist.address, paymentlist.city, paymentlist.country, paymentlist.postCode);
                            quoteCommunicationClass.quoteDictionary.Add("CardCharges" + paymentListCount, "£" + paymentlist.cardCharges.ToString());
                            quoteCommunicationClass.quoteDictionary.Add("PaymentAuthorisation" + paymentListCount, paymentlist.txnResult);
                            quoteCommunicationClass.quoteDictionary.Add("PaymentAuthorisationCode" + paymentListCount, paymentlist.authCode);
                            quoteCommunicationClass.quoteDictionary.Add("Amount" + paymentListCount, "£" + (paymentlist.cost + paymentlist.cardCharges + balanceDueAmount).ToString("0.00"));
                            quoteCommunicationClass.quoteDictionary.Add("Paymentreceived" + paymentListCount, "£" + paymentlist.cost.ToString());
                            quoteCommunicationClass.quoteDictionary.Add("BalanceDue" + paymentListCount, "£" + balanceDueAmount.ToString());
                            quoteCommunicationClass.quoteDictionary.Add("CardType" + paymentListCount, paymentlist.cardType.ToString());
                            quoteCommunicationClass.quoteDictionary.Add("CardDetails" + paymentListCount, MaskCreditCardNumber(paymentlist.pan != null ? paymentlist.pan : quoteData.CCNumber));
                            quoteCommunicationClass.quoteDictionary.Add("CardExpiry" + paymentListCount, paymentlist.cardExpiry.ToString());
                            quoteCommunicationClass.quoteDictionary.Add("NameOnCard" + paymentListCount, paymentlist.firstName.ToString() + paymentlist.lastName.ToString());
                            quoteCommunicationClass.quoteDictionary.Add("AddressDetails" + paymentListCount, cardMemberAddress);

                        }
                        quoteCommunicationClass.quoteDictionary.Add("PaymentCharges1", "£" + (Quote.quoteHeader.GrandTotal + totalCardCharges).ToString());
                    }
                    var extraCharges = commonHelper.GetExtraCharges();
                    if (extraCharges.Count() > 0)
                    {
                        quoteCommunicationClass.quoteDictionary.Add("Profit", "£" + (Quote.quoteHeader.MarginHotel + Quote.quoteHeader.MarginFlight + Quote.quoteHeader.MarginExtras).ToString());
                        quoteCommunicationClass.quoteDictionary.Add("FidelityCharges", "£" + Quote.quoteHeader.FidelityCost.ToString());
                        quoteCommunicationClass.quoteDictionary.Add("BookedBy", AgentDetails.FirstName + " " + AgentDetails.LastName);
                    }
                }
                quoteCommunicationClass.quoteDictionary.Add("FlightCount", flightCount.ToString());
                quoteCommunicationClass.quoteDictionary.Add("airport", "Airport");
                quoteCommunicationClass.quoteDictionary.Add("PassengerCount", passengerCount.ToString());
                quoteCommunicationClass.quoteDictionary.Add("BaggageCount", baggageCount.ToString());
                quoteCommunicationClass.quoteDictionary.Add("PaymentCount", paymentListCount.ToString());
            }
            return quoteCommunicationClass.quoteDictionary;
        }
        private string MaskCreditCardNumber(string CreditCardNumber)
        {
            var cardNumber = CreditCardNumber;
            var firstDigits = cardNumber.Substring(0, 6);
            var lastDigits = cardNumber.Substring(cardNumber.Length - 4, 4);
            var requiredMask = new String('X', cardNumber.Length - firstDigits.Length - lastDigits.Length);
            var maskedString = string.Concat(firstDigits, requiredMask, lastDigits);
            return maskedString;
        }

        private Dictionary<string, string> LoadDataIntoDictionary(string[] pairOfStrings)
        {
            QuoteCommunication quoteCommunicationClass = new QuoteCommunication();
            foreach (string s in pairOfStrings)
            {
                if (s.Contains("::"))
                {
                    string Key = s.Substring(0, s.IndexOf("::"));
                    string value = s.Substring(s.IndexOf("::") + 2);
                    quoteCommunicationClass.quoteDictionary.Add(Key, value);
                }

            }
            return quoteCommunicationClass.quoteDictionary;
        }

        private void PopulateFlightOutBoundInformation(Quote quoteData, QuoteCommunication quoteCommunicationClass, IEnumerable<Businessobjects.Sesame.SesameComponent> sesameBookFlights, int flightCount, int passengerCount, Businessobjects.Flights.FlightComponent flights)
        {

            if (flights.FlightComponentDetails.First().MarketingAirlineCode == SFConstants.EasyJetMarketingAirlineCode)
            {

                if (flights.FlightComponentDetails.Select(x => x.DirectionOptionId).FirstOrDefault().Equals(SFConstants.Direction_Outbound) && quoteData.EasyJetBookings.Count > 0
                    && quoteData.EasyJetBookings.Where(ob => ob.ComponentTypeOptionId.Equals(SFConstants.Direction_Outbound)).Count() > 0)
                {
                    var easyJetOutbound = quoteData.EasyJetBookings.FirstOrDefault(x => x.ComponentTypeOptionId.Equals(SFConstants.Direction_Outbound));                   
                    var OBFlightSupplierBookingId = easyJetOutbound.supplierRef;
                    var OBFlightPrice = (easyJetOutbound.adultPricePP * easyJetOutbound.adultsCount) + (easyJetOutbound.childPricePP * easyJetOutbound.childrenCount) + (easyJetOutbound.infantPricePP * easyJetOutbound.infantsCount);
                    quoteCommunicationClass.quoteDictionary.Add("SupplierReferenceNumber" + flightCount, !string.IsNullOrEmpty(OBFlightSupplierBookingId) ? OBFlightSupplierBookingId : "Booking Failed For Flights");
                    quoteCommunicationClass.quoteDictionary.Add("FlightsFailed" + flightCount, !string.IsNullOrEmpty(OBFlightSupplierBookingId) ? "" : "style=\"color: red;\"");
                    quoteCommunicationClass.quoteDictionary.Add("QuotePrice" + flightCount, flights.Amount.ToString());
                    if (Convert.ToDecimal(OBFlightPrice) != 0)
                    {
                        quoteCommunicationClass.quoteDictionary.Add("SupplierCost" + flightCount, "£" + OBFlightPrice.ToString());
                    }
                    else
                    {
                        quoteCommunicationClass.quoteDictionary.Add("SupplierCost" + flightCount, flights.Amount.ToString());
                    }

                }
                else if (flights.FlightComponentDetails.Select(x => x.DirectionOptionId).FirstOrDefault().Equals(SFConstants.Direction_Outbound))
                {                    
                    quoteCommunicationClass.quoteDictionary.Add("SupplierReferenceNumber" + flightCount, string.Empty);
                    quoteCommunicationClass.quoteDictionary.Add("FlightsFailed" + flightCount, "style=\"color: red;\"");
                    quoteCommunicationClass.quoteDictionary.Add("QuotePrice" + flightCount, flights.Amount.ToString());
                    quoteCommunicationClass.quoteDictionary.Add("SupplierCost" + flightCount, flights.Amount.ToString());
                }

            }
            else
            {                
                var OBFlightSupplierBookingId = sesameBookFlights.Where(z => z.DirectionOptionId == SFConstants.Direction_Outbound).Select(x => x.SupplierBookingId).FirstOrDefault();
                var OBFlightPrice = quoteData.SesameBookBasket.BasketComponents.Where(a => a.DirectionOptionId == SFConstants.Direction_Outbound).Select(x => x.ComponentDetails.Take(passengerCount).Sum(o => Convert.ToDecimal(o.SellingPricePence) / 100)).FirstOrDefault().ToString();
                quoteCommunicationClass.quoteDictionary.Add("SupplierReferenceNumber" + flightCount, !string.IsNullOrEmpty(OBFlightSupplierBookingId) ? OBFlightSupplierBookingId : "Booking Failed For Flights");
                quoteCommunicationClass.quoteDictionary.Add("FlightsFailed" + flightCount, !string.IsNullOrEmpty(OBFlightSupplierBookingId) ? "" : "style=\"color: red;\"");
                quoteCommunicationClass.quoteDictionary.Add("QuotePrice" + flightCount, flights.Amount.ToString());
                if (Convert.ToDecimal(OBFlightPrice) != 0)
                {
                    quoteCommunicationClass.quoteDictionary.Add("SupplierCost" + flightCount, "£" + OBFlightPrice.ToString());
                }
                else
                {
                    quoteCommunicationClass.quoteDictionary.Add("SupplierCost" + flightCount, flights.Amount.ToString());
                }
            }
        }

        private void PopulateFlightInBoundInformation(Quote quoteData, QuoteCommunication quoteCommunicationClass, IEnumerable<Businessobjects.Sesame.SesameComponent> sesameBookFlights, int flightCount, int passengerCount, Businessobjects.Flights.FlightComponent flights)
        {
            if (flights.FlightComponentDetails.First().MarketingAirlineCode == SFConstants.EasyJetMarketingAirlineCode)
            {
                if (flights.FlightComponentDetails.Select(x => x.DirectionOptionId).FirstOrDefault().Equals(SFConstants.Direction_Inbound) && quoteData.EasyJetBookings.Count > 0
                   && quoteData.EasyJetBookings.Where(ob => ob.ComponentTypeOptionId.Equals(SFConstants.Direction_Inbound)).Count() > 0)
                {
                    var easyJetInbound = quoteData.EasyJetBookings.FirstOrDefault(x => x.ComponentTypeOptionId.Equals(SFConstants.Direction_Inbound));
                    
                    var IBFlightSupplierBookingId = easyJetInbound.supplierRef;
                    var IBFlightPrice = (easyJetInbound.adultPricePP * easyJetInbound.adultsCount) + (easyJetInbound.childPricePP * easyJetInbound.childrenCount) + (easyJetInbound.infantPricePP * easyJetInbound.infantsCount);
                    quoteCommunicationClass.quoteDictionary.Add("SupplierReferenceNumber" + flightCount, !string.IsNullOrEmpty(IBFlightSupplierBookingId) ? IBFlightSupplierBookingId : "Booking Failed For Flights");
                    quoteCommunicationClass.quoteDictionary.Add("FlightsFailed" + flightCount, !string.IsNullOrEmpty(IBFlightSupplierBookingId) ? "" : "style=\"color: red;\"");
                    quoteCommunicationClass.quoteDictionary.Add("QuotePrice" + flightCount, flights.Amount.ToString());
                    if (Convert.ToDecimal(IBFlightPrice) != 0)
                    {
                        quoteCommunicationClass.quoteDictionary.Add("SupplierCost" + flightCount, "£" + IBFlightPrice.ToString());
                    }
                    else
                    {
                        quoteCommunicationClass.quoteDictionary.Add("SupplierCost" + flightCount, flights.Amount.ToString());
                    }

                }
                else if (flights.FlightComponentDetails.Select(x => x.DirectionOptionId).FirstOrDefault().Equals(SFConstants.Direction_Inbound))
                {                    
                    quoteCommunicationClass.quoteDictionary.Add("SupplierReferenceNumber" + flightCount, string.Empty);
                    quoteCommunicationClass.quoteDictionary.Add("FlightsFailed" + flightCount, "style=\"color: red;\"");
                    quoteCommunicationClass.quoteDictionary.Add("QuotePrice" + flightCount, flights.Amount.ToString());
                    quoteCommunicationClass.quoteDictionary.Add("SupplierCost" + flightCount, flights.Amount.ToString());

                }

            }
            else
            {                
                var InBFlightSupplierBookingId = sesameBookFlights.Where(z => z.DirectionOptionId == SFConstants.Direction_Inbound).Select(x => x.SupplierBookingId).FirstOrDefault();
                var InBFlightPrice = quoteData.SesameBookBasket.BasketComponents.Where(a => a.DirectionOptionId == SFConstants.Direction_Inbound).Select(x => x.ComponentDetails.Take(passengerCount).Sum(u => Convert.ToDecimal(u.SellingPricePence) / 100)).FirstOrDefault().ToString();
                quoteCommunicationClass.quoteDictionary.Add("SupplierReferenceNumber" + flightCount, !string.IsNullOrEmpty(InBFlightSupplierBookingId) ? InBFlightSupplierBookingId : "Booking Failed For Flights");
                quoteCommunicationClass.quoteDictionary.Add("FlightsFailed" + flightCount, !string.IsNullOrEmpty(InBFlightSupplierBookingId) ? "" : "style=\"color: red;\"");
                quoteCommunicationClass.quoteDictionary.Add("QuotePrice" + flightCount, flights.Amount.ToString());
                if (Convert.ToDecimal(InBFlightPrice) != 0)
                {
                    quoteCommunicationClass.quoteDictionary.Add("SupplierCost" + flightCount, "£" + InBFlightPrice.ToString());

                }
                else
                {
                    quoteCommunicationClass.quoteDictionary.Add("SupplierCost" + flightCount, flights.Amount.ToString());
                }
            }
        }

        private void PopulateFlightReturnInformation(Quote quoteData, QuoteCommunication quoteCommunicationClass, IEnumerable<Businessobjects.Sesame.SesameComponent> sesameBookFlights, int flightCount, int passengerCount, Businessobjects.Flights.FlightComponent flights)
        {
            if (flights.FlightComponentDetails.First().MarketingAirlineCode == SFConstants.EasyJetMarketingAirlineCode)
            {
                if (flights.FlightComponentDetails.Select(x => x.DirectionOptionId).FirstOrDefault().Equals(SFConstants.QuoteComponent_FlightReturn) && quoteData.EasyJetBookings.Count > 0
                  && quoteData.EasyJetBookings.Where(ob => ob.ComponentTypeOptionId.Equals(SFConstants.QuoteComponent_FlightReturn)).Count() > 0)
                {
                    var easyJetReturn = quoteData.EasyJetBookings.FirstOrDefault(x => x.ComponentTypeOptionId.Equals(SFConstants.QuoteComponent_FlightReturn));                    
                    var FlightReturnSupplierBookingId = easyJetReturn.supplierRef;
                    var FlightReturnPrice = (easyJetReturn.adultPricePP * easyJetReturn.adultsCount) + (easyJetReturn.childPricePP * easyJetReturn.childrenCount) + (easyJetReturn.infantPricePP * easyJetReturn.infantsCount);
                    quoteCommunicationClass.quoteDictionary.Add("SupplierReferenceNumber" + flightCount, !string.IsNullOrEmpty(FlightReturnSupplierBookingId) ? FlightReturnSupplierBookingId : "Booking Failed For Flights");
                    quoteCommunicationClass.quoteDictionary.Add("FlightsFailed" + flightCount, !string.IsNullOrEmpty(FlightReturnSupplierBookingId) ? "" : "style=\"color: red;\"");
                    quoteCommunicationClass.quoteDictionary.Add("QuotePrice" + flightCount, flights.Amount.ToString());
                    if (Convert.ToDecimal(FlightReturnPrice) != 0)
                    {
                        quoteCommunicationClass.quoteDictionary.Add("SupplierCost" + flightCount, "£" + FlightReturnPrice.ToString());

                    }
                    else
                    {
                        quoteCommunicationClass.quoteDictionary.Add("SupplierCost" + flightCount, flights.Amount.ToString());
                    }
                }
                else
                {                    
                    quoteCommunicationClass.quoteDictionary.Add("SupplierReferenceNumber" + flightCount, string.Empty);
                    quoteCommunicationClass.quoteDictionary.Add("FlightsFailed" + flightCount, "style=\"color: red;\"");
                    quoteCommunicationClass.quoteDictionary.Add("QuotePrice" + flightCount, flights.Amount.ToString());
                    quoteCommunicationClass.quoteDictionary.Add("SupplierCost" + flightCount, flights.Amount.ToString());
                }
            }
            else
            {                
                var FlightReturnSupplierBookingId = sesameBookFlights.Where(z => z.DirectionOptionId == SFConstants.QuoteComponent_FlightReturn).Select(x => x.SupplierBookingId).FirstOrDefault();
                var ReturnFlightPrice = quoteData.SesameBookBasket.BasketComponents.Where(a => a.DirectionOptionId == SFConstants.QuoteComponent_FlightReturn).SelectMany(v => v.ComponentDetails.Take(passengerCount)).Sum(o => Convert.ToDecimal(o.SellingPricePence) / 100);
                quoteCommunicationClass.quoteDictionary.Add("SupplierReferenceNumber" + flightCount, !string.IsNullOrEmpty(FlightReturnSupplierBookingId) ? FlightReturnSupplierBookingId : "Booking Failed For Flights");
                quoteCommunicationClass.quoteDictionary.Add("FlightsFailed" + flightCount, !string.IsNullOrEmpty(FlightReturnSupplierBookingId) ? "" : "style=\"color: red;\"");
                quoteCommunicationClass.quoteDictionary.Add("QuotePrice" + flightCount, flights.Amount.ToString());
                if (Convert.ToDecimal(ReturnFlightPrice) != 0)
                {
                    quoteCommunicationClass.quoteDictionary.Add("SupplierCost" + flightCount, "£" + ReturnFlightPrice.ToString());
                }
                else
                {
                    quoteCommunicationClass.quoteDictionary.Add("SupplierCost" + flightCount, flights.Amount.ToString());
                }
            }
        }
    }
}