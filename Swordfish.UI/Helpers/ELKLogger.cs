﻿using System;
using System.Web;
using System.Diagnostics;
using System.Threading.Tasks;
using SwordFish.Models.Account;
using Newtonsoft.Json;
using System.Text;

namespace SwordFish.Helpers
{
    public static class ELKLogger
    {
        public static Task WriteELKLog(string LogType, string LogMessage)
        {
            try
            {
                ErrorLogModel error = new ErrorLogModel();             
                error.Message = LogMessage;
                Task.Run(async () =>
                {
                    await WriteELKLog(LogType, error);
                }).ConfigureAwait(false);

            }
            catch (Exception ex)
            {
                EventLog(ex);
            }

            return Task.Delay(1);
        }

        public static Task WriteELKLog(string LogType, ErrorLogModel errorLog)
        {
            return WriteELKLog(LogType, errorLog, null);
        }

        public static Task WriteELKLog(string LogType, ErrorLogModel errorLog,Exception excep)
        {
            try
            {
                errorLog.SessionId = GetSession();
                errorLog.Environment =GetEnvironment();

                Task.Run(async () =>
                {
                    await ELKLog.Logger.Log(LogType, JsonConvert.SerializeObject(errorLog).ToString(), excep);
                }).ConfigureAwait(false);

            }
            catch (Exception ex)
            {
                EventLog(ex);
            }

            return Task.Delay(1);
        }

        public static string GetSession()
        {
            string strSession = string.Empty;
            if(HttpContext.Current !=null )
            { 
                strSession= HttpContext.Current.Session.SessionID;
            }
            return strSession;
        }

        private static string GetEnvironment()
        {
            return SFConfiguration.GetEnvironment();
        }

        public static void EventLog(Exception ex)
        {
            StringBuilder stringBuilder = new StringBuilder();
            stringBuilder.Append(ex.Message);
            if (ex.InnerException != null)
            {
                stringBuilder.Append(ex.InnerException.Message);
            }
            stringBuilder.Append(ex.StackTrace);
            if (!System.Diagnostics.EventLog.SourceExists("SwordFish"))
                System.Diagnostics.EventLog.CreateEventSource("SwordFish", "SwordFish");
            System.Diagnostics.EventLog eventLog = new System.Diagnostics.EventLog("SwordFish");
            eventLog.Source = "SwordFish";
            if (stringBuilder != null && stringBuilder.ToString() == "")
            {
                eventLog.WriteEntry(stringBuilder.ToString());
            }


        }

    }
}