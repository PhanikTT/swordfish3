﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Configuration;
using System.IO;
using Newtonsoft.Json;
using System.Web.Script.Serialization;
using System.Collections.Concurrent;
using System.Xml.Linq;
using System.Data;
using Membership = Swordfish.Businessobjects.Membership;
using Swordfish.Businessobjects.Passengers;
using Swordfish.Businessobjects.Hotels;
using Swordfish.Businessobjects.Quotes;
using Swordfish.Businessobjects.Basket;
using Swordfish.Utilities.Cache;
using Swordfish.Businessobjects.Flights;
using Swordfish.Businessobjects.Payments;
using Swordfish.Businessobjects.Bookings;
using Swordfish.Businessobjects.Customers;
using Swordfish.UI.Models.Hotel;
using Swordfish.UI.Models.Account;
using Swordfish.UI.Models.Booking;
using System.Xml;

namespace Swordfish.UI.Helpers
{
    public class SFConfiguration
    {
        static XDocument customerCommXDoc;
        static List<DestinationInfo> destinationInfoList;
        static List<SourceInfo> sourceInfoList;
        static XmlDocument resotLinesXDoc = null;
        static XmlDocument hotelLinesXDoc = null;
        static XDocument hotelCodesxmlDoc = null;
        static XmlDocument agentNoteXml = null;
        static SFConfiguration()
        {
            string filepath = HttpContext.Current.Server.MapPath(@"~/xmldata/CustomerCommunication.xml");
            customerCommXDoc = XDocument.Load(filepath);
            if (resotLinesXDoc == null && File.Exists(HttpContext.Current.Server.MapPath(@"~/XmlData/ResortTree.xml")))
            {
                resotLinesXDoc = new XmlDocument();
                resotLinesXDoc.Load(HttpContext.Current.Server.MapPath(@"~/XmlData/ResortTree.xml"));
            }
            if (hotelLinesXDoc == null && File.Exists(HttpContext.Current.Server.MapPath(@"~/XmlData/HotelList.xml")))
            {
                hotelLinesXDoc = new XmlDocument();
                hotelLinesXDoc.Load(System.Web.HttpContext.Current.Server.MapPath(@"~/XmlData/HotelList.xml"));
            }
            if (hotelCodesxmlDoc == null)
            {
                string docPath = HttpContext.Current.Server.MapPath(GetValueforKey("HotelCodesListPath"));
                if (File.Exists(docPath))
                {
                    hotelCodesxmlDoc = XDocument.Load(docPath);
                    // TBD Load the XML file
                }
            }
        }


        #region public 
        #region accessing data from xml file
        internal static string GetPassWordRecoverySubjectLine()
        {
            return GetValuefromXML("Subject", "PassWordRecoveryEMail");
        }
        internal static string GetBodyforEmailToCustomer()
        {
            return GetValuefromXML("Body", "SendPaxDetailsToCustomer");
        }
        internal static string GetBodyforCustomerDetailsEmail()
        {
            return GetValuefromXML("Body", "SendCustomersDetailsEmail");
        }
        internal static string GetSubjectforCustomerDetailsEmail()
        {
            return GetValuefromXML("Subject", "SendCustomersDetailsEmail");
        }
        internal static string GetSubjectforEmailToCustomer()
        {
            return GetValuefromXML("Subject", "SendPaxDetailsToCustomer");
        }
        internal static string GetPassWordFromEmail()
        {
            return GetValuefromXML("FromEmailAddress", "PassWordRecoveryEMail");
        }
        internal static string GetPassWordToEmail()
        {
            return GetValuefromXML("ToEmailAddress", "PassWordRecoveryEMail");
        }
        internal static string GetPassWordRecoveryBody()
        {
            return GetValuefromXML("Body", "PassWordRecoveryEMail");
        }
        internal static string GetMailHeading(string EmailType)
        {
            return GetValuefromXML("Body", EmailType);
        }
        internal static string GetAgentdetails(string EmailType)
        {
            return GetValuefromXML("Agentdetails", EmailType);
        }
        internal static string GetQuotereference(string EmailType)
        {
            return GetValuefromXML("Quotereference", EmailType);
        }
        internal static string GetEmail(string EmailType)
        {
            return GetValuefromXML("Email", EmailType);
        }
        internal static string GetImportantNotes(string EmailType)
        {
            return GetValuefromXML("ImportantNotes", EmailType);
        }
        internal static Passenger GetPassengerDetails(string EmailType)
        {
            Passenger passenger = new Passenger();
            GetValuefromXML("PassengerDetails", EmailType);
            return passenger;
        }
        internal static string GetTypeName(string EmailType)
        {
            return GetValuefromXML("QuoteType", EmailType);
        }
        internal static string getBodyForAgentEmail()
        {
            return GetValuefromXML("Body", "SendMailToAgent");
        }
        internal static string getSubjectForAgentEmail()
        {
            return GetValuefromXML("Subject", "SendMailToAgent");
        }

        #endregion
        #region Accessing Keys from config file
        internal static string GetTravelSaasFetchResort()
        {
            return GetValueforKey("TravelSaas.FetchResort");
        }
        internal static string GetTravelSaasMultiRoom()
        {
            return GetValueforKey("TravelSaas.FetchMultiRoom");
        }
        internal static string GetTravelSaasAsyncResort()
        {
            return GetValueforKey("TravelSaas.GetAsyncResort");
        }
        internal static string GetTravelSaasAsyncMultiRoom()
        {
            return GetValueforKey("TravelSaas.FetchAsyncMultiRoom");
        }
        internal static string GetTravelSaasSearchStatus()
        {
            return GetValueforKey("TravelSaas.FetchSearchStatus");
        }
        internal static string GetTravelSaasDeltaResults()
        {
            return GetValueforKey("TravelSaas.FetchDeltaResults");
        }
        internal static string GetTravelSaasHotelInfo()
        {
            return GetValueforKey("TravelSaas.FetchHotelInfo");
        }
        internal static string GetTravelSaasTransfers()
        {
            return GetValueforKey("TravelSaas.FetchTransfers");
        }
        public static string getSMTPServer()
        {
            return GetValueforKey("SMTPServer.Url");
        }
        internal static string getRegisteredMobileForSMS()
        {
            return GetValueforKey("SmsRegisteredMobileNumber");
        }
        public static string getPort()
        {
            return GetValueforKey("SMTPServer.Port");
        }
        public static string getSenderEmail()
        {
            return GetValueforKey("SMTPServer.SenderEmail");
        }
        public static string GetEmailDisplayName()
        {
            return GetValueforKey("SMTPServer.EmailDisplayName");
        }

        public static string GetHolidayTaxiDefaultEmailId()
        {
            return GetValueforKey("HolidayTaxiDefaultEmailId");
        }

        public static string getSsl()
        {
            return GetValueforKey("SMTPServer.Ssl");
        }
        public static string getSenderPassword()
        {
            return GetValueforKey("SMTPServer.SenderPassword");
        }
        public static string GetTravelSAASWebApiUrl()
        {
            return GetValueforKey("TravelSAASWebApiUrl");
        }
        public static bool GetAsynchronousFlag()
        {
            return GetValueforKey("IsAsynchronous") != null ? Convert.ToBoolean(GetValueforKey("IsAsynchronous")) : false;
        }
        public static int GetAjaxCallTime()
        {
            return GetValueforKey("AjaxCallTime") != null ? Convert.ToInt32(GetValueforKey("AjaxCallTime")) : 20000;
        }
        public static string getHotelcodePath()
        {
            string fileName = "HotelCodes.xml";
            return GetValueforKey("HotelcodePath") + fileName;


        }

        public static string GetCustomerURL()
        {
            return GetValueforKey("CustUrl");
        }
        public static string GetCustomerSMSURL()
        {
            return GetValueforKey("CustUrlSMS");
        }
        public static int GetMaxRooms()
        {
            return Convert.ToInt32(GetValueforKey("MaxRooms"));
        }
        public static int GetMaxAultsInRoom()
        {
            return Convert.ToInt32(GetValueforKey("MaxAultsInRoom"));
        }
        public static int GetMaxPaxInRoom()
        {
            return Convert.ToInt32(GetValueforKey("MaxPaxInRoom"));
        }
        public static int GetMaxChildInRoom()
        {
            return Convert.ToInt32(GetValueforKey("MaxChildInRoom"));
        }
        public static int GetMaxInfantInRoom()
        {
            return Convert.ToInt32(GetValueforKey("MaxInfantInRoom"));
        }
        public static int GetMaxPaxPerBooking()
        {
            return Convert.ToInt32(GetValueforKey("MaxPaxPerBooking"));
        }
        public static string GetSingleTTSSQuote()
        {
            return GetValueforKey("TTSS.Url");
        }
        public static string GetCOMPANY()
        {
            return GetValueforKey("COMPANY");
        }
        public static string GetEnvironment()
        {
            return GetValueforKey("Environment");
        }

        public static string GetMultipleTTSSQuote()
        {
            return GetValueforKey("MultipleTTSSQuote");
        }
        public static string GetSupportEmail()
        {
            return GetValueforKey("SupportEmail");
        }
        public static string GetSupportPassword()
        {
            return GetValueforKey("SupportPassword");
        }
        public static string Get10()
        {
            return GetValueforKey("10");
        }
        public static string GetMaxResults()
        {
            return GetValueforKey("MaxResults");
        }
        public static string GetNearByAirports()
        {
            return GetValueforKey("FetchNearbyAirports");
        }
        public static string GetEjMarketingAirlineCode()
        {
            return GetValueforKey("Ej.MarketingAirlineCode");
        }

        public static string GetchilliDataBase()
        {
            return GetConnectionStringValueforKey("chilliDataBase");
        }
        public static string GetWEXCardHolderName()
        {
            return GetValueforKey("WEX.CardHolderName");
        }
        public static string GetWEXrequestReference()
        {
            return GetValueforKey("WEX.requestReference");
        }
        public static string GetWEXusername()
        {
            return GetValueforKey("WEX.username");
        }
        public static string GetWEXGBPaccount()
        {
            return GetValueforKey("WEX.GBP.account");
        }
        public static string GetWEXEUROaccount()
        {
            return GetValueforKey("WEX.EURO.account");
        }
        public static string GetWEXexpiry()
        {
            return GetValueforKey("WEX.expiry");
        }
        public static string GetWEXinstant()
        {
            return GetValueforKey("WEX.instant");
        }
        public static string GetWEXactivated()
        {
            return GetValueforKey("WEX.activated");
        }
        public static string GetWEXdescription()
        {
            return GetValueforKey("WEX.description");
        }
        public static string GetWEXtitle()
        {
            return GetValueforKey("WEX.title");
        }
        public static string GetWEXfirstName()
        {
            return GetValueforKey("WEX.firstName");
        }
        public static string GetWEXGender()
        {
            return GetValueforKey("WEX.gender");
        }
        public static string GetWEXDateOfBirth()
        {
            return GetValueforKey("WEX.dateOfBirth");
        }
        public static string GetWEXEmail()
        {
            return GetValueforKey("WEX.email");
        }
        public static string GetWEXStreet()
        {
            return GetValueforKey("WEX.street");
        }
        public static string GetWEXPostcode()
        {
            return GetValueforKey("WEX.postcode");
        }
        public static string GetWEXCity()
        {
            return GetValueforKey("WEX.city");
        }
        public static string GetTTSSFooter()
        {
            return GetValueforKey("TTSSFooter");
        }
        public static string GetTTSSLogo()
        {
            return GetValueforKey("TTSSLogo");
        }
        public static string GetEJAdultAge()
        {
            return GetValueforKey("EJ.AdultAge");
        }
        public static string GetEJChildAge()
        {
            return GetValueforKey("EJ.ChildAge");
        }
        public static string GetEJPriceType()
        {
            return GetValueforKey("EJ.priceType");
        }
        public static string GetEJDemoMode()
        {
            return GetValueforKey("EJ.demoMode");
        }
        public static string GetEJCurrency()
        {
            return GetValueforKey("EJ.currency");
        }
        public static string GetEJOperatingAirline()
        {
            return GetValueforKey("EJ.operatingAirline");
        }
        public static string GetEJEmail()
        {
            return GetValueforKey("EJ.email");
        }
        public static string GetEJPassword()
        {
            return GetValueforKey("EJ.password");
        }
        public static string GetEJPaxFlightAdult()
        {
            return GetValueforKey("EJ.pax.flight.adult");
        }
        public static string GetEJPaxFlightChild()
        {
            return GetValueforKey("EJ.pax.flight.child");
        }
        public static string GetEJPaxFlightInfant()
        {
            return GetValueforKey("EJ.pax.flight.infant");
        }
        public static string GetGlobalSMSUrl()
        {
            return GetValueforKey("GlobalSMS.Url");
        }
        public static string GetFlightFilterMorningTime()
        {
            return GetValueforKey("Morning");
        }
        public static string GetFlightFilterAfternoonTime()
        {
            return GetValueforKey("Afternoon");
        }
        public static string GetFlightFilterEveningTime()
        {
            return GetValueforKey("Evening");
        }
        public static string GetFlightFilterLateNightTime()
        {
            return GetValueforKey("LateNight");
        }
        public static string GetNumberOfQuotesToDisplay()
        {
            return GetValueforKey("NumberOfQuotesToDisplay");
        }

        internal static string GetTravelSaasReturnFlights()
        {
            return GetValueforKey("TravelSaas.FetchReturnFlights");
        }
        internal static string GetTravelSaasOneWayFlights()
        {
            return GetValueforKey("TravelSaas.FetchOneWayFlights");
        }
        internal static string GetTravelSaasFlightCreateBasketDetails()
        {
            return GetValueforKey("TravelSaas.FetchFlightCreateBasketDetails");

        }
        internal static string GetTravelSaasCreateBasket()
        {
            return GetValueforKey("TravelSaas.CreateBasket");
        }
        internal static string GetTravelSaasRefreshBasket()
        {
            return GetValueforKey("TravelSaas.RefreshBasket");
        }
        internal static string GetTravelSaasBookBasket()
        {
            return GetValueforKey("TravelSaas.BookBasket");
        }
        internal static string GetTravelSaasBookTransfers()
        {
            return GetValueforKey("TravelSaas.BookTransfers");
        }
        //public static string GetTransfersApiUrlGet()
        //{
        //    return GetValueforKey("TransfersApiUrlGet");
        //}
        //public static string GetTransfersAPI_KEY()
        //{
        //    return GetValueforKey("TransfersAPI_KEY");

        //}
        //public static string GetTransfersAffiliateCode()
        //{
        //    return GetValueforKey("TransfersAffiliateCode");
        //}
        //public static string GetTransfersXMLPassword()
        //{
        //    return GetValueforKey("TransfersXMLPassword");
        //}
        //public static string GetTransfersAGENT_REF()
        //{
        //    return GetValueforKey("TransfersAGENT_REF");
        //}


        public static string GetGlobalSMSAccountSid()
        {
            return GetValueforKey("GlobalSMS.AccountSid");
        }
        public static string GetGlobalSMSAuthToken()
        {
            return GetValueforKey("GlobalSMS.AuthToken");
        }
        public static string GetWEXCountry()
        {
            return GetValueforKey("WEX.country");
        }
        public static string GetWEXHouseNumber()
        {
            return GetValueforKey("WEX.houseNumber");
        }
        public static string GetWEXCardType()
        {
            return GetValueforKey("WEX.CardType");
        }
        public static string GetWEXHouseName()
        {
            return GetValueforKey("WEX.houseName");
        }
        public static string GetFlightOperatorCode()
        {
            return GetValueforKey("FlightOperatorCode");
        }
        public static string GetHotelOperatorCode()
        {
            return GetValueforKey("HotelOperatorCode");
        }
        public static string GetTransferOperatorCode()
        {
            return GetValueforKey("TransferOperatorCode");
        }
        public static string GetFlightServiceClass()
        {
            return GetValueforKey("FlightServiceClass");
        }
        public static string GetFlightOperator()
        {
            return GetValueforKey("FlightOperator");
        }
        public static string GetCurrency()
        {
            return GetValueforKey("Currency");
        }
        public static string GetTopDogUserId()
        {
            return GetValueforKey("TopDogUserId");
        }
        public static string GetTopDogPassword()
        {
            return GetValueforKey("TopDogPassword");
        }
        public static string GetTopDogLanguage()
        {
            return GetValueforKey("TopDogLanguage");
        }
        public static string GetCCNumber()
        {
            return GetValueforKey("CCNumber");
        }
        public static string GetCCType()
        {
            return GetValueforKey("CCType");
        }
        public static string GetCCStartDate()
        {
            return GetValueforKey("CCStartDate");
        }
        public static string GetCCEndDate()
        {
            return GetValueforKey("CCEndDate");
        }
        public static string GetSecurityNum()
        {
            return GetValueforKey("SecurityNum");
        }
        public static int GetArrivalPickUp()
        {
            return Convert.ToInt32(GetValueforKey("ArrivalPickUp"));
        }
        public static int GetDeparturePickUp()
        {
            return Convert.ToInt32(GetValueforKey("DeparturePickUp"));
        }
        public static string GetAddressType()
        {
            return GetValueforKey("AddressType");
        }
        public static string GetPhoneType()
        {
            return GetValueforKey("PhoneType");
        }
        public static string GetEmailType()
        {
            return GetValueforKey("EmailType");
        }

        public static string GetTopDogDefaultAdultAge()
        {
            return String.Format("{0:dd/MM/yyyy}", DateTime.Today.AddYears(-Convert.ToInt32(GetValueforKey("TopDogDefaultAdultAge"))));
        }
        public static string GetTopDogDefaultChildAge()
        {
            return String.Format("{0:dd/MM/yyyy}", DateTime.Today.AddYears(-Convert.ToInt32(GetValueforKey("TopDogDefaultChildAge"))));
        }
        public static string GetTopDogDefaultInfantAge()
        {
            return String.Format("{0:dd/MM/yyyy}", DateTime.Today.AddYears(-Convert.ToInt32(GetValueforKey("TopDogDefaultInfantAge"))));
        }
        public static decimal GetTopDogHotelCommision()
        {
            return Convert.ToDecimal(GetValueforKey("TopDogHotelCommision"));
        }
        public static decimal GetTopDogTransferCommision()
        {
            return Convert.ToDecimal(GetValueforKey("TopDogTransferCommision"));
        }

        public static string GetTopDogPassengerNationality()
        {
            return GetValueforKey("TopDogPassengerNationality");
        }
        public static int GetDepositDays()
        {
            return Convert.ToInt32(GetValueforKey("DepositDays"));
        }

        public static string BookingPageExpireMsg()
        {
            return GetValueforKey("PageExpireMsg");
        }
        public static string GetTopDogToEmailList()
        {
            return GetValueforKey("TopDogFailureToEmail");
        }
        public static string GetCardEasyEndPoint()
        {
            return GetValueforKey("CardEasyEndPoint");
        }
        public static HotelMappingCodes GetHotelCodesForIFFCode(string code)
        {
            HotelMappingCodes hotelCodes = new HotelMappingCodes();

            var hotels = (from hotel in hotelCodesxmlDoc.Descendants("hotelMapping")
                          select new HotelMappingCodes()
                          {
                              Masterhotelid = hotel.Element("Masterhotelid").Value,
                              IFFCode = hotel.Element("IFFCode").Value,
                              TTICode = hotel.Element("TTICode").Value,
                              GIATACode = hotel.Element("GIATACode").Value
                          }).ToList();


            hotelCodes = hotels.Where(x => x.IFFCode == code).FirstOrDefault();
            return hotelCodes;
        }
        #endregion
        #region Accesing Cookies


        public static int GetUserID()
        {
            string KeyValue = GetRedisSessionData("UserID");
            if (KeyValue != null && KeyValue != "")
                return GetTypeFromCache<int>(KeyValue);
            else return 0;
        }



        public static void SetUserID(int UserID, string sessionID = null)
        {

            SetRedisSessionData("UserID", UserID.ToString(), sessionID);
        }
        public static string GetUserName()
        {
            var KeyValue = GetRedisSessionData("UserName");
            if (KeyValue != null && KeyValue != "")
                return GetTypeFromCache<string>(KeyValue);
            else return null;
        }
        public static void SetUserName(string UserName, string sessionID = null)
        {
            SetRedisSessionData("UserName", UserName, sessionID);
        }

        public static void SetUserInfo(Membership.UserInfo userInfo,string sessionID = null)
        {
            SetRedisSessionData("UserInfo", userInfo, sessionID);
        }
        public static Membership.UserInfo GetUserInfo()
        {
            var KeyValue = GetRedisSessionData("UserInfo");
            if (KeyValue != null && KeyValue != "")
                return GetTypeFromCache<Membership.UserInfo>(KeyValue);
            else return null;
        }

        public static string GetUserEmail()
        {
            var KeyValue = GetRedisSessionData("UserEmail");
            if (KeyValue != null && KeyValue != "")
                return GetTypeFromCache<string>(KeyValue);
            else return null;
        }
        public static void SetUserEmail(string UserEmail,string sessionID=null)
        {
            SetRedisSessionData("UserEmail", UserEmail,sessionID);
        }
        public static SearchModel GetDashBSearch()
        {
            var KeyValue = GetRedisSessionData("DashBSearch");
            if (KeyValue != null && KeyValue != "")
                return GetTypeFromCache<SearchModel>(KeyValue);
            else return null;
        }
        public static void SetDashBSearch(SearchModel DashBSearch)
        {
            SetRedisSessionData("DashBSearch", DashBSearch);
        }
        public static string GetTTSSSearch()
        {
            var KeyValue = GetRedisSessionData("TTSSSearch");
            if (KeyValue != null && KeyValue != "")
                return GetTypeFromCache<string>(KeyValue);
            else return null;
        }

        public static void SetTTSSSearch(bool TTSSSearch)
        {
            SetRedisSessionData("TTSSSearch", TTSSSearch.ToString());
        }
        public static string GetIncomingCutomerName()
        {
            var KeyValue = GetRedisSessionData("IncomingCutomerName");
            if (KeyValue != null && KeyValue != "")
                return GetTypeFromCache<string>(KeyValue);
            else return null;
        }

        public static void SetIncomingCutomerName(string IncomingCutomerName)
        {
            SetRedisSessionData("IncomingCutomerName", IncomingCutomerName);
        }
        public static string GetQuoteCount()
        {
            var KeyValue = GetRedisSessionData("QuoteCount");
            if (KeyValue != null && KeyValue != "")
                return GetTypeFromCache<string>(KeyValue);
            else return null;
        }
        public static void SetQuoteCount(string QuoteCount)
        {
            SetRedisSessionData("QuoteCount", QuoteCount.ToString());
        }
        public static string GetHotelNameFilters()
        {
            var KeyValue = GetRedisSessionData("hotelNameFilters");
            if (KeyValue != null && KeyValue != "")
                return GetTypeFromCache<string>(KeyValue);
            else return null;
        }
        public static void SethotelNameFilters(string hotelNameFilters)
        {
            SetRedisSessionData("hotelNameFilters", hotelNameFilters);
        }
        public static ConcurrentBag<DestinationHotels> GetHotelLists()
        {
            string KeyValue = GetRedisSessionData("HotelList");
            if (KeyValue != null && KeyValue != "")
                return new ConcurrentBag<DestinationHotels>(GetTypeFromCache<List<DestinationHotels>>(KeyValue));
            else return null;
        }
        public static void SetHotelList(ConcurrentBag<DestinationHotels> HotelList)
        {
            SetRedisSessionData("HotelList", HotelList);
        }

        public static string GetTTSSQSearch()
        {
            var KeyValue = GetRedisSessionData("TTSSQSearch");
            if (KeyValue != null && KeyValue != "")
                return GetTypeFromCache<string>(KeyValue);
            else return null;
        }
        public static void SetTTSSQSearch(string TTSSQSearch)
        {
            SetRedisSessionData("TTSSQSearch", TTSSQSearch);
        }
        public static void SetQuoteCreateRQ(QuoteRef QuoteCreateRQ)
        {
            SetRedisSessionData("QuoteCreateRQ", QuoteCreateRQ);
        }
        public static QuoteRef GetQuoteCreateRQ()
        {
            var KeyValue = GetRedisSessionData("QuoteCreateRQ");
            if (KeyValue != null && KeyValue != "")
                return GetTypeFromCache<QuoteRef>(KeyValue);
            else return null;
        }
        public static IncomingCustomer GetCustomerDetails()
        {
            var KeyValue = GetRedisSessionData("CustomerDetails");
            if (KeyValue != null && KeyValue != "")
                return GetTypeFromCache<IncomingCustomer>(KeyValue);
            else return null;
        }
        public static void SetCustomerDetails(IncomingCustomer CustomerDetails)
        {
            SetRedisSessionData("CustomerDetails", CustomerDetails);
        }
        public static Basket GetChilliBasket()
        {
            var KeyValue = GetRedisSessionData("ChilliBasket");
            if (KeyValue != null && KeyValue != "")
                return GetTypeFromCache<Basket>(KeyValue);
            else return null;
        }
        public static void SetChilliBasket(Basket ChilliBasket)
        {
            SetRedisSessionData("ChilliBasket", ChilliBasket);
        }
        public static QuoteDetails GetCustomerQuoteData()
        {
            var KeyValue = GetRedisSessionData("CustomerQuoteData");
            if (KeyValue != null && KeyValue != "")
                return GetTypeFromCache<QuoteDetails>(KeyValue);
            else return null;
        }
        public static void SetCustomerQuoteData(QuoteDetails CustomerQuoteData)
        {
            SetRedisSessionData("CustomerQuoteData", CustomerQuoteData);
        }
        public static void SetHotelData(ConcurrentBag<Hotel> HotelData)
        {
            SetRedisSessionData("HotelData", HotelData);

        }
        public static ConcurrentBag<Hotel> GetHotelData(string Key = "")
        {
            string localKey = Key == string.Empty ? "HotelData" : Key;
            var KeyValue = GetRedisSessionData(localKey);
            if (KeyValue != null && KeyValue != "")
                return new ConcurrentBag<Hotel>(GetTypeFromCache<List<Hotel>>(KeyValue));
            else return null;
        }
        public static void RemoveCacheFromRedis()
        {
            string[] SessionList = { "UserID", "UserName", "UserEmail", "DashBSearch", "TTSSSearch", "IncomingCutomerName", "QuoteCount", "hotelNameFilters", "HotelList", "TTSSQSearch", "CustomerDetails", "ChilliBasket", "CustomerQuoteData", "HotelData", "PassengerModel", "PackageData_HotelData", "PackageData_FlightData", "FlightData" };
            string sessionId = SFLogger.GetSessionID();
            foreach (string sessionvar in SessionList)
            {
                string key = sessionvar + "_" + sessionId;
                if (CacheRepository.CheckKeyExists(key))
                    CacheRepository.RemoveKey(key);
            }
        }
        public static PassengersModel GetPassengerModel()
        {
            var KeyValue = GetRedisSessionData("PassengerModel");
            if (KeyValue != null && KeyValue != "")
                return GetTypeFromCache<PassengersModel>(KeyValue);
            else return null;
        }
        public static void SetPassengerModel(PassengersModel PassengerModel)
        {
            SetRedisSessionData("PassengerModel", PassengerModel);
        }
        public static void SetFlightData(ConcurrentBag<Flight> FlightData)
        {

            SetRedisSessionData("FlightData", FlightData);
        }
        public static ConcurrentBag<Flight> GetFlightData(string key = "")
        {
            string localKey = key == string.Empty ? "HotelData" : key;
            var KeyValue = GetRedisSessionData(localKey);
            if (KeyValue != null && KeyValue != "")
                return new ConcurrentBag<Flight>(GetTypeFromCache<List<Flight>>(KeyValue));
            else return null;
        }

        public static string SerializeObject(object data)
        {
            JavaScriptSerializer serializer = new JavaScriptSerializer();
            return serializer.Serialize(data);
        }
        #endregion
        #region others
        public static ResortHotelRatings GetResortAndHotelRatings(string TTIcode, string HotelCode, string SupplierResortId)
        {
            ResortHotelRatings resortHotelRatings = new ResortHotelRatings();
            XmlNode hotelNode = null;
            string tempHotelCode = string.Empty;
            if (!string.IsNullOrEmpty(SupplierResortId))
            {
                var resortNodes = resotLinesXDoc.SelectNodes("/SSM_ResortTreeRS/ResortTree/Resort/Resort/Resort[@SupplierResortId =\"" + SupplierResortId + "\"]");
                if (resortNodes != null && resortNodes.Count > 0)
                {
                    var XmlElements = resortNodes.Cast<XmlElement>();
                    resortHotelRatings.ResortName = XmlElements.FirstOrDefault(x => x.Attributes["SupplierResortId"].Value.Equals(SupplierResortId)).Attributes["Name"].Value;
                }
            }
            if (!string.IsNullOrEmpty(HotelCode))
            {
                 tempHotelCode = string.Empty;
                string[] arHotelCode = HotelCode.Split(';');
                if (arHotelCode.Length == 3)
                    tempHotelCode = arHotelCode[2];
                else
                    tempHotelCode = HotelCode;
            }
            if ((!string.IsNullOrEmpty(TTIcode) && TTIcode != "0") || (!string.IsNullOrEmpty(tempHotelCode) && tempHotelCode!="0"))
            {
               
                if(tempHotelCode !="0" && !string.IsNullOrEmpty(TTIcode))
                 hotelNode = hotelLinesXDoc.SelectSingleNode("/SSM_HotelListRS/Hotel[@SupplierAccommId ='" + tempHotelCode + "' or @TTICode='" + TTIcode + "']"); ;
                if (hotelNode != null)
                {
                    resortHotelRatings.Rating = Convert.ToInt32((string.IsNullOrEmpty(hotelNode.Attributes["Rating"].Value) ? "0" : hotelNode.Attributes["Rating"].Value));
                    resortHotelRatings.TripAdvisorScore = Convert.ToDouble((string.IsNullOrEmpty(hotelNode.Attributes["TripAdvisorScore"].Value) ? "0" : hotelNode.Attributes["TripAdvisorScore"].Value));
                }
            }
            resortHotelRatings.TTICode = TTIcode;
            resortHotelRatings.SupplierResortId = SupplierResortId;
            return resortHotelRatings;
        }

        public static HotelCodes getHotelCodes(string HodelCode)
        {
            HotelCodes hotelCodes = new HotelCodes();
            var path = getHotelcodePath();
            XDocument doc = XDocument.Load(path);
            var Hotels = (from hotel in doc.Descendants("hotelMapping")
                          select new
                          {
                              HotelCode = hotel.Element("Masterhotelid").Value,
                              IFFCode = hotel.Element("IFFCode").Value,
                              TTICode = hotel.Element("TTICode").Value,
                              GIATACode = hotel.Element("GIATACode").Value



                          });
            HotelCodes userDetails = (from hotel in Hotels
                                      where hotel.HotelCode == HodelCode
                                      select new HotelCodes
                                      {
                                          HotelCode = hotel.HotelCode,
                                          IFFCode = hotel.IFFCode,
                                          TTICode = hotel.TTICode,
                                          GIATACode = hotel.GIATACode
                                      }).FirstOrDefault();


            return hotelCodes;
        }

        public static bool DistinctHotelResults()
        {
            bool retValue = false;
            if (GetValueforKey("DistinctHotelResults").ToLower() == "yes")
            {
                retValue = true;
            }
            return retValue;
        }

        public static bool CheapestFlightResults()
        {
            bool retValue = false;
            if (GetValueforKey("CheapestFlightResults").ToLower() == "yes")
            {
                retValue = true;
            }
            return retValue;
        }

        public static List<CardTypes> GetCardTypes()
        {
            List<CardTypes> cardTypes = new List<CardTypes>();

            var path = HttpContext.Current.Server.MapPath("~/jsondata/cardtype.json");

            using (StreamReader r = new StreamReader(path))
            {
                string json = r.ReadToEnd();
                cardTypes = JsonConvert.DeserializeObject<List<CardTypes>>(json);
            }
            return cardTypes;
        }

        public static CardTypes ChangeCardTypePrice(string Id, string HotelFare)
        {
            CardTypes cardType = new CardTypes();
            List<CardTypes> cardTypes = new List<CardTypes>();
            cardTypes = SFConfiguration.GetCardTypes();
            cardType = (from s in cardTypes where s.PaymentTypeID == Convert.ToInt32(Id) select s).FirstOrDefault();
            if (cardType.PaymentRule == "PCT")
            {
                cardType.CardChargesDesc = String.Format(cardType.CardChargesDesc, cardType.Price);
                cardType.Description = (Convert.ToDecimal(cardType.Price / 100) * Convert.ToDecimal(HotelFare)).ToString("00.00").TrimStart('0');
                cardType.Price = (float)((Convert.ToDecimal(cardType.Price / 100) * Convert.ToDecimal(HotelFare)) + Convert.ToDecimal(HotelFare));
            }
            else if (cardType.PaymentRule == "FRV")
            {
                cardType.CardChargesDesc = String.Format(cardType.CardChargesDesc, cardType.Price);
                cardType.Description = Convert.ToDecimal(cardType.Price).ToString("00.00").TrimStart('0');
                cardType.Price = (float)(Convert.ToDecimal((cardType.Price / 100) * 100) + Convert.ToDecimal(HotelFare));
            }
            return cardType;
        }

        public static string GetDOBForTopDogPost()
        {
            return DateTime.Today.AddYears(-25).ToString("dd-MM-yyyy");
        }

        public static PaymentDetails GetCardDetails()
        {
            PaymentDetails payment = new PaymentDetails();
            payment.NameOnCard = "teletext";
            payment.CardNumber = "4111111111111111";
            payment.CardExpiry = "0420";
            payment.CardValidFrom = "0103";
            payment.CardType = "VIS";
            payment.IsPaymentSuccess = "1";
            payment.Status = "Success";
            payment.TransactionReference = "EFH2341";
            payment.PaymentDate = DateTime.Now;
            payment.CardIssueNumber = "123";
            payment.PaymentCountry = "EU";
            return payment;
        }

        public static Agent GetAgentDetails()
        {
            Agent agent = new Agent();

            agent.FirstName = "Thamma";
            agent.LastName = "Reddy";
            agent.Initial = "Agent";
            agent.Title = "Mr";
            agent.AddressType = "WORKADD";
            agent.Address1 = "Ameerpet";
            agent.Address2 = "Ameerpet";
            agent.PostCode = "500061";
            agent.Email = "thammag@clickwt.com";
            agent.EmailType = "EMAIL";
            agent.Phone = "1111111111";
            agent.PhoneType = "PHONE";

            return agent;
        }

        public static CustomerRequest GetCustomerRequest()
        {
            CustomerRequest cust = new CustomerRequest();
            cust.Address1 = "CWT Digital";
            cust.Address2 = "New England House";
            cust.Address3 = "Brighton";
            cust.Country = "GB";
            cust.PostCode = "BN1 4GH";
            return cust;
        }

        public static string GetAirportName(string code, string destJsonPath, string srcJsonPath)
        {
            string airportName = string.Empty;
            object lockDestObject = new object();
            if (destinationInfoList == null)
            {
                using (StreamReader r = new StreamReader(destJsonPath))
                {
                    string json = r.ReadToEnd();
                    lock (lockDestObject)
                    {
                        if (destinationInfoList == null)
                        {
                            destinationInfoList = new List<DestinationInfo>();
                            destinationInfoList = JsonConvert.DeserializeObject<List<DestinationInfo>>(json);
                        }
                    }
                }
            }
            if (destinationInfoList.FirstOrDefault(dest => dest.d != null && dest.d.Equals(code)) != null)
                airportName = destinationInfoList.FirstOrDefault(dest => dest.d.Equals(code)).dest;

            if (!string.IsNullOrEmpty(airportName))
                return airportName;
            else
            {
                object lockSrcObject = new object();
                if (sourceInfoList == null)
                {
                    using (StreamReader r = new StreamReader(srcJsonPath))
                    {
                        string json = r.ReadToEnd();
                        lock (lockSrcObject)
                        {
                            if (sourceInfoList == null)
                            {
                                sourceInfoList = new List<SourceInfo>();
                                sourceInfoList = JsonConvert.DeserializeObject<List<SourceInfo>>(json);
                            }
                        }
                    }
                }
                if (sourceInfoList.FirstOrDefault(s => s.code != null && s.code.Equals(code)) != null)
                    airportName = sourceInfoList.FirstOrDefault(s => s.code.Equals(code)).desc;

                if (!string.IsNullOrEmpty(airportName))
                    return airportName;
                else
                    return code;
            }
        }
        public static string GetFidelityCharges()
        {
            return GetValueforKey("FidelityCharges");
        }

        public static bool IsNewSearch()
        {
            bool retVal;
            bool.TryParse(GetValueforKey("IsNewSearch"), out retVal);
            return retVal;
        }
        #endregion
        #endregion
        #region private
        /// <summary>
        /// 
        /// </summary>
        /// <param name="Key"></param>
        /// <returns></returns>
        /// 
        #region accessing data from xml file
        private static string GetValuefromXML(string element, string EmailType)
        {

            var Hotel = (from xml in customerCommXDoc.Descendants(EmailType)
                         select new
                         {
                             xml.Element(element).Value
                         });
            string result = Hotel.ElementAt(0).Value.Replace("\n", "").ToString();
            return result;
        }
        public static string GetValuefromXML(string element, string EmailType, string InnerElement)
        {
            var Hotel = (from xml in customerCommXDoc.Descendants(EmailType).Elements(element)
                         select new
                         {
                             xml.Element(InnerElement).Value
                         });
            string result = Hotel.ElementAt(0).Value.Replace("\n", "").ToString();
            return result;
        }
        #endregion
        private static string GetValueforKey(string Key)
        {
            return ConfigurationManager.AppSettings[Key].ToString();
        }
        private static string GetConnectionStringValueforKey(string Key)
        {
            return ConfigurationManager.ConnectionStrings[Key].ToString();
        }
        private static T GetTypeFromCache<T>(string keyvalue)
        {
            var serialize = new JavaScriptSerializer();
            return serialize.Deserialize<T>(keyvalue);

        }
        private static string GetRedisSessionData(string key)
        {
            string CacheInfo = null;
            string sessionid = SFLogger.GetSessionID();
            key = key + "_" + sessionid;
            bool s = CacheRepository.CheckKeyExists(key);
            if (CacheRepository.CheckKeyExists(key) == true)
            {
                CacheInfo = CacheRepository.GetData(key);
            }
            return CacheInfo;
        }

        private static void SetRedisSessionData(string Key, dynamic data,string nweSessionID = null)
        {
            if(string.IsNullOrEmpty(nweSessionID))
                nweSessionID = SFLogger.GetSessionID();
            Key = Key + "_" + nweSessionID;
            if (CacheRepository.CheckKeyExists(Key) == true)
            {
                CacheRepository.RemoveKey(Key);
            }

            var serializer = new JavaScriptSerializer();
            var serializedResult = serializer.Serialize(data);

            CacheRepository.SetDataForever(Key, serializedResult.ToString());

        }
        public static int GetCobrowseTime()
        {
            var value = GetValueforKey("CobrowseTime") != null ? Convert.ToInt32(GetValueforKey("CobrowseTime")) : 15;
            return value;
        }

        public static string GetPaymentType()
        {
            var value = GetValueforKey("PaymentType");
            return value;
        }

        public static string GetTitle()
        {
            var value = GetValueforKey("Title");
            return value;
        }

        public static string GetIataCode()
        {
            var value = GetValueforKey("IATACode");
            return value;
        }

        public static string GetGIataCode()
        {
            var value = GetValueforKey("GIATACode");
            return value;
        }

        public static string GetHotelPhoneNumber()
        {
            var value = GetValueforKey("HotelPhoneNumber");
            return value;
        }

        public static string GetAddressLine()
        {
            var value = GetValueforKey("ADDRESS");
            return value;
        }

        public static string GetTerminal()
        {
            var value = GetValueforKey("TERMINAL");
            return value;
        }

        public static string GetopenPostCodeUrl()
        {
            var value = GetValueforKey("PostCode.Url");
            return value;
        }
        public static string GetopenPostCodeKey()
        {
            var value = GetValueforKey("PostCode.Key");
            return value;
        }
        internal static string GetWEXLastName()
        {
            return GetValueforKey("WEX.lastName");
        }

        public static string GetAgentNotes()
        {
            string result = string.Empty;
            if (agentNoteXml == null && File.Exists(HttpContext.Current.Server.MapPath(@"~/XmlData/AgentNotes.xml")))
            {
                agentNoteXml = new XmlDocument();
                agentNoteXml.Load(HttpContext.Current.Server.MapPath(@"~/XmlData/AgentNotes.xml"));
            }
            var noteElements = agentNoteXml.DocumentElement.SelectNodes("/AgentNotes/AgentNote");
            foreach (XmlNode item in noteElements)
            {
                result += item.SelectSingleNode("Key").InnerText;
                result += item.SelectSingleNode("Value").InnerText + "\n";
            }
            return result;
        }
        public static string GetSpecialKeyValue()
        {
            return GetValueforKey("SpecialKey");
        }
        #endregion
    }

}