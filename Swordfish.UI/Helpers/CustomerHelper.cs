﻿using Swordfish.Businessobjects.Customers;
using System.Collections.Generic;
using System.Threading.Tasks;
using Swordfish.Components.BookingServices;

namespace Swordfish.UI.Helpers
{
    public class CustomerHelper
    {
        private IBookingService bookingService;
        public CustomerHelper(IBookingService packagingRepository)
        {
            bookingService = packagingRepository;
        }
        public  async Task<string> SaveCustomer(Customer customer)
        {
            return SaveCustomerInfo(bookingService, customer).Result;
        }

        private async Task<string> SaveCustomerInfo(IBookingService packagingRepository, Customer customer)
        {
            var Result = await packagingRepository.SaveCustomerDetails(customer).ConfigureAwait(false);
            return Result;
        }

        public Task<List<Customer>> SearchCustomerData(Customer customer)
        {
            return SearchCustomerInfo(bookingService, customer);
        }

        public Task<List<Customer>> GetCustomerDatabyMembershipNo(Customer customer)
        {
            return GetCustomerInfobyMembershipNo(bookingService, customer);
        }
        
        private async Task<List<Customer>> GetCustomerInfobyMembershipNo(IBookingService packagingRepository, Customer customer)
        {
            var result = await packagingRepository.GetCustomerDatabyMembershipNo(customer).ConfigureAwait(false);
            return result;
        }

        private async Task<List<Customer>> SearchCustomerInfo(IBookingService packagingRepository, Customer customer)
        {
            var result = await packagingRepository.SearchCustomerDetails(customer).ConfigureAwait(false);
            return result;
        }

        public async Task<string> UpdateCustomer(Customer customer)
        {
            return UpdateCustomerInfo(bookingService, customer).Result;
        }

        private async Task<string> UpdateCustomerInfo(IBookingService packagingRepository, Customer customer)
        {
            var Result = await packagingRepository.UpdateCustomerDetails(customer).ConfigureAwait(false);
            return Result;
        }

    }
}