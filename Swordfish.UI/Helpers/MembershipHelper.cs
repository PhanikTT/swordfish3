﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Threading.Tasks;
using System.Security.Cryptography;
using System.Text;
using System.Collections.Concurrent;
using System.Web.Security;
using System.Globalization;
using Swordfish.Components.MembershipServices;
using Membership = Swordfish.Businessobjects.Membership;
using Swordfish.Businessobjects.Customers;
using Swordfish.UI.Models.Account;
using System.Web.SessionState;

namespace Swordfish.UI.Helpers
{
    public class MembershipHelper
    {
        private IMembershipService UserMembershipRepository;

        public MembershipHelper(IMembershipService membershipRepository)
        {
            UserMembershipRepository = membershipRepository;
        }

        public int CreateAgent(IMembershipService serviceRepository, UserModel user)
        {
            SFLogger.logMessage("MembershipHelper:CreateAgent(): started!!.");
            var isUserCreated = Task.Run(() => CreateUser(user));
            Task.WhenAll(isUserCreated);
            SFLogger.logMessage("MembershipHelper:CreateAgent(): end.");
            return isUserCreated.Result;
        }
        public int CreateUser(UserModel user)
        {
            SFLogger.logMessage("MembershipHelper:CreateUser(): started!!.");
            Membership.Agent agent = new Membership.Agent();
            agent.FirstName = user.FirstName;
            agent.LastName = user.LastName;
            agent.Email = user.Username;
            agent.Password = GetMd5Hash(user.Password);
            agent.CreatedBy = 1;
            agent.syntecAccountID = user.syntecAccountID;
            SFLogger.logMessage("MembershipHelper:CreateUser(): end.");
            return CreateAgent(UserMembershipRepository, agent);
        }

        public Membership.UserInfo SetupFormsAuthTicket(string userName, bool persistanceFlag)
        {
            SFLogger.logMessage("MembershipHelper:SetupFormsAuthTicket(): started!!.");
            Membership.UserInfo user = new Membership.UserInfo();
            // Get User Info from here
            MembershipService serviceRepository = new MembershipService();
            user = GetUserDetails(serviceRepository, userName);
            var userId = user.UserId;
            var userData = userId.ToString(CultureInfo.InvariantCulture);
            HttpContext.Current.Session["UserId"] = userId;
            FormsAuthentication.SetAuthCookie(userName, false);
            SFLogger.logMessage("MembershipHelper:SetupFormsAuthTicket(): end.");
            return user;
        }

        public Membership.UserInfo GetUserDetails(IMembershipService serviceRepository, string username)
        {
            SFLogger.logMessage("MembershipHelper:GetUserDetails(): started!!.");
            var user = Task.Run(() => GetUserDetails(username));
            Task.WhenAll(user);
            SFLogger.logMessage("MembershipHelper:GetUserDetails(): end.");
            return user.Result;
        }
        public int ValidateUser(LoginModel login)
        {
            SFLogger.logMessage("MembershipHelper:ValidateUser(): started!!.");
            Membership.LoginUser user = new Membership.LoginUser();
            user.Username = login.Username;
            user.Password = GetMd5Hash(login.Password);
            SFLogger.logMessage("MembershipHelper:ValidateUser(): end.");
            return CheckUser(UserMembershipRepository, user);
        }

        public async Task<int> CheckUserExists(IMembershipService serviceRepository, string username)
        {
            SFLogger.logMessage("MembershipHelper:CheckUserExists(): started!!.");
            var isValidUser = await Task.Run(() => ForgotPassword(username)).ConfigureAwait(false);
            SFLogger.logMessage("MembershipHelper:CheckUserExists(): end.");
            return isValidUser;
        }

        public int ForgotPassword(string email)
        {
            return ForgotUserEmailCheck(UserMembershipRepository, email);
        }

        public bool EnableResetPassword(MembershipService serviceRepository, string username, bool isEnable)
        {
            SFLogger.logMessage("MembershipHelper:EnableResetPassword(): started!!.");
            var isValidUser = Task.Run(() => EnableUserResetPassword(username, isEnable));
            Task.WhenAll(isValidUser);
            /// isValidUser.Wait();
            SFLogger.logMessage("MembershipHelper:EnableResetPassword(): started!!.");
            return isValidUser.Result;
        }

        public bool EnableUserResetPassword(string email, bool isEnable)
        {
            return EnableResetPassword(UserMembershipRepository, email, isEnable);
        }
        public async Task<bool> CheckResetPasswordEnable(IMembershipService serviceRepository, string email)
        {
            SFLogger.logMessage("MembershipHelper:CheckResetPasswordEnable(): started!!.");
            var isUsed = await Task.Run(() => CheckResetPasswordEnable(email)).ConfigureAwait(false);
            SFLogger.logMessage("MembershipHelper:CheckResetPasswordEnable(): end.");
            return isUsed;
        }
        public bool CheckResetPasswordEnable(string email)
        {
            return IsEnableResetPasswordToUser(UserMembershipRepository, email);
        }
        public async Task<bool> ResetUserPassword(IMembershipService serviceRepository, ResetPasswordViewModel model)
        {
            SFLogger.logMessage("MembershipHelper:ResetUserPassword(): started!!.");
            var isPasswordUpdate = await Task.Run(() => ResetUserPassword(model)).ConfigureAwait(false);
            SFLogger.logMessage("MembershipHelper:ResetUserPassword(): end.");
            return isPasswordUpdate;
        }
        public bool ResetUserPassword(ResetPasswordViewModel model)
        {
            SFLogger.logMessage("MembershipHelper:ResetUserPassword(): started!!.");
            Membership.UserResetPassword userPassword = new Membership.UserResetPassword();
            userPassword.Username = model.Email;
            userPassword.NewPassword = GetMd5Hash(model.Password);
            SFLogger.logMessage("MembershipHelper:ResetUserPassword(): end.");
            return ResetNewPassword(UserMembershipRepository, userPassword);
        }

        public Membership.UserInfo GetUserDetails(string username)
        {
            return GetUserDetailsByUsername(UserMembershipRepository, username);
        }

        public bool UpdateAgentLoginDate(int userId)
        {
            return UpdateLoginDateForAgent(UserMembershipRepository, userId);
        }

        private bool UpdateLoginDateForAgent(IMembershipService membershipRepository, int userId)
        {
            return AgentLoginDateUpdate(membershipRepository, userId);
        }

        private bool AgentLoginDateUpdate(IMembershipService membershipRepository, int userId)
        {
            SFLogger.logMessage("MembershipHelper:AgentLoginDateUpdate(): started!!.");
            bool status = false;
            try
            {
                status = membershipRepository.UpdateAgentLoginDate(userId);
            }
            catch (Exception ex)
            {
                SFLogger.logError("MembershipHelper:AgentLoginDateUpdate(): end.", ex);
            }
            SFLogger.logMessage("MembershipHelper:AgentLoginDateUpdate():end.");
            return status;
        }

        public List<Membership.Roles> GetAllRoles()
        {
            SFLogger.logMessage("MembershipHelper:GetAllRoles(): started!!.");
            ConcurrentBag<Membership.Roles> roles = new ConcurrentBag<Membership.Roles>();
            roles = GetAllApplicationRoles(UserMembershipRepository);
            SFLogger.logMessage("MembershipHelper:GetAllRoles(): end.");
            return roles.ToList<Membership.Roles>();
        }

        public IEnumerable<string> GetUserRoles(string username)
        {
            return GetUserAssignedRoles(UserMembershipRepository, username);
        }

        public int InsertNewRole(RoleModel role)
        {
            SFLogger.logMessage("MembershipHelper:InsertNewRole(): started!!.");
            Membership.Roles addrole = new Membership.Roles();
            addrole.RoleName = role.RoleName;
            addrole.Description = role.Description;
            addrole.CreatedBy = 1;
            addrole.CreatedOn = DateTime.Now;
            addrole.Action = "Create";
            SFLogger.logMessage("MembershipHelper:InsertNewRole(): end.");
            return RoleOperations(UserMembershipRepository, addrole);
        }

        public int UpdateRole(RoleModel role)
        {
            SFLogger.logMessage("MembershipHelper:UpdateRole(): started!!.");
            Membership.Roles addrole = new Membership.Roles();
            addrole.RoleId = role.RoleId;
            addrole.RoleName = role.RoleName;
            addrole.Description = role.Description;
            addrole.UpdatedBy = 1;
            addrole.UpdatedOn = DateTime.Now;
            addrole.Action = "Update";
            SFLogger.logMessage("MembershipHelper:UpdateRole(): end.");
            return RoleOperations(UserMembershipRepository, addrole);
        }

        public int DeleteRole(RoleModel role)
        {
            SFLogger.logMessage("MembershipHelper:DeleteRole(): started!!.");
            Membership.Roles addrole = new Membership.Roles();

            addrole.RoleId = role.RoleId;
            addrole.Action = "Delete";
            SFLogger.logMessage("MembershipHelper:DeleteRole(): end.");
            return RoleOperations(UserMembershipRepository, addrole);
        }

        public List<Membership.GraphData> GetLast7DaysGraph(string currentDate, int userId)
        {
            SFLogger.logMessage("MembershipHelper:GetLast7DaysGraph(): started!!.");
            ConcurrentBag<Membership.GraphData> graphData = new ConcurrentBag<Membership.GraphData>();
            graphData = Get7DaysGraph(UserMembershipRepository, currentDate, userId);
            SFLogger.logMessage("MembershipHelper:GetLast7DaysGraph(): end.");
            return graphData.ToList();
        }

        public List<Membership.GraphData> GetLastFourWeeksGraph(string currentDate, int userId)
        {
            SFLogger.logMessage("MembershipHelper:GetLastFourWeeksGraph(): started!!.");
            ConcurrentBag<Membership.GraphData> graphData = new ConcurrentBag<Membership.GraphData>();
            graphData = GetFourWeeksGraph(UserMembershipRepository, currentDate, userId);
            SFLogger.logMessage("MembershipHelper:GetLastFourWeeksGraph(): end.");
            return graphData.ToList();
        }

        public List<Membership.TodaysActions> GetTodaysActionData(int userId, bool isOnlyTodayAndPastActions)
        {
            SFLogger.logMessage("MembershipHelper:GetTodaysActionData(): started!!.");
            ConcurrentBag<Membership.TodaysActions> graphData = new ConcurrentBag<Membership.TodaysActions>();
            graphData = getTodaysActionData(UserMembershipRepository, userId);
            if (isOnlyTodayAndPastActions)
            {
                var toDayAndPastActions = new ConcurrentBag<Membership.TodaysActions>();
                foreach (var actionitem in graphData)
                {
                    if (!String.IsNullOrEmpty(actionitem.remindersdate))
                    {
                        if (DateTime.Now.Date.AddDays(1).CompareTo(Convert.ToDateTime(actionitem.remindersdate)) >= 0)
                            toDayAndPastActions.Add(actionitem);
                    }
                }
                graphData = toDayAndPastActions;
            }
            SFLogger.logMessage("MembershipHelper:GetTodaysActionData(): end.");
            return graphData.ToList();
        }

        private ConcurrentBag<Membership.GraphData> Get7DaysGraph(IMembershipService membershipRepository, string currentDate, int userId)
        {
            return GetGraphFor7Days(membershipRepository, currentDate, userId);
        }

        private ConcurrentBag<Membership.GraphData> GetFourWeeksGraph(IMembershipService membershipRepository, string currentDate, int userId)
        {
            return GetGraphForFourWeeks(membershipRepository, currentDate, userId);
        }

        private ConcurrentBag<Membership.TodaysActions> getTodaysActionData(IMembershipService membershipRepository, int userId)
        {
            return GetTodaysAction(membershipRepository, userId);
        }

        private ConcurrentBag<Membership.GraphData> GetGraphFor7Days(IMembershipService membershipRepository, string currentDate, int userId)
        {
            SFLogger.logMessage("MembershipHelper:GetGraphFor7Days(): started!!.");
            ConcurrentBag<Membership.GraphData> graphData = new ConcurrentBag<Membership.GraphData>();
            try
            {
                graphData = membershipRepository.GetGraphData(userId, currentDate);
            }
            catch (Exception ex)
            {
                SFLogger.logError("MembershipHelper:GetGraphFor7Days(): end.", ex); ;
            }
            SFLogger.logMessage("MembershipHelper:GetGraphFor7Days(): end.");
            return graphData;
        }

        private ConcurrentBag<Membership.GraphData> GetGraphForFourWeeks(IMembershipService membershipRepository, string currentDate, int userId)
        {
            SFLogger.logMessage("MembershipHelper:GetGraphForFourWeeks(): started!!.");
            ConcurrentBag<Membership.GraphData> graphData = new ConcurrentBag<Membership.GraphData>();

            try
            {
                graphData = membershipRepository.GetGraphDataforfourWeeks(userId, currentDate);
            }
            catch (Exception ex)
            {
                SFLogger.logError("MembershipHelper:GetGraphForFourWeeks(): end.", ex);
            }
            SFLogger.logMessage("MembershipHelper:GetGraphForFourWeeks(): end.");
            return graphData;
        }

        private ConcurrentBag<Membership.TodaysActions> GetTodaysAction(IMembershipService membershipRepository, int userId)
        {
            SFLogger.logMessage("MembershipHelper:GetTodaysAction(): started!!.");
            ConcurrentBag<Membership.TodaysActions> graphData = new ConcurrentBag<Membership.TodaysActions>();
            try
            {
                graphData = membershipRepository.GetTodaysActionData(userId);
            }
            catch (Exception ex)
            {
                SFLogger.logError("MembershipHelper:GetTodaysAction(): end.", ex);
            }
            SFLogger.logMessage("MembershipHelper:GetTodaysAction(): end.");
            return graphData;
        }

        private IEnumerable<string> GetUserAssignedRoles(IMembershipService membershipRepository, string username)
        {
            return GetAllAssingedRolesToUser(membershipRepository, username);
        }

        private IEnumerable<string> GetAllAssingedRolesToUser(IMembershipService membershipRepository, string username)
        {
            SFLogger.logMessage("MembershipHelper:GetAllAssingedRolesToUser(): started!!.");
            IEnumerable<string> roles = null;
            try
            {
                roles = membershipRepository.GetUserAssignedRoles(username);
            }
            catch (Exception ex)
            {
                SFLogger.logError("MembershipHelper:GetAllAssingedRolesToUser(): end.", ex);
            }
            SFLogger.logMessage("MembershipHelper:GetAllAssingedRolesToUser(): end.");
            return roles;
        }

        private ConcurrentBag<Membership.Roles> GetAllApplicationRoles(IMembershipService membershipRepository)
        {
            return GetRoles(membershipRepository);
        }

        private ConcurrentBag<Membership.Roles> GetRoles(IMembershipService membershipRepository)
        {
            SFLogger.logMessage("MembershipHelper:GetRoles(): started!!.");
            ConcurrentBag<Membership.Roles> roles = new ConcurrentBag<Membership.Roles>();
            try
            {
                roles = membershipRepository.GetAllRoles();
            }
            catch (Exception ex)
            {
                SFLogger.logError("MembershipHelper:GetRoles():end.", ex);
            }
            SFLogger.logMessage("MembershipHelper:GetRoles(): end.");
            return roles;
        }

        public bool IsUserInRole(string userid, string role)
        {
            return CheckUserInRole(UserMembershipRepository, userid, role);
        }

        private Membership.UserInfo GetUserDetailsByUsername(IMembershipService membershipRepository, string username)
        {
            return GetUserCompleteDetails(membershipRepository, username);
        }

        private Membership.UserInfo GetUserCompleteDetails(IMembershipService membershipRepository, string username)
        {
            SFLogger.logMessage("MembershipHelper:GetUserCompleteDetails(): started!!.");
            Membership.UserInfo userDetails = new Membership.UserInfo();
            try
            {
                userDetails = membershipRepository.GetUserDetails(username);
            }
            catch (Exception ex)
            {
                SFLogger.logError("MembershipHelper:GetUserCompleteDetails(): end.", ex);
            }
            SFLogger.logMessage("MembershipHelper:GetUserCompleteDetails(): end.");
            return userDetails;
        }

        private bool CheckUserInRole(IMembershipService membershipRepository, string userid, string role)
        {
            return UserRoleMapping(membershipRepository, userid, role);
        }

        private bool UserRoleMapping(IMembershipService membershipRepository, string userid, string role)
        {
            SFLogger.logMessage("MembershipHelper:UserRoleMapping(): started!!.");
            bool result = false;
            try
            {
                result = membershipRepository.IsUserInRole(userid, role);
            }
            catch (Exception ex)
            {
                SFLogger.logError("MembershipHelper:UserRoleMapping(): end.", ex);
            }
            SFLogger.logMessage("MembershipHelper:UserRoleMapping(): end.");
            return result;
        }

        private bool ResetNewPassword(IMembershipService membershipRepository, Membership.UserResetPassword model)
        {
            return ResetPassword(membershipRepository, model);
        }

        private bool ResetPassword(IMembershipService membershipRepository, Membership.UserResetPassword model)
        {
            SFLogger.logMessage("MembershipHelper:ResetPassword(): started!!.");
            bool result = false;
            try
            {
                result = membershipRepository.ResetPassword(model);
            }
            catch (Exception ex)
            {
                SFLogger.logError("MembershipHelper:ResetPassword(): end.", ex);
            }
            SFLogger.logMessage("MembershipHelper:ResetPassword(): end.");
            return result;
        }

        private int RoleOperations(IMembershipService membershipRepository, Membership.Roles role)
        {
            return AddRole(membershipRepository, role);
        }

        private int AddRole(IMembershipService membershipRepository, Membership.Roles role)
        {
            SFLogger.logMessage("MembershipHelper:AddRole(): started!!.");
            int result = 0;
            try
            {
                result = membershipRepository.RoleOperations(role);
            }
            catch (Exception ex)
            {
                SFLogger.logError("MembershipHelper:AddRole(): end.", ex);
            }
            SFLogger.logMessage("MembershipHelper:AddRole(): started!!.");
            return result;
        }

        private bool IsEnableResetPasswordToUser(IMembershipService membershipRepository, string email)
        {
            return CheckResetPasswordEnabled(membershipRepository, email);
        }

        private bool CheckResetPasswordEnabled(IMembershipService membershipRepository, string email)
        {
            SFLogger.logMessage("MembershipHelper:CheckResetPasswordEnabled(): started!!.");
            bool result = false;
            try
            {
                result = membershipRepository.IsEnableResetPasswordToUser(email);
            }
            catch (Exception ex)
            {
                SFLogger.logError("MembershipHelper:CheckResetPasswordEnabled(): end.", ex);
            }
            SFLogger.logMessage("MembershipHelper:CheckResetPasswordEnabled(): end.");
            return result;
        }

        private bool EnableResetPassword(IMembershipService membershipRepository, string email, bool isEnable)
        {
            return EnableResetPasswordOption(membershipRepository, email, isEnable);
        }

        private bool EnableResetPasswordOption(IMembershipService membershipRepository, string email, bool isEnable)
        {
            SFLogger.logMessage("MembershipHelper:EnableResetPasswordOption(): started!!.");
            bool result = false;
            try
            {
                result = membershipRepository.EnableUserResetPassword(email, isEnable);
            }
            catch (Exception ex)
            {
                SFLogger.logError("MembershipHelper:EnableResetPasswordOption(): end.", ex);
            }
            SFLogger.logMessage("MembershipHelper:EnableResetPasswordOption(): end.");
            return result;
        }

        private int ForgotUserEmailCheck(IMembershipService membershipRepository, string email)
        {
            return CheckEmailExists(membershipRepository, email);
        }

        private int CheckEmailExists(IMembershipService membershipRepository, string email)
        {
            SFLogger.logMessage("MembershipHelper:CheckEmailExists(): started!!.");
            int result = 0;
            try
            {
                result = membershipRepository.ForgotPassword(email);
            }
            catch (Exception ex)
            {
                SFLogger.logError("MembershipHelper:CheckEmailExists(): end.", ex);
            }
            SFLogger.logMessage("MembershipHelper:CheckEmailExists(): end.");
            return result;
        }

        private int CreateAgent(IMembershipService membershipRepository, Membership.Agent agent)
        {
            return CreateNewAgent(membershipRepository, agent);
        }

        private int CreateNewAgent(IMembershipService membershipRepository, Membership.Agent agent)
        {
            SFLogger.logMessage("MembershipHelper:CreateNewAgent(): started!!.");
            int result = 0;
            try
            {
                result = membershipRepository.CreateAgent(agent);
            }
            catch (Exception ex)
            {
                SFLogger.logError("MembershipHelper:CreateNewAgent(): started!!.", ex);
            }
            SFLogger.logMessage("MembershipHelper:CreateNewAgent(): started!!.");
            return result;
        }

        private int CheckUser(IMembershipService membershipRepository, Membership.LoginUser user)
        {
            return CheckUserExists(membershipRepository, user);
        }

        private int CheckUserExists(IMembershipService membershipRepository, Membership.LoginUser user)
        {
            SFLogger.logMessage("MembershipHelper:CheckUserExists(): started!!.");
            int result = 0;
            try
            {
                result = membershipRepository.ValidateUser(user);
            }
            catch (Exception ex)
            {
                SFLogger.logError("MembershipHelper:CheckUserExists(): end.", ex);
            }
            SFLogger.logMessage("MembershipHelper:CheckUserExists(): end.");
            return result;
        }

        private static string GetMd5Hash(string value)
        {
            SFLogger.logMessage("MembershipHelper:GetMd5Hash(): started!!.");
            var md5Hasher = MD5.Create();
            var data = md5Hasher.ComputeHash(Encoding.Default.GetBytes(value));
            var sBuilder = new StringBuilder();
            for (var i = 0; i < data.Length; i++)
            {
                sBuilder.Append(data[i].ToString("x2"));
            }
            SFLogger.logMessage("MembershipHelper:GetMd5Hash(): end.");
            return sBuilder.ToString();
        }

        public IncomingCustomer InsertIncomingCustomerData(IncomingCustomer CustomerObject)
        {
            return InsertIncomingCustomer(UserMembershipRepository, CustomerObject);
        }

        private IncomingCustomer InsertIncomingCustomer(IMembershipService membershipRepository, IncomingCustomer CustomerObject)
        {
            return GetCreateQuoteInformation(membershipRepository, CustomerObject);
        }

        private IncomingCustomer GetCreateQuoteInformation(IMembershipService membershipRepository, IncomingCustomer CustomerObject)
        {
            var IncomingCustomer = membershipRepository.InsertIncomingCustomerData(CustomerObject);
            return IncomingCustomer;
        }

        public bool DecriptPasswordToken(string token)
        {
            SFLogger.logMessage("MembershipHelper:DecriptPasswordToken(): started!!.");
            bool isValidToken = false;
            byte[] data = Convert.FromBase64String(token);
            DateTime when = DateTime.FromBinary(BitConverter.ToInt64(data, 0));
            if (DateTime.UtcNow <= when.AddMinutes(15))
            {
                isValidToken = true;
            }
            SFLogger.logMessage("MembershipHelper:DecriptPasswordToken(): end.");
            return isValidToken;
        }

        public string GeneratePasswordToken()
        {
            SFLogger.logMessage("MembershipHelper:GeneratePasswordToken(): started!!.");
            byte[] time = BitConverter.GetBytes(DateTime.UtcNow.ToBinary());
            byte[] key = Guid.NewGuid().ToByteArray();
            string token = Convert.ToBase64String(time.Concat(key).ToArray());
            SFLogger.logMessage("MembershipHelper:GeneratePasswordToken(): end.");
            return token;
        }

        public void ResetSessionID()
        {
            SFLogger.logMessage("MembershipHelper:ResetSessionID(): started!!.");
            MembershipService serviceRepository = new MembershipService();
            SessionIDManager manager = new SessionIDManager();
            manager.RemoveSessionID(HttpContext.Current);
            var newId = manager.CreateSessionID(HttpContext.Current);
            var userInfo = GetUserDetails(serviceRepository, SFConfiguration.GetUserEmail());
            userInfo.Environment = SFConfiguration.GetEnvironment();
            var UserName = userInfo.FirstName + " " + userInfo.LastName;
            SFConfiguration.SetUserEmail(SFConfiguration.GetUserEmail(), newId);
            SFConfiguration.SetUserInfo(userInfo, newId);
            SFConfiguration.SetUserName(UserName, newId);
            SFConfiguration.SetUserID(userInfo.UserId, newId);
            SFConfiguration.SetUserEmail(userInfo.UserEmailAddress, newId);
            var isRedirected = true;
            var isAdded = true;
            manager.SaveSessionID(HttpContext.Current, newId, out isRedirected, out isAdded);
            SFLogger.logMessage("MembershipHelper:ResetSessionID(): end.");
        }
    }
}