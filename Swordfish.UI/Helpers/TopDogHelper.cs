﻿using Swordfish.Businessobjects.Basket;
using Swordfish.Businessobjects.Passengers;
using Swordfish.Businessobjects.Payments;
using Swordfish.Businessobjects.TopDog;
using System.Collections.Generic;
using Swordfish.Components.TopDogServices;

namespace Swordfish.UI.Helpers
{
    public class TopDogHelper
    {
        private ITopDogService Repository;

        public TopDogHelper(ITopDogService topdogRepository)
        {
            Repository = topdogRepository;
        }

        public List<string> SaveDataTopDog(ITopDogService topdogRepository, Basket ChilliBasket, List<Passenger> PassengerLst, PaymentDetails PayDtl, List<ExtraCharge> extraCharge)
        {
            return DataToTopDog(Repository, ChilliBasket, PassengerLst, PayDtl, extraCharge);
        }

        private List<string> DataToTopDog(ITopDogService topdogRepository, Basket ChilliBasket, List<Passenger> PassengerLst, PaymentDetails PayDtl, List<ExtraCharge> extraCharge)
        {
            //List<string> TopDogResponselst = topdogRepository.SaveDataInTopDog(ChilliBasket, PassengerLst, PayDtl, extraCharge);
            return null;
        }


    }
}
