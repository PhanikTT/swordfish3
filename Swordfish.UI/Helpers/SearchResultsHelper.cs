﻿using Newtonsoft.Json;
using RestSharp;
using Swordfish.Businessobjects.Bookings;
using Swordfish.Businessobjects.Common;
using Swordfish.Businessobjects.Customers;
using Flights = Swordfish.Businessobjects.Flights;
using Swordfish.Businessobjects.Hotels;
using Swordfish.Businessobjects.Quotes;
using Swordfish.Businessobjects.Sesame;
using Swordfish.Businessobjects.Transfers;
using System;
using System.Collections;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using Swordfish.Businessobjects.Baggage;
using Swordfish.Components.BookingServices;
using Swordfish.UI.Models.Account;
using Swordfish.UI.Models.Hotel;
using Swordfish.UI.Models.Transfers;
using Swordfish.UI.Models.Flight;
using Swordfish.UI.Models.Package;
using Swordfish.UI.Models.Basket;
using Swordfish.UI.Models.Booking;
using Swordfish.UI.Models.TravelSAAS;

namespace Swordfish.UI.Helpers
{
    public class SearchResultsHelper
    {
        private static List<DestinationInfo> DestinationItems = null;
        private static List<SourceInfo> SourceItems = null;
        public static List<HotelDetails> GetResort(SearchModel searchModel)
        {
            SFLogger.logMessage("SearchResultsHelper:GetResort(): started!!.", "GetResort", JsonConvert.SerializeObject(searchModel));
            string url = string.Format(SFConfiguration.GetTravelSaasFetchResort(), GetSessionID());
            var task = Task.Run(() => GetHotelListFromService(searchModel, url));
            Task.WhenAll(task);
            var hotelDetails = GetHotelDetails(searchModel, task.Result);
            var predicate = GetPridicate(searchModel);
            SFLogger.logMessage("SearchResultsHelper:GetResort(): end. Total number of resorts " + hotelDetails.Count);
            SFLogger.logMessage("SearchResultsHelper:GetResort(): started!!.", "GetResort", JsonConvert.SerializeObject(task.Result));
            return task.Result.AsQueryable().Where(predicate).OrderByDescending(n => n.PopularHotel).ThenByDescending(r => r.Rating).ThenByDescending(t => t.TripAdvisorScore).ThenBy(p => p.AmountAfterTax).ToList();
        }
        public static List<HotelDetails> GetMultiRoom(SearchModel searchModel)
        {
            SFLogger.logMessage("SearchResultsHelper:GetMultiRoom(): started!!.", "GetMultiRoom", JsonConvert.SerializeObject(searchModel));
            string url = string.Format(SFConfiguration.GetTravelSaasMultiRoom(), GetSessionID());
            var task = Task.Run(() => GetHotelListFromService(searchModel, url));
            Task.WhenAll(task);
            var hotelDetails = GetHotelDetails(searchModel, task.Result);
            var predicate = GetPridicate(searchModel);
            SFLogger.logMessage("SearchResultsHelper:GetMultiRoom(): end!!.", "GetMultiRoom", JsonConvert.SerializeObject(task.Result));
            return task.Result.AsQueryable().Where(predicate).OrderByDescending(n => n.PopularHotel).ThenByDescending(r => r.Rating).ThenByDescending(t => t.TripAdvisorScore).ThenBy(p => p.AmountAfterTax).ToList();
        }
        public static AccoAsyncResortResponse GetAsyncResort(SearchModel searchModel)
        {
            SFLogger.logMessage("SearchResultsHelper:GetAsyncResort(): started!!.", "GetAsyncResort", JsonConvert.SerializeObject(searchModel));
            string url = string.Format(SFConfiguration.GetTravelSaasAsyncResort(), GetSessionID());
            var task = Task.Run(() => GetResultSetKeyFromService(searchModel, url ));

            SFLogger.logMessage("SearchResultsHelper:GetAsyncResort(): end!!.", "GetAsyncResort", JsonConvert.SerializeObject(task.Result));
            return task.Result;
        }
        public static AccoAsyncResortResponse GetAsyncMultiRoom(SearchModel searchModel)
        {
            SFLogger.logMessage("SearchResultsHelper:GetAsyncMultiRoom(): started!!.", "GetAsyncMultiRoom", JsonConvert.SerializeObject(searchModel));
            string url = string.Format(SFConfiguration.GetTravelSaasAsyncMultiRoom(), GetSessionID());
            var task = Task.Run(() => GetResultSetKeyFromService(searchModel, url));

            SFLogger.logMessage("SearchResultsHelper:GetAsyncMultiRoom(): end!!.", "GetAsyncMultiRoom", JsonConvert.SerializeObject(task.Result));
            return task.Result;
        }
        public static AccoSearchStatus GetSearchStatus(AccoSearchStatusRequest accoSearchStatusModel)
        {
            SFLogger.logMessage("SearchResultsHelper:GetSearchStatus(): started!!.", "GetSearchStatus", JsonConvert.SerializeObject(accoSearchStatusModel));
            string url = string.Format(SFConfiguration.GetTravelSaasSearchStatus(), GetSessionID());
            var task = Task.Run(() => GetSearchStatusFromService(accoSearchStatusModel, url));

            SFLogger.logMessage("SearchResultsHelper:GetSearchStatus(): end!!.", "GetSearchStatus", JsonConvert.SerializeObject(task.Result));
            return task.Result;
        }
        public static List<HotelDetails> GetDeltaResults(AccoDeltaResultsRequest accoDeltaResultsRequest)
        {
            SFLogger.logMessage("SearchResultsHelper:GetDeltaResults(): started!!.", "GetDeltaResults", JsonConvert.SerializeObject(accoDeltaResultsRequest));
            string url = string.Format(SFConfiguration.GetTravelSaasDeltaResults(), GetSessionID());
            var task = Task.Run(() => GetDeltaResultsFromService(accoDeltaResultsRequest, url));
            Task.WhenAll(task);
           // var hotelDetails = GetHotelDetails(accoDeltaResultsRequest.searchModel, task.Result);

            var predicate = GetPridicate(accoDeltaResultsRequest.searchModel);
            SFLogger.logMessage("SearchResultsHelper:GetDeltaResults(): end. Total number of resorts " + task.Result.Count);
            SFLogger.logMessage("SearchResultsHelper:GetDeltaResults(): end!!.", "GetDeltaResults", JsonConvert.SerializeObject(task.Result));
            return task.Result.ToList();// AsQueryable().Where(predicate).OrderByDescending(n => n.PopularHotel).ThenByDescending(r => r.Rating).ThenByDescending(t => t.TripAdvisorScore).ThenBy(p => p.AmountAfterTax).ToList();
        }
        private static Expression<Func<HotelDetails, bool>> GetPridicate(SearchModel searchModel)
        {
            SFLogger.logMessage("SearchResultsHelper:GetPridicate(): started!!.");
            var predicate = PredicateBuilder.True<HotelDetails>();

            //if (!string.IsNullOrEmpty(searchModel.Hotel))
            //{
            //    predicate = predicate.And(o => o.HotelName.Contains(searchModel.Hotel));
            //}
            //if (!string.IsNullOrEmpty(searchModel.BoardType))
            //{
            //    predicate = predicate.And(o => searchModel.BoardType.Contains(o.BoardType));
            //}
            //if (searchModel.Rating > 0)
            //{
            //    predicate = predicate.And(o => o.Rating >= searchModel.Rating);
            //}
            SFLogger.logMessage("SearchResultsHelper:GetPridicate(): end.");
            return predicate;
        }
        public static HotelInformatoin GetHotelInfo(HotelInfoRequest hotelInfoRequest)
        {
            SFLogger.logMessage("SearchResultsHelper:GetHotelInfo(): started!!.", "GetHotelInfo", JsonConvert.SerializeObject(hotelInfoRequest));
            if (!string.IsNullOrEmpty(hotelInfoRequest.SupplierAccommId))
            {
                var hotelMasterKey = hotelInfoRequest.SupplierAccommId.Split(';');
                if (hotelMasterKey.Length == 3)
                    hotelInfoRequest.SupplierAccommId = hotelMasterKey[2];// index 2 for hotel supplier master key
            }
            string url = string.Format(SFConfiguration.GetTravelSaasHotelInfo(), GetSessionID());
            var task = Task.Run(() => GetHotelInfoFromService(hotelInfoRequest, url));
            Task.WhenAll(task);

            SFLogger.logMessage("SearchResultsHelper:GetHotelInfo(): end!!.", "GetHotelInfo", JsonConvert.SerializeObject(task.Result));
            return task.Result;
        }

        private static string GetSessionID()
        {
            if (HttpContext.Current != null)
                return HttpContext.Current.Session.SessionID;
            else
                return "Not Available";
        }

        private static ConcurrentBag<HotelDetails> GetHotelListFromService(SearchModel searchModel, string url)
        {
            SFLogger.logMessage("SearchResultsHelper:GetHotelListFromService(): started!!.");
            var lstHotels = new ConcurrentBag<HotelDetails>();
            var list = new ArrayList();
            list.Add(searchModel);
            var travelSAASClient = new TravelSAASRestClient();
            var hotelList = new HotelList();
            var result = travelSAASClient.ExecuteRequest(url, RequestType.Post, list);
            if (result.StatusCode == (int)System.Net.HttpStatusCode.OK)
                hotelList = JsonConvert.DeserializeObject<HotelList>(result.Response);
            else
                throw new SwordfishAjaxException(result.Response);
            if (hotelList != null && hotelList.HotelResults != null)
                lstHotels = new ConcurrentBag<HotelDetails>(hotelList.HotelResults);
            SFLogger.logMessage("SearchResultsHelper:GetHotelListFromService(): end.");
            return lstHotels;
        }
        private static AccoAsyncResortResponse GetResultSetKeyFromService(SearchModel searchModel, string url)
        {
            SFLogger.logMessage("SearchResultsHelper:GetResultSetKeyFromService(): started!!.");
            var list = new ArrayList();
            list.Add(searchModel);
            var travelSAASClient = new TravelSAASRestClient();
            var accHotelList = new AccoAsyncResortResponse();
            var result = travelSAASClient.ExecuteRequest(url, RequestType.Post, list);
            if (result.StatusCode == (int)System.Net.HttpStatusCode.OK)
                accHotelList = JsonConvert.DeserializeObject<AccoAsyncResortResponse>(result.Response);
            else
                throw new SwordfishAjaxException(result.Response);
            SFLogger.logMessage("SearchResultsHelper:GetResultSetKeyFromService(): end.");
            return accHotelList;
        }
        private static AccoSearchStatus GetSearchStatusFromService(AccoSearchStatusRequest accoSearchStatusModel, string url)
        {
            SFLogger.logMessage("SearchResultsHelper:GetSearchStatusFromService(): started!!.");
            var list = new ArrayList();
            list.Add(accoSearchStatusModel);
            var travelSAASClient = new TravelSAASRestClient();
            var accHotelList = new AccoSearchStatus();
            var result = travelSAASClient.ExecuteRequest(url, RequestType.Post, list);
            if (result.StatusCode == (int)System.Net.HttpStatusCode.OK)
                accHotelList = JsonConvert.DeserializeObject<AccoSearchStatus>(result.Response);
            else
                throw new SwordfishAjaxException(result.Response);
            SFLogger.logMessage("SearchResultsHelper:GetSearchStatusFromService(): end.");
            return accHotelList;
        }
        private static ConcurrentBag<HotelDetails> GetDeltaResultsFromService(AccoDeltaResultsRequest req, string url)
        {
            SFLogger.logMessage("SearchResultsHelper:GetDeltaResultsFromService(): started!!.");
            var lstHotels = new ConcurrentBag<HotelDetails>();
            var list = new ArrayList();
            list.Add(req);
            var travelSAASClient = new TravelSAASRestClient();
            var hotelList = new HotelList();
            var result = travelSAASClient.ExecuteRequest(url, RequestType.Post, list);
            if (result.StatusCode == (int)System.Net.HttpStatusCode.OK)
                hotelList = JsonConvert.DeserializeObject<HotelList>(result.Response);
            else
                throw new SwordfishAjaxException(result.Response);
            if (hotelList != null && hotelList.HotelResults != null)
                lstHotels = new ConcurrentBag<HotelDetails>(hotelList.HotelResults);
            SFLogger.logMessage("SearchResultsHelper:GetDeltaResultsFromService(): end");
            return lstHotels;
        }
        private static HotelInformatoin GetHotelInfoFromService(HotelInfoRequest hotelInfoRequest, string url)
        {
            SFLogger.logMessage("SearchResultsHelper:GetHotelInfoFromService(): started!!.");
            var list = new ArrayList();
            list.Add(hotelInfoRequest);
            var travelSAASClient = new TravelSAASRestClient();
            var hotelInfo = new HotelInformatoin();
            var result = travelSAASClient.ExecuteRequest(url, RequestType.Post, list);
            if (result.StatusCode == (int)System.Net.HttpStatusCode.OK)
                hotelInfo = JsonConvert.DeserializeObject<HotelInformatoin>(result.Response);
            else
                throw new SwordfishAjaxException(result.Response);
            SFLogger.logMessage("SearchResultsHelper:GetHotelInfoFromService(): end.");
            return hotelInfo;
        }
        private static ConcurrentBag<HotelDetails> GetHotelDetails(SearchModel searchModel, ConcurrentBag<HotelDetails> lstHotels)
        {
            SFLogger.logMessage("SearchResultsHelper:GetHotelDetails(): started!!.");
            try
            {
                if (lstHotels != null && lstHotels.Count > 0)
                {
                    Parallel.ForEach(lstHotels, hotel =>
                    // foreach(HotelDetails hotel in lstHotels)
                    {
                        //SFLogger.logMessage("Pricessig hotel with ID " + hotel.HotelName + " - TTICode " + hotel.TTIcode + " - ResortID  " + hotel.SupplierResortId + " - MasterHotelID  " + hotel.HotelCode);
                        try
                        {
                            Task<ResortHotelRatings> t_Ratings = Task.Factory.StartNew(() => SFConfiguration.GetResortAndHotelRatings(hotel.TTIcode, hotel.HotelCode, hotel.SupplierResortId));
                           // ResortHotelRatings resortHotelRatings = SFConfiguration.GetResortAndHotelRatings(hotel.TTIcode, hotel.HotelCode, hotel.SupplierResortId);
                            var resortHotelRatings = t_Ratings.Result;
                            if (resortHotelRatings != null)
                            {
                                hotel.ResortName = resortHotelRatings.ResortName;
                                hotel.Rating = resortHotelRatings.Rating;
                                hotel.TripAdvisorScore = resortHotelRatings.TripAdvisorScore;
                            }
                            if (hotel.RoomRates != null && hotel.RoomRates.Count > 0)
                            {
                                hotel.RoomRates = hotel.RoomRates.OrderBy(p => Convert.ToDecimal(hotel.RoomRates[0].AmountAfterTax) / 100).ToList();
                                hotel.RoomDescription = hotel.RoomRates[0].RoomDescription;
                                hotel.RateQuoteId = hotel.RoomRates[0].QuoteId;
                                hotel.RoomTypeCode = hotel.RoomRates[0].RoomTypeCode;
                                var leastPrice = Convert.ToDecimal(hotel.RoomRates[0].AmountAfterTax) / 100;
                                hotel.AmountAfterTax = leastPrice;
                                hotel.BoardType = hotel.RoomRates[0].RatePlanCode;
                                hotel.PricePerPerson = leastPrice / (searchModel.Adults + searchModel.Child);
                                foreach (var roomRate in hotel.RoomRates)
                                {
                                    roomRate.AmountAfterTax = Convert.ToDecimal(roomRate.AmountAfterTax) / 100;
                                }
                            }
                        }
                        catch (Exception ex)
                        {
                            SFLogger.logError("Error occured while pricessig hotel with ID" + hotel.HotelName + " - TTICode " + hotel.TTIcode + " - ResortID  " + hotel.SupplierResortId + " - hotelCodeId  " + hotel.HotelCode, ex);

                        }

                    });
                }
            }
            catch (Exception ex)
            {
                SFLogger.logError("SearchResultsHelper:GetHotelDetails(): end.", ex);
                throw ex;
            }
            SFLogger.logMessage("SearchResultsHelper:GetHotelDetails(): end.");
            return lstHotels;
        }

        public static List<Rootobject> GetTransfersDataFromService(TransferSearchRequest transferRequest)
        {
            SFLogger.logMessage("SearchResultsHelper:GetTransfersDataFromService(): started!!.", "GetTransfersDataFromService", JsonConvert.SerializeObject(transferRequest));
            TransferResult lstTransfersData = null;
            lstTransfersData = GetTrasfersForLook(transferRequest);
            if (lstTransfersData.Rootobject != null && lstTransfersData.Rootobject.Count == 0)
            {
                var hotelInfo = GetHotelInfo(transferRequest.HotelInfoRequest);
                transferRequest.ToType = string.Empty;
                transferRequest.ToCode = string.Empty;
                transferRequest.LatLong = hotelInfo.Address.LatLong;
                transferRequest.LatLongType = SFConstants.LatLongType;
                lstTransfersData = GetTrasfersForLook(transferRequest);
            }
            return lstTransfersData.Rootobject;
        }

        private static TransferResult GetTrasfersForLook(TransferSearchRequest transferRequest)
        {
            string url = string.Format(SFConfiguration.GetTravelSaasTransfers(), GetSessionID());
            var list = new ArrayList();
            var lstTransfersData = new TransferResult();
            list.Add(transferRequest);
            var travelSAASClient = new TravelSAASRestClient();
            var result = travelSAASClient.ExecuteRequest(url, RequestType.Post, list);
            if (result.StatusCode == (int)System.Net.HttpStatusCode.OK)
            {
                lstTransfersData = JsonConvert.DeserializeObject<TransferResult>(result.Response);
                PerformTravellingReturnMapping(lstTransfersData);
            }
            else
                throw new SwordfishAjaxException(result.Response);
            SFLogger.logMessage("SearchResultsHelper:GetTransfersDataFromService(): end!!.", "GetTransfersDataFromService", JsonConvert.SerializeObject(result));
            return lstTransfersData;
        }
        private static List<Rootobject> PerformTravellingReturnMapping(TransferResult lstTransfersData)
        {
            var transfersReturnings = (from p in lstTransfersData.Rootobject[0].returning.products select p);
            foreach (var transfersReturnId in transfersReturnings)
            {
                var objtransferTravelling = lstTransfersData.Rootobject[0].travelling.products.SingleOrDefault(x => x.general[0].productid.Equals(transfersReturnId.general[0].productid));
                if (objtransferTravelling != null)
                {
                    objtransferTravelling.general[0].MappingReturnFound = true;
                    objtransferTravelling.pricing[0].totalPrice = objtransferTravelling.pricing[0].price + transfersReturnId.pricing[0].price;
                }
            }
            return lstTransfersData.Rootobject;
        }
        /// <summary>
        /// Logic to get Flights list.
        /// </summary>
        /// <param name="searchModel">TwoWay Search criteria data</param>
        /// <returns></returns>
        public static FlightDetails GetFlights(SearchModel searchModel)
        {
            SFLogger.logMessage("SearchResultsHelper:GetFlights(): started!!.", "GetFlights", JsonConvert.SerializeObject(searchModel));
            var srcJsonPath = HttpContext.Current.Server.MapPath("~/JsonData/Source.json");
            var destJsonPath = HttpContext.Current.Server.MapPath("~/JsonData/destination.json");
            string url = string.Format(SFConfiguration.GetTravelSaasReturnFlights(), GetSessionID());
            Task<ConcurrentBag<Swordfish.Businessobjects.Flights.Flight>> taskReturn = Task.Factory.StartNew(() => GetFlightListFromService(searchModel, url));

            string url1 = string.Format(SFConfiguration.GetTravelSaasOneWayFlights(), GetSessionID());
            Task<ConcurrentBag<Flights.Flight >> taskOneWay = Task.Factory.StartNew(() => GetFlightListFromService(searchModel, url1));

            Task<List<FlightModel>> taskflightReturnList = taskReturn.ContinueWith((Task<ConcurrentBag<Flights.Flight>> antecedent) => GetRTNFlights(searchModel, antecedent.Result, srcJsonPath, destJsonPath));
            Task<List<FlightModel>> taskflightOutList = taskOneWay.ContinueWith((Task<ConcurrentBag<Flights.Flight>> antecedent) => GetOBFlights(searchModel, antecedent.Result, srcJsonPath, destJsonPath));
            Task<List<FlightModel>> taskflightInlist = taskOneWay.ContinueWith((Task<ConcurrentBag<Flights.Flight>> antecedent) => GetIBFlights(searchModel, antecedent.Result, srcJsonPath, destJsonPath));

            Task.WaitAll(taskflightReturnList, taskflightOutList, taskflightInlist);

            var flightDetails = new FlightDetails();
            flightDetails.ReturnFlights = taskflightReturnList.Result;
            flightDetails.OutBoundFlights = taskflightOutList.Result;
            flightDetails.InBoundFlights = taskflightInlist.Result;

            var searchAirLines = Task.Factory.StartNew(() => GetAirlinersWithMinPrice(flightDetails));
            var test = new List<SearchAirline>();
            Task<List<SearchAirline>> tasksearchAirLines = searchAirLines.ContinueWith((Task<List<SearchAirline>> antecedent) =>
            {
                if (antecedent.Result != null && antecedent.Result.Count > 0)
                    test = antecedent.Result.GroupBy(x => x.value).Select(x => x.OrderBy(y => y.price)).Select(x => x.First()).ToList();
                return test;
            });

            var searchDeptAirLines = Task.Factory.StartNew(() => GetDeptAirlinersWithMinPrice(flightDetails));
            var test1 = new List<SearchDeptAirline>();
            Task<List<SearchDeptAirline>> tasksearchDeptAirLines = searchDeptAirLines.ContinueWith((Task<List<SearchDeptAirline>> antecedent) =>
            {
                if (antecedent.Result != null && antecedent.Result.Count > 0)
                    test1 = antecedent.Result.GroupBy(x => x.value).Select(x => x.OrderBy(y => y.price)).Select(x => x.First()).ToList();
                return test1;
            });
            Task.WaitAll(searchAirLines, searchDeptAirLines);

            flightDetails.SearchAirlines = tasksearchAirLines.Result;
            flightDetails.SearchDeptAirlines = tasksearchDeptAirLines.Result;
            flightDetails.OutBoundDayAirlines = GetDayAirlineFilters();
            flightDetails.InBoundDayAirlines = GetDayAirlineFilters();
            SFLogger.logMessage("SearchResultsHelper:GetFlights(): Total number of return flights " + flightDetails.ReturnFlights.Count);
            SFLogger.logMessage("SearchResultsHelper:GetFlights(): Total number of outbound flights " + flightDetails.OutBoundFlights.Count);
            SFLogger.logMessage("SearchResultsHelper:GetFlights(): Total number of inbound flights " + flightDetails.InBoundFlights.Count);
            SFLogger.logMessage("SearchResultsHelper:GetFlights(): end!!.", "GetFlights", JsonConvert.SerializeObject(flightDetails));
            return flightDetails;
        }

        public static FlightDetails GetOneWayFlights(SearchModel searchModel)
        {
            SFLogger.logMessage("SearchResultsHelper:GetOneWayFlights(): started!!.", "GetOneWayFlights", JsonConvert.SerializeObject(searchModel));
            var srcJsonPath = HttpContext.Current.Server.MapPath("~/JsonData/Source.json");
            var destJsonPath = HttpContext.Current.Server.MapPath("~/JsonData/destination.json");
            string url1 = string.Format(SFConfiguration.GetTravelSaasOneWayFlights(), GetSessionID());
            Task<ConcurrentBag<Flights.Flight>> taskOneWay = Task.Factory.StartNew(() => GetFlightListFromService(searchModel, url1));
            Task<List<FlightModel>> taskflightOutList = taskOneWay.ContinueWith((Task<ConcurrentBag<Flights.Flight>> antecedent) => GetOBFlights(searchModel, antecedent.Result, srcJsonPath, destJsonPath));
            Task<List<FlightModel>> taskflightInlist = taskOneWay.ContinueWith((Task<ConcurrentBag<Flights.Flight>> antecedent) => GetIBFlights(searchModel, antecedent.Result, srcJsonPath, destJsonPath));

            Task.WaitAll(taskflightOutList, taskflightInlist);

            var flightDetails = new FlightDetails();
            flightDetails.OutBoundFlights = taskflightOutList.Result;
            flightDetails.InBoundFlights = taskflightInlist.Result;

            var searchAirLines = Task.Factory.StartNew(() => GetAirlinersWithMinPrice(flightDetails));
            var test = new List<SearchAirline>();
            Task<List<SearchAirline>> tasksearchAirLines = searchAirLines.ContinueWith((Task<List<SearchAirline>> antecedent) =>
            {
                if (antecedent.Result != null && antecedent.Result.Count > 0)
                    test = antecedent.Result.GroupBy(x => x.value).Select(x => x.OrderBy(y => y.price)).Select(x => x.First()).ToList();
                return test;
            });

            var searchDeptAirLines = Task.Factory.StartNew(() => GetDeptAirlinersWithMinPrice(flightDetails));
            var test1 = new List<SearchDeptAirline>();
            Task<List<SearchDeptAirline>> tasksearchDeptAirLines = searchDeptAirLines.ContinueWith((Task<List<SearchDeptAirline>> antecedent) =>
            {
                if (antecedent.Result != null && antecedent.Result.Count > 0)
                    test1 = antecedent.Result.GroupBy(x => x.value).Select(x => x.OrderBy(y => y.price)).Select(x => x.First()).ToList();
                return test1;
            });
            Task.WaitAll(searchAirLines, searchDeptAirLines);

            flightDetails.SearchAirlines = tasksearchAirLines.Result;
            flightDetails.SearchDeptAirlines = tasksearchDeptAirLines.Result;
            flightDetails.OutBoundDayAirlines = GetDayAirlineFilters();
            flightDetails.InBoundDayAirlines = GetDayAirlineFilters();
            SFLogger.logMessage("SearchResultsHelper:GetOneWayFlights(): Total number of outbound flights " + flightDetails.OutBoundFlights.Count);
            SFLogger.logMessage("SearchResultsHelper:GetOneWayFlights(): Total number of inbound flights " + flightDetails.InBoundFlights.Count);
            SFLogger.logMessage("SearchResultsHelper:GetOneWayFlights(): end!!.", "GetOneWayFlights", JsonConvert.SerializeObject(flightDetails));

            return flightDetails;
        }
        public static FlightDetails GetReturnFlights(SearchModel searchModel)
        {
            SFLogger.logMessage("SearchResultsHelper:GetReturnFlights(): started!!.", "GetReturnFlights", JsonConvert.SerializeObject(searchModel));
            var srcJsonPath = HttpContext.Current.Server.MapPath("~/JsonData/Source.json");
            var destJsonPath = HttpContext.Current.Server.MapPath("~/JsonData/destination.json");
            string url1 = string.Format(SFConfiguration.GetTravelSaasReturnFlights(), GetSessionID());
            Task<ConcurrentBag<Flights.Flight>> taskReturn = Task.Factory.StartNew(() => GetFlightListFromService(searchModel, url1));
            Task<List<FlightModel>> taskflightReturnList = taskReturn.ContinueWith((Task<ConcurrentBag<Flights.Flight>> antecedent) => GetRTNFlights(searchModel, antecedent.Result, srcJsonPath, destJsonPath));

            Task.WaitAll(taskflightReturnList);

            var flightDetails = new FlightDetails();
            flightDetails.ReturnFlights = taskflightReturnList.Result;
            var searchAirLines = Task.Factory.StartNew(() => GetAirlinersWithMinPrice(flightDetails));
            var test = new List<SearchAirline>();
            Task<List<SearchAirline>> tasksearchAirLines = searchAirLines.ContinueWith((Task<List<SearchAirline>> antecedent) =>
            {
                if (antecedent.Result != null && antecedent.Result.Count > 0)
                    test = antecedent.Result.GroupBy(x => x.value).Select(x => x.OrderBy(y => y.price)).Select(x => x.First()).ToList();
                return test;
            });

            var searchDeptAirLines = Task.Factory.StartNew(() => GetDeptAirlinersWithMinPrice(flightDetails));
            var test1 = new List<SearchDeptAirline>();
            Task<List<SearchDeptAirline>> tasksearchDeptAirLines = searchDeptAirLines.ContinueWith((Task<List<SearchDeptAirline>> antecedent) =>
            {
                if (antecedent.Result != null && antecedent.Result.Count > 0)
                    test1 = antecedent.Result.GroupBy(x => x.value).Select(x => x.OrderBy(y => y.price)).Select(x => x.First()).ToList();
                return test1;
            });
            Task.WaitAll(searchAirLines, searchDeptAirLines);

            flightDetails.SearchAirlines = tasksearchAirLines.Result;
            flightDetails.SearchDeptAirlines = tasksearchDeptAirLines.Result;
            SFLogger.logMessage("SearchResultsHelper:GetReturnFlights(): Total number of return flights " + flightDetails.ReturnFlights.Count);
            SFLogger.logMessage("SearchResultsHelper:GetReturnFlights(): end!!.", "GetReturnFlights", JsonConvert.SerializeObject(flightDetails));
            return flightDetails;
        }
        private static List<SearchAirline> GetAirlinersWithMinPrice(FlightDetails flightDetails)
        {
            SFLogger.logMessage("SearchResultsHelper:GetAirlinersWithMinPrice(): started!!.");
            var searchAirLines = new List<SearchAirline>();
            if (flightDetails != null)
            {
                var searchOBAirLines = new List<SearchAirline>();
                var searchIBAirLines = new List<SearchAirline>();
                var searchRTAirLines = new List<SearchAirline>();
                if (flightDetails.OutBoundFlights != null && flightDetails.OutBoundFlights.Count > 0)
                {
                    var outBoundAirlines = flightDetails.OutBoundFlights.Select(f => f.AirLaneName).Distinct();

                    if (outBoundAirlines != null && outBoundAirlines.Count() > 0)
                    {
                        foreach (var outBoundAirline in outBoundAirlines)
                        {
                            if (!string.IsNullOrEmpty(outBoundAirline))
                            {
                                var searchAirLine = new SearchAirline();
                                searchAirLine.value = outBoundAirline;
                                searchAirLine.price = (flightDetails.OutBoundFlights.Where(o => o.AirLaneName != null && o.AirLaneName.Equals(outBoundAirline))
                                    .Min(o => o.PricePerPerson)).ToString();
                                searchAirLine.selected = false;
                                searchOBAirLines.Add(searchAirLine);
                            }
                        }
                        if (searchOBAirLines != null && searchOBAirLines.Count > 0)
                            searchAirLines.AddRange(searchOBAirLines);
                    }
                }

                if (flightDetails.InBoundFlights != null && flightDetails.InBoundFlights.Count > 0)
                {
                    var inBoundAirlines = flightDetails.InBoundFlights.Select(f => f.AirLaneName).Distinct();
                    if (inBoundAirlines != null && inBoundAirlines.Count() > 0)
                    {
                        foreach (var inBoundAirline in inBoundAirlines)
                        {
                            if (!string.IsNullOrEmpty(inBoundAirline))
                            {
                                var searchAirLine = new SearchAirline();
                                searchAirLine.value = inBoundAirline;
                                searchAirLine.price = (flightDetails.InBoundFlights.Where(o => o.AirLaneName != null && o.AirLaneName.Equals(inBoundAirline))
                                    .Min(o => o.PricePerPerson)).ToString();
                                searchAirLine.selected = false;
                                searchIBAirLines.Add(searchAirLine);
                            }
                        }
                        if (searchIBAirLines != null && searchIBAirLines.Count > 0)
                            searchAirLines.AddRange(searchIBAirLines);
                    }
                }
                //returen
               if (flightDetails.ReturnFlights != null && flightDetails.ReturnFlights.Count > 0)
                {
                    var returnAirlines = flightDetails.ReturnFlights.Select(f => f.AirLaneName).Distinct();
                    {
                        if (returnAirlines != null && returnAirlines.Count() > 0)
                        {
                            foreach (var returnAirline in returnAirlines)
                            {
                                if (!String.IsNullOrEmpty(returnAirline))
                                {
                                    var searchAirLine = new SearchAirline();
                                    searchAirLine.value = returnAirline;
                                    searchAirLine.price = (flightDetails.ReturnFlights.Where(o => o.AirLaneName != null && o.AirLaneName.Equals(returnAirline))
                                        .Min(o => o.PricePerPerson)).ToString();
                                    searchAirLine.selected = false;
                                    searchRTAirLines.Add(searchAirLine);
                                }
                            }
                            if (searchRTAirLines != null && searchRTAirLines.Count > 0)
                            {
                                searchAirLines.AddRange(searchRTAirLines);
                            }
                        }
                    }
                }
            if (searchAirLines != null && searchAirLines.Count > 0)
                    searchAirLines = searchAirLines.GroupBy(x => x.value).Select(x => x.OrderBy(y => y.price))
                        .Select(x => x.First()).ToList();
            }
            SFLogger.logMessage("SearchResultsHelper:GetAirlinersWithMinPrice():end.");
            return searchAirLines;
        }
        private static List<SearchDeptAirline> GetDeptAirlinersWithMinPrice(FlightDetails flightDetails)
        {
            SFLogger.logMessage("SearchResultsHelper:GetDeptAirlinersWithMinPrice(): started!!.");
            var searchDeptAirLines = new List<SearchDeptAirline>();
            if (flightDetails != null)
            {
                var searchOBDeptAirLines = new List<SearchDeptAirline>();
                var searchIBDeptAirLines = new List<SearchDeptAirline>();
                var searchRTDeptAirLines = new List<SearchDeptAirline>();
                if (flightDetails.OutBoundFlights != null && flightDetails.OutBoundFlights.Count > 0)
                {
                    var outBoundDeptAirlines = flightDetails.OutBoundFlights.Select(f => f.OutboundFlightFrom).Distinct();
                    if (outBoundDeptAirlines != null && outBoundDeptAirlines.Count() > 0)
                    {
                        foreach (var outBoundAirline in outBoundDeptAirlines)
                        {
                            if (!string.IsNullOrEmpty(outBoundAirline))
                            {
                                var searchDeptAirLine = new SearchDeptAirline();
                                searchDeptAirLine.value = outBoundAirline;
                                searchDeptAirLine.price = (flightDetails.OutBoundFlights.Where(o => o.OutboundFlightFrom != null
                                && o.OutboundFlightFrom.Equals(outBoundAirline)).Min(o => o.PricePerPerson)).ToString();
                                searchDeptAirLine.selected = false;
                                searchOBDeptAirLines.Add(searchDeptAirLine);
                            }
                        }
                        if (searchOBDeptAirLines != null && searchOBDeptAirLines.Count > 0)
                            searchDeptAirLines.AddRange(searchOBDeptAirLines);
                    }
                }

                if (flightDetails.InBoundFlights != null && flightDetails.InBoundFlights.Count > 0)
                {
                    var inBoundAirlines = flightDetails.InBoundFlights.Select(f => f.InboundFlightTo).Distinct();
                    if (inBoundAirlines != null && inBoundAirlines.Count() > 0)
                    {
                        foreach (var inBoundAirline in inBoundAirlines)
                        {
                            if (!string.IsNullOrEmpty(inBoundAirline))
                            {
                                var searchDeptAirLine = new SearchDeptAirline();
                                searchDeptAirLine.value = inBoundAirline;
                                searchDeptAirLine.price = (flightDetails.InBoundFlights.Where(o => o.InboundFlightTo != null && o.InboundFlightTo.Equals(inBoundAirline))
                                    .Min(o => o.PricePerPerson)).ToString();
                                searchDeptAirLine.selected = false;
                                searchIBDeptAirLines.Add(searchDeptAirLine);
                            }
                        }
                        if (searchIBDeptAirLines != null && searchIBDeptAirLines.Count > 0)
                            searchDeptAirLines.AddRange(searchIBDeptAirLines);
                    }
                }
                // For return flights
                if (flightDetails.ReturnFlights != null && flightDetails.ReturnFlights.Count > 0)
                {
                    var rTAirlines = flightDetails.ReturnFlights.Select(f => f.OutboundFlightFrom).Distinct();
                    if (rTAirlines != null && rTAirlines.Count() > 0)
                    {
                        foreach (var rTAirline in rTAirlines)
                        {
                            if (!string.IsNullOrEmpty(rTAirline))
                            {
                                var searchDeptAirLine = new SearchDeptAirline();
                                searchDeptAirLine.value = rTAirline;
                                searchDeptAirLine.price = (flightDetails.ReturnFlights.Where(o => o.InboundFlightTo != null && o.InboundFlightTo.Equals(rTAirline))
                                    .Min(o => o.PricePerPerson)).ToString();
                                searchDeptAirLine.selected = false;
                                searchRTDeptAirLines.Add(searchDeptAirLine);
                            }
                        }
                        if (searchRTDeptAirLines != null && searchRTDeptAirLines.Count > 0)
                            searchDeptAirLines.AddRange(searchRTDeptAirLines);
                    }
                }
                if (searchDeptAirLines != null && searchDeptAirLines.Count > 0)
                    searchDeptAirLines = searchDeptAirLines.GroupBy(x => x.value).Select(x => x.OrderBy(y => y.price))
                        .Select(x => x.First()).ToList();
            }
            SFLogger.logMessage("SearchResultsHelper:GetDeptAirlinersWithMinPrice(): end.");
            return searchDeptAirLines;
        }
        private static List<SearchDayAirline> GetDayAirlineFilters()
        {
            SFLogger.logMessage("SearchResultsHelper:GetDayAirlineFilters(): started!!.");
            var searchDayAirlines = new List<SearchDayAirline>();

            var mSearchDay = new SearchDayAirline();
            mSearchDay.day = "Morning";
            mSearchDay.minvalue = SFConfiguration.GetFlightFilterMorningTime().Split('-')[0];
            mSearchDay.maxvalue = SFConfiguration.GetFlightFilterMorningTime().Split('-')[1];
            mSearchDay.selected = false;
            searchDayAirlines.Add(mSearchDay);

            var aSearchDay = new SearchDayAirline();
            aSearchDay.day = "Afternoon";
            aSearchDay.minvalue = SFConfiguration.GetFlightFilterAfternoonTime().Split('-')[0];
            aSearchDay.maxvalue = SFConfiguration.GetFlightFilterAfternoonTime().Split('-')[1];
            aSearchDay.selected = false;
            searchDayAirlines.Add(aSearchDay);

            var eSearchDay = new SearchDayAirline();
            eSearchDay.day = "Evening";
            eSearchDay.minvalue = SFConfiguration.GetFlightFilterEveningTime().Split('-')[0];
            eSearchDay.maxvalue = SFConfiguration.GetFlightFilterEveningTime().Split('-')[1];
            eSearchDay.selected = false;
            searchDayAirlines.Add(eSearchDay);

            var lSearchDay = new SearchDayAirline();
            lSearchDay.day = "Late Night";
            lSearchDay.minvalue = SFConfiguration.GetFlightFilterLateNightTime().Split('-')[0];
            lSearchDay.maxvalue = SFConfiguration.GetFlightFilterLateNightTime().ToString().Split('-')[1];
            lSearchDay.selected = false;
            searchDayAirlines.Add(lSearchDay);
            SFLogger.logMessage("SearchResultsHelper:GetDayAirlineFilters(): end.");
            return searchDayAirlines;
        }
        private static List<FlightModel> GetRTNFlights(SearchModel searchModel, ConcurrentBag<Flights.Flight> lstflight, string srcJsonPath, string destJsonPath)
        {
            SFLogger.logMessage("SearchResultsHelper:GetRTNFlights(): started!!.");
            var flightList = new List<FlightModel>();
            if (lstflight != null)
            {
                if (lstflight.Count > 0)
                {
                    foreach (var flightDetail in lstflight)
                    {
                        var flightModelObj = new FlightModel();
                        flightModelObj.FlightFare = 0;
                        flightModelObj.PricePerPerson = (Convert.ToDecimal(flightDetail.AirItineraryPricingInfo.FirstOrDefault().TotalFareAmount) / 100);
                        flightModelObj.AirlineSrc = "/Content/Assets/americanairlines.gif";
                        if (flightDetail.AirItinerary.Count > 1)
                        {
                            var outBoundAirItineraries = flightDetail.AirItinerary.Where(a => a.JourneyIndicator.Equals("1")).ToList();
                            var inBoundAirItineraries = flightDetail.AirItinerary.Where(a => a.JourneyIndicator.Equals("2")).ToList();

                            if (outBoundAirItineraries != null)
                            {
                                var obFlightFirst = outBoundAirItineraries.FirstOrDefault();
                                if (obFlightFirst != null)
                                {
                                    flightModelObj.OutBoundFlightNumber = obFlightFirst.FlightNumber;
                                    flightModelObj.OutBoundFlightName = obFlightFirst.MarketingAirline;
                                    flightModelObj.OutBoundFlightCode = obFlightFirst.MarketingAirlineCode;
                                    flightModelObj.OutboundFlightFrom = obFlightFirst.DepartureAirportLocationCode;
                                    flightModelObj.OutboundFlightFromName = SFConfiguration.GetAirportName(flightModelObj.OutboundFlightFrom, destJsonPath, srcJsonPath);
                                    flightModelObj.OutboundDepartureTime = obFlightFirst.DepartureDateTime.ToString();
                                    flightModelObj.OutboundDepartureTimeV = Convert.ToDateTime(obFlightFirst.DepartureDateTime).ToUniversalTime().ToString("ddd, dd/MM/yyyy HH:mm");
                                }
                                var obFlightLast = outBoundAirItineraries.LastOrDefault();
                                if (obFlightLast != null)
                                {
                                    flightModelObj.OutboundFlightTo = obFlightLast.ArrivalAirportLocationCode;
                                    flightModelObj.OutboundFlightToName = SFConfiguration.GetAirportName(flightModelObj.OutboundFlightTo, destJsonPath, srcJsonPath);
                                    flightModelObj.OutboundArrivalTime = obFlightLast.ArrivalDateTime;
                                    flightModelObj.OutboundArrivalTimeV = Convert.ToDateTime(obFlightLast.ArrivalDateTime).ToUniversalTime().ToString("ddd, dd/MM/yyyy HH:mm");
                                    var estHours = (Convert.ToDateTime(flightModelObj.OutboundArrivalTime) - Convert.ToDateTime(flightModelObj.OutboundDepartureTime));
                                    flightModelObj.OutBoundEstimatedHours = estHours.Hours.ToString("00") + ":" + estHours.Minutes.ToString("00");
                                }
                                flightModelObj.OBJourneyIndicator = obFlightFirst.JourneyIndicator;
                            }

                            if (inBoundAirItineraries != null)
                            {
                                var ibFlightFirst = inBoundAirItineraries.FirstOrDefault();
                                if (ibFlightFirst != null)
                                {
                                    flightModelObj.InBoundFlightNumber = ibFlightFirst.FlightNumber;
                                    flightModelObj.InBoundFlightName = ibFlightFirst.MarketingAirline;
                                    flightModelObj.InBoundFlightCode = ibFlightFirst.MarketingAirlineCode;
                                    flightModelObj.InboundFlightFrom = ibFlightFirst.DepartureAirportLocationCode;
                                    flightModelObj.InboundFlightFromName = SFConfiguration.GetAirportName(flightModelObj.InboundFlightFrom, destJsonPath, srcJsonPath);
                                    flightModelObj.InboundDepartureTime = ibFlightFirst.DepartureDateTime.ToString();
                                    flightModelObj.InboundDepartureTimeV = Convert.ToDateTime(ibFlightFirst.DepartureDateTime).ToUniversalTime().ToString("ddd, dd/MM/yyyy HH:mm");
                                }
                                var ibFlightLast = inBoundAirItineraries.LastOrDefault();
                                if (ibFlightLast != null)
                                {
                                    flightModelObj.InboundFlightTo = ibFlightLast.ArrivalAirportLocationCode;
                                    flightModelObj.InboundFlightToName = SFConfiguration.GetAirportName(flightModelObj.InboundFlightTo, destJsonPath, srcJsonPath);
                                    flightModelObj.InboundArrivalTime = ibFlightLast.ArrivalDateTime;
                                    flightModelObj.InboundArrivalTimeV = Convert.ToDateTime(ibFlightLast.ArrivalDateTime).ToUniversalTime().ToString("ddd, dd/MM/yyyy HH:mm");
                                    var estHours = (Convert.ToDateTime(flightModelObj.InboundArrivalTime) - Convert.ToDateTime(flightModelObj.InboundDepartureTime));
                                    flightModelObj.InboundEstimatedHours = estHours.Hours.ToString("00") + ":" + estHours.Minutes.ToString("00");
                                }
                                flightModelObj.IBJourneyIndicator = ibFlightFirst.JourneyIndicator;
                            }

                            //Logic to get OutBound AirItineraries.
                            if (outBoundAirItineraries != null && outBoundAirItineraries.Count > 1)
                            {
                                var outBoundStopList = new List<FlightStopPoints>();
                                var totalDuration = new TimeSpan();
                                foreach (var outBoundAirItinerary in outBoundAirItineraries)
                                {
                                    var flightStopPoint = new FlightStopPoints();
                                    flightStopPoint.DepartureDateTime = outBoundAirItinerary.DepartureDateTime;
                                    flightStopPoint.DepartureDateTimeV = Convert.ToDateTime(outBoundAirItinerary.DepartureDateTime).ToUniversalTime().ToString("ddd, dd/MM/yyyy HH:mm");
                                    flightStopPoint.StopAirportCode = outBoundAirItinerary.DepartureAirportLocationCode;
                                    flightStopPoint.StopAirportName = SFConfiguration.GetAirportName(flightStopPoint.StopAirportCode, destJsonPath, srcJsonPath);
                                    flightStopPoint.NextStopAirportCode = outBoundAirItinerary.ArrivalAirportLocationCode;
                                    flightStopPoint.NextStopAirportName = SFConfiguration.GetAirportName(flightStopPoint.NextStopAirportCode, destJsonPath, srcJsonPath);
                                    flightStopPoint.NextStopArrrivalDateTime = outBoundAirItinerary.ArrivalDateTime;
                                    flightStopPoint.NextStopArrrivalDateTimeV = Convert.ToDateTime(outBoundAirItinerary.ArrivalDateTime).ToUniversalTime().ToString("ddd, dd/MM/yyyy HH:mm");
                                    var depDateTime = (Convert.ToDateTime(flightStopPoint.NextStopArrrivalDateTime) - Convert.ToDateTime(flightStopPoint.DepartureDateTime));
                                    totalDuration += depDateTime;
                                    flightStopPoint.Duration = depDateTime.Hours.ToString("00") + ":" + depDateTime.Minutes.ToString("00") + " h";
                                    flightStopPoint.FlightNumber = outBoundAirItinerary.FlightNumber;
                                    flightStopPoint.FlightName = outBoundAirItinerary.MarketingAirline;
                                    flightStopPoint.FlightCode = outBoundAirItinerary.MarketingAirlineCode;
                                    if (outBoundStopList != null && outBoundStopList.Count > 0)
                                    {
                                        var lastOutBoungStop = outBoundStopList.Last();
                                        if (lastOutBoungStop != null)
                                        {
                                            var longWaitDateTime = (Convert.ToDateTime(flightStopPoint.DepartureDateTime) - Convert.ToDateTime(lastOutBoungStop.NextStopArrrivalDateTime));
                                            totalDuration = totalDuration + longWaitDateTime;
                                            flightStopPoint.LongWait = longWaitDateTime.Hours.ToString("00") + ":" + longWaitDateTime.Minutes.ToString("00") + " h";
                                            lastOutBoungStop.LongWait = flightStopPoint.LongWait;
                                        }
                                    }
                                    flightStopPoint.JourneyIndicator = outBoundAirItinerary.JourneyIndicator;

                                    outBoundStopList.Add(flightStopPoint);
                                }
                                flightModelObj.OutBoundJourneyDuration = totalDuration.Hours.ToString("00") + ":" + totalDuration.Minutes.ToString("00") + " h";
                                flightModelObj.OutBoundStopPoints = outBoundStopList;
                                flightModelObj.OutBoundStopsCount = outBoundAirItineraries.Count - 1;
                                flightModelObj.OutBoundFlightWay = flightModelObj.OutBoundStopsCount.ToString() + " Stop";
                            }
                            else
                            {
                                flightModelObj.OutBoundStopsCount = 0;
                                flightModelObj.OutBoundFlightWay = "Direct";

                            }
                            //Logic to get InBound AirItineraries.
                            if (inBoundAirItineraries != null && inBoundAirItineraries.Count > 1)
                            {
                                var inBoundStopList = new List<FlightStopPoints>();
                                var totalDuration = new TimeSpan();
                                foreach (var item in inBoundAirItineraries)
                                {
                                    var flightStopPoint = new FlightStopPoints();
                                    flightStopPoint.DepartureDateTime = item.DepartureDateTime;
                                    flightStopPoint.DepartureDateTimeV = Convert.ToDateTime(item.DepartureDateTime).ToUniversalTime().ToString("ddd, dd/MM/yyyy HH:mm");
                                    flightStopPoint.StopAirportCode = item.DepartureAirportLocationCode;
                                    flightStopPoint.StopAirportName = SFConfiguration.GetAirportName(flightStopPoint.StopAirportCode, destJsonPath, srcJsonPath);
                                    flightStopPoint.NextStopAirportCode = item.ArrivalAirportLocationCode;
                                    flightStopPoint.NextStopAirportName = SFConfiguration.GetAirportName(flightStopPoint.NextStopAirportCode, destJsonPath, srcJsonPath);
                                    flightStopPoint.NextStopArrrivalDateTime = item.ArrivalDateTime;
                                    flightStopPoint.NextStopArrrivalDateTimeV = Convert.ToDateTime(item.ArrivalDateTime).ToUniversalTime().ToString("ddd, dd/MM/yyyy HH:mm");
                                    var depDateTime = (Convert.ToDateTime(flightStopPoint.NextStopArrrivalDateTime) - Convert.ToDateTime(flightStopPoint.DepartureDateTime));
                                    totalDuration += depDateTime;
                                    flightStopPoint.Duration = depDateTime.Hours.ToString("00") + ":" + depDateTime.Minutes.ToString("00") + " h";
                                    flightStopPoint.FlightNumber = item.FlightNumber;
                                    flightStopPoint.FlightName = item.MarketingAirline;
                                    flightStopPoint.FlightCode = item.MarketingAirlineCode;
                                    if (inBoundStopList != null && inBoundStopList.Count > 0)
                                    {
                                        var lastInBoungStop = inBoundStopList.Last();
                                        if (lastInBoungStop != null)
                                        {
                                            var longWaitDateTime = (Convert.ToDateTime(flightStopPoint.DepartureDateTime) - Convert.ToDateTime(lastInBoungStop.NextStopArrrivalDateTime));
                                            totalDuration = totalDuration + longWaitDateTime;
                                            flightStopPoint.LongWait = longWaitDateTime.Hours.ToString("00") + ":" + longWaitDateTime.Minutes.ToString("00") + " h";
                                            lastInBoungStop.LongWait = flightStopPoint.LongWait;
                                        }
                                    }
                                    flightStopPoint.JourneyIndicator = item.JourneyIndicator;
                                    inBoundStopList.Add(flightStopPoint);
                                }
                                flightModelObj.InBoundJourneyDuration = totalDuration.Hours.ToString("00") + ":" + totalDuration.Minutes.ToString("00") + " h";
                                flightModelObj.InBoundStopPoints = inBoundStopList;
                                flightModelObj.InBoundStopsCount = inBoundAirItineraries.Count - 1;
                                flightModelObj.InboundFlightWay = flightModelObj.InBoundStopsCount.ToString() + " Stop";
                            }
                            else
                            {
                                flightModelObj.InBoundStopsCount = 0;
                                flightModelObj.InboundFlightWay = "Direct";
                            }
                        }
                        else
                        {
                            flightModelObj.OutboundFlightFrom = (from air in flightDetail.AirItinerary
                                                                 select air.DepartureAirportLocationCode).FirstOrDefault();
                            flightModelObj.OutboundDepartureTime = (from air in flightDetail.AirItinerary
                                                                    select air.DepartureDateTime).FirstOrDefault().ToString();
                            flightModelObj.OutboundDepartureTimeV = Convert.ToDateTime(flightModelObj.OutboundDepartureTime).ToUniversalTime().ToString("ddd, dd/MM/yyyy HH:mm");
                            flightModelObj.OutboundFlightTo = (from air in flightDetail.AirItinerary
                                                               select air.ArrivalAirportLocationCode).FirstOrDefault();
                            flightModelObj.OutboundArrivalTime = (from air in flightDetail.AirItinerary
                                                                  select air.ArrivalDateTime).FirstOrDefault().ToString();
                            flightModelObj.OutboundArrivalTimeV = Convert.ToDateTime(flightModelObj.OutboundArrivalTime).ToUniversalTime().ToString("ddd, dd/MM/yyyy HH:mm");
                            var estHours = (Convert.ToDateTime(flightModelObj.OutboundArrivalTime) - Convert.ToDateTime(flightModelObj.OutboundDepartureTime));
                            flightModelObj.OutBoundEstimatedHours = estHours.Hours.ToString("00") + ":" + estHours.Minutes.ToString("00");
                            flightModelObj.OutBoundFlightName = (from air in flightDetail.AirItinerary
                                                                 select air.MarketingAirline).FirstOrDefault();
                            flightModelObj.OutBoundFlightCode = (from air in flightDetail.AirItinerary
                                                                 select air.MarketingAirlineCode).FirstOrDefault();
                        }
                        flightModelObj.IsRTNPriceUpdated = 0;
                        flightModelObj.FlightQuoteId = (from air in flightDetail.AirItineraryPricingInfo
                                                        select air.QuoteId).FirstOrDefault();
                        flightModelObj.AirLaneName = (from air in flightDetail.AirItinerary
                                                      select air.MarketingAirline).FirstOrDefault();
                        flightModelObj.CurrencyCode = (from air in flightDetail.AirItineraryPricingInfo
                                                       select air.TotalFareCurrencyCode).FirstOrDefault();
                        flightModelObj.NoOfAdults = searchModel.Adults;
                        flightModelObj.NoOfChilds = searchModel.Child;
                        flightModelObj.NoOfInfants = searchModel.Infants;
                        flightModelObj.NoOfBags = searchModel.Bags;
                        flightModelObj.QuoteReference = PackageViewModel.GetUniqueKey(12).ToUpper();
                        flightModelObj.SourceUrl = flightDetail.SourceURL;
                        flightList.Add(flightModelObj);
                    }
                }
            }
            SFLogger.logMessage("SearchResultsHelper:GetRTNFlights(): end.");
            List<FlightModel> result = null;
            if (SFConfiguration.GetNearByAirports().Equals("false"))
            {
                result = flightList.Where(d => searchModel.SourceFromId.Contains(d.OutboundFlightFrom)).Where(x=>searchModel.DestinationToId.Contains(x.InboundFlightFrom)).ToList();
                result.AddRange(flightList.Where(d => searchModel.SourceFromId.Contains(d.OutboundFlightFrom)).TakeWhile(x => x.OutBoundStopPoints != null).TakeWhile(x => x.OutBoundStopPoints.Count > 0).
                    Where(d => d.OutBoundStopPoints[d.OutBoundStopPoints.Count - 1].NextStopAirportCode.Equals(searchModel.DestinationToId)).ToList());
            }
            else
            {
                result = flightList;
            }
            return result.OrderBy(x => x.PricePerPerson).ToList();
        }
        private static List<FlightModel> GetIBFlights(SearchModel searchModel, ConcurrentBag<Flights.Flight> lstflight, string srcJsonPath, string destJsonPath)
        {
            SFLogger.logMessage("SearchResultsHelper:GetIBFlights(): started!!.");
            var flightList = new List<FlightModel>();
            if (lstflight != null)
            {
                if (lstflight.Count > 0)
                {
                    foreach (var flightDetail in lstflight)
                    {
                        if (flightDetail.AirItinerary.Count > 0 && flightDetail.AirItinerary.FirstOrDefault().FlightType.Equals("In"))
                        {
                            var flightModelObj = new FlightModel();
                            flightModelObj.FlightFare = 0;
                            flightModelObj.PricePerPerson = (Convert.ToDecimal(flightDetail.AirItineraryPricingInfo.FirstOrDefault().TotalFareAmount) / 100);
                            flightModelObj.AirlineSrc = "/Content/Assets/americanairlines.gif";
                            if (flightDetail.AirItinerary.Count > 1)
                            {
                                var inboundDepartureDetails = flightDetail.AirItinerary.FirstOrDefault();
                                var inboundArrivalDetails = flightDetail.AirItinerary.LastOrDefault();
                                if (inboundDepartureDetails != null)
                                {
                                    flightModelObj.InBoundFlightNumber = inboundDepartureDetails.FlightNumber;
                                    flightModelObj.InBoundFlightCode = inboundDepartureDetails.MarketingAirlineCode;
                                    flightModelObj.InBoundFlightName = inboundDepartureDetails.MarketingAirline;
                                    flightModelObj.InboundFlightFrom = inboundDepartureDetails.DepartureAirportLocationCode;
                                    flightModelObj.InboundFlightFromName = SFConfiguration.GetAirportName(flightModelObj.InboundFlightFrom, destJsonPath, srcJsonPath);
                                    flightModelObj.InboundDepartureTime = inboundDepartureDetails.DepartureDateTime;
                                }
                                if (inboundArrivalDetails != null)
                                {
                                    flightModelObj.InboundFlightTo = inboundArrivalDetails.ArrivalAirportLocationCode;
                                    flightModelObj.InboundFlightToName = SFConfiguration.GetAirportName(flightModelObj.InboundFlightTo, destJsonPath, srcJsonPath);
                                    flightModelObj.InboundArrivalTime = inboundArrivalDetails.ArrivalDateTime;
                                    var estHours = (Convert.ToDateTime(flightModelObj.InboundArrivalTime) - Convert.ToDateTime(flightModelObj.InboundDepartureTime));
                                    flightModelObj.InboundEstimatedHours = estHours.Hours.ToString("00") + ":" + estHours.Minutes.ToString("00");
                                    flightModelObj.InboundDepartureTimeV = Convert.ToDateTime(inboundDepartureDetails.DepartureDateTime).ToUniversalTime().ToString("ddd, dd/MM/yyyy HH:mm");
                                    flightModelObj.InboundArrivalTimeV = Convert.ToDateTime(inboundArrivalDetails.ArrivalDateTime).ToUniversalTime().ToString("ddd, dd/MM/yyyy HH:mm");
                                }
                                var inBoundStopList = new List<FlightStopPoints>();
                                var totalDuration = new TimeSpan();
                                foreach (var item in flightDetail.AirItinerary)
                                {
                                    var flightStopPoint = new FlightStopPoints();
                                    flightStopPoint.DepartureDateTime = item.DepartureDateTime;
                                    flightStopPoint.StopAirportCode = item.DepartureAirportLocationCode;
                                    flightStopPoint.StopAirportName = SFConfiguration.GetAirportName(flightStopPoint.StopAirportCode, destJsonPath, srcJsonPath);
                                    flightStopPoint.NextStopAirportCode = item.ArrivalAirportLocationCode;
                                    flightStopPoint.NextStopAirportName = SFConfiguration.GetAirportName(flightStopPoint.NextStopAirportCode, destJsonPath, srcJsonPath);
                                    flightStopPoint.NextStopArrrivalDateTime = item.ArrivalDateTime;
                                    var depDateTime = (Convert.ToDateTime(flightStopPoint.NextStopArrrivalDateTime) - Convert.ToDateTime(flightStopPoint.DepartureDateTime));
                                    totalDuration += depDateTime;
                                    flightStopPoint.Duration = depDateTime.Hours.ToString("00") + ":" + depDateTime.Minutes.ToString("00") + " h";
                                    flightStopPoint.FlightNumber = item.FlightNumber;
                                    flightStopPoint.FlightName = item.MarketingAirline;
                                    flightStopPoint.FlightCode = item.MarketingAirlineCode;
                                    flightStopPoint.JourneyIndicator = item.JourneyIndicator;
                                    if (inBoundStopList != null && inBoundStopList.Count > 0)
                                    {
                                        var lastInBoungStop = inBoundStopList.Last();
                                        if (lastInBoungStop != null)
                                        {
                                            var longWaitDateTime = (Convert.ToDateTime(flightStopPoint.DepartureDateTime) - Convert.ToDateTime(lastInBoungStop.NextStopArrrivalDateTime));
                                            totalDuration = totalDuration + longWaitDateTime;
                                            flightStopPoint.LongWait = longWaitDateTime.Hours.ToString("00") + ":" + longWaitDateTime.Minutes.ToString("00") + " h";
                                            lastInBoungStop.LongWait = flightStopPoint.LongWait;
                                        }
                                    }
                                    flightStopPoint.DepartureDateTimeV = Convert.ToDateTime(item.DepartureDateTime).ToUniversalTime().ToString("ddd, dd/MM/yyyy HH:mm");
                                    flightStopPoint.NextStopArrrivalDateTimeV = Convert.ToDateTime(item.ArrivalDateTime).ToUniversalTime().ToString("ddd, dd/MM/yyyy HH:mm");
                                    inBoundStopList.Add(flightStopPoint);
                                }
                                flightModelObj.InBoundJourneyDuration = totalDuration.Hours.ToString("00") + ":" + totalDuration.Minutes.ToString("00") + " h";
                                flightModelObj.InBoundStopPoints = inBoundStopList;
                                flightModelObj.InBoundStopsCount = flightDetail.AirItinerary.Count - 1;
                                flightModelObj.InboundFlightWay = flightModelObj.InBoundStopsCount.ToString() + " Stop";
                            }
                            else
                            {
                                flightModelObj.InBoundFlightNumber = flightDetail.AirItinerary.FirstOrDefault().FlightNumber;
                                flightModelObj.InboundFlightFrom = flightDetail.AirItinerary.FirstOrDefault().DepartureAirportLocationCode;
                                flightModelObj.InBoundFlightCode = flightDetail.AirItinerary.FirstOrDefault().MarketingAirlineCode;
                                flightModelObj.InBoundFlightName = flightDetail.AirItinerary.FirstOrDefault().MarketingAirline;
                                flightModelObj.InboundFlightFromName = SFConfiguration.GetAirportName(flightModelObj.InboundFlightFrom, destJsonPath, srcJsonPath);
                                flightModelObj.InboundDepartureTime = flightDetail.AirItinerary.FirstOrDefault().DepartureDateTime;
                                flightModelObj.InboundFlightTo = flightDetail.AirItinerary.FirstOrDefault().ArrivalAirportLocationCode;
                                flightModelObj.InboundFlightToName = SFConfiguration.GetAirportName(flightModelObj.InboundFlightTo, destJsonPath, srcJsonPath);
                                flightModelObj.InboundArrivalTime = flightDetail.AirItinerary.FirstOrDefault().ArrivalDateTime;
                                var estHours = (Convert.ToDateTime(flightModelObj.InboundArrivalTime) - Convert.ToDateTime(flightModelObj.InboundDepartureTime));
                                flightModelObj.InboundEstimatedHours = estHours.Hours.ToString("00") + ":" + estHours.Minutes.ToString("00");
                                flightModelObj.InBoundStopsCount = 0;
                                flightModelObj.InboundFlightWay = "Direct";
                                flightModelObj.InboundDepartureTimeV = Convert.ToDateTime(flightDetail.AirItinerary.FirstOrDefault().DepartureDateTime).ToUniversalTime().ToString("ddd, dd/MM/yyyy HH:mm");
                                flightModelObj.InboundArrivalTimeV = Convert.ToDateTime(flightDetail.AirItinerary.FirstOrDefault().ArrivalDateTime).ToUniversalTime().ToString("ddd, dd/MM/yyyy HH:mm");
                            }
                            flightModelObj.IBJourneyIndicator = flightDetail.AirItinerary.FirstOrDefault().JourneyIndicator;
                            flightModelObj.IsIBPriceUpdated = 0;
                            flightModelObj.FlightQuoteId = flightDetail.AirItineraryPricingInfo.FirstOrDefault().QuoteId;
                            flightModelObj.AirLaneName = flightDetail.AirItinerary.FirstOrDefault().MarketingAirline;
                            flightModelObj.CurrencyCode = flightDetail.AirItineraryPricingInfo.FirstOrDefault().TotalFareCurrencyCode;
                            flightModelObj.NoOfAdults = searchModel.Adults;
                            flightModelObj.NoOfChilds = searchModel.Child;
                            flightModelObj.NoOfInfants = searchModel.Infants;
                            flightModelObj.NoOfBags = searchModel.Bags;
                            flightModelObj.QuoteReference = PackageViewModel.GetUniqueKey(12).ToUpper();
                            flightModelObj.BaseFareAmount = !string.IsNullOrEmpty(flightDetail.AirItineraryPricingInfo.FirstOrDefault().BaseFareAmount) ? 
                                        Convert.ToDecimal(flightDetail.AirItineraryPricingInfo.FirstOrDefault().BaseFareAmount): 0;
                            flightModelObj.BaseFareCurrencyCode = flightDetail.AirItineraryPricingInfo.FirstOrDefault().BaseFareCurrencyCode;
                            flightModelObj.SourceUrl = flightDetail.SourceURL;
                            flightList.Add(flightModelObj);
                        }
                    }
                }
            }
            SFLogger.logMessage("SearchResultsHelper:GetIBFlights(): end.");
            List<FlightModel> result = null;
            if (SFConfiguration.GetNearByAirports().Equals("false"))
            {
                result = flightList.Where(d => searchModel.DestinationToId.Contains(d.InboundFlightFrom)).Where(x => searchModel.SourceFromId.Contains(x.InboundFlightTo)).ToList();
                result.AddRange(flightList.Where(d => searchModel.DestinationToId.Contains(d.InboundFlightFrom)).TakeWhile(x => x.InBoundStopPoints != null).TakeWhile(x => x.InBoundStopPoints.Count > 0).
                    Where(d => d.InBoundStopPoints[d.InBoundStopPoints.Count - 1].NextStopAirportCode.Equals(searchModel.SourceFromId)).ToList());
            }
            else
            {
                result= flightList;
            }
            return result.OrderBy(x => x.PricePerPerson).ToList();
        }
        private static List<FlightModel> GetOBFlights(SearchModel searchModel, ConcurrentBag<Flights.Flight> lstflight, string srcJsonPath, string destJsonPath)
        {
            SFLogger.logMessage("SearchResultsHelper:GetOBFlights(): started!!.");
            var flightList = new List<FlightModel>();
            if (lstflight != null)
            {
                if (lstflight.Count > 0)
                {
                    foreach (var flightDetail in lstflight)
                    {
                        if (flightDetail.AirItinerary.Count > 0 && flightDetail.AirItinerary.FirstOrDefault().FlightType.Equals("Out"))
                        {
                            var flightModelObj = new FlightModel();
                            flightModelObj.FlightFare = 0;
                            flightModelObj.PricePerPerson = (Convert.ToDecimal(flightDetail.AirItineraryPricingInfo.FirstOrDefault().TotalFareAmount) / 100);
                            flightModelObj.AirlineSrc = "/Content/Assets/americanairlines.gif";
                            if (flightDetail.AirItinerary.Count > 1)
                            {
                                var outboundDepartureDetails = flightDetail.AirItinerary.FirstOrDefault();
                                var outboundArrivalDetails = flightDetail.AirItinerary.LastOrDefault();
                                if (outboundDepartureDetails != null)
                                {
                                    flightModelObj.OutBoundFlightNumber = outboundDepartureDetails.FlightNumber;
                                    flightModelObj.OutBoundFlightCode = outboundDepartureDetails.MarketingAirlineCode;
                                    flightModelObj.OutBoundFlightName = outboundDepartureDetails.MarketingAirline;
                                    flightModelObj.OutboundFlightFrom = outboundDepartureDetails.DepartureAirportLocationCode;
                                    flightModelObj.OutboundFlightFromName = SFConfiguration.GetAirportName(flightModelObj.OutboundFlightFrom, destJsonPath, srcJsonPath);
                                    flightModelObj.OutboundDepartureTime = outboundDepartureDetails.DepartureDateTime;
                                }
                                if (outboundArrivalDetails != null)
                                {
                                    flightModelObj.OutboundFlightTo = outboundArrivalDetails.ArrivalAirportLocationCode;
                                    flightModelObj.OutboundFlightToName = SFConfiguration.GetAirportName(flightModelObj.OutboundFlightTo, destJsonPath, srcJsonPath);
                                    flightModelObj.OutboundArrivalTime = outboundArrivalDetails.ArrivalDateTime;
                                    var estHours = (Convert.ToDateTime(flightModelObj.OutboundArrivalTime) - Convert.ToDateTime(flightModelObj.OutboundDepartureTime));
                                    flightModelObj.OutBoundEstimatedHours = estHours.Hours.ToString("00") + ":" + estHours.Minutes.ToString("00");
                                    flightModelObj.OutboundDepartureTimeV = Convert.ToDateTime(outboundDepartureDetails.DepartureDateTime).ToUniversalTime().ToString("ddd, dd/MM/yyyy HH:mm");
                                    flightModelObj.OutboundArrivalTimeV = Convert.ToDateTime(outboundArrivalDetails.ArrivalDateTime).ToUniversalTime().ToString("ddd, dd/MM/yyyy HH:mm");
                                }
                                var outBoundStopList = new List<FlightStopPoints>();
                                var totalDuration = new TimeSpan();
                                foreach (var item in flightDetail.AirItinerary)
                                {
                                    var flightStopPoint = new FlightStopPoints();
                                    flightStopPoint.DepartureDateTime = item.DepartureDateTime;
                                    flightStopPoint.StopAirportCode = item.DepartureAirportLocationCode;
                                    flightStopPoint.StopAirportName = SFConfiguration.GetAirportName(flightStopPoint.StopAirportCode, destJsonPath, srcJsonPath);
                                    flightStopPoint.NextStopAirportCode = item.ArrivalAirportLocationCode;
                                    flightStopPoint.NextStopAirportName = SFConfiguration.GetAirportName(flightStopPoint.NextStopAirportCode, destJsonPath, srcJsonPath);
                                    flightStopPoint.NextStopArrrivalDateTime = item.ArrivalDateTime;
                                    var depDateTime = (Convert.ToDateTime(flightStopPoint.NextStopArrrivalDateTime) - Convert.ToDateTime(flightStopPoint.DepartureDateTime));
                                    totalDuration += depDateTime;
                                    flightStopPoint.Duration = depDateTime.Hours.ToString("00") + ":" + depDateTime.Minutes.ToString("00") + " h";
                                    flightStopPoint.FlightNumber = item.FlightNumber;
                                    flightStopPoint.FlightName = item.MarketingAirline;
                                    flightStopPoint.FlightCode = item.MarketingAirlineCode;
                                    flightStopPoint.JourneyIndicator = item.JourneyIndicator;
                                    if (outBoundStopList != null && outBoundStopList.Count > 0)
                                    {
                                        var lastOutBoungStop = outBoundStopList.Last();
                                        if (lastOutBoungStop != null)
                                        {
                                            var longWaitDateTime = (Convert.ToDateTime(flightStopPoint.DepartureDateTime) - Convert.ToDateTime(lastOutBoungStop.NextStopArrrivalDateTime));
                                            totalDuration = totalDuration + longWaitDateTime;
                                            flightStopPoint.LongWait = longWaitDateTime.Hours.ToString("00") + ":" + longWaitDateTime.Minutes.ToString("00") + " h";
                                            lastOutBoungStop.LongWait = flightStopPoint.LongWait;
                                        }
                                    }
                                    flightStopPoint.DepartureDateTimeV = Convert.ToDateTime(item.DepartureDateTime).ToUniversalTime().ToString("ddd, dd/MM/yyyy HH:mm");
                                    flightStopPoint.NextStopArrrivalDateTimeV = Convert.ToDateTime(item.ArrivalDateTime).ToUniversalTime().ToString("ddd, dd/MM/yyyy HH:mm");
                                    outBoundStopList.Add(flightStopPoint);
                                }
                                flightModelObj.OutBoundJourneyDuration = totalDuration.Hours.ToString("00") + ":" + totalDuration.Minutes.ToString("00") + " h";
                                flightModelObj.OutBoundStopPoints = outBoundStopList;
                                flightModelObj.OutBoundStopsCount = flightDetail.AirItinerary.Count - 1;
                                flightModelObj.OutBoundFlightWay = flightModelObj.OutBoundStopsCount.ToString() + " Stop";
                            }
                            else
                            {
                                flightModelObj.OutBoundFlightNumber = flightDetail.AirItinerary.FirstOrDefault().FlightNumber;
                                flightModelObj.OutboundFlightFrom = flightDetail.AirItinerary.FirstOrDefault().DepartureAirportLocationCode;
                                flightModelObj.OutBoundFlightName = flightDetail.AirItinerary.FirstOrDefault().MarketingAirline;
                                flightModelObj.OutBoundFlightCode = flightDetail.AirItinerary.FirstOrDefault().MarketingAirlineCode;
                                flightModelObj.OutboundFlightFromName = SFConfiguration.GetAirportName(flightModelObj.OutboundFlightFrom, destJsonPath, srcJsonPath);
                                flightModelObj.OutboundDepartureTime = flightDetail.AirItinerary.FirstOrDefault().DepartureDateTime.ToString();
                                flightModelObj.OutboundFlightTo = flightDetail.AirItinerary.FirstOrDefault().ArrivalAirportLocationCode;
                                flightModelObj.OutboundFlightToName = SFConfiguration.GetAirportName(flightModelObj.OutboundFlightTo, destJsonPath, srcJsonPath);
                                flightModelObj.OutboundArrivalTime = flightDetail.AirItinerary.FirstOrDefault().ArrivalDateTime.ToString();
                                var estHours = (Convert.ToDateTime(flightModelObj.OutboundArrivalTime) - Convert.ToDateTime(flightModelObj.OutboundDepartureTime));
                                flightModelObj.OutBoundEstimatedHours = estHours.Hours.ToString("00") + ":" + estHours.Minutes.ToString("00");
                                flightModelObj.OutBoundStopsCount = 0;
                                flightModelObj.OutBoundFlightWay = "Direct";
                                flightModelObj.OutboundDepartureTimeV = Convert.ToDateTime(flightDetail.AirItinerary.FirstOrDefault().DepartureDateTime).ToUniversalTime().ToString("ddd, dd/MM/yyyy HH:mm");
                                flightModelObj.OutboundArrivalTimeV = Convert.ToDateTime(flightDetail.AirItinerary.FirstOrDefault().ArrivalDateTime).ToUniversalTime().ToString("ddd, dd/MM/yyyy HH:mm");
                            }
                            flightModelObj.OBJourneyIndicator = flightDetail.AirItinerary.FirstOrDefault().JourneyIndicator;
                            flightModelObj.IsOBPriceUpdated = 0;
                            flightModelObj.FlightQuoteId = flightDetail.AirItineraryPricingInfo.FirstOrDefault().QuoteId;
                            flightModelObj.AirLaneName = flightDetail.AirItinerary.FirstOrDefault().MarketingAirline;
                            flightModelObj.CurrencyCode = flightDetail.AirItineraryPricingInfo.FirstOrDefault().TotalFareCurrencyCode;
                            flightModelObj.NoOfAdults = searchModel.Adults;
                            flightModelObj.NoOfChilds = searchModel.Child;
                            flightModelObj.NoOfInfants = searchModel.Infants;
                            flightModelObj.NoOfBags = searchModel.Bags;
                            flightModelObj.QuoteReference = PackageViewModel.GetUniqueKey(12).ToUpper();
                            flightModelObj.SourceUrl = flightDetail.SourceURL;
                            flightList.Add(flightModelObj);
                        }
                    }
                }
            }
            SFLogger.logMessage("SearchResultsHelper:GetOBFlights(): end.");
            List<FlightModel> result = null;
            if (SFConfiguration.GetNearByAirports().Equals("false"))
            {
                result = flightList.Where(d => searchModel.SourceFromId.Contains(d.OutboundFlightFrom)).Where(x => searchModel.DestinationToId.Contains(x.OutboundFlightTo)).ToList();
                result.AddRange(flightList.Where(d => searchModel.SourceFromId.Contains(d.OutboundFlightFrom)).TakeWhile(x => x.OutBoundStopPoints != null).TakeWhile(x => x.OutBoundStopPoints.Count > 0).
                    Where(d => d.OutBoundStopPoints[d.OutBoundStopPoints.Count - 1].NextStopAirportCode.Equals(searchModel.DestinationToId)).ToList());
            }
            else
            {
                result = flightList;
            }
            return result.OrderBy(x => x.PricePerPerson).ToList();
        }

        private static ConcurrentBag<Flights.Flight> GetFlightListFromService(SearchModel searchModel, string url)
        {
            SFLogger.logMessage("SearchResultsHelper:GetFlightListFromService(): started!!.");
            var lstFlights = new ConcurrentBag<Flights.Flight>();
            var list = new ArrayList();
            list.Add(searchModel);
            var travelSAASClient = new TravelSAASRestClient();
            var flightList = new List<Flights.Flight>();
            var result = travelSAASClient.ExecuteRequest(url, RequestType.Post, list);
            if (result.StatusCode == (int)System.Net.HttpStatusCode.OK)
                flightList = JsonConvert.DeserializeObject<List<Flights.Flight>>(result.Response);
            else
                throw new SwordfishAjaxException(result.Response);
            if (flightList != null && flightList.Count > 0)
                lstFlights = new ConcurrentBag<Flights.Flight>(flightList);
            SFLogger.logMessage("SearchResultsHelper:GetFlightListFromService(): end.");
            return lstFlights;
        }

        private static SesameBasket GetFlightItinaryPriceFromService(SesameFlightItinaryPriceRequest flightItinaryPriceRequest, string url)
        {
            SFLogger.logMessage("SearchResultsHelper:GetFlightItinaryPriceFromService(): started!!.");
            var list = new ArrayList();
            list.Add(flightItinaryPriceRequest);
            var travelSAASClient = new TravelSAASRestClient();
            var sesameBasket = new SesameBasket();
            var result = travelSAASClient.ExecuteRequest(url, RequestType.Post, list);
            if (result.StatusCode == (int)System.Net.HttpStatusCode.OK)
                sesameBasket = JsonConvert.DeserializeObject<SesameBasket>(result.Response);
            else
                throw new SwordfishAjaxException(result.Response);
            SFLogger.logMessage("SearchResultsHelper:GetFlightItinaryPriceFromService(): end.");
            return sesameBasket;
        }
        public static SesameBasket GetFlightItinaryPrice(SesameFlightItinaryPriceRequest flightItinaryPriceRequest, string sourceUrl)
        {
            SFLogger.logMessage("SearchResultsHelper:GetFlightItinaryPrice(): started!!.", "GetFlightItinaryPrice", flightItinaryPriceRequest.quoteId);
            Task<SesameBasket> taskFlightPrice = Task.Factory.StartNew(() => GetFlightItinaryPriceFromService(flightItinaryPriceRequest, String.Format(SFConfiguration.GetTravelSaasFlightCreateBasketDetails(), flightItinaryPriceRequest, sourceUrl)));
            Task.WaitAll(taskFlightPrice);
            SFLogger.logMessage("SearchResultsHelper:GetFlightItinaryPrice(): end.");
            SFLogger.logMessage("SearchResultsHelper:GetFlightItinaryPrice(): end!!.", "GetFlightItinaryPrice", JsonConvert.SerializeObject(taskFlightPrice.Result));
            return taskFlightPrice.Result;
        }
        public async Task<List<Customer>> SearchCustomer(Customer customer)
        {
            SFLogger.logMessage("SearchResultsHelper:SearchCustomer(): started!!.");
            var result = new List<Customer>();
            var serviceRepository = new BookingService();
            result = await Task.Run(() => SearchCustomerDetails(serviceRepository, customer).Result).ConfigureAwait(false);
            SFLogger.logMessage("SearchResultsHelper:SearchCustomer(): end.");
            return result;
        }


        public List<Customer> GetCustomerbyMembershipNo(Customer customer)
        {
            SFLogger.logMessage("SearchResultsHelper:GetCustomerbyMembershipNo(): started!!.");
            var result = new List<Customer>();
            var serviceRepository = new BookingService();
            result = GetCustomerbyMembershipNo(serviceRepository, customer).Result;
            SFLogger.logMessage("SearchResultsHelper:GetCustomerbyMembershipNo(): end.");
            return result;
        }
        public async Task<string> CheckEmailExist(string emailId, string memberShipNo)
        {
            SFLogger.logMessage("SearchResultsHelper:CheckEmailExist(): started!!.");
            BookingService serviceRepository = new BookingService();
            var result = await Task.Run(() => serviceRepository.CheckEmailExist(emailId, memberShipNo)).ConfigureAwait(false);
            return result;
        }

        public async Task<string> CheckMobileNoExist(string phone_number, string memberShipNo)
        {
            SFLogger.logMessage("SearchResultsHelper:CheckEmailExist(): started!!.");
            BookingService serviceRepository = new BookingService();
            var result = await Task.Run(() => serviceRepository.CheckPhoneNumberExist(phone_number, memberShipNo)).ConfigureAwait(false);
            return result;
        }


        private async Task<List<Customer>> GetCustomerbyMembershipNo(BookingService serviceRepository, Customer customer)
        {
            SFLogger.logMessage("SearchResultsHelper:GetCustomerbyMembershipNo(): started!!.");
            var customerHelper = new CustomerHelper(serviceRepository);
            var result = await Task.Run(() => customerHelper.GetCustomerDatabyMembershipNo(customer)).ConfigureAwait(false);
            SFLogger.logMessage("SearchResultsHelper:GetCustomerbyMembershipNo(): end.");
            return result;
        }
        private async Task<List<Customer>> SearchCustomerDetails(BookingService serviceRepository, Customer customer)
        {
            SFLogger.logMessage("SearchResultsHelper:SearchCustomerDetails(): started!!.");
            var customerHelper = new CustomerHelper(serviceRepository);
            var result = await Task.Run(() => customerHelper.SearchCustomerData(customer)).ConfigureAwait(false);
            SFLogger.logMessage("SearchResultsHelper:SearchCustomerDetails(): end.");
            return result;
        }
        public IEnumerable<CustomerQuotes> GetCustomerQuotes(int CustomerId)
        {
            SFLogger.logMessage("SearchResultsHelper:GetCustomerQuotes(): started!!.");
            var serviceRepository = new BookingService();
            var customerQuotes = GetCustomerQuotes(serviceRepository, CustomerId);
            SFLogger.logMessage("SearchResultsHelper:GetCustomerQuotes(): end.");
            return customerQuotes.Result.Take(Convert.ToInt32(SFConfiguration.GetNumberOfQuotesToDisplay()));
        }
        private async Task<ConcurrentBag<CustomerQuotes>> GetCustomerQuotes(BookingService serviceRepository, int CustomerId)
        {
            SFLogger.logMessage("SearchResultsHelper:GetCustomerQuotes(): started!!.");
            var quoteHelper = new QuoteHelper(serviceRepository);
            var recentQuotes = await Task.Run(() => quoteHelper.GetCustomerQuotes(CustomerId)).ConfigureAwait(false);
            // Task.WhenAll(recentQuotes);
            SFLogger.logMessage("SearchResultsHelper:GetCustomerQuotes(): end.");
            return recentQuotes;
        }
        public async Task<int> SaveAgentJournal(string QuoteRef, string JournalType, string JournalNotes)
        {
            SFLogger.logMessage("SearchResultsHelper:SaveAgentJournal(): started!!.");
            int agentId = 0;
            if (SFConfiguration.GetUserID() > 0)
            {
                agentId = SFConfiguration.GetUserID();
            }
            BookingService serviceRepository = new BookingService();
            var result = await Task.Run(() => serviceRepository.SaveAgentJournal(agentId, QuoteRef, JournalType, JournalNotes)).ConfigureAwait(false);
            SFLogger.logMessage("SearchResultsHelper:SaveAgentJournal(): end.");
            return result;
        }
        public object GetBaggageSlabs(BookCreateRequest bookCreateRequest)
        {
            SFLogger.logMessage("SearchResultsHelper:GetBaggageSlabs(): started!!.");
            string url1 = string.Format(SFConfiguration.GetTravelSaasCreateBasket(), GetSessionID());
            var task = Task.Run(() => GetBaggage(bookCreateRequest, url1));
            Task.WhenAll(task);
            SFLogger.logMessage("SearchResultsHelper:GetBaggageSlabs(): end.");
            return task.Result;
        }
        private object GetBaggage(BookCreateRequest bookCreateRequest, string url)
        {
            SFLogger.logMessage("SearchResultsHelper:GetBaggage(): started!!.");
            var list = new ArrayList();
            list.Add(bookCreateRequest);
            var travelSAASClient = new TravelSAASRestClient();
            var response = travelSAASClient.ExecuteRequest(url, RequestType.Post, list);
            SFLogger.logMessage("SearchResultsHelper:GetBaggage(): end.");
            return response;
        }
        private static string GetPrice(BookCreateRequest bookCreateRequest)
        {
            SFLogger.logMessage("SearchResultsHelper:GetPrice(): started!!.");
            string url1 = string.Format(SFConfiguration.GetTravelSaasCreateBasket(), GetSessionID());
            var task = Task.Run(() => GetAirItineraryPrice(bookCreateRequest, url1));
            Task.WhenAll(task);
            SFLogger.logMessage("SearchResultsHelper:GetPrice(): end.");
            return ((RestResponseBase)task.Result).Content.ToString();
        }
        private static object GetAirItineraryPrice(BookCreateRequest bookCreateRequest, string url)
        {
            SFLogger.logMessage("SearchResultsHelper:GetAirItineraryPrice(): started!!.");
            var list = new ArrayList();
            list.Add(bookCreateRequest);
            var travelSAASClient = new TravelSAASRestClient();
            var response = travelSAASClient.ExecuteRequest(url, RequestType.Post, list);
            SFLogger.logMessage("SearchResultsHelper:GetAirItineraryPrice(): end.");
            return response;
        }
        public Quote ConvertBasketToQuote(BasketStructure basketStructure)
        {
            SFLogger.logMessage("SearchResultsHelper:ConvertBasketToQuote(): started!!.");
            var quote = new Quote();
            //Conversion and assignment of baggages
            if (basketStructure.baggage.selectedRoundTripBaggage != null || basketStructure.baggage.inboundSelectedBaggage != null || basketStructure.baggage.OutboundSelectedBaggage != null)
                ConvertBaggageSlabs(basketStructure.baggage, quote);
            else
                quote.bagsComponents = null;

            AssignSearchModelValuesToQuote(basketStructure, quote);
            //Assignment of flights
            if (basketStructure.flightsReturn != null)
                AssignFlightsModelValuesToQuote(basketStructure.flightsReturn, quote, SFConstants.QuoteComponent_FlightReturn);
            if (basketStructure.flightsOneWayOutBound != null)
                AssignFlightsModelValuesToQuote(basketStructure.flightsOneWayOutBound, quote, SFConstants.QuoteComponent_Flightoneway);
            if (basketStructure.flightsOneWayInBound != null)
                AssignFlightsModelValuesToQuote(basketStructure.flightsOneWayInBound, quote, SFConstants.QuoteComponent_Flightoneway);
            //Assignment of hotels
            GetHotelDetailsFromBasketStructure(basketStructure, quote);
            //Assignment of transfers
            if (basketStructure.transfersdata != null)
                GetTransfersFromBasketStructure(basketStructure, quote);
            else
                quote.transferComponents = null;
            //Assignment of basket header
            GetValuesForQuoteHeaderFromBasketStructure(basketStructure, quote);
            SFLogger.logMessage("SearchResultsHelper:ConvertBasketToQuote(): end.");
            return quote;

        }
        private void AssignSearchModelValuesToQuote(BasketStructure basketStructure, Quote quoteobject)
        {
            Businessobjects.Quotes.Occupancy occupancyRoom = null;
            SFLogger.logMessage("SearchResultsHelper:AssignSearchModelValuesToQuote(): started!!.");
            quoteobject.searchModel.AdultsCount = basketStructure.searchModel.Adults;
            quoteobject.searchModel.ChildrenCount = basketStructure.searchModel.Child;
            quoteobject.searchModel.InfantsCount = basketStructure.searchModel.Infants;
            quoteobject.searchModel.TTSSQuoteId = basketStructure.searchModel.TTSSQuoteReference;
            quoteobject.searchModel.sfQuoteId = basketStructure.searchModel.sfquoteId;
            quoteobject.searchModel.sourceName = basketStructure.searchModel.SourceFrom;
            quoteobject.searchModel.sourceCode = basketStructure.searchModel.SourceFromId;
            quoteobject.searchModel.destinationName = basketStructure.searchModel.DestinationTo;
            quoteobject.searchModel.destinationCode = basketStructure.searchModel.DestinationToId;
            quoteobject.searchModel.startDate = Convert.ToDateTime(basketStructure.searchModel.CheckIn);
            quoteobject.searchModel.endDate = Convert.ToDateTime(basketStructure.searchModel.CheckOut);
            quoteobject.searchModel.hotelName = basketStructure.searchModel.Hotel;
            quoteobject.searchModel.BoardTypeAI = basketStructure.searchModel.BoardTypeAI;
            quoteobject.searchModel.BoardTypeFB = basketStructure.searchModel.BoardTypeFB;
            quoteobject.searchModel.BoardTypeHB = basketStructure.searchModel.BoardTypeHB;
            quoteobject.searchModel.BoardTypeBB = basketStructure.searchModel.BoardTypeBB;
            quoteobject.searchModel.BoardTypeSC = basketStructure.searchModel.BoardTypeSC;
            quoteobject.searchModel.BoardTypeRO = basketStructure.searchModel.BoardTypeRO;
            quoteobject.searchModel.RoomNo = basketStructure.searchModel.Rooms;
            quoteobject.searchModel.starRating = basketStructure.searchModel.Rating;
            quoteobject.searchModel.Nights = basketStructure.searchModel.Nights;
            foreach (var item in basketStructure.searchModel.OccupancyRooms)
            {
                occupancyRoom = new Businessobjects.Quotes.Occupancy();
                occupancyRoom.RoomNo = item.RoomNo;
                occupancyRoom.AdultsCount = item.AdultsCount;
                occupancyRoom.ChildCount = item.ChildCount;
                occupancyRoom.InfantCount = item.InfantCount;
                quoteobject.searchModel.OccupancyRooms.Add(occupancyRoom);
            }
            SFLogger.logMessage("SearchResultsHelper:AssignSearchModelValuesToQuote(): end.");
        }
        private void ConvertBaggageSlabs(Baggage baggage, Quote quoteobject)
        {
            SFLogger.logMessage("SearchResultsHelper:ConvertBaggageSlabs(): started!!.");
            if (baggage.isTwoWay == true && baggage.selectedRoundTripBaggage != null)
            {
                SeparateSlabs(baggage.selectedRoundTripBaggage, baggage.roundTripBaggageAllSlabs);
                quoteobject.bagsComponents = AssignSelectedBaggageSlabsToQuote(baggage.selectedRoundTripBaggage, SFConstants.QuoteComponent_FlightReturn);
            }
            else if (baggage.isTwoWay == false && (baggage.OutboundSelectedBaggage != null || baggage.inboundSelectedBaggage != null))
            {
                if (baggage.OutboundSelectedBaggage != null)
                {
                    SeparateSlabs(baggage.OutboundSelectedBaggage, baggage.outboundBaggageAllSlabs);
                    quoteobject.bagsComponents = AssignSelectedBaggageSlabsToQuote(baggage.OutboundSelectedBaggage, SFConstants.Direction_Outbound);
                }
                if (baggage.inboundSelectedBaggage != null)
                {
                    SeparateSlabs(baggage.inboundSelectedBaggage, baggage.inboundBaggageAllSlabs);
                    quoteobject.bagsComponents.AddRange(AssignSelectedBaggageSlabsToQuote(baggage.inboundSelectedBaggage, SFConstants.Direction_Inbound));
                }

            }
            SFLogger.logMessage("SearchResultsHelper:ConvertBaggageSlabs(): end.");
        }

        private List<BagComponent> AssignSelectedBaggageSlabsToQuote(List<SelectedBaggage> selectedBaggages, int directionOptionId)
        {
            SFLogger.logMessage("SearchResultsHelper:AssignSelectedBaggageSlabsToQuote(): started!!.");
            var bagComponents = new List<BagComponent>();
            BagComponent bagComponent = null;
            foreach (var selectedBaggage in selectedBaggages)
            {
                foreach (var slab in selectedBaggage.baggage.slab)
                {
                    bagComponent = new BagComponent();
                    bagComponent.BagsCode = slab.Code;
                    bagComponent.BagsDescription = slab.Description;
                    bagComponent.BagsType = slab.Type;
                    bagComponent.BuyingPrice = Convert.ToDecimal(slab.BuyingPricePence);
                    bagComponent.SellingPrice = Convert.ToDecimal(slab.SellingPricePence);
                    bagComponent.QuantityAvailable = Convert.ToInt32(slab.QuantityAvailable);
                    bagComponent.QuantityRequired = (Convert.ToInt32(slab.QuantityRequired) == 0) ? 1 : Convert.ToInt32(slab.QuantityRequired);
                    bagComponent.DirectionOptionId = directionOptionId;
                    bagComponent.Items = slab.Items;
                    bagComponent.PassengerIndex = selectedBaggage.passengerIndex;
                    bagComponents.Add(bagComponent);
                }
            }
            SFLogger.logMessage("SearchResultsHelper:AssignSelectedBaggageSlabsToQuote(): end.");
            return bagComponents;
        }
        private void SeparateSlabs(List<SelectedBaggage> listOfSelectedSlabs, List<BaggageSlab> allSlabs)
        {
            SFLogger.logMessage("SearchResultsHelper:SeparateSlabs(): started!!.");
            var modifiedSelectedBaggage = new List<SelectedBaggage>();
            if (listOfSelectedSlabs != null && listOfSelectedSlabs.Count > 0)
            {
                foreach (var selectedSlab in listOfSelectedSlabs)
                {
                    var slabs = new List<BaggageSlab>();
                    if (selectedSlab.baggage.slab[0].Code.IndexOf('|') != -1)
                    {
                        var codes = selectedSlab.baggage.slab[0].Code.Split('|');
                        foreach (var code in codes)
                        {
                            for (int index = 0; allSlabs.Count() > index; index++)
                            {
                                if (allSlabs[index].Code == code)
                                {
                                    slabs.Add(allSlabs[index]);
                                    break;
                                }
                            }

                        }
                        selectedSlab.baggage.slab = slabs;
                    }
                }
            }
            SFLogger.logMessage("SearchResultsHelper:SeparateSlabs(): end.");
        }
        private void AssignFlightsModelValuesToQuote(FlightModel flightModel, Quote quoteobject, int directionOptionId)
        {
            SFLogger.logMessage("SearchResultsHelper:AssignFlightsModelValuesToQuote(): started!!.");
            var flightComponent = new Flights.FlightComponent();
            List<Flights.FlightComponentDetail> FlightComponentDetails = new List<Flights.FlightComponentDetail>();
            flightComponent.Amount = Convert.ToString(flightModel.FlightFare);
            flightComponent.CurrencyCode = flightModel.CurrencyCode;
            flightComponent.FlightQuoteId = flightModel.FlightQuoteId;
            flightComponent.BaseFareAmount = flightModel.BaseFareAmount;
            flightComponent.BaseFareCurrencyCode = flightModel.BaseFareCurrencyCode;
            flightComponent.SourceUrl = flightModel.SourceUrl;
            flightComponent.ExchangeRateUsed = flightModel.ExchangeRateUsed;
            if (flightModel.OutBoundFlightNumber != null && flightModel.InBoundFlightNumber != null)
            {
                flightComponent.ComponentTypeOptionId = SFConstants.QuoteComponent_FlightReturn;
            }
            if (flightModel.OutBoundFlightNumber != null)
            {
                flightComponent.Source = flightModel.OutboundFlightFrom;
                flightComponent.Arrival = flightModel.OutboundFlightTo;
                flightComponent.ComponentTypeOptionId = directionOptionId;
                if (flightModel.OutBoundStopPoints != null && flightModel.OutBoundStopPoints.Count > 0)
                {
                    flightComponent.FlightComponentDetails = GetFlightStops(flightModel.OutBoundStopPoints, SFConstants.Direction_Outbound);
                    if (directionOptionId == SFConstants.QuoteComponent_FlightReturn)
                    {
                        flightComponent.FlightComponentDetails.AddRange(GetFlightStops(flightModel.InBoundStopPoints, SFConstants.Direction_Inbound));

                    }
                }

                else
                {
                    flightComponent.FlightComponentDetails = new List<Flights.FlightComponentDetail>();
                    flightComponent.FlightComponentDetails.Add(GetHopsForOutboundForDirect(flightModel, SFConstants.Direction_Outbound));
                    if (flightModel.OutBoundFlightNumber != null && flightModel.InBoundFlightNumber != null)
                    {
                        flightComponent.FlightComponentDetails.Add(GetHopsForInboundForDirect(flightModel, SFConstants.Direction_Inbound));
                    }

                }
                quoteobject.flightComponents.Add(flightComponent);

            }
            else if (flightModel.InBoundFlightNumber != null)
            {
                flightComponent.Source = flightModel.InboundFlightFrom;
                flightComponent.Arrival = flightModel.InboundFlightTo;
                flightComponent.ComponentTypeOptionId = directionOptionId;
                if (flightModel.InBoundStopPoints != null && flightModel.InBoundStopPoints.Count > 0)
                    flightComponent.FlightComponentDetails = GetFlightStops(flightModel.InBoundStopPoints, SFConstants.Direction_Inbound);
                else
                {
                    flightComponent.FlightComponentDetails = new List<Flights.FlightComponentDetail>();
                    flightComponent.FlightComponentDetails.Add(GetHopsForInboundForDirect(flightModel, SFConstants.Direction_Inbound));
                }
                quoteobject.flightComponents.Add(flightComponent);
            }
            SFLogger.logMessage("SearchResultsHelper:AssignFlightsModelValuesToQuote(): end.");
        }
        private Flights.FlightComponentDetail GetHopsForOutboundForDirect(FlightModel flightModel, int directionOptionId)
        {
            SFLogger.logMessage("SearchResultsHelper:GetHopsForOutboundForDirect(): started!!.");
            var flightComponentsDetail = new Flights.FlightComponentDetail();
            flightComponentsDetail.DepartureDateTime = Convert.ToDateTime(flightModel.OutboundDepartureTime).ToUniversalTime();
            flightComponentsDetail.ArrivalDateTime = Convert.ToDateTime(flightModel.OutboundArrivalTime).ToUniversalTime();
            flightComponentsDetail.FlightNumber = flightModel.OutBoundFlightNumber;
            flightComponentsDetail.JourneyIndicator = flightModel.OBJourneyIndicator;
            flightComponentsDetail.DepartureAirportCode = flightModel.OutboundFlightFrom;
            flightComponentsDetail.ArrivalAirportCode = flightModel.OutboundFlightTo;
            flightComponentsDetail.MarketingAirLine = flightModel.AirLaneName;
            flightComponentsDetail.MarketingAirlineCode = flightModel.OutBoundFlightCode;
            flightComponentsDetail.DirectionOptionId = directionOptionId;
            SFLogger.logMessage("SearchResultsHelper:GetHopsForOutboundForDirect(): end.");
            return flightComponentsDetail;
        }

        private Flights.FlightComponentDetail GetHopsForInboundForDirect(FlightModel flightModel, int directionOptionId)
        {
            SFLogger.logMessage("SearchResultsHelper:GetHopsForInboundForDirect(): started!!.");
            var flightComponentsDetail = new Flights.FlightComponentDetail();
            flightComponentsDetail.DepartureDateTime = Convert.ToDateTime(flightModel.InboundDepartureTime).ToUniversalTime();
            flightComponentsDetail.ArrivalDateTime = Convert.ToDateTime(flightModel.InboundArrivalTime).ToUniversalTime();
            flightComponentsDetail.FlightNumber = flightModel.InBoundFlightNumber;
            flightComponentsDetail.JourneyIndicator = flightModel.IBJourneyIndicator;
            flightComponentsDetail.DepartureAirportCode = flightModel.InboundFlightFrom;
            flightComponentsDetail.ArrivalAirportCode = flightModel.InboundFlightTo;
            flightComponentsDetail.MarketingAirLine = flightModel.AirLaneName;
            flightComponentsDetail.MarketingAirlineCode = flightModel.InBoundFlightCode;
            flightComponentsDetail.DirectionOptionId = directionOptionId;
            SFLogger.logMessage("SearchResultsHelper:GetHopsForInboundForDirect(): end.");
            return flightComponentsDetail;
        }
        private List<Flights.FlightComponentDetail> GetFlightStops(List<FlightStopPoints> flightStops, int directionOptionId)
        {
            SFLogger.logMessage("SearchResultsHelper:GetFlightStops(): started!!.");
            Flights.FlightComponentDetail flightComponentsDetail = null;
            var flightComponentsDetails = new List<Flights.FlightComponentDetail>();
            foreach (var flightStopPoint in flightStops)
            {
                flightComponentsDetail = new Flights.FlightComponentDetail();
                flightComponentsDetail.DepartureDateTime = Convert.ToDateTime(flightStopPoint.DepartureDateTime).ToUniversalTime();
                flightComponentsDetail.ArrivalDateTime = Convert.ToDateTime(flightStopPoint.NextStopArrrivalDateTime).ToUniversalTime();
                flightComponentsDetail.FlightNumber = flightStopPoint.FlightNumber;
                flightComponentsDetail.JourneyIndicator = flightStopPoint.JourneyIndicator;
                flightComponentsDetail.DepartureAirportCode = flightStopPoint.StopAirportCode;
                flightComponentsDetail.ArrivalAirportCode = flightStopPoint.NextStopAirportCode;
                flightComponentsDetail.MarketingAirLine = flightStopPoint.FlightName;
                flightComponentsDetail.MarketingAirlineCode = flightStopPoint.FlightCode;
                flightComponentsDetail.DirectionOptionId = directionOptionId;
                flightComponentsDetail.CabinClass = flightStopPoint.CabinClass;
                flightComponentsDetails.Add(flightComponentsDetail);
            }
            SFLogger.logMessage("SearchResultsHelper:GetFlightStops(): end.");
            return flightComponentsDetails;
        }
        private void GetHotelDetailsFromBasketStructure(BasketStructure basketStructure, Quote quoteobject)
        {
            SFLogger.logMessage("SearchResultsHelper:GetHotelDetailsFromBasketStructure(): started!!.");
            var hotelComponent = basketStructure.hotel;
            var hotelComponentsDetail = new HotelComponentDetail();
            var hotelComponentsDetails = new List<HotelComponentDetail>();
            quoteobject.hotelComponent.HotelName = hotelComponent.HotelName;
            quoteobject.hotelComponent.HotelCode = hotelComponent.HotelCode;
            quoteobject.hotelComponent.HotelCodeContext = hotelComponent.HotelCodeContext;
            quoteobject.hotelComponent.SupplierResortId = hotelComponent.SupplierResortId;
            quoteobject.hotelComponent.TTICode = !String.IsNullOrEmpty(hotelComponent.TTIcode) ? Convert.ToInt32(hotelComponent.TTIcode) : 0;
            quoteobject.hotelComponent.HotelTypeId = !String.IsNullOrEmpty(hotelComponent.ReferenceType) ? Convert.ToInt32(hotelComponent.ReferenceType) : 0;
            quoteobject.hotelComponent.HotelQuoteId = hotelComponent.ReferenceId;
            quoteobject.hotelComponent.ComponentTypeOptionId = SFConstants.QuoteComponent_Accommodation;
            quoteobject.hotelComponent.CheckInDate = hotelComponent.checkIn ?? Convert.ToDateTime(basketStructure.searchModel.CheckIn);
            quoteobject.hotelComponent.CheckOutDate = hotelComponent.checkOut ?? Convert.ToDateTime(basketStructure.searchModel.CheckOut);
            quoteobject.hotelComponent.StarRating = (decimal)hotelComponent.Rating;
            quoteobject.hotelComponent.TripAdvisorRating = (decimal)hotelComponent.TripAdvisorScore;
            hotelComponentsDetail.AmountAfterTax = Convert.ToDecimal(hotelComponent.AmountAfterTax);
            hotelComponentsDetail.RateQuoteId = hotelComponent.RateQuoteId;
            hotelComponentsDetail.RoomDescription = hotelComponent.RoomDescription;
            hotelComponentsDetail.RoomTypeCode = hotelComponent.RoomTypeCode;
            hotelComponentsDetail.RatePlanCode = hotelComponent.BoardType;
            if (hotelComponent.RoomRates != null && hotelComponent.RoomRates.Count > 0)
            {
                hotelComponentsDetail.RatePlanName = hotelComponent.RoomRates.FirstOrDefault(x => x.RoomTypeCode == hotelComponent.RoomTypeCode && x.QuoteId == hotelComponent.RateQuoteId).RatePlanName;
                hotelComponentsDetail.CancelPolicyIndicator =  hotelComponent.RoomRates.FirstOrDefault(x => x.RoomTypeCode == hotelComponent.RoomTypeCode && x.QuoteId == hotelComponent.RateQuoteId).CancelPolicyIndicator ?? false;
                hotelComponentsDetail.NonRefundable = hotelComponent.RoomRates.FirstOrDefault(x => x.RoomTypeCode == hotelComponent.RoomTypeCode && x.QuoteId == hotelComponent.RateQuoteId).NonRefundable ?? false;
                hotelComponentsDetails.Add(hotelComponentsDetail);
            }
            quoteobject.hotelComponent.hotelComponentsDetails = hotelComponentsDetails;
            SFLogger.logMessage("SearchResultsHelper:GetHotelDetailsFromBasketStructure(): end.");
        }

        private void GetTransfersFromBasketStructure(BasketStructure basketStructure, Quote quoteobject)
        {
            SFLogger.logMessage("SearchResultsHelper:GetTransfersFromBasketStructure(): started!!.");
            List<TransfersModel> transferModel = basketStructure.transfersdata;
            var transferComponentsDetails = new List<TransferComponent>();
            TransferComponent transferComponentsDetail = null;
            foreach (var transfersobjects in transferModel)
            {
                transferComponentsDetail = new TransferComponent();
                transferComponentsDetail.ProductId = Convert.ToString(transfersobjects.general[0].productid);
                transferComponentsDetail.BookingTypeId = Convert.ToString(transfersobjects.general[0].bookingtypeid);
                transferComponentsDetail.TransferTime = transfersobjects.general[0].transfertime;
                transferComponentsDetail.ProductTypeId = Convert.ToString(transfersobjects.general[0].producttypeid);
                transferComponentsDetail.ProductType = transfersobjects.general[0].producttype;
                transferComponentsDetail.MinPax = transfersobjects.general[0].minpax;
                transferComponentsDetail.MaxPax = transfersobjects.general[0].maxpax;
                transferComponentsDetail.Luggage = transfersobjects.general[0].luggage;
                transferComponentsDetail.perPerson = (Convert.ToDecimal(transfersobjects.pricing[0].price) / (basketStructure.searchModel.Adults + basketStructure.searchModel.Child + basketStructure.searchModel.Infants)).ToString();
                transferComponentsDetail.productDescription = transfersobjects.general[0].description.Replace("'", "");
                transferComponentsDetail.oldPrice = Convert.ToDecimal(transfersobjects.pricing[0].prices[0].oldprice);
                transferComponentsDetail.totalPrice = Convert.ToDecimal(transfersobjects.pricing[0].price);
                transferComponentsDetail.Currency = transfersobjects.pricing[0].currency;
                transferComponentsDetail.TypeCode = transfersobjects.pricing[0].prices[0].typecode;
                transferComponentsDetail.Units = Convert.ToInt32(transfersobjects.pricing[0].prices[0].units);
                transferComponentsDetail.PricePerPersonOldPrice = Convert.ToDecimal(transfersobjects.pricing[0].prices[0].price);
                transferComponentsDetail.PriceDescription = transfersobjects.pricing[0].prices[0].description.Replace("'", "");
                transferComponentsDetail.TransferTypeOptionId = transfersobjects.general[0].transferTypeOptionId;
                transferComponentsDetails.Add(transferComponentsDetail);
                quoteobject.transferComponents.Add(transferComponentsDetail);
                SFLogger.logMessage("SearchResultsHelper:GetTransfersFromBasketStructure(): end.");
            }

        }
        private void GetValuesForQuoteHeaderFromBasketStructure(BasketStructure basketStructure, Quote quoteobject)
        {
            SFLogger.logMessage("SearchResultsHelper:GetValuesForQuoteHeaderFromBasketStructure(): started!!.");
            quoteobject.quoteHeader.CustomerReferenceCode = basketStructure.customer.MembershipNo;
            //quoteobject.QuoteHeader.CustomerReferenceCode = "QQ";
            quoteobject.quoteHeader.AgentId = basketStructure.userInfo.UserId;
            quoteobject.quoteHeader.CustomerId = basketStructure.customer.CustomerId;
            quoteobject.quoteHeader.HotelCost = Convert.ToDecimal(basketStructure.hotel.AmountAfterTax);
            quoteobject.quoteHeader.BaggageCost = Convert.ToDecimal(basketStructure.baggage.totalPrice);
            if (basketStructure.transfersdata != null)
                quoteobject.quoteHeader.TransfersCost = basketStructure.transfersdata[0].pricing[0].price + basketStructure.transfersdata[1].pricing[0].price;
            quoteobject.quoteHeader.FidelityCost = basketStructure.fidelityVal;          
            quoteobject.quoteHeader.FlightCost = Convert.ToDecimal((basketStructure.flightsOneWayInBound == null ? 0 : basketStructure.flightsOneWayInBound.FlightFare)
                                                + (basketStructure.flightsOneWayOutBound == null ? 0 : basketStructure.flightsOneWayOutBound.FlightFare)
                                                + (basketStructure.flightsReturn == null ? 0 : basketStructure.flightsReturn.FlightFare));           
            quoteobject.quoteHeader.InternalNotes = basketStructure.internalNotes;
            quoteobject.quoteHeader.SentToEmail = Convert.ToString(basketStructure.customer.IsEmail);
            quoteobject.quoteHeader.SentToSms = Convert.ToString(basketStructure.customer.IsSms);
            quoteobject.quoteHeader.MarginHotel = !String.IsNullOrEmpty(basketStructure.marginHotelVal) ? Convert.ToDecimal(basketStructure.marginHotelVal) : 0;
            quoteobject.quoteHeader.MarginFlight = !String.IsNullOrEmpty(basketStructure.marginFlighVal) ? Convert.ToDecimal(basketStructure.marginFlighVal) : 0;
            quoteobject.quoteHeader.MarginExtras = !String.IsNullOrEmpty(basketStructure.marginExtraVal) ? Convert.ToDecimal(basketStructure.marginExtraVal) : 0;
            quoteobject.quoteHeader.TotalCost = quoteobject.quoteHeader.HotelCost + quoteobject.quoteHeader.FidelityCost + quoteobject.quoteHeader.BaggageCost + quoteobject.quoteHeader.FlightCost + quoteobject.quoteHeader.TransfersCost;
            quoteobject.quoteHeader.GrandTotal = quoteobject.quoteHeader.HotelCost + quoteobject.quoteHeader.FidelityCost + quoteobject.quoteHeader.BaggageCost + quoteobject.quoteHeader.FlightCost + quoteobject.quoteHeader.TransfersCost + quoteobject.quoteHeader.MarginHotel + quoteobject.quoteHeader.MarginFlight + quoteobject.quoteHeader.MarginExtras;
            quoteobject.quoteHeader.PricePerPerson = (quoteobject.quoteHeader.GrandTotal / (basketStructure.searchModel.Adults + basketStructure.searchModel.Child));
            quoteobject.quoteHeader.quotestatus = "QQ";
            SFLogger.logMessage("SearchResultsHelper:GetValuesForQuoteHeaderFromBasketStructure(): end.");
        }

        public Dictionary<string, string> SaveQuote(BasketStructure basketData, List<string> errorMsg)
        {
            SFLogger.logMessage("SearchResultsHelper:SaveQuote(): started!!.");
            var quoteObj = new Quote();
            BookingService serviceRepository = new BookingService();
            var quoteHelper = new QuoteHelper(serviceRepository);
            var result = new Dictionary<string, string>();
            string basketQuoteRefferenceAndID = string.Empty;
            if (basketData != null)
            {
                quoteObj = ConvertBasketToQuote(basketData);
                if (quoteObj.hotelComponent != null && quoteObj.flightComponents != null)
                {
                    quoteHelper.LoadQuoteData(quoteObj);
                    basketQuoteRefferenceAndID = quoteHelper.SaveQuote(quoteObj);
                    if (basketQuoteRefferenceAndID != null)
                    {
                        SFConfiguration.SetQuoteCount(basketQuoteRefferenceAndID);
                        result.Add("customerReferenceCode", basketQuoteRefferenceAndID.Split(',')[0]);
                        result.Add("quoteHeaderId", basketQuoteRefferenceAndID.Split(',')[1]);
                        errorMsg.Add("Saved successfully.");
                    }
                }
            }
            SFLogger.logMessage("SearchResultsHelper:SaveQuote(): end." + result + "," + errorMsg);
            return result;
        }

        public bool ValidateQuoteInput(BasketStructure basketStructure, List<string> errorMsg)
        {
            SFLogger.logMessage("SearchResultsHelper:ValidateQuoteInput(): started!!.");
            bool isInputValid = true;
            //var errorMsg = new List<string>();
            if (basketStructure.flightsReturn == null && basketStructure.flightsOneWayInBound == null && basketStructure.flightsOneWayOutBound == null)
            {
                isInputValid = false;
                errorMsg.Add(ErrorMessages.flightIsEmpty);
            }
            if (basketStructure.flightsReturn == null && (String.IsNullOrEmpty(basketStructure.flightsOneWayInBound.FlightQuoteId) || String.IsNullOrEmpty(basketStructure.flightsOneWayOutBound.FlightQuoteId)))
            {
                if (String.IsNullOrEmpty(basketStructure.flightsOneWayInBound.FlightQuoteId))
                {
                    isInputValid = false;
                    errorMsg.Add(ErrorMessages.inboundFlightIsEmpty);
                }
                if (String.IsNullOrEmpty(basketStructure.flightsOneWayOutBound.FlightQuoteId))
                {
                    isInputValid = false;
                    errorMsg.Add(ErrorMessages.outboundFlightIsEmpty);
                }
            }

            if (String.IsNullOrEmpty(basketStructure.hotel.HotelCode))
            {
                isInputValid = false;
                errorMsg.Add(ErrorMessages.hotelIsEmpty);
            }
            SFLogger.logMessage("SearchResultsHelper:ValidateQuoteInput():end.");
            return isInputValid;
        }



        public bool SavePassengers(List<PassengersModel> passengersModel)
        {
            bool isSuccess = false;
            SFLogger.logMessage("SavePassengers() calling started!!.For User " + SFConfiguration.GetUserID().ToString() + " at " + DateTime.Now.ToString());
            BookingService serviceRepository = new BookingService();
            foreach (var passenger in passengersModel)
            {
                if (!string.IsNullOrEmpty(passenger.DateOfBirth))
                    passenger.DateOfBirth = Convert.ToDateTime(passenger.DateOfBirth).ToString("dd/MM/yyyy");
            }
            var result = serviceRepository.SavePassengerDetails(JsonConvert.SerializeObject(passengersModel));
            isSuccess =Convert.ToBoolean(result);
            SFLogger.logMessage("SavePassengers() calling Ended!!.For User " + SFConfiguration.GetUserID().ToString() + " at " + DateTime.Now.ToString());
            return isSuccess;
        }

        public async Task<bool> SaveCoBrowseDetails(int quoteHeaderId,string quoteReference, string emailId, string leadPassingerName, string url,ControllerContext controllerContext)
        {
            SFLogger.logMessage("BookingController:SaveCoBrowseDetails(): started with QuoteHeaderId : " + quoteHeaderId);
            var mailHelper = new MailHelper();
            bool isMailSent = false;
            url = url.Substring(0, url.LastIndexOf("/"));
            SFLogger.logMessage("SaveCoBrowseDetails() calling started!!.For User " + SFConfiguration.GetUserID().ToString() + " at " + DateTime.Now.ToString());
            var serviceRepository = new BookingService();
            var coBrowseModel = new CoBrowseModel();
            coBrowseModel.QuoteHeaderID = quoteHeaderId;
            coBrowseModel.Email = emailId;
            coBrowseModel.GuId = Guid.NewGuid();
            coBrowseModel.ValidityTime = SFConfiguration.GetCobrowseTime();
            coBrowseModel.CreatedDateTime = DateTime.Now;
            var result = await Task.Run(() => serviceRepository.SaveCoBrowseDetails(JsonConvert.SerializeObject(coBrowseModel))).ConfigureAwait(false);
            SFLogger.logMessage("SaveCoBrowseDetails() calling Ended!!.For User " + SFConfiguration.GetUserID().ToString() + " at " + DateTime.Now.ToString());
            if (result == 1)// For success
            {
                if (!string.IsNullOrEmpty(emailId))
                {
                    try
                    {
                   
                        SFLogger.logMessage("Mail sending to the customer started!!.by User:: " + SFConfiguration.GetUserID().ToString() + " at " + DateTime.Now.ToString());
                        url = url.Substring(0, url.LastIndexOf("/"));
                        NotificationHelper notification = new NotificationHelper();
                        isMailSent= notification.SendLiveShareEMail(leadPassingerName,url, controllerContext, coBrowseModel.GuId, coBrowseModel.QuoteHeaderID,emailId, quoteReference);
                        SFLogger.logMessage("Mail sending to customer completed!!.by User:: " + SFConfiguration.GetUserID().ToString() + " at " + DateTime.Now.ToString());
                    }
                    catch (Exception ex)
                    {
                        SFLogger.logError("BookingController:SaveCoBrowseDetails(): error occured with QuoteHeaderId : " + quoteHeaderId + " at " + DateTime.Now.ToString(), ex);
                    }
                }
                SFLogger.logMessage("BookingController:SaveCoBrowseDetails(): end occured with QuoteHeaderId : " + quoteHeaderId + " at " + DateTime.Now.ToString());
                return isMailSent;
            }

            else
                return isMailSent;
        }


    }
}