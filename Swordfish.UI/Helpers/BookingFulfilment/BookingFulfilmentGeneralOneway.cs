﻿using Swordfish.UI.Models.Booking;
using System;

namespace Swordfish.UI.Helpers.BookingFulfilment
{
    public class BookingFulfilmentGeneralOneway : BookingFulfilmentBase, IBookingFulfilment
    {
        BookingFulfillmentModel bookingFulfilmentModel = null;
        private BookingFulfilmentGeneralOneway() { }
        internal BookingFulfilmentGeneralOneway(BookingFulfillmentModel _value)
        {
            this.bookingFulfilmentModel = _value;
        }
        public void FulfilBooking()
        {
            throw new NotImplementedException();
        }
    }
}