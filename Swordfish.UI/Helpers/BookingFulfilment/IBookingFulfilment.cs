﻿namespace Swordfish.UI.Helpers.BookingFulfilment
{
    public interface IBookingFulfilment
    {
        void FulfilBooking();
    }
}
