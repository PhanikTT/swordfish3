﻿using Swordfish.UI.Models.Booking;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Swordfish.UI.Helpers.BookingFulfilment
{
    public class BookingFulfilmentBase
    {
        public static IBookingFulfilment CreateBookingFulfilment(BookingFulfillmentModel bookingFulfilmentModel)
        {

            IBookingFulfilment bookingFulfilment = null;

            switch (bookingFulfilmentModel.BookingMarketingAirlineTypeCurrent)
            {
                case BookingMarketingAirlineType.GeneralReturns:
                    bookingFulfilment = new BookingFulfilmentGeneralReturn(bookingFulfilmentModel);
                    break;
                case BookingMarketingAirlineType.GeneralOneWay:
                    bookingFulfilment = new BookingFulfilmentGeneralOneway(bookingFulfilmentModel);
                    break;
            }
            return bookingFulfilment;
        }

        protected void LoadQuote() { }
        protected void LoadSFBasket() { }
        protected void LoadSesameBasket() { }
        protected void LoadPassengers() { }
        protected void CreateBasket(string AccoRoomType, string AccoRate, )
        {


        }
        protected void RefreshBasket() { }
        protected void BookBasket() { }

        //protected void CreateEUROasket() { }
        //protected void RefreshEUROBasket() { }
        //protected void BookEUROBasket() { }

        protected void BookTransfers() { }

        protected void RedirectTOEasyJetBooking() { }


    }
}
