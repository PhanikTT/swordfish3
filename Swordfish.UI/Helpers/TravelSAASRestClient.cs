﻿using RestSharp;
using Swordfish.UI.Models.TravelSAAS;
using System.Collections;
using System.Collections.Generic;

namespace Swordfish.UI.Helpers
{
    public enum RequestType
    {
        Get = 0,
        Post = 1,
        Put = 2,
        Delete = 3
    }
    public class TravelSAASRestClient
    {
        private readonly RestClient client;
        private readonly string baseUrl = SFConfiguration.GetTravelSAASWebApiUrl();
        public TravelSAASRestClient()
        {
            client = new RestClient(baseUrl);
        }

        public TravelSAASRestResponse ExecuteRequest(string resourceUrl, RequestType requetType, ArrayList list)
        {
            var responseObj = new TravelSAASRestResponse();
            switch (requetType)
            {
                case RequestType.Get:
                    responseObj = Execute(resourceUrl, list, Method.GET);
                    break;
                case RequestType.Post:
                    responseObj = Execute(resourceUrl, list, Method.POST);
                    break;
                case RequestType.Put:
                    responseObj = Execute(resourceUrl, list, Method.PUT);
                    break;
                case RequestType.Delete:
                    responseObj = Execute(resourceUrl, list, Method.DELETE);
                    break;
                default:
                    break;
            }
            return responseObj;
        }

        private TravelSAASRestResponse Execute(string resourceUrl, ArrayList list, Method method)
        {
            var request = new RestRequest(resourceUrl, method) { RequestFormat = DataFormat.Json };

            if (list != null && list.Count > 0)
            {
                foreach (var item in list)
                {
                    request.AddBody(item);
                }

                try
                {
                    string sessionID = ((Businessobjects.Sesame.SesameRequestBase)list[0]).SessionID;
                    request.AddHeader("SessionId", sessionID);
                }
                catch
                {
                }
            }
            var response = client.Execute(request);
            var restResponseBase = ((RestResponseBase)response);
            var travelSAASRestResponse = new TravelSAASRestResponse();

            travelSAASRestResponse.StatusCode = (int)restResponseBase.StatusCode;
            travelSAASRestResponse.Response = restResponseBase.Content;

            return travelSAASRestResponse;
        }
    }
}