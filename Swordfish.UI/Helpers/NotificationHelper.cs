﻿using Swordfish.Businessobjects.Notification;
using Swordfish.Businessobjects.Quotes;
using System;
using System.Collections.Generic;
using System.Web;
using System.Web.Mvc;
using System.Xml;

namespace Swordfish.UI.Helpers
{
    public class NotificationHelper
    {
        Dictionary<string, string> quoteDictionary = new Dictionary<string, string>();
        MailHelper mailHelper = new MailHelper();
        Email email = new Email();
        public bool SendQuickQuoteByEMail(string quoteHeaderId, ControllerContext controllerContext, string email)
        {
            return mailHelper.SendQuoteByEMail(quoteHeaderId, controllerContext, Constants.EmailTypes.QuickQuoteEmail, email);
        }
        public bool SendBookQuoteByEMail(string quoteHeaderId, ControllerContext controllerContext, string email)
        {
            return mailHelper.SendQuoteByEMail(quoteHeaderId, controllerContext, Constants.EmailTypes.BookQuoteEmail, email);

        }
        public bool SendLiveShareEMail(string leadPassingerName, string url, ControllerContext controllerContext, Guid guid, int quoteHeaderId, string email, string quoteReference)
        {
            return mailHelper.SendLiveShareEMail(leadPassingerName, url, controllerContext, guid, Constants.EmailTypes.SendLiveShareEmail, quoteHeaderId, email, quoteReference);
        }
        public bool SendTopDogFailureEMail(ControllerContext controllerContext, int quoteHeaderId, Quote quoteData,out string emailList)
        {
            return mailHelper.SendTopDogFailureEMail(controllerContext, quoteHeaderId, quoteData, Constants.EmailTypes.TopDogFailureEmail,out emailList);
        }
        public bool SendBookingFullFillmentFailureEmail(ControllerContext controllerContext, int quoteHeaderId, Quote quoteData, out string emailList)
        {
            return mailHelper.SendBookingFullFillmentFailureEmail(controllerContext, quoteHeaderId, quoteData, Constants.EmailTypes.BookingFullFillmentFailureEmail, out emailList);
        }
        public bool SendBookingAcknowledgementEmail(ControllerContext controllerContext, int quoteHeaderId, Quote quoteData, out string emailList)
        {
            return mailHelper.SendBookingAcknowledgementEmail(controllerContext, quoteHeaderId, quoteData, Constants.EmailTypes.BookingAcknowledgementEmail, out emailList);
        }
        public bool SendQuickQuoteBySMS(string quoteHeaderId, string mobileNo)
        {
            return mailHelper.sendQuoteBySms(quoteHeaderId, Constants.SmsTypes.QuickQuoteSMS, mobileNo);
        }
        public bool SendBookQuoteBySMS(string quoteHeaderId, string mobileNo)
        {
            return mailHelper.sendQuoteBySms(quoteHeaderId, Constants.SmsTypes.BookQuoteSMS, mobileNo);
        }
        public bool SendEMailForPasswordRecovery(string callBackURL, string userEmail)
        {
            string recoveryToEmail = SFConfiguration.GetPassWordToEmail().Trim();
            string recoveryFromEmail = SFConfiguration.GetPassWordFromEmail().Trim();
            string path = "~/Xmldata/CustomerCommunication.xml";
            string fromEmailContent = string.Empty;
            string toEmailContent = string.Empty;
            string attachement = string.Empty;
            string filePath = string.Empty;
            if (HttpContext.Current != null)
            {
                filePath = HttpContext.Current.Server.MapPath(@"" + path + "");
            }
            else {
                filePath = HttpRuntime.AppDomainAppPath + path.Replace("~", string.Empty).Replace('/', '\\');
            }
            QuoteCommunication QuoteCommunication = new QuoteCommunication();
            Dictionary<string, string> dictionaryEmailContent = new Dictionary<string, string>();
            XmlDocument document = new XmlDocument();
            document.Load(filePath);
            XmlNode passwordRecoveryEMailNodes = document.SelectSingleNode("/Communications/EmailCmmunication/" + Constants.EmailTypes.PassWordRecoveryEMail + "/Attachment");
            if (passwordRecoveryEMailNodes.InnerText != "")
            {
                attachement = passwordRecoveryEMailNodes.ChildNodes[0].Value.Replace("\r\n", string.Empty).Trim();
            }
            else
            {
                attachement = "";
            }
            if (recoveryToEmail != null && recoveryToEmail != "")
            {
                email.ToEmail = userEmail + "," + recoveryToEmail;
            }
            else
            {
                email.ToEmail = userEmail;
            }
            if (attachement != null && attachement != "")
            {
                email.FileType = ".pdf";
                email.AttachementFileString = attachement;
            }
            email.FromEmail = recoveryFromEmail;
            email.Subject = SFConfiguration.GetPassWordRecoverySubjectLine(); // "Password Recovery";
            email.Body = String.Format(SFConfiguration.GetPassWordRecoveryBody(), callBackURL);// "Please reset your password by <a href='" + callBackURL + "'>Clicking here</a>";
            bool result = mailHelper.SendMail(email);
            return result;
        }
        public bool SendToCustomer(string LeadPassName, string customerUrl, string result, string LeadmailID)
        {
            email.ToEmail = LeadmailID;
            email.Subject = SFConfiguration.GetSubjectforEmailToCustomer();
            email.Body = string.Format(SFConfiguration.GetBodyforEmailToCustomer(), LeadPassName, customerUrl, result);
            bool res = mailHelper.SendMail(email);
            return res;
        }

        public bool SendEmailToCustomers(string strMail, string strFirstName, string strLastName, string strPhoneNumber, string strCustomerInformation)
        {
            email.ToEmail = strMail;
            email.Subject = SFConfiguration.GetSubjectforCustomerDetailsEmail();
            email.Body = string.Format(SFConfiguration.GetBodyforCustomerDetailsEmail(), strFirstName, strLastName, strPhoneNumber, strCustomerInformation);
            bool res = mailHelper.SendMail(email);
            return res;
        }
        public bool SendMailToAgent(string Email, string QuoteID, string AttachementFileString)
        {
            email.ToEmail = Email;
            email.Subject = string.Format(SFConfiguration.getSubjectForAgentEmail(), QuoteID);
            email.Body = SFConfiguration.getBodyForAgentEmail();
            email.FileType = ".pdf";
            email.AttachementFileString = AttachementFileString;
            bool res = mailHelper.SendMail(email);
            return res;
        }
        public string SendSmsAndEmail(string mobile, string Email, ControllerContext ControllerContext, string Quotereference, string EmailType)
        {
            SFLogger.logMessage("NotificationHelper:SendSmsAndEmail(): started!!. Email:" + email + ", Mobile:" + mobile);
            string SmsStatus = "Sms";
            string EmailStatus = "Email";
            if (EmailType == Constants.SmsTypes.BookQuoteSMS.ToString())
            {
                if (!string.IsNullOrEmpty(mobile))
                {
                    bool status = SendBookQuoteBySMS(Quotereference, mobile);
                    SmsStatus = SmsStatus + status.ToString();
                }
                if (!string.IsNullOrEmpty(Email))
                {
                    bool status = SendBookQuoteByEMail(Quotereference, ControllerContext, Email);
                    EmailStatus = EmailStatus + status.ToString();
                }
            }
            else
            {
                if (!string.IsNullOrEmpty(mobile))
                {
                    bool status = SendQuickQuoteBySMS(Quotereference, mobile);
                    SmsStatus = SmsStatus + status.ToString();
                }
                if (!string.IsNullOrEmpty(Email))
                {
                    bool status = SendQuickQuoteByEMail(Quotereference, ControllerContext, Email);
                    EmailStatus = EmailStatus + status.ToString();
                }
            }
            SFLogger.logMessage("NotificationHelper:SendSmsAndEmail(): end. Email:" + email + ", Mobile:" + mobile);
            return SmsStatus.ToString() + "," + EmailStatus.ToString();
        }


    }
}
