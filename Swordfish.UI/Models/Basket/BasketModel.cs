﻿namespace Swordfish.UI.Models.Basket
{
    public class BasketModel
    {
        public string BasketReferenceNumber { get; set; }
        public string CurrencyCode { get; set; }
        public HotelInformation BasketAccomodationDetails { get; set; }
        public FlightInformation BasketFlightDetails { get; set; }
        public TransferInformation BasketTransferDetails { get; set; }
        public int BagsCount { get; set; }
    }

    public class HotelInformation
    {
        public string HotelComponentReference { get; set; }
        public string  HotelDescription { get; set; }
        public string  HotelProductTypeCode { get; set; }
        public string  HotelSellingPricePence { get; set; }
        public string  HotelSourcePricePence { get; set; }
        public string  HotelSupplierCode { get; set; }
        public string  HotelBoardCode { get; set; }
        public string  HotelRoomCode { get; set; }
        public string  HotelRoomDescription { get; set; }
        public string ErrataInformation { get; set; }
    }

    public class FlightInformation
    {
        public string FlightComponentReference { get; set; }
        public string FlightProductTypeCode { get; set; }
        public string FlightDescrption { get; set; }
        public string FlightSellingPricePence { get; set; }
        public string FlightSourcePricePence { get; set; }
        public string FlightSupplierCode { get; set; }

        public string OutboundFlightFrom { get; set; }
        public string OutboundFlightTo { get; set; }
        public string OutboundDepartureTime { get; set; }
        public string OutboundArrivalTime { get; set; }

        public string InboundFlightFrom { get; set; }
        public string InboundFlightTo { get; set; }
        public string InboundDepartureTime { get; set; }
        public string InboundArrivalTime { get; set; }
        public string BaggageCode { get; set; }
        public int BaggageQuantity { get; set; }
        public string ErrataInformation { get; set; }
    }

    public class TransferInformation
    {
    }
}