﻿namespace Swordfish.UI.Models.Basket
{
    public class WrapperModel
    {
        public string QuoteReference { get; set; }
        public string BasketReference { get; set; }
        public int Adults { get; set; }
        public int Childs { get; set; }
        public int Infants { get; set; }
        public string CurrencyCode { get; set; }
        public string Destination { get; set; }
        public string Source { get; set; }
        public string DaysQuoteDescription { get; set; }
        public string FlightQuoteReference { get; set; }
        public string HotelQuoteReference { get; set; }

        public string FlightCode { get; set; }
        public string OutboundDepartureTime { get; set; }
        public string OutboundArrivalTime { get; set; }
        public string InboundDepartureTime { get; set; }
        public string InboundArrivalTime { get; set; }
        public string OutboundFlightDetails { get; set; }
        public string InboundFlightDetails { get; set; }
        public decimal FlightFare { get; set; }
        public string OutboundFlightFrom { get; set; }
        public string OutboundFlightTo { get; set; }
        public string InboundFlightFrom { get; set; }
        public string InboundFlightTo { get; set; }
        public string MarketingAirLane { get; set; }
        public string FlightSupplierCode { get; set; }

        public string HotelCode { get; set; }
        public string HotelCodeContext { get; set; }       
        public string BoardType { get; set; }       
        public int TotalNights { get; set; }
        public int RoomsRequired { get; set; }     
        public string TtiiCode { get; set; }       
        public string RoomDescription { get; set; }
        public string HotelName { get; set; } 
        public string HotelCityCode { get; set; }
        public string HotelCity { get; set; }
        public string AccommodationName { get; set; }
        public decimal AccommodationFare { get; set; }
        public string RoomTypeCode { get; set; }
        public string RoomRateQuote { get; set; }

        public string BaggageQuoteReference { get; set; }
        public int BagsCount { get; set; }
        public string TotalBaggageFare { get; set; }
        public string AmountPerPerson { get; set; }
        public string TotalFare { get; set; }
        public string MarginAmount { get; set; }
        public string DepositAmount { get; set; }
        public string ResultMessage { get; set; }
        public string ActionType { get; set; }

        public decimal TransfersFare { get; set; }
        public string TransfersProductId { get; set; }
        public string TransfersDescription { get; set; }
        public int TransfersQuantity { get; set; }
        public string ErrataInformationForHotel { get; set; }
        public string ErrataInformationForFlight { get; set; }
    }
}
