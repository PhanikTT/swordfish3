﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SwordFish.Models.Basket
{
    public static class Configure
    {
        public static string flightComponentTypeForReturs { get; } = "Flight Return";
        public static string flightComponentTypeForOneWay { get; } = "Flight one way";
        public static string flightComponentTypeForOutBound { get; } = "Outbound";
        public static string flightComponentTypeForInBound { get; } = "Inbound";
        public static string accommodationType { get; } = "Accommodation";
        public static string flightComponentTypeForRoundTrip { get; } = "RoundTrip";
    }
}