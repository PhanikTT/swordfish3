﻿using Swordfish.Businessobjects.Customers;
using Swordfish.Businessobjects.Membership;
using Swordfish.UI.Models.Account;
using Swordfish.UI.Models.Flight;
using System;
using System.Collections.Generic;

namespace Swordfish.UI.Models.Basket
{
    public class BasketStructure
    {
        public bool isDirty { get; set; }
        public string internalNotes { get; set; }
        public string marginExtraVal { get; set; }
        public string marginFlighVal { get; set; }
        public string marginHotelVal { get; set; }
        public decimal fidelityVal { get; set; }
        public Baggage baggage { get; set; }
        public Customer customer { get; set; }
        public FlightModel flightsOneWayInBound { get; set; }
        public FlightModel flightsOneWayOutBound { get; set; }
        public FlightModel flightsReturn { get; set; }
        public HotelModel hotel { get; set; }
        public SearchModel searchModel { set; get; }
        public List<TransfersModel> transfersdata { set; get; }
        public UserInfo userInfo { set; get; }
    }

    public class HotelModel
    {
        public string AmountAfterTax { get; set; }
        public string BoardType { get; set; }
        public string HotelCity { get; set; }
        public string HotelCode { get; set; }
        public string HotelCodeContext { get; set; }
        public string HotelName { get; set; }       
        public string PricePerPerson { get; set; }
        public string RateQuoteId { get; set; }
        public double Rating { get; set; }
        public string ReferenceId { get; set; }
        public string ReferenceType { get; set; }
        public string ResortName { get; set; }  
        public string RoomDescription { set; get; }
        public string RoomTypeCode { get; set; }
        public string SupplierResortId { get; set; }
        public string TTIcode { get; set; } 
        public DateTime? checkIn { get; set; }
        public DateTime? checkOut { get; set; }
        public int? nights { get; set; }
        public string RatePlanName { get; set; }
        public double TripAdvisorScore { get; set; }
        public PAXCount PAXCount { get; set; }
        public List<RoomRates> RoomRates { get; set; }
        public List<RoomsInfo> roomsInfo { get; set; }
        public List<string> boardTypes { get; set; }
        public List<BoardRates> boardRates { get; set; }
    }

    public class RoomsInfo
    {
        public int adultsCount { get; set; }
        public int childCount { get; set; }
        public int infantCount { get; set; }
        public string boardType { get; set; }
        public string roomType { get; set; }
    }
    public class RoomRates
    {
        public string AmountAfterTax { get; set; }
        public string QuoteId { get; set; }
        public string RatePlanCode { get; set; }
        public string RatePlanName { get; set; }
        public string RoomDescription { get; set; }
        public string RoomTypeCode { get; set; }
        public bool? NonRefundable { get; set; }
        public bool? CancelPolicyIndicator { get; set; }
    }
    public class BoardRates
    {
        public string roomDescription { get; set; }
        public List<RoomRates> boardPrices { set; get; }
    }
    public class PAXCount
    {
        public int AdultsCount { get; set; }
        public int InfantCount { get; set; }
        public int ChildCount { get; set; }
    }

    public class TransfersModel
    {
        public List<General> general { get; set; }
        public List<Pricing> pricing { get; set; }
        public string transfersImg { get; set; }
    }

    public class General
    {
        public int productid { get; set; }
        public int bookingtypeid { get; set; }
        public int transfertime { get; set; }
        public int producttypeid { get; set; }
        public int minpax { get; set; }
        public int maxpax { get; set; }
        public int luggage { get; set; }
        public int perperson { get; set; }
        public string producttype { get; set; }
        public string description { get; set; }
        public string transferType { get; set; }
        public int transferTypeOptionId { get; set; }

    }
    public class Pricing
    {
        public object oldprice { get; set; }
        public decimal price { get; set; }
        public string currency { get; set; }
        public List<Price> prices { get; set; }
    }
    public class Price
    {
        public string type { get; set; }
        public string typecode { get; set; }
        public string description { get; set; }
        public string units { get; set; }
        public string oldprice { get; set; }
        public string price { get; set; }
    }

    public class Baggage
    {
        public bool isTwoWay { set; get; }
        public decimal totalPrice { set; get; }
        public List<SelectedBaggage> OutboundSelectedBaggage { get; set; }
        public List<BaggageSlab> outboundBaggageAllSlabs { get; set; }
        public List<SelectedBaggage> inboundSelectedBaggage { get; set; }
        public List<BaggageSlab> inboundBaggageAllSlabs { get; set; }
        public List<SelectedBaggage> selectedRoundTripBaggage { get; set; }
        public List<BaggageSlab> roundTripBaggageAllSlabs { get; set; }
    }

    public class SelectedBaggage
    {
        public BaggageSlabs baggage { get; set; }
        public int passengerIndex { set; get; }
    }

    public class BaggageSlabs
    {
        public List<BaggageSlab> slab { set; get; }
    }
    public class BaggageSlab
    {
        public string BuyingPricePence { get; set; }
        public string Code { get; set; }
        public string Description { get; set; }
        public string QuantityRequired { get; set; }
        public string SellingPricePence { get; set; }
        public string Type { get; set; }
        public string QuantityAvailable { get; set; }
        public string Items { get; set; }
    }

}