﻿using Newtonsoft.Json;
using Swordfish.UI.Helpers;
using Swordfish.UI.Models.Account;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Web;

namespace Swordfish.UI.Models.Package
{
    public static class MyCacheSource
    {
        private static List<DestinationInfo> items = null;
        private static List<SourceInfo> SourceItems = null;

        public static IEnumerable<DestinationInfo> GetHotelDestinations(string searchterm, int local = 10)
        {
            string la = SFConfiguration.Get10();
            if (System.Web.HttpRuntime.Cache["desti"] == null || items == null)
            {
                using (StreamReader r = new StreamReader(HttpContext.Current.Server.MapPath("~/JsonData/destination.json")))
                {
                    string json = r.ReadToEnd();
                    items = JsonConvert.DeserializeObject<List<DestinationInfo>>(json);
                    System.Web.HttpRuntime.Cache["desti"] = items;

                    System.Web.HttpRuntime.Cache.Insert("desti", items, null, DateTime.Now.AddMinutes(240), TimeSpan.Zero);
                }
            }
            //  var filterResults = (List<DestinationInfo>)(System.Web.HttpRuntime.Cache["desti"]);
            int maxresults = 0;
            int.TryParse(SFConfiguration.GetMaxResults(), out maxresults);
            var returnResults = (from fr in items
                                 where ((fr.dest.ToLower().Contains(searchterm.ToLower()) || fr.dest.ToLower().StartsWith(searchterm.ToLower()) || fr.dest.ToLower().EndsWith(searchterm.ToLower()) || fr.d.ToLower().Equals(searchterm.ToLower())) && fr.lo.Equals(la))
                                 orderby fr.pr ascending
                                 select fr).Take(maxresults);
            return returnResults;
        }
        public static IEnumerable<SourceInfo> GetAirportSources(string searchterm, int local = 10)
        {
            try
            {

                string la = ConfigurationManager.AppSettings["10"];
                if (System.Web.HttpRuntime.Cache["sour"] == null || SourceItems == null)
                {
                    using (StreamReader r = new StreamReader(HttpContext.Current.Server.MapPath("~/JsonData/Source.json")))
                    {
                        string json = r.ReadToEnd();
                        SourceItems = JsonConvert.DeserializeObject<List<SourceInfo>>(json);
                        System.Web.HttpRuntime.Cache["sour"] = SourceItems;

                        System.Web.HttpRuntime.Cache.Insert("sour", SourceItems, null, DateTime.Now.AddMinutes(240), TimeSpan.Zero);
                    }
                }
                int maxresults = 0;
                int.TryParse(ConfigurationManager.AppSettings["MaxResults"], out maxresults);
                var returnResults = (from fr in SourceItems
                                     where ((fr.desc.ToLower().Contains(searchterm.ToLower()) || fr.desc.ToLower().StartsWith(searchterm.ToLower()) || fr.desc.ToLower().EndsWith(searchterm.ToLower()) || fr.code.ToLower().Equals(searchterm.ToLower())) && fr.lo.Equals(la))
                                     orderby fr.pr ascending
                                     select fr).Take(maxresults);
                return returnResults;
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        public static IEnumerable<DestinationInfo> GetAirPortsList(int local = 10)
        {
            string la = SFConfiguration.Get10();
            if (System.Web.HttpRuntime.Cache["desti"] == null || items == null)
            {
                using (StreamReader r = new StreamReader(HttpContext.Current.Server.MapPath("~/JsonData/destination.json")))
                {
                    string json = r.ReadToEnd();
                    items = JsonConvert.DeserializeObject<List<DestinationInfo>>(json);
                    System.Web.HttpRuntime.Cache["desti"] = items;

                    System.Web.HttpRuntime.Cache.Insert("desti", items, null, DateTime.Now.AddMinutes(240), TimeSpan.Zero);
                }
            }
            int maxresults = 0;
            int.TryParse(SFConfiguration.GetMaxResults(), out maxresults);
            var returnResults = (from fr in items
                                 orderby fr.pr ascending
                                 select fr).Take(maxresults);
            return returnResults;
        }
    }
}