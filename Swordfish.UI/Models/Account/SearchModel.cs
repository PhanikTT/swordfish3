﻿using Swordfish.Businessobjects.Sesame;
using Swordfish.Businessobjects.TTSS;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Swordfish.UI.Models.Account
{
    public class SearchModel : SesameRequestBase
    {
        [Required]
        public string SourceFrom { get; set; }
        public string SourceFromId { get; set; }

        [Required]
        public string DestinationTo { get; set; }
        public string DestinationToId { get; set; }

        [Required]
        public string CheckIn { get; set; }
        [Required]
        public string CheckOut { get; set; }
        public string RoomType { get; set; }
        public string HotelName { get; set; }
        public int Rooms { get; set; }
        public int Adults { get; set; }
        public int Child { get; set; }
        public int Infants { get; set; }
        [Required]
        public int Nights { get; set; }
        public int Bags { get; set; }
        public string ResultType { get; set; }
        public string FlightWay { get; set; }
        public decimal Average { get; set; }
        public bool Transfer { get; set; }
        public decimal Rating { get; set; }
        public string BoardType { get; set; }

        public bool BoardTypeAI { get; set; }
        public bool BoardTypeFB { get; set; }
        public bool BoardTypeHB { get; set; }
        public bool BoardTypeBB { get; set; }
        public bool BoardTypeSC { get; set; }
        public bool BoardTypeRO { get; set; }
        public string ttssquoteId { get; set; }

        public string sfquoteId { get; set; }
        public string ErrorMessage { get; set; }
        public string ParentBasketId { get; set; }
        public int MaxRooms { get; set; }

        public int MaxAdultsInRoom { get; set; }
        public int MaxPaxInRoom { get; set; }
        public int MaxChildInRoom { get; set; }
        public int MaxInfantInRoom { get; set; }
        public int MaxPaxPerBooking { get; set; }
        public string Hotel { get; set; }
        public string TTSSQuoteReference { set; get; }
        public string TTSSListedPricePerPerson { set; get; }
        public string IncomingPhoneNumber { set; get; }
        public string CustomerFirstname { set; get; }
        public string CustomerLastName { set; get; }
        public string CustomerEmail { set; get; }

        public List<Occupancy> OccupancyRooms { get; set; }
        public TTSSQuote TTSSQuote { get; set; }

        public string ModifiedCheckIn { get; set; }
        public string ModifiedCheckOut { get; set; }
        public string HotelKey { set; get; }
        public string MasterHotelId { set; get; }
        public string TTICode { set; get; }
        public string SupplierAccommId { set; get; }

        public SearchModel()
        {
            OccupancyRooms = new List<Occupancy>();
        }
    }
    public class Occupancy
    {
        public Occupancy()
        {
            ChildAge = new List<int>();
            InfantAge = new List<int>();
        }
        public int AdultsCount { get; set; }
        public int ChildCount { get; set; }
        public List<int> ChildAge { get; set; }
        public int InfantCount { get; set; }
        public List<int> InfantAge { get; set; }
        public int RoomNo { set; get; }
    }
    
}