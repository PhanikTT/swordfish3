﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Web.Security;

namespace Swordfish.UI.Models.Account
{
    public class LoginModel
    {
        [Required(ErrorMessage = "Email required")]
        [Display(Name = "Email")]
        [EmailAddress]
        public string Username { get; set; }

        [Required(ErrorMessage = "Password required")]
        [DataType(DataType.Password)]
        [StringLength(15)]
        //[MembershipPassword(
        //    MinRequiredNonAlphanumericCharacters = 1,
        //    MinNonAlphanumericCharactersError = "Your password needs to contain at least one symbol (!, @, #, etc).",
        //    MinRequiredPasswordLength = 8,
        //    ErrorMessage = "Your password must be 8 characters long and contain at least one symbol (!, @, #, etc)."
        //)]
        [Display(Name = "Password")]
        public string Password { get; set; }

        [Display(Name = "Remember me?")]
        public bool RememberMe { get; set; }

        public string ErrorMessage { get; set; }

        public string ErrorType { get; set; }
    }

    public class RegisterModel
    {
        [Required]
        [Display(Name = "Email")]
        [EmailAddress]
        public string Username { get; set; }

        [Required]
        [DataType(DataType.Password)]
        [Display(Name = "Password")]
        public string Password { get; set; }

        [DataType(DataType.Password)]
        [Display(Name = "Confirm password")]
        [Compare("Password", ErrorMessage = "The password and confirmation password do not match.")]
        public string ConfirmPassword { get; set; }

        public string MobileNo { get; set; }
        public int RoleId { get; set; }
        public string PasswordQuestion { get; set; }
        public string PasswordAnswer { get; set; }
        public bool IsApproved { get; set; }
        public bool IsLockedOut { get; set; }
    }

    public class PaswordRecoveryModel
    {
        [Required]
        [Display(Name = "Email")]
        [EmailAddress]
        public string Username { get; set; }

        [Required]
        [DataType(DataType.Password)]
        [Display(Name = "Password")]
        public string Password { get; set; }

        [DataType(DataType.Password)]
        [Display(Name = "Confirm password")]
        [Compare("Password", ErrorMessage = "The password and confirmation password do not match.")]
        public string ConfirmPassword { get; set; }
    }

    public class MembershipModel
    {
        public string UserId { get; set; }
        public string Username { get; set; }
        public string Email { get; set; }
        public string MobileNo { get; set; }
        public string RoleId { get; set; }
        public string RoleName { get; set; }
        public string IsApproved { get; set; }
        public string IsLockedOut { get; set; }
        public string CreateDate { get; set; }
        public string LastLoginDate { get; set; }
        public string LastPasswordChangedDate { get; set; }
        public string LastLockedOutDate { get; set; }
        public string FailedPasswordAttemptCount { get; set; }
        public string FailedPasswordAttemptWindowStart { get; set; }
        public string FailedPasswordAnswerAttemptCount { get; set; }
        public string FailedPasswordAnswerAttemptWindowStart { get; set; }
        public string Comment { get; set; }
    }

    public class ForgotPasswordViewModel
    {
        [Required(ErrorMessage = "Email required")]
        [Display(Name = "Email")]
        [EmailAddress]
        public string Email { get; set; }

        public string ErrorMessage { get; set; }

        public string ErrorType { get; set; }
    }

    public class ResetPasswordViewModel
    {
        [Required(ErrorMessage = "Email required")]
        [EmailAddress]
        [Display(Name = "Email")]
        public string Email { get; set; }

        [Required(ErrorMessage = "Enter password")]
        [DataType(DataType.Password)]
        [StringLength(15, MinimumLength = 6, ErrorMessage = "Password must be greater than 6 characters and less than 15 characters.")]
        [MembershipPassword(
            MinRequiredNonAlphanumericCharacters = 1,
            MinNonAlphanumericCharactersError = "Your password needs to contain at least one symbol (!, @, #, etc).",
            MinRequiredPasswordLength = 6,
            ErrorMessage = "Your password needs to contain atleast one symbol (!, @, #, etc)."
        )]
        [Display(Name = "Password")]
        public string Password { get; set; }

        [DataType(DataType.Password)]
        [Display(Name = "Confirm password")]
        [Required(ErrorMessage = "Enter confirm password.")]
        [Compare("Password", ErrorMessage = "Password and confirm password does not match.")]
        public string ConfirmPassword { get; set; }

        public string ErrorMessage { get; set; }

        public string ErrorType { get; set; }
    }

    public class RoleModel
    {
        public int RoleId { get; set; }

        [Required]
        [Display(Name = "Role Name")]
        public string RoleName { get; set; }

        [Display(Name = "Description")]
        public string Description { get; set; }

        public int CreatedBy { get; set; }
        public DateTime CreatedOn { get; set; }
        public int UpdatedBy { get; set; }
        public DateTime UpdatedOn { get; set; }
    }
}