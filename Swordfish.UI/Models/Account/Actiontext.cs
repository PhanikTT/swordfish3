﻿using System;

namespace Swordfish.UI.Models.Account
{
    public class Actiontext
    {
        public string TaskText { get; set; }

        public string WeekDate { get; set; }

        public int[] TaskPrimaryKeyid { get; set; }

        public string Type { get; set; }


        public string reminderdate { get; set; }
        public Int16 Is_deleted { get; set; }

        public string reminder_status { get; set; }
        public Int16 reminder { get; set; }
    }
}