﻿using System.ComponentModel.DataAnnotations;

namespace Swordfish.UI.Models.Account
{
    public class UserModel
    {
        [Required]
        public string Username { get; set; }

        [Required]
        public string FirstName { get; set; }

        [Required]
        public string LastName { get; set; }

        [Required]
        public string Password { get; set; }

        public string ErrorMessage { get; set; }

        [Required(ErrorMessage = "Please enter syntech code")]
        public string syntecAccountID { get; set; }
    }
}
