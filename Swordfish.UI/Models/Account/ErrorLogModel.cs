﻿namespace Swordfish.UI.Models.Account
{
    public class ErrorLogModel
    {
        public string Environment { get; set; }
        public string SessionId { get; set; }
        public string Message { get; set; }
        public string InputParams { get; set; }
    }
}
