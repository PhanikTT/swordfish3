﻿namespace Swordfish.UI.Models.Account
{
    public class SourceInfo
    {
        public string desc { get; set; }
        public string lo { get; set; }
        public string code { get; set; }
        public int r { get; set; }
        public int h { get; set; }
        public int pr { get; set; }
    }
}
