﻿namespace Swordfish.UI.Models.Account
{
    public class DestinationInfo
    {
        public string dest { get; set; }
        public string lo { get; set; }
        public string d { get; set; }
        public int r { get; set; }
        public int h { get; set; }
        public int pr { get; set; }
    }
}
