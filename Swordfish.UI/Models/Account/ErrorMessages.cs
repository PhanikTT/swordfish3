﻿namespace Swordfish.UI.Models.Account
{
    public static class ErrorMessages
    {
        public const string flightIsEmpty = "Select flights";
        public const string inboundFlightIsEmpty = "Select outbound flight";
        public const string outboundFlightIsEmpty = "Select Flight";
        public const string hotelIsEmpty = "Select Hotel";

        public const string success = "Success";
        public const string failed = "Failed";
    }
}