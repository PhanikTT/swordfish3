﻿using System.Security.Principal;
using System.Web.Security;

namespace Swordfish.UI.Models.Account
{
    public class UserIdentity : IIdentity
    {
        private readonly FormsAuthenticationTicket _ticket;

        public UserIdentity(FormsAuthenticationTicket ticket)
        {
            _ticket = ticket;
        }

        public string AuthenticationType
        {
            get { return "User"; }
        }

        public bool IsAuthenticated
        {
            get { return true; }
        }

        public string Name
        {
            get { return _ticket.Name; }
        }

        public string UserId
        {
            get { return _ticket.UserData; }
        }

        public IIdentity Identity
        {
            get { return this; }
        }
    }
}