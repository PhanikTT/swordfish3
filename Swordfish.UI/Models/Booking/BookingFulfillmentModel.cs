﻿using Swordfish.Businessobjects.Easyjet;
using Swordfish.Businessobjects.Quotes;
using Swordfish.Businessobjects.TopDog;
using Swordfish.Businessobjects.Transfers;
using System.Collections.Generic;

namespace Swordfish.UI.Models.Booking
{
    public partial class BookingFulfillmentModel
    {
        public BookingFulfillmentModel()
        {
            Flights = new List<BookingFulfillmentFlight>();
            QuoteData = new Quote();
            Transfers = new Transfersdata();
            TopDogResponse = new List<TopDogResponse>();
            EasyJetBookingStatus = new List<EasyJetBookingStatus>();
        }
        //Properties added to track 
        public FlightBookingType FlightBookingTypeCurrent;
        public BookingMarketingAirlineType BookingMarketingAirlineTypeCurrent;

        public string BookingType { get; set; }
        public string QuoteHeaderId { get; set; }

        public BookingFulfillmentHotel Hotel { get; set; }
        public List<BookingFulfillmentFlight> Flights { get; set; }
        public Transfersdata Transfers { get; set; }
        public Quote QuoteData { get; set; }
        public List<NonEjStatusCodes> NonEjHotelStatusCodes { get; set; }
        public List<NonEjStatusCodes> NonEjFlightStatusCodes { get; set; }
        public int TransferStatusCodes { get; set; }
        public string TransferStatusMessage { get; set; }
        public int ComponentTypeOptionId { get; set; }
        public string ReturnButtonText { get; set; }
        public string OutBoundButtonText { get; set; }
        public string InBoundButtonText { get; set; }
        public bool IsEasyjetWexcardCreated { get; set; } = true;
        public List<TopDogResponse> TopDogResponse { get; set; }
        public List<EasyJetBookingStatus> EasyJetBookingStatus { get; set; }
        public int IsFromSubmitted { get; set; }
        public string FulfillmentTransactionSummary { get; set; }
        public bool FlightFailureStatus { get; set; }
        public bool HotelFailureStatus { get; set; }
        public bool TransfersFailureStatus { get; set; }



    }
    public class NonEjStatusCodes
    {
        public int StatusCode { get; set; }
        public string RefCode { get; set; }
        public string ComponentError { get; set; }
        public int DirectionOptionId { get; set; }
    }
    public class BookingFulfillmentHotel
    {
        public string HotelName { get; set; }
        public string ResortName { get; set; }
        public string HotelDescription { get; set; }
        public decimal StarRating { get; set; }
    }
    
    public class BookingFulfillmentFlight
    {
        public string DepartureAirport { get; set; }
        public string ArrivalAirport { get; set; }
        public string MarketingAirline { get; set; }
        public string MarketingAirlineCode { get; set; }
        public int ComponentTypeOptionId { get; set; }
        public int DirectionOptionId { get; set; }
    }
    public class Transfersdata
    {
        public TransfersBookGeneral general { get; set; }
        public TransfersBookContact contact { get; set; }
        public TransfersBookPricing pricing { get; set; }
        public List<TransferBook> transferbookdata { get; set; }

    }
    public enum FlightBookingType
    {
        Returns = 3,
        OneWay = 4
    }

    public enum BookingMarketingAirlineType
    {
        GeneralReturns,
        GeneralOneWay,
        EasyJetReturns,
        EasyJetOneWay,
        EasyJetOutBound,
        EasyJetInbound
    }
}