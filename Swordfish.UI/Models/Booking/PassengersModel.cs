﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Swordfish.UI.Models.Booking
{
    public class PassengersModel
    {
        public string QuoteRef { get; set; }

        public int BasketId { get; set; }

        public string Title { get; set; }

        [Required]
        public string FirstName { get; set; }

        [Required]
        public string LastName { get; set; }

        [Required]
        [EmailAddress]
        public string Email { get; set; }

        public string PassengerType { get; set; }

        [Required]
        [Phone]
        public string Mobile { get; set; }

        [Required]
        public string Telephone { get; set; }

        [Required]
        public string PostCode { get; set; }

        [Required]
        public string Address1 { get; set; }
        public string Address2 { get; set; }
        public string Address3 { get; set; }

        public string HouseNumber { get; set; }
        public string CityTown { get; set; }
        public string Country { get; set; }

        public string PaymentPostCode { get; set; }
        public string PaymentAddress1 { get; set; }
        public string PaymentAddress2 { get; set; }
        public string PaymentAddress3 { get; set; }

        public string DateOfBirth { get; set; }
        public string PassportNumber { get; set; }

        public bool IsCardHolderAddress { get; set; }

        [Required]
        public string NameOnCard { get; set; }

        [Required]
        public string CardNumber { get; set; }

        [Required]
        public string CardExpiryMonth { get; set; }

        [Required]
        public string CardExpiryYear { get; set; }

        [Required]
        public string CVV { get; set; }

        public string CardType { get; set; }

        public bool IsAcceptedTerms { get; set; }        

        public string CoPassengers { get; set; }

        public decimal AmountPaid { get; set; }


        public int UserId { get; set; }

        public List<CoPassengersModel> CoPassengersList { get; set; }

        public string ErrorMessage { get; set; }

        public string ErrorType { get; set; }

        public decimal FlightFare { get; set; }
        public decimal HotelFare { get; set; }
        public string BasketQuoteRefreshReference { get; set; }
        public bool sendOffersDetailsOnEmail { get; set; }
        public bool sendOffersDetailsOnSMS { get; set; }
        public bool isLeadPssenger { get; set; }
        public string QouteHeaderID { get; set; }
        public int SeqNo { get; set; }
        public BookingModel BookingInfo { get; set; }

        public List<CardTypes> cardTypes { get; set; }
    }

    public class CoPassengersModel
    {
        public string Title { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Email { get; set; }
        public string DateOfBirth { get; set; }
        public string PassportNumber { get; set; }
        public string PassengerType { get; set; }
    }

    public class CardTypes
    {
       public int PaymentTypeID { get; set; }
        public string PaymentType { get; set; }
        public string Description { get; set; }
        public string PaymentRule { get; set; }

        public string CardChargesDesc { get; set; }
        public string CardTypeCode { get; set; }
        public float Price { get; set; }
        public Int16 IsActive { get; set; }
    
    }
}