﻿using Swordfish.UI.Helpers;
using Swordfish.Businessobjects.Common;
using System.Collections.Generic;
using Swordfish.Businessobjects.Flights;
using System.Linq;
using System;

namespace Swordfish.UI.Models.Booking
{
    public partial class BookingFulfillmentModel
    {

        #region Hotel Details helper methods
        public string GetHotelName()
        {
            return string.Empty;
        }

        public string GetResortName()
        {
            return string.Empty;
        }

        public string HotelDescription() { return string.Empty; }

        public string StarRating() { return string.Empty; }
        #endregion

        #region Flight Details helper methods
        public string DepartureAirport() { return string.Empty; }
        public string ArrivalAirport() { return string.Empty; }
        public string MarketingAirline() { return string.Empty; }
        public string MarketingAirlineCode() { return string.Empty; }
        //public int ComponentTypeOptionId() { return 0; }
        public int DirectionOptionId() { return 0; }
        #endregion

        public void LoadQuote(string quoteHeaderId)
        {
            CommonHelper commonHelper = new CommonHelper();
            this.QuoteData = commonHelper.LoadQuoteData(quoteHeaderId);
            this.QuoteData.SesameBookBasket  = commonHelper.GetComponentDetails(Convert.ToInt32(quoteHeaderId), string.Empty, SFConstants.SesameBasket_Book);
            CheckAndSetFlightType();
            CheckAndSetBookingMarketingAirlineType();
        }
        /// <summary>
        /// Checking booking type as Easyjet or General or Hybrid
        /// </summary>
        public  bool IsFlightBookingReturns()
        {
            bool retValue = false;
            // Case Returns
            if (this.QuoteData.flightComponents[0].ComponentTypeOptionId == SFConstants.QuoteComponent_FlightReturn)
            { retValue = true; }
            return retValue;
        }

        public  bool IsFlightBookingOneWay()
        {
            bool retValue = false;
            // Case Returns
            if (this.QuoteData.flightComponents.Count == 2)
            { retValue = true; }
            return retValue;
        }

        private void CheckAndSetFlightType()
        {
            if (this.IsFlightBookingOneWay())
                this.FlightBookingTypeCurrent = FlightBookingType.OneWay;
            else if (this.IsFlightBookingReturns())
                this.FlightBookingTypeCurrent = FlightBookingType.Returns;
        }

        private void CheckAndSetBookingMarketingAirlineType()
        {
            // Case returns
            if (IsFlightBookingReturns())
            {
                foreach (var flightDetails in this.QuoteData.flightComponents[0].FlightComponentDetails)
                {
                    if (flightDetails.MarketingAirlineCode == SFConstants.EasyJetMarketingAirlineCode)
                    {
                        this.BookingMarketingAirlineTypeCurrent = BookingMarketingAirlineType.EasyJetReturns;
                        break;
                    }
                    else
                    {
                        this.BookingMarketingAirlineTypeCurrent = BookingMarketingAirlineType.GeneralReturns;
                        break;
                    }
                }
            }
            // case one way
            else if (IsFlightBookingOneWay())
            {
                List<FlightComponentDetail> inBoundFlights = GetFlightsForOneWay(SFConstants.Direction_Inbound);
                List<FlightComponentDetail> outBoundFlights = GetFlightsForOneWay(SFConstants.Direction_Outbound);

                if (outBoundFlights.Any(f => f.MarketingAirlineCode == SFConstants.EasyJetMarketingAirlineCode) && inBoundFlights.Any(f => f.MarketingAirlineCode == SFConstants.EasyJetMarketingAirlineCode))
                {
                    this.BookingMarketingAirlineTypeCurrent = BookingMarketingAirlineType.EasyJetOneWay;
                }
                else if (outBoundFlights.Any(f => f.MarketingAirlineCode == SFConstants.EasyJetMarketingAirlineCode) && inBoundFlights.Any(f => f.MarketingAirlineCode != SFConstants.EasyJetMarketingAirlineCode))
                {
                    this.BookingMarketingAirlineTypeCurrent = BookingMarketingAirlineType.EasyJetOutBound;
                }
                else if (outBoundFlights.Any(f => f.MarketingAirlineCode != SFConstants.EasyJetMarketingAirlineCode) && inBoundFlights.Any(f => f.MarketingAirlineCode == SFConstants.EasyJetMarketingAirlineCode))
                {
                    this.BookingMarketingAirlineTypeCurrent = BookingMarketingAirlineType.EasyJetInbound;
                }
                else
                    this.BookingMarketingAirlineTypeCurrent = BookingMarketingAirlineType.GeneralOneWay;
            }
        }


        private List <FlightComponentDetail> GetOutBoundFlightsForOneWay()
        {
            List<FlightComponentDetail> outBoundFlights = null;

            if (IsFlightBookingOneWay())
            {
                if (this.QuoteData.flightComponents[0].FlightComponentDetails[0].DirectionOptionId == SFConstants.Direction_Outbound)
                    outBoundFlights = this.QuoteData.flightComponents[0].FlightComponentDetails;
                else if(this.QuoteData.flightComponents[0].FlightComponentDetails[0].DirectionOptionId == SFConstants.Direction_Outbound)
                    outBoundFlights = this.QuoteData.flightComponents[0].FlightComponentDetails;
            }
            return outBoundFlights;
        }

        private List<FlightComponentDetail> GetInBoundFlightsForOneWay()
        {
            List<FlightComponentDetail> outBoundFlights = null;

            if (IsFlightBookingOneWay())
            {
                if (this.QuoteData.flightComponents[0].FlightComponentDetails[0].DirectionOptionId == SFConstants.Direction_Outbound)
                    outBoundFlights = this.QuoteData.flightComponents[0].FlightComponentDetails;
                else if (this.QuoteData.flightComponents[0].FlightComponentDetails[0].DirectionOptionId == SFConstants.Direction_Outbound)
                    outBoundFlights = this.QuoteData.flightComponents[0].FlightComponentDetails;
            }
            return outBoundFlights;
        }

        private List<FlightComponentDetail> GetFlightsForOneWay(int  DirectionOptionId)
        {
            List<FlightComponentDetail> flightComponentDetails = null;

            if (IsFlightBookingOneWay())
            {
                if (this.QuoteData.flightComponents[0].FlightComponentDetails[0].DirectionOptionId == DirectionOptionId)
                    flightComponentDetails = this.QuoteData.flightComponents[0].FlightComponentDetails;
                else if (this.QuoteData.flightComponents[1].FlightComponentDetails[0].DirectionOptionId == DirectionOptionId)
                    flightComponentDetails = this.QuoteData.flightComponents[1].FlightComponentDetails;
            }
            return flightComponentDetails;
        }
        public void PopulateBookingFulfilmentViewModel()
        {
            // Filling Hotel data
            this.Hotel = new BookingFulfillmentHotel();
            this.Hotel.HotelName = this.QuoteData.hotelComponent.HotelName;
            this.Hotel.HotelDescription = this.QuoteData.hotelComponent.hotelComponentsDetails[0].RoomDescription;
            this.Hotel.ResortName = this.QuoteData.hotelComponent.SupplierResortName;
            this.Hotel.StarRating = this.QuoteData.hotelComponent.StarRating;

            //Filling
            // TBD tobe filled by Nagendra

        }
    }
}