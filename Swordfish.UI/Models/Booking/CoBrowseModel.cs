﻿using System;

namespace Swordfish.UI.Models.Booking
{
    public class CoBrowseModel
    {
        public int QuoteHeaderID { get; set; }
        public Guid GuId { get; set; }
        public string Email { get; set; }
        public int ValidityTime { get; set; }
        public string Status { get; set; }
        public int CreatedBy { get; set; }
        public DateTime CreatedDateTime { set; get; } 
    }
}