﻿using Swordfish.Businessobjects.Passengers;
using Swordfish.Businessobjects.Quotes;
using System.Collections.Generic;

namespace Swordfish.UI.Models.Booking
{
    public class CoBrowseResultModel
    {
        public Quote BasketQuoteObject { get; set; }
        public List<Passenger> Passengers { get; set; }
    }
}