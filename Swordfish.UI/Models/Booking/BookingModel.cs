﻿using System.ComponentModel.DataAnnotations;

namespace Swordfish.UI.Models.Booking
{
    public class BookingModel
    {
        [Required]
        public string BasketId { get; set; }

        [Required]
        public string QuoteReference { get; set; }

        public string HotelName { get; set; }

        public string QuoteData { get; set; }

        public string ReturnFlight { get; set; }

        public string OutboundFlightInfo { get; set; }

        public string InboundFlightInfo { get; set; }

        [Required]
        public string TotalPrice { get; set; }
      
        public string ReducedCCPrice { get; set; }
       
        public string DepositAmount { get; set; }

        [Required]
        public int NoOfAdults { get; set; }

        [Required]
        public int NoOfChildren { get; set; }

        public int NoOfInfants { get; set; }
        public string HotelCity { get; set; }
    }
}