﻿using Swordfish.Businessobjects.Sesame;
using System.Collections.Generic;

namespace Swordfish.UI.Models.Flight
{
    public class FlightModel
    {
        public string FlightQuoteId { get; set; }
        public string InboundFlightFrom { get; set; }
        public string InboundFlightFromName { get; set; }
        public string InboundFlightTo { get; set; }
        public string InboundFlightToName { get; set; }
        public string InboundDepartureTime { get; set; }
        public string InboundDepartureTimeV { get; set; }
        public string InboundArrivalTime { get; set; }
        public string InboundArrivalTimeV { get; set; }
        public string OutboundFlightFrom { get; set; }
        public string OutboundFlightFromName { get; set; }
        public string OutboundFlightTo { get; set; }
        public string OutboundFlightToName { get; set; }
        public string OutboundDepartureTime { get; set; }
        public string OutboundDepartureTimeV { get; set; }
        public string OutboundArrivalTime { get; set; }
        public string OutboundArrivalTimeV { get; set; }
        public string AirLaneName { get; set; }
        public decimal FlightFare { get; set; }
        public decimal PricePerPerson { get; set; }
        public int NoOfAdults { get; set; }
        public int NoOfChilds { get; set; }
        public int NoOfInfants { get; set; }
        public int NoOfBags { get; set; }
        public string CurrencyCode { get; set; }
        public string QuoteReference { get; set; }
        public string FlightWay { get; set; }
        public int OutBoundStopsCount { get; set; }
        public int InBoundStopsCount { get; set; }
        public string OutBoundEstimatedHours { get; set; }
        public string InboundEstimatedHours { get; set; }
        public string OutBoundFlightWay { get; set; }
        public string InboundFlightWay { get; set; }
        public string AirlineSrc { get; set; }
        public string OutBoundFlightNumber { get; set; }
        public string InBoundFlightNumber { get; set; }
        public string OutBoundFlightCode { get; set; }
        public string InBoundFlightCode { get; set; }
        public string OutBoundFlightName { get; set; }
        public string InBoundFlightName { get; set; }
        public string OBJourneyIndicator { get; set; }
        public string IBJourneyIndicator { get; set; }
        public int IsIBPriceUpdated { get; set; }
        public int IsOBPriceUpdated { get; set; }
        public int IsRTNPriceUpdated { get; set; }
        public string InBoundJourneyDuration { get; set; }
        public string OutBoundJourneyDuration { get; set; }
        public decimal? BaseFareAmount { get; set; }
        public string BaseFareCurrencyCode { get; set; }
        public double? ExchangeRateUsed { get; set; }
        public string  SourceUrl { get; set; }
        public List<FlightStopPoints> InBoundStopPoints { get; set; }
        public List<FlightStopPoints> OutBoundStopPoints { get; set; }
        public SesameBasket sesameBasketDetails { get; set; }
    }

    public class FlightStopPoints
    {
        public string DepartureDateTime { get; set; }
        public string DepartureDateTimeV { get; set; }
        public string StopAirportCode { get; set; }
        public string StopAirportName { get; set; }
        public string NextStopAirportCode { get; set; }
        public string NextStopAirportName { get; set; }
        public string NextStopArrrivalDateTime { get; set; }
        public string NextStopArrrivalDateTimeV { get; set; }
        public string Duration { get; set; }
        public string FlightNumber { get; set; }
        public string FlightName { get; set; }
        public string FlightCode { get; set; }
        public string EstimatedHours { get; set; }
        public string JourneyIndicator { get; set; }
        public string LongWait { get; set; }
        public string JourneyDuration { get; set; }
        public string CabinClass { get; set; }
    }

    public class FlightDetails
    {
        public List<FlightModel> ReturnFlights { get; set; }
        public List<FlightModel> OutBoundFlights { get; set; }
        public List<FlightModel> InBoundFlights { get; set; }
        public List<SearchAirline> SearchAirlines { get; set; }
        public List<SearchDeptAirline> SearchDeptAirlines { get; set; }
        public List<SearchDayAirline> OutBoundDayAirlines { get; set; }
        public List<SearchDayAirline> InBoundDayAirlines { get; set; }
    }

    public class SearchAirline
    {
        public string value { get; set; }
        public string price { get; set; }
        public bool selected { get; set; }
    }

    public class SearchDeptAirline
    {
        public string value { get; set; }
        public string price { get; set; }
        public bool selected { get; set; }
    }

    public class SearchDayAirline
    {
        public string day { get; set; }
        public string minvalue { get; set; }
        public string maxvalue { get; set; }
        public bool selected { get; set; }
    }
}