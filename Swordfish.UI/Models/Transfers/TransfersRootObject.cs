﻿using System;
using System.Collections.Generic;

namespace Swordfish.UI.Models.Transfers
{
    public class TransferResult
    {
        public List<Rootobject> Rootobject { get; set; }
        public TransferResult()
        {
            this.Rootobject = new List<Rootobject>();
        }
    }
    public class Rootobject
    {
        public List<Search> search { get; set; }
        public Travelling travelling { get; set; }
        public Returning returning { get; set; }
    }
    public class Search
    {
        public string fromtype { get; set; }
        public string fromcode { get; set; }
        public string totype { get; set; }
        public string tocode { get; set; }
        public DateTime travelling { get; set; }
        public string adults { get; set; }
        public string children { get; set; }
        public string infants { get; set; }
        public object fromproperty { get; set; }
        public string fromdetails { get; set; }
        public object toproperty { get; set; }
        public string todetails { get; set; }
        public string fromname { get; set; }
        public string toname { get; set; }
    }
    public class Travelling
    {
        public List<Product> products { get; set; }
    }
    public class Returning
    {
        public List<Product> products { get; set; }
    }
    public class Product
    {
        public List<General> general { get; set; }
        public List<Pricing> pricing { get; set; }
    }

    public class General
    {
        public int productid { get; set; }
        public int bookingtypeid { get; set; }
        public int transfertime { get; set; }
        public int producttypeid { get; set; }
        public int? minpax { get; set; }
        public int? maxpax { get; set; }
        public int? luggage { get; set; }
        public int perperson { get; set; }
        public string producttype { get; set; }
        public string description { get; set; }
        public bool MappingReturnFound { get; set; }

    }

    public class Pricing
    {
        public object oldprice { get; set; }
        public decimal price { get; set; }
        public string currency { get; set; }
        public List<Price> prices { get; set; }
        public decimal totalPrice { get; set; }
    }


    public class Price
    {
        public string type { get; set; }
        public string typecode { get; set; }
        public string description { get; set; }
        public string units { get; set; }
        public string oldprice { get; set; }
        public string price { get; set; }
    }
}