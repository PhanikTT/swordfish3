﻿using Swordfish.Businessobjects.Sesame;
using Swordfish.UI.Models.Hotel;

namespace Swordfish.UI.Models.Transfers
{
    public class TransferSearchRequest : SesameRequestBase
    {
        public string FromType { get; set; }//IATA
        public string FromCode { get; set; }
        public string ToType { get; set; } //GATA
        public string ToCode { get; set; }
        public string CheckIn { get; set; }
        public string CheckOut { get; set; }
        public string AdultCount { get; set; }
        public string ChildrenCount { get; set; }
        public string InfantCount { get; set; }
        public string LatLong { get; set; }
        public string LatLongType { get; set; }
        public HotelInfoRequest HotelInfoRequest { get; set; }
        public TransferSearchRequest()
        {
            HotelInfoRequest = new HotelInfoRequest();
        }
    }
   
}