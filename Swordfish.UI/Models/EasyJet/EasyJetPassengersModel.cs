﻿namespace Swordfish.UI.Models.Easyjet
{
    public class EasyJetPassengersModel
    {
        public string title { get; set; }
        public string firstName { get; set; }
        public string surname { get; set; }
        public string age { get; set; }
        public string personAgeCode { get; set; }
    }
}