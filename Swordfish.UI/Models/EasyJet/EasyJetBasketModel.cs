﻿using Swordfish.Businessobjects.Easyjet;
using System;
using System.Collections.Generic;
using System.Web.Mvc;

namespace Swordfish.UI.Models.Easyjet
{
    public class EasyJetBasketModel
    {
        public EasyJetBasketModel()
        {
            route = new List<EasyJetRouteModel>();
            supplements = new List<EasyJetSupplementsModel>();
            childAges = new List<int>();
            sportsitem = new List<EasyJetSpotrsModel>();
            passengers = new List<EasyJetPassengersModel>();
            accountInfo = new List<EasyJetLoginModel>();
        }
        public string quoteHeaderId { get; set; }
        public decimal adultPrice { get; set; }
        public decimal childPrice { get; set; }
        public decimal infantPrice { get; set; }
        public string priceType { get; set; }
        public int adultsCount { get; set; }
        public int childrenCount { get; set; }
        public int infantsCount { get; set; }
        public List<int> childAges { get; set; }
        public bool demoMode { get; set; }
        public string currency { get; set; }
        public string cardNumber { get; set; }
        public string cardType { get; set; }
        public string cv2 { get; set; }
        public string cardHolderName { get; set; }
        public string expireDate { get; set; }
        public int ComponentTypeOptionId { get; set; }
        public bool priceMatching { get; set; }
        public List<EasyJetRouteModel> route { get; set; }
        public List<EasyJetSupplementsModel> supplements { get; set; }
        public List<EasyJetSpotrsModel> sportsitem { get; set; }
        public List<EasyJetPassengersModel> passengers { get; set; }
        public List<EasyJetLoginModel> accountInfo { get; set; }
    }
}