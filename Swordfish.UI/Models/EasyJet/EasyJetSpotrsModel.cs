﻿namespace Swordfish.UI.Models.Easyjet
{
    public class EasyJetSpotrsModel
    {
        public string name { get; set; }
        public decimal price_per_item { get; set; }
        public int count { get; set; }
    }
}