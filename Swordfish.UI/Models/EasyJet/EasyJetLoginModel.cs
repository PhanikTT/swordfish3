﻿namespace Swordfish.UI.Models.Easyjet
{
    public class EasyJetLoginModel
    {
        public string email { get; set; }
        public string password { get; set; }
    }
}