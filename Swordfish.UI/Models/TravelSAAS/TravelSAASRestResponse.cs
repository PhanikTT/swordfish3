﻿namespace Swordfish.UI.Models.TravelSAAS
{
    public class TravelSAASRestResponse
    {
        public int StatusCode { get; set; }
        public string Response { get; set; }
    }
}