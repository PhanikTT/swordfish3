﻿using Swordfish.Businessobjects.Sesame;
using System.Runtime.Serialization;

namespace Swordfish.UI.Models.Hotel
{
    [DataContract]
    public class AccoAsyncResortResponse : SesameRequestBase
    {
        [DataMember]
        public string ResultSetKey { get; set; }
    }
}