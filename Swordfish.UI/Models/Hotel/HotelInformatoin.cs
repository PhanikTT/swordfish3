﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SwordFish.Models.HotelModels
{
    public class HotelInformatoin
    {
        public string Name { get; set; }
        public string Rating { get; set; }
        public string TTICode { get; set; }
        public string SupplierAccommId { get; set; }
        public string SupplierCode { get; set; }
        public string SupplierResortId { get; set; }
        public string TripAdvisorScore { get; set; }
        public HotelAddress Address { get; set; }
        public List<Description> Descriptions { get; set; }
        public List<Image> Images { get; set; }
    }
    public class HotelAddress
    {
        public string Address1 { get; set; }
        public string Address2 { get; set; }
        public string Address3 { get; set; }
        public string City { get; set; }
        public string County { get; set; }
        public string Postcode { get; set; }
        public string Country { get; set; }
        public string Tel { get; set; }
        public string Fax { get; set; }
        public string Email { get; set; }
        public string Url { get; set; }
        public string LatLong { get; set; }
    }
    public class Image
    {
        public string url { get; set; }
        public string caption { get; set; }
    }
    public class Description
    {
        public string Title { get; set; }
        public string Code { get; set; }
        public string Value { get; set; }
    }
}