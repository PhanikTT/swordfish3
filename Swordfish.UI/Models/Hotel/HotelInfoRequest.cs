﻿using Swordfish.Businessobjects.Sesame;

namespace Swordfish.UI.Models.Hotel
{
    public class HotelInfoRequest : SesameRequestBase
    {
        public string TTICode { get; set; }
        public string SupplierAccommId { get; set; }
    }
}