﻿using System.Collections.Generic;
using System.Runtime.Serialization;

namespace Swordfish.UI.Models.Hotel
{
    [DataContract]
    public class HotelList
    {
        [DataMember]
        public List<HotelDetails> HotelResults { get; set; }
    }
    [DataContract]
    public class HotelDetails
    {
        public HotelDetails()
        {
            RoomRates = new List<RoomRates>();
        }
        [DataMember]
        public List<RoomRates> RoomRates { get; set; }
        [DataMember]
        public string TTIcode { get; set; }
        [DataMember]
        public string HotelCodeContext { get; set; }
        [DataMember]
        public string HotelCode { get; set; }
        [DataMember]
        public string HotelName { get; set; }
        [DataMember]
        public string HotelCity { get; set; }
        [DataMember]
        public string ReferenceId { get; set; }
        [DataMember]
        public string ReferenceType { get; set; }
        [DataMember]
        public string SupplierResortId { get; set; }
        [DataMember]
        public int Rating { get; set; }
        [DataMember]
        public double TripAdvisorScore { get; set; }
        [DataMember]
        public string ResortName { get; set; }
        [DataMember]
        public string RoomDescription { get; set; }
        [DataMember]
        public decimal AmountAfterTax { get; set; }
        [DataMember]
        public decimal PricePerPerson { get; set; }
        [DataMember]
        public string BoardType { get; set; }
        [DataMember]
        public string RateQuoteId { get; set; }
        [DataMember]
        public string RoomTypeCode { get; set; }
        [DataMember]
        public bool PopularHotel { get; set; }
        [DataMember]
        public bool NonRefundable { get; set; }
    }
    public class RoomRates
    {
        public string RatePlanName { get; set; }
        public string RoomTypeCode { get; set; }
        public string RatePlanCode { get; set; }
        public string RoomDescription { get; set; }
        public string QuoteId { get; set; }
        public decimal AmountAfterTax { get; set; }
        public bool? CancelPolicyIndicator { get; set; }
        public bool? NonRefundable { set; get; }
    }
    //public class Resorts
    //{
    //    public string Name { get; set; }
    //    public decimal Price { get; set; }
    //}
}