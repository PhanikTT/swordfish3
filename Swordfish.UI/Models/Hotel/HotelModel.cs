﻿namespace Swordfish.UI.Models.Hotel
{
    public class HotelModel
    {
        public string AccommodationReferenceId { get; set; }
        public string Image { get; set; }
        public string HotelName { get; set; }
        public decimal HotelFare { get; set; }
        public int TotalNights { get; set; }
        public string HotelCode { get; set; }
        public string HotelContext { get; set; }
        public string HotelCity { get; set; }
        public decimal HotelRating { get; set; }
        public decimal TripAdvisorScore { get; set; }
        public string BoardType { get; set; }
        public string RoomDescription { get; set; }
        public int RoomsRequired { get; set; }
        public int NoOfAdults { get; set; }
        public int NoOfChilds { get; set; }
        public int NoOfInfants { get; set; }
        public string CurrencyCode { get; set; }
        public string RoomTypeCode { get; set; }
        public string RatePlanCode { get; set; }
        public string RoomRateQuoteId { get; set; }
        public decimal PricePerPerson { get; set; }
        public string TTICode { get; set; }
        public string QuoteReference { get; set; }
    }
   public class HotelCodes
    {
        public string HotelCode { get; set; }
        public string IFFCode { get; set; }
        public string TTICode { get; set; }
        public string GIATACode { get; set; }
    }
}