﻿using System.Collections.Generic;
using System.Runtime.Serialization;

namespace Swordfish.UI.Models.Hotel
{
    [DataContract]
    public class AccoSearchStatus
    {
        [DataMember]
        public ResultSet Results { get; set; }
    }
    [DataContract]
    public class ResultSet
    {
        [DataMember]
        public string ResultSetKey { get; set; }
        [DataMember]
        public string Status { get; set; }
        [DataMember]
        public string StartDelta { get; set; }
        [DataMember]
        public string MaxDelta { get; set; }
        [DataMember]
        public List<RemoteSupplier> RemoteSuppliers { get; set; }
    }
    [DataContract]
    public class RemoteSupplier
    {
        [DataMember]
        public string Name { get; set; }
        [DataMember]
        public string Results { get; set; }
        [DataMember]
        public string Status { get; set; }
    }
}