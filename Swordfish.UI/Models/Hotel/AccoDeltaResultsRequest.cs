﻿using Swordfish.Businessobjects.Sesame;
using Swordfish.UI.Models.Account;

namespace Swordfish.UI.Models.Hotel
{
    public class AccoDeltaResultsRequest: SesameRequestBase
    {
        public string Key { get; set; }
        public string StartDelta { get; set; }
        public string MaxDelta { get; set; }
        public SearchModel searchModel { get; set; }
    }
}