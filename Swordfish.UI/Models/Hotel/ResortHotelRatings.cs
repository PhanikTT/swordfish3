﻿namespace Swordfish.UI.Models.Hotel
{
    public class ResortHotelRatings
    {
        public int Rating { get; set; }
        public double TripAdvisorScore { get; set; }
        public string TTICode { get; set; }
        public string SupplierResortId { get; set; }
        public string ResortName { get; set; }
    }
}