﻿namespace Swordfish.UI.Models.Hotel
{
    public class HotelMappingCodes
    {
        public string IFFCode { get; set; }
        public string Masterhotelid { get; set; }
        public string GIATACode { get; set; }
        public string TTICode { get; set; }
    }
}
