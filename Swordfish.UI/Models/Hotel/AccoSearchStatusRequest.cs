﻿using Swordfish.Businessobjects.Sesame;
using System.Runtime.Serialization;

namespace Swordfish.UI.Models.Hotel
{
    [DataContract]
    public class AccoSearchStatusRequest : SesameRequestBase
    {
        [DataMember]
        public string Key { get; set; }
        [DataMember]
        public string ProductTypeCode { get; set; }
    }
}