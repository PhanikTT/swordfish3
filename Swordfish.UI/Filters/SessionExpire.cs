﻿using System.Web.Mvc;
using System.Web.Routing;
using Swordfish.UI.Helpers;

namespace SwordFish.Filters
{
    public class SessionExpire : ActionFilterAttribute
    {
        public override void OnActionExecuting(ActionExecutingContext filterContext)
        {


            //if (filterContext.HttpContext.Request.IsAjaxRequest())
            //{
            //    var sessions = filterContext.HttpContext.Session;
            //    if (HttpContext.Current.Session["UserId"] != null)
            //    {
            //        return;
            //    }
            //    else
            //    {
            //        filterContext.Result = new JsonResult
            //        {
            //            Data = new
            //            {
            //                status = "401"
            //            },
            //            JsonRequestBehavior = JsonRequestBehavior.AllowGet
            //        };

            //        //xhr status code 401 to redirect
            //        filterContext.HttpContext.Response.StatusCode = 401;

            //        return;
            //    }
            //}

            var session = filterContext.HttpContext.Session;
            if (SFConfiguration.GetUserID().ToString() != null)
                return;
            
            //Redirect to login page.
            var redirectTarget = new RouteValueDictionary { { "action", "LogOff" }, { "controller", "Account" } };
            filterContext.Result = new RedirectToRouteResult(redirectTarget);
           // throw new Exception("Session Expire");
            //if (HttpContext.Current.Session["UserId"] == null)
            //{
            //    FormsAuthentication.SignOut();
            //    filterContext.Result =
            //   new RedirectToRouteResult(new RouteValueDictionary
            //     { 
            // { "action", "Index" },
            //{ "account", "Login" },
            //{ "returnUrl", filterContext.HttpContext.Request.RawUrl}
            //      });

            //    return;
            //}
        }
    }
}