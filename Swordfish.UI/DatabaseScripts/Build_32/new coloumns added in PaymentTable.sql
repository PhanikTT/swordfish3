
ALTER TABLE payments ADD COLUMN payment_due_date TYPE date;

ALTER TABLE payments ADD COLUMN payment_due_amount  numeric(10,2);
