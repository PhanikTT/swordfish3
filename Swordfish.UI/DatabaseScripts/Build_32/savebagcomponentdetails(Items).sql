﻿alter table bag_component_details add column Items character varying(5);
-- Function: savebagcomponentdetails(text)

-- DROP FUNCTION savebagcomponentdetails(text);

CREATE OR REPLACE FUNCTION savebagcomponentdetails(bagcomponent text)
  RETURNS text AS
$BODY$
	Declare Error_log text;
	Declare bag_component_id int;
	Declare COLUMN_NAME text;
	Declare PG_DATATYPE_NAME text;
	Declare TABLE_NAME text;
	Declare PG_EXCEPTION_CONTEXT text;
	Declare bag_component json;
	DECLARE bag_component_details JSON;
	DECLARE BAG_OPTION_ID INT;
Begin
	select (bagcomponent)::json into bag_component;
	
	IF(select count(*) from json_array_elements(bag_component))>0
	THEN
		INSERT INTO public.bag_component(
		created_by,
		created_on,
		modified_by,
		modified_on		
		)values(
		645,
		now(),
		645,
		now()
			);

		bag_component_id:=(SELECT currval(pg_get_serial_sequence('bag_component','bag_component_id')));
		raise notice 'end of inserting data in bag_component table';
		-- else
-- 		RAISE
-- 		EXCEPTION 'error  storing data in bag_component table';
-- 		GET STACKED DIAGNOSTICS COLUMN_NAME= COLUMN_NAME,
-- 					PG_DATATYPE_NAME=PG_DATATYPE_NAME,
-- 					TABLE_NAME=TABLE_NAME;
-- 					PG_EXCEPTION_CONTEXT=PG_EXCEPTION_CONTEXT;
-- 		Error_log='column name= % data type related to exception =%;name of the table releated to exception =%;name of the function that raised exception =SaveSearchData; line number releated to exception=%;',
-- 		COLUMN_NAME,PG_DATATYPE_NAME,TABLE_NAME,PG_EXCEPTION_CONTEXT;
-- 		ROLLBACK TRANSACTION;

		FOR bag_component_details IN select * from json_array_elements((bagcomponent)::json) 
		LOOP IF (select count(*) from json_each(bag_component_details))>0
		THEN 
			--BAG_OPTION_ID:=(select tsoptionid from tsoption where LOWER(name)=LOWER(bag_component_details->>'Direction'));
			raise notice 'inserting bag component details in bag_component_details table using bag_component_id';
			insert into public.bag_component_details(
			bag_component_id,
			bag_type,
			bag_description,
			bag_code,
			qty_available,
			qty_required,
			buying_price,
			selling_price,
			price,
			direction_option_ID,
			created_by,
			created_on,
			modified_by,
			modified_on,
			passenger_index,
			Items
			) values(
			cast(bag_component_id as int),
			bag_component_details->>'BagsType' ,
			bag_component_details->>'BagsDescription',
			bag_component_details->>'BagsCode',
			Cast(bag_component_details->>'QuantityAvailable' as int),
			Cast(bag_component_details->>'QuantityRequired' as int),
			Cast(bag_component_details->>'BuyingPrice' as money) ,
			Cast(bag_component_details->>'SellingPrice' as money),
			Cast(bag_component_details->>'Price' as money),
			cast(bag_component_details->>'DirectionOptionId' as int),
			Cast(bag_component_details->>'CreatedBy' as int),
			now(),
			Cast(bag_component_details->>'ModifiedBy' as int),
			now(),
			Cast(bag_component_details->>'PassengerIndex' as int),
			bag_component_details->>'Items'
				);
			Raise NOTICE 'End of inserting data in bag_component_details';
			else
			RAISE
			EXCEPTION 'error saving data in bag_component_details';
			GET STACKED DIAGNOSTICS COLUMN_NAME= COLUMN_NAME,
						PG_DATATYPE_NAME=PG_DATATYPE_NAME,
						TABLE_NAME=TABLE_NAME;
						PG_EXCEPTION_CONTEXT=PG_EXCEPTION_CONTEXT;
			Error_log='column name= % data type related to exception =%;name of the table releated to exception =%;name of the function that raised exception =SaveSearchData; line number releated to exception=%;',
			COLUMN_NAME,PG_DATATYPE_NAME,TABLE_NAME,PG_EXCEPTION_CONTEXT;
			ROLLBACK TRANSACTION;
			RAISE NOTICE 'ERROR SAVING DATA IN bag_component_details table';
		END IF;
		END LOOP;
	END IF;

RETURN cast(bag_component_id AS TEXT);
End;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
ALTER FUNCTION savebagcomponentdetails(text)
  OWNER TO chilli;
