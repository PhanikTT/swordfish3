﻿-- Function: savesearchdata(text)

-- DROP FUNCTION savesearchdata(text);

CREATE OR REPLACE FUNCTION savesearchdata(searchdata text)
  RETURNS text AS
$BODY$
	Declare Error_log text;
	Declare Search_data_ID int;
	Declare COLUMN_NAME text;
	Declare PG_DATATYPE_NAME text;
	Declare TABLE_NAME text;
        DECLARE Search_Data JSON;
	Declare PG_EXCEPTION_CONTEXT text;
	Declare OccupancyRooms JSON;
	Declare OccupancyRoom JSON;

Begin
select (SearchData)::json into Search_Data;
	IF(select count(*) from json_each(Search_Data))>0
	THEN
		insert into public.search_model(
		ttss_quote_id,
		customer_reference_code,
		source_name,
		source_code,
		destination_name,
		destination_code,
		start_date,
		end_date,
		hotel_name,		
		star_rating,
		board_type_ai,
		board_type_fb,
		board_type_hb,
		board_type_bb,
		board_type_sc,
		board_type_ro,
		created_by,
		created_on,
		nights
		) values(
		Search_Data->>'TTSSQuoteId',
		Search_Data->>'sfQuoteId',
		Search_Data->>'sourceName',
		Search_Data->>'sourceCode',
		Search_Data->>'destinationName',
		Search_Data->>'destinationCode',
		Cast(Search_Data->>'startDate' as date),
		Cast(Search_Data->>'endDate' as date),
		Search_Data->>'hotelName',		
		Cast(Search_Data->>'starRating' as real),
		Cast(Search_Data->>'BoardTypeAI' as boolean),
		Cast(Search_Data->>'BoardTypeFB' as boolean),
		Cast(Search_Data->>'BoardTypeHB' as boolean),
		Cast(Search_Data->>'BoardTypeBB' as boolean),
		Cast(Search_Data->>'BoardTypeSC' as boolean),
		Cast(Search_Data->>'BoardTypeRO' as boolean),
		Cast(Search_Data->>'CreatedBy' as INT),
		cast(now() as TIMESTAMP WITHOUT TIME ZONE),
		Cast(Search_Data->>'Nights' as INT)
		);		
		Search_data_ID:=(SELECT currval(pg_get_serial_sequence('search_model','search_model_id')));		
		if(Search_data_ID)>0
		then
			SELECT (Search_Data->>'OccupancyRooms')::json INTO OccupancyRooms;
		FOR OccupancyRoom IN select * from json_array_elements((OccupancyRooms)::json) 
		LOOP IF (select count(*) from json_each(OccupancyRoom))>0 Then

			insert into public.occupancy(
			room_no,
			search_model_id,
			adults_count,
			children_count,
			infants_count,
			created_by,
			created_on
			) values(
			cast(OccupancyRoom->>'RoomNo' as INT),
			cast(Search_data_ID as int),
			cast(OccupancyRoom->>'AdultsCount' as INT),
			cast(OccupancyRoom->>'ChildCount' as INT),
			cast(OccupancyRoom->>'InfantCount' as INT),
			cast(Search_Data->>'CreatedBy' as INT),
			now()
			);
		END IF;					
		END LOOP;
			ELSE
			RAISE NOTICE 'ERROR SAVING DATA IN OCCUPANCY TABLE';
		END IF;
		-- RAISE
-- 		EXCEPTION 'error  storing data in OCCUPANCY model';
-- 		GET STACKED DIAGNOSTICS COLUMN_NAME= COLUMN_NAME,
-- 					PG_DATATYPE_NAME=PG_DATATYPE_NAME,
-- 					TABLE_NAME=TABLE_NAME;
-- 					PG_EXCEPTION_CONTEXT=PG_EXCEPTION_CONTEXT;
-- 		Error_log='column name= % data type related to exception =%;name of the table releated to exception =%;name of the function that raised exception =SaveSearchData; line number releated to exception=%;',
-- 		COLUMN_NAME,PG_DATATYPE_NAME,TABLE_NAME,PG_EXCEPTION_CONTEXT;
		
		-- RAISE NOTICE 'ERROR SAVING DATA IN SEARCH_MODEL';
		
	END IF;

RETURN cast(Search_data_ID AS TEXT);
End;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
ALTER FUNCTION savesearchdata(text)
  OWNER TO chilli;
