﻿-- Function: getsearchdetails(text)

-- DROP FUNCTION getsearchdetails(text);

CREATE OR REPLACE FUNCTION getsearchdetails(IN search_modelid text)
  RETURNS TABLE(searchmodelid integer, ttssquoteid character varying, customerreferencecode character varying, sourcename character varying, sourcecode character varying, destinationname character varying, destinationcode character varying, startdate text, enddate text, hotelname character varying, nights integer, starrating real, boardtypeai boolean, boardtypefb boolean, boardtypehb boolean, boardtypebb boolean, boardtypesc boolean, boardtypero boolean, createdby integer, roomno integer, adultscount integer, childrencount integer, infantscount integer, occupancyrooms json) AS
$BODY$
Declare OccupancyRooms JSON;
Begin
				
			if(search_modelid <> '')
			then
					raise notice '%',search_modelid;
						OccupancyRooms:= (SELECT to_json(array_agg(Occupancy_Rooms)) FROM(SELECT oc.room_no RoomNo,oc.adults_count AdultsCount,oc.children_count ChildCount,oc.infants_count InfantCount FROM occupancy oc 
						where cast(search_model_id as text)= cast(search_modelid as text)) Occupancy_Rooms);
					RETURN QUERY
					select 	
					search_model.search_model_id as searchModelId,
					search_model.ttss_quote_id as ttssQuoteId, 
					search_model.customer_reference_code as customerReferenceCode, 
					search_model.source_name as sourceName, 
					search_model.source_code as sourceCode, 
					search_model.destination_name as destinationName, 
					search_model.destination_code as destinationCode, 
					to_char(search_model.start_date, 'MM/DD/YYYY') as startDate, 
					to_char(search_model.end_date, 'MM/DD/YYYY') as endDate,  
					search_model.hotel_name as hotelName, 
					search_model.nights as nights, 
					search_model.star_rating as starRating, 
					search_model.board_type_ai as boardTypeAI, 
					search_model.board_type_fb as boardTypeFB, 
					search_model.board_type_hb as boardTypeHB, 
					search_model.board_type_bb as boardTypeBB, 
					search_model.board_type_sc as boardTypeSC, 
					search_model.board_type_ro as boardTypeRO, 
					search_model.created_by as createdBy,    
					(SELECT cast(max(room_no) as int) FROM occupancy where cast(search_model_id as text)= cast(search_modelid as text)) as roomNo , 
					(select cast(sum(adults_count) as int) FROM occupancy where cast(search_model_id as text) = cast(search_modelid as text)) as adultsCount, 
					(select cast(sum(children_count) as int) FROM occupancy where cast(search_model_id as text) = cast(search_modelid as text))  as childrenCount, 
					(select cast(sum(infants_count) as int) FROM occupancy where cast(search_model_id as text) = cast(search_modelid as text))  as infantsCount,
					OccupancyRooms	
					FROM
					public.search_model 
					where search_model.customer_reference_code=cast(search_modelid as text) or cast(search_model.search_model_id as text)=cast(search_modelid as text);
					--or search_model.search_model_id=cast(search_modelid as int);
				
			end if;
				
end;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100
  ROWS 1000;
ALTER FUNCTION getsearchdetails(text)
  OWNER TO chilli;
