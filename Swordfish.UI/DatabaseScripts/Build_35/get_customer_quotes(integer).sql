﻿-- Function: get_customer_quotes(integer)

-- DROP FUNCTION get_customer_quotes(integer);

CREATE OR REPLACE FUNCTION get_customer_quotes(IN customerid integer)
  RETURNS TABLE(quoteref character varying, totalprice money, priceperperson money, quoteddate timestamp without time zone, source character varying, destination character varying, formdate date, todate date, hotelname character varying,
   numberofadults integer, numberofchilds integer, numberofinfants integer, quote_header_id integer) AS
$BODY$
DECLARE tsoption_id int;
BEGIN
tsoption_id:=(select tsoptionid from tsoption where LOWER(tsoption.name)='accommodation');
raise notice '%',tsoption_id;
RETURN QUERY
select distinct  qh.customer_reference_code as QuoteRef
	,qh.grand_total as TotalPrice
	,qh.price_per_person as PriceperPerson
	,qh.created_on as QuotedDate
	,sm.source_code as Source
 	,sm.destination_code as Destination
 	,sm.start_date as FormDate
 	,sm.end_date as ToDate
        ,hc.hotel_name as HotelName
 	,cast((select sum(oc.adults_count) from occupancy oc where sm.search_model_id =oc.search_model_id) as int)  as NumberOfAdults
 	,cast((select sum(oc.children_count) from occupancy oc where sm.search_model_id =oc.search_model_id) as int) as NumberOfChilds
	,cast((select sum(oc.infants_count) from occupancy oc where sm.search_model_id =oc.search_model_id) as int) as NumberOfInfants
	,qh.quote_header_id
from quote_header qh
 inner join search_model sm on qh.search_model_id = sm.search_model_id
 inner join quote_details qd on qh.quote_header_id = qd.quote_header_id
 inner join hotel_component hc on qd.component_id = hc.hotel_component_id
 inner join occupancy oc on sm.search_model_id =oc.search_model_id  
 inner join tsoption ts on qd.component_type_option_id=ts.tsoptionid
 where qh.customer_id=customerid and  ts.tsoptionid=tsoption_id order by qh.quote_header_id;
-- 5 is "Accommodation"
END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100
  ROWS 1000;
ALTER FUNCTION get_customer_quotes(integer)
  OWNER TO root;
