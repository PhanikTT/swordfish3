-- Function: getquoteheaderdata(text)

-- DROP FUNCTION getquoteheaderdata(text);

CREATE OR REPLACE FUNCTION getquoteheaderdata(IN quoteid text)
  RETURNS TABLE(quoteheaderid integer, searchmodelid integer, customerreferencecode character varying, agentid integer, customerid integer, customerfirstname character varying, customerlastname character varying, customeremailaddress character varying, address character varying, address_line1 character varying, address_line2 character varying, city character varying, country character varying, housenumber character varying, customertitle character varying, optionid integer, postcode character varying, phonenumbertype character varying, phonenumber character varying, telephonenumber character varying, countrycode character varying, quotetype character varying, quotestatusoptionid integer, hotelcost text, baggagecost text, transferscost text, fidelitycost text, totalcost text, marginhotel text, marginflight text, marginextra text, grandtotal text, priceperperson text, internalnotes character varying, senttoemail character varying, senttosms character varying, flightcost text) AS
$BODY$

Begin
		Return query
		select quote_header.quote_header_id as quoteHeaderId,
			quote_header.search_model_id as searchModelId, 
			quote_header.customer_reference_code as customerReferenceCode, 
			quote_header.agent_id as agentId, 
			quote_header.customer_id as customerId, 
			customers.first_name as customerfirstname,
			customers.last_name as customerlastname,
			customers.email_address as customeremailaddress,
			cast(customers_addresses.address as character varying),			
			customers_addresses.address_line1 as address_line1 ,
			customers_addresses.address_line2 as address_line2,
			customers_addresses.city as city,
			customers_addresses.country as country ,
			customers_addresses.house_number as housenumber ,
			(select name from tsoption where tsoptionid= customers.title_type_option_id limit 1) as customertitle,
			 customers.title_type_option_id as optionid,
			customers_addresses.postcode as postcode,
			customers_phone_numbers.phone_number_type as phonenumbertype,
			customers_phone_numbers.phone_number as phonenumber,
			customers_phone_numbers.telephone_number as telephonenumber,
			customers_phone_numbers.country_code as countrycode,
			tsoption.name as quoteType, 
			quote_header.quote_status_option_id as quoteStatusOptionId, 
			(SELECT regexp_replace(cast(quote_header.hotel_cost as text), '[�]', '', 'gi'))as hotelCost,			
			(SELECT regexp_replace(cast(quote_header.baggage_cost as text), '[�]', '', 'gi'))as baggageCost,
                        (SELECT regexp_replace(cast(quote_header.transfers_cost as text), '[�]', '', 'gi'))as transfersCost,
                        (SELECT regexp_replace(cast(quote_header.fidelity_cost as text), '[�]', '', 'gi'))as fidelityCost,
                        (SELECT regexp_replace(cast(quote_header.total_cost as text), '[�]', '', 'gi'))as totalCost,
                        (SELECT regexp_replace(cast(quote_header.margin_hotel as text), '[�]', '', 'gi'))as marginHotel,
                        (SELECT regexp_replace(cast(quote_header.margin_flight as text), '[�]', '', 'gi'))as marginflight,
                        (SELECT regexp_replace(cast(quote_header.margin_extra as text), '[�]', '', 'gi'))as marginExtra,
                        (SELECT regexp_replace(cast(quote_header.grand_total as text), '[�]', '', 'gi'))as grandTotal,
                        (SELECT regexp_replace(cast(quote_header.price_per_person as text), '[�]', '', 'gi'))as priceperPerson,			
			quote_header.internal_notes as internalNotes, 
			quote_header.sent_to_email as senttoEmail, 
			quote_header.sent_to_sms as senttoSms, 
			(SELECT regexp_replace(cast(quote_header.flight_cost as text), '[�]', '', 'gi'))as flightCost
			
		FROM public.tsoption join 
		public.quote_header on(tsoption.tsoptionid=quote_header.quote_type_option_id) join
		customers on (quote_header.customer_id=customers.customer_id) join
		customers_addresses on(customers.customer_id=customers_addresses.customer_id) join 
		customers_phone_numbers on(customers.customer_id=customers_phone_numbers.customer_id)
		where quote_header.customer_reference_code=quoteid or cast(quote_header.quote_header_id as text)=quoteid;
End
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100
  ROWS 1000;
ALTER FUNCTION getquoteheaderdata(text)
  OWNER TO chilli;

