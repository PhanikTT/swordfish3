﻿CREATE TABLE component_message
(
  component_message_id serial NOT NULL,
  component_id integer NOT NULL,
  message character varying,
  created_by integer,
  created_date timestamp without time zone,
  modify_by integer,
  modify_date timestamp without time zone,
  CONSTRAINT component_message_pkey PRIMARY KEY (component_message_id),
  CONSTRAINT component_message_id_fkey FOREIGN KEY (component_id)
      REFERENCES components (component_id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION
)
WITH (
  OIDS=FALSE
);
ALTER TABLE component_errors
  OWNER TO chilli;
