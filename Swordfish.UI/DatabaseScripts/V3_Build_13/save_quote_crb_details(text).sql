﻿-- Function: save_quote_crb_details_mes(text)

-- DROP FUNCTION save_quote_crb_details_mes(text);

CREATE OR REPLACE FUNCTION save_quote_crb_details(jsoninput text)
  RETURNS boolean AS
$BODY$
-- Created/Modified By		||Created/Modified Date 		||Comments
--========================================================================================================
-- Thamma Reddy		||21/06/2016				||Restrucuring DB Object
-- 
  DECLARE basket_object  JSON;  
  DECLARE component_object JSON;
  DECLARE component JSON;
  DECLARE component_id INT; 
  DECLARE component_details_object JSON;
  DECLARE component_dtl JSON;
  DECLARE component_option_object JSON;
  DECLARE component_opt JSON;
  DECLARE accommodation_object JSON;
  DECLARE accomm JSON;
  DECLARE flight_object JSON;
  DECLARE flight JSON;
  DECLARE flightId INT;
  DECLARE flight_segment_object JSON;
  DECLARE flight_segment JSON;
  DECLARE componenterrors JSON;
  DECLARE componenterror JSON;
  DECLARE component_vcn_Details JSON;
  DECLARE component_vcn_Detail JSON;
  DECLARE errormessages text[];
  DECLARE errormessage text;
  DECLARE componentmessages JSON;
  DECLARE messages text[];
  DECLARE componentmessage text;
  
  BEGIN
	IF length(jsoninput) <> 0 THEN
		RAISE notice 'Converting input param as JSON object';
		SELECT (jsoninput)::json  INTO   basket_object;
	    IF(SELECT count(*) FROM   json_each(basket_object)) > 0 THEN
		RAISE notice 'Collecting basket details from basket object';
		SELECT (basket_object->>'BasketComponents')::json INTO  Component_object;
			IF ( SELECT count(*) FROM   json_array_elements(Component_object)) > 0  THEN
			RAISE notice 'Inserting components';
			     FOR component IN (SELECT * FROM   json_array_elements((Component_object)::json))   LOOP			     
				    IF(SELECT count(*) FROM   json_each(component)) > 0    THEN	

				     	RAISE notice 'Inserting components';		 
					INSERT INTO components
					(  
					        quote_header_id,
						supplier_basket_id,
						custom_field_1,
						supplier_component_id,
						product_type_code,
						supplier_code,    
						custom_field_2,
						custom_field_3,
						custom_field_4,
						status_code,
						supplier_booking_id,
						sesame_basket_type_option_id,
						messages,
						direction_option_id,
						created_by,
						created_date,
						source_url						                       
					)
					VALUES
					(
						cast(basket_object->>'QuoteHeaderId' as int),
						basket_object->>'BasketId',
						basket_object->>'CurrencyCode',
						component->>'ComponentID',
						component->>'ProductTypeCode',
						component->>'SupplierCode',
						component->>'BookingAttempted',
						component->>'BookingSucceeded',
						component->>'BookingCancelled',
						component->>'StatusCode',
						component->>'SupplierBookingId',
						cast(component->>'Type' as int),
						basket_object->>'ResponseXml',
						cast(component->>'DirectionOptionId' as int),					                                
						cast(basket_object->>'AgentId' AS INT),
						now(),
						basket_object->>'SourceUrl'
					);
					--component_id:=(SELECT currval(pg_get_serial_sequence('components','component_id')));    
					component_id:=(SELECT lastval());                
					raise notice ' id %',component_id;
			    
						IF(component_id)>0 THEN	
						raise notice 'component_id %',component_id;			
						    --save Component Details
						     SELECT (component->>'ComponentDetails')::json INTO  component_details_object;
						
							IF ( SELECT count(*) FROM   json_array_elements(component_details_object)) > 0  THEN
							 RAISE notice 'Inserting component_details';
							     FOR component_dtl IN (SELECT * FROM   json_array_elements((component_details_object)::json)) LOOP
								    IF(SELECT count(*) FROM   json_each(component_dtl)) > 0 THEN

									 RAISE notice 'Inserting component_details';	
									INSERT INTO component_details
									(  
										component_id,
										component_detail_type,
										supplier_description,
										supplier_currency,
										cost_to_supplier_pence,
										current_cost_to_customer_pence,    
										quantity_covered,
										created_by,										
										created_date,
										exchange_rate_used						                       
									)
									VALUES
									(
										component_id,
										component_dtl->>'Type',
										component_dtl->>'Description',
										component_dtl->>'SourceCurrency',
										cast(component_dtl->>'SourcePricePence' as decimal),
										cast(component_dtl->>'SellingPricePence' as decimal),
										cast(component_dtl->>'Quantity' as int),															                                
										cast(basket_object->>'AgentId' AS INT),
										now(),
										cast(component_dtl->>'ExchangeRateUsed' as numeric)
									);									

									END IF;
							     END LOOP; 
							 END IF;	

							 --Save Component options
						        SELECT (component->>'ComponentOptions')::json INTO  component_option_object;
						
							IF ( SELECT count(*) FROM   json_array_elements(component_option_object)) > 0  THEN
							 RAISE notice 'Inserting component_options';
							     FOR component_opt IN (SELECT * FROM   json_array_elements((component_option_object)::json)) LOOP
								    IF(SELECT count(*) FROM   json_each(component_opt)) > 0 THEN

							             RAISE notice 'Inserting component_options';
									INSERT INTO component_options
									(  
										component_id,
										option_description,
										supplier_option_code,
										option_type,
										quantity_available,
										quantity_required,    
										buying_price_pence,
										selling_price_pence,
										created_by,										
										created_date						                       
									)
									VALUES
									(
										component_id,
										component_opt->>'Description',
										component_opt->>'Code',
										component_opt->>'Type',
										cast(component_opt->>'QuantityAvailable' as int),
										cast(component_opt->>'QuantityRequired' as int),
										cast(component_opt->>'BuyingPricePence' as decimal),
										cast(component_opt->>'SellingPricePence' as decimal),																									                                
										cast(basket_object->>'AgentId' AS INT),
										now()
									);									

									END IF;
							     END LOOP; 
							 END IF;	

							 --Save Accommodations
							    SELECT (component->>'ComponentHotelInformation')::json INTO  accommodation_object;
						
								IF ( SELECT count(*) FROM   json_array_elements(accommodation_object)) > 0  THEN
								 RAISE notice 'Inserting Accommodations';
								     FOR accomm IN (SELECT * FROM   json_array_elements((accommodation_object)::json)) LOOP
									    IF(SELECT count(*) FROM   json_each(accomm)) > 0 THEN

								              RAISE notice 'Inserting accommodations';
										INSERT INTO accommodations
										(  
											component_id,
											board_description,
											board_code,
											special_information,										
											created_by,										
											created_date						                       
										)
										VALUES
										(
											component_id,
											accomm->>'Description',
											accomm->>'Code',
											accomm->>'BoardCode',																																	                                
											cast(basket_object->>'AgentId' AS INT),
											now()
										);									

										END IF;
								      END LOOP; 
								  END IF;

								--Save Flights
								SELECT (component->>'ComponentFlightInformation')::json INTO  flight_object;
						
								IF ( SELECT count(*) FROM   json_array_elements(flight_object)) > 0  THEN
								 RAISE notice 'Inserting Accommodations';
								     FOR flight IN (SELECT * FROM   json_array_elements((flight_object)::json)) LOOP
									    IF(SELECT count(*) FROM   json_each(flight)) > 0 THEN

										RAISE notice 'Inserting flights';	
										INSERT INTO flights
										(  
											component_id,
											from_airport_code,
											to_airport_code,
											departure_ts,
											arrival_ts,										
											created_by,										
											created_date						                       
										)
										VALUES
										(
											component_id,
											flight->>'FromAirportCode',
											flight->>'ToAirportCode',
											cast(flight->>'DepartureDateTime' as timestamp without time zone),
											cast(flight->>'ArrivalDateTime' as timestamp without time zone),																																	                                
											cast(basket_object->>'AgentId' AS INT),
											now()
										);

												flightID:=(SELECT lastval());                
												raise notice ' flightID %',flightID;
										    
													IF(flightID)>0 THEN	
													raise notice 'flightID %',flightID;			
													--save Component Details
													  SELECT (flight->>'Legs')::json INTO  flight_segment_object;
													
														IF ( SELECT count(*) FROM   json_array_elements(flight_segment_object)) > 0  THEN
														 RAISE notice 'Inserting component_details';
														     FOR flight_segment IN (SELECT * FROM   json_array_elements((flight_segment_object)::json)) LOOP
															    IF(SELECT count(*) FROM   json_each(flight_segment)) > 0 THEN

														               RAISE notice ' inserting flight_legs';	
																INSERT INTO flight_legs
																(  
																	flight_id,
																	from_airport_code,
																	to_airport_code,
																	departure_ts,
																	arrival_ts,
																	flight_number,    
																	flight_operator,
																	created_by,										
																	created_date						                       
																)
																VALUES
																(
																	flightID,
																	flight_segment->>'FromAirportCode',
																	flight_segment->>'ToAirportCode',																
																	cast(flight_segment->>'DepartureDateTime' as timestamp without time zone),
																	cast(flight_segment->>'ArrivalDateTime' as timestamp without time zone),
																	flight_segment->>'FlightNumber',	
																	flight_segment->>'MarketingAirline',															                                
																	cast(basket_object->>'AgentId' AS INT),
																	now()
																);									

																END IF;
														     END LOOP; 
														 END IF;																				
													
													 END IF;																		

										END IF;
								     END LOOP; 
								 END IF; 

								--Save component Errors	
								SELECT (component->>'ComponentErrors')::json INTO  componenterrors;	
								raise notice '% componenterrors', componenterrors;
								
				
								IF ( SELECT count(*) FROM   json_array_elements(componenterrors)) > 0  THEN
								RAISE notice 'Inserting ComponentErrors';
								errormessage := cast(componenterrors as text);
								errormessage := regexp_replace(errormessage, '"', '','g');
								errormessage := regexp_replace(errormessage, '\[', '','g');
								errormessage := regexp_replace(errormessage, '\]', '','g');
								errormessages := string_to_array(errormessage,',' );
									raise notice '%Error messages :', errormessages;
									FOREACH errormessage IN ARRAY errormessages LOOP
										--raise notice 'Entered into component errors loop with error message :',errormessage;

										 --IF(SELECT count(*) FROM   json_each(componenterror)) > 0 THEN
											INSERT INTO component_errors(
												component_id,
												error,
												created_by,
												created_date
											)
											Values(
												component_id,												
												errormessage,
												cast(basket_object->>'AgentId' AS INT),
												now()
											);
										 --END IF;
									END LOOP;

								END IF;
								
								-- Save Component messages
								SELECT (component->>'ComponentMessages')::json INTO  componentmessages;
								raise notice '% ComponentMessages', componentmessages;
									IF ( SELECT count(*) FROM   json_array_elements(componentmessages)) > 0  THEN
									RAISE notice 'Inserting ComponentMessages';
									componentmessage := cast(componentmessages as text);
									componentmessage := regexp_replace(componentmessage, '"', '','g');
									componentmessage := regexp_replace(componentmessage, '\[', '','g');
									componentmessage := regexp_replace(componentmessage, '\]', '','g');
									messages := string_to_array(componentmessage,',' );
									raise notice '%Component messages :', messages;

									FOREACH componentmessage IN ARRAY messages LOOP
									componentmessage := replace(componentmessage, '~', ',');
									raise notice '%Component message :', componentmessage;

										INSERT INTO component_message(
												component_id,
												message,
												created_by,
												created_date
										)
										Values(
												component_id,												
												componentmessage,
												cast(basket_object->>'AgentId' AS INT),
												now()
											);
									
									END LOOP;
									END IF;




								--Save component VCN Details
								raise notice 'Inserting VCNDetails';
								SELECT (component->>'ComponentVCNDetials')::json INTO  component_vcn_Details;					
								IF ( SELECT count(*) FROM   json_array_elements(component_vcn_Details)) > 0  THEN
								For component_vcn_Detail IN (SELECT * FROM   json_array_elements((component_vcn_Details)::json)) LOOP
										 IF(SELECT count(*) FROM   json_each(component_vcn_Detail)) > 0 THEN
											INSERT INTO component_vcn(
												component_id,
												pan,
												card_type,
												expiry_date,
												card_holder_name,
												reference,
												created_by,
												created_date
											)
											Values(
												component_id,												
												component_vcn_Detail->>'PAN',
												component_vcn_Detail->>'CardType',
												component_vcn_Detail->>'ExpiryDate',
												component_vcn_Detail->>'CardHolderName',
												component_vcn_Detail->>'Reference',
												cast(basket_object->>'AgentId' AS INT),
												now()
											);
										 END IF;
									END LOOP;

								END IF;
						ELSE
						RAISE EXCEPTION 'Error saving  component details ';
						END IF;

					END IF;
			     END LOOP; 
	                 END IF;		
    
		
	    END IF;	
	END IF;	
RETURN TRUE;
END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
ALTER FUNCTION save_quote_crb_details_mes(text)
  OWNER TO chilli;
