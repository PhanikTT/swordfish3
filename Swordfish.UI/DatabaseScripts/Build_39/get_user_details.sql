﻿CREATE OR REPLACE FUNCTION get_user_details(IN user_name character varying)
  RETURNS TABLE(userid integer, username character varying, firstname character varying, lastname character varying, email character varying, teamid integer, validfrom timestamp without time zone, createdby integer, createdon timestamp without time zone, updatedby integer, updatedon timestamp without time zone, last_login_date timestamp without time zone, teamname character varying, tiercode character varying, tierdescription character varying, imagepath character varying, mobile_no character varying, special_instructions character varying, margin_flight_min integer, margin_flight_max integer, margin_hotel_min integer, margin_hotel_max integer, margin_extra_min integer, margin_extra_max integer, initial character varying, title character varying, address_type character varying, address1 character varying, address2 character varying, address3 character varying, state character varying, country character varying, postcode character varying, email_type character varying, phone_type character varying, phone character varying, syntec_account_id character varying,topdog_id integer) AS
$BODY$
BEGIN   
      RETURN QUERY select distinct us.userid,us.username,mem.firstname,mem.lastname,mem.email,mem.teamid,mem.validfrom,mem.createdby,mem.createdon,mem.updatedby,mem.updatedon,mem.last_login_date,t.teamname,ti.code as tiercode,ti.description as tierdescription
,us.imagepath,us.mobile_no,mem.Special_instructions,r.margin_flight_min,r.margin_flight_max,r.margin_hotel_min,r.margin_hotel_max,r.margin_extra_min,r.margin_extra_max , us.initial,us.title, us.address_type,us.address1, us.address2,us.address3, us.state,
us.country, us.postcode,us.email_type,us.phone_type,us.phone,us.syntec_account_id,us.topdog_id
from membership mem 
join users us on us.userid=mem.userid 
join team t on mem.teamid=mem.teamid
join userroles ur on ur.userid=us.userid
join roles r on r.roleid=r.roleid
join tier ti on mem.tierid=ti.tierid where us.username=user_name;
END
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100
  ROWS 1000;
ALTER FUNCTION get_user_details(character varying)
  OWNER TO chilli;