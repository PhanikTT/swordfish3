﻿-- Function: update_agentnotes(text, text)

-- DROP FUNCTION update_agentnotes(text, text);

CREATE OR REPLACE FUNCTION update_agentnotes(
    quote_id text,
    agent_notes text)
  RETURNS boolean AS
$BODY$

-- Created/Modified By		||Created/Modified Date 		||Comments
--========================================================================================================
-- Nagendra Babu		||22/11/2016				||Updating agent notes to Basket
-- 


BEGIN
	update quote_header set internal_notes = internal_notes ||' '||agent_notes where cast(quote_header_id  as varchar) = quote_id;
 RETURN true;
END;

$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
ALTER FUNCTION update_agentnotes(text, text)
  OWNER TO chilli;




