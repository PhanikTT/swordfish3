﻿-- Function: insert_customer_details(character varying, character varying, character varying, character varying, character varying, character varying, character varying, character varying, character varying, character varying, character varying, boolean, character varying, text, character varying, character varying, character varying, character varying, character varying, boolean, character varying, boolean, boolean, character varying)

-- DROP FUNCTION insert_customer_details(character varying, character varying, character varying, character varying, character varying, character varying, character varying, character varying, character varying, character varying, character varying, boolean, character varying, text, character varying, character varying, character varying, character varying, character varying, boolean, character varying, boolean, boolean, character varying);

CREATE OR REPLACE FUNCTION insert_customer_details(
    email_address character varying,
    first_name character varying,
    last_name character varying,
    pass_word character varying,
    dob character varying,
    emailtype character varying,
    city character varying,
    country character varying,
    postcode character varying,
    membership_no character varying,
    created_by character varying,
    status boolean,
    address_type character varying,
    address text,
    address_line1 character varying,
    address_line2 character varying,
    phone_number_type character varying,
    phone_number character varying,
    telephone_number character varying,
    email_consent boolean,
    title_type character varying,
    optedfor_email boolean,
    optedfor_sms boolean,
    datatype character varying)
  RETURNS text AS
$BODY$
-- Created/Modified By		||Created/Modified Date 		||Comments
--========================================================================================================
-- Nagendra Babu		||02/29/2016				||Restrucuring DB Object
--
DECLARE myoutput boolean := FALSE;
DECLARE last_customer_id INT;
DECLARE title_type_optionid int; 

BEGIN
title_type_optionid:=(select tsoptionid from tsoption where lower(tsoption.name)=lower(title_type));
IF (datatype = 'Insert') THEN
       -- title_type_option_id:=(select tsoptionid from tsoption where lower(tsoption.name)=lower(title_type));
	INSERT INTO PUBLIC.customers (
		email_address
		,first_name
		,last_name
		,pass_word		
                ,emailtype		
		,membership_no
		,created_by
		,STATUS
		,created_on
                ,dob
                ,email_consent
                ,title_type_option_id
                ,opted_for_sms
                ,opted_for_email
		)
	VALUES (
		email_address
		,first_name
		,last_name
		,pass_word		
                ,emailtype		
		,membership_no
		,created_by
		,STATUS
		,now()
                ,dob
                ,email_consent
                ,title_type_optionid
                ,optedfor_sms
                ,optedfor_email
		);

	last_customer_id:= (
			SELECT currval('customers_customer_id_seq')
			);

	INSERT INTO PUBLIC.customers_addresses (
		customer_id
		,address_type
		,address
		,address_line1
		,address_line2
		,created_on
		,created_by
		,city
		,country
		,postcode
		)
	VALUES (
		last_customer_id
		,address_type
		,address
		,address_line1
		,address_line2
		,now()
		,created_by
		,city
		,country
		,postcode
		);

	INSERT INTO PUBLIC.customers_phone_numbers (
		customer_id
		,phone_number_type
		,phone_number
		,telephone_number
		,created_by
		,created_on		
		)
	VALUES (
		last_customer_id
		,phone_number_type
		,phone_number
		,telephone_number
		,created_by
		,now()
		);

	myoutput:= TRUE;     
	RETURN myoutput;
ELSE

last_customer_id :=(select customers.customer_id from customers where customers.membership_no=insert_customer_details.membership_no);
raise notice '%', title_type_optionid;
UPDATE customers
SET email_address = insert_customer_details.email_address,first_name = insert_customer_details.first_name,last_name = insert_customer_details.last_name,pass_word = insert_customer_details.pass_word,
emailtype = insert_customer_details.emailtype,created_by = insert_customer_details.created_by,STATUS = insert_customer_details.STATUS,created_on=now(),dob = insert_customer_details.dob,
email_consent = insert_customer_details.email_consent,title_type_option_id=title_type_optionid, opted_for_sms = optedfor_SMS,opted_for_email = optedfor_email
WHERE customers.membership_no = insert_customer_details.membership_no;


UPDATE customers_addresses
SET address_type = insert_customer_details.address_type,address = insert_customer_details.address,address_line1 = insert_customer_details.address_line1,address_line2 = insert_customer_details.address_line2,created_on = now(),created_by = insert_customer_details.created_by,
city = insert_customer_details.city,
country = insert_customer_details.country,postcode = insert_customer_details.postcode
WHERE customers_addresses.customer_id = last_customer_id;

UPDATE customers_phone_numbers
SET phone_number_type = insert_customer_details.phone_number_type,phone_number = insert_customer_details.phone_number,telephone_number = insert_customer_details.telephone_number,created_by = insert_customer_details.created_by,created_on = now()
WHERE customers_phone_numbers.customer_id = last_customer_id;


myoutput:= TRUE;     
RETURN myoutput;

END IF;
END;$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
ALTER FUNCTION insert_customer_details(character varying, character varying, character varying, character varying, character varying, character varying, character varying, character varying, character varying, character varying, character varying, boolean, character varying, text, character varying, character varying, character varying, character varying, character varying, boolean, character varying, boolean, boolean, character varying)
  OWNER TO chilli;
