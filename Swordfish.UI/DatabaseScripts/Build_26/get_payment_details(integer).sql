-- Function: get_payment_details(integer)

-- DROP FUNCTION get_payment_details(integer);

CREATE OR REPLACE FUNCTION get_payment_details(quoteid integer)
  RETURNS SETOF json AS
$BODY$
Declare payment_object json;
BEGIN
-- Created/Modified By		||Created/Modified Date 		||Comments
--========================================================================================================
-- Nagendra BAbu		||10/06/2016				||Restrucuring DB Object
-- Nagendra BAbu		||10/06/2016				||lapday, statuoptionid and status description added
-- 
if(quoteid <> 0 or quoteid is not null )
THEN
 
payment_object :=(
select array_to_json(array_agg(row_to_json(a))) as resutlt from(
select 
	p.payment_id as id,
	p.quote_header_id as quoteHeaderId,
	p.card_type as cardType,
	p.card_number as cardNumber,
	p.name_on_card as name,
	p.payment_address1 as address,
	p.payment_address2 as city,
	p.payment_country as country,
	p.card_type_code as card_type_code,
	p.payment_postcode as postCode,
	p.amount as amount,
	p.cost as cost,
	p.card_charges as cardCharges,
	p.total_cost as totalCost,
	pe.merchant_refrence as merchantRefrence,
	pe.transaction_id as transactionId,
	pe.result_datetime as resultDatetime,
	pe.processing_db as processingdb,
	pe.error_msg as errormsg,
	pe.tid as tid,
	pe.scheme_name as schemeName,
	pe.message_number as messageNumber,
	pe.auth_code as authCode,
	pe.auth_message as authMessage,
	pe.vertel as vertel,
	pe.txn_result as txnResult,
	pe.pcavs_result as pcavsResult,
	pe.cvc_result as cvcResult,
	pe.arc as arc,
	pe.iadarc as iadarc,
	pe.iadoad as iadoad,
	p.card_valid_from as cardValidFrom,
	p.card_expiry as cardExpiry,
	pe.isd as isd,
	pe.authorisingentity as authorisingentity,
	p.house_number as houseNumber,
	pe.card_easy_response as cardEasyResponse,
	p.deposit_type as depositType,
	p.card_type_code as cardtypecode,
	p.first_name as firstName,
	p.last_name as lastName,
	pe.recur as recur,
	pe.card_type as cardType,
	pe.surcharge as surcharge,
	pe.pan as pan,
	pe.manual as manual
	from payments as p 
	join payment_extension as pe on p.payment_id = pe.payment_id where p.quote_header_id=quoteid)a 
	) ;

End If;
raise notice '%',payment_object;
return query 
select payment_object as result;
END
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100
  ROWS 1000;
ALTER FUNCTION get_payment_details(integer)
  OWNER TO chilli;
