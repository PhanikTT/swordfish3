﻿-- Function: getallquoteinfo(text)

-- DROP FUNCTION getallquoteinfo(text);

CREATE OR REPLACE FUNCTION getallquoteinfo(basketid text)
  RETURNS SETOF json AS
$BODY$
DECLARE basket_header json;
DECLARE basket_header_id INT;
DECLARE Search_model_id INT;
DECLARE search_model json;
DECLARE quotedetails json;
DECLARE flightcomponentdetails json;
DECLARE hotelcomponentdetails json;
DECLARE transfercomponentdetails json;
DECLARE bagcomponentdetails json;
DECLARE component json;
DECLARE flightcomponent json;
DECLARE FlightsData Json;
DECLARE hotelcomponent json;
DECLARE QUOTEALLINFO JSON;
DECLARE flightcomponentdata json;
DECLARE flightcomponentdata1 json;
DECLARE flightcomponentdata2 json;
DECLARE hotelcomponentdata json;

BEGIN
	basket_header:=(SELECT to_json(quote_header) FROM(SELECT * FROM getquoteheaderdata(CAST(basketid AS TEXT))) quote_header);
	IF (SELECT count(*) FROM json_each(basket_header)) > 0
	THEN
		raise notice '%',basket_header->>'searchmodelid';	
		Search_model_id:=(CAST(basket_header->>'searchmodelid' AS INT));
		raise notice '%',Search_model_id;	
		search_model:=(SELECT to_json(Quote_search_model) FROM(SELECT * FROM getsearchdetails(cast(Search_model_id as text))) Quote_search_model);
		raise notice '%',search_model;
		basket_header_id:=(CAST(basket_header->>'quoteheaderid' as INT));
		raise notice '%',basket_header_id;
		quotedetails:=(SELECT to_json(array_agg(Quote_detail_model)) FROM(SELECT * FROM getquotedetailsdata(CAST(basket_header->>'quoteheaderid'as INT))) Quote_detail_model);
		raise notice '%',quotedetails;
		FOR component IN SELECT * FROM json_array_elements(quotedetails)
   	        LOOP
			IF(cast(component->>'tsoptionid' as int)=3 OR cast(component->>'tsoptionid' as int)=4)
			THEN
				flightcomponent:=(SELECT to_json(flight_component) FROM(
				SELECT flight_component.amount as amount, 
				flight_component.currency_code as currencyCode, 
				flight_component.source, 
				flight_component.arrival, 
				flight_component.flight_quote_id as flightQuoteId,
				tsoption.tsoptionid as flighttypeoptionid,
				tsoption.name as flighttype,
				flight_component.source_url,
				flight_component.base_fare_amount,
				flight_component.base_fare_currency_code
				FROM public.flight_component join tsoption on(flight_component.component_type_option_id=tsoption.tsoptionid)
				WHERE flight_component.flight_component_id=CAST(component->>'component_id' AS INT)) AS flight_component);
				RAISE NOTICE 'flight component name % %',(LOWER(CAST(component->>'name' AS TEXT))),component->>'tsoptionid';
				flightcomponentdetails:=(SELECT to_json(array_agg(flight_component_details)) FROM(SELECT * FROM getflightcomponentdetails(CAST(component->>'component_id' AS INT))) flight_component_details);				
				IF (SELECT count(*) FROM json_each(flightcomponentdata2)) > 0
				THEN
					flightcomponentdata1:=(SELECT row_to_json(flight_data) AS flight_data FROM (SELECT flightcomponent,flightcomponentdetails) flight_data);
					
								--  CAST(flightcomponentdata->>'flightcomponent' AS json) AS flightcomponent,
--   								CAST(flightcomponentdata->>'flightcomponentdetails' AS json) AS flightcomponentdetails) flight_data);

					RAISE NOTICE 'flight component data %', flightcomponentdata1;
				ELSE
					flightcomponentdata2:=(SELECT row_to_json(flight_data) AS flight_data FROM (SELECT flightcomponent,flightcomponentdetails) flight_data);
					RAISE NOTICE 'flight component data %', flightcomponentdata2;
				END IF;				
			END IF;
			IF(cast(component->>'tsoptionid' as int))=5
			THEN
				RAISE NOTICE 'after IF % %',component->>'name',component->>'component_id';
				hotelcomponent:=(SELECT to_json(hotels_component) FROM(
				SELECT 
				hotel_component.hotel_name as hotelName, 
				hotel_component.hotel_code as hotelCode, 
				hotel_component.hotel_code_conTEXT as hotelCodeContext, 
				hotel_component.supplier_resort_id as supplierResortId, 
				to_char(hotel_component.checkin_date, 'MM/DD/YYYY') as checkindate, 
				hotel_component.star_rating as starrating, 
				hotel_component.trip_advisor_rating as tripadvisorrating,
				to_char(hotel_component.checkout_date, 'MM/DD/YYYY') as checkoutdate,
				hotel_component.tti_code as ttiCode, 
				hotel_component.hotel_type_id as hotelTypeId, 
				hotel_component.hotel_quote_id as hotelQuoteId
				FROM  public.hotel_component
				WHERE hotel_component.hotel_component_id=CAST(component->>'component_id' AS INT)) AS hotels_component);					
				RAISE NOTICE '%',hotelcomponent;
				hotelcomponentdetails:=(SELECT to_json(array_agg(hotel_component_details)) FROM(SELECT * FROM gethotelcomponentdetails(CAST(component->>'component_id' AS INT))) hotel_component_details);
			END IF;
			IF(cast(component->>'tsoptionid' as int))=6
			THEN
				RAISE NOTICE 'after IF % %',component->>'name',component->>'component_id';
				transfercomponentdetails:=(SELECT to_json(array_agg(transfer_component_details)) FROM(SELECT * FROM gettransfercomponentdetails(CAST(component->>'component_id' AS INT))) transfer_component_details);
			END IF;
			IF(cast(component->>'tsoptionid' as int))=7
			THEN
				RAISE NOTICE 'after IF % %',component->>'name',component->>'component_id';
				bagcomponentdetails:=(SELECT to_json(array_agg(bag_component_details)) FROM(SELECT * FROM getbagscomponentdetails(CAST(component->>'component_id' AS INT))) bag_component_details);
				RAISE NOTICE '%',bagcomponentdetails;
			END IF;

			
		END LOOP;
	END IF;
		-- FlightComponentData:=(SELECT array_to_json(array_agg(row_to_json(FlightData))) AS flight_data FROM (SELECT flightcomponentdata as flightcomponentdata,  flightcomponentdata as flightcomponentdata) FlightData);
-- 		FlightComponentData1:=(SELECT array_to_json(array_agg(row_to_json(FlightData))) FROM (SELECT flightcomponentdata1 as flightcomponentdata1 ) FlightData);
-- raise notice 'FlightComponentData1 %',FlightComponentData;
		--FlightComponentData.push(FlightComponentData1);
		flightcomponentdata:=(SELECT to_json(array_agg(flight_data)) AS flight_data FROM (SELECT (flightcomponentdata2->>'flightcomponent')::json as flightcomponent1,(flightcomponentdata2->>'flightcomponentdetails')::json as 	
						flightcomponentdetails1,(flightcomponentdata1->>'flightcomponent')::json as flightcomponent2,(flightcomponentdata1->>'flightcomponentdetails')::json as flightcomponentdetails2) as flight_data);
		hotelcomponentdata:=(SELECT row_to_json(hoteldata) AS flight_data FROM (SELECT hotelcomponent,hotelcomponentdetails) hoteldata);
		RAISE NOTICE '%',hotelcomponentdata;
		QUOTEALLINFO:=(SELECT row_to_json(basket) AS basketdetails
		FROM(
		  SELECT
		basket_header AS BASKETHEADER,
		search_model AS SEARCHMODEL,
		flightcomponentdata AS FLIGHTCOMPONENTDATA,
		hotelcomponentdata AS hotelcomponentdata,
		transfercomponentdetails AS transfercomponentdetails,
		bagcomponentdetails AS bagcomponentdetails
		 ) basket);

		RAISE NOTICE '%',QUOTEALLINFO;
		RETURN query  
		SELECT QUOTEALLINFO;
END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100
  ROWS 1000;
ALTER FUNCTION getallquoteinfo(text)
  OWNER TO chilli;
