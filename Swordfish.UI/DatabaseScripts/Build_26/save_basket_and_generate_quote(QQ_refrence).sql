﻿-- Function: save_basket_and_generate_quote(text)

-- DROP FUNCTION save_basket_and_generate_quote(text);

CREATE OR REPLACE FUNCTION save_basket_and_generate_quote(basketjson text)
  RETURNS text AS
$BODY$
	DECLARE Quote_object JSON;
	Declare Search_Data JSON;
	Declare Customer_refrence_code character varying(15);
	DECLARE Search_Model_ID TEXT;
	DECLARE Quote_Header JSON;
	DECLARE QuoteHeaderID INT;
	DECLARE Flight_Component JSON;
	DECLARE Flight_Component_ID INT;
	DECLARE Component_Type TEXT;
	DECLARE Quote_Details_ID INT;
	DECLARE Hotel_Component JSON;
	DECLARE Hotel_Component_ID INT;
	DECLARE Transfer_Component JSON;
	Declare Error_log text;
	Declare COLUMN_NAME text;
	Declare PG_DATATYPE_NAME text;
	Declare TABLE_NAME text;
	Declare PG_EXCEPTION_CONTEXT text;
	DECLARE Transfer_Component_ID INT;
	DECLARE Bags_Component JSON;
	DECLARE Bags_Component_ID INT;
	DECLARE flight_component_details json;
Begin
	IF length(BasketJson) <> 0
	THEN
		
		raise notice 'Convering input parameter as JSON Object';
		SELECT  (BasketJson)::json INTO Quote_object;
		IF(select count(*) from json_each(Quote_object))>0
		THEN 
			raise notice 'Collecting search data from Quote_object';
			SELECT (Quote_object->>'searchModel')::json into Search_Data;
			raise notice '%',Search_Data;
			raise notice 'Collecting QuoteHeader data from Quote object';
			SELECT (Quote_object->>'quoteHeader')::json INTO Quote_Header;
			raise notice '%',Quote_Header;
			raise notice 'Collecting flight component data and pusing them into json object Flight_Component';
			SELECT (Quote_object->>'flightComponents')::json INTO Flight_Component;
			raise notice '%',Flight_Component;
			raise notice 'Collecting Hotel component data and pusing them into json object Hotel_Component';
			SELECT (Quote_object->>'hotelComponent')::json INTO Hotel_Component;
			raise notice '%',Hotel_Component;
			raise notice 'Collecting Bags Component data and pusing them into json object Bags_Component';
			SELECT (Quote_object->>'bagsComponents')::json INTO Bags_Component;	
			raise notice '%',Bags_Component;	
			raise notice 'Collecting Transfer Component data and pusing them into json object Transfer_Component';
			SELECT (Quote_object->>'transferComponents')::json INTO Transfer_Component;	
			raise notice '%',Transfer_Component;
			IF(select count(*) from json_each(Hotel_Component) )> 0
			THEN
				IF( select count(*) from json_array_elements(Flight_Component))>0
				THEN
					IF(select count(*) from json_each(Search_Data))>0
					THEN
						raise notice 'Inserting Search Data into Search_Model table';
						select savesearchdata(cast(Search_Data as text)) into Search_Model_ID;
						IF(cast(Search_Model_ID as int) = 0)
						THEN
							raise exception '';
						END IF;
					END IF;
					IF(select count(*) from json_each(Quote_Header))>0
					THEN
						select savebasketheader(cast(Quote_Header as text),Search_Model_ID) into QuoteHeaderID;
						IF(cast(QuoteHeaderID as int) = 0)
						THEN
							raise exception '';
						else
						Customer_refrence_code:=( select 'SF-QQ' || to_char(now(),'yy') || LPAD(QuoteHeaderID::text, 6, '0'));
						update public.quote_header set customer_reference_code=cast(Customer_refrence_code as character varying) where quote_header_id=QuoteHeaderID;
						END IF;
					else
					raise exception '';
					END IF;
					IF(select count(*) from json_array_elements(Flight_Component))>0
					THEN
						FOR flight_component_details IN select * from json_array_elements((Flight_Component)::json) 
						LOOP IF (select count(*) from json_each(flight_component_details))>0
						THEN
							raise notice 'Inserting flight component data into flight_component table';
							raise notice 'flight_component_details %',flight_component_details;
							select saveflightcomponentdetails(cast(flight_component_details as text)) into Flight_Component_ID;
							raise notice 'end Inserting components in flightcomponnet table %',Flight_Component_ID;
			
							IF(Flight_Component_ID <> 0)
							THEN		
								Raise Notice 'Flight_Component_ID %',Flight_Component_ID;
								select savequotedetails(cast(Flight_Component_ID as text),cast(QuoteHeaderID as text),cast(flight_component_details->>'ComponentTypeOptionId' as int),cast(Search_Data->>'CreatedBy' as text)) into 								Quote_Details_ID;
							END IF;
							raise notice 'Inserting components in Quote_details table %',Quote_Details_ID;
						END IF;					
						END LOOP;
					
					END IF;
					IF(select count(*) from json_each(Hotel_Component))>0
					THEN
						raise notice 'Inserting flight component data into flight_component table';
						select savehotelcomponentdetails(cast(Hotel_Component as text)) into Hotel_Component_ID ;
						raise notice 'Inserting components in Quote_details table';
						IF(Hotel_Component_ID <> 0)
						THEN
							select  savequotedetails(cast(Hotel_Component_ID as text),cast(QuoteHeaderID as Text),cast(Hotel_Component->>'ComponentTypeOptionId' as int),cast(Search_Data->>'CreatedBy' as text)) into Quote_Details_ID; 	
						END IF;
					END IF;
			
					IF(SELECT COUNT(*) FROM json_array_elements(TRANSFER_COMPONENT))>0
					THEN
						RAISE NOTICE 'INSERTING TRANSFER COMPONENT DATA INTO TRANSFER_COMPONENT TABLE';
						RAISE NOTICE '%',TRANSFER_COMPONENT;
						SELECT savetransfercomponentdetails(cast(TRANSFER_COMPONENT as text)) INTO TRANSFER_COMPONENT_ID;
						RAISE NOTICE 'INSERTING COMPONENTS IN QUOTE_DETAILS TABLE';
						IF(TRANSFER_COMPONENT_ID <> 0)
						THEN
							raise notice '%',TRANSFER_COMPONENT_ID;
							SELECT SAVEQUOTEDETAILS(cast(TRANSFER_COMPONENT_ID as text),cast(QuoteHeaderID as text),6,cast(Search_Data->>'CreatedBy' as text)) INTO QUOTE_DETAILS_ID;
						END IF;
					END IF;
			
					IF(select count(*) from json_array_elements(Bags_Component))>0
					THEN
						raise notice 'Inserting bag Component data into bag_component table';
						raise notice '%',Bags_Component;
						select savebagcomponentdetails(cast(Bags_Component as text)) into Bags_Component_ID;
						raise notice 'end of Inserting components  bags_component %',Bags_Component_ID;
						IF(cast(Bags_Component_ID as int) <> 0)
						THEN
							select savequotedetails(cast(Bags_Component_ID as text),cast(QuoteHeaderID as text),7,cast(Search_Data->>'CreatedBy' as text)) into Quote_Details_ID;
						END IF;
					END IF;
	
				END IF;
		-- Raise EXCEPTION 'error saving data in basket releated tables';
-- 		GET STACKED DIAGNOSTICS COLUMN_NAME= COLUMN_NAME,
-- 					PG_DATATYPE_NAME=PG_DATATYPE_NAME,
-- 					TABLE_NAME=TABLE_NAME;
-- 					PG_EXCEPTION_CONTEXT=PG_EXCEPTION_CONTEXT;
-- 		Error_log='column name= % data type related to exception =%;name of the table releated to exception =%;name of the function that raised exception =SaveSearchData; line number releated to exception=%;',
-- 		COLUMN_NAME,PG_DATATYPE_NAME,TABLE_NAME,PG_EXCEPTION_CONTEXT;
			END IF;
		
		END IF;
	RETURN cast(Customer_refrence_code||','||QuoteHeaderID as text);
	
	END IF;	
COMMIT;
END;

$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
ALTER FUNCTION save_basket_and_generate_quote(text)
  OWNER TO chilli;
