-- Function: save_booking_payment_details(text)

-- DROP FUNCTION save_booking_payment_details(text);

CREATE OR REPLACE FUNCTION save_booking_payment_details(jsoninput text)
  RETURNS text AS
$BODY$
-- Created/Modified By		||Created/Modified Date 		||Comments
--========================================================================================================
-- Thamma Reddy		||07/06/2016				||Restrucuring DB Object
-- 
  DECLARE payment_object  JSON;  
  DECLARE Payment JSOn;
  DECLARE pay_id INT; 
  BEGIN
	IF length(jsoninput) <> 0
	THEN
		raise notice 'Converting input param as JSON object';
		SELECT (jsoninput)::json  INTO   payment_object;
    
		raise notice 'Inserting payments';
		FOR Payment IN (SELECT * FROM   json_array_elements((payment_object)::json)) 
		LOOP IF(SELECT count(*) FROM   json_each(Payment)) > 0
			THEN
				pay_id = cast(Payment->>'id' as int);
				raise notice ' Payment Id Temp %',pay_id;
				IF(pay_id>0)
				THEN
					UPDATE public.payment_extension
					SET
					merchant_refrence = Payment->>'merchantRefrence',
					transaction_id = Payment->>'transactionId',
					result_datetime = Payment->>'resultDatetime',
					processing_db = Payment->>'processingdb',
					error_msg = Payment->>'errormsg',
					merchant_number = Payment->>'merchantNumber',
					tid = Payment->>'tid',
					scheme_name = Payment->>'schemeName',
					message_number = Payment->>'messageNumber',
					auth_code = Payment->>'authCode',
					auth_message = Payment->>'authMessage',
					vertel = Payment->>'vertel',
					txn_result = Payment->>'txnResult',
					pcavs_result = Payment->>'pcavsResult',
					ad1avs_result = Payment->>'ad1avsResult',
					cvc_result = Payment->>'cvcResult',
					arc = Payment->>'arc',
					iadarc = Payment->>'iadarc',
					iadoad = Payment->>'iadoad',
					isd = Payment->>'isd',
					authorisingentity = Payment->>'authorisingentity',
					card_easy_response = Payment->>'cardEasyResponse',
					recur=Payment->>'recur',
					card_type=Payment->>'cardType',
					surcharge=Payment->>'surcharge',
					pan=Payment->>'pan',
					manual=Payment->>'manual',
					modify_by = cast(Payment->>'agentId' as INT),
					modify_date = now()
					WHERE payment_id = pay_id;
				ELSE
					INSERT INTO payments
					(
						quote_header_id,
						amount,
						cost,
						card_charges,
						card_number,
						total_cost,
						first_name,
						last_name,      
						name_on_card,
						card_type,
						card_type_code,
						payment_address1,
						payment_address2,
						payment_country,
						payment_postcode,
						card_valid_from,
						card_expiry,
						house_number,
						deposit_type,
						create_by,
						create_date
						                        
					)
					VALUES
					(
						cast(Payment->>'quoteHeaderId' as int),
						cast(Payment->>'amount' AS  NUMERIC),
						cast(Payment->>'cost' AS NUMERIC),
						cast(Payment->>'cardCharges'  AS NUMERIC),
						Payment->>'cardNumber',
						cast(Payment->>'totalCost'  AS NUMERIC),
						Payment->>'firstName',
						Payment->>'lastName',
						Payment->>'name',
						Payment->>'cardType',
						Payment->>'cardTypeCode',
						Payment->>'address',
						Payment->>'city',
						Payment->>'country',
						Payment->>'postCode',
						Payment->>'cardValidFrom',   
						Payment->>'cardExpiry',  
						Payment->>'houseNumber',
						Payment->>'depositType',                                  
						cast(Payment->>'agentId' AS INT),
						now()
					);
					--payment_id:=(SELECT currval(pg_get_serial_sequence('payments','payment_id')));    
					pay_id:=(SELECT lastval());                
					raise notice ' id %',pay_id;
			    
						IF(pay_id)>0 
						THEN	
						raise notice 'payment_id %',pay_id;				
							insert into public.payment_extension
							(
								payment_id,
								merchant_refrence,
								transaction_id,
								result_datetime,
								processing_db,
								error_msg,
								merchant_number,
								tid,
								scheme_name,
								message_number,
								auth_code,
								auth_message,
								vertel,
								txn_result,
								pcavs_result,
								ad1avs_result,
								cvc_result,
								arc,
								iadarc,
								iadoad,
								isd,
								authorisingentity,
								card_easy_response,
								recur,
					                        card_type,
					                        surcharge,
					                        pan,
					                        manual,
								create_by,
								create_date
							) values
							(
								 pay_id,
								 Payment->>'merchantRefrence',
								 Payment->>'transactionId',
								 Payment->>'resultDatetime',
								 Payment->>'processingdb',
								 Payment->>'errormsg',
								 Payment->>'merchantNumber',
								 Payment->>'tid',
								 Payment->>'schemeName',
								 Payment->>'messageNumber',
								 Payment->>'authCode',
								 Payment->>'authMessage',
								 Payment->>'vertel',
								 Payment->>'txnResult',
								 Payment->>'pcavsResult',
								 Payment->>'ad1avsResult',
								 Payment->>'cvcResult',
								 Payment->>'arc',
								 Payment->>'iadarc',
								 Payment->>'iadoad',
								 Payment->>'isd',
								 Payment->>'authorisingentity',
								 Payment->>'cardEasyResponse',
								 Payment->>'recur',
					                         Payment->>'cardType',
					                         Payment->>'surcharge',
					                         Payment->>'pan',
					                         Payment->>'manual',
								cast(Payment->>'agentId' as INT),
								now()
							);
						ELSE
						RAISE NOTICE 'ERROR SAVING DATA IN payment_extension TABLE';
						END IF;
				END IF;
				
				

				END IF;
			END LOOP; 
		END IF;	
		RETURN pay_id;
END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
ALTER FUNCTION save_booking_payment_details(text)
  OWNER TO chilli;
