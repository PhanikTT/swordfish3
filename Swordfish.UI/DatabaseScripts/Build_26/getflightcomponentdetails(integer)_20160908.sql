﻿-- Function: getflightcomponentdetails(integer)

-- DROP FUNCTION getflightcomponentdetails(integer);

CREATE OR REPLACE FUNCTION getflightcomponentdetails(IN component_id integer)
  RETURNS TABLE(departuredatetime timestamp without time zone, arrivaldatetime timestamp without time zone, flightnumber integer, journeyindicator integer, departureairportcode character varying, arrivalairportcode character varying, marketingairline character varying, 
flightdirectionoptionid integer, flightdirection character varying, marketingairlinecode character varying,CabinClass character varying) AS
$BODY$

Begin
				
				
					
					return query
					select					
					flight_component_details.departure_datetime as departureDateTime, 
					flight_component_details.arrival_datetime as arrivalDateTime, 
					flight_component_details.flight_number as flightNumber, 
					flight_component_details.journey_indicator as journeyIndicator, 
					flight_component_details.departure_airport_code as departureAirportCode, 
					flight_component_details.arrival_airport_code as arrivalAirportCode, 					
					flight_component_details.marketing_airline as marketingAirline,	
					tsoption.tsoptionid as flightdirectionoptionid,
					tsoption.name as flightdirection,
					flight_component_details.marketing_airline_code as marketingairlinecode,
					flight_component_details.cabin_class as CabinClass
					FROM
					public.flight_component_details join public.tsoption on(flight_component_details.direction_option_id=tsoption.tsoptionid)
					where flight_component_details.flight_component_id=cast(component_id as int)
					order by flight_component_details_id;
				

				
end;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100
  ROWS 1000;
ALTER FUNCTION getflightcomponentdetails(integer)
  OWNER TO chilli;
