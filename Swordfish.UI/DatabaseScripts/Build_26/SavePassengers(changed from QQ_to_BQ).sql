﻿-- Function: savepassengerdetails(text)

-- DROP FUNCTION savepassengerdetails(text);

CREATE OR REPLACE FUNCTION savepassengerdetails(passengerdetails text)
  RETURNS integer AS
$BODY$ DECLARE passengers_Data JSON;
DECLARE passenger_Data JSON;
BEGIN
-- Created/Modified BY		||Created/Modified date 		||Comments
--========================================================================================================
-- Nagendra BAbu		||10/06/2016				||Restrucuring DB OBJECT
-- 
    IF length (
        passengerDetails )
    <> 0 THEN raise notice 'Converting input param as JSON object';
SELECT (
        passengerDetails ) ::json INTO passengers_Data;
RAISE NOTICE 'Save passenger deails started';
FOR passenger_Data IN (
    SELECT
        *
    FROM
        json_array_elements (
            (
                passengers_Data ) ::json ) )
LOOP IF (
    SELECT
        count ( * )
    FROM
        json_each (
            passenger_Data ) )
> 0 THEN IF (
    (
        SELECT
            1
        FROM
            passengers
        WHERE
            quote_header_id = CAST (
                passenger_Data ->> 'QouteHeaderID' AS INT )
            AND seq_no = CAST (
                passenger_Data ->> 'SeqNo' AS INT ) )
    = 1 )
THEN UPDATE passengers
SET
    title = passenger_Data ->> 'Title',
    first_name = passenger_Data ->> 'FirstName',
    last_name = passenger_Data ->> 'LastName',
    is_lead_passenger = CAST (
        passenger_Data ->> 'isLeadPssenger' AS BOOLEAN ),
    counts_as = CASE
        WHEN passenger_Data ->> 'PassengerType' = 'Adult' THEN 'A'
        WHEN passenger_Data ->> 'PassengerType' = 'Child' THEN 'C'
        WHEN passenger_Data ->> 'PassengerType' = 'Infant' THEN 'I'
    END,
    is_deleted = FALSE,
    address1 = passenger_Data ->> 'Address1',
    address2 = passenger_Data ->> 'Address2',
    address3 = passenger_Data ->> 'Address3',
    email = passenger_Data ->> 'Email',
    mobile = passenger_Data ->> 'Mobile',
    telephone = passenger_Data ->> 'Telephone',
    postcode = passenger_Data ->> 'PostCode',
 
    dob = CAST (
        passenger_Data ->> 'DateOfBirth' AS date ),
    quote_header_id = CAST (
        passenger_Data ->> 'QouteHeaderID' AS INT ),
      
    passenger_type_option_id = (
        SELECT
            tsoptionid
        FROM
            tsoption
        WHERE
            name LIKE passenger_Data ->> 'PassengerType' ),
    is_email_subscribed = CAST (
        passenger_Data ->> 'sendOffersDetailsOnEmail' AS BOOLEAN ),
    is_sms_subscribed = CAST (
        passenger_Data ->> 'sendOffersDetailsOnSMS' AS BOOLEAN ),
    modify_by = CAST (
        passenger_Data ->> 'UserId' AS INT ),
    modify_date = now ( ),
     city_town = passenger_Data ->> 'CityTown',
    country = passenger_Data ->> 'Country'
WHERE
    quote_header_id = CAST (
        passenger_Data ->> 'QouteHeaderID' AS INT )
    AND seq_no = CAST (
        passenger_Data ->> 'SeqNo' AS INT );
ELSE INSERT INTO passengers (
    quote_reference,
    title,
    first_name,
    last_name,
    is_lead_passenger,
    counts_as,
    is_deleted,
    address1,
    address2,
    address3,
    email,
    mobile,
    telephone,
    postcode,
    dob,  
    quote_header_id,
    passenger_type_option_id,
    is_email_subscribed,
    is_sms_subscribed,  
    created_by,
    created_date,
    seq_no,
    city_town,
    country )
VALUES (
    passenger_Data ->> 'QuoteRef',
    passenger_Data ->> 'Title',
    passenger_Data ->> 'FirstName',
    passenger_Data ->> 'LastName',
    CAST (
        passenger_Data ->> 'isLeadPssenger' AS BOOLEAN ),
    CASE
        WHEN passenger_Data ->> 'PassengerType' = 'Adult' THEN 'A'
        WHEN passenger_Data ->> 'PassengerType' = 'Child' THEN 'C'
        WHEN passenger_Data ->> 'PassengerType' = 'Infant' THEN 'I'
    END,
    FALSE,
    passenger_Data ->> 'Address1',
    passenger_Data ->> 'Address2',
    passenger_Data ->> 'Address3',
    passenger_Data ->> 'Email',
    passenger_Data ->> 'Mobile',
    passenger_Data ->> 'Telephone',
    passenger_Data ->> 'PostCode',
        CAST (
        passenger_Data ->> 'DateOfBirth' AS date ),
    CAST (
        passenger_Data ->> 'QouteHeaderID' AS INT ),
    (
        SELECT
            tsoptionid
        FROM
            tsoption
        WHERE
            name LIKE passenger_Data ->> 'PassengerType' ),
    CAST (
        passenger_Data ->> 'sendOffersDetailsOnEmail' AS BOOLEAN ),
    CAST (
        passenger_Data ->> 'sendOffersDetailsOnSMS' AS BOOLEAN ),
   

    CAST (
        passenger_Data ->> 'UserId' AS INT ),
    now ( ),
    CAST (
        passenger_Data ->> 'SeqNo' AS INT ),
        passenger_Data ->> 'CityTown',
        passenger_Data ->> 'Country');
END IF;
END IF;
END LOOP;
UPDATE public.quote_header SET customer_reference_code = replace(customer_reference_code, 'QQ', 'BQ') where quote_header_id=CAST (passenger_Data ->> 'QouteHeaderID' AS INT );
RAISE NOTICE 'Save passenger deails ended';
RETURN 1 AS RESULT;
END IF;
END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
ALTER FUNCTION savepassengerdetails(text)
  OWNER TO chilli;
