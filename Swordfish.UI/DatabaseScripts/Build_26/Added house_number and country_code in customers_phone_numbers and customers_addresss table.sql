ALTER TABLE customers_addresses ADD COLUMN house_number varchar(50);

ALTER TABLE customers_phone_numbers ADD COLUMN country_code varchar(20);