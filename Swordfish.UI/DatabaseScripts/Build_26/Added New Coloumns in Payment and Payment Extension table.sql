ALTER TABLE payments ADD COLUMN first_name varchar(100);

ALTER TABLE payments ADD COLUMN last_name varchar(100);

ALTER TABLE payment_extension ADD COLUMN recur varchar(50);

ALTER TABLE payment_extension ADD COLUMN card_type varchar(50);

ALTER TABLE payment_extension ADD COLUMN surcharge varchar(50);

ALTER TABLE payment_extension ADD COLUMN pan varchar(50);

ALTER TABLE payment_extension ADD COLUMN manual varchar(50);

ALTER TABLE payment_extension ALTER card_easy_response TYPE text;