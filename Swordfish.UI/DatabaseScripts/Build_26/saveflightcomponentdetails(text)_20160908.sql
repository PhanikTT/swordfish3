﻿-- Function: saveflightcomponentdetails(text)

-- DROP FUNCTION saveflightcomponentdetails(text);

CREATE OR REPLACE FUNCTION saveflightcomponentdetails(flight_component_detail text)
  RETURNS text AS
$BODY$
	Declare Error_log text;
	Declare flight_component_id int;
	Declare COLUMN_NAME text;
	Declare PG_DATATYPE_NAME text;
	Declare TABLE_NAME text;
	Declare PG_EXCEPTION_CONTEXT text;
	DECLARE FlightComponent json;
	DECLARE flight_component_details JSON;
	Declare Flight_Component json;
	DECLARE flight_component_details_id int;
	DECLARE flight_option_id int;
	DECLARE flight_direction_option_id int;

Begin
	
	select (flight_component_detail)::json into Flight_Component;	
	IF(select count(*) from json_each(Flight_Component))>0
	THEN
		--flight_option_ID:=(select tsoptionid from tsoption where LOWER(name)=LOWER(Flight_Component->>'ComponentType'));
		Raise notice 'Flight_Component %',Flight_Component->'Amount';
		INSERT INTO public.flight_component(
		amount,
		currency_code,
		source,
		arrival,
		component_type_option_id,
		created_by,
		created_on,
		modified_by,
		modified_on,
		flight_quote_id,
		source_url,
		base_fare_amount,
		base_fare_currency_code		
		)
	
	values(
		cast(Flight_Component->>'Amount' as money),
		Flight_Component->>'CurrencyCode',
		Flight_Component->>'Source',
		Flight_Component->>'Arrival',
		cast(Flight_Component->>'ComponentTypeOptionId' as int),
		Cast(Flight_Component->>'CreatedBy' as int),
		now(),
		Cast(Flight_Component->>'ModifiedBy' as int),
		now(),
		Flight_Component->>'FlightQuoteId',
		Flight_Component->>'SourceUrl',
		cast(Flight_Component->>'BaseFareAmount' as money),
		Flight_Component->>'BaseFareCurrencyCode'
		);

		flight_component_id:=(SELECT currval(pg_get_serial_sequence('flight_component','flight_component_id')));
		raise notice 'end of inserting data in flight component table %',flight_component_id;
		-- else
-- 		RAISE
-- 		EXCEPTION 'error  storing data in flight_component table';
-- 		GET STACKED DIAGNOSTICS COLUMN_NAME= COLUMN_NAME,
-- 					PG_DATATYPE_NAME=PG_DATATYPE_NAME,
-- 					TABLE_NAME=TABLE_NAME;
-- 					PG_EXCEPTION_CONTEXT=PG_EXCEPTION_CONTEXT;
-- 		Error_log='column name= % data type related to exception =%;name of the table releated to exception =%;name of the function that raised exception =SaveSearchData; line number releated to exception=%;',
-- 		COLUMN_NAME,PG_DATATYPE_NAME,TABLE_NAME,PG_EXCEPTION_CONTEXT;
-- 		ROLLBACK TRANSACTION;

		FOR flight_component_details IN select * from json_array_elements((Flight_Component->>'FlightComponentDetails')::json) 
		LOOP IF (select count(*) from json_each(flight_component_details))>0
		THEN
			--flight_direction_option_id:=(select tsoptionid from tsoption where LOWER(name)=LOWER(flight_component_details->>'Direction'));
			raise notice '%',flight_component_details;
			raise notice 'inserting flight component details in flight_component_details table using flight_component_id';
			insert into public.flight_component_details(
			flight_component_id,
			departure_datetime,
			arrival_datetime,
			flight_number,
			journey_indicator,
			departure_airport_code,
			arrival_airport_code,
			marketing_airline,
			marketing_airline_code,
			direction_option_id,
			created_by,
			created_on,
			modified_by,
			modified_on,
			cabin_class
			)  values(
			cast(flight_component_id as int),
			Cast(flight_component_details->>'DepartureDateTime' as timestamp without time zone),
			Cast(flight_component_details->>'ArrivalDateTime' as timestamp without time zone),
			Cast(flight_component_details->>'FlightNumber' as int),
			Cast(flight_component_details->>'JourneyIndicator' as int),
			flight_component_details->>'DepartureAirportCode',			
			flight_component_details->>'ArrivalAirportCode' ,
			flight_component_details->>'MarketingAirLine',
			flight_component_details->>'MarketingAirlineCode',
			cast(flight_component_details->>'DirectionOptionId' as int),
			Cast(flight_component_details->>'CreatedBy' as int),
			now(),
			Cast(flight_component_details->>'ModifiedBy' as int),
			now(),
			flight_component_details->>'CabinClass'
			);
			flight_component_details_id:=(SELECT currval(pg_get_serial_sequence('flight_component_details','flight_component_details_id')));
			Raise NOTICE 'End of inserting data in flight component details %',flight_component_details_id;
			else
			RAISE
			EXCEPTION 'error saving data in flight component details';
			GET STACKED DIAGNOSTICS COLUMN_NAME= COLUMN_NAME,
						PG_DATATYPE_NAME=PG_DATATYPE_NAME,
						TABLE_NAME=TABLE_NAME;
						PG_EXCEPTION_CONTEXT=PG_EXCEPTION_CONTEXT;
			Error_log='column name= % data type related to exception =%;name of the table releated to exception =%;name of the function that raised exception =SaveSearchData; line number releated to exception=%;',
			COLUMN_NAME,PG_DATATYPE_NAME,TABLE_NAME,PG_EXCEPTION_CONTEXT;
			ROLLBACK TRANSACTION;
			RAISE NOTICE 'ERROR SAVING DATA IN flight_component table';
		END IF;
		END LOOP;
	END IF;

	RETURN cast(flight_component_id AS TEXT);
End;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
ALTER FUNCTION saveflightcomponentdetails(text)
  OWNER TO chilli;
