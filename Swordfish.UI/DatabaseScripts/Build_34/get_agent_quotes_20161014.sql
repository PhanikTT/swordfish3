﻿-- Function: get_agent_quotes(character varying, character varying, text, text, integer, boolean)

-- DROP FUNCTION get_agent_quotes(character varying, character varying, text, text, integer, boolean);

CREATE OR REPLACE FUNCTION get_agent_quotes(
    IN quote_ref character varying,
    IN customer_name character varying,
    IN from_date text,
    IN to_date text,
    IN agentid integer,
    IN islast24hours boolean)
  RETURNS TABLE(quote_header_id integer, customer_reference_code character varying, quotedon timestamp without time zone, first_name character varying, last_name character varying, source_code character varying, destination_code character varying, start_date date, end_date date, hotel_name character varying, adults_count integer, children_count integer, infants_count integer, grand_total money, price_per_person money, occupancy json) AS
$BODY$ DECLARE todate date;
DECLARE fromdate date;
BEGIN
    IF (
        from_date <> '' )
    THEN fromdate := to_date (
        from_date,
        'YYYY-MM-DD' );
raise notice '%fromdate :',
fromdate;
END IF;
IF (
    to_date <> '' )
THEN todate := to_date (
    to_date,
    'YYYY-MM-DD' );
raise notice '%todate :',
to_date;
END IF;
IF (
    quote_ref = '' )
THEN quote_ref := NULL;
raise notice '%quote_ref :',
quote_ref;
END IF;
IF (
    customer_name = '' )
THEN customer_name := NULL;
raise notice '%customer_name :',
customer_name;
END IF;
IF (
    islast24hours = TRUE )
THEN RETURN QUERY
SELECT
    qh.quote_header_id,
    qh.customer_reference_code,
    qh.created_on AS quotedon,
    cu.first_name,
    cu.last_name,
    sm.source_code,
    sm.destination_code,
    sm.start_date,
    sm.end_date,
    hc.hotel_name,
    (select cast(sum(oc.adults_count) as int) from occupancy oc where sm.search_model_id = oc.search_model_id) adults_count,
   (select cast(sum(oc.children_count) as int) from occupancy oc where sm.search_model_id = oc.search_model_id) children_count,
   (select cast(sum(oc.infants_count) as int) from occupancy oc where sm.search_model_id = oc.search_model_id) children_count,
    qh.grand_total,
    qh.price_per_person,
    (SELECT to_json(array_agg(Occupancy_Rooms)) FROM(SELECT oc.room_no RoomNo,oc.adults_count AdultsCount,oc.children_count ChildCount,oc.infants_count InfantCount FROM occupancy oc where oc.search_model_id = qh.search_model_id) Occupancy_Rooms) occupancy
FROM
    search_model sm
    INNER JOIN quote_header qh ON sm.search_model_id = qh.search_model_id
    LEFT JOIN customers cu ON qh.customer_id = cu.customer_id
    LEFT JOIN quote_details qd ON qh.quote_header_id = qd.quote_header_id
    LEFT JOIN hotel_component hc ON qd.component_id = hc.hotel_component_id
WHERE
    qd.component_type_option_id = 5
    AND qh.created_on BETWEEN CAST (
        (
            CURRENT_TIMESTAMP - interval '1 day' ) AS timestamp WITHOUT time ZONE )
    AND CAST (
        CURRENT_TIMESTAMP AS timestamp WITHOUT time ZONE )
    AND qh.agent_id = agentid and sm.start_date >now()  order by qh.created_on DESC;
ELSE RETURN QUERY
SELECT
    qh.quote_header_id,
    qh.customer_reference_code,
    qh.created_on AS quotedon,
    cu.first_name,
    cu.last_name,
    sm.source_code,
    sm.destination_code,
    sm.start_date,
    sm.end_date,
    hc.hotel_name,
   (select cast(sum(oc.adults_count) as int) from occupancy oc where sm.search_model_id = oc.search_model_id) adults_count,
   (select cast(sum(oc.children_count) as int) from occupancy oc where sm.search_model_id = oc.search_model_id) children_count,
   (select cast(sum(oc.infants_count) as int) from occupancy oc where sm.search_model_id = oc.search_model_id) children_count,
   qh.grand_total,
   qh.price_per_person,
    (SELECT to_json(array_agg(Occupancy_Rooms)) FROM(SELECT oc.room_no RoomNo,oc.adults_count AdultsCount,oc.children_count ChildCount,oc.infants_count InfantCount FROM occupancy oc where oc.search_model_id = qh.search_model_id) Occupancy_Rooms) occupancy
FROM
    search_model sm
    INNER JOIN quote_header qh ON sm.search_model_id = qh.search_model_id
    LEFT JOIN customers cu ON qh.customer_id = cu.customer_id
    LEFT JOIN quote_details qd ON qh.quote_header_id = qd.quote_header_id
    LEFT JOIN hotel_component hc ON qd.component_id = hc.hotel_component_id
WHERE
    qd.component_type_option_id = 5
    AND qh.agent_id = agentid
    AND (
        COALESCE (
            quote_ref,
            NULL )
        IS NULL
        OR quote_ref = qh.customer_reference_code )
    AND (
        COALESCE (
            customer_name,
            NULL )
        IS NULL
        OR  LOWER(cu.first_name) like '%'||LOWER(customer_name) ||'%'
        OR COALESCE (
            customer_name,
            NULL )
        IS NULL
        OR  LOWER(cu.last_name)  like '%'||LOWER(customer_name) ||'%'
        OR  LOWER(cu.first_name||' '||cu.last_name) like LOWER(customer_name)||'%')
    AND (
        COALESCE (
            fromdate,
            NULL )
        IS NULL
        OR fromdate <= cast(qh.created_on as date))
    AND (
        COALESCE (
            todate,
            NULL )
        IS NULL
        OR todate >= cast(qh.created_on as date)) and  sm.start_date >now() order by qh.created_on DESC;
END IF;
END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100
  ROWS 1000;
ALTER FUNCTION get_agent_quotes(character varying, character varying, text, text, integer, boolean)
  OWNER TO chilli;
