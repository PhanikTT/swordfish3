﻿--select * from topdog_supplier_mapping where record_type_option_id=41 and source_system_code='GMD'


update topdog_supplier_mapping
set mapped_system_code='383'
where mapping_id=7089;

update topdog_supplier_mapping
set mapped_system_code='700'
where mapping_id=7078;

update topdog_supplier_mapping
set mapped_system_description='Avro plc',source_system_description='Avro plc'
where mapping_id=6982;

update topdog_supplier_mapping
set mapped_system_code='686'
where mapping_id=6989;

update topdog_supplier_mapping
set mapped_system_description='First Choice Holidays',source_system_description='First Choice Holidays',mapped_system_code='8'
where mapping_id=7015;


insert into topdog_supplier_mapping
(source_system_code,source_system_description,record_type_option_id,source_system_option_id,mapped_system_code,mapped_system_description,Mapped_System_Option_ID,created_by,created_date,modified_by,modified_date)
Select 'LS','Jet2',41,37,'691','Jet2',43,6,now(),6,now();


insert into topdog_supplier_mapping
(source_system_code,source_system_description,record_type_option_id,source_system_option_id,mapped_system_code,mapped_system_description,Mapped_System_Option_ID,created_by,created_date,modified_by,modified_date)
Select 'OAH','Olympic Holidays',41,37,'725','Olympic Holidays',43,6,now(),6,now();


insert into topdog_supplier_mapping
(source_system_code,source_system_description,record_type_option_id,source_system_option_id,mapped_system_code,mapped_system_description,Mapped_System_Option_ID,created_by,created_date,modified_by,modified_date)
Select 'GMD','Gold Medal Travel Group',41,37,'370','Gold Medal Travel Group',43,6,now(),6,now();

insert into topdog_supplier_mapping
(source_system_code,source_system_description,record_type_option_id,source_system_option_id,mapped_system_code,mapped_system_description,Mapped_System_Option_ID,created_by,created_date,modified_by,modified_date)
Select 'JWL','Jewel in the Crown Holidays',41,37,'497','Jewel in the Crown',43,6,now(),6,now();

insert into topdog_supplier_mapping
(source_system_code,source_system_description,record_type_option_id,source_system_option_id,mapped_system_code,mapped_system_description,Mapped_System_Option_ID,created_by,created_date,modified_by,modified_date)
Select 'TEL','Teleticket',41,37,'295','Teleticket',43,6,now(),6,now();


insert into topdog_supplier_mapping
(source_system_code,source_system_description,record_type_option_id,source_system_option_id,mapped_system_code,mapped_system_description,Mapped_System_Option_ID,created_by,created_date,modified_by,modified_date)
Select 'MON','Fly Monarch',41,37,'685','Fly Monarch',43,6,now(),6,now();
