-- Function: get_customer(character varying, character varying, character varying, character varying, character varying, character varying)

-- DROP FUNCTION get_customer(character varying, character varying, character varying, character varying, character varying, character varying);

CREATE OR REPLACE FUNCTION get_customer(
    IN cust_membershipno character varying,
    IN cust_lastname character varying,
    IN cust_emailtype character varying,
    IN cust_emailaddress character varying,
    IN cust_phonenumber character varying,
    IN cust_postcode character varying)
  RETURNS TABLE(customer_id integer, emailtype character varying, email_address character varying, membership_no character varying, first_name character varying, last_name character varying, email_consent boolean, phone_number character varying, postcode character varying, city character varying, country character varying, address_line1 character varying, address_line2 character varying, numberofbookedquotes bigint, totalquotes bigint) AS
$BODY$
BEGIN


IF (cust_membershipno = '') THEN
cust_membershipno=NULL;
END IF;
IF (cust_lastname = '') THEN
cust_lastname=NULL;
END IF;
IF (cust_emailtype = '') THEN
cust_emailtype=NULL;
END IF;
IF (cust_emailaddress = '') THEN
cust_emailaddress=NULL;
END IF;
IF (cust_phonenumber = '') THEN
cust_phonenumber=NULL;
END IF;
IF (cust_postcode = '') THEN
cust_postcode=NULL;
END IF;

RETURN QUERY


--select coalesce(cust_membershipno,'it is empty');
 select cu.customer_id,cu.emailtype, cu.email_address ,cu.membership_no ,cu.first_name , cu.last_name ,cu.email_consent ,  cp.phone_number , ca.postcode ,ca.city,ca.country,ca.address_line1,ca.address_line2,
 (select count(q.*) from quote_header q
 inner join tsoption tso on q.quote_type_option_id = tso.tsoptionid
  where cu.customer_id =q.customer_id  and tso.name='Book quote') as numberOfBookedQuotes,
 (select count(q.*) from quote_header q where cu.customer_id =q.customer_id) as totalQuotes
   from customers cu 
 inner join customers_phone_numbers cp on cu.customer_id = cp.customer_id
 inner join customers_addresses ca on ca.customer_id=cu.customer_id
where lower(cu.membership_no) like '%'|| lower(coalesce(cust_membershipno,cu.membership_no))||'%'   and  (lower(cu.last_name) like '%'||lower(coalesce(cust_lastname,cu.last_name))||'%' or lower(cu.first_name) like '%'||lower(coalesce(cust_lastname,cu.last_name))||'%') and lower(cu.emailtype) like '%'|| lower(coalesce(cust_emailtype,cu.emailtype))||'%' and  
 lower(cu.email_address) like '%'||  lower(coalesce(cust_emailaddress,cu.email_address))||'%' and cp.phone_number like '%'||  coalesce(cust_phonenumber,cp.phone_number)||'%'and lower(ca.postcode) like '%'||  lower(coalesce(cust_postcode,ca.postcode ))||'%' 
;

END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100
  ROWS 1000;
ALTER FUNCTION get_customer(character varying, character varying, character varying, character varying, character varying, character varying)
  OWNER TO root;