-- Function: getflightcomponentdetails(integer, text)

-- DROP FUNCTION getflightcomponentdetails(integer, text);

CREATE OR REPLACE FUNCTION getflightcomponentdetails(
    IN component_id integer,
    IN component_type text)
  RETURNS TABLE(departuredatetime timestamp without time zone, arrivaldatetime timestamp without time zone, flightnumber integer, journeyindicator integer, departureairportcode character varying, arrivalairportcode character varying, marketingairline character varying, flightdirection character varying, marketingairlinecode character varying) AS
$BODY$

Begin
				
				if(component_type='flight return' or component_type='flight one way')																												
				THEN
					
					return query
					select					
					flight_component_details.departure_datetime as departureDateTime, 
					flight_component_details.arrival_datetime as arrivalDateTime, 
					flight_component_details.flight_number as flightNumber, 
					flight_component_details.journey_indicator as journeyIndicator, 
					flight_component_details.departure_airport_code as departureAirportCode, 
					flight_component_details.arrival_airport_code as arrivalAirportCode, 					
					flight_component_details.marketing_airline as marketingAirline,					
					tsoption.name as flightdirection,
					flight_component_details.marketing_airline_code as marketingairlinecode
					FROM
					public.flight_component_details join public.tsoption on(flight_component_details.direction_option_id=tsoption.tsoptionid)
					where flight_component_details.flight_component_id=cast(component_id as int)
					order by flight_component_details_id;
				

				END IF;
end;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100
  ROWS 1000;
ALTER FUNCTION getflightcomponentdetails(integer, text)
  OWNER TO chilli;
