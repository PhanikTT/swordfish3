﻿
alter table SF_log Add client_IP

CREATE INDEX idx_sf_log_log_id ON sf_log(log_id);
CREATE INDEX idx_sf_log_user_session ON sf_log(user_session);

-- Function: save_sf_log(text, integer, text, text, text, text, text, text, text, integer)

-- DROP FUNCTION save_sf_log(text, integer, text, text, text, text, text, text, text, integer,text);

CREATE OR REPLACE FUNCTION save_sf_log(
    log_type text,
    user_id integer,
    user_session text,
    environment text,
    thread text,
    function_name text,
    message text,
    payload text,
    stack_trace text,
    created_by integer,
    client_ip text)
  RETURNS boolean AS
$BODY$

-- Created/Modified By		||Created/Modified Date 		||Comments
--========================================================================================================
-- Thamma Reddy		||12/07/2016				||Restrucuring DB Object
-- 

BEGIN
	INSERT INTO sf_log (log_type,log_datetime,user_id,user_session,environment,thread,function_name,message,payload,stack_trace,created_by,created_on,client_ip)
	VALUES (log_type,now(),user_id,user_session,environment,thread,function_name,message,payload,stack_trace,created_by,now(),client_ip);

	RETURN TRUE;
END;

$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
ALTER FUNCTION save_sf_log(text, integer, text, text, text, text, text, text, text, integer,text)
  OWNER TO chilli;

SElect * from SF_log