﻿-- Function: gethotelcomponentdetails(integer)

-- DROP FUNCTION gethotelcomponentdetails(integer);

CREATE OR REPLACE FUNCTION gethotelcomponentdetails(IN component_id integer)
  RETURNS TABLE(roomtypecode character varying, rateplancode character varying, rateplanname character varying, ratequoteid character varying, amountaftertax real, roomdescription character varying, canclepolicyindicator boolean,nonrefundable boolean) AS
$BODY$

Begin
				
					return query
					select 	
					hotel_component_details.room_type_code as roomTypeCode,
					hotel_component_details.rate_plan_code as ratePlanCode, 		
					hotel_component_details.rate_plan_name as ratePlanName,		
					hotel_component_details.rate_quote_id as rateQuoteId, 
					cast(translate(substring(cast(hotel_component_details.amount_after_tax as text) from 2 for char_length(cast(hotel_component_details.amount_after_tax as text))),',','') as real) as amountAfterTax,
					hotel_component_details.room_description as roomDescription,
					hotel_component_details.cancelpolicyindicator as cancelpolicyindicator,
					hotel_component_details.nonrefundable as nonrefundable
					FROM
					public.hotel_component join public.hotel_component_details on(hotel_component.hotel_component_id=hotel_component_details.hotel_component_id)
					where hotel_component.hotel_component_id=cast(component_id as int) ;
				
end;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100
  ROWS 1000;
ALTER FUNCTION gethotelcomponentdetails(integer)
  OWNER TO chilli;
