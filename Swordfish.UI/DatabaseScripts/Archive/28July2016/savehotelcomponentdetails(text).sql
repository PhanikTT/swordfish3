﻿-- Function: savehotelcomponentdetails_test(text)

-- DROP FUNCTION savehotelcomponentdetails_test(text);

CREATE OR REPLACE FUNCTION savehotelcomponentdetails_test(hotelcomponent text)
  RETURNS text AS
$BODY$
	Declare Error_log text;
	Declare hotel_component_id int;
	Declare COLUMN_NAME text;
	Declare PG_DATATYPE_NAME text;
	Declare TABLE_NAME text;
	Declare PG_EXCEPTION_CONTEXT text;
	DECLARE hotel_component_details JSON;
	declare Hotel_Component JSON;
	declare hotel_component_details_id int;
Begin
	select (hotelcomponent)::json into Hotel_Component;
	IF(select count(*) from json_each(Hotel_Component))>0
	THEN
		raise notice 'start of inserting hotel component data';
		INSERT INTO public.hotel_component(
		hotel_name,
		hotel_code,
		hotel_code_context,
		supplier_resort_id,
		checkin_date,
		checkout_date,
		tti_code,
		Star_Rating,
		Trip_Advisor_Rating,
		hotel_type_id,
		hotel_quote_id,
		created_by,
		created_on,
		modified_by,
		modified_on
		) values(
		Hotel_Component->>'HotelName',
		Hotel_Component->>'HotelCode',
		Hotel_Component->>'HotelCodeContext' ,
		Hotel_Component->>'SupplierResortId',
		cast(Hotel_Component->>'CheckInDate' as timestamp without time zone) ,
		cast(Hotel_Component->>'CheckOutDate' as timestamp without time zone),
		Cast(Hotel_Component->>'TTICode' as int),
		Cast(Hotel_Component->>'StarRating' as real),
		Cast(Hotel_Component->>'TripAdvisorRating' as real),
		Cast(Hotel_Component->>'HotelTypeId' as int),
		Hotel_Component->>'HotelQuoteId',
		Cast(Hotel_Component->>'CreatedBy' as int),
		now(),
		Cast(Hotel_Component->>'ModifiedBy' as int),
		now()
		);

		hotel_component_id:=(SELECT currval(pg_get_serial_sequence('hotel_component','hotel_component_id')));
		raise notice 'end of inserting data in hotel_component table %',hotel_component_id;
		-- else
-- 		RAISE
-- 		EXCEPTION 'error  storing data in hotel_component table';
-- 		GET STACKED DIAGNOSTICS COLUMN_NAME= COLUMN_NAME,
-- 					PG_DATATYPE_NAME=PG_DATATYPE_NAME,
-- 					TABLE_NAME=TABLE_NAME;
-- 					PG_EXCEPTION_CONTEXT=PG_EXCEPTION_CONTEXT;
-- 		Error_log='column name= % data type related to exception =%;name of the table releated to exception =%;name of the function that raised exception =SaveSearchData; line number releated to exception=%;',
-- 		COLUMN_NAME,PG_DATATYPE_NAME,TABLE_NAME,PG_EXCEPTION_CONTEXT;
-- 		ROLLBACK TRANSACTION;
		FOR hotel_component_details IN select * from json_array_elements((Hotel_Component->>'hotelComponentsDetails')::json) 
		LOOP IF (select count(*) from json_each(hotel_component_details))>0
		THEN
			 raise notice 'start of inserting hotel component details in hotel_component_details table using hotel_component_id';
			insert into public.hotel_component_details(
			hotel_component_id,
			room_type_code,
			rate_plan_code,
			rate_plan_name,
			rate_quote_id,
			amount_after_tax,
			created_by,
			created_on,
			modified_by,
			modified_on,
			room_description,
			cancelpolicyindicator,
			nonrefundable
			) values(
			cast(hotel_component_id as int),
			hotel_component_details->>'RoomTypeCode' ,
			hotel_component_details->>'RatePlanCode',
			hotel_component_details->>'RatePlanName' ,
			hotel_component_details->>'RateQuoteId' ,
			cast(hotel_component_details->>'AmountAfterTax' as money),
			Cast(hotel_component_details->>'CreatedBy' as int),
			now(),
			Cast(hotel_component_details->>'ModifiedBy' as int),
			now(),
			hotel_component_details->>'RoomDescription',
			Cast(hotel_component_details->>'CancelPolicyIndicator' as boolean),
			Cast(hotel_component_details->>'NonRefundable' as boolean)
			);
			hotel_component_details_id:=(SELECT currval(pg_get_serial_sequence('hotel_component_details','hotel_component_detail_id')));
			Raise NOTICE 'End of inserting data in hotel component details %',hotel_component_details_id;
			else
			RAISE
			EXCEPTION 'error saving data in hotel component details';
			GET STACKED DIAGNOSTICS COLUMN_NAME= COLUMN_NAME,
						PG_DATATYPE_NAME=PG_DATATYPE_NAME,
						TABLE_NAME=TABLE_NAME;
						PG_EXCEPTION_CONTEXT=PG_EXCEPTION_CONTEXT;
			Error_log='column name= % data type related to exception =%;name of the table releated to exception =%;name of the function that raised exception =SaveSearchData; line number releated to exception=%;',
			COLUMN_NAME,PG_DATATYPE_NAME,TABLE_NAME,PG_EXCEPTION_CONTEXT;
			ROLLBACK TRANSACTION;
			RAISE NOTICE 'ERROR SAVING DATA IN hotel_component_details table';
		END IF;
		END LOOP;
	END IF;
RETURN cast(hotel_component_id AS TEXT);
End;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
ALTER FUNCTION savehotelcomponentdetails_test(text)
  OWNER TO chilli;
