-- Function: getquoteheaderdata(text)

-- DROP FUNCTION getquoteheaderdata(text);

CREATE OR REPLACE FUNCTION getquoteheaderdata(IN quoteid text)
  RETURNS TABLE(quoteheaderid integer, searchmodelid integer, customerreferencecode character varying, agentid integer, customerid integer, customerfirstname character varying, customerlastname character varying, customeremailaddress character varying, address character varying, address_line1 character varying, address_line2 character varying, city character varying, country character varying, customertitle character varying, optionid integer, postcode character varying, phonenumbertype character varying, phonenumber character varying,telephonenumber character varying, quotetype character varying, quotestatusoptionid integer, hotelcost real, baggagecost real, transferscost real, fidelitycost real, totalcost real, marginhotel real, marginflight real, marginextra real, grandtotal real, priceperperson real, internalnotes character varying, senttoemail character varying, senttosms character varying, flightcost real) AS
$BODY$

Begin
		Return query
		select quote_header.quote_header_id as quoteHeaderId,
			quote_header.search_model_id as searchModelId, 
			quote_header.customer_reference_code as customerReferenceCode, 
			quote_header.agent_id as agentId, 
			quote_header.customer_id as customerId, 
			customers.first_name as customerfirstname,
			customers.last_name as customerlastname,
			customers.email_address as customeremailaddress,
			cast(customers_addresses.address as character varying),			
			customers_addresses.address_line1 as address_line1 ,
			customers_addresses.address_line2 as address_line2,
			customers_addresses.city as city,
			customers_addresses.country as country ,
			(select name from tsoption where tsoptionid= customers.title_type_option_id limit 1) as customertitle,
			 customers.title_type_option_id as optionid,
			customers_addresses.postcode as postcode,
			customers_phone_numbers.phone_number_type as phonenumbertype,
			customers_phone_numbers.phone_number as phonenumber,
			customers_phone_numbers.telephone_number as telephonenumber,
			tsoption.name as quoteType, 
			quote_header.quote_status_option_id as quoteStatusOptionId, 
			cast(translate(substring(cast(quote_header.hotel_cost as text) from 2 for char_length(cast(quote_header.hotel_cost as text))),',','') as real) as hotelCost,
			cast(translate(substring(cast(quote_header.baggage_cost as text) from 2 for char_length(cast(quote_header.baggage_cost as text))),',','') as real) as baggageCost,
			cast(translate(substring(cast(quote_header.transfers_cost as text) from 2 for char_length(cast(quote_header.transfers_cost as text))),',','') as real) as transfersCost,
			cast(translate(substring(cast(quote_header.fidelity_cost as text) from 2 for char_length(cast(quote_header.fidelity_cost as text))),',','') as real) as fidelityCost,
			cast(translate(substring(cast(quote_header.total_cost as text) from 2 for char_length(cast(quote_header.total_cost as text))),',','') as real) as totalCost,
			cast(translate(substring(cast(quote_header.margin_hotel as text) from 2 for char_length(cast(quote_header.margin_hotel as text))),',','') as real) as marginHotel,
			cast(translate(substring(cast(quote_header.margin_flight as text) from 2 for char_length(cast(quote_header.margin_flight as text))),',','') as real) as marginflight,
			cast(translate(substring(cast(quote_header.margin_extra as text) from 2 for char_length(cast(quote_header.margin_extra as text))),',','') as real) as marginExtra,
			cast(translate(substring(cast(quote_header.grand_total as text) from 2 for char_length(cast(quote_header.grand_total as text))),',','') as real) as grandTotal,
			cast(translate(substring(cast(quote_header.price_per_person as text) from 2 for char_length(cast(quote_header.price_per_person as text))),',','') as real) as priceperPerson,			
			quote_header.internal_notes as internalNotes, 
			quote_header.sent_to_email as senttoEmail, 
			quote_header.sent_to_sms as senttoSms, 
			cast(translate(substring(cast(quote_header.flight_cost as text) from 2 for char_length(cast(quote_header.flight_cost as text))),',','') as real) as flightCost
			
		FROM public.tsoption join 
		public.quote_header on(tsoption.tsoptionid=quote_header.quote_type_option_id) join
		customers on (quote_header.customer_id=customers.customer_id) join
		customers_addresses on(customers.customer_id=customers_addresses.customer_id) join 
		customers_phone_numbers on(customers.customer_id=customers_phone_numbers.customer_id)
		where quote_header.customer_reference_code=quoteid or cast(quote_header.quote_header_id as text)=quoteid;
End
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100
  ROWS 1000;
ALTER FUNCTION getquoteheaderdata(text)
  OWNER TO chilli;
