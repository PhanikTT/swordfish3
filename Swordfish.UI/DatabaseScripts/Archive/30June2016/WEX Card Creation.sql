﻿-- Table: wex_request

-- DROP TABLE wex_request;

CREATE TABLE wex_request
(
  id serial NOT NULL,
  quoteheaderid character varying(250),
  loadamount numeric(10,2),
  requestreference character varying(250),
  username character varying(250),
  signature character varying(250),
  account character varying(50),
  expiry integer,
  instant boolean,
  activated boolean,
  description character varying(1000),
  effectivestartdate character varying(250),
  title character varying(50),
  firstname character varying(250),
  lastname character varying(250),
  gender character varying(50),
  dateofbirth character varying(50),
  email character varying(250),
  addressid integer NOT NULL,
  street character varying(250),
  postcode character varying(250),
  city character varying(250),
  country character varying(50),
  housenumber character varying(50),
  createddate timestamp without time zone,
  CONSTRAINT wex_request_pkey PRIMARY KEY (id)
)
WITH (
  OIDS=FALSE
);
ALTER TABLE wex_request
  OWNER TO chilli;
COMMENT ON TABLE wex_request
  IS 'This table is used to store WEX request object';

  -- Table: wex_response

-- DROP TABLE wex_response;

CREATE TABLE wex_response
(
  id serial NOT NULL,
  quoteheaderid character varying(250),
  requestreference character varying(250),
  signature character varying(250),
  success character varying(250),
  resultcode character varying(250),
  message character varying(50),
  pan character varying(250),
  cv2 character varying(250),
  expirydate character varying(250),
  cardreference character varying(1000),
  createddate timestamp without time zone,
  CONSTRAINT wex_response_pkey PRIMARY KEY (id)
)
WITH (
  OIDS=FALSE
);
ALTER TABLE wex_response
  OWNER TO chilli;
COMMENT ON TABLE wex_response
  IS 'This table is used to store WEX response object';

-- Function: save_wex_request(numeric, character varying, character varying, character varying, character varying, character varying, integer, boolean, boolean, character varying, character varying, character varying, character varying, character varying, character varying, character varying, character varying, integer, character varying, character varying, character varying, character varying, character varying)

-- DROP FUNCTION save_wex_request(numeric, character varying, character varying, character varying, character varying, character varying, integer, boolean, boolean, character varying, character varying, character varying, character varying, character varying, character varying, character varying, character varying, integer, character varying, character varying, character varying, character varying, character varying);

CREATE OR REPLACE FUNCTION save_wex_request(
    loadamount numeric,
    quoteheaderid character varying,
    requestreference character varying,
    username character varying,
    signature character varying,
    account character varying,
    expiry integer,
    instant boolean,
    activated boolean,
    description character varying,
    effectivestartdate character varying,
    title character varying,
    firstname character varying,
    lastname character varying,
    gender character varying,
    dateofbirth character varying,
    email character varying,
    addressid integer,
    street character varying,
    postcode character varying,
    city character varying,
    country character varying,
    housenumber character varying)
  RETURNS boolean AS
$BODY$
-- Created/Modified By		||Created/Modified Date 		||Comments
--========================================================================================================
-- Venkat Chikkanti		||17/06/2016				||logging WEX Request
-- 
 
  BEGIN
	
				INSERT INTO wex_request
				(
					loadamount,
					quoteheaderid,
					requestreference,
					username,
					signature,
					account,
					expiry,
					  instant,
					  activated ,
					  description ,
					  effectivestartdate ,
					  title ,
					  firstname ,
					  lastname ,
					  gender ,
					  dateofbirth ,
					  email ,
					  addressid ,
					  street ,
					  postcode ,
					  city ,
					  country ,
					  housenumber,
					  createddate                              
				)
				VALUES
				(
					loadamount,
					quoteheaderid,
					requestreference,
					username,
					signature,
					account,
					expiry,
					instant,
					activated ,
					description ,
					effectivestartdate ,
					title ,
					firstname ,
					lastname ,
					gender ,
					dateofbirth ,
					email ,
					addressid ,
					street ,
					postcode ,
					city ,
					country ,
					housenumber,
					now()
				);				

				
 RETURN TRUE;
END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
ALTER FUNCTION save_wex_request(numeric, character varying, character varying, character varying, character varying, character varying, integer, boolean, boolean, character varying, character varying, character varying, character varying, character varying, character varying, character varying, character varying, integer, character varying, character varying, character varying, character varying, character varying)
  OWNER TO chilli;

-- Function: save_wex_response(character varying, character varying, character varying, character varying, character varying, character varying, character varying, character varying, character varying, character varying)

-- DROP FUNCTION save_wex_response(character varying, character varying, character varying, character varying, character varying, character varying, character varying, character varying, character varying, character varying);

CREATE OR REPLACE FUNCTION save_wex_response(
    requestreference character varying,
    quoteheaderid character varying,
    signature character varying,
    success character varying,
    resultcode character varying,
    message character varying,
    pan character varying,
    cv2 character varying,
    expirydate character varying,
    cardreference character varying)
  RETURNS boolean AS
$BODY$
-- Created/Modified By		||Created/Modified Date 		||Comments
--========================================================================================================
-- Venkat Chikkanti		||17/06/2016				||logging WEX Response
-- 
 
  BEGIN
	
				INSERT INTO wex_response
				(
					 requestreference ,  
					 quoteheaderid,
					 signature ,  
					 success ,  
					 resultcode ,  
					 message ,  
					 pan ,  
					 cv2 ,  
					 expirydate ,  
					 cardreference ,  
					 createddate                         
				)
				VALUES
				(
					requestreference ,  
					quoteheaderid,
					 signature ,  
					 success ,  
					 resultcode ,  
					 message ,  
					 pan ,  
					 cv2 ,  
					 expirydate ,  
					 cardreference ,  
					now()
				);				

				
 RETURN TRUE;
END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
ALTER FUNCTION save_wex_response(character varying, character varying, character varying, character varying, character varying, character varying, character varying, character varying, character varying, character varying)
  OWNER TO chilli;

-- Function: get_wexcard(character varying)

-- DROP FUNCTION get_wexcard(character varying);

CREATE OR REPLACE FUNCTION get_wexcard(IN quoteheader_id character varying)
  RETURNS TABLE(quoteheaderid character varying, requestreference character varying, signature character varying, success character varying, resultcode character varying, message character varying, pan character varying, cv2 character varying, expirydate character varying, cardreference character varying, createddate timestamp without time zone) AS
$BODY$
-- Created/Modified By		||Created/Modified Date 		||Comments
--========================================================================================================
-- Venkateshwarlu     ||06/27/2016				||Get wex card details 
--
DECLARE sql VARCHAR;

BEGIN
	RETURN QUERY

	SELECT wr.quoteheaderid
        ,wr.requestreference
        , wr.signature,
        wr.success,
        wr.resultcode,
        wr.message,
        wr.pan,
        wr.cv2,
        wr.expirydate,
        wr.cardreference,
        wr.createddate
   FROM  wex_response wr
   where wr.quoteheaderid = quoteheader_id;
END;$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100
  ROWS 1000;
ALTER FUNCTION get_wexcard(character varying)
  OWNER TO chilli;
