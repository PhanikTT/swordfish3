﻿-- Function: getbagscomponentdetails(integer)

-- DROP FUNCTION getbagscomponentdetails(integer);

CREATE OR REPLACE FUNCTION getbagscomponentdetails(IN component_id integer)
  RETURNS TABLE(bagtype character varying, bagdescription character varying, bagcode character varying, quantityavailable integer, quantityrequired integer, buyingprice numeric, sellingprice numeric, price numeric, createdby integer, modifiedby text, bagdirectionoptionid integer, bagdirection character varying,passengerindex int) AS
$BODY$
Begin
				
					RETURN QUERY
					select 					
					
					bag_component_details.bag_type as bagType, 
					bag_component_details.bag_description as bagDescription, 
					bag_component_details.bag_code as bagCode, 
					cast(bag_component_details.qty_available as int) as quantityAvailable , 
					cast(bag_component_details.qty_required as int) as quantityRequired, 
					cast(bag_component_details.buying_price as decimal) as buyingPrice, 
					cast(bag_component_details.selling_price as decimal) as sellingPrice, 
					cast(bag_component_details.price as decimal), 
					cast(bag_component_details.created_by as int) as createdBy, 
					cast(bag_component_details.modified_by  as text) as modifiedBy,
					tsoption.tsoptionid as bagdirectionoptionid,
					tsoption.name as bagdirection,
					bag_component_details.passenger_index as PassengerIndex
					FROM
					public.bag_component_details join tsoption on(bag_component_details.direction_option_id=tsoption.tsoptionid)
					where bag_component_details.bag_component_id=cast(component_id as int) 
					order by bag_component_id ;
				
end;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100
  ROWS 1000;
ALTER FUNCTION getbagscomponentdetails(integer)
  OWNER TO chilli;
