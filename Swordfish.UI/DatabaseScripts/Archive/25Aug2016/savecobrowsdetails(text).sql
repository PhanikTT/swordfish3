﻿-- Function: savecobrowsdetails(text)

-- DROP FUNCTION savecobrowsdetails(text);

CREATE OR REPLACE FUNCTION savecobrowsdetails(inputparam text)
  RETURNS integer AS
$BODY$ DECLARE SaveCobrows_Data JSON;
DECLARE RESULT INT;
BEGIN
    -- Created/Modified BY		||Created/Modified date 		||Comments
    --========================================================================================================
    -- Nagendra BAbu		||10/06/2016				||Restrucuring DB OBJECT
    -- 
    result:= 0;
IF length (
    inputparam )
<> 0 THEN raise notice 'Converting input param as JSON object';
SELECT (
        inputParam ) ::json INTO SaveCobrows_Data;
RAISE NOTICE 'Save cobrowse deails started';
IF (
    SELECT
        EXISTS (
            SELECT
                1
            FROM
                cobrowsing
            WHERE
                quote_header_id = CAST (
                    SaveCobrows_Data ->> 'QuoteHeaderID' AS INT ) ) )
THEN UPDATE cobrowsing
SET
    status_option_id = (
        SELECT
            tsoptionid
        FROM
            tsoption tso
        INNER JOIN tsvalue tv ON tso.tsvalueid = tv.tsvalueid
        WHERE
            tv.name LIKE '%Cobrows Status%'
            AND tso.name LIKE '%Expired%' )
WHERE
    quote_header_id = CAST (
        SaveCobrows_Data ->> 'QuoteHeaderID' AS INT );
END IF;
INSERT INTO cobrowsing (
    guid,
    quote_header_id,
    email,
    validity_time,
    status_option_id,
    createdby,
    createddate )
VALUES (
    SaveCobrows_Data ->> 'GuId',
    CAST (
        SaveCobrows_Data ->> 'QuoteHeaderID' AS INT ),
    SaveCobrows_Data ->> 'Email',
    CAST (
        SaveCobrows_Data ->> 'ValidityTime' AS INT ),
    (
        SELECT
            tsoptionid
        FROM
            tsoption tso
        INNER JOIN tsvalue tv ON tso.tsvalueid = tv.tsvalueid
        WHERE
            tv.name LIKE '%Cobrows Status%'
            AND tso.name LIKE '%New%' ),
    CAST (
        SaveCobrows_Data ->> 'CreatedBy' AS INT ),
    CAST(SaveCobrows_Data ->> 'CreatedDateTime' AS timestamp) );
result:= 1;
END IF;
RAISE NOTICE 'Save cobrowse deails ended';
RETURN RESULT AS RESULT;
END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
ALTER FUNCTION savecobrowsdetails(text)
  OWNER TO chilli;
