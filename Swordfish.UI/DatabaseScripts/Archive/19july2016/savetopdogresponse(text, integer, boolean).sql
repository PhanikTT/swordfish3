﻿-- Function: savetopdogresponse(text, integer, boolean)

-- DROP FUNCTION savetopdogresponse(text, integer, boolean);


CREATE OR REPLACE FUNCTION savetopdogresponse(
    topdogresponse text,
    quoteheaderid integer,
    userid integer)
  RETURNS SETOF json AS
$BODY$
--Declare Statements
DECLARE errors json;
DECLARE topdog_response json;
DECLARE topdog_response_element json;

--End of declare statements
--Begin statement
BEGIN
	if(topdogresponse not similar TO null or topdogresponse not similar TO '')
	then 
	SELECT  (topdogresponse)::json INTO topdog_response;
		FOR topdog_response_element IN select * from json_array_elements((topdog_response)::json) 
		LOOP IF (select count(*) from json_each(topdog_response_element))>0
		THEN 
			if(topdog_response_element->'IsError' not similar to null)
			then
			SELECT  (topdog_response_element->'IsError') INTO errors;
			end if;	
			
			insert into topdogResponse
			(
			Quote_Header_id,
			Verb_name,
			verb_response,
			errors,
			created_by,
			created_on
			
			)
			values
			(
			cast(quoteheaderid as int),
			topdog_response_element->>'VerbName',
			topdog_response_element->>'VerbResponse',
			errors,
			userid,
			now()
			);
		End if;
		End LOOP;
	end if;

END;

--ENd statement
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100
  ROWS 1000;
ALTER FUNCTION savetopdogresponse(text, integer, int)
  OWNER TO chilli;
