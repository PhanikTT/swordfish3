﻿-- Table: easy_jet_bookings

-- DROP TABLE easy_jet_bookings;

CREATE TABLE easy_jet_bookings
(
  easy_jet_booking_id serial NOT NULL,
  componenttypeoptionid integer,
  quoteheaderid character varying(100),
  currencycode character varying(100),
  pricetype character varying(100),
  supplierref character varying(100),
  totalpriceonconfirmstage money,
  cardfeeonconfirmstage money,
  confirmationpage character varying,
  unexpectedcnfpage character varying,
  createddate timestamp without time zone,
  CONSTRAINT easy_jet_bookings_pkey PRIMARY KEY (easy_jet_booking_id)
)
WITH (
  OIDS=FALSE
);
ALTER TABLE easy_jet_bookings
  OWNER TO chilli;

  -- Function: save_easy_jet_bookings(integer, character varying, character varying, character varying, character varying, money, money, character varying, character varying)

-- DROP FUNCTION save_easy_jet_bookings(integer, character varying, character varying, character varying, character varying, money, money, character varying, character varying);

CREATE OR REPLACE FUNCTION save_easy_jet_bookings(
    pcomponenttypeoptionid integer,
    quoteheader_id character varying,
    currency_code character varying,
    price_type character varying,
    supplier_ref character varying,
    total_priceonconfirmstage money,
    cardfee_onconfirmstage money,
    confirmation_page character varying,
    unexpectedcnf_page character varying)
  RETURNS bit AS
$BODY$
-- Created/Modified By		||Created/Modified Date 		||Comments
--========================================================================================================
-- Nagendra Babu		||07/04/2016				||Insert easy jet booking
-- 

BEGIN
insert into easy_jet_bookings
	(
	componentTypeOptionId,
	quoteHeaderId,
	currencyCode,
	priceType,
	supplierRef ,
	totalPriceOnConfirmStage ,
	cardFeeOnConfirmStage ,
	confirmationPage ,
	unexpectedCnfPage,
	createddate )
values(
	pcomponentTypeOptionId,
	quoteHeader_Id,
	currency_Code,
	price_Type,
	supplier_Ref,
	total_PriceOnConfirmStage,
	cardFee_OnConfirmStage,
	confirmation_Page,
	unexpectedCnf_Page,
	now()
	);

RETURN true;
END;$BODY$
  LANGUAGE plpgsql STABLE
  COST 100;
ALTER FUNCTION save_easy_jet_bookings(integer, character varying, character varying, character varying, character varying, money, money, character varying, character varying)
  OWNER TO chilli;


  -- Function: get_easyjet_booking(character varying, integer)

-- DROP FUNCTION get_easyjet_booking(character varying, integer);

CREATE OR REPLACE FUNCTION get_easyjet_booking(
    IN quoteheader_id character varying,
    IN pcomponenttypeoptionid integer)
  RETURNS TABLE(componenttypeoptionid integer, quoteheaderid character varying, currencycode character varying, pricetype character varying, supplierref character varying, totalpriceonconfirmstage money, cardfeeonconfirmstage money, confirmationpage character varying, unexpectedcnfpage character varying, createddate timestamp without time zone) AS
$BODY$
-- Created/Modified By		||Created/Modified Date 		||Comments
--========================================================================================================
-- Venkateshwarlu     ||06/27/2016				||Get Easy Jet Booking details 
--
DECLARE sql VARCHAR;

BEGIN
	RETURN QUERY

	SELECT ejb.componentTypeOptionId
        ,ejb.quoteHeaderId
        , ejb.currencyCode,
        ejb.priceType,
        ejb.supplierRef,
        ejb.totalPriceOnConfirmStage,
        ejb.cardFeeOnConfirmStage,
        ejb.confirmationPage,
        ejb.unexpectedCnfPage,
        ejb.createddate
   FROM  easy_jet_bookings ejb
   where ejb.quoteheaderid = quoteheader_id and ejb.componentTypeOptionId = pcomponentTypeOptionId;
END;$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100
  ROWS 1000;
ALTER FUNCTION get_easyjet_booking(character varying, integer)
  OWNER TO chilli;
