-- Function: get_componentsdetails(integer, text, integer)

-- DROP FUNCTION get_componentsdetails(integer, text, integer);

CREATE OR REPLACE FUNCTION get_componentsdetails(
    IN quoteid integer,
    IN supplier_b_id text,
    IN type_id integer)
  RETURNS TABLE(component_id integer, supplier_basket_id character varying, supplier_component_id character varying, product_type_code character varying, supplier_code character varying, custom_field_1 character varying, custom_field_2 character varying, custom_field_3 character varying, custom_field_4 character varying, status_code text, supplier_booking_id character varying, sesame_basket_type_option_id integer, component_detail_info json, component_option_info json, flights_details json, accommodation_details json) AS
$BODY$
DECLARE componentsql CHARACTER VARYING;
DECLARE searchsql CHARACTER VARYING;


BEGIN
-- Created/Modified By		||Created/Modified Date 		||Comments
--========================================================================================================
-- Nagendra BAbu		||29/06/2016				||Restrucuring DB Object
-- 

searchsql :=  'where co.quote_header_id = ' || quoteid  || ' and co.sesame_basket_type_option_id = ' || type_id;
IF(supplier_b_id <>'')Then
searchsql :=searchsql || ' and co.supplier_basket_id =''' || supplier_b_id || '''';
End IF;
raise notice '%',searchsql;
componentsql:='select co.component_id, co.supplier_basket_id, co.supplier_component_id, co.product_type_code, co.supplier_code, co.custom_field_1, co.custom_field_2, co.custom_field_3, co.custom_field_4, co.status_code, co.supplier_booking_id, co.sesame_basket_type_option_id, 
(
	select array_to_json(array_agg(row_to_json(a))) from
	(
		select  cd.component_detail_type as Type, cd.supplier_description as Description, cd.supplier_currency as SourceCurrency, cd.cost_to_supplier_pence as SourcePricePence, cd.current_cost_to_customer_pence as SellingPricePence, cd.quantity_covered as Quantity
		from component_details cd where cd.component_id  = co.component_id
	)a
) as component_detail_info,
(
	select array_to_json(array_agg(row_to_json(b))) from
	(
		select  cp.option_description as Description, cp.supplier_option_code as Code, cp.option_type as Type, cp.quantity_available as QuantityAvailable, cp.quantity_required as QuantityRequired, cp.buying_price_pence as BuyingPricePence, cp.selling_price_pence as SellingPricePence
		from component_options cp where cp.component_id  = co.component_id
	)b
) as component_option_info,

(
	select array_to_json(array_agg(row_to_json(c))) from
	(
		select fl.flight_id, fl.from_airport_code as FromAirportCode, fl.to_airport_code as ToAirportCode, fl.departure_ts as DepartureDateTime, fl.arrival_ts as ArrivalDateTime,
		(
			select array_to_json(array_agg(row_to_json(d))) from
			(
				select  flg.from_airport_code as FromAirportCode, flg.to_airport_code as ToAirportCode, flg.departure_ts as DepartureDateTime, flg.arrival_ts as ArrivalDateTime, flg.flight_number as FlightNumber, flg.flight_operator as MarketingAirline
				from flight_legs flg where flg.flight_id = fl.flight_id 
			)d
		) as flight_legs
		from flights fl where fl.component_id = co.component_id
	)c
)as flights_details,
(
	select array_to_json(array_agg(row_to_json(e))) from
	(
		select  ac.board_description as Description, ac.board_code as Code, ac.special_information as BoardCode
		from accommodations ac where ac.component_id = co.component_id
	)e
)as accommodation_details
from components co '
|| searchsql ;

RETURN query EXECUTE componentsql;
END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100
  ROWS 1000;
ALTER FUNCTION get_componentsdetails(integer, text, integer)
  OWNER TO chilli;
