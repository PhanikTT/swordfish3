﻿-- Function: getpassengers_info_for_quote(text)

-- DROP FUNCTION getpassengers_info_for_quote(text);

CREATE OR REPLACE FUNCTION getpassengers_info_for_quote(IN quoteid text)
  RETURNS TABLE(quotereference character varying, title character varying, firstname character varying, lastname character varying, middlename text,
   isleadpassenger boolean, countsas character varying, address1 text, address2 text, address3 text, email character varying, mobile character varying, 
   telephone character varying, postcode character varying, passportnumber character varying, nationality text, gender character, dependent character varying, revisionid integer, dob text, quoteheaderid integer,passengertypeoptionid int, passengertype character varying, createdby integer, createddate timestamp without time zone, isemailsubscribed boolean, issmssubscribed boolean) AS
$BODY$
BEGIN
-- Created/Modified By		||Created/Modified Date 		||Comments
--========================================================================================================
-- Nagendra BAbu		||10/06/2016				||Restrucuring DB Object
-- 
if(QuoteId <> '')
THEN
RETURN QUERY



SELECT 
	  p.quote_reference as quotereference, 
	  p.title as title, 
	  p.first_name as first_name, 
	  p.last_name as lastname, 
	  p.middle_name as middlename, 
	  p.is_lead_passenger as isleadpassenger, 
	  p.counts_as as countsas, 
	  p.address1, 
	  p.address2, 
	  p.address3, 
	  p.email, 
	  p.mobile, 
	  p.telephone, 
	  p.postcode, 
	  p.passport_number, 
	  p.nationality, 
	  p.gender, 
	  p.dependent, 
	  p.revision_id as revisionid, 
	  to_char(p.dob, 'DD/MM/YYYY') as dob,	
	  p.quote_header_id as quoteheaderid, 
	  tso.tsoptionid as passengertypeoptionid,
	  tso.name as passengertype, 
	  p.created_by as createdby, 
	  p.created_date as createddate, 
	  p.is_email_subscribed as isemailsubscribed, 
	  p.is_sms_subscribed as issmssubscribed
	FROM 
	  passengers p 	  
	  join tsoption tso on p.passenger_type_option_id=tso.tsoptionid 
	  join tsvalue tsv on tso.tsvalueid = tsv.tsvalueid

where lower(p.quote_reference)=LOWER(quoteid) OR lower(cast(p.quote_header_id as text))=LOWER(quoteid);
 
	
END IF;
END
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100
  ROWS 1000;
ALTER FUNCTION getpassengers_info_for_quote(text)
  OWNER TO chilli;


  select * from getpassengers_info_for_quote('913')
