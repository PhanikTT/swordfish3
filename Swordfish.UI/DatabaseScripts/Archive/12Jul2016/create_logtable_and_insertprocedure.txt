CREATE TABLE sf_log
(
  log_Id serial NOT NULL,
  log_type character varying(255) NOT NULL,
  log_datetime timestamp without time zone NOT NULL,
  user_id int NOT NULL,
  user_session character varying(255) NOT NULL,
  environment character varying(255) NOT NULL,
  thread character varying(255) NOT NULL,
  function_name character varying(255) NOT NULL,
  message character varying(1000) NOT NULL,
  payload character varying(255) NOT NULL,
  stack_trace text NOT NULL,
  created_by integer,
  created_on timestamp without time zone,
  modified_by integer,
  modified_on timestamp without time zone,
  CONSTRAINT sf_log_pkey PRIMARY KEY (log_Id)
)
WITH (
  OIDS=FALSE
);
ALTER TABLE sf_log
  OWNER TO chilli;


  CREATE OR REPLACE FUNCTION save_sf_log(
    log_type text,
    user_id integer,
    user_session text,
    environment text,
    thread text,
    function_name text,
    message text,
    payload text,
    stack_trace text,
    created_by integer
    )
  RETURNS boolean AS
$BODY$

-- Created/Modified By		||Created/Modified Date 		||Comments
--========================================================================================================
-- Thamma Reddy		||12/07/2016				||Restrucuring DB Object
-- 

BEGIN
	INSERT INTO sf_log (log_type,log_datetime,user_id,user_session,environment,thread,function_name,message,payload,stack_trace,created_by,created_on)
	VALUES (log_type,now(),user_id,user_session,environment,thread,function_name,message,payload,stack_trace,created_by,now());

	RETURN TRUE;
END;

$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
ALTER FUNCTION save_sf_log(text, integer, text, text,text,text,text,text,text,integer)
  OWNER TO chilli;
