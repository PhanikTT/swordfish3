-- Function: get_customer_quotes(integer)

-- DROP FUNCTION get_customer_quotes(integer);

CREATE OR REPLACE FUNCTION get_customer_quotes(IN customerid integer)
  RETURNS TABLE(quoteref character varying, totalprice money, priceperperson money, quoteddate timestamp without time zone, source character varying, destination character varying, formdate date, todate date, hotelname character varying, numberofadults integer, numberofchilds integer, numberofinfants integer) AS
$BODY$
BEGIN
RETURN QUERY
select   qh.customer_reference_code as QuoteRef
	,qh.grand_total as TotalPrice
	,qh.price_per_person as PriceperPerson
	,qh.created_on as QuotedDate
	,sm.source_code as Source
 	,sm.destination_code as Destination
 	,sm.start_date as FormDate
 	,sm.end_date as ToDate
 	,hc.hotel_name as HotelName
 	,oc.adults_count as NumberOfAdults
 	,oc.children_count as NumberOfChilds
	,oc.infants_count as NumberOfInfants
	
from quote_header qh
 inner join search_model sm on qh.search_model_id = sm.search_model_id
 inner join quote_details qd on qh.quote_header_id = qd.quote_header_id
 inner join hotel_component hc on qd.component_id = hc.hotel_component_id
 inner join occupancy oc on sm.search_model_id =oc.search_model_id  
 

where qh.customer_id=customerId  order by qh.created_on desc;

END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100
  ROWS 1000;
ALTER FUNCTION get_customer_quotes(integer)
  OWNER TO root;
