DROP TABLE quote_details; 
DROP TABLE bag_component_details;
DROP TABLE bag_component;
DROP TABLE flight_component_details;
DROP TABLE flight_component;
 DROP TABLE hotel_component_details;
DROP TABLE hotel_component; 
 DROP TABLE transfer_component;
DROP TABLE quote_header;
drop table occupancy;
drop table search_model;
drop table tsoption;
drop TABLE tsvalue;
CREATE TABLE search_model
(
  search_model_id serial NOT NULL,
  ttss_quote_id character varying(255),
  sf_quote_id character varying(255),
  source_name character varying(255) NOT NULL,
  source_code character varying(255) NOT NULL,
  destination_name character varying(25) NOT NULL,
  destination_code character varying(25) NOT NULL,
  start_date date NOT NULL,
  end_date date NOT NULL,
  hotel_name character varying(255),
  star_rating real,
  board_type_ai boolean NOT NULL,
  board_type_fb boolean NOT NULL,
  board_type_hb boolean NOT NULL,
  board_type_bb boolean NOT NULL,
  board_type_sc boolean NOT NULL,
  board_type_ro boolean NOT NULL,
  created_by integer NOT NULL,
  created_on date NOT NULL,
  CONSTRAINT search_model_pkey PRIMARY KEY (search_model_id)
)
WITH (
  OIDS=FALSE
);
ALTER TABLE search_model
  OWNER TO chilli;
  
   CREATE TABLE occupancy
(
  occupancy_id serial NOT NULL,
  room_no integer NOT NULL,
  search_model_id integer NOT NULL,
  adults_count integer NOT NULL,
  children_count integer,
  infants_count integer,
  created_by integer NOT NULL,
  created_on date NOT NULL,
  CONSTRAINT occupancy_pkey PRIMARY KEY (occupancy_id)
)
WITH (
  OIDS=FALSE
);
ALTER TABLE occupancy
  OWNER TO chilli;

CREATE TABLE tsoption
(
  tsoptionid serial NOT NULL,
  tsvalueid integer,
  name character varying(255),
  description character varying(255),
  createdby character varying(50),
  createddatetime date,
  modifiedby character varying(50),
  modifieddatetime date,
  CONSTRAINT tsoption_pkey PRIMARY KEY (tsoptionid)
)
WITH (
  OIDS=FALSE
);
ALTER TABLE tsoption
  OWNER TO chilli;

  
  
  CREATE TABLE tsvalue
(
  tsvalueid serial NOT NULL,
  name character varying(255),
  description character varying(255),
  createdby character varying(50),
  createddatetime date,
  modifiedby character varying(50),
  modifieddatetime date,
  CONSTRAINT tsvalue_pkey PRIMARY KEY (tsvalueid)
)
WITH (
  OIDS=FALSE
);
ALTER TABLE tsvalue
  OWNER TO chilli;
  
  
  -- Table: quote_header

-- DROP TABLE quote_header;

CREATE TABLE quote_header
(
  basket_header_id serial NOT NULL,
  search_model_id integer NOT NULL,
  customer_reference_code character varying(255) NOT NULL,
  agent_id integer,
  customer_id integer,
  quote_type_option_id integer,
  quote_status_option_id integer,
  hotel_cost money NOT NULL,
  baggage_cost money,
  transfers_cost money NOT NULL,
  fidelity_cost money,
  total_cost money NOT NULL,
  margin_hotel money,
  margin_flight money,
  margin_extra money,
  grand_total money NOT NULL,
  price_per_person money,
  internal_notes character varying(5000),
  sent_to_email character varying(255),
  sent_to_sms character varying(10),
  created_by integer NOT NULL,
  created_on date NOT NULL,
  modified_by integer NOT NULL,
  modified_on date NOT NULL,
  flight_cost money,
  CONSTRAINT quote_header_pkey PRIMARY KEY (basket_header_id)
)
WITH (
  OIDS=FALSE
);
ALTER TABLE quote_header
  OWNER TO chilli;
  
  
  -- Table: quote_details

-- DROP TABLE quote_details;

CREATE TABLE quote_details
(
  basket_detail_id serial NOT NULL,
  basket_header_id integer NOT NULL,
  component_id integer NOT NULL,
  component_type_option_id integer NOT NULL,
  created_by integer NOT NULL,
  created_on date NOT NULL,
  modified_by integer NOT NULL,
  modified_on date NOT NULL,
  CONSTRAINT quote_details_pkey PRIMARY KEY (basket_detail_id)
)
WITH (
  OIDS=FALSE
);
ALTER TABLE quote_details
  OWNER TO chilli;

  
  
  -- Table: bag_component

-- DROP TABLE bag_component;

CREATE TABLE bag_component
(
  bag_component_id serial NOT NULL,
  created_by integer NOT NULL,
  created_on date NOT NULL DEFAULT ('now'::text)::date,
  modified_by integer NOT NULL,
  modified_on date NOT NULL DEFAULT ('now'::text)::date,
  CONSTRAINT bag_component_pkey PRIMARY KEY (bag_component_id)
)
WITH (
  OIDS=FALSE
);
ALTER TABLE bag_component
  OWNER TO chilli;
  
  
  -- Table: bag_component_details

-- DROP TABLE bag_component_details;

CREATE TABLE bag_component_details
(
  bag_component_details_id serial NOT NULL ,
  bag_type character varying(255) NOT NULL,
  bag_description character varying(255) NOT NULL,
  bag_code character varying(255),
  qty_available integer,
  qty_required integer,
  buying_price money,
  selling_price money,
  price money NOT NULL,
  created_by integer NOT NULL,
  created_on date NOT NULL,
  modified_by integer NOT NULL,
  modified_on date NOT NULL,
  bag_component_id integer,
  CONSTRAINT bags_pkey PRIMARY KEY (bag_component_details_id)
)
WITH (
  OIDS=FALSE
);
ALTER TABLE bag_component_details
  OWNER TO chilli;
  
  
  
  -- Table: flight_component

-- DROP TABLE flight_component;

CREATE TABLE flight_component
(
  flight_component_id serial NOT NULL,
  amount money NOT NULL,
  currency_code character varying(25) NOT NULL,
  created_by integer NOT NULL,
  created_on date NOT NULL,
  modified_by integer NOT NULL,
  modified_on date NOT NULL,
  flight_quote_id character varying(255),
  sourcefrom character varying(30),
  arrivalto character varying(30),
  CONSTRAINT flight_component_pkey PRIMARY KEY (flight_component_id)
)
WITH (
  OIDS=FALSE
);
ALTER TABLE flight_component
  OWNER TO chilli;
  
  
  -- Table: flight_component_details

-- DROP TABLE flight_component_details;

CREATE TABLE flight_component_details
(
  flight_component_details_id serial NOT NULL,
  flight_component_id integer NOT NULL,
  departure_datetime date NOT NULL,
  arrival_datetime date NOT NULL,
  flight_number integer NOT NULL,
  journey_indicator integer NOT NULL,
  departure_airport_code character varying(25) NOT NULL,
  arrival_airport_code character varying(25) NOT NULL,
  marketing_airline character varying(255) NOT NULL,
  created_by integer NOT NULL,
  created_on date NOT NULL,
  modified_by integer,
  modified_on date,
  direction character varying(30),
  CONSTRAINT flight_component_details_pkey PRIMARY KEY (flight_component_details_id)
)
WITH (
  OIDS=FALSE
);
ALTER TABLE flight_component_details
  OWNER TO chilli;
  
  
  -- Table: hotel_component

-- DROP TABLE hotel_component;

CREATE TABLE hotel_component
(
  hotel_component_id serial NOT NULL,
  hotel_name character varying(255) NOT NULL,
  hotel_code character varying(25) NOT NULL,
  hotel_code_context character varying(25) NOT NULL,
  supplier_resort_id character varying(25) NOT NULL,
  tti_code integer,
  hotel_type_id integer NOT NULL,
  hotel_quote_id character varying(30) NOT NULL,
  created_by integer NOT NULL,
  created_on date NOT NULL,
  modified_by integer NOT NULL,
  modified_on date NOT NULL,
  CONSTRAINT hotel_component_pkey PRIMARY KEY (hotel_component_id)
)
WITH (
  OIDS=FALSE
);
ALTER TABLE hotel_component
  OWNER TO chilli;
  
  
  -- Table: hotel_component_details

-- DROP TABLE hotel_component_details;

CREATE TABLE hotel_component_details
(
  hotel_component_detail_id serial NOT NULL,
  hotel_component_id integer NOT NULL,
  room_type_code character varying(25) NOT NULL,
  rate_plan_code character varying(25) NOT NULL,
  rate_plan_name character varying(25) NOT NULL,
  rate_quote_id character varying(30) NOT NULL,
  amount_after_tax money NOT NULL,
  created_by integer NOT NULL,
  created_on date NOT NULL,
  modified_by integer NOT NULL,
  modified_on date NOT NULL,
  room_description character varying(499) NOT NULL,
  CONSTRAINT hotel_component_details_pkey PRIMARY KEY (hotel_component_detail_id)
)
WITH (
  OIDS=FALSE
);
ALTER TABLE hotel_component_details
  OWNER TO chilli;
  
  
  
  -- Table: transfer_component

-- DROP TABLE transfer_component;

CREATE TABLE transfer_component
(
  transfer_component_id serial NOT NULL,
  product_id character varying(255) NOT NULL,
  bookingtype_id character varying(25) NOT NULL,
  transfer_time integer NOT NULL,
  producttype_id character varying(25) NOT NULL,
  product_type character varying(100),
  minpax integer,
  maxpax integer,
  luggage integer,
  perperson character varying(25),
  product_description character varying(5000) NOT NULL,
  oldprice money,
  total_price money NOT NULL,
  currency character varying(25),
  pricetype character varying(25),
  typecode character varying(25) NOT NULL,
  price_description character varying(255) NOT NULL,
  units integer,
  price_perperson_oldprice money,
  created_by integer NOT NULL,
  created_on date NOT NULL,
  modified_by integer NOT NULL,
  modified_on date NOT NULL,
  CONSTRAINT transfer_component_pkey PRIMARY KEY (transfer_component_id)
)
WITH (
  OIDS=FALSE
);
ALTER TABLE transfer_component
  OWNER TO chilli;
  
  
  
  