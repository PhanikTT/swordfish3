﻿-- Function: save_cobrows_response(integer, text, text)

-- DROP FUNCTION save_cobrows_response(integer, text, text);

CREATE OR REPLACE FUNCTION save_cobrows_response(
    quote_id integer,
    gu_id text,
    status text)
  RETURNS integer AS
$BODY$

-- Created/Modified By		||Created/Modified Date 		||Comments
--========================================================================================================
-- Nagendra BAbu		||10/06/2016				||Restrucuring DB Object
-- 


DECLARE result int;
BEGIN	
	result:=0;
IF((select 1 from cobrowsing a where a.quote_header_id = quote_id and a.guid = guid order by 1 desc limit 1 ) =1) Then
	
		update cobrowsing set status_option_id = (select  tsoptionid from tsoption where name = status), modifieddate = now()
		where quote_header_id = quote_id and guid = gu_id;
	result:=1;
END IF;
	RETURN result;
END;

$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
ALTER FUNCTION save_cobrows_response(integer, text, text)
  OWNER TO chilli;
