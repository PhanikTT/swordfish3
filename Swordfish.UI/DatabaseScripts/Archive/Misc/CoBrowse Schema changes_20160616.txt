insert into tsvalue (name, description,createdby, createddatetime,modifiedby, modifieddatetime)
select 'Passenger Type','Passenger Type',999,now(),999,now()




insert into tsoption (tsvalueid, name, description,createdby, createddatetime,modifiedby, modifieddatetime)
select (select tsvalueid from tsvalue where name like '%Passenger Type%'), 'Adult','Adult',999,now(),999,now()

insert into tsoption (tsvalueid, name, description,createdby, createddatetime,modifiedby, modifieddatetime)
select	(select tsvalueid from tsvalue where name like '%Passenger Type%'),'Child','Child',999,now(),999,now()

insert into tsoption (tsvalueid, name, description,createdby, createddatetime,modifiedby, modifieddatetime)
select	(select tsvalueid from tsvalue where name like '%Passenger Type%'), 'Infant','Infant',999,now(),999,now()



alter table passengers add  quote_header_id int ;
alter table passengers add passenger_type_option_id int;
alter table passengers add is_email_subscribed boolean;
alter table passengers add is_sms_subscribed bit;
alter table passengers add created_by int;
alter table passengers add created_date timestamp;
alter table passengers add modify_by int;
alter table passengers add modify_date timestamp;
alter table passengers add  seq_no int ;

alter table passengers add CONSTRAINT passendgers_quote_header_id_FK foreign key (quote_header_id) references quote_header(quote_header_id)

alter table passengers add CONSTRAINT passengers_tsoptionid_FK foreign key (passenger_type_option_id) references tsoption(tsoptionid)







--====================================================Cobrows Changes=========================================================

create table cobrowsing(cobrows_id serial primary key ,
		guid varchar(200),
		quote_header_id int,
		email varchar(100),
		validity_time int,
		status_option_id int,
		createdby int,
		createddate timestamp default now(),
		modifiedby int,
		modifieddate timestamp
		
		)


alter table cobrowsing add CONSTRAINT quote_header_id_fk  foreign key (quote_header_id) references quote_header(quote_header_id);


alter table cobrowsing add CONSTRAINT status_option_id_fk foreign key (status_option_id) references tsoption(tsoptionid)

insert into tsvalue(name, description, createdby, createddatetime,modifiedby, modifieddatetime)
	select 'Cobrows Status','Cobrows Status', 999,now(),999,now()
	
	
insert into tsoption(tsvalueid, name, description, createdby, createddatetime,modifiedby, modifieddatetime)
select (select tsvalueid from tsvalue where name like '%Cobrows Status%'),'New','New cobrows',999, now(), 999, now()

insert into tsoption(tsvalueid, name, description, createdby, createddatetime,modifiedby, modifieddatetime)
select (select tsvalueid from tsvalue where name like '%Cobrows Status%'),'Approved','Approved cobrows',999, now(), 999, now()

insert into tsoption(tsvalueid, name, description, createdby, createddatetime,modifiedby, modifieddatetime)
select (select tsvalueid from tsvalue where name like '%Cobrows Status%'),'Rejected','Rejected cobrows',999, now(), 999, now()

insert into tsoption(tsvalueid, name, description, createdby, createddatetime,modifiedby, modifieddatetime)
select (select tsvalueid from tsvalue where name like '%Cobrows Status%'),'Expired','Expired cobrows',999, now(), 999, now()
	