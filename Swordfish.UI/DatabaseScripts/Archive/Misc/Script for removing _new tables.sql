﻿--Scripts for ading new columns in basket.basket_new Table
ALTER TABLE basket.baskets_new ADD COLUMN   client_ip_address character varying(100)
ALTER TABLE basket.baskets_new ADD COLUMN    wfe_version character varying(50)
ALTER TABLE basket.baskets_new ADD COLUMN     site_code text
ALTER TABLE basket.baskets_new ADD COLUMN   booking_id integer
ALTER TABLE basket.baskets_new ADD COLUMN  schema_version character varying DEFAULT 3.0 
ALTER TABLE basket.baskets_new ADD COLUMN    is_test_booking boolean DEFAULT false

--End of adding new columns in basket.basket_new Table


--Scripts for ading new columns in basket.accommodation_rooms_new Table

ALTER TABLE basket.accommodation_rooms_new ADD COLUMN    num_infants smallint

--End of adding new columns in basket.accommodation_rooms_new Table

--Scripts for ading new columns in basket.components_new Table

ALTER TABLE basket.components_new ADD COLUMN       revision_id integer 
ALTER TABLE basket.components_new ADD COLUMN       supplier_session_id character varying(100)
ALTER TABLE basket.components_new ADD COLUMN         status_code text
ALTER TABLE basket.components_new ADD COLUMN       api_recorded boolean DEFAULT false
ALTER TABLE basket.components_new ADD COLUMN        custom_field_1 character varying
ALTER TABLE basket.components_new ADD COLUMN        custom_field_2 character varying
ALTER TABLE basket.components_new ADD COLUMN       custom_field_3 character varying
ALTER TABLE basket.components_new ADD COLUMN       custom_field_4 character varying
ALTER TABLE basket.components_new ADD COLUMN        custom_field_5 character varying
ALTER TABLE basket.components_new ADD COLUMN        package_set_id character varying
ALTER TABLE basket.components_new ADD COLUMN       supplier_booking_id text
ALTER TABLE basket.components_new ADD COLUMN        componentdescription text
ALTER TABLE basket.components_new ADD COLUMN        non_refundable boolean DEFAULT false
ALTER TABLE basket.components_new ADD COLUMN          remote_supplier_code text
ALTER TABLE basket.components_new ADD COLUMN         messages text
ALTER TABLE basket.components_new ADD COLUMN        remote_supplier_username character varying

--End of adding new columns in basket.components_new Table
--Scripts for ading new columns in basket.component_options_new Table

ALTER TABLE basket.component_options_new ADD COLUMN       supplier_option_code character varying 
ALTER TABLE basket.component_options_new ADD COLUMN        option_description character varying 
ALTER TABLE basket.component_options_new ADD COLUMN        option_type character varying(32)
ALTER TABLE basket.component_options_new ADD COLUMN      operator_code character varying(30)
ALTER TABLE basket.component_options_new ADD COLUMN       applies_to character varying(32) 
ALTER TABLE basket.component_options_new ADD COLUMN         passenger_id integer


--End of adding new columns in basket.component_options_new Table

--Scripts for ading new columns in basket.component_details_new Table

ALTER TABLE basket.component_details_new ADD COLUMN     cost_to_supplier_pence integer
ALTER TABLE basket.component_details_new ADD COLUMN      original_cost_to_customer_pence integer
ALTER TABLE basket.component_details_new ADD COLUMN       current_cost_to_customer_pence integer
ALTER TABLE basket.component_details_new ADD COLUMN     supplier_currency character(3)
ALTER TABLE basket.component_details_new ADD COLUMN      site_markup_pence integer DEFAULT 0
ALTER TABLE basket.component_details_new ADD COLUMN        agency_markup_pence integer  DEFAULT 0
ALTER TABLE basket.component_details_new ADD COLUMN      source_cost_pence integer
ALTER TABLE basket.component_details_new ADD COLUMN        fpc character varying

--End of adding new columns in basket.component_details_new Table

--Scripts for ading new columns in passengers_new Table

ALTER TABLE passengers_new ADD COLUMN revision_id integer 

--End of adding new columns in passengers_new Table

--Scripts for ading new columns in payments_new Table

ALTER TABLE payments_new ADD COLUMN   payment_tel_day character varying(30)
ALTER TABLE payments_new ADD COLUMN     payment_tel_eve character varying(30)
ALTER TABLE payments_new ADD COLUMN     payment_postcode character varying(50) 
ALTER TABLE payments_new ADD COLUMN     card_issue_number character varying(2)

--End of adding new columns in payments_new Table

--Scripts for dropping the unwanted tables
DROP TABLE IF EXISTS basket.accommodation_rooms;
DROP TABLE IF EXISTS basket.accommodations;
DROP TABLE IF EXISTS basket.component_details;
DROP TABLE IF EXISTS basket.component_options;
DROP TABLE IF EXISTS basket.extras;
DROP TABLE IF EXISTS payments;



--These tables cannot be dropped directly as another tables are dependent on these tables
--flight_legs is dependent on flights
ALTER TABLE basket.flight_legs DROP CONSTRAINT basket_flight_legs_flight_id_fkey

DROP TABLE IF EXISTS basket.flights;

--cruises ,supplier_cost_breakdowns tables are dependent on components table
ALTER TABLE basket.cruises DROP CONSTRAINT cruises_component_id_fkey
ALTER TABLE basket.supplier_cost_breakdowns DROP CONSTRAINT csupplier_cost_breakdowns_component_id_fkey
DROP view IF EXISTS basket.itinerary_items;

DROP TABLE IF EXISTS basket.components;

--booking_promotions,revisions,booking_markups are dependent on basket table
ALTER TABLE basket.booking_promotions DROP CONSTRAINT booking_promotions_basket_id_fkey
ALTER TABLE basket.revisions DROP CONSTRAINT revisions_basket_id_fkey 
ALTER TABLE basket.booking_markups DROP CONSTRAINT booking_markups_basket_id_fkey 

DROP TABLE IF EXISTS basket.baskets;


--supplier_passengers is dependent on passengers table
ALTER TABLE supplier_passengers DROP CONSTRAINT supplier_passengers_passenger_id_fkey 


DROP TABLE IF EXISTS passengers;

--End of dropping the tables

--Renaming the tables suffixed with _new by removing _new from it

ALTER TABLE basket.accommodation_rooms_new RENAME TO accommodation_rooms
ALTER TABLE basket.accommodations_new RENAME TO accommodations
ALTER TABLE basket.component_details_new RENAME TO component_details
ALTER TABLE basket.component_options_new RENAME TO component_options
ALTER TABLE basket.extras_new RENAME TO extras
ALTER TABLE basket.flights_new RENAME TO flights
ALTER TABLE basket.components_new RENAME TO components
ALTER TABLE basket.baskets_new RENAME TO baskets
ALTER TABLE passengers_new RENAME TO passengers
ALTER TABLE payments_new RENAME TO payments

--CREATE VIEW

CREATE OR REPLACE VIEW basket.itinerary_items AS 
 SELECT c.component_id,
    c.revision_id,
    c.supplier_session_id,
    c.supplier_basket_id,
    c.supplier_code,
    c.product_type_code,
    c.start_date,
    c.end_date,
    c.status_code,
    c.api_recorded,
    c.api_required,
    c.custom_field_1,
    c.custom_field_2,
    c.custom_field_3,
    c.custom_field_4,
    c.custom_field_5,
    c.package_set_id,
    c.supplier_booking_id,
    c.remote_supplier_code
   FROM components c
  WHERE c.product_type_code::text = ANY (ARRAY['FLI'::character varying::text, 'ACO'::character varying::text, 'CRU'::character varying::text, 'TCK'::character varying::text, 'TXI'::character varying::text, 'INS'::character varying::text, 'SFC'::character varying::text, 'FCR'::character varying::text, 'PCK'::character varying::text, 'OPT'::character varying::text]);

ALTER TABLE itinerary_items
  OWNER TO root;
GRANT ALL ON TABLE itinerary_items TO root;
GRANT UPDATE, INSERT ON TABLE itinerary_items TO write_bookings;
GRANT SELECT ON TABLE itinerary_items TO read_bookings;
-- DROP FUNCTION f_insert_quote_details(text);

DROP FUNCTION IF EXISTS f_insert_quote_details(text);


--Creating the Function f_insert_quote_details

CREATE OR REPLACE FUNCTION f_insert_quote_details(jsoninput text)
  RETURNS text AS
$BODY$
DECLARE basket_object json;
DECLARE quote_object json;
DECLARE packaged_item json;
DECLARE flight_item json;
DECLARE flight_flying_item json;
DECLARE flight_segment_item json;
DECLARE accomodation_item json;
DECLARE accomodation_room json;
DECLARE extra_item json;
DECLARE quote_notes_item json;
DECLARE basketid int;
DECLARE flight_component_id int;
DECLARE flight_component_option_id int;
DECLARE flight_current_id int;
DECLARE hotel_component_id int;
DECLARE hotel_component_option_id int;
DECLARE extra_component_id int;
DECLARE extra_component_option_id int;
DECLARE hotel_start_date timestamp;
DECLARE hotel_end_date timestamp;
DECLARE days_required int;
DECLARE accommodationid int;
DECLARE QuoteNotes json;
DECLARE AccommodationRooms json;
DECLARE QuoteCount int;
BEGIN
	IF length(jsoninput) <> 0
	THEN
		RAISE NOTICE 'Converting input param as JSON object';
		SELECT (jsoninput)::json into basket_object;
INSERT INTO Chilli_Error_Log(Error_Dtls) VALUES(basket_object->>'BookingItems');
		IF (select count(*) from json_each(basket_object)) > 0
		THEN
			RAISE NOTICE 'Collecting quote details from basket object';
			SELECT (basket_object->>'Quote')::json into quote_object;

			IF (select count(*) from json_each(quote_object)) > 0
			THEN
				RAISE NOTICE 'Inserting basket details';				
				INSERT INTO basket.baskets (currency, cwt_session_id, user_id, consumer_booking_ref, customer_id) VALUES (basket_object->>'Currency',basket_object->>'SessionId',CAST(basket_object->>'UserId' as int), quote_object->>'QuoteRef',CAST(quote_object->>'CustomerId' as int));      
				basketid := (SELECT CURRVAL(pg_get_serial_sequence('basket.baskets','basket_id')));
				RAISE NOTICE 'End of inserting basket details';

				RAISE NOTICE 'Inserting quote details';
				INSERT INTO basket.quote(quote_ref,customer_id,agent_id,status,quotes_status,quote_desc,quote_type) values (quote_object->>'QuoteRef',CAST(quote_object->>'CustomerId' as int),CAST(basket_object->>'UserId' as int),CAST(quote_object->>'QuoteStatus' as bit(1)),'QQ',basket_object->>'BookingItems',basket_object->>'ActionType');
				RAISE NOTICE 'End of inserting quote details';		
				QuoteCount := (Select count(*) from basket.quote where customer_id=CAST(quote_object->>'CustomerId' as int) and DATE_PART('Day',now() - created_on::timestamptz) < 1);		
                                   INSERT INTO Chilli_Error_Log(Error_Dtls) VALUES(basket_object->>'BookingItems');
                                   
                                   
                                   
				RAISE NOTICE 'Collecting Package Information from quote object';
				SELECT (quote_object->>'PackageInformation')::json into packaged_item;

				IF (select count(*) from json_each(packaged_item)) > 0
				THEN				
					SELECT (packaged_item->>'QuoteAdditionalNotes')::json into QuoteNotes;
					IF (json_array_length(QuoteNotes)) > 0
					THEN
						RAISE NOTICE 'Inserting Quote Notes';
						FOR quote_notes_item IN SELECT * FROM json_array_elements((packaged_item->>'QuoteAdditionalNotes')::json)
						    LOOP
							IF (select count(*) from json_each(quote_notes_item)) > 0
							THEN
								INSERT INTO basket.quote_notes(quote_ref,notes) values (quote_notes_item->>'QuoteRef',quote_notes_item->>'Notes');
							ELSE
								RAISE EXCEPTION 'Error saving basket information in quote notes details';
							END IF;						       
						    END LOOP;        
						RAISE NOTICE 'Ending Inserting Quote Notes';	
					ELSE
						RAISE EXCEPTION 'Error saving basket information in quotenotes details';
					END IF;

					RAISE NOTICE 'Collecting flight json object from package information';
					SELECT (packaged_item->>'FlightInfo')::json into flight_item;

					IF (select count(*) from json_each(flight_item)) > 0
					THEN
						-- Flight Object insertion starts here --

						RAISE NOTICE 'Inserting Components from flight info';
						INSERT INTO basket.components (basket_id,supplier_basket_id,supplier_code,product_type_code) values (basketid,flight_item->>'SupplierBasketId',flight_item->>'SupplierCode','flight');
						flight_component_id := (SELECT CURRVAL(pg_get_serial_sequence('basket.components','component_id')));
						RAISE NOTICE 'End of inserting flight component';

						RAISE NOTICE 'Collecting Flight Details from flight information';						
						FOR flight_flying_item IN SELECT * FROM json_array_elements((flight_item->>'FlightDetails')::json)
					        LOOP
							IF (select count(*) from json_each(flight_flying_item)) > 0
							THEN
								RAISE NOTICE 'Inserting Components Options from flight info';
								INSERT INTO basket.component_options(component_id,quantity_required)values(flight_component_id,CAST(flight_flying_item->>'NoOfSeats' as int));
								flight_component_option_id := (SELECT CURRVAL(pg_get_serial_sequence('basket.component_options','component_option_id')));
								RAISE NOTICE 'End of inserting Components Options from flight info';

								RAISE NOTICE 'Inserting Components Details from flight info';
								INSERT INTO basket.component_details(component_id,component_option_id,total_selling_price,quantity_covered)values(flight_component_id,flight_component_option_id,CAST(flight_flying_item->>'TotalSellingPrice' as decimal),CAST(flight_flying_item->>'NoOfSeats' as int));
								RAISE NOTICE 'End of inserting Components Details from flight info';

								RAISE NOTICE 'Inserting flight details';
								INSERT INTO basket.flights (component_id,flight_number,departure_ts,arrival_ts,from_airport_code,to_airport_code,flight_operator,fare_type,errata,component_option_id) values (flight_component_id,flight_flying_item->>'FlightNumber',CAST(flight_flying_item->>'DepartureTime' as timestamp),CAST(flight_flying_item->>'ArrivalTime' as timestamp),flight_flying_item->>'DepartureAirport',flight_flying_item->>'ArrivalAirport',flight_item->>'Operator',flight_item->>'FareType',CAST(flight_item->>'Errata' as bit(1)),flight_component_option_id);
								flight_current_id := (SELECT CURRVAL(pg_get_serial_sequence('basket.flights','flight_id')));

								RAISE NOTICE 'Collecting flight segments from flight info';								
								FOR flight_segment_item IN SELECT * FROM json_array_elements((flight_flying_item->>'FlightSegments')::json)
								LOOP
									IF(select count(*) from json_each(flight_segment_item)) > 0
									THEN
										RAISE NOTICE 'Inserting flight segments from flight info';
										INSERT INTO basket.flights_segments (flight_id, flight_number, departure_ts, arrival_ts, from_airport_code, to_airport_code, flight_operator) values (flight_current_id, flight_segment_item->>'FlightNumber', CAST(flight_segment_item->>'DepartureTime' as timestamp),CAST(flight_segment_item->>'ArrivalTime' as timestamp),flight_segment_item->>'DepartureAirport',flight_segment_item->>'ArrivalAirport',flight_segment_item->>'Operator');
									ELSE
										RAISE EXCEPTION 'Error saving flight information in segment details';
									END IF;
								END LOOP;
								
								RAISE NOTICE 'End of inserting flight details';
							ELSE
								RAISE EXCEPTION 'Error saving flight information in flying details';
							END IF;						       
					        END LOOP;
					        RAISE NOTICE 'End of collecting Flight Details from flight information';	
						-- Flight Object insertion ends here --
					--ELSE
						--RAISE EXCEPTION 'Error saving basket information in flight details';
					END IF;

					RAISE NOTICE 'Collecting accommodation json object from package information';
					SELECT (packaged_item->>'AccommodationInfo')::json into accomodation_item;

					IF (select count(*) from json_each(accomodation_item)) > 0
					THEN
						-- Accommodation Object insertion starts here --
						RAISE NOTICE 'Inserting Components from accommodation info';
						hotel_start_date := CAST(accomodation_item->>'StartDate' as timestamp);
						days_required := CAST(accomodation_item->>'NoOfDaysRequired' as int);	
						hotel_end_date :=  hotel_start_date::date + days_required;
						
						INSERT INTO basket.components (basket_id,supplier_basket_id,supplier_code,product_type_code,start_date,end_date) values (basketid,accomodation_item->>'SupplierBasketId',accomodation_item->>'SupplierCode','Accommodation',CAST(accomodation_item->>'StartDate' as timestamp),(hotel_end_date + hotel_start_date::time));
						hotel_component_id := (SELECT CURRVAL(pg_get_serial_sequence('basket.components','component_id')));
						RAISE NOTICE 'End of inserting accommodation component';		

						RAISE NOTICE 'Inserting Components Options from accommodation info';
						INSERT INTO basket.component_options(component_id,quantity_required)values(hotel_component_id,CAST(accomodation_item->>'NoOfRooms' as int));
						hotel_component_option_id := (SELECT CURRVAL(pg_get_serial_sequence('basket.component_options','component_option_id')));
						RAISE NOTICE 'End of inserting Components Options from accommodation info';

						RAISE NOTICE 'Inserting Components Details from accommodation info';
						INSERT INTO basket.component_details(component_id,component_option_id,total_selling_price,quantity_covered)values(hotel_component_id,hotel_component_option_id,CAST(accomodation_item->>'AmountAfterTax' as decimal),CAST(accomodation_item->>'NoOfRooms' as int));
						RAISE NOTICE 'End of inserting Components Details from accommodation info';

						RAISE NOTICE 'Inserting accommodation details';
						INSERT INTO basket.accommodations (component_id,accomm_name,address,supplier_accomm_id,remote_supplier_code,resort_name,board_code,rating,user_score,num_nights,phone_number,supplier_resort_id,board_description,accommodation_ttii_code) values (hotel_component_id,accomodation_item->>'HotelName',accomodation_item->>'Address',accomodation_item->>'HotelId',accomodation_item->>'SupplierCode',accomodation_item->>'ResortName',accomodation_item->>'BoardCode',CAST(accomodation_item->>'Rating' as numeric),CAST(accomodation_item->>'UserScore' as numeric),CAST(accomodation_item->>'NoOfDaysRequired' as int),accomodation_item->>'PhoneNo',accomodation_item->>'SupplierBasketId',accomodation_item->>'BoardDesc',accomodation_item->>'TTICode');
						accommodationid := (SELECT CURRVAL(pg_get_serial_sequence('basket.accommodations','accommodation_id')));
						RAISE NOTICE 'End of inserting accommodation details';

						SELECT (accomodation_item->>'AccommodationRoomInfo')::json into AccommodationRooms;						
						IF (json_array_length(AccommodationRooms)) > 0
						THEN
							RAISE NOTICE 'Collecting accommodation room json object from accommodation information';
							FOR accomodation_room IN SELECT * FROM json_array_elements((accomodation_item->>'AccommodationRoomInfo')::json)
							    LOOP    
								IF (select count(*) from json_each(accomodation_room)) > 0
								THEN
									INSERT INTO basket.accommodation_rooms(accommodation_id,room_code,description,quantity,num_adults,num_children) values (accommodationid,accomodation_room->>'RoomCode',accomodation_room->>'Description',CAST(accomodation_room->>'Quantity' as int),CAST(accomodation_room->>'NoOfAdult' as int),CAST(accomodation_room->>'NoOfChild' as int));
								ELSE
									RAISE EXCEPTION 'Error saving basket information in accommodation room details';
								END IF;
							    END LOOP;        
							RAISE NOTICE 'Ending Inserting Accommodation Room Details';

						ELSE
							RAISE EXCEPTION 'Error saving basket information in accommodation Rooms';
						END IF;	
						-- Accommodation Object insertion ends here --
					--ELSE
						--RAISE EXCEPTION 'Error saving basket information in accommodation details';
					END IF;

					RAISE NOTICE 'Inserting Extras Details starts here';
					FOR extra_item IN SELECT * FROM json_array_elements((packaged_item->>'ExtraInfo')::json)
					    LOOP
						IF (select count(*) from json_each(extra_item)) > 0
						THEN
							-- Extras Object insertion starts here -- 
							RAISE NOTICE 'Inserting extra object component';
							INSERT INTO basket.components (basket_id,product_type_code) values (basketid,extra_item->>'ProductLabel');
							extra_component_id := (SELECT CURRVAL(pg_get_serial_sequence('basket.components','component_id')));
							RAISE NOTICE 'End of inserting extra object component';
									
							RAISE NOTICE 'Inserting extra object component option';
							INSERT INTO basket.component_options(component_id,quantity_required)values(extra_component_id,CAST(extra_item->>'Quantity' as int));
							extra_component_option_id := (SELECT CURRVAL(pg_get_serial_sequence('basket.component_options','component_option_id')));
							RAISE NOTICE 'End of inserting extra object component option';

							RAISE NOTICE 'Inserting extra object component details';
							INSERT INTO basket.component_details(component_id,component_option_id,quantity_covered,total_selling_price) values (extra_component_id,extra_component_option_id,CAST(extra_item->>'Quanitity' as int),CAST(extra_item->>'Fare' as decimal));
							RAISE NOTICE 'End of inserting extra object component details';

							RAISE NOTICE 'Inserting extra object details';
							INSERT INTO basket.extras(component_id,product_label,quantity,reference_data,reference_description) values (extra_component_id,extra_item->>'ProductLabel',CAST(extra_item->>'Quantity' as int),extra_item->>'ProductReference',extra_item->>'ProductReferenceDescription');
							RAISE NOTICE 'End of inserting extra object details';
							-- Extras Object insertion ends here --	
						ELSE
							RAISE EXCEPTION 'Error saving extras information';
						END IF;						       
					    END LOOP;        
					RAISE NOTICE 'Inserting Extras Details ends here';
				ELSE
					RAISE EXCEPTION 'Error saving basket information in package details';
				END IF;						
			ELSE
				RAISE EXCEPTION 'Error saving basket information in quote details';
			END IF;
			
		ELSE
			RAISE EXCEPTION 'Error saving basket information';
		END IF;
	ELSE
		RAISE EXCEPTION 'Basket information should not be null';
	END IF;	
	
	return CAST(basketid as text)||','|| CAST(QuoteCount as text);        
END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
ALTER FUNCTION f_insert_quote_details(text)
  OWNER TO chilli;


-- DROP FUNCTION get_recent_quotes(character varying);

DROP FUNCTION IF EXISTS get_recent_quotes(character varying);

--Create function get_recent_quotes(character varying);

CREATE OR REPLACE FUNCTION get_recent_quotes(IN searchdata character varying)
  RETURNS TABLE(basket_id integer, quote_ref character varying, first_name character varying, last_name character varying, email_address character varying, created_date timestamp without time zone, booking_date timestamp without time zone, inbound_quantity integer, inbound_flight_fare numeric, inbound_flight_number character varying, inbound_fare_type text, inbound_flight_operator character varying, inbound_departure_time timestamp without time zone, inbound_arrival_time timestamp without time zone, inbound_source character varying, inbound_destination character varying, outbound_quantity integer, outbound_flight_fare numeric, outbound_flight_number character varying, outbound_fare_type text, outbound_flight_operator character varying, outbound_departure_time timestamp without time zone, outbound_arrival_time timestamp without time zone, outbound_source character varying, outbound_destination character varying, accomm_name text, num_nights smallint, address text, rating numeric, user_score numeric, board_code character, hotel_fare numeric, adults_count smallint, children_count smallint, rooms_required smallint, hotel_code character varying, ttii_code character varying, bag_count smallint, hoteldescription text, quote_type character varying, quote_desc text) AS
$BODY$
DECLARE
 sql character varying;
BEGIN	

sql:='select distinct bn.basket_id, q.quote_ref, c.first_name, c.last_name, c.email_address, q.created_on created_date, bn.booking_date booking_date
, fl.inbound_quantity,fl.inbound_flight_fare,fl.inbound_flight_number,fl.inbound_fare_type,fl.inbound_flight_operator
, fl.inbound_departure_time,fl.inbound_arrival_time,fl.inbound_source,fl.inbound_destination
, fl.outbound_quantity,fl.outbound_flight_fare,fl.outbound_flight_number,fl.outbound_fare_type,fl.outbound_flight_operator
, fl.outbound_departure_time,fl.outbound_arrival_time,fl.outbound_source,fl.outbound_destination
, ht.accomm_name,ht.num_nights,ht.address,ht.rating,ht.user_score,ht.board_code,ht.total_selling_price as hotel_fare,ht.num_adults as adults_count
, ht.num_children children_count, ht.quantity as rooms_required, ht.supplier_accomm_id as hotel_code,ht.accommodation_ttii_code as ttii_code,bg.quantity as bag_count,ht.description as hoteldescription,
q.Quote_Type,q.Quote_desc
from basket.baskets bn
join basket.Quote q on q.quote_ref=bn.consumer_booking_ref
join customers c on c.customer_id=q.customer_id
join (select cn1.basket_id, con1.component_id, con1.component_option_id,con2.component_option_id, con1.quantity_required as inbound_quantity,con2.quantity_required as outbound_quantity,cdn1.total_selling_price as inbound_flight_fare, cdn2.total_selling_price as outbound_flight_fare, 
f1.flight_number as inbound_flight_number,f1.fare_type as inbound_fare_type,f1.flight_operator as inbound_flight_operator,f1.departure_ts as inbound_departure_time, f1.arrival_ts as inbound_arrival_time, f1.from_airport_code as inbound_source,f1.to_airport_code as inbound_destination,  
f2.flight_number as outbound_flight_number,f2.fare_type as outbound_fare_type,f2.flight_operator as outbound_flight_operator,f2.departure_ts as outbound_departure_time, f2.arrival_ts as outbound_arrival_time, f2.from_airport_code as outbound_source,f2.to_airport_code as outbound_destination  
from basket.components cn1, basket.component_options con1, basket.component_options con2, basket.component_details cdn1, basket.component_details cdn2,  
basket.flights f1, basket.flights f2
where con1.component_option_id > con2.component_option_id and con1.component_id = con2.component_id 
and cdn1.component_option_id = con1.component_option_id and cdn2.component_option_id = con2.component_option_id
and f1.component_option_id = con1.component_option_id and f2.component_option_id = con2.component_option_id
and cn1.component_id = con1.component_id
and cn1.component_id = con2.component_id
) fl on fl.basket_id = bn.basket_id
join (select * from basket.components cn, basket.component_options con, basket.component_details cdn, basket.accommodations an, basket.accommodation_rooms arn
where cn.component_id = con.component_id and con.component_option_id = cdn.component_option_id and an.component_id = cn.component_id and an.accommodation_id = arn.accommodation_id
) ht on ht.basket_id = bn.basket_id 
join (select * from basket.components cn, basket.component_options con, basket.component_details cdn, basket.extras en
where cn.component_id = con.component_id and con.component_option_id = cdn.component_option_id and en.component_id = cn.component_id
) bg on  bg.basket_id = bn.basket_id 

where '||searchdata||' order by created_date desc LIMIT 50';	 

RETURN QUERY EXECUTE sql;		
     
END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100
  ROWS 1000;
ALTER FUNCTION get_recent_quotes(character varying)
  OWNER TO chilli;



-- Function: get_quote_details(refcursor, refcursor, refcursor, refcursor, refcursor, character varying)

-- DROP FUNCTION get_quote_details(refcursor, refcursor, refcursor, refcursor, refcursor, character varying);

DROP FUNCTION IF EXISTS get_quote_details(refcursor, refcursor, refcursor, refcursor, refcursor, character varying);


  CREATE OR REPLACE FUNCTION get_quote_details(
    ref1 refcursor,
    ref2 refcursor,
    ref3 refcursor,
    ref4 refcursor,
    ref5 refcursor,
    searchsql character varying)
  RETURNS SETOF refcursor AS
$BODY$
DECLARE basketsql character varying;
DECLARE flightsql character varying;
DECLARE accommodationsql character varying;
DECLARE bagssql character varying;
DECLARE transferssql character varying;
BEGIN 
	basketsql:= 'SELECT bn.basket_id, q.quote_ref, c.first_name, c.last_name, c.email_address, 
		q.created_on created_date, bn.booking_date booking_date 
		FROM basket.baskets bn, basket.Quote q, customers c 
		WHERE q.quote_ref = bn.consumer_booking_ref AND '||searchSql||' 	
		ORDER BY q.created_on desc LIMIT 50';

	flightsql:= 'SELECT bn.basket_id, cn1.component_id,
		con1.quantity_required as inbound_quantity,
		cdn1.total_selling_price as inbound_flight_fare,
		f1.flight_number as inbound_flight_number,
		f1.fare_type as inbound_fare_type,
		f1.flight_operator as inbound_flight_operator,
		f1.departure_ts as inbound_departure_time,
		f1.arrival_ts as inbound_arrival_time,
		f1.from_airport_code as inbound_source,
		f1.to_airport_code as inbound_destination,
		con2.quantity_required as outbound_quantity,
		cdn2.total_selling_price as outbound_flight_fare,
		f2.flight_number as outbound_flight_number,
		f2.fare_type as outbound_fare_type,
		f1.flight_operator as outbound_flight_operator,
		f1.departure_ts as outbound_departure_time,
		f1.arrival_ts as outbound_arrival_time,
		f1.from_airport_code as  outbound_source,
		f1.to_airport_code as outbound_destination 
		FROM  basket.components cn1, basket.component_options con1, 
		basket.component_options con2, basket.component_details cdn1, basket.component_details cdn2,  
		basket.flights f1, basket.flights f2, basket.baskets bn, basket.quote q
		WHERE con1.component_option_id > con2.component_option_id and con1.component_id = con2.component_id 
		AND cdn1.component_option_id = con1.component_option_id and cdn2.component_option_id = con2.component_option_id
		AND f1.component_option_id = con1.component_option_id and f2.component_option_id = con2.component_option_id
		AND cn1.component_id = con1.component_id
		AND cn1.component_id = con2.component_id
		AND bn.basket_id = cn1.basket_id
		AND bn.consumer_booking_ref = q.quote_ref AND '||searchSql||' 		
		ORDER BY q.created_on desc LIMIT 50';
		
	accommodationsql:= 'SELECT bn.basket_id, cn.component_id,
		an.accomm_name,
		an.num_nights,an.address,
		an.rating,an.user_score,
		an.board_code,cdn.total_selling_price as hotel_fare,
		arn.num_adults as adults_count,
		arn.num_children children_count, 
		arn.quantity as rooms_required, 
		an.supplier_accomm_id as hotel_code,
		an.accommodation_ttii_code as ttii_code,
		arn.description as hoteldescription,
		arn.room_code as room_type_code,
		arn.room_rate_code as room_rate_code,
		cn.start_date as hotel_check_in,
		cn.end_date as hotel_check_out
		FROM basket.components cn, 
		basket.component_options con, basket.component_details cdn, 
		basket.accommodations an, basket.accommodation_rooms arn, basket.baskets bn, basket.quote q
		WHERE cn.component_id = con.component_id 
		AND con.component_option_id = cdn.component_option_id
		AND an.component_id = cn.component_id
		AND an.accommodation_id = arn.accommodation_id 
		AND bn.basket_id = cn.basket_id
		AND bn.consumer_booking_ref = q.quote_ref AND '||searchSql||'
		ORDER BY q.created_on desc LIMIT 50';

	bagssql:= 'SELECT bn.basket_id, cn.component_id, en.quantity, cdn.total_selling_price as baggage_fare
		FROM basket.components cn, basket.component_options con, 
		basket.component_details cdn, basket.extras en, basket.baskets bn, basket.quote q
		WHERE cn.component_id = con.component_id 
		AND con.component_option_id = cdn.component_option_id 
		AND en.component_id = cn.component_id
		AND bn.basket_id = cn.basket_id
		AND bn.consumer_booking_ref = q.quote_ref
		AND cn.product_type_code = ''Baggage'' AND '||searchSql||'
		ORDER BY q.created_on desc LIMIT 50';

	transferssql:='SELECT bn.basket_id, cn.component_id, en.quantity, cdn.total_selling_price as transfers_fare, en.reference_data as transfers_provider_id 
		FROM basket.components cn, basket.component_options con, basket.component_details cdn, basket.extras en, basket.baskets bn, basket.quote q
		WHERE cn.component_id = con.component_id 
		AND con.component_option_id = cdn.component_option_id 
		AND en.component_id = cn.component_id
		AND bn.basket_id = cn.basket_id
		AND bn.consumer_booking_ref = q.quote_ref
		AND cn.product_type_code = ''Transfers'' AND '||searchSql||'
		ORDER BY q.created_on desc LIMIT 50';

    OPEN ref1 FOR EXECUTE basketsql;    
    RETURN NEXT ref1;
    OPEN ref2 FOR EXECUTE flightsql;		
    RETURN NEXT ref2; 
    OPEN ref3 FOR EXECUTE accommodationsql;
    RETURN NEXT ref3;   
    OPEN ref4 FOR EXECUTE bagssql;
    RETURN NEXT ref4;
    OPEN ref5 FOR EXECUTE transferssql;   
    RETURN NEXT ref5;
END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100
  ROWS 1000;
ALTER FUNCTION get_quote_details(refcursor, refcursor, refcursor, refcursor, refcursor, character varying)
  OWNER TO chilli;

-- Function: show_baskets()

-- DROP FUNCTION show_baskets();
DROP FUNCTION IF EXISTS show_baskets();



CREATE OR REPLACE FUNCTION show_baskets()
  RETURNS SETOF basket.baskets AS
$BODY$
BEGIN

RETURN QUERY
SELECT * FROM basket.baskets where basket_id=1;

RETURN QUERY
SELECT * FROM basket.baskets where basket_id=2;   -- has to return same rowtype as first_table!

END
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100
  ROWS 1000;
ALTER FUNCTION show_baskets()
  OWNER TO chilli;


  -- Function: f_save_booking_details(text)

-- DROP FUNCTION f_save_booking_details(text);

DROP FUNCTION IF EXISTS f_save_booking_details(text);



CREATE OR REPLACE FUNCTION f_save_booking_details(jsoninput text)
  RETURNS boolean AS
$BODY$
DECLARE booking_object json;
DECLARE passengers_object json;
DECLARE passengers_array json;
DECLARE payment_object json;
DECLARE payment_component_id int;
DECLARE payment_component_option_id int;
BEGIN
	IF length(jsoninput) <> 0
	THEN
		RAISE NOTICE 'Converting input param as JSON object';
		SELECT (jsoninput)::json into booking_object;
		
		IF (select count(*) from json_each(booking_object)) > 0
		THEN
			SELECT (booking_object->>'PassengerDetails')::json into passengers_array;	
			IF (json_array_length(passengers_array)) > 0
			THEN
				RAISE NOTICE 'Inserting passengers';
				FOR passengers_object IN SELECT * FROM json_array_elements((booking_object->>'PassengerDetails')::json)
				    LOOP  
					IF (select count(*) from json_each(passengers_object)) > 0
					THEN
						INSERT INTO passengers(quote_reference,title,first_name,last_name,middle_name,email,mobile,telephone,postcode,address1,address2,address3,passport_number,dob,is_lead_passenger,nationality,gender,counts_as) values (booking_object->>'QuoteReference',passengers_object->>'Title',passengers_object->>'FirstName',passengers_object->>'LastName',passengers_object->>'MiddleName',passengers_object->>'Email',passengers_object->>'Mobile',passengers_object->>'Telephone',passengers_object->>'PostCode',passengers_object->>'Address1',passengers_object->>'Address2',passengers_object->>'Address3',passengers_object->>'PassportNumber',CAST(passengers_object->>'DateOfBirth' as date),CAST(passengers_object->>'IsLeadPassenger' as Boolean),'UK',passengers_object->>'Gender',passengers_object->>'Type');
                                                Update basket.quote set quotes_status='BQ' where quote_ref=booking_object->>'QuoteReference';
					ELSE
						RAISE EXCEPTION 'Error saving booking information in passenger details';
					END IF;				       
				    END LOOP;        
				RAISE NOTICE 'Ending insert of passengers';

				RAISE NOTICE 'Collecting payment object from booking object';
				SELECT (booking_object->>'PaymentInfo')::json into payment_object;

				IF (select count(*) from json_each(payment_object)) > 0
				THEN
					RAISE NOTICE 'Inserting component for payment details';
					INSERT INTO basket.components(basket_id,supplier_code,product_type_code) values (CAST(booking_object->>'BasketId' as int),payment_object->>'PaymentProvider','CRD');
					payment_component_id := (SELECT CURRVAL(pg_get_serial_sequence('basket.components','component_id')));
					RAISE NOTICE 'End of inserting component for payment details';

					RAISE NOTICE 'Inserting Components Options from payment info';
					INSERT INTO basket.component_options(component_id)values(payment_component_id);
					payment_component_option_id := (SELECT CURRVAL(pg_get_serial_sequence('basket.component_options','component_option_id')));
					RAISE NOTICE 'End of inserting Components Options from payment info';

					RAISE NOTICE 'Inserting Components Details from payment info';
					INSERT INTO basket.component_details(component_id,component_option_id,total_selling_price)values(payment_component_id,payment_component_option_id,CAST(payment_object->>'Amount' as decimal));
					RAISE NOTICE 'End of inserting Components Details from payment info';	
					
					RAISE NOTICE 'Inserting payment details';	
					INSERT into payments(component_id,payment_date,amount,name_on_card,payment_email,payment_country,card_type,card_number,card_valid_from,card_expiry,status,success,transaction_reference,payment_provider,admin_user_id) values (payment_component_id,CAST(payment_object->>'PaymentDate' as timestamp), CAST(payment_object->>'Amount' as decimal),payment_object->>'NameOnCard',payment_object->>'PaymentEmail',payment_object->>'PaymentCountry',payment_object->>'CardType',payment_object->>'CardNumber',payment_object->>'CardValidFrom',payment_object->>'CardExpiry',payment_object->>'Status',CAST(payment_object->>'IsPaymentSuccess' as Boolean),payment_object->>'TransactionReference',payment_object->>'PaymentProvider',CAST(booking_object->>'UserId' as int));

					RAISE NOTICE 'End of inserting payment details';
						
				ELSE
					RAISE EXCEPTION 'Error saving booking information in payment details';
				END IF;					
			ELSE
				RAISE EXCEPTION 'Error saving booking information: Passenger details should not be null';
			END IF;
		ELSE
			RAISE EXCEPTION 'Error saving booking information';
		END IF;
	ELSE
		RAISE EXCEPTION 'Booking information should not be empty';
	END IF;	
        return true;        
END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
ALTER FUNCTION f_save_booking_details(text)
  OWNER TO chilli;


  -- Function: run_for_component_options_flights()

-- DROP FUNCTION run_for_component_options_flights();
DROP FUNCTION IF EXISTS run_for_component_options_flights();

CREATE OR REPLACE FUNCTION run_for_component_options_flights()
  RETURNS integer AS
$BODY$
    DECLARE myCId int;
    DECLARE NewComponent_id int;
    DECLARE OldComponent_id int; 
BEGIN
    OldComponent_id:=0;

    FOR i in 585..810   
    LOOP
        NewComponent_id:= (Select fn.component_id from basket.flights fn where flight_id = i limit 1);
        RAISE NOTICE 'Component id %', NewComponent_id;
        RAISE NOTICE 'Old Component id %', OldComponent_id;
        If(NewComponent_id = OldComponent_id)
        Then
	     RAISE NOTICE 'Same ComponentId came';
	     RAISE NOTICE 'FlightId %', i;
	     myCId:= (select component_option_id from basket.component_options con where con.component_id = CAST(NewComponent_id as int) limit 1);
	     RAISE NOTICE 'Option Id %', myCid + 1;
	     update basket.flights set component_option_id = myCId + 1 where flight_id = i;
	     OldComponent_id:= NewComponent_id;
        Else
             RAISE NOTICE 'FlightId %', i;
	     myCId:= (select component_option_id from basket.component_options con where con.component_id = CAST(NewComponent_id as int) limit 1);
	     RAISE NOTICE 'Option Id %', myCid;
	     update basket.flights set component_option_id = myCId where flight_id = i;
	     OldComponent_id:= NewComponent_id;
        End If;
    END LOOP; 
END 
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
ALTER FUNCTION run_for_component_options_flights()
  OWNER TO chilli;


  -- Function: myfuncshowbasket(refcursor, refcursor, integer)

-- DROP FUNCTION myfuncshowbasket(refcursor, refcursor, integer);

DROP FUNCTION IF EXISTS myfuncshowbasket(refcursor, refcursor, integer);

CREATE OR REPLACE FUNCTION myfuncshowbasket(
    refcursor,
    refcursor,
    basketid integer)
  RETURNS SETOF refcursor AS
$BODY$
BEGIN
    OPEN $1 FOR SELECT * FROM basket.baskets bn where bn.basket_id = basketid;
    RETURN NEXT $1;
    OPEN $2 FOR SELECT cn.* FROM basket.components cn join basket.baskets bn on cn.basket_id = bn.basket_id where bn.basket_id = basketid;
    RETURN NEXT $2;
END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100
  ROWS 1000;
ALTER FUNCTION myfuncshowbasket(refcursor, refcursor, integer)
  OWNER TO chilli;



-- Function: get_agents_graphdata(integer, date)

-- DROP FUNCTION get_agents_graphdata(integer, date);

DROP FUNCTION IF EXISTS get_agents_graphdata(integer, date);

CREATE OR REPLACE FUNCTION get_agents_graphdata(
    IN user_id integer,
    IN paydate date)
  RETURNS TABLE(paymentdate date, count bigint) AS
$BODY$
BEGIN   
      RETURN QUERY select date(payment_date) as paymentdate, count(*) as count  from payments where admin_user_id=user_id and date(payment_date) between date(paydate+1) and date(paydate)+7  group by date(payment_date);
END
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100
  ROWS 1000;
ALTER FUNCTION get_agents_graphdata(integer, date)
  OWNER TO chilli;


  -- Function: get_agents_graphdata_basedon_weeks(integer, date)

-- DROP FUNCTION get_agents_graphdata_basedon_weeks(integer, date);

 DROP FUNCTION IF EXISTS get_agents_graphdata_basedon_weeks(integer, date);

CREATE OR REPLACE FUNCTION get_agents_graphdata_basedon_weeks(
    IN user_id integer,
    IN paydate date)
  RETURNS TABLE(week1 bigint, week2 bigint, week3 bigint, week4 bigint) AS
$BODY$
Declare firstweek bigint;
	 secondweek bigint;
	 thirdweek bigint;
	 forthdweek bigint;
BEGIN   
	
	select sum(a.dcount) into firstweek from (select date(payment_date) as paymentdate, count(*) as dcount  from payments where admin_user_id=user_id group by payment_date) a where a.paymentdate between date(paydate)-7 and date(paydate);
	select sum(a.dcount) into secondweek from (select date(payment_date) as paymentdate, count(*) as dcount  from payments where admin_user_id=user_id group by payment_date) a where a.paymentdate between date(paydate)-14 and date(paydate)-7;
        select sum(a.dcount) into thirdweek from (select date(payment_date) as paymentdate, count(*) as dcount  from payments where admin_user_id=user_id group by payment_date) a where a.paymentdate between date(paydate)-21 and date(paydate)-14;
        select sum(a.dcount) into forthdweek from (select date(payment_date) as paymentdate, count(*) as dcount  from payments where admin_user_id=user_id group by payment_date) a where a.paymentdate between date(paydate)-28 and date(paydate)-21;

        RETURN QUERY select firstweek,secondweek,thirdweek,forthdweek;
END
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100
  ROWS 1000;
ALTER FUNCTION get_agents_graphdata_basedon_weeks(integer, date)
  OWNER TO chilli;

  -- Function: getagentsgraphdata(integer, date)

-- DROP FUNCTION getagentsgraphdata(integer, date);

DROP FUNCTION IF EXISTS getagentsgraphdata(integer, date);

CREATE OR REPLACE FUNCTION getagentsgraphdata(
    IN user_id integer,
    IN paydate date)
  RETURNS TABLE(paymentdate date, count bigint) AS
$BODY$
BEGIN   
      RETURN QUERY select date(payments.payment_date) as paymentdate, count(*) as count  from payments where payments.admin_user_id=user_id and date(payments.payment_date) between date(paydate) and date(paydate)+7 group by date(payments.payment_date);
END
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100
  ROWS 1000;
ALTER FUNCTION getagentsgraphdata(integer, date)
  OWNER TO chilli;



----Removing unused functions and tables


 DROP FUNCTION IF EXISTS f_insert_quote_details_bkp07122015(jsoninput text);
 DROP FUNCTION IF EXISTS f_insert_quote_details_bkp19012016(jsoninput text);
 DROP FUNCTION IF EXISTS f_insert_quote_details_bkp19012016a2(jsoninput text);
 DROP FUNCTION IF EXISTS f_insert_quote_details_bkp19112015(jsoninput text);
 DROP FUNCTION IF EXISTS f_insert_quote_details_bkp20112015(jsoninput text);
 DROP FUNCTION IF EXISTS f_insert_quote_details_bkp25012016(jsoninput text);
 DROP FUNCTION IF EXISTS get_recent_quotes_bkp11012015(IN searchdata character varying);
 DROP FUNCTION IF EXISTS get_recent_quotes_bkp16112015(IN searchdata character varying);
 DROP FUNCTION IF EXISTS get_recent_quotes_bkp20112015(IN searchdata character varying);
 DROP FUNCTION IF EXISTS get_recent_quotes_bkp2012016(IN searchdata character varying);
 DROP FUNCTION IF EXISTS get_recent_quotes_bkp23112015(IN searchdata character varying);

 DROP TABLE IF EXISTS test
 

