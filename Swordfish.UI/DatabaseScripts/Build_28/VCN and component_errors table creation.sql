﻿
Create table component_errors(
	    component_error_id serial,
            component_id integer NOT NULL,
            error character varying,
            created_by integer,
            created_date timestamp without time zone,
            modify_by integer,
            modify_date timestamp without time zone,
            CONSTRAINT component_errors_pkey PRIMARY KEY (component_error_id),
            CONSTRAINT components_errors_id_fkey FOREIGN KEY (component_id)
      REFERENCES components (component_id) MATCH SIMPLE

);




Create table component_vcn (
	    component_vcn_id serial,
            component_id integer NOT NULL,
            pan character varying,
            card_type character varying,
            expiry_date character varying,
            card_holder_name character varying,
            reference character varying,
            created_by integer,
            created_date timestamp without time zone,
            modify_by integer,
            modify_date timestamp without time zone,
            CONSTRAINT component_vcn_pkey PRIMARY KEY (component_vcn_id),
	CONSTRAINT components_vcn_id_fkey FOREIGN KEY (component_id)
      REFERENCES components (component_id) MATCH SIMPLE

);