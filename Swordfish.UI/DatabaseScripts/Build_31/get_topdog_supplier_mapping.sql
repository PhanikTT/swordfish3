﻿-- Function: get_topdog_supplier_mapping(character varying, integer)

-- DROP FUNCTION get_topdog_supplier_mapping(character varying, integer);

CREATE OR REPLACE FUNCTION get_topdog_supplier_mapping(
    sourcecode character varying,
    recordoption_id integer)
  RETURNS SETOF json AS
$BODY$
BEGIN
-- Created/Modified By		||Created/Modified Date 		||Comments
--========================================================================================================
-- Thamma Reddy		         ||04/10/2016				||Restrucuring DB Object

return query 
select array_to_json(array_agg(row_to_json(a))) as resutlt from (
select 
	source_system_code as SourceSystemCode,
	source_system_description as SourceSystemDescription,
	ss_custom_column1 as SSCustomColumn1,
	ss_custom_column2 as SSCustomColumn2,
	record_type_option_id as RecordTypeOptionId,
	source_system_option_id as SourceSystemOptionId,
	mapped_system_code as MappedSystemCode,
	mapped_system_description as MappedSystemDescription,
	ms_custom_column1 as MSCustomColumn1,
	ms_custom_column2 as MSCustomColumn2,	
	mapped_system_option_id as MappedSystemOptionId
	from topdog_supplier_mapping 	where source_system_code=sourcecode and record_type_option_id=recordoption_id 
	) a ;
END
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100
  ROWS 1000;
ALTER FUNCTION get_topdog_supplier_mapping(character varying, integer)
  OWNER TO chilli;
