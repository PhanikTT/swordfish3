﻿-- Table: topdog_supplier_mapping

-- DROP TABLE topdog_supplier_mapping;

CREATE TABLE topdog_supplier_mapping
(
  mapping_id serial NOT NULL,
  source_system_code character varying(25),
  source_system_description character varying(255),
  ss_custom_column1 character varying(255),
  ss_custom_column2 character varying(255),
  record_type_option_id integer,
  source_system_option_id integer,
  mapped_system_code character varying(25),
  mapped_system_description character varying(255),
  ms_custom_column1 character varying(255),
  ms_custom_column2 character varying(255),
  mapped_system_option_id integer,
  created_by integer,
  created_date timestamp without time zone,
  modified_by integer,
  modified_date timestamp without time zone
)
WITH (
  OIDS=FALSE
);
ALTER TABLE topdog_supplier_mapping
  OWNER TO chilli;
