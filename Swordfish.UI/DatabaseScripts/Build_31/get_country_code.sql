﻿-- Function: get_country_code(character varying)

-- DROP FUNCTION get_country_code(character varying);

CREATE OR REPLACE FUNCTION get_country_code(country character varying)
  RETURNS character varying AS
$BODY$
BEGIN
-- Created/Modified By		||Created/Modified Date 		||Comments
--========================================================================================================
-- Thamma Reddy		         ||04/10/2016				||Restrucuring DB Object

return (select country_code from view_Country_master where country_name=country);
END
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
ALTER FUNCTION get_country_code(character varying)
  OWNER TO chilli;
