﻿(function (app) {
    'use strict'

    //Controller Creation
    app.controller('hotelCtrl', hotelCtrl);

    //DI for Controller
    hotelCtrl.$inject = ['$scope', 'hotelService', 'notificationService', 'transfersService', '$interval', '$filter'];

    //Hotel Controller
    function hotelCtrl($scope, hotelService, notificationService, transfersService, $interval, $filter) {
        $scope.hotelPriceOrderBy = '+AmountAfterTax';
        $scope.initHotelSearch = initHotelSearch;
        $scope.updateHotelCount = updateHotelCount;
        $scope.refreshHotels = refreshHotels;
        $scope.stopUpdatingHotelCount = stopUpdatingHotelCount;
        $scope.addHotel = addHotel;
        $scope.removeHotel = removeHotel;
        //$scope.orderByHotelPrice = orderByHotelPrice;
        $scope.hotelFilter = hotelFilter;
        $scope.priorityStates = {};
        $scope.selectedQuoteId = '';
        $scope.setSelectedHotelPrice = setSelectedHotelPrice;
        $scope.searchModel = notificationService.searchModel;
        $scope.isAsyncProcess = hotelService.isAsyncProcess();
        $scope.isHotelSearchDone = false;
        $scope.latestHotelsCount = 0;
        $scope.queryStatus = 'Not yet started';
        $scope.TTSSHotel = {};
        $scope.TTSSHotel.showTTSSDetials = false;
        $scope.TTSSHotel.upsellCount = 3;
        $scope.upsell = [];
        $scope.TTSSHotel.hotelResult = [];
        $scope.hotelResults = [];
        $scope.isHotelsSearchButttonNeeded = true;
        $scope.hotelStates = notificationService.hotelStates;
        $scope.TTSSHotel.isFoundInHotelResult = false
        $scope.$watch(function () { return notificationService.hotel.isHotelsSearchButttonNeeded }, function (newValue) {
            $scope.isHotelsSearchButttonNeeded = notificationService.hotel.isHotelsSearchButttonNeeded;
        });

        stopUpdatingHotelCount();
        function initHotelSearch() {
            try {
                stopUpdatingHotelCount();
                $("#loader").show();
                notificationService.searchModel.ModifiedCheckOut = undefined;
                notificationService.searchModel.ModifiedCheckIn = undefined;
                var CheckOut = new Date(notificationService.searchModel.CheckOut);
                var CheckIn = new Date(notificationService.searchModel.CheckIn);
                var continueToGetHotels = false;
                var checkinDay = CheckIn.getUTCDate();
                var checkoutDay = CheckOut.getUTCDate();
                if (notificationService.flightsReturn != undefined && notificationService.flightsReturn.OutboundArrivalTime != undefined && notificationService.flightsReturn.InboundDepartureTime != undefined) {
                    var arrivalDateTime = new Date(notificationService.flightsReturn.OutboundArrivalTime);
                    var departureDateTime = new Date(notificationService.flightsReturn.InboundDepartureTime);
                    var arrivalHour = arrivalDateTime.getUTCHours();
                    var departureHour = departureDateTime.getUTCHours();
                    if (departureHour >= 23) {
                        var date = departureDateTime.getUTCDate();
                        var month = departureDateTime.getUTCMonth() + 1;
                        var year = departureDateTime.getUTCFullYear();
                        var lastDateOfMonth = new Date(year, month, 0).getUTCDate();
                        if (date == lastDateOfMonth) {
                            if (month == 12) {
                                year++;
                                month = 1;
                            }
                            else {
                                month++;
                            }
                            date = 1;
                        }
                        else {
                            date++;
                        }
                        if (checkoutDay != date)
                            notificationService.searchModel.ModifiedCheckOut = new Date(Date.UTC(year, (month - 1), date));
                    }
                    if (arrivalHour < 6) {
                        var date = arrivalDateTime.getUTCDate();
                        var month = arrivalDateTime.getUTCMonth() + 1;
                        var year = arrivalDateTime.getUTCFullYear();
                        if (date == 1) {
                            if (month == 1) {
                                year--;
                                month = 12;
                            }
                            else {
                                month--;
                            }
                            date = new Date(year, month, 0).getDate();
                        }
                        else {
                            date--;
                        }
                        if (checkinDay != date)
                            notificationService.searchModel.ModifiedCheckIn = new Date(Date.UTC(year, (month - 1), date));
                    }
                    continueToGetHotels = true;
                }
                else if (notificationService.flightsOneWayOutBound != undefined && notificationService.flightsOneWayOutBound.OutboundArrivalTime != undefined && notificationService.flightsOneWayInBound != undefined && notificationService.flightsOneWayInBound.InboundDepartureTime != undefined) {
                    var arrivalDateTime = new Date(notificationService.flightsOneWayOutBound.OutboundArrivalTime);
                    var departureDateTime = new Date(notificationService.flightsOneWayInBound.InboundDepartureTime);
                    var arrivalHour = arrivalDateTime.getUTCHours();
                    var departureHour = departureDateTime.getUTCHours();
                    if (arrivalHour < 6) {
                        var date = arrivalDateTime.getUTCDate();
                        var month = arrivalDateTime.getUTCMonth() + 1;
                        var year = arrivalDateTime.getUTCFullYear();
                        if (date == 1) {
                            if (month == 1) {
                                year--;
                                month = 12;
                            }
                            else {
                                month--;
                            }
                            date = new Date(year, month, 0).getUTCDate();
                        }
                        else {
                            date--;
                        }
                        if (checkinDay != date)
                            notificationService.searchModel.ModifiedCheckIn = new Date(Date.UTC(year, (month - 1), date));
                    }
                    if (departureHour >= 23) {
                        var date = departureDateTime.getUTCDate();
                        var month = departureDateTime.getUTCMonth() + 1;
                        var year = departureDateTime.getUTCFullYear();
                        var lastDateOfMonth = new Date(year, month, 0).getUTCDate();
                        if (date == lastDateOfMonth) {
                            if (month == 12) {
                                year++;
                                month = 1;
                            }
                            else {
                                month++;
                            }
                            date = 1;
                        }
                        else {
                            date++;
                        }
                        if (checkoutDay != date)
                            notificationService.searchModel.ModifiedCheckOut = new Date(Date.UTC(year, (month - 1), date));
                    }
                    continueToGetHotels = true;
                }
                else {
                    notificationService.validationResult = { showPopup: true, errorMsg: ['Please select flight(s)'], title: 'Alert' };
                    continueToGetHotels = false;
                    $("#loader").hide();
                }

                if (continueToGetHotels) {
                    removeHotel();
                    if (!$scope.isAsyncProcess) {
                        $scope.queryStatus = 'Query not running in async mode';
                        getHotels();
                    }
                    else {
                        $scope.isHotelSearchDone = false;
                        $scope.queryStatus = "Active";
                        $scope.latestHotelsCount = 0;
                        hotelService.getAsyncHotels(successCB, errorCB);
                    }
                }
            } catch (e) {
                $("#loader").hide();
            }
        }
        function getHotels() {
            hotelService.getHotels(successCB, errorCB);
        }
        //stopUHC=stopUpdatingHotelCount
        var stopUHC;
        function updateHotelCount() {
            try {
                if (angular.isDefined(stopUHC)) return;
                stopUHC = $interval(function () {
                    if (!$scope.isHotelSearchDone) {
                        if ($scope.isAsyncProcess)
                            hotelService.updateHotelCount(updateHotelsSCB, errorCB);
                    } else {
                        $scope.stopUpdatingHotelCount();
                    }
                }, hotelService.getAjaxCallTime());
            } catch (e) {
                stopUpdatingHotelCount();
                $scope.queryStatus = "Done";
            }
        }
        function stopUpdatingHotelCount() {
            if (angular.isDefined(stopUHC)) {
                $interval.cancel(stopUHC);
                stopUHC = undefined;
            }
        }
        function refreshHotels() {
            if ($scope.latestHotelsCount > 0) {
                if ($scope.isAsyncProcess) {
                    stopUpdatingHotelCount();
                    $scope.TTSSHotel.isFoundInHotelResult = false;
                    $scope.TTSSHotel.Livepirce = "Waiting for Live Price";
                    $("#loader").show();
                    hotelService.refreshHotels(successCB, errorCB);
                }
                else
                    alert("Hotels fetching is not configured as Async");
            }
        }
        function successCB(result) {
            try {
                if (result.data != undefined && result.data != null && result.data.length > 0) {
                    resetHotelFilters();
                    var resortFilters = [];
                    var boardTypeFilters = [];
                    var hotelRatingFilters = [];
                    for (var i = 0; i < result.data.length; i++) {
                        result.data[i].PricePerPerson = result.data[i].AmountAfterTax / (parseInt(notificationService.searchModel.Adults) + parseInt(notificationService.searchModel.Child) + parseInt(notificationService.searchModel.Infants))
                        result.data[i].boardTypes = [];
                        resortFilters.push({ value: result.data[i].ResortName, price: result.data[i].AmountAfterTax, selected: false });
                        if (result.data[i].Rating != undefined && result.data[i].Rating != null) {
                            hotelRatingFilters.push({ value: result.data[i].Rating, price: result.data[i].AmountAfterTax, selected: false });
                        }
                        for (var j = 0; j < result.data[i].RoomRates.length; j++) {
                            if (!(result.data[i].boardTypes.indexOf(result.data[i].RoomRates[j].RatePlanCode) > -1))
                                result.data[i].boardTypes.push(result.data[i].RoomRates[j].RatePlanCode);
                            boardTypeFilters.push({ value: result.data[i].RoomRates[j].RatePlanCode, price: result.data[i].AmountAfterTax, selected: false });
                        }
                    }
                    var tempResortFilters = [];
                    for (var f = 0; f < resortFilters.length; f++) {
                        if (!(tempResortFilters.indexOf(resortFilters[f].value) > -1)) {
                            if (resortFilters[f].value != null && resortFilters[f].value != "") {
                                tempResortFilters.push(resortFilters[f].value);
                                notificationService.hotelFilter.resort.push(resortFilters[f]);
                            }
                        }
                        else if (notificationService.hotelFilter.resort.length > 0) {
                            for (var i in notificationService.hotelFilter.resort) {
                                if (notificationService.hotelFilter.resort[i].value == resortFilters[f].value && parseInt(notificationService.hotelFilter.resort[i].price) > parseInt(resortFilters[f].price))
                                    notificationService.hotelFilter.resort[i].price = resortFilters[f].price
                            }
                        }
                    }
                    var tempBoardTypeFilters = [];
                    for (var f = 0; f < boardTypeFilters.length; f++) {
                        if (!(tempBoardTypeFilters.indexOf(boardTypeFilters[f].value) > -1)) {
                            tempBoardTypeFilters.push(boardTypeFilters[f].value);
                            notificationService.hotelFilter.board.push(boardTypeFilters[f]);
                        }
                        else if (notificationService.hotelFilter.board.length > 0) {
                            for (var i in notificationService.hotelFilter.board) {
                                if (notificationService.hotelFilter.board[i].value == boardTypeFilters[f].value && parseInt(notificationService.hotelFilter.board[i].price) > parseInt(boardTypeFilters[f].price))
                                    notificationService.hotelFilter.board[i].price = boardTypeFilters[f].price
                            }
                        }
                    }
                    var tempHotelRatingFilters = [];
                    for (var f = 0; f < hotelRatingFilters.length; f++) {
                        if (!(tempHotelRatingFilters.indexOf(hotelRatingFilters[f].value) > -1)) {
                            if (hotelRatingFilters[f].value != null && hotelRatingFilters[f].value != "") {
                                tempHotelRatingFilters.push(hotelRatingFilters[f].value);
                                notificationService.hotelFilter.hotelRating.push(hotelRatingFilters[f]);
                            }
                        }
                        else if (notificationService.hotelFilter.hotelRating.length > 0) {
                            for (var i in notificationService.hotelFilter.hotelRating) {
                                if (notificationService.hotelFilter.hotelRating[i].value == hotelRatingFilters[f].value && parseInt(notificationService.hotelFilter.hotelRating[i].price) > parseInt(hotelRatingFilters[f].price))
                                    notificationService.hotelFilter.hotelRating[i].price = hotelRatingFilters[f].price
                            }
                        }
                    }
                }
                if (result.data != undefined && result.data != null && result.data.length > 0) {
                    for (var i = 0; i < result.data.length; i++) {
                        result.data[i].boardRates = [];
                        for (var j = 0; j < result.data[i].RoomRates.length; j++) {
                            var boardRate = { roomDescription: '', boardPrices: [] }
                            boardRate.roomDescription = result.data[i].RoomRates[j].RoomDescription;
                            boardRate.NonRefundable = (result.data[i].RoomRates[j].NonRefundable != null && result.data[i].RoomRates[j].NonRefundable != false) ? result.data[i].RoomRates[j].NonRefundable : false;
                            for (var k = 0; k < result.data[i].boardTypes.length; k++) {
                                if (result.data[i].boardTypes[k] == result.data[i].RoomRates[j].RatePlanCode) {
                                    boardRate.boardPrices.push(result.data[i].RoomRates[j]);
                                }
                                else
                                    boardRate.boardPrices.push('');
                            }
                            result.data[i].boardRates.push(boardRate);
                        }
                    }
                }
                //Hotel Rating Sorting 1-5
                if (notificationService.hotelFilter.hotelRating != undefined && notificationService.hotelFilter.hotelRating.length > 0) {
                    notificationService.hotelFilter.hotelRating = notificationService.hotelFilter.hotelRating.sort(function (a, b) { return (a.value > b.value) ? 1 : ((b.value > a.value) ? -1 : 0); });
                }
                //Resort A-Z sorting
                if (notificationService.hotelFilter.resort != undefined && notificationService.hotelFilter.resort.length > 0) {
                    notificationService.hotelFilter.resort.sort(function (a, b) { if (a.value < b.value) return -1; if (a.value > b.value) return 1; return 0; });
                }
                $scope.hotelResultsBase = result.data;
                $scope.hotelResults = result.data;
                if ($scope.isAsyncProcess) {
                    $scope.latestHotelsCount = 0;
                    $scope.updateHotelCount();
                }
                $scope.upsell = [];
                for (var i = 0; i < $scope.TTSSHotel.upsellCount; i++) {
                    if ($scope.hotelResults.length > 0 && $scope.hotelResults[0].PopularHotel == true)
                        $scope.upsell.push($scope.hotelResults.shift());
                }

                if ($scope.searchModel.TTSSQuoteReference != undefined && $scope.searchModel.TTSSQuoteReference != '' && $scope.upsell.length > 2)
                    $scope.upsell.length = 2;
                angular.forEach($scope.hotelResults, function (value, key) {
                    var hotelMasterKey = '';
                    var hotelKey = value.HotelCode.split(";");
                    if (hotelKey.length == 3) {
                        hotelMasterKey = hotelKey[2];
                    }
                    else {
                        hotelMasterKey = value.HotelCode;
                    }
                    if ($scope.TTSSHotel.isFoundInHotelResult == false) {
                        if ($scope.TTSSHotel != undefined && $scope.TTSSHotel != null && $scope.TTSSHotel.hotelResult.length > 0 &&
                            (hotelMasterKey == $scope.TTSSHotel.hotelResult[0].MasterHotelId || $scope.TTSSHotel.hotelResult[0].HotelCode == value.HotelCode)) {

                            $scope.TTSSHotel.Livepirce = "£" + parseInt(value.AmountAfterTax).toFixed(0);
                            $scope.TTSSHotel.isFoundInHotelResult = true;
                            $scope.TTSSHotel.hotelResult[0] = value;
                            $scope.hotelResults.splice(key, 1);
                        }
                    }
                });
                if (!$scope.TTSSHotel.isFoundInHotelResult) {
                    $scope.TTSSHotel.Livepirce = "Hotel not available";
                }
                if ($scope.hotelResults.length > 0 || $scope.upsell.length > 0) {
                    notificationService.hotel.isHotelsSearchButttonNeeded = false;
                }
                $scope.isHotelExpanded = true;
                $("#loader").hide();
            } catch (e) {
                $("#loader").hide();
            }
        }
        function updateHotelsSCB(hotelsCount, isHotelSearchDone) {
            try {
                $scope.isHotelSearchDone = isHotelSearchDone;
                $scope.queryStatus = isHotelSearchDone ? "Done" : "Active";
                $scope.latestHotelsCount += hotelsCount;
                $("#loader").hide();
            } catch (e) {
                $("#loader").hide();
            }
        }
        function resetHotelFilters() {
            hotelService.clearHotelFilters();
        }
        function errorCB(error) {
            $("#loader").hide();
            if (error != null) {
                if (error.status == -1) {
                    notificationService.validationResult = { showPopup: true, errorMsg: ["Some technical error occurred. Please refresh or contact system Administrator..!"], title: 'Hotels: Server Error' };
                }
                else {
                    if (error.message != null && error.message != undefined && error.message != '') {
                        notificationService.validationResult = { showPopup: true, errorMsg: [error.message], title: 'Hotels: Error' };

                    } else {
                        if (error.status == 500) {
                            notificationService.validationResult = { showPopup: true, errorMsg: ["Cannot reach the server, Please try again."], title: 'Hotels: Error' };
                        }
                        else {
                            notificationService.validationResult = { showPopup: true, errorMsg: [error.status, error.statusText], title: 'Hotels: Error' };
                        }
                    }
                }
            }
        }
        function addHotel(hotel) {
            if (notificationService.hotel == undefined) {
                $scope.transfers = "";
                $scope.TransfersStatus = "Please click Get Transfers button to get data.";
                transfersService.removeTransfers();
            }
            else if (hotel.TTIcode != notificationService.hotel.TTIcode) {
                $scope.transfers = "";
                $scope.TransfersStatus = "Please click Get Transfers button to get data.";
                transfersService.removeTransfers();
            }
            $scope.IsRefreshClicked = false;
            hotelService.addHotel(hotel);
        }
        function removeHotel() {
            hotelService.removeHotel();
            notificationService.resetFlights = false;
            notificationService.resetHotels = false;
            notificationService.clearNotificationData(false, false);
            notificationService.hotelStates.activeItem = "";
            if (notificationService.resetHotels) {
                if (newValue) {
                    if ($scope.isAsyncProcess) {
                        $scope.latestHotelsCount = 0;
                        $scope.queryStatus = 'Not yet started';
                    }
                    stopUpdatingHotelCount();
                    $scope.hotelResults = [];
                    $scope.upsell = [];
                }
            }
        }
        function setSelectedHotelPrice(selectedQuote, hotel) {
            $scope.selectedQuoteId = selectedQuote.QuoteId + selectedQuote.RoomTypeCode + hotel.ReferenceId;
            hotel.RateQuoteId = selectedQuote.QuoteId;
            hotel.RoomTypeCode = selectedQuote.RoomTypeCode;
            hotel.RoomDescription = selectedQuote.RoomDescription;
            hotel.AmountAfterTax = selectedQuote.AmountAfterTax;
            hotel.NonRefundable = selectedQuote.NonRefundable;
            var paxCount = 0;
            angular.forEach(notificationService.searchModel.OccupancyRooms, function (Key, value) {
                paxCount = paxCount + parseInt(Key.AdultsCount) + parseInt(Key.ChildCount) + parseInt(Key.InfantCount);
            });
            hotel.PricePerPerson = parseFloat(hotel.AmountAfterTax) / paxCount;
            hotel.BoardType = selectedQuote.RatePlanCode;
            $scope.addHotel(hotel);
        }
        //$scope.$watch(function () { return notificationService.hotelFilter; }, function (newValue, oldValue) { $scope.hotelFilter = newValue; });
        $scope.$watch(function () { return notificationService.changeSearch; }, function (newValue, oldValue) {
            if (notificationService.resetHotels) {
                if (newValue) {
                    if ($scope.isAsyncProcess) {
                        $scope.latestHotelsCount = 0;
                        $scope.queryStatus = 'Not yet started';
                    }
                    stopUpdatingHotelCount();
                    $scope.hotelResults = [];
                    $scope.upsell = [];
                    $scope.TTSSHotel = [];
                    $scope.TTSSHotel.showTTSSDetials = false;
                    //$scope.TTSSHotel.hotelResult = [];
                }
            }
        }, true);

        $scope.selectPrice = [];
        $scope.selectPosition = function ($parentIndex, $index) {
            $scope.selectedPosition = {
                parent: $parentIndex,
                index: $index
            };
        };

        if ($scope.searchModel.TTSSQuoteReference != undefined && $scope.searchModel.TTSSQuoteReference != '') {
            hotelService.getTTSSHotelDetails({ TtssQuote: $scope.searchModel.TTSSQuoteReference }, successCBForTTSSHotelDetails, erroCBForTTSSHotelDetails);
        }


        function successCBForTTSSHotelDetails(result) {
            if (result.status = 200 && result.data != undefined) {
                $scope.TTSSHotel.showTTSSDetials = true;
                $scope.TTSSHotel.upsellCount = 2;
                $scope.TTSSHotel.hotelResult[0] = {};
                $scope.TTSSHotel.hotelResult[0].AmountAfterTax = (parseInt(result.data.AccomPrice) * (parseInt(result.data.Adults) + parseInt(result.data.Children)));
                $scope.TTSSHotel.hotelResult[0].BoardType = getBoardType(result.data.BoardTypeCode.toLocaleLowerCase());
                $scope.TTSSHotel.hotelResult[0].HotelCity = result.data.GatewayName;
                $scope.TTSSHotel.hotelResult[0].HotelKey = result.data.HotelKey;
                $scope.TTSSHotel.hotelResult[0].MasterHotelId = result.data.MasterHotelId;
                $scope.TTSSHotel.hotelResult[0].TTIcode = result.data.TTICode;
                $scope.TTSSHotel.hotelResult[0].HotelCodeContext = result.data.contentSource;
                $scope.TTSSHotel.hotelResult[0].HotelName = result.data.Accommodation;
                $scope.TTSSHotel.hotelResult[0].PAXCount = {};
                $scope.TTSSHotel.hotelResult[0].PopularHotel = null;
                $scope.TTSSHotel.hotelResult[0].PricePerPerson = $scope.TTSSHotel.hotelResult[0].AmountAfterTax / (parseInt(result.data.Adults) + parseInt(result.data.Children));
                $scope.TTSSHotel.hotelResult[0].RateQuoteId = null;
                $scope.TTSSHotel.hotelResult[0].Rating = result.data.Rating
                $scope.TTSSHotel.hotelResult[0].ReferenceId = null;
                $scope.TTSSHotel.hotelResult[0].ReferenceType = null;
                $scope.TTSSHotel.hotelResult[0].ResortName = null;
                $scope.TTSSHotel.hotelResult[0].RoomDescription = null;
                $scope.TTSSHotel.hotelResult[0].RoomRates = [];
                $scope.TTSSHotel.hotelResult[0].RoomTypeCode = null;
                $scope.TTSSHotel.hotelResult[0].SupplierResortId = null;
                $scope.TTSSHotel.hotelResult[0].TripAdvisorScore = null;
                $scope.TTSSHotel.hotelResult[0].boardRates = null;
                $scope.TTSSHotel.hotelResult[0].checkIn = null;
                $scope.TTSSHotel.hotelResult[0].checkOut = null;
                $scope.TTSSHotel.hotelResult[0].nights = result.data.duration;
                $scope.TTSSHotel.hotelResult[0].roomsInfo = [];
                $scope.TTSSHotel.Livepirce = "Waiting for live price";
            }
        }

        function erroCBForTTSSHotelDetails() {

        }

        function getBoardType(board) {
            if (board == 'Room Only'.toLocaleLowerCase())
                return 'RO';
            else if (board == 'Self Catering'.toLocaleLowerCase())
                return 'SC';
            else if ((board == 'Bed and Breakfast'.toLocaleLowerCase()) || (board == 'Bed & Breakfast'.toLocaleLowerCase()) || (board == 'Bed &amp; Breakfast'.toLocaleLowerCase()))
                return 'BB';
            else if (board == 'Half Board'.toLocaleLowerCase())
                return 'HB';
            else if (board == 'Full Board'.toLocaleLowerCase())
                return 'FB';
            else if (board == 'All Inclusive'.toLocaleLowerCase())
                return 'AL';
        }


        $scope.$watch(function () { return notificationService.applyHotelFilters; }, function (newValue, oldValue) {
            var isFilterApplied = false;
            var filteredData = [];
            if (notificationService.hotelFilter.hotelName != undefined && notificationService.hotelFilter.hotelName != '') {
                filteredData = $filter('filter')($scope.hotelResultsBase, { HotelName: notificationService.hotelFilter.hotelName })
                isFilterApplied = true;
            }
            if (notificationService.hotelFilter.board.filter(function myfunction(el) {
                return el.selected
            }).length > 0) {
                filteredData = $filter('boardTypeFilter')(isFilterApplied ? filteredData : $scope.hotelResultsBase, { filters: notificationService.hotelFilter.board, target: 'BoardType' })
                isFilterApplied = true;
            }
            if (notificationService.hotelFilter.resort.filter(function myfunction(el) {
                return el.selected
            }).length > 0) {
                filteredData = $filter('searchResultsFilter')(isFilterApplied ? filteredData : $scope.hotelResultsBase, { filters: notificationService.hotelFilter.resort, target: 'ResortName' })
                isFilterApplied = true;
            }
            if (notificationService.hotelFilter.hotelRating.filter(function myfunction(el) {
                return el.selected
            }).length > 0) {
                filteredData = $filter('searchResultsFilter')(isFilterApplied ? filteredData : $scope.hotelResultsBase, { filters: notificationService.hotelFilter.hotelRating, target: 'Rating' })
                isFilterApplied = true;
            }
            if (notificationService.hotelFilter.userRating.minValue != 0 || notificationService.hotelFilter.userRating.maxValue != 5) {
                filteredData = $filter('decimalRangeFilter')(isFilterApplied ? filteredData : $scope.hotelResultsBase, { config: notificationService.hotelFilter.userRating, target: 'TripAdvisorScore' })
                isFilterApplied = true;
            }
            if (!isFilterApplied)
                $scope.hotelResults = $scope.hotelResultsBase;
            else
                $scope.hotelResults = filteredData;
        });
    }

})(angular.module('SwordFish'));