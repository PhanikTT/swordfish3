﻿(function (app) {
    'use strict'
    //Directive Creation
    app.directive('hotelResults', hotelResults);

    //Directive
    function hotelResults() {
        return {
            restrict: 'E',
            replace: true,
            templateUrl: './../Scripts/Custom/Hotels/Directives/hotelResults.html'
        };
    }

})(angular.module("SwordFish"));