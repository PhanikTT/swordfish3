﻿(function (app) {
    'use strict'
    //Controller Creation
    app.controller('hotelInfoPopupCtrl', hotelInfoPopupCtrl);

    //DI for Controller
    hotelInfoPopupCtrl.$inject = ['$scope', 'hotelService'];
    function hotelInfoPopupCtrl($scope, hotelService) {
        $scope.showHotelModal = false;
        $scope.showHotelInfo = false;
        $scope.stopHotelPoints = [];
        $scope.getHotelInfo = getHotelInfo;
        $scope.hotelInformation = { noDataFoundMessage: 'Please wait..', loadingImage: './../Assets/loader.gif' };
        $scope.toggleHotelModal = function (modelType, data) {
            if (modelType === 'HotelPRResults')
                $scope.showHotelModal = !$scope.showHotelModal;
            else if (modelType === 'HotelResults')
                $scope.showHotelInfo = !$scope.showHotelInfo;
        };
        $scope.toggleHotelInfoModal = function (TTIcode, HotelCode) {
            $scope.showHotelInfo = !$scope.showHotelInfo;
            getHotelInfo(TTIcode, HotelCode);
        };
        function getHotelInfo(TTIcode, HotelCode) {

            if (HotelCode.split(";")[2] != 0 || (TTIcode != undefined && TTIcode !="")) {
            hotelService.getHotelInfo({ TTICode: TTIcode, SupplierAccommId: HotelCode }, hotelInfoSCB, hotelInfoECB);
        }
         else{
        $scope.hotelInformation.noDataFoundMessage = 'No data found';
        }
        }
        $scope.myInterval = 5000;
        $scope.thumbnailSize = 5;
        $scope.thumbnailPage = 1;
        var slides = $scope.slides = [];
        function hotelInfoSCB(result) {
            $scope.hotelInformation = result.data;
            if ($scope.hotelInformation.Name == undefined || $scope.hotelInformation.Name == null || $scope.hotelInformation.Name == '')
                $scope.hotelInformation.noDataFoundMessage = 'No data found';
            else $scope.hotelInformation.noDataFoundMessage = undefined;
            //$scope.hotelInformation.PAXCount = hotelService.getPAXCount();
            if ($scope.hotelInformation.Images != undefined && $scope.hotelInformation.Images.length != undefined) {
                for (var i = 0; i < $scope.hotelInformation.Images.length; i++) {
                    $scope.addSlide(i);
                }
            }
            $scope.showThumbnails = slides.slice(($scope.thumbnailPage - 1) * $scope.thumbnailSize, $scope.thumbnailPage * $scope.thumbnailSize);
            console.log($scope.hotelInformation);
        }
        function hotelInfoECB(error) {
            $scope.hotelInformation = {};
            $scope.hotelInformation.noDataFoundMessage = 'Unknown error occurred please try again later';
        }

        $scope.addSlide = function (i) {
            var newWidth = 600 + slides.length;
            slides.push({ image: $scope.hotelInformation.Images[i].url, index: i });
        };


        $scope.prevPage = function () {
            if ($scope.thumbnailPage > 1) {
                $scope.thumbnailPage--;
            }
            $scope.showThumbnails = slides.slice(($scope.thumbnailPage - 1) * $scope.thumbnailSize, $scope.thumbnailPage * $scope.thumbnailSize);
        }

        $scope.nextPage = function () {
            if ($scope.thumbnailPage <= Math.floor(slides.length / $scope.thumbnailSize)) {
                $scope.thumbnailPage++;
            }
            $scope.showThumbnails = slides.slice(($scope.thumbnailPage - 1) * $scope.thumbnailSize, $scope.thumbnailPage * $scope.thumbnailSize);
        }
        $scope.setActive = function (idx) {
            $scope.slides[idx].active = true;
        }


    }

})(angular.module('SwordFish'));