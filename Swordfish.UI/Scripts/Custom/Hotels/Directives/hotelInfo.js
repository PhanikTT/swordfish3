﻿(function (app) {
    'use strict'

    app.directive('hotelInfo', function () {
        return {
            restrict: 'E',
            replace: true,
            templateUrl: './../Scripts/Custom/Hotels/Directives/hotelInfo.html',
        };
    })

})(angular.module('SwordFish'));