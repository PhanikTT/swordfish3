﻿(function (app) {
    'use strict'

    //Service Creation
    app.factory('hotelService', hotelService);

    //DI for Service
    hotelService.$inject = ['notificationService', 'apiService', 'basketService'];

    //Hotel Service
    function hotelService(notificationService, apiService, basketService) {
        var service = {
            getHotels: getHotels,
            refreshHotels: refreshHotels,
            getAsyncHotels: getAsyncHotels,
            updateHotelCount: updateHotelCount,
            addHotel: addHotel,
            getHotelInfo: getHotelInfo,
            getPAXCount: getPAXCount,
            removeHotel: removeHotel,
            isAsyncProcess: isAsyncProcess,
            getAjaxCallTime: getAjaxCallTime,
            clearHotelFilters: clearHotelFilters,
            getTTSSHotelDetails: getTTSSHotelDetails
        };
        function isAsyncProcess() {
            return notificationService.hotelAsyncProcess.isAsynchronous;
        }
        function getAjaxCallTime() {
            return notificationService.hotelAsyncProcess.ajaxCallTime;
        }
        function getHotels(successCB, failureCB) {
            if (notificationService.searchModel.OccupancyRooms.length == 1) {
                apiService.post('./../SearchResults/GetResort', notificationService.searchModel, successCB, failureCB);
            }
            else if (notificationService.searchModel.OccupancyRooms.length > 1) {
                apiService.post('./../SearchResults/GetMultiRoom', notificationService.searchModel, successCB, failureCB);
            }
            else {
                $("#loader").hide();
            }
        }
        function getAsyncHotels(successCB, failureCB) {
            service.currentSCB = successCB;
            service.currentFCB = failureCB;
            getAsyncResultSetKey(failureCB);
        }
        function getAsyncResultSetKey(failureCB) {
            if (notificationService.searchModel.OccupancyRooms.length == 1) {
                apiService.post('./../SearchResults/GetAsyncResort', notificationService.searchModel, saveAsyncResultSetKey, failureCB);
            }
            else if (notificationService.searchModel.OccupancyRooms.length > 1) {
                apiService.post('./../SearchResults/GetAsyncMultiRoom', notificationService.searchModel, saveAsyncResultSetKey, failureCB);
            } else {
                $("#loader").hide();
            }
        }
        function saveAsyncResultSetKey(result) {
            notificationService.hotelAsyncProcess.resultSetKey = result.data.ResultSetKey;
            service.getDelta = true;
            getAsyncSearchStatus();
        }
        function saveAsyncResultSets(result) {
            if (notificationService.hotelAsyncProcess.resultSets.length >= 2) {
                var tempResultSets = notificationService.hotelAsyncProcess.resultSets;
                notificationService.hotelAsyncProcess.resultSets = [];
                notificationService.hotelAsyncProcess.resultSets.push(tempResultSets[tempResultSets.length - 1]);
                notificationService.hotelAsyncProcess.resultSets.push(result.data.Results);
            }
            else {
                notificationService.hotelAsyncProcess.resultSets.push(result.data.Results);
            }
            if (service.getDelta)
                getAsyncDeltaResults();
            else {
                if (notificationService.hotelAsyncProcess.resultSets.length > 1) {
                    var startValue = parseInt(notificationService.hotelAsyncProcess.resultSets[notificationService.hotelAsyncProcess.resultSets.length - 1].MaxDelta);
                    var endValue = parseInt(notificationService.hotelAsyncProcess.resultSets[notificationService.hotelAsyncProcess.resultSets.length - 2].MaxDelta);
                    var latestResultCount = 0;
                    if (startValue != undefined && startValue > 0 && endValue != undefined && endValue > 0)
                        latestResultCount = startValue - endValue;
                    service.currentSCB(latestResultCount, ((result.data.Results.Status == "active") ? false : true));
                }
            }
        }
        function getAsyncSearchStatus(successCB, failureCB) {
            if (notificationService.hotelAsyncProcess.resultSetKey != undefined && notificationService.hotelAsyncProcess.resultSetKey != "")
                apiService.post('./../SearchResults/GetSearchStatus', { key: notificationService.hotelAsyncProcess.resultSetKey }, saveAsyncResultSets, service.currentFCB);
            else {
                $("#loader").hide();
            }
        }
        function getAsyncDeltaResults(successCB, failureCB) {
            if (notificationService.hotelAsyncProcess.resultSetKey != undefined && notificationService.hotelAsyncProcess.resultSetKey != "" && notificationService.hotelAsyncProcess.resultSets.length > 0)
                apiService.post('./../SearchResults/GetDeltaResults', { Key: notificationService.hotelAsyncProcess.resultSetKey, StartDelta: notificationService.hotelAsyncProcess.resultSets[notificationService.hotelAsyncProcess.resultSets.length - 1].StartDelta, searchModel: notificationService.searchModel }, service.currentSCB, service.currentFCB);
            else {
                $("#loader").hide();
            }
        }
        function refreshHotels(successCB, failureCB) {
            service.currentSCB = successCB;
            service.currentFCB = failureCB;
            getAsyncDeltaResults(successCB, failureCB);
        }
        function updateHotelCount(successCB, failureCB) {
            service.currentSCB = successCB;
            service.currentFCB = failureCB;
            service.getDelta = false;
            getAsyncSearchStatus();
        }
        function addHotel(hotel) {
            if (notificationService.searchModel.ModifiedCheckIn)
                hotel.checkIn = notificationService.searchModel.ModifiedCheckIn;
            else
                hotel.checkIn = notificationService.searchModel.CheckIn;
            if (notificationService.searchModel.ModifiedCheckOut)
                hotel.checkOut = notificationService.searchModel.ModifiedCheckOut;
            else
                hotel.checkOut = notificationService.searchModel.CheckOut;
            hotel.nights = notificationService.searchModel.Nights;
            hotel.PAXCount = getPAXCount();
            hotel.roomsInfo = [];
            angular.forEach(notificationService.searchModel.OccupancyRooms, function (item) {
                var roomInfo = { adultsCount: 0, childCount: 0, infantCount: 0 };
                roomInfo.adultsCount = parseInt(item.AdultsCount);
                roomInfo.childCount = parseInt(item.ChildCount);
                roomInfo.infantCount = parseInt(item.InfantCount);
                roomInfo.roomType = hotel.RoomDescription;
                roomInfo.boardType = hotel.BoardType;
                hotel.roomsInfo.push(roomInfo);
            });
            notificationService.hotel = hotel;
        }
        //Clear or Reset Hotel Filters
        function clearHotelFilters() {
            notificationService.hotelFilter = {
                board: [],
                hotelRating: [],
                userRating: { minValue: 0, maxValue: 5, rangeFrom: 1, rangeTo: 5 },
                hotelFeatures: [],
                resort: [],
                hotelName : ''
            }
        }

        function getHotelInfo(data, successCB, failureCB) {
            apiService.post('./../SearchResults/GetHotelInfo', data, successCB, failureCB);
        }
        function getPAXCount() {
            return notificationService.getPAXCount();
        }
        function removeHotel() {
            notificationService.hotel = {};
        }

        function getTTSSHotelDetails(TTSSQuoteRefference, successCB, failureCB) {
            apiService.post('./../Home/GetTTSSQuoteInfo', TTSSQuoteRefference, successCB, failureCB);
        }
        return service;
    }

})(angular.module('SwordFish'));