﻿(function (app) {
    'use strict'

    //Service Creation
    app.factory('flightService', flightService);

    //DI for Service
    flightService.$inject = ['notificationService', 'apiService', 'basketService'];

    //Flight Service
    function flightService(notificationService, apiService, basketService) {
        var service = {
            getOneWayFlights: getOneWayFlights,
            getReturnFlights: getReturnFlights,
            getFlightPrice: getFlightPrice,
            addReturnFlight: addReturnFlight,
            addOneWayOutBoundFlight: addOneWayOutBoundFlight,
            addOneWayInBoundFlight: addOneWayInBoundFlight,
            removeReturnFlight: removeReturnFlight,
            removeOutBoundFlight: removeOutBoundFlight,
            removeInBoundFlight: removeInBoundFlight
        };
        function getOneWayFlights(successCB, failureCB) {
            apiService.post('./../SearchResults/GetOneWayFlights', notificationService.searchModel, successCB, failureCB);
        }
        function getReturnFlights(successCB, failureCB) {
            apiService.post('./../SearchResults/GetReturnFlights', notificationService.searchModel, successCB, failureCB);
        }
        function getFlightPrice(data, successCB, failureCB) {
            apiService.post('./../SearchResults/GetPriceForFlightQuoteId', data, successCB, failureCB);
        }
        function addReturnFlight(flight, successCB, failureCB) {
            getFlightPrice({ "quoteId": flight.FlightQuoteId, "sourceUrl": flight.SourceUrl }, successCB, failureCB);
            notificationService.selectedFlight = flight;
        }
        function addOneWayOutBoundFlight(flight, successCB, failureCB) {
            getFlightPrice({ "quoteId": flight.FlightQuoteId, "sourceUrl": flight.SourceUrl }, successCB, failureCB);
            notificationService.selectedFlight = flight;
        }
        function addOneWayInBoundFlight(flight, successCB, failureCB) {
            getFlightPrice({ "quoteId": flight.FlightQuoteId, "sourceUrl": flight.SourceUrl }, successCB, failureCB);
            notificationService.selectedFlight = flight;
        }
        function removeReturnFlight() {
            notificationService.flightsReturn = {};
        }
        function removeOutBoundFlight() {
            notificationService.flightsOneWayOutBound = {};
        }
        function removeInBoundFlight() {
            notificationService.flightsOneWayInBound = {};
        }
        return service;
    }

})(angular.module('SwordFish'));