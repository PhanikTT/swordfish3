﻿(function (app) {
    'use strict'

    app.directive('itineraryInfo', itineraryInfo);

    function itineraryInfo() {
        return {
            restrict: 'E',
            replace: true,
            templateUrl: './../Scripts/Custom/Flights/itineraryInfo.html',
            controller: 'flightCtrl'
        };
    }

})(angular.module('SwordFish'));