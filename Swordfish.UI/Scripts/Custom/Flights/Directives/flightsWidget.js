﻿(function (app) {
    'use strict'
    //Directive Creation
    app.directive('flightsWidget', flightsWidget);

    //Directive
    function flightsWidget() {
        return {
            restrict: 'E',
            replace: true,
            templateUrl: './../Scripts/Custom/Flights/Directives/flightsWidget.html'
        };
    }

})(angular.module("SwordFish"));