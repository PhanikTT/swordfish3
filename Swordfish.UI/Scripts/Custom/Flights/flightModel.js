﻿var ScheduledFlightsList = [];
var EachwayFlightsList = [];
function loadEachwayFlights() {
    EachwayFlightsList.push(
    {
        OutboundAirlineName: 'American Airlines',
        OutboundAirlineSrc: '/Content/Assets/americanairlines.gif',
        OutboundSourceFlightTime: '14:35',
        OutboundDestinFlightTime: '22:35',
        OutboundStopHours: '3',
        OutboundSourceFlightCode: 'LHR',
        OutboundDestinFlightCode: 'IBZ',
        OutboundFlightStops: 'Derect',
        OutboundPackagePrice: '1854',

        InboundAirlineName: 'Air Portugal',
        InboundAirlineSrc: '/Content/Assets/tapportugal.gif',
        InboundSourceFlightTime: '15:35',
        InboundDestinFlightTime: '23:35',
        InboundStopHours: '4:20',
        InboundSourceFlightCode: 'IBZ',
        InboundDestinFlightCode: 'LHR',
        InboundFlightStops: '2 Stops',
        InboundPackagePrice: '1800',
        schedule: 0,
        OutboundStopPoints: [{
            DepartureDatelTime: '12-03-2016 14:35',
            StopAirportCode: 'LHR',
            NextStopAirportCode: 'MIA',
            NextStopArrrivalDateTime: '12-03-2016 15:50'
        },
        {
            DepartureDatelTime: '12-03-2016 16:30',
            StopAirportCode: 'MIA',
            NextStopAirportCode: 'DUB',
            NextStopArrrivalDateTime: '12-03-2016 18:00'
        },
        {
            DepartureDatelTime: '12-03-2016 18:30',
            StopAirportCode: 'DUB',
            NextStopAirportCode: 'IBZ',
            NextStopArrrivalDateTime: '12-03-2016 22:35'
        }],
        InboundStopPoints: [{
            DepartureDatelTime: '17-03-2016 15:35',
            StopAirportCode: 'IBZ',
            NextStopAirportCode: 'DUB',
            NextStopArrrivalDateTime: '17-03-2016 18:00'
        },
        {
            DepartureDatelTime: '17-03-2016 18:30',
            StopAirportCode: 'DUB',
            NextStopAirportCode: 'MIA',
            NextStopArrrivalDateTime: '17-03-2016 19:00'
        },
        {
            DepartureDatelTime: '17-03-2016 20:30',
            StopAirportCode: 'MIA',
            NextStopAirportCode: 'LHR',
            NextStopArrrivalDateTime: '17-03-2016 23:35'
        }]
    });

    EachwayFlightsList.push(
    {
        OutboundAirlineName: 'American Airlines',
        OutboundAirlineSrc: '/Content/Assets/americanairlines.gif',
        OutboundSourceFlightTime: '14:35',
        OutboundDestinFlightTime: '22:35',
        OutboundStopHours: '3',
        OutboundSourceFlightCode: 'LHR',
        OutboundDestinFlightCode: 'IBZ',
        OutboundFlightStops: 'Derect',
        OutboundPackagePrice: '1854',

        InboundAirlineName: 'Air Portugal',
        InboundAirlineSrc: '/Content/Assets/tapportugal.gif',
        InboundSourceFlightTime: '15:35',
        InboundDestinFlightTime: '23:35',
        InboundStopHours: '4:20',
        InboundSourceFlightCode: 'IBZ',
        InboundDestinFlightCode: 'LHR',
        InboundFlightStops: '2 Stops',
        InboundPackagePrice: '1800',
        schedule: 1
    });

    EachwayFlightsList.push(
        {
            OutboundAirlineName: 'American Airlines',
            OutboundAirlineSrc: '/Content/Assets/americanairlines.gif',
            OutboundSourceFlightTime: '14:35',
            OutboundDestinFlightTime: '22:35',
            OutboundStopHours: '3',
            OutboundSourceFlightCode: 'LHR',
            OutboundDestinFlightCode: 'IBZ',
            OutboundFlightStops: 'Derect',
            OutboundPackagePrice: '3854',

            InboundAirlineName: 'Air Portugal',
            InboundAirlineSrc: '/Content/Assets/tapportugal.gif',
            InboundSourceFlightTime: '15:35',
            InboundDestinFlightTime: '23:35',
            InboundStopHours: '4:20',
            InboundSourceFlightCode: 'IBZ',
            InboundDestinFlightCode: 'LHR',
            InboundFlightStops: '2 Stops',
            InboundPackagePrice: '3800',
            schedule: 2
        });
    EachwayFlightsList.push({
        OutboundAirlineName: 'American Airlines',
        OutboundAirlineSrc: '/Content/Assets/americanairlines.gif',
        OutboundSourceFlightTime: '14:35',
        OutboundDestinFlightTime: '22:35',
        OutboundStopHours: '3',
        OutboundSourceFlightCode: 'LHR',
        OutboundDestinFlightCode: 'IBZ',
        OutboundFlightStops: 'Derect',
        OutboundPackagePrice: '2054',

        InboundAirlineName: 'Air Portugal',
        InboundAirlineSrc: '/Content/Assets/tapportugal.gif',
        InboundSourceFlightTime: '15:35',
        InboundDestinFlightTime: '23:35',
        InboundStopHours: '4:20',
        InboundSourceFlightCode: 'IBZ',
        InboundDestinFlightCode: 'LHR',
        InboundFlightStops: '2 Stops',
        InboundPackagePrice: '2000',
        schedule: 3
    });
}

function loadScheduledFlights() {
    ScheduledFlightsList.push(
    {
        AirlineName: 'American Airlines',
        AirlineSrc: '/Content/Assets/americanairlines.gif',

        OutboundSourceFlightTime: '14:35',
        OutboundDestinFlightTime: '22:35',
        OutboundStopHours: '3',
        OutboundSourceFlightCode: 'LHR',
        OutboundDestinFlightCode: 'IBZ',
        OutboundFlightStops: 'Derect',

        InboundSourceFlightTime: '15:35',
        InboundDestinFlightTime: '23:35',
        InboundStopHours: '4:20',
        InboundSourceFlightCode: 'IBZ',
        InboundDestinFlightCode: 'LHR',
        InboundFlightStops: '2 Stops',

        PackagePrice: '1854',
        schedule:0,
        OutboundStopPoints: [{
            DepartureDatelTime: '12-03-2016 14:35',
            StopAirportCode: 'LHR',
            NextStopAirportCode: 'MIA',
            NextStopArrrivalDateTime: '12-03-2016 15:50'
        },
        {
            DepartureDatelTime: '12-03-2016 16:30',
            StopAirportCode: 'MIA',
            NextStopAirportCode: 'DUB',
            NextStopArrrivalDateTime: '12-03-2016 18:00'
        },
        {
            DepartureDatelTime: '12-03-2016 18:30',
            StopAirportCode: 'DUB',
            NextStopAirportCode: 'IBZ',
            NextStopArrrivalDateTime: '12-03-2016 22:35'
        }],
        InboundStopPoints: [{
            DepartureDatelTime: '17-03-2016 15:35',
            StopAirportCode: 'IBZ',
            NextStopAirportCode: 'DUB',
            NextStopArrrivalDateTime: '17-03-2016 18:00'
        },
        {
            DepartureDatelTime: '17-03-2016 18:30',
            StopAirportCode: 'DUB',
            NextStopAirportCode: 'MIA',
            NextStopArrrivalDateTime: '17-03-2016 19:00'
        },
        {
            DepartureDatelTime: '17-03-2016 20:30',
            StopAirportCode: 'MIA',
            NextStopAirportCode: 'LHR',
            NextStopArrrivalDateTime: '17-03-2016 23:35'
        }]
    });

    ScheduledFlightsList.push(
    {
        AirlineName: 'American Airlines',
        AirlineSrc: '/Content/Assets/americanairlines.gif',
        OutboundSourceFlightTime: '14:35',
        OutboundDestinFlightTime: '17:35',
        OutboundStopHours: '3',
        OutboundSourceFlightCode: 'LHR',
        OutboundDestinFlightCode: 'IBZ',
        OutboundFlightStops: 'Derect',
        InboundSourceFlightTime: '17:35',
        InboundDestinFlightTime: '22:35',
        InboundStopHours: '4:20',
        InboundSourceFlightCode: 'IBZ',
        InboundDestinFlightCode: 'LHR',
        InboundFlightStops: '2 Stops',
        PackagePrice: '1854',
        schedule:1
    });

    ScheduledFlightsList.push(
        {
            AirlineName: 'American Airlines',
            AirlineSrc: '/Content/Assets/americanairlines.gif',
            OutboundSourceFlightTime: '14:35',
            OutboundDestinFlightTime: '17:35',
            OutboundStopHours: '3',
            OutboundSourceFlightCode: 'LHR',
            OutboundDestinFlightCode: 'IBZ',
            OutboundFlightStops: 'Derect',
            InboundSourceFlightTime: '17:35',
            InboundDestinFlightTime: '22:35',
            InboundStopHours: '4:20',
            InboundSourceFlightCode: 'IBZ',
            InboundDestinFlightCode: 'LHR',
            InboundFlightStops: '2 Stops',
            PackagePrice: '1854',
            schedule:2
    });
    ScheduledFlightsList.push({
        AirlineName: 'American Airlines',
        AirlineSrc: '/Content/Assets/americanairlines.gif',
        OutboundSourceFlightTime: '14:35',
        OutboundDestinFlightTime: '17:35',
        OutboundStopHours: '3',
        OutboundSourceFlightCode: 'LHR',
        OutboundDestinFlightCode: 'IBZ',
        OutboundFlightStops: 'Derect',
        InboundSourceFlightTime: '17:35',
        InboundDestinFlightTime: '22:35',
        InboundStopHours: '4:20',
        InboundSourceFlightCode: 'IBZ',
        InboundDestinFlightCode: 'LHR',
        InboundFlightStops: '2 Stops',
        PackagePrice: '1854',
        schedule:3
    });
}

loadScheduledFlights();
loadEachwayFlights();
