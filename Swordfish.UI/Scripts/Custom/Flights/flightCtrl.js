﻿(function (app) {
    'use strict'

    //Controller Creation
    app.controller('flightCtrl', flightCtrl);

    //Dependency Injection for Controller
    flightCtrl.$inject = ['$scope', 'flightService', 'notificationService', '$timeout'];

    //Flight Controller
    function flightCtrl($scope, flightService, notificationService, $timeout) {
        $scope.getOneWayFlightData = getOneWayFlightData;
        $scope.getReturnFlightData = getReturnFlightData;
        $scope.addReturnFlight = addReturnFlight;
        $scope.addOneWayOutBoundFlight = addOneWayOutBoundFlight;
        $scope.addOneWayInBoundFlight = addOneWayInBoundFlight;
        $scope.removeReturnFlight = removeReturnFlight;
        $scope.removeOutBoundFlight = removeOutBoundFlight;
        $scope.removeInBoundFlight = removeInBoundFlight;
        $scope.flightStates = notificationService.flightStates;
        var selectedFlightObject = {};
        var inboundOldPrice = 0;
        var outboundOldPrice = 0;
        var roundTripOldPrice = 0;
        function getOneWayFlightData() {
            $("#loader").show();
            getReturnFlightData();
            flightService.getOneWayFlights(successCB, errorCB);
            
        }
        function successCB(result) {
            try {
                $scope.OneWayOutBoundFlights = result.data.OutBoundFlights;
                $scope.OneWayInBoundFlights = result.data.InBoundFlights;
                notificationService.flightFilter.airlines = notificationService.MergeArilines(result.data.SearchAirlines, notificationService.flightFilter.airlines);
                notificationService.flightFilter.deptAirport = notificationService.MergeArilines(result.data.SearchDeptAirlines, notificationService.flightFilter.deptAirport);
                notificationService.flightFilter.outBoundDayAirlines = result.data.OutBoundDayAirlines;
                notificationService.flightFilter.inBoundDayAirlines = result.data.InBoundDayAirlines;
                notificationService.changeSearch = false;
            } catch (e) {
                $("#loader").hide();
                notificationService.changeSearch = false;
            }
        }
        function errorCB(error) {
            $("#loader").hide();
            if (error != null) {
                if (error.status == -1) {
                    notificationService.validationResult = { showPopup: true, errorMsg: ["Some technical error occurred. Please refresh or contact system Administrator..!"], title: 'Flights: Server Error' };
                }
                else {
                    if (error.message != null && error.message != undefined && error.message != '') {
                        notificationService.validationResult = { showPopup: true, errorMsg: [error.message], title: 'Flights: Error' };

                    } else {
                        if (error.status == 500) {
                            notificationService.validationResult = { showPopup: true, errorMsg: ["Cannot reach the server, Please try again."], title: 'Flights: Error' };
                        }
                        else {
                            notificationService.validationResult = { showPopup: true, errorMsg: [error.status, error.statusText], title: 'Flights: Error' };
                        }
                    }
                }
            }
        }
        function getReturnFlightData() {
            $("#loader").show();
            flightService.getReturnFlights(successRCB, errorCB);
        }
        function successRCB(result) {
            $scope.ReturnFlights = result.data.ReturnFlights;
            notificationService.flightFilter.airlines = notificationService.MergeArilines(result.data.SearchAirlines, notificationService.flightFilter.airlines);
            notificationService.flightFilter.deptAirport = notificationService.MergeArilines(result.data.SearchDeptAirlines, notificationService.flightFilter.deptAirport);
            $("#loader").hide();
        }

        function addReturnFlight(flight) {
            $("#loader").show();
            notificationService.flightsOneWayOutBound = {};
            notificationService.flightsOneWayInBound = {};
            selectedFlightObject = flight;
            if (flight.FlightFare == 0) {
                flightService.addReturnFlight(flight, successRPCB, errorPCB);
            }
            else {
                notificationService.flightsReturn = flight;
                $("#loader").hide();
            }
        }
        function addOneWayOutBoundFlight(flight) {
            $("#loader").show();
            notificationService.flightsReturn = {};
            selectedFlightObject = flight;
            if (flight.FlightFare == 0) {
                flightService.addOneWayOutBoundFlight(flight, successOPCB, errorPCB);
            }
            else {
                notificationService.flightsOneWayOutBound = flight;
                $("#loader").hide();
            }
        }
        function addOneWayInBoundFlight(flight) {
            $("#loader").show();
            notificationService.flightsReturn = {};
            selectedFlightObject = flight;
            if (flight.FlightFare == 0) {
                flightService.addOneWayInBoundFlight(flight, successIPCB, errorPCB);
            }
            else {
                notificationService.flightsOneWayInBound = flight;
                $("#loader").hide();
            }
        }
        function removeReturnFlight() {
            flightService.removeReturnFlight();
            resetDataOnFlightDelete("Return");
        }
        function removeOutBoundFlight() {
            flightService.removeOutBoundFlight();
            resetDataOnFlightDelete("Outbound");
        }
        function removeInBoundFlight() {
            flightService.removeInBoundFlight();
            resetDataOnFlightDelete("Inbound");
        }
        function resetDataOnFlightDelete(Direction) {
            notificationService.resetFlights = false;
            notificationService.resetHotels = true;
            if (Direction == "Return") {
                notificationService.flightStates.activeReturnItem = "";
            }
            if (Direction == "Outbound") {
                notificationService.flightStates.activeOutboundItem = "";
            }
            if (Direction == "Inbound") {
                notificationService.flightStates.activeInboundItem = "";
            }
            notificationService.clearNotificationData(false, true);
            notificationService.changeSearch = true;
        }
        $scope.$watch(function () { return notificationService.flightFilter; }, function (newValue, oldValue) { $scope.flightFilter = newValue; })
        $scope.$watch(function () { return notificationService.changeSearch; }, function (newValue, oldValue) {
            if (notificationService.resetFlights) {
                if (newValue) {
                    $scope.OneWayOutBoundFlights = [];
                    $scope.OneWayInBoundFlights = [];
                    $scope.ReturnFlights = [];
                    getOneWayFlightData();
                    notificationService.resetFlights = true;
                }
            }
            $timeout(function () {
                notificationService.changeSearch = false;
                notificationService.resetHotels = true;
            }, 1000)
        }, true);

        function successOPCB(result) {
            if (result.data != undefined && result.data.BasketComponents != undefined && result.data.BasketComponents.length > 0) {
                var livePrice = 0;
                if (result.data.BasketComponents[0].ComponentDetails != undefined && result.data.BasketComponents[0].ComponentDetails.length > 0) {
                    for (var i = 0; i < result.data.BasketComponents[0].ComponentDetails.length; i++) {
                        var detailObj = result.data.BasketComponents[0].ComponentDetails[i];
                        if (detailObj.Type.indexOf('pax') > -1) {
                            livePrice = livePrice + (detailObj.SellingPricePence / 100);
                        }
                    }
                }
                else {
                    notificationService.validationResult = { showPopup: true, errorMsg: ["Unable to fetch the price at this time, provider may be busy or price unavailable. Please try again after sometime."], title: 'Flight pricing request: Error' };
                }
                notificationService.flightsReturn = {};
                notificationService.flightsOneWayOutBound = notificationService.selectedFlight;
                notificationService.flightsOneWayOutBound.sesameBasketDetails = result.data;
                notificationService.flightsOneWayOutBound.FlightFare = livePrice;
                var paxDetails = notificationService.getPAXCount();
                var paxCount = paxDetails.AdultsCount + paxDetails.ChildCount + paxDetails.InfantCount;
                outboundOldPrice = notificationService.flightsOneWayOutBound.PricePerPerson;
                notificationService.flightsOneWayOutBound.PricePerPerson =livePrice / paxCount;
                notificationService.selectedFlight = {};
                if (outboundOldPrice != notificationService.flightsOneWayOutBound.PricePerPerson) {
                    selectedFlightObject.showNewPirce = true;
                    selectedFlightObject.oldPrice = outboundOldPrice;
                }
                else {
                    selectedFlightObject.showNewPirce = false;
                }
                if (outboundOldPrice < notificationService.flightsOneWayOutBound.PricePerPerson) {
                    selectedFlightObject.isPirceIncrease = true;
                }
                else if (outboundOldPrice > notificationService.flightsOneWayOutBound.PricePerPerson) {
                    selectedFlightObject.isPirceIncrease = false;
                }
            }
            else {
                notificationService.validationResult = { showPopup: true, errorMsg: ["Unable to fetch the price at this time, provider may be busy or price unavailable. Please try again after sometime."], title: 'Flight pricing request: Error' };
            }
            $("#loader").hide();
        }
        function successIPCB(result) {
            if (result.data != undefined && result.data.BasketComponents != undefined && result.data.BasketComponents.length > 0) {
                var livePrice = 0;
                if (result.data.BasketComponents[0].ComponentDetails != undefined && result.data.BasketComponents[0].ComponentDetails.length > 0) {
                    for (var i = 0; i < result.data.BasketComponents[0].ComponentDetails.length; i++) {
                        var detailObj = result.data.BasketComponents[0].ComponentDetails[i];
                        if (detailObj.Type.indexOf('pax') > -1) {
                            livePrice = livePrice + (detailObj.SellingPricePence / 100);
                        }
                    }
                }
                else {
                    notificationService.validationResult = { showPopup: true, errorMsg: ["Unable to fetch the price at this time, provider may be busy or price unavailable. Please try again after sometime."], title: 'Flight pricing request: Error' };
                }
                notificationService.flightsReturn = {};
                notificationService.flightsOneWayInBound = notificationService.selectedFlight;
                notificationService.flightsOneWayInBound.sesameBasketDetails = result.data;
                notificationService.flightsOneWayInBound.FlightFare = livePrice;
                var paxDetails = notificationService.getPAXCount();
                var paxCount = paxDetails.AdultsCount + paxDetails.ChildCount + paxDetails.InfantCount;
                inboundOldPrice = notificationService.flightsOneWayInBound.PricePerPerson;
                notificationService.flightsOneWayInBound.PricePerPerson = livePrice / paxCount;
                notificationService.selectedFlight = {};
                if (inboundOldPrice != notificationService.flightsOneWayInBound.PricePerPerson) {
                    selectedFlightObject.showNewPirce = true;
                    selectedFlightObject.oldPrice = inboundOldPrice;
                }
                else {
                    selectedFlightObject.showNewPirce = false;
                }
                if (inboundOldPrice < notificationService.flightsOneWayInBound.PricePerPerson) {
                    selectedFlightObject.isPirceIncrease = true;
                }
                else if (inboundOldPrice > notificationService.flightsOneWayInBound.PricePerPerson) {
                    selectedFlightObject.isPirceIncrease = false;
                }
            }
            else {
                notificationService.validationResult = { showPopup: true, errorMsg: ["Unable to fetch the price at this time, provider may be busy or price unavailable. Please try again after sometime."], title: 'Flight pricing request: Error' };
            }
            $("#loader").hide();
        }

        function successRPCB(result) {
            if (result.data != undefined && result.data.BasketComponents != undefined && result.data.BasketComponents.length > 0) {
                var livePrice = 0;
                if (result.data.BasketComponents[0].ComponentDetails != undefined && result.data.BasketComponents[0].ComponentDetails.length > 0) {
                    for (var i = 0; i < result.data.BasketComponents[0].ComponentDetails.length; i++) {
                        var detailObj = result.data.BasketComponents[0].ComponentDetails[i];
                        if (detailObj.Type.indexOf('pax') > -1) {
                            livePrice = livePrice + (detailObj.SellingPricePence / 100);
                        }
                    }
                }
                else {
                    notificationService.validationResult = { showPopup: true, errorMsg: ["Unable to fetch the price at this time, provider may be busy or price unavailable. Please try again after sometime."], title: 'Flight pricing request: Error' };
                }
                notificationService.flightsOneWayOutBound = {};
                notificationService.flightsOneWayInBound = {};
                notificationService.flightsReturn = notificationService.selectedFlight;
                notificationService.flightsReturn.sesameBasketDetails = result.data;
                notificationService.flightsReturn.FlightFare = livePrice;
                var paxDetails = notificationService.getPAXCount();
                var paxCount = paxDetails.AdultsCount + paxDetails.ChildCount + paxDetails.InfantCount;
                roundTripOldPrice = notificationService.flightsReturn.PricePerPerson;
                notificationService.flightsReturn.PricePerPerson = livePrice / paxCount;
                notificationService.selectedFlight = {};
                if (roundTripOldPrice != notificationService.flightsReturn.PricePerPerson) {
                    selectedFlightObject.showNewPirce = true;
                    selectedFlightObject.oldPrice = roundTripOldPrice;
                }
                else {
                    selectedFlightObject.showNewPirce = false;
                }
                if (roundTripOldPrice < notificationService.flightsReturn.PricePerPerson) {
                    selectedFlightObject.isPirceIncrease = true;
                }
                else if (roundTripOldPrice > notificationService.flightsReturn.PricePerPerson) {
                    selectedFlightObject.isPirceIncrease = false;
                }
            }
            else {
                notificationService.validationResult = { showPopup: true, errorMsg: ["Unable to fetch the price at this time, provider may be busy or price unavailable. Please try again after sometime."], title: 'Flight pricing request: Error' };
            }
            $("#loader").hide();
        }

        function errorPCB(error) {
            notificationService.selectedFlight = {}
            if (error != null) {
                if (error.status == -1) {
                    notificationService.validationResult = { showPopup: true, errorMsg: ["Some technical error occurred. Please refresh or contact system Administrator..!"], title: 'Flights: Server Error' };
                }
                else {
                    if (error.message != null && error.message != undefined && error.message != '') {
                        notificationService.validationResult = { showPopup: true, errorMsg: [error.message], title: 'Flights: Error' };

                    } else {
                        notificationService.validationResult = { showPopup: true, errorMsg: [error.status, error.statusText], title: 'Flights: Error' };
                    }
                }
            }
            $("#loader").hide();
        }
        $scope.flightReverse = false;
        $scope.orderByflightChange = function (orderByFlight) {
            $scope.flightReverse = eval(orderByFlight);
        };
    }

})(angular.module('SwordFish'));