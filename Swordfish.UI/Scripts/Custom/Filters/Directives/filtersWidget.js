﻿(function (app) {
    'use strict'
    //Directive Creation
    app.directive('filtersWidget', filtersWidget);

    //Directive
    function filtersWidget() {
        return {
            restrict: 'E',
            replace: true,
            templateUrl: './../Scripts/Custom/Filters/Directives/filtersWidget.html'
        };
    }

})(angular.module("SwordFish"));