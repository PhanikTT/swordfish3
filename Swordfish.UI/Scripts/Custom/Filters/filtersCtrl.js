﻿(function (app) {
    'use strict'

    //Controller Creation
    app.controller('filtersCtrl', filtersCtrl);

    //DI for Controller
    filtersCtrl.$inject = ['$scope', 'notificationService'];

    //Filters Controller
    function filtersCtrl($scope, notificationService) {
        $scope.BudgetRange = {
            minValue: 0,
            maxValue: 500,
            options: {
                floor: 0,
                ceil: 500,
                step: 1,
                noSwitching: true
            }
        };
        $scope.usrRateSlider = {
            minValue: 1,
            maxValue: 5,
            options: {
                floor: 0,
                ceil: 5,
                step: 1,
                noSwitching: true
            }
        };

        $scope.userRatingSlider = {
            value: 0,
            options: {
                floor: 0,
                ceil: 5,
                step: 0.5,
                precision: 1
            }
        }
        $scope.flightFilter = notificationService.flightFilter;

        $scope.resetFlightFilters = resetFlightFilters;

        $scope.flightFilterUpdate = flightFilterUpdate;

        $scope.hotelFilter = notificationService.hotelFilter;

        $scope.$watch(function () { return notificationService.hotelFilter }, function (newValue, oldValue) {
            $scope.hotelFilter = newValue;
            $scope.hotelName = $scope.hotelFilter.hotelName;
        });
        $scope.$watch(function () { return notificationService.flightFilter }, function (newValue, oldValue) { $scope.flightFilter = newValue });

        $scope.filtersUpdate = filtersUpdate;

        $scope.resetHotelFilters = resetHotelFilters;

        $scope.bestFlightFilter = bestFlightFilter;
        $scope.lateNightFlightFilter = lateNightFlightFilter;
        $scope.isDselected = false;
        $scope.refreshHotelFilters = refreshHotelFilters;
        function bestFlightFilter() {
            $scope.isDselected = true;
            $scope.isBselected = false;
            var obBestFlight = notificationService.flightFilter.outBoundDayAirlines;
            if (obBestFlight != null && obBestFlight.length > 0)
            {
                for (var i = 0; i < obBestFlight.length; i++) {
                    obBestFlight[i].selected = false;
                }
                for (var i = 0; i < obBestFlight.length; i++) {
                    if(obBestFlight[i].day === 'Morning')
                        obBestFlight[i].selected = true;
                    if (obBestFlight[i].day === 'Evening')
                        obBestFlight[i].selected = true;
                }
            }
            notificationService.flightFilter.outBoundDayAirlines = obBestFlight;
            
            var ibBestFlight = notificationService.flightFilter.inBoundDayAirlines;
            if (ibBestFlight != null && ibBestFlight.length > 0) {
                for (var i = 0; i < ibBestFlight.length; i++) {
                    ibBestFlight[i].selected = false;
                }
                for (var i = 0; i < ibBestFlight.length; i++) {
                    if (ibBestFlight[i].day === 'Morning')
                        ibBestFlight[i].selected = true;
                    if (ibBestFlight[i].day === 'Evening')
                        ibBestFlight[i].selected = true;
                }
            }
            notificationService.flightFilter.inBoundDayAirlines = ibBestFlight;
            $scope.flightFilter = notificationService.flightFilter;
        }

        function lateNightFlightFilter() {
            try {
                $("#loader").show();
                $scope.isDselected = true;
                $scope.isBselected = true;
                var obBestFlight = notificationService.flightFilter.outBoundDayAirlines;
                if (obBestFlight != null && obBestFlight.length > 0) {
                    for (var i = 0; i < obBestFlight.length; i++) {
                        obBestFlight[i].selected = false;
                    }
                    for (var i = 0; i < obBestFlight.length; i++) {
                        if (obBestFlight[i].day === 'Afternoon')
                            obBestFlight[i].selected = true;
                        if (obBestFlight[i].day === 'Late Night')
                            obBestFlight[i].selected = true;
                    }
                }
                notificationService.flightFilter.outBoundDayAirlines = obBestFlight;

                var ibBestFlight = notificationService.flightFilter.inBoundDayAirlines;
                if (ibBestFlight != null && ibBestFlight.length > 0) {
                    for (var i = 0; i < ibBestFlight.length; i++) {
                        ibBestFlight[i].selected = false;
                    }
                    for (var i = 0; i < ibBestFlight.length; i++) {
                        if (ibBestFlight[i].day === 'Afternoon')
                            ibBestFlight[i].selected = true;
                        if (ibBestFlight[i].day === 'Late Night')
                            ibBestFlight[i].selected = true;
                    }
                }
                notificationService.flightFilter.inBoundDayAirlines = ibBestFlight;
                $scope.flightFilter = notificationService.flightFilter;
                $("#loader").hide();
            }
            catch(ex)
            {
                $("#loader").hide();
            }
        }

        function filtersUpdate(data) {
            notificationService.hotelFilter.hotelName = $scope.hotelName;
            notificationService.hotelFilter = data;
        }
        function refreshHotelFilters() {
            notificationService.applyHotelFilters = !notificationService.applyHotelFilters;
        }
        function resetHotelFilters(filterSections) {
            $("#loader").show();
            if (typeof (filterSections) === typeof ([])) {
                angular.forEach(filterSections, function (filterSection) {
                    if (filterSection == 'userRating') {
                        $scope.hotelFilter[filterSection].minValue = 0;
                        $scope.hotelFilter[filterSection].maxValue = 5;
                    }
                    else if (filterSection == 'hotelName') {
                        $scope.hotelName = '';
                    }
                    else {
                        angular.forEach($scope.hotelFilter[filterSection], function (filter) {
                            filter.selected = false;
                        });
                    }
                });
                filtersUpdate($scope.hotelFilter);
                refreshHotelFilters();
            }
            else {
                $("#loader").hide();
                return false;
            }
            $("#loader").hide();
        }
        function flightFilterUpdate(data) {
                $("#loader").show();
            notificationService.flightFilter = data;
                $("#loader").hide();
        }

        function resetFlightFilters(filterSections) {
            $("#loader").show();
            if (filterSections[0] === 'outBoundDayAirlines' || filterSections[0] === 'inBoundDayAirlines') {
                $scope.isDselected = false;
                $scope.isBselected = false;
            }
            if (typeof (filterSections) === typeof ([])) {
                angular.forEach(filterSections, function (filterSection) {
                    angular.forEach($scope.flightFilter[filterSection], function (filter) {
                        filter.selected = false;
                    })
                });
                flightFilterUpdate($scope.flightFilter);
            }
            else {
                $("#loader").hide();
                return false;
            }
            $("#loader").hide();
        }
    }

})(angular.module('SwordFish'));