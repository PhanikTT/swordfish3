﻿(function (app) {
    'use strict'

    //Controller Creation
    app.controller('dateRangeCtrl', dateRangeCtrl);

    //DI for Controller
    dateRangeCtrl.$inject = ['$scope', 'notificationService'];

    //DateRange Controller
    function dateRangeCtrl($scope, notificationService) {
        $scope.date = {
            startDate: notificationService.searchModel.CheckIn.replace(' ', ''),
            endDate: notificationService.searchModel.CheckOut.replace(' ', '')
        };
        notificationService.searchModel.CheckIn = notificationService.searchModel.CheckIn.replace(' ', '');
        notificationService.searchModel.CheckOut = notificationService.searchModel.CheckOut.replace(' ', '');
        $scope.singleDate = moment();
        $scope.nights = notificationService.searchModel.Nights;
        $scope.opts = {
            locale: {
                applyClass: 'btn-green',
                applyLabel: "Apply",
                fromLabel: "From",
                format: "DD/MM/YYYY",
                toLabel: "To",
                cancelLabel: 'Cancel',
                customRangeLabel: 'Custom range'
            }
        };
        $scope.min = new Date();
        $scope.DateChange = DateChange;

        function DateChange() {
            notificationService.changeSearch = false;
            $scope.basket.quoteReference = "";
            $scope.basket.quoteHeaderId = "";
            $('#QuoteRef').val('');
            $('#QuoteHeaderId').val('');
            if (new Date($scope.date.startDate).getTime() != new Date(notificationService.searchModel.CheckIn).getTime() && new Date($scope.date.endDate).getTime() != new Date(notificationService.searchModel.CheckOut).getTime()) {
                var checkInDate = new Date($scope.date.startDate);
                var checkOutDate = new Date($scope.date.endDate);
                notificationService.searchModel.CheckIn = checkInDate.getFullYear() + "-" + ("0" + (checkInDate.getMonth() + 1)).slice(-2) + "-" + ("0" + checkInDate.getDate()).slice(-2);
                notificationService.searchModel.CheckOut = checkOutDate.getFullYear() + "-" + ("0" + (checkOutDate.getMonth() + 1)).slice(-2) + "-" + ("0" +checkOutDate.getDate()).slice(-2);
                notificationService.searchModel.Nights = Math.floor((checkOutDate - checkInDate) / 86400000);
                $scope.nights = Math.floor((checkOutDate - checkInDate) / 86400000);
                //Data Cleaning
                notificationService.clearNotificationData(true, true);
                notificationService.resetFlights = true;
                notificationService.resetHotels = true;
                notificationService.changeSearch = true;
            }
        }
    }
    app.controller('singleDateCtrl', singleDateCtrl);

    singleDateCtrl.$inject = ['$scope'];

    function singleDateCtrl($scope) {
        $scope.date = {
            startDate: moment(),
            endDate: moment().add(7, "days")
        };
        $scope.singleDate = moment();
        $scope.nights = moment();
        $scope.opts = {
            locale: {
                applyClass: 'btn-green',
                applyLabel: "Apply",
                fromLabel: "From",
                format: "dd-mm-yyyy",
                toLabel: "To",
                cancelLabel: 'Cancel',
                customRangeLabel: 'Custom range'
            }
        };
        //Watch for date changes
        $scope.$watch('date', function (newDate) {
            $scope.nights = Math.floor(($scope.date.endDate - $scope.date.startDate) / 86400000);
            // console.log($scope.nights);
        }, false);
    }
    app.controller('singleDateTimeCtrl', singleDateTimeCtrl);

    singleDateTimeCtrl.$inject = ['$scope', 'commonService', 'notificationService'];

    function singleDateTimeCtrl($scope, commonService, notificationService) {
        $scope.dateTime = {
            startDate: moment(),
            endDate: moment()
        };
        $scope.singleDate = moment();
        $scope.nights = moment();
        $scope.opts = {
            singleDatePicker: true, 
            timePicker:true,
            locale: {
                applyClass: 'btn-green',
                applyLabel: "Apply",
                fromLabel: "From",
                format: "DD/MM/YYYY hh:mm A",
                toLabel: "To",
                cancelLabel: 'Cancel',
                customRangeLabel: 'Custom range'
            }
        };
        $scope.min = new Date();
        //Watch for date changes
        $scope.$watch('date', function (newDate) {
            if(newDate != undefined)
            $scope.nights = Math.floor(($scope.date.endDate - $scope.date.startDate) / 86400000);
            // console.log($scope.nights);
        }, false);

        $scope.actionReminderModel.addReminder = function () {
            try {
                if (validateActionRemainder().isValid) {
                    $("#loader").show();
                    var data = {};
                    data.Is_deleted = 0;
                    data.TaskPrimaryKeyid = null;
                    data.TaskText = $scope.actionReminderModel.reminderText;
                    data.Type = "Insert";
                    data.WeekDate = null;
                    data.reminder = $scope.actionReminderModel.scheduled ? 1 : 0;
                    data.reminder_status = "new";
                    if ($scope.dateTime._d != undefined)
                        data.reminderdate = $scope.dateTime._d.getDate() + "/" +(parseInt( $scope.dateTime._d.getMonth()) + 1) + "/" + $scope.dateTime._d.getFullYear() + " " + $scope.dateTime._d.getHours() + ":" + $scope.dateTime._d.getMinutes();
                    commonService.InsertUpdateTodaysAction(data, addReminderSuccessCB, addReminderErrorCB);
                }
                else {
                    notificationService.validationResult = { showPopup: !validateActionRemainder().isValid, errorMsg: validateActionRemainder().errorMsg, title: 'Validation Result' };
                }
            }
            catch (ex) {
                $("#loader").hide();
            }
            
        }

        function addReminderSuccessCB(result) {
            if (result.data != null) {
                $("#loader").hide();
                $(".modal").modal('hide')
                notificationService.isTodaysActionsNeedsToBind = true;
            }
        }

        function addReminderErrorCB(ex) {
            
        }

        function validateActionRemainder() {
            var result = {};
            result.isValid = true;
            result.errorMsg = [];
            if ($scope.actionReminderModel.reminderText == "" || $scope.actionReminderModel.reminderText == undefined || $scope.actionReminderModel.reminderText.legnth == 0) {
                result.isValid = false;
                result.errorMsg.push("Please enter today's action message.");
            }
            if ($scope.actionReminderModel.scheduled && !$scope.dateTime.hasOwnProperty("_d")) {
                result.isValid = false;
                result.errorMsg.push("Please select date.");
            }
            return result;
        }
    }
})(angular.module("SwordFish"));