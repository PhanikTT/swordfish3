﻿(function (app) {
    'use strict'

    //Controller Creation
    app.controller('panelCtrl', panelCtrl);

    //DI for Controller
    panelCtrl.$inject = ['$scope'];

    //Panel Controller
    function panelCtrl($scope) {
        $scope.isExpanded = false;
        $scope.toggle = function () {
            $scope.isExpanded = !$scope.isExpanded;
        };
    }

})(angular.module("SwordFish"));