﻿(function (app) {
    'use strict'

    //Controller Creation
    app.controller('searchFiltersCtrl', searchFiltersCtrl);

    //DI for Controller
    searchFiltersCtrl.$inject = ['$scope', 'notificationService'];

    //Search Filters Controller
    function searchFiltersCtrl($scope, notificationService) {
        //TODO :searchFilterValues model need to get data from notification service and assign instead of hardcoding as below
        $scope.searchFilterValues = {
            Source: notificationService.searchModel.SourceFromId,
            Destination: notificationService.searchModel.DestinationToId,
            TravelDates: new Date(notificationService.searchModel.CheckIn),
            Nights: notificationService.searchModel.Nights,
            PAX: notificationService.searchModel.Adults + 'A ' + notificationService.searchModel.Child + 'C ' + notificationService.searchModel.Infants + 'I - ' + notificationService.searchModel.Rooms + 'Rooms',
            Hotel: notificationService.searchModel.Hotel,
            BoardType: notificationService.searchModel.BoardType,
            StarRating: notificationService.searchModel.Rating,
            TtssQuoteRef: notificationService.searchModel.TTSSQuoteReference,
            TtssPrice: notificationService.searchModel.TTSSListedPricePerPerson
        };
        $scope.searchFilterValues.showTTSSRibbon = ($scope.searchFilterValues.TtssQuoteRef == null ? false : true);

        $scope.$watch(function () { return notificationService.searchModel }, function (newValue, oldValue) {
            $scope.modifiedCheckIn = newValue.ModifiedCheckIn;
            $scope.modifiedCheckOut = newValue.ModifiedCheckOut;
        }, true);
        $scope.$watch(function () { return notificationService.changeSearch; }, function (newValue, oldValue) {
            if (newValue) {
                var date = new Date(notificationService.searchModel.CheckIn);
                $scope.searchFilterValues.TravelDates = ((date.getDate() > 9) ? date.getDate() : ("0" + date.getDate())) + "/" + ((date.getMonth() + 1) > 9 ? (date.getMonth() + 1) : ("0" + (date.getMonth() + 1))) + "/" + date.getFullYear();
                $scope.searchFilterValues.Nights = notificationService.searchModel.Nights;
                $scope.searchFilterValues.showTTSSRibbon = false;
            }
        }, true);
    }

})(angular.module("SwordFish"));