﻿(function (app) {

    app.controller('layoutCtrl', layoutCtrl);

    layoutCtrl.$inject = ['$scope', 'notificationService', 'commonService', '$interval'];

    function layoutCtrl($scope, notificationService, commonService, $interval) {
        $scope.actionReminderModel = {}
        $scope.actionReminderModel.actionsList = [];

        $scope.$watch(function () { return notificationService.isTodaysActionsNeedsToBind }, function (newValue) {
            if (notificationService.isTodaysActionsNeedsToBind) {
                getAllActiveActionsText();
            }
        });
        $scope.GetTodaysActionData = getAllActiveActionsText;
        function getAllActiveActionsText() {
            commonService.getAllActiveActionsText(getAllActiveActionsTextSuccessCB, getAllActiveActionsTextErrorCB)
        }
        $scope.GetTodaysActionData();
        function getAllActiveActionsTextSuccessCB(result) {
            try {
                if (result.data != null) {
                    $scope.actionReminderModel.actionsList = result.data;
                    bindNextAction(0);
                }
            }
            catch (ex) {
                $("#loader").hide();
            }

        }
        function getAllActiveActionsTextErrorCB(errorObject) {
            $("#loader").hide();
        }

        $scope.bellringing = false;
        
        $interval(function () {
            if (!$scope.bellringing) {
                for (var index = 0; $scope.actionReminderModel.actionsList.length > index; index++) {
                    var actionItem = $scope.actionReminderModel.actionsList[index];
                    if (new Date(actionItem.remindersdate) <= new Date(new Date().getTime() + 15 * 60000)) {
                        $scope.bellringing = true;
                        $scope.animateClass = "icon icon-bell open-slide bellAnim";
                    }
                }
            }
        }, 10000);

        function bindNextAction(index) {
            $scope.actionReminderModel.actionItem = {};
            if (index == undefined)
                index = 0;
            $scope.actionReminderModel.actionItem.isNextDisabled = false;
            if ($scope.actionReminderModel.actionsList == undefined || $scope.actionReminderModel.actionsList.length == 0) {
                $scope.actionReminderModel.actionItem = null;
                $scope.bellringing = false;
                $scope.animateClass = "icon icon-bell open-slide";
            }
            else {
                for (var count = index; count < $scope.actionReminderModel.actionsList.length; count++) {
                    var actionItem = $scope.actionReminderModel.actionsList[count];

                    if (actionItem.reminder == 1) {
                        $scope.actionReminderModel.actionItem.Is_deleted = actionItem.Is_deleted;
                        $scope.actionReminderModel.actionItem.TaskId = actionItem.TaskId;
                        $scope.actionReminderModel.actionItem.TaskPKeyid = actionItem.TaskPKeyid;
                        $scope.actionReminderModel.actionItem.TaskPrimaryKeyid = actionItem.TaskPrimaryKeyid;
                        $scope.actionReminderModel.actionItem.TaskText = actionItem.TaskText;
                        $scope.actionReminderModel.actionItem.TodaysActionDate = actionItem.TodaysActionDate;
                        $scope.actionReminderModel.actionItem.Type = actionItem.Type;
                        $scope.actionReminderModel.actionItem.UserId = actionItem.UserId;
                        $scope.actionReminderModel.actionItem.reminder = actionItem.reminder;
                        $scope.actionReminderModel.actionItem.reminder_status = actionItem.reminder_status;
                        $scope.actionReminderModel.actionItem.remindersdate = new Date(actionItem.remindersdate);
                        $scope.actionReminderModel.actionItem.index = count + 1;
                        if (count == $scope.actionReminderModel.actionsList.length - 1) {
                            $scope.actionReminderModel.actionItem.isNextDisabled = true;
                        }
                        break;
                    }
                }
            }
            notificationService.isTodaysActionsNeedsToBind = false;
        }

        $scope.actionReminderModel.nextActionItem = function (index) {
            try {
                $("#loader").show();
                bindNextAction(index);
                $("#loader").hide();
            }
            catch (ex) {
                $("#loader").hide();
            }

        }

        $scope.actionReminderModel.disMissActionItem = function (keyValue) {

            try {
                $("#loader").show();
                data = {};
                data.TaskPrimaryKeyid = [];
                data.Is_deleted = 0;
                data.reminder_status = "complete";
                data.TaskPrimaryKeyid.push(keyValue);
                InsertUpdateTodaysAction(data);
                $("#loader").hide();
            }
            catch (ex) {

                $("#loader").hide();
            }
        }

        function InsertUpdateTodaysAction(data) {
            commonService.InsertUpdateTodaysAction(data, disMissActionItemSuccessCB, disMissActionItemErrorCB)
        }

        function disMissActionItemSuccessCB(result) {

            if (result.data != undefined) {
                $("#loader").show();
                $scope.actionReminderModel.actionsList = result.data;
                bindNextAction(0);
                $("#loader").hide();
            }
        }

        function disMissActionItemErrorCB(ex) {
        }
    }

})(angular.module("SwordFish"));