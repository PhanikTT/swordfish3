(function (app) {

    app.controller('rootCtrl', rootCtrl);

    rootCtrl.$inject = ['$scope', 'notificationService', 'commonService', '$interval'];

    function rootCtrl($scope, notificationService, commonService, $interval) {
        $scope.bsExpanded = false;
        $scope.validationResult = {};
        $scope.showCommonPopup = false;
        $scope.showLightBoxPopup = false;
        $scope.currentContext = '';
        $scope.isHotelExpanded = false;
        $scope.isTransfersExpanded = false;
        $scope.toggleHotelExpand = function () {
            $scope.isHotelExpanded = !$scope.isHotelExpanded;
        };
        $scope.toggleTransfersExpand = function () {
            $scope.isTransfersExpanded = !$scope.isTransfersExpanded;
        };

        $scope.$watch(function () { return notificationService.validationResult }, function (newValue) {
            $scope.validationResult = newValue;
            $scope.title = $scope.validationResult.title;
            $scope.showCommonPopup = $scope.validationResult.showPopup == undefined ? false : $scope.validationResult.showPopup;
            var validationMes = '';
            if ($scope.validationResult.errorMsg != undefined) {
                if ($scope.validationResult.errorMsg.length > 0) {
                    validationMes = '<ul>';
                    for (var index = 0; $scope.validationResult.errorMsg.length > index; index++) {
                        validationMes += '<li class="list-style-type:square">' + $scope.validationResult.errorMsg[index] + '</li>';
                    }
                    validationMes += '</ul>';
                }
            }
            $scope.showCommonPopupData = validationMes;
        });
        
        $scope.toggleCommonPopup = function () {
            $scope.showCommonPopup = !$scope.showCommonPopup;
        };
        $scope.bsExpand = function () {
            $scope.bsExpanded = !$scope.bsExpanded;
        };
        $scope.showMenu = false;
        $scope.showNav = function () {
            $scope.showMenu = !$scope.showMenu;
        };
        $scope.disableSticking = false;

        $scope.toggleCustomerNotesModal = function () {
            $scope.showCustomerNotesModal = !$scope.showCustomerNotesModal;
            $scope.csCustomerNotesExpanded = !$scope.csCustomerNotesExpanded;
        };

        $scope.toggleActionReminderModal = function () {
            $scope.showActionReminderModal = !$scope.showActionReminderModal;
        };
        
        $scope.getSearchModel = function (searchData, userInfo, fidelityVal, quoteReference, isAsynchronous, ajaxCallTime) {
            notificationService.searchModel = searchData;
            notificationService.userInfo = userInfo;
            notificationService.fidelityVal = fidelityVal;
            notificationService.quoteReference = quoteReference;
            notificationService.hotelAsyncProcess.isAsynchronous = isAsynchronous;
            notificationService.hotelAsyncProcess.ajaxCallTime = ajaxCallTime;
        }

        // For Action remainder
       
       
        //$scope.actionReminderModel.bellIconCss = 'alaram_shakeNone';
       
       
        //This is not required because of bell shake functionality was stoped.
       
        
       

        
       
    }
    
app.controller('ProgressBar', ProgressBar);


function ProgressBar($scope, $timeout) {
    var flightresultCount = 600;
    var HotelsresultCount = 2500;
    $scope.flgtcountTo = flightresultCount;
    $scope.HotelscountTo = HotelsresultCount;
    $scope.countFrom = 0;
    $timeout(function () {
        $scope.flgtprogressValue = flightresultCount;
        $scope.HotelsprogressValue = HotelsresultCount;
    }, 200);
}

app.filter('range', function () {
    return function (input, total) {
        total = parseInt(total);
        for (var i = 0; i < total; i++)
            input.push(i);
        return input;
    };
});

app.filter('rangeReverse', function () {
    return function (inputArr, totalObj) {
        value = parseInt(totalObj.value);
        reverse = parseInt(totalObj.reverse);
        reversedValue = reverse - value;
        for (var i = 0; i < reversedValue; i++)
            inputArr.push(i);
        return inputArr;
    };
});

app.filter('convertToNumber', function () {
    return function (input) {
        if (input != undefined && !isNaN(input.replace(',', '')))
            return parseInt(input.replace(',', ''), 10);
        else
            return 0;
    };
});
    app.filter('convertToDecimal', function () {
        return function (input) {
            if (input!= undefined && !isNaN(input.replace(',','')))
                return parseFloat(input.replace(',', ''), 10);
            else
                return 0;
        };
});

})(angular.module("SwordFish"));