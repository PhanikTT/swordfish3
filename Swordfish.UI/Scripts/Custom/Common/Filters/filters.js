﻿(function (app) {
    'use strict';

    app.filter('searchResultsFilter', function () {
        return function (data, values) {
            return coreFilter(data, values);
        }
    });
    app.filter('stopsResultsFilter', function () {
        return function (data, values) {
            return stopsFilter(data, values);
        }
    });
    function stopsFilter(data, values) {
        var selectedFilters = [];
        angular.forEach(values.filters, function (item) {
            if (!!item.selected) {
                selectedFilters.push(item.count);
            }
        });

        if (selectedFilters.length === 0) return data;

        var result = [];
        angular.forEach(data, function (item) {
            var stopsCount = item[values.target]
            if (stopsCount >= 2)
                stopsCount = 2;
            if (selectedFilters.indexOf(stopsCount) >= 0) {
                result.push(item);
            }
        });
        return result;
    }

    function coreFilter(data, values) {
        var selectedFilters = [];
        angular.forEach(values.filters, function (item) {
            if (!!item.selected) {
                selectedFilters.push(item.value);
            }
        });

        if (selectedFilters.length === 0) return data;

        var result = [];
        angular.forEach(data, function (item) {
            if (selectedFilters.indexOf(item[values.target]) >= 0) {
                result.push(item);
            }
        });
        return result;
    }

    app.filter('boardTypeFilter', function () {
        return function (data, values) {
            return coreSubFilter(data, values);
        }
    });

    function coreSubFilter(data, values) {
        var selectedFilters = [];
        angular.forEach(values.filters, function (item) {
            if (!!item.selected) {
                selectedFilters.push(item.value);
            }
        });

        if (selectedFilters.length === 0) return data;

        var result = [];
        angular.forEach(data, function (item) {
            if (selectedFilters.indexOf(item[values.target]) >= 0) {
                result.push(item);
            }
            else {
                angular.forEach(item.boardTypes, function (boardType) {
                    if (selectedFilters.indexOf(boardType) >= 0) {
                        result.push(item);
                    }
                });
            }
        });
        return result;
    }

    app.filter('rangeFilter', function () {
        return function (data, range) {
            var values = { filters: [], target: '' };
            if (typeof (range.config.minValue) === typeof (0) && typeof (range.config.maxValue) === typeof (0) && range.config.minValue > 0 && range.config.maxValue > 0) {
                var rangeFrom = 1, rangeTo = 6;
                if (typeof (range.config.rangeFrom) === typeof (0) && typeof (range.config.rangeTo) === typeof (0)) {
                    rangeFrom = range.config.rangeFrom; rangeTo = range.config.rangeTo;
                }
                for (var i = rangeFrom; i < rangeTo + 1; i++) {
                    if (i >= range.config.minValue && i <= range.config.maxValue)
                        values.filters.push({ value: i, selected: true });
                }
                values.target = range.target;
            }
            return coreFilter(data, values);
        }
    });

    app.filter('decimalRangeFilter', function () {
        return function (data, range) {
            var values = { filters: [], target: '' };
            if (typeof (range.config.minValue) === typeof (0) && typeof (range.config.maxValue) === typeof (0) && range.config.minValue >= 0 && range.config.maxValue > 0) {
                var rangeFrom = 0.0, rangeTo = 5.5;
                var decimalMinAdded = false;
                for (var i = rangeFrom; i < rangeTo + 1; i = i + 0.5) {
                    if (i >= range.config.minValue && i <= range.config.maxValue) {
                        values.filters.push({ value: i, selected: true });
                    }
                }
                values.target = range.target;
            }
           
            if (values.filters.length == (rangeTo + 4)) {
                values.filters.push({ value: null, selected: true });
                values.filters.push({ value: undefined, selected: true });
                values.filters.push({ value: '', selected: true });
            }
            return coreFilter(data, values);
        }
    });

    function coreFlightTimeFilter(data, range) {
        var result = [];
        var selCount = 0;
        if (range.config != null && range.config.length > 0) {
            if (range.config[0].selected) {
                selCount++;
                angular.forEach(data, function (item) {
                    var hours = moment.utc(item[range.target]).format('H'); //Local conversion is removed because of bug 2284
                    if (parseInt(hours) >= parseInt(range.config[0].minvalue) && parseInt(hours) < parseInt(range.config[0].maxvalue)) {
                        result.push(item);
                    }
                });
            }
            if (range.config[1].selected) {
                selCount++;
                angular.forEach(data, function (item) {
                    var hours = moment.utc(item[range.target]).format('H');//Local conversion is removed because of bug 2284
                    if (parseInt(hours) >= parseInt(range.config[1].minvalue) && parseInt(hours) < parseInt(range.config[1].maxvalue)) {
                        result.push(item);
                    }
                });
            }

            if (range.config[2].selected) {
                selCount++;
                angular.forEach(data, function (item) {
                    var hours = moment.utc(item[range.target]).format('H');//Local conversion is removed because of bug 2284
                    if (parseInt(hours) >= parseInt(range.config[2].minvalue) && parseInt(hours) < parseInt(range.config[2].maxvalue)) {
                        result.push(item);
                    }
                });
            }

            if (range.config[3].selected) {
                selCount++;
                angular.forEach(data, function (item) {
                    var hours = moment.utc(item[range.target]).format('H');//Local conversion is removed because of bug 2284
                    if (parseInt(hours) >= parseInt(range.config[3].minvalue) && parseInt(hours) < parseInt(range.config[3].maxvalue)) {
                        result.push(item);
                    }
                });
            }
        }
        if (selCount == 0) return data;
        return result;
    }

    app.filter('flightTimeFilter', function () {
        return function (data, range) {
            return coreFlightTimeFilter(data, range);
        }


    });

    app.filter('onlyBooleanValueFilter', [function () {
        return function (input) {
            var filtered = [];
            if (input != "" && input!=undefined) {
                for (var i = 0; i < input.length; i++) {
                    var item = input[i];
                    if (item.general[0].MappingReturnFound == true) {
                        filtered.push(item);
                    }
                }
            }
            return filtered;
        };
    }])
})(angular.module('SwordFish'));