﻿(function (app) {
    'use strict';

    app.factory('validationService', validationService);

    function validationService() {

        var service = {
            validateEmail: validateEmail,
            validateEmails: validateEmails,
            validateMobile: validateMobile,
            validateMobiles: validateMobiles,
            validateNumbers: validateNumbers,
            validateAlphaNumeric: validateAlphaNumeric,
            isNumber: isNumber,
            isNullOrEmpty: isNullOrEmpty,
            validateDateFormat: validateDateFormat,
            isString: isString
        };
        //Email Validation : email is a string
        function validateEmail(email) {
            var regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
            return regex.test(email);
        }
        //Emails Validation : emails is a Javascript Array
        function validateEmails(emails) {
            var validCount = 0;
            for (var i = 0; i < emails.length; i++) {
                var regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
                if (regex.test(emails[i]))
                    validCount++;
            }
            return emails.length == validCount;
        }
        //Mobile Validation : mobile is a string
        function validateMobile(mobile) {
                var regex = /^[0-9-+]+$/;
                return regex.test(mobile);
        }
        //Mobiles Validation : mobiles is a Javascript Array
        function validateMobiles(mobiles) {
            var validCount = 0;
            for (var i = 0; i < mobiles.length; i++) {
                var regex = /^[0-9-+]+$/;
                if (regex.test(mobiles[i]))
                    validCount++;
            }
            return mobiles.length == validCount;
        }

        function validateNumbers(number) {           
            var regex = /^\d+$/;
            return regex.test(number);
        }

        function validateAlphaNumeric() {
            //TODO
        }
        function isNumber($event) {        
            return $event.charCode >= 48 && $event.charCode <= 57;
        }
        function isNullOrEmpty(value) {
            if (value === "" || value === null || typeof value === "undefined") {
                return true;
            }
        }
        function validateDateFormat(date) {
            var regex = /(0[1-9]|[12][0-9]|3[01])[ \.-](0[1-9]|1[012])[ \.-](19|20|)\d\d/;
            return regex.test(date);
        }
        function isString(value)
        {
            var regex = /^[a-zA-Z]*$/;
            return regex.test(value);
        }
        return service;
    }

})(angular.module('SwordFish'));