﻿(function (app, $scope) {
    'use strict'

    //Service Creation
    app.factory('commonService', commonService);
    //DI for Service
    commonService.$inject = ['notificationService', 'apiService'];
    
    //Basket Service
    function commonService(notificationService, apiService) {
        var service = {
            getAllActiveActionsText: getAllActiveActionsText,
            InsertUpdateTodaysAction: InsertUpdateTodaysAction
        };

        function getAllActiveActionsText(successCB, failureCB) {
            apiService.get('../Account/GetTodaysActionData?isOnlyTodayAndPastActions=true', null, successCB, failureCB);
        }
        function InsertUpdateTodaysAction(data,successCB, failureCB) {
            apiService.post('../Account/InsertUpdateTodaysAction', data, successCB, failureCB);
        }
        return service;
    }
})(angular.module('SwordFish'));