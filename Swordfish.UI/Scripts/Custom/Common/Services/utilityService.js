﻿(function (app) {
    'use strict';

    app.factory('utilityService', utilityService);

    function utilityService() {

        //toastr.options = {
        //    "debug": false,
        //    "positionClass": "toast-top-right",
        //    "onclick": null,
        //    "fadeIn": 300,
        //    "fadeOut": 1000,
        //    "timeOut": 3000,
        //    "extendedTimeOut": 1000
        //};

        var service = {
            displaySuccess: displaySuccess,
            displayError: displayError,
            displayWarning: displayWarning,
            displayInfo: displayInfo,
            dateDiff: dateDiff,
            getUTCDate: getUTCDate,
            getLocalDate: getLocalDate,
            numberFromString: numberFromString
        };

        return service;
        function getUTCDate(date, format) {
            var tempDate = moment(date);
            return tempDate.utc().format(format);
        }
        function getLocalDate(date, format) {
            var localTime = moment.utc(date).toDate();
            return moment(localTime).format(format);
        }
        function displaySuccess(message) {
            //toastr.success(message);
        }

        function displayError(error) {
            if (Array.isArray(error)) {
                error.forEach(function (err) {
                    //toastr.error(err);
                });
            } else {
                //toastr.error(error);
            }
        }

        function displayWarning(message) {
            //toastr.warning(message);
        }

        function displayInfo(message) {
            //toastr.info(message);
        }
        function dateDiff(fromDate, toDate) {
            var date1 = new Date(formatDateString(fromDate));
            var date2 = new Date(formatDateString(toDate));
            var timeDiff = Math.abs(date2.getTime() - date1.getTime());
            return Math.ceil(timeDiff / (1000 * 3600 * 24));
        }

        function formatDateString(format) {
            var day = parseInt(format.substring(8, 10));
            var month = parseInt(format.substring(5, 7));
            var year = parseInt(format.substring(0, 4));
            var date = new Date(year, month, day);
            return date;
        }

        function numberFromString(value) {
            var result = '';
            for (var index = 0; index < value.length; index++) {
                if (value[index] >= 0 && value[index] <= 9) {
                    result += value[index] + '';
                }
            }
            return result.replace(' ','');
        }
    }

})(angular.module('SwordFish'));