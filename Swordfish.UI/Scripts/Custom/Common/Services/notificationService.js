﻿(function (app) {
    'use strict';

    app.factory('notificationService', notificationService);

    function notificationService() {

        var service = {
            searchModel: {},
            userInfo: {},
            baggageSearchModel: {},
            transferSearchModel: [],
            customer: {},
            isReSearchQuotes: false,
            fidelityVal: 0,
            flightFilter: {
                departRange: {
                    minValue: 0, maxValue: 24, rangeFrom: 0, rangeTo: 24, options: { floor: 0, ceil: 24, step: 0.5, noSwitching: true }
                },
                arrivalRange: { minValue: 0, maxValue: 24, rangeFrom: 0, rangeTo: 24, options: { floor: 0, ceil: 24, step: 1, noSwitching: true } },
                stops: [
                    { "value": "Direct", "selected": false, "count": 0 },
                    { "value": "1 Stop", "selected": false, "count": 1 },
                    { "value": "2 Stops+", "selected": false, "count": 2 }
                ],
                airlines: [],
                deptAirport: [],
                outBoundDayAirlines: {},
                inBoundDayAirlines: {}
            },
            hotelFilter: {
                hotelName: '',
                board: [],
                hotelRating: [],
                userRating: { minValue: 0, maxValue: 5, rangeFrom: 0.5, rangeTo: 5.5 },
                hotelFeatures: [],
                resort: []
            },
            flightsReturn: {},
            flightsOneWayOutBound: {},
            flightsOneWayInBound: {},
            hotel: {},
            transfers: {},
            baggage: {},
            getPAXCount: getPAXCount,
            getBagsForAirline: getBagsForAirline,
            getTransfersForHotel: getTransfersForHotel,
            isAsynchronous: false,
            validationResult: {},
            hotelAsyncProcess: { isAsynchronous: false, ajaxCallTime: 20000, resultSetKey: "", resultSets: [] },
            isReturnSelected: isReturnSelected,
            isOneWaySelected: isOneWaySelected,
            hotelStates: { activeItem: "" },
            transferStates: { activeItem: "" },
            flightStates: { activeOutboundItem: "", activeInboundItem: "", activeReturnItem: "" },
            bookingModel: bookingModel,
            selectedFlight: {},
            clearNotificationData: clearNotificationData,
            isTodaysActionsNeedsToBind: false,
            applyHotelFilters: false,
            MergeArilines: MergeArilines
        };

        function MergeArilines(sourceArray, destinationArray) {
            if (destinationArray != undefined) {
                for (var index = 0; index < sourceArray.length; index++) {
                    var isAirlineFound = false;
                    for (var arryIndex = 0; arryIndex < destinationArray.length; arryIndex++) {
                        if (destinationArray[arryIndex].value == sourceArray[index].value && destinationArray[arryIndex].price > sourceArray[index].price) {
                            destinationArray[arryIndex] = sourceArray[index];
                            isAirlineFound = true;
                            break;
                        }
                        else if (destinationArray[arryIndex].value == sourceArray[index].value) {
                            isAirlineFound = true;
                            break;
                        }
                    }
                    if (isAirlineFound == false) {
                        destinationArray.push(sourceArray[index]);
                    }
                }
            }
            else {
                destinationArray = sourceArray;
            }
            return destinationArray;
        }

        function isReturnSelected() {
            if (this.flightsReturn != undefined && this.flightsReturn.FlightQuoteId != undefined && this.flightsReturn.FlightQuoteId != '' && this.flightsReturn.FlightQuoteId != null)
                return true;
            else return false;
        }
        function isOneWaySelected() {
            if (this.flightsOneWayOutBound != undefined && this.flightsOneWayOutBound.FlightQuoteId != undefined && this.flightsOneWayOutBound.FlightQuoteId != '' && this.flightsOneWayOutBound.FlightQuoteId != null && this.flightsOneWayInBound != undefined && this.flightsOneWayInBound.FlightQuoteId != undefined && this.flightsOneWayInBound.FlightQuoteId != '' && this.flightsOneWayInBound.FlightQuoteId != null)
                return true;
            else return false;
        }
        function getPAXCount() {
            var PAXCount = { AdultsCount: 0, ChildCount: 0, InfantCount: 0 };
            if (this.searchModel != undefined && this.searchModel != null) {
                angular.forEach(this.searchModel.OccupancyRooms, function (item) {
                    PAXCount.AdultsCount += parseInt(item.AdultsCount)
                    PAXCount.ChildCount += parseInt(item.ChildCount)
                    PAXCount.InfantCount += parseInt(item.InfantCount)
                });
            }
            return PAXCount;
        }

        function getBagsForAirline() {
            if (!angular.isUndefined(this.flightsReturn.$$hashKey)) {
                this.baggageSearchModel = [{
                    "returnQuoteId": this.flightsReturn.FlightQuoteId,
                    "outBoundQuoteId": "",
                    "inBoundQuoteId": "",
                    "flightType": 1,
                    "outBoundFlightFromCode": this.flightsReturn.OutboundFlightFrom,
                    "outBoundFlightFromName": this.flightsReturn.OutboundFlightFromName,
                    "outBoundDepartureTime": this.flightsReturn.OutboundDepartureTime,
                    "outboundArrivalTime": this.flightsReturn.OutboundArrivalTime,
                    "outBoundFlightToCode": this.flightsReturn.OutboundFlightTo,
                    "outBoundFlightToName": this.flightsReturn.OutboundFlightToName,
                    "outBoundAirLine": this.flightsReturn.AirLaneName,
                    "inBoundFlightFromCode": this.flightsReturn.InboundFlightFrom,
                    "inBoundFlightFromName": this.flightsReturn.InboundFlightFromName,
                    "inBoundFlightToCode": this.flightsReturn.InboundFlightTo,
                    "inBoundFlightToName": this.flightsReturn.InboundFlightToName,
                    "inBoundDepartureTime": this.flightsReturn.InboundDepartureTime,
                    "InboundArrivalTime": this.flightsReturn.InboundArrivalTime,
                    "inBoundAirLine": this.flightsReturn.AirLaneName,
                    "noOfAdults": this.searchModel.Adults,
                    "noOfChilds": this.searchModel.Child,
                    "noOfInfants": this.searchModel.Infants,
                    "totalPassangers": this.searchModel.Adults + this.searchModel.Child + this.searchModel.Infants
                }];
            }
            else if (!angular.isUndefined(this.flightsOneWayOutBound.$$hashKey) || !angular.isUndefined(this.flightsOneWayInBound.$$hashKey)) {
                this.baggageSearchModel = [{
                    "returnQuoteId": "",
                    "outBoundQuoteId": this.flightsOneWayOutBound.FlightQuoteId,
                    "inBoundQuoteId": this.flightsOneWayInBound.FlightQuoteId,
                    "flightType": 2,
                    "outBoundFlightFromCode": this.flightsOneWayOutBound.OutboundFlightFrom,
                    "outBoundFlightFromName": this.flightsOneWayOutBound.OutboundFlightFromName,
                    "outBoundDepartureTime": this.flightsOneWayOutBound.OutboundDepartureTime,
                    "outboundArrivalTime": this.flightsOneWayOutBound.OutboundArrivalTime,
                    "outBoundFlightToCode": this.flightsOneWayOutBound.OutboundFlightTo,
                    "outBoundFlightToName": this.flightsOneWayOutBound.OutboundFlightToName,
                    "outBoundAirLine": this.flightsOneWayOutBound.AirLaneName,
                    "inBoundFlightFromCode": this.flightsOneWayInBound.InboundFlightFrom,
                    "inBoundFlightFromName": this.flightsOneWayInBound.InboundFlightFromName,
                    "inBoundFlightToCode": this.flightsOneWayInBound.InboundFlightTo,
                    "inBoundFlightToName": this.flightsOneWayInBound.InboundFlightToName,
                    "inBoundDepartureTime": this.flightsOneWayInBound.InboundDepartureTime,
                    "InboundArrivalTime": this.flightsOneWayInBound.InboundArrivalTime,
                    "inBoundAirLine": this.flightsOneWayInBound.AirLaneName,
                    "noOfAdults": this.searchModel.Adults,
                    "noOfChilds": this.searchModel.Child,
                    "noOfInfants": this.searchModel.Infants,
                    "totalPassangers": this.searchModel.Adults + this.searchModel.Child + this.searchModel.Infants
                }];
            }
        }

        function getTransfersForHotel() {
            if (!angular.isUndefined(this.flightsReturn.$$hashKey)) {
                this.transferSearchModel = [{
                    "airportCode": this.flightsReturn.OutboundFlightTo,
                    "fromType": "IATA",
                    "hotelCode": this.hotel.TTIcode,
                    "toType": "GIATA",
                    "checkIn": this.flightsReturn.OutboundArrivalTime,
                    "checkOut": this.flightsReturn.InboundDepartureTime,
                    "noOfAdults": this.searchModel.Adults,
                    "noOfChilds": this.searchModel.Child,
                    "noOfInfants": this.searchModel.Infants,
                    "totalPassangers": this.searchModel.Adults + this.searchModel.Child + this.searchModel.Infants
                }];
            }
            else if (!angular.isUndefined(this.flightsOneWayOutBound.$$hashKey) && !angular.isUndefined(this.flightsOneWayInBound.$$hashKey)) {
                this.transferSearchModel = [{
                    "airportCode": this.flightsOneWayOutBound.OutboundFlightTo,
                    "fromType": "IATA",
                    "hotelCode": this.hotel.TTIcode,
                    "toType": "GIATA",
                    "checkIn": this.flightsOneWayOutBound.OutboundArrivalTime,
                    "checkOut": this.flightsOneWayInBound.InboundDepartureTime,
                    "noOfAdults": this.searchModel.Adults,
                    "noOfChilds": this.searchModel.Child,
                    "noOfInfants": this.searchModel.Infants,
                    "totalPassangers": this.searchModel.Adults + this.searchModel.Child + this.searchModel.Infants
                }];
            }
        };

        function clearNotificationData(flightsDataAlso, hotelsDataAlso) {
            if (flightsDataAlso) {
                this.flightFilter = {
                    departRange: {
                        minValue: 0, maxValue: 24, rangeFrom: 0, rangeTo: 24, options: { floor: 0, ceil: 24, step: 0.5, noSwitching: true }
                    },
                    arrivalRange: { minValue: 0, maxValue: 24, rangeFrom: 0, rangeTo: 24, options: { floor: 0, ceil: 24, step: 1, noSwitching: true } },
                    stops: [
                        { "value": "Direct", "selected": false, "count": 0 },
                        { "value": "1 Stop", "selected": false, "count": 1 },
                        { "value": "2 Stops+", "selected": false, "count": 2 }
                    ],
                    airlines: [],
                    deptAirport: [],
                    outBoundDayAirlines: {},
                    inBoundDayAirlines: {}
                };
                this.flightsReturn = {};
                this.flightsOneWayOutBound = {};
                this.flightsOneWayInBound = {};
            }
            this.transferSearchModel = [];
            if (hotelsDataAlso) {
                this.hotelFilter = {
                    board: [],
                    hotelRating: [],
                    userRating: { minValue: 0, maxValue: 5, rangeFrom: 1, rangeTo: 5 },
                    hotelFeatures: [],
                    resort: []
                };
                this.baggage = {};

                this.baggageSearchModel = {};
            }
            this.hotel = {};
            this.transfers = {};
        }

        return service;
    }

})(angular.module('SwordFish'));