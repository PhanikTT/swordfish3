﻿(function (app) {
    'use strict';

    app.factory('httpRequestInterceptor', httpRequestInterceptor);

    function httpRequestInterceptor() {
        return {
            request: function (config) {

                config.headers['Accept'] = 'application/json;';
                config.headers['Content-Type'] = 'application/json;';

                return config;
            }
        };
    }

    //app.config(['$httpProvider', function ($httpProvider) {
    //    $httpProvider.interceptors.push('httpRequestInterceptor');
    //}]);

})(angular.module('SwordFish'));