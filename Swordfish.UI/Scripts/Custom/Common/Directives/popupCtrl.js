﻿(function (app) {
    'use strict'

    //Controller Creation
    app.controller('popupCtrl', popupCtrl);

    //DI for Controller
    popupCtrl.$inject = ['$scope'];

    //Popup Controller
    function popupCtrl($scope) {
        $scope.showModal = false;
        $scope.stopPoints = [];
        $scope.toggleModal = function (index, target) {
            $scope.showModal = !$scope.showModal;
            var currentSchedule = ScheduledFlightsList[index];
            $scope.stopPoints = currentSchedule[target]
        };

        $scope.toggleFlight = function (type, target) {
            $scope.showModal = !$scope.showModal;
            $scope.fltWayType = type;
            $scope.stopPoints = target;
            console.log(target);
        };
    }

})(angular.module("SwordFish"));