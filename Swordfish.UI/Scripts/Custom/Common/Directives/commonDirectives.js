﻿(function (app) {
    //Action Reminder
    app.directive('actionReminder', function () {
        return {
            restrict: 'E',
            replace: true,
            templateUrl: './../Scripts/Custom/Common/Directives/actionReminder.html',
            controller: 'rootCtrl'
        };
    });

    //Layout
    app.directive('layout', function () {
        return {
            restrict: 'E',
            replace: true,
            templateUrl: './../Scripts/Custom/Common/Directives/layout.html',
            controller: 'layoutCtrl'
        };
    });

    //Customer Notes
    app.directive('customerNotes', function () {
        return {
            restrict: 'E',
            replace: true,
            templateUrl: './../Scripts/Custom/Common/Directives/customerNotes.html',
            controller: 'rootCtrl'
        };
    });

    //Vertical Menu
    app.directive('verticalMenu', function () {
        return {
            restrict: 'E',
            replace: true,
            templateUrl: './../Scripts/Custom/Common/Directives/verticalMenu.html',
            controller: 'rootCtrl'
        };
    });

    //Search Filters
    app.directive('searchFilters', function () {
        return {
            restrict: 'E',
            replace: true,
            templateUrl: './../Scripts/Custom/Common/Directives/searchFilters.html',
            controller: 'searchFiltersCtrl'
        };
    });

    //Re Search
    app.directive('reSearch', function () {
        return {
            restrict: 'E',
            replace: true,
            templateUrl: './../Scripts/Custom/Common/Directives/reSearch.html'
        };
    });

    app.directive('tripAdvisorScore', function () {
        return {
            transclude: true,
            scope: {
                tripAdvisorScore: "&tripAdvisorScore"
            },
            link: function postLink(scope, element, attrs) {
                if (!isNaN(attrs.tripadvisorscore)) {
                    switch (parseFloat(attrs.tripadvisorscore)) {
                        case 0:
                            element.css("display", "inline-none");
                            element.css("background-position", "0px 100%");
                            break;
                        case 0.5:
                            element.css("display", "inline-block");
                            element.css("background-position", "0px 91%");
                            break;
                        case 1:
                            element.css("display", "inline-block");
                            element.css("background-position", "0px 81%");
                            break;
                        case 1.5:
                            element.css("display", "inline-block");
                            element.css("background-position", "0px 71%");
                            break;
                        case 2:
                            element.css("display", "inline-block");
                            element.css("background-position", "0px 61%");
                            break;
                        case 2.5:
                            element.css("display", "inline-block");
                            element.css("background-position", "0px 51%");
                            break;
                        case 3:
                            element.css("display", "inline-block");
                            element.css("background-position", "0px 41%");
                            break;
                        case 3.5:
                            element.css("display", "inline-block");
                            element.css("background-position", "0px 30%");
                            break;
                        case 4:
                            element.css("display", "inline-block");
                            element.css("background-position", "0px 20%");
                            break;
                        case 4.5:
                            element.css("display", "inline-block");
                            element.css("background-position", "0px 10%");
                            break;
                        case 5:
                            element.css("display", "inline-block");
                            element.css("background-position", "0px 0%");
                            break;
                        default:
                            element.css("display", "none");
                    }
                }
            }
        };
    });

    app.directive('ngRepeatEndWatch', function () {
        return {
            restrict: 'A',
            scope: {},
            link: function (scope, element, attrs) {
                if (attrs.ngRepeat) {
                    if (scope.$parent.$last) {
                        if (attrs.ngRepeatEndWatch !== '') {
                            if (typeof scope.$parent.$parent[attrs.ngRepeatEndWatch] === 'function') {
                                // Executes defined function
                                scope.$parent.$parent[attrs.ngRepeatEndWatch]();
                            } else {
                                // For watcher, if you prefer
                                scope.$parent.$parent[attrs.ngRepeatEndWatch] = true;
                            }
                        } else {
                            // If no value was provided than we will provide one on you controller scope, that you can watch
                            // WARNING: Multiple instances of this directive could yeild unwanted results.
                            scope.$parent.$parent.ngRepeatEnd = true;
                        }
                    }
                } else {
                    throw 'ngRepeatEndWatch: `ngRepeat` Directive required to use this Directive';
                }
            }
        };
    });

    app.directive('scrollPosition', ['$window', function ($window) {
        return {
            scope: {
                basketscroll: '=scrollPosition',
            },
            link: function (scope, element, attrs) {
                var windowEl = angular.element($window);
                var handler = function () {
                    scope.init_top = $('#header').offset().top;
                    scope.scrolled_y = windowEl.scrollTop();
                    scope.basket_y = $('.search-basket-wrp').outerHeight();
                    scope.rescol_y = $('.search-results-area-wrp').outerHeight();
                    scope.window_y = windowEl.height();
                    scope.basketbounds = [0, Math.max(0, scope.rescol_y - scope.basket_y)];
                    scope.basket_offset = Math.min(0, scope.window_y - scope.basket_y);
                    scope.basketMarginTop = (scope.scrolled_y + scope.basket_offset) - scope.init_top;
                    scope.basketscroll = scope.basketMarginTop > scope.basketbounds[1] ? scope.basketbounds[1] : (scope.basketMarginTop < scope.basketbounds[0] ? scope.basketbounds[0] : scope.basketMarginTop);


                }
                windowEl.on('scroll', scope.$apply.bind(scope, handler));
                //handler();
            }
        };
    }]);

    app.directive('scrollPosition2', ['$window', function ($window) {
        return {
            scope: {
                filterscroll2: '=scrollPosition2'
            },
            link: function (scope, element, attrs) {
                var windowEl = angular.element($window);
                var handler = function () {
                    scope.init_top = $('#header').offset().top;
                    scope.scrolled_y = windowEl.scrollTop();
                    scope.filter_y = $('.float-filt').outerHeight();
                    scope.rescol_y = $('.search-results-area-wrp').outerHeight();
                    scope.window_y = windowEl.height();
                    scope.filterbounds = [0, Math.max(0, scope.rescol_y - scope.filter_y)];
                    scope.filter_offset = Math.min(0, scope.window_y - scope.filter_y);
                    scope.filterMarginTop = (scope.scrolled_y + scope.filter_offset) - scope.init_top;
                    scope.filterscroll2 = scope.filterMarginTop > scope.filterbounds[1] ? scope.filterbounds[1] : (scope.filterMarginTop < scope.filterbounds[0] ? scope.filterbounds[0] : scope.filterMarginTop);


                }
                windowEl.on('scroll', scope.$apply.bind(scope, handler));
                // handler();
            }
        };
    }]);

    app.directive('lightBox', function () {
        return {
            restrict: 'E',
            replace: true,
            templateUrl: './../Scripts/Custom/Common/Directives/lightBox.html',
            controller: 'rootCtrl',
            link: function (scope, element, attrs) {

            }
        }
    });

    app.directive('numbersOnly', function () {
        return {
            restrict: 'A',
            link: function (scope, elm, attrs, ctrl) {
                elm.bind("keypress", function (event) {
                    var keyCode = event.which || event.keyCode; 
                    var keyCodeChar = String.fromCharCode(keyCode); 
                    if (!keyCodeChar.match(new RegExp(attrs.numbersOnly, "i")) && keyCode != 8 && keyCode != 9 && keyCode != 37 && keyCode != 39 && keyCode != 46) {
                        event.preventDefault();
                        return false;
                    }

                });
            }
        }
    });
})(angular.module("SwordFish"));