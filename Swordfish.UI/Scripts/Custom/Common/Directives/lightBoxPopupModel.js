﻿(function (app) {
    app.directive('lightboxmodal', function () {
        return {            
            templateUrl: './../Scripts/Custom/Common/Directives/lightBox.html',
            restrict: 'E',
            transclude: true,
            replace: true,
            scope: true,
            link: function postLink(scope, element, attrs) {
                scope.title = attrs.title;

                scope.$watch(attrs.visible, function (value) {
                    if (value == true) {
                        $(element).modal('show');
                        scope.title = attrs.title;
                    }
                    else
                        $(element).modal('hide');
                }, true);

                $(element).on('shown.bs.modal', function () {
                    scope.$apply(function () {
                        scope.$parent[attrs.visible] = true;
                    });
                });

                $(element).on('hidden.bs.modal', function () {
                    scope.$apply(function () {
                        scope.$parent[attrs.visible] = false;
                    });
                });
            }
        };
    });
})(angular.module("SwordFish"));