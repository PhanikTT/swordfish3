﻿(function (app) {
    'use strict'

    //Controller Creation
    app.controller('customerCtrl', customerCtrl);

    //DI for Controller
    customerCtrl.$inject = ['$scope', 'customerService', 'notificationService', '$filter'];

    //Customer Controller
    function customerCtrl($scope, customerService, notificationService, $filter) {
        $scope.CustomerModel = {};
        $scope.CustomerModel.myInterval = -1;
        $scope.CustomerModel.noWrapSlides = false;
        $scope.CustomerModel.ShowProfile = false;
        $scope.CustomerModel.ShowQuotes = false;
        $scope.manualCustomerAddress = false;
        $scope.active = 0;
        $scope.CustomerModel.CustomerQuotesDetails = {};
       
        //$scope.CustomerModel.CustomerQuotesDetails = CustomerQuotesDetailslist;
        $scope.CustomerModel.selectedAddress = {};
        $scope.CustomerModel.CustomerQuotesDetails.CustomerPic = '/Content/Assets/ic-user.png';
        $scope.CustomerModel.getCutomerDetailsWithQueotes = getCutomerDetailsWithQueotes;
        $scope.CustomerModel.isFromCustomer = false; 
        $scope.CustomerModel.title = "Mr";
        $scope.CustomerModel.country = "GB";
        $scope.CustomerModel.countrycode="44"
        $scope.$watch(function () { return notificationService.isReSearchQuotes }, function (newValue) {
            if (newValue) {
                customerService.getCustomerDetailsWithQuotes(notificationService.customer, successCB, failureCB);
            }
        });
        if ((notificationService.searchModel.IncomingPhoneNumber != '' || notificationService.searchModel.CustomerLastName != '' || notificationService.searchModel.CustomerEmail != '') && (notificationService.searchModel.IncomingPhoneNumber != null || notificationService.searchModel.CustomerLastName != null || notificationService.searchModel.CustomerEmail != null)) {
            var customer = {};
            customer.email_address = notificationService.searchModel.CustomerEmail,
            customer.last_name = notificationService.searchModel.CustomerLastName,
            customer.phone_number = notificationService.searchModel.IncomingPhoneNumber,
            customer.first_name = notificationService.searchModel.CustomerFirstname;
            getCutomerDetailsWithQueotes(customer);
        }
        if (notificationService.isReSearchQuotes) {
            getCutomerDetailsWithQueotes(notificationService.customer);

        }

        function getCutomerDetailsWithQueotes(customer) {
            if (customer != undefined) {
                $scope.CustomerModel.isFromCustomer = true;
                customerService.getCustomerDetailsWithQuotes(customer, successCB, failureCB);

            }
        }
        function successCB(result) {
            var customerValues = {};
            if (result.status = 200 && result.data != null) {
                var customerDetailResult = result.data.customerDetails[0];
                $scope.CustomerModel.CustomerQuotesDetails = {
                    CustomerID: customerDetailResult.CustomerId,
                    MemberShipNo: customerDetailResult.MembershipNo,
                    CustomerName: customerDetailResult.first_name + ' ' + customerDetailResult.last_name,
                    CustomerEmail: customerDetailResult.email_address,
                    CustomerMobileNo: customerDetailResult.phone_number,
                    LiveBookings: customerDetailResult.numberOfBookedQuotes,
                    Quotes: customerDetailResult.totalQuotes,
                    IsSms: false,
                    IsEmail: false
                };
                $scope.CustomerModel.ShowProfile = true;
                if ($scope.CustomerModel.isFromCustomer) {
                    customerValues.CustomerID = customerDetailResult.CustomerId;
                    customerValues.MemberShipNo = customerDetailResult.MembershipNo;
                    customerValues.CustomerName = customerDetailResult.first_name + ' ' + customerDetailResult.last_name;
                    customerValues.CustomerEmail = customerDetailResult.email_address;
                    customerValues.CustomerMobileNo = customerDetailResult.phone_number;
                    customerValues.CountryCode = customerDetailResult.CountryCode,
                    customerValues.IsSms = false;
                    customerValues.IsEmail = false;

                    notificationService.customer = customerValues;
                    notificationService.isReSearchQuotes = false;
                    $scope.CustomerModel.isFromCustomer = false;
                }


                var quotes = result.data.customerQuoteDetails;
                var Quoteslist = [];
                for (var index = 0; quotes.length / 3 > index; index++) {
                    var innerArrayForQuotes = [];
                    for (var innerCount = 0; innerCount < 3 && quotes.length > (index * 3 + innerCount) ; innerCount++) {
                        var value = quotes[index * 3 + innerCount];
                        var Quote = {};
                        Quote.BagageFare = 0;
                        Quote.TransfersFare = 0;
                        var Quote = {};
                        Quote.BagageFare = 0;
                        Quote.TransfersFare = 0;
                        Quote.QuoteRefNo = value.QuoteRef;
                        Quote.NoOfAdults = value.NumberOfAdults;
                        Quote.NoOfChildren = value.NumberOfChilds;
                        Quote.NoOfInfants = value.NumberOfInfants;
                        Quote.QuotedDate = new Date(parseInt(value.QuotedDate.substr(6)));
                        Quote.HotelName = value.HotelName.length > 20 ? value.HotelName.substring(0, 20) + ".." : value.HotelName;
                        Quote.HotelFullName = value.HotelName;
                        Quote.HotelCheckIn = new Date(parseInt(value.FormDate.substr(6)));
                        Quote.HotelCheckOut = new Date(parseInt(value.ToDate.substr(6)));
                        Quote.NoOfNights = Math.round(Math.abs((Quote.HotelCheckIn.getTime() - Quote.HotelCheckOut.getTime()) / (24 * 60 * 60 * 1000)));
                        //Quote.HotelFare = value.HotelDetails.HotelFare.toFixed(2);
                        Quote.DepartureLocationCode = value.Source;
                        Quote.ArrivalLocationCode = value.Destination;
                        //Quote.FlightFare = (value.FlightDetails.FlyingDetails[0].TotalSellingPrice + value.FlightDetails.FlyingDetails[1].TotalSellingPrice).toFixed(2);
                        Quote.DepartureDate = new Date(parseInt(value.FormDate.substr(6)));
                        Quote.ArrivalDate = new Date(parseInt(value.ToDate.substr(6)));
                        //if (value.BagsDetails != null && value.BagsDetails.BagsFare != null) { Quote.BagageFare = value.BagsDetails.BagsFare }
                        //if (value.TransfersDetails != null && value.TransfersDetails.TransfersFare !=null) { Quote.TransfersFare = value.TransfersDetails.TransfersFare }
                        Quote.Total = value.TotalPrice.toFixed(2);
                        Quote.PricePerPerson = value.PricePerPerson.toFixed(2);
                        innerArrayForQuotes.push(Quote);
                    }
                    Quoteslist.push(innerArrayForQuotes);
                    $scope.CustomerModel.ShowQuotes = true;
                }
                $scope.CustomerModel.CustomerQuotesDetails.Quoteslist = Quoteslist;
                $("#createCustomer").removeAttr("data-target")
            }
            notificationService.isReSearchQuotes = false;
        }
        function failureCB(error) {

        }


        $scope.clear = function () {
            $scope.CustomerModel.first_name = "";
            $scope.CustomerModel.last_name = "";
            $scope.CustomerModel.emailtype = "";
            $scope.CustomerModel.email_address = "";
            $scope.CustomerModel.phone_number = "";
            $scope.CustomerModel.telephone_number = "0044";
            $scope.CustomerModel.houseno = "";
            $scope.CustomerModel.address_line1 = "";
            $scope.CustomerModel.postcode = "";
            $scope.CustomerModel.address_line2 = "";
            $scope.CustomerModel.city = "";
            $scope.CustomerModel.country = "";
            $scope.CustomerModel.emailcheckbox = "";
            $scope.CustomerModel.countrycode = "44";
        };




        $scope.CustomerModel.createCustomerClick = function () {
            $scope.clear();
            $scope.CustomerCreateForm.$setPristine();
            $scope.CustomerModel.createcustomerwrapper = true;
            $scope.CustomerModel.createcustomerlist = false;
            $scope.CustomerModel.Warningalert = false;
            $scope.CustomerModel.btnUpdate = false;
            $scope.CustomerModel.btnCreate = false;
            $scope.CustomerHeading = "Create a new customer";
            $scope.CustomerModel.country = "GB";
            customerService.gettsOptionsTitles(failure, success);

        }
        function success(result) {
            if (result.status = 200 && result.data != null) {
                var titleList = [];
                $scope.CustomerModel.titleList = result.data;
                $scope.CustomerModel.title = "Mr";
            }
        }
        function failure(error) {
            console.log(error);
            $scope.CustomerModel.hdnMembershipNo = "";
        }

        $scope.CustomerModel.closeCustomerClick = function () {
            $scope.clear();
            $scope.CustomerCreateForm.$setPristine();
            $scope.CustomerModel.createcustomerwrapper = false;
            $scope.CustomerModel.createcustomerlist = false;
            $scope.CustomerModel.Warningalert = false;
            $scope.CustomerModel.btnUpdate = false;
            $scope.CustomerModel.btnCreate = false;
        }

        $scope.function = function () {
            console.log(bookingModel.selectedAddress);
        }
        $scope.CustomerModel.findPostcode = function () {
            var postCode = $scope.CustomerModel.postcode;
            if (postCode == null || postCode == "") {
                $scope.CustomerModel.Warningalert = true;
                $scope.CustomerModel.ErrorMessage = "Please enter valid postcode";
            }
            else {
                $("#loader").show();
                customerService.findPostcode({ "postCode": postCode }, findPostcodeSuccessCB, findPostcodeErrorCB);
            }

            function findPostcodeSuccessCB(result) {
                $("#loader").hide();
                var postCodeList = [];
                $scope.manualCustomerAddress = false;
                $scope.CustomerModel.postCodeList = [];               
                if (result.status == 200 && result.data != "NotFound") {
                    $scope.CustomerModel.Warningalert = false;
                    $scope.CustomerModel.selectedAddress = "";
                    $scope.CustomerModel.postCodeList = JSON.parse(result["data"]).result;
                    $scope.CustomerModel.Error = "";
                }
                else {

                    $scope.CustomerModel.Warningalert = true;
                    $scope.CustomerModel.ErrorMessage = "Please enter valid postcode";
                    $scope.CustomerModel.postCodeList = [];
                    $scope.manualCustomerAddress = false;
                }
            };

            function findPostcodeErrorCB(error) {
                $scope.CustomerModel.Warningalert = true;
                $scope.CustomerModel.ErrorMessage = "Call failed" + error.status + "  " + error.statusText;
            };
        };

        $scope.bindAddresData = function (postCodeData) {
            console.log(postCodeData);
            $scope.CustomerModel.address_line1 = postCodeData.line_1 + " " + postCodeData.line_2 + " " + postCodeData.line_3;
            $scope.CustomerModel.city = postCodeData.post_town;
            $scope.CustomerModel.postcode = postCodeData.postcode;
            $scope.CustomerModel.houseno = postCodeData.building_number;
            $scope.manualCustomerAddress = true;
        }

        $scope.CustomerModel.addCustomer = function () {

            if ($scope.CustomerCreateForm.$valid) {
                $scope.CustomerModel.country = "GB";
                var customerDetail = {
                    title: $scope.CustomerModel.title,
                    email_address: $scope.CustomerModel.email_address,
                    first_name: $scope.CustomerModel.first_name,
                    last_name: $scope.CustomerModel.last_name,
                    pass_word: "",
                    dob: new Date(),
                    created_on: new Date(),
                    emailtype: $scope.CustomerModel.emailtype,                  
                    phone_number: $scope.CustomerModel.phone_number,
                    telephone_number: $scope.CustomerModel.telephone_number,
                    countrycode: $scope.CustomerModel.countrycode,
                    housenumber: $scope.CustomerModel.houseno,
                    address_line1: $scope.CustomerModel.address_line1,
                    address_line2: $scope.CustomerModel.address_line2,
                    city: $scope.CustomerModel.city,
                    country: $scope.CustomerModel.country,
                    postcode: $scope.CustomerModel.postcode,
                    emailconsent: $scope.CustomerModel.emailcheckbox,
                    OptedForSMS: $scope.CustomerModel.optedForSMS,
                    OptedForEmail: $scope.CustomerModel.optedForEmail
                };
                $scope.CustomerModel.addCustomerModel = customerDetail;
                customerService.addCustomer(JSON.stringify(customerDetail), addCustomerSuccessCB, addCustomerErrorCB);
            }
            else {
                $scope.shadow = [];
                $scope.shadow.push('shadow');
            }
        }
        function addCustomerSuccessCB(result) {
            $scope.CustomerModel.Warningalert = false;
            $scope.CustomerModel.successalert = false;
            if (result.data.Message == "t") {
                $scope.CustomerModel.createcustomerwrapper = false;
                $scope.CustomerModel.successalert = true;
                $scope.CustomerModel.SuccessMessage = "Customer Saved Successfully with Membership No : " + result.data.Membershipno + "";
                angular.element('#addCustomer').modal('hide');
                getCutomerDetailsWithQueotes($scope.CustomerModel.addCustomerModel);
            }
            else if (result.data.Message == "f") {
                $scope.CustomerModel.Warningalert = true;
                $scope.CustomerModel.ErrorMessage = "Error in Customer Creation";
            }
            else {
                $scope.CustomerModel.Warningalert = true;
                $scope.CustomerModel.ErrorMessage = result.data.Message;
            }
           

        }
        function addCustomerErrorCB(error) {
            $scope.CustomerModel.Warningalert = true;
            $scope.CustomerModel.ErrorMessage = error;
        }

        // };
        $scope.CustomerModel.UpdateCustomer = function () {
            if ($scope.CustomerCreateForm.$valid) {
                if ($scope.CustomerModel.countrycode == "") {
                    $scope.CustomerModel.countrycode = "44";
                }
                var CustomerDetail = {
                    membershipNo: $scope.CustomerModel.hdnMembershipNo,
                    title: $scope.CustomerModel.title,
                    email_address: $scope.CustomerModel.email_address,
                    first_name: $scope.CustomerModel.first_name,
                    last_name: $scope.CustomerModel.last_name,
                    pass_word: "",
                    dob: new Date(),
                    created_on: new Date(),
                    emailtype: $scope.CustomerModel.emailtype,
                    phone_number:$scope.CustomerModel.phone_number,
                    telephone_number: $scope.CustomerModel.telephone_number,
                    countrycode: $scope.CustomerModel.countrycode,
                    housenumber: $scope.CustomerModel.houseno,
                    address_line1: $scope.CustomerModel.address_line1,
                    address_line2: $scope.CustomerModel.address_line2,
                    city: $scope.CustomerModel.city,
                    country: $scope.CustomerModel.country,
                    postcode: $scope.CustomerModel.postcode,
                    emailconsent: $scope.CustomerModel.emailcheckbox,
                    OptedForSMS: $scope.CustomerModel.optedForSMS,
                    OptedForEmail: $scope.CustomerModel.optedForEmail
                };
                customerService.UpdateCustomer(JSON.stringify(CustomerDetail), UpdateCustomerSuccessCB, UpdateCustomerErrorCB);
            }
            else {
                $scope.shadow = [];
                $scope.shadow.push('shadow');

            }

            function UpdateCustomerSuccessCB(result) {

                if (result.data == "t") {
                    $scope.CustomerModel.createcustomerwrapper = false;
                    $scope.CustomerModel.successalert = true;
                    $scope.CustomerModel.SuccessMessage = "Customer Updated Successfully.";
                    $scope.clear();
                }
                else {

                    $scope.CustomerModel.Warningalert = true;
                    $scope.CustomerModel.ErrorMessage = "Error in Customer Update.";
                    $scope.clear();
                }
            };
            function UpdateCustomerErrorCB(error) {
                $scope.CustomerModel.Warningalert = true;
                $scope.CustomerModel.ErrorMessage = error;
            };
        };


        $scope.CustomerModel.getCustomersOnly = function () {
            var SearchCustomer = {
                MembershipNo: $scope.CustomerModel.search_membership_no,
                first_name: $filter('lowercase')($scope.CustomerModel.search_first_name),
                EmailType: $filter('lowercase')($scope.CustomerModel.search_emailtype),
                email_address: $scope.CustomerModel.search_email_address,
                phone_number: $scope.CustomerModel.search_phone_number,
                Postcode: $scope.CustomerModel.search_postcode
            };
            $scope.CustomerModel.createcustomerwrapper = false;
            $scope.CustomerModel.createcustomerlist = false;
            if (SearchCustomer.MembershipNo == null && SearchCustomer.first_name == null && SearchCustomer.EmailType == null && SearchCustomer.email_address == null && SearchCustomer.phone_number == null && SearchCustomer.Postcode == null) {
                $scope.CustomerModel.successalert = false;
                $scope.CustomerModel.Warningalert = true;
                $scope.CustomerModel.ErrorMessage = "Please enter at least one field.";
            }
            else {
                customerService.getCustomersOnly(JSON.stringify(SearchCustomer), getCustomersOnlySuccessCB, getCustomersOnlyErrorCB);
            }
        }
        function getCustomersOnlySuccessCB(customer) {
            if (customer.data == "Please enter at least one field") {
                var message = customer.data;
            }
            else if (customer.data.length > 0) {
                $scope.CustomerModel.CustomerCount = customer.data.length;
                $scope.CustomerModel.customerData = customer.data;
                $scope.CustomerModel.createcustomerwrapper = false;
                $scope.CustomerModel.createcustomerlist = true;
                $scope.CustomerModel.Warningalert = false;
                $scope.CustomerModel.successalert = false;
                clearSearchCustomer();
            }
            else {
                $scope.CustomerModel.CustomerCount = customer.data.length;
                $scope.CustomerModel.successalert = false;
                $scope.CustomerModel.Warningalert = true;
                $scope.CustomerModel.ErrorMessage = "No record found enter correct data.";
                clearSearchCustomer();
            }
        }
        function getCustomersOnlyErrorCB(error) {

            $scope.CustomerModel.Warningalert = true;
            $scope.CustomerModel.ErrorMessage = "Call failed" + error.status + "  " + error.statusText;
        }


        function clearSearchCustomer() {
            $scope.CustomerModel.search_membership_no = null;
            $scope.CustomerModel.search_first_name = null;
            $scope.CustomerModel.search_emailtype = null;
            $scope.CustomerModel.search_email_address = null;
            $scope.CustomerModel.search_phone_number = null;
            $scope.CustomerModel.search_postcode = null;
        };

        $scope.CustomerModel.selectCustomer = function (cutomer) {
            angular.element('#addCustomer').modal('hide');
            getCutomerDetailsWithQueotes(cutomer);
        }

        $scope.CustomerModel.getCustomerbyMembershipNo = function (customer) {
            $scope.CustomerModel.createcustomerwrapper = true;
            $scope.CustomerModel.createcustomerlist = false;
            $scope.CustomerModel.country = "GB";
            customerService.gettsOptionsTitles(failure, success);
            customerService.getCustomerbyMembershipNo(customer, getCustomerbyMembershipNoSuccessCB, getCustomerbyMembershipNoErrorCB);
            function getCustomerbyMembershipNoSuccessCB(result) {
                if (result.data.length > 0) {
                    $scope.CustomerModel.btnUpdate = true;
                    $scope.CustomerModel.btnCreate = true;
                    $scope.CustomerHeading = "Edit Customer";
                    $scope.CustomerModel.hdnMembershipNo = result.data[0].MembershipNo;
                    $scope.CustomerModel.first_name = result.data[0].first_name;
                    $scope.CustomerModel.last_name = result.data[0].last_name;
                    $scope.CustomerModel.title = result.data[0].Title;
                    $scope.CustomerModel.emailtype = result.data[0].EmailType;
                    $scope.CustomerModel.email_address = result.data[0].email_address;                 
                    $scope.CustomerModel.phone_number = result.data[0].phone_number;
                    $scope.CustomerModel.telephone_number = (result.data[0].telephone_number ==''?"0044":result.data[0].telephone_number);
                    $scope.CustomerModel.countrycode = result.data[0].CountryCode;
                    $scope.CustomerModel.houseno = result.data[0].HouseNumber;
                    $scope.CustomerModel.address_line1 = result.data[0].address_line1;
                    $scope.CustomerModel.postcode = result.data[0].Postcode;
                    $scope.CustomerModel.address_line2 = result.data[0].address_line2;
                    $scope.CustomerModel.city = result.data[0].City;
                    $scope.CustomerModel.country = result.data[0].Country;
                    $scope.CustomerModel.optedForSMS = (result.data[0].OptedForSMS == true ? 1 : 0);
                    $scope.CustomerModel.optedForEmail = (result.data[0].OptedForEmail == true ? 1 : 0);
                    if (result.data[0].emailConsent == true) {
                        $scope.CustomerModel.emailcheckbox = 1;
                    }
                    else {
                        $scope.CustomerModel.emailcheckbox = 0;
                    }
                }
                else {
                    $scope.CustomerModel.Warningalert = true;
                    $scope.CustomerModel.ErrorMessage = "Customer Not Found.";
                }
            };
            function getCustomerbyMembershipNoErrorCB(error) {
                $scope.CustomerModel.Warningalert = true;
                $scope.CustomerModel.ErrorMessage = "Call failed" + error.status + "  " + error.statusText;
            };

        }

        $scope.CustomerModel.CheckEmailExist = function () {
            if ($scope.CustomerModel.email_address.length > 0) {
                var data = { emaiId: $scope.CustomerModel.email_address, memberShipNo: $scope.CustomerModel.hdnMembershipNo };
                customerService.CheckEmailExist(data, CheckEmailExistSuccessCB, CheckEmailExistErrorCB);
            }
            function CheckEmailExistSuccessCB(result) {
                if (result.data == "True") {
                    notificationService.validationResult = { showPopup: true, errorMsg: ["Email address already exists"], title: 'Email: Error' };
                    $scope.CustomerModel.email_address = "";
                }
            };
            function CheckEmailExistErrorCB(error) {
                if (error != null) {
                    if (error.message != null && error.message != undefined && error.message != '') {
                        notificationService.validationResult = { showPopup: true, errorMsg: [error.message], title: 'Email: Error' };
                    }
                    else {
                        notificationService.validationResult = { showPopup: true, errorMsg: [error.status, error.statusText], title: 'Email: Error' };
                    }
                }
            };
        }

        $scope.CustomerModel.CheckPhoneNumberExist = function () {
            if ($scope.CustomerModel.phone_number.length > 0) {
                var data = { phoneNumber: $scope.CustomerModel.phone_number, memberShipNo: $scope.CustomerModel.hdnMembershipNo };
                customerService.CheckPhoneNumberExist(data, CheckPhoneNumberExistSuccessCB, CheckPhoneNumberExistErrorCB);
            }
            function CheckPhoneNumberExistSuccessCB(result) {
                if (result.data == "True") {
                    notificationService.validationResult = { showPopup: true, errorMsg: ["Phone number already exists"], title: 'Email: Error' };
                    $scope.CustomerModel.phone_number = "";
                }
            };
            function CheckPhoneNumberExistErrorCB(error) {
                if (error != null) {
                    if (error.message != null && error.message != undefined && error.message != '') {
                        notificationService.validationResult = { showPopup: true, errorMsg: [error.message], title: 'Phone Number: Error' };
                    }
                    else {
                        notificationService.validationResult = { showPopup: true, errorMsg: [error.status, error.statusText], title: 'Phone Number: Error' };
                    }
                }
            };
        }




        function dateFormatConvertion(dateValue) {
            var value = new Date(dateValue);
            return value.getDate() + "/" + getMonthName(new Date(value.getMonth())) + "/" + value.getFullYear();
        }
        function getMonthName(timing) {
            var month = new Array();
            month[0] = "Jan";
            month[1] = "Feb";
            month[2] = "Mar";
            month[3] = "Apr";
            month[4] = "May";
            month[5] = "Jun";
            month[6] = "Jul";
            month[7] = "Aug";
            month[8] = "Sep";
            month[9] = "Oct";
            month[10] = "Nov";
            month[11] = "Dec";
            var monthName = month[timing.getMonth()];
            return monthName;
        }

    }

})(angular.module('SwordFish'));