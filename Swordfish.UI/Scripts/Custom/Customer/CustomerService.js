﻿(function (app) {
    'use strict'

    //Service Creation
    app.factory('customerService', customerService);

    //DI for Service
    customerService.$inject = ['notificationService', 'apiService'];

    //Customer Service
    function customerService(notificationService, apiService) {
        var service = {
            gettsOptionsTitles:gettsOptionsTitles,
            getCustomerDetailsWithQuotes: getCustomerDetailsWithQuotes,
            addCustomer: addCustomer,
            getCustomersOnly: getCustomersOnly,
            findPostcode: findPostcode,
            getCustomerbyMembershipNo: getCustomerbyMembershipNo,
            UpdateCustomer: UpdateCustomer,
            CheckEmailExist: CheckEmailExist,
            CheckPhoneNumberExist: CheckPhoneNumberExist
        };

        function gettsOptionsTitles(failure, success) {
            //notificationService.customer = customerModel;
            apiService.post('../Customer/GetTsOptionsTitles', failure, success);
        }
        function getCustomerDetailsWithQuotes(customerModel, successCB, failureCB) {
            //notificationService.customer = customerModel;
            apiService.post('../SearchResults/GetCustomerDetailsWithQuotes', customerModel, successCB, failureCB);
        }
        function addCustomer(data, successCB, failureCB) {
            //notificationService.customer = data;
            apiService.post('../Customer/AddCustomer', data, successCB, failureCB);
        }

        function UpdateCustomer(data, successCB, failureCB) {
            //notificationService.customer = data;
            apiService.post('../Customer/UpdateCustomer', data, successCB, failureCB);
        }

        function getCustomersOnly(data, successCB, failureCB) {
            apiService.post('../SearchResults/GetCustomersOnly', data, successCB, failureCB)
        }
        function getCustomerbyMembershipNo(customer, successCB, failureCB) {
            apiService.post('../SearchResults/GetCustomerbyMembershipNo', customer, successCB, failureCB)
        }

        function CheckEmailExist(data, successCB, failureCB) {
            apiService.post('../SearchResults/CheckEmailExist',  data, successCB, failureCB);
        }
        function CheckPhoneNumberExist(data, successCB, failureCB) {
            apiService.post('../SearchResults/CheckPhoneNumberExist', data, successCB, failureCB);
        }
 
        function findPostcode(data, successCB, failureCB) {
            apiService.post('../Booking/GetOpenPostCodeData', data, successCB, failureCB);
        }
        return service;
    }

})(angular.module('SwordFish'));