﻿(function (app) {
    'use strict'
    //Directive Creation
    app.directive('addCustomer', addCustomer);
    //Directive
    function addCustomer() {
        return {
            restrict: 'E',
            replace: true,
            templateUrl: '../SearchResults/CustomerGrid',
            controller: 'customerCtrl'
        };
    }
})(angular.module("SwordFish"));