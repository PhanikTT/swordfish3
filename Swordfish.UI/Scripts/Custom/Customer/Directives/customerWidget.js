﻿(function (app) {
    'use strict'
    //Directive Creation
    app.directive('customerWidget', customerWidget);

    //Directive
    function customerWidget() {
        return {
            restrict: 'E',
            replace: true,
            templateUrl: './../Scripts/Custom/Customer/Directives/customerWidget.html'
        };
    }

})(angular.module("SwordFish"));