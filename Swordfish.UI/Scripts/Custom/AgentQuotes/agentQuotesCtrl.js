﻿(function (app) {
    'use strict'

    //Controller Creation
    app.controller('agentQuotesCtrl', agentQuotesCtrl);

    //DI for Controller
    agentQuotesCtrl.$inject = ['$scope', "agentQuotesService", "notificationService", "utilityService"];
    //AgentQuotes Controller
    function agentQuotesCtrl($scope, agentQuotesService, notificationService, utilityService) {
        var data = {};
        $scope.minDate = new Date();
        $scope.maxDate = new Date(new Date().setYear((new Date().getFullYear()) + 1));
        GetLast24HoursQuotes();

        $scope.getLast24HoursQuotes = GetLast24HoursQuotes;

        $scope.searchQuotes = function () {
            clareBasket();
            data.quoteRef = $scope.quoteRefference;
            data.customerName = $scope.customerName;
            data.fromDate = ($scope.fromDate != undefined && $scope.fromDate != '') ? $scope.fromDate.format("YYYY-MM-DD") : null;
            data.toDate = ($scope.toDate != undefined && $scope.toDate != '') ? $scope.toDate.format("YYYY-MM-DD") : null;
            data.agentId = 0;
            $scope.agentQuotes = [];
            data.isLast24Hours = false;
            if (($scope.quoteRefference != '' && $scope.quoteRefference != undefined) || ($scope.customerName != undefined && $scope.customerName != '') || ($scope.fromDate != undefined && $scope.fromDate != '') || ($scope.toDate != undefined && $scope.toDate != '')) {
                agentQuotesService.GetAgentQuotes(data, agentQuoteSuccessCB, errorCB);
            }
            else {
                GetLast24HoursQuotes();
            }
        }
        //Success Call Back for GetAgentQuotes
        function agentQuoteSuccessCB(result) {
            $scope.totalItems = 0;
            if (result.data != null) {
                $scope.AgentName = result.data.agentQuote;
                $scope.agentQuotes = [];
                if (result.data.model.length > 0) {
                    $scope.agentQuotes = result.data.model;
                    $scope.totalItems = $scope.agentQuotes.length;
                    for (var index = 0; index < $scope.agentQuotes.length; index++) {
                        $scope.agentQuotes[index].FromtDate = new Date(parseInt($scope.agentQuotes[index].FromtDate.substr(6)));
                        $scope.agentQuotes[index].QuotedOn = new Date(parseInt($scope.agentQuotes[index].QuotedOn.substr(6)));
                        $scope.agentQuotes[index].ToDate = new Date(parseInt($scope.agentQuotes[index].ToDate.substr(6)));
                    }
                }
                else {
                    notificationService.validationResult = { showPopup: true, errorMsg: ["Quotes not found for the given search."], title: 'Result' };
                }
                clearFields();
            }
        }

        function errorCB(e) {
            notificationService.validationResult = { showPopup: true, errorMsg: [e], title: 'Error' };
        }
        //Load Quote start
        $scope.loadQuote = function (quoteHeaderID) {
            var data = {};
            data.quoteHeaderID = quoteHeaderID;
            if (quoteHeaderID != undefined && quoteHeaderID != '') {
                //$("#loader").show();
                agentQuotesService.LoadQuote(data, loadQuoteSuccessCB, errorCB);
            }
        }

        //Success Call back for LoadQuote
        function loadQuoteSuccessCB(result) {
            if (result.data != null) {
                $scope.showBasket = true;
                $scope.basket = {};
                $scope.basket.accommodation = {};
                $scope.basket.flightReturn = {};
                $scope.basket.flightInbound = {};
                $scope.basket.flightOutbound = {};
                $scope.basket.Transfer = {};
                $scope.baggage = {};
                $scope.baggage.returnBaggage = [];
                $scope.baggage.inboundBaggage = [];
                $scope.baggage.outboundBaggage = [];
                var quote = result.data;
                $scope.searchModel = quote.searchModel;
                $scope.basket.quoteRefference = quote.quoteHeader.CustomerReferenceCode;
                $scope.basket.adultCount = quote.searchModel.AdultsCount;
                $scope.basket.childCount = quote.searchModel.ChildrenCount;
                $scope.basket.infantCount = quote.searchModel.InfantsCount;

                for (var index = 0; index < quote.flightComponents.length; index++) {
                    //For Flight retrun Amount
                    if (quote.flightComponents[index].ComponentTypeOptionId == 3) {
                        $scope.basket.flightReturnAmount = quote.flightComponents[index].Amount;
                    }
                    //For Flight Inbound Amount
                    if (quote.flightComponents[index].FlightComponentDetails[0].DirectionOptionId == 11) {
                        $scope.basket.flightInboundAmount = quote.flightComponents[index].Amount;
                    }
                    //For Flight Outbound Amount
                    if (quote.flightComponents[index].FlightComponentDetails[0].DirectionOptionId == 12) {
                        $scope.basket.flightOutboundAmount = quote.flightComponents[index].Amount;
                    }
                }
                $scope.basket.flightCost = quote.quoteHeader.FlightCost
                $scope.basket.hotelCost = quote.quoteHeader.HotelCost;
                $scope.basket.baggageCost = quote.quoteHeader.BaggageCost;
                $scope.basket.transfersCost = quote.quoteHeader.TransfersCost;
                $scope.basket.fidelityCost = quote.quoteHeader.FidelityCost;
                $scope.basket.marginFlight = quote.quoteHeader.MarginFlight;
                $scope.basket.marginHotel = quote.quoteHeader.MarginHotel;
                $scope.basket.marginExtras = quote.quoteHeader.MarginExtras;

                //Accommodation data
                $scope.basket.accommodation.hotelName = quote.hotelComponent.HotelName;
                $scope.basket.accommodation.supplierResortName = quote.hotelComponent.SupplierResortName;
                $scope.basket.accommodation.checkInDate = quote.hotelComponent.CheckInDate;
                $scope.basket.accommodation.checkOutDate = quote.hotelComponent.CheckOutDate;
                $scope.basket.accommodation.roomDescription = quote.hotelComponent.hotelComponentsDetails[0].RoomDescription;
                $scope.basket.accommodation.ratePlanCode = quote.hotelComponent.hotelComponentsDetails[0].RatePlanCode;
                $scope.basket.accommodation.nights = quote.searchModel.Nights;
                $scope.basket.accommodation.occupancyRooms = quote.searchModel.OccupancyRooms;

                //Flight Outbound
                $scope.basket.flightOutbounds = quote.flightComponentUI.flightComponentsOutbound;

                //Flight Inbound
                $scope.basket.flightInbounds = quote.flightComponentUI.flightComponentsInbound;

                //Baggage
                for (var index = 0; index < quote.bagsComponents.length; index++) {
                    if (quote.bagsComponents[index].DirectionOptionId == 3)
                        $scope.baggage.returnBaggage.push(quote.bagsComponents[index])
                    else if (quote.bagsComponents[index].DirectionOptionId == 11)
                        $scope.baggage.inboundBaggage.push(quote.bagsComponents[index])
                    else if (quote.bagsComponents[index].DirectionOptionId == 12)
                        $scope.baggage.outboundBaggage.push(quote.bagsComponents[index])

                }

                //Transfers
                var transfers = result.data.transferComponents;
                if (result.data.transferComponents != null && result.data.transferComponents.length > 0) {
                    $scope.basket.Transfer.ProductType = transfers[0].ProductType;
                    $scope.basket.Transfer.TransferTime = transfers[0].TransferTime;
                    $scope.basket.Transfer.MinPax = transfers[0].MinPax;
                    $scope.basket.Transfer.MaxPax = transfers[0].MaxPax;
                    $scope.basket.Transfer.Luggage = transfers[0].Luggage;
                    $scope.basket.Transfer.Units = transfers[0].Units;
                }
            }
        }

        $scope.reSearch = function () {
            var searchModel = $scope.searchModel;
            var startDate = searchModel.startDate;
            var endDate = searchModel.endDate;
            var Occupancy_Rooms = OccupancyRooms(searchModel.OccupancyRooms);
            var model = {
                SourceFrom: searchModel.sourceName,
                SourceFromId: searchModel.sourceCode,
                DestinationTo: searchModel.destinationName,
                DestinationToId: searchModel.destinationCode,
                CheckIn: startDate.substring(0,10),
                CheckOut: endDate.substring(0, 10),
                Nights: 7,
                Rooms: searchModel.RoomNo,
                Adults: searchModel.AdultsCount,
                Child: searchModel.ChildrenCount,
                ResultType: "Package",
                HotelName: 0,
                Hotel: $scope.basket.accommodation.hotelName,
                Transfer: true,
                Rating: searchModel.starRating,
                FlightWay: "TwoWay",
                //BoardType: boardtypes,
                Infants: searchModel.InfantsCount,
                //TTSSQuoteReference: TTSSQuote,
                //TTSSListedPricePerPerson: TTSSPrice,
                //IncomingPhoneNumber: incomingPhoneNumber,
                //CustomerFirstname: customerFirstname,
                //CustomerLastName: customerLastName,
                //CustomerEmail: customerEmail,
                OccupancyRooms: Occupancy_Rooms,
                BoardTypeAI: searchModel.BoardTypeAI,
                BoardTypeFB: searchModel.BoardTypeFB,
                BoardTypeHB: searchModel.BoardTypeHB,
                BoardTypeBB: searchModel.BoardTypeBB,
                BoardTypeSC: searchModel.BoardTypeSC,
                BoardTypeRO: searchModel.BoardTypeRO
            };
            checkandSaveCookie(model);
            agentQuotesService.Serarch(model, SearchSuccesCB, errorCB);
        }

        //Pagenation
        $scope.numPerPage = 5;
        $scope.currentPage = 1;
        $scope.paginate = function (value) {
            var begin, end, index;
            begin = ($scope.currentPage - 1) * $scope.numPerPage;
            end = begin + $scope.numPerPage;
            index = $scope.agentQuotes.indexOf(value);
            return (begin <= index && index < end);
        };

        $scope.resetData = function () {
            $scope.basket = {};
            $scope.showBasket = true;
            clearFields();
            GetLast24HoursQuotes();
        }

        function SearchSuccesCB(result) {
            if (result.data != null) {
                window.location.href = result.data.RedirectUrl;
            }
        }
        function GetLast24HoursQuotes() {
            clareBasket();
            data.quoteRef = null;
            data.customerName = null;
            data.fromDate = null;
            data.toDate = null;
            data.agentId = 0;
            data.isLast24Hours = true;
            $scope.fromDate = "";
            $scope.toDate = "";
            dateValidations();
            agentQuotesService.GetAgentQuotes(data, agentQuoteSuccessCB, errorCB);
        }
        function dateValidations() {
            $(function () {
                $('input[id="txtFromDate"],input[id="txtToDate"]').daterangepicker({
                    singleDatePicker: true,
                    maxDate: 'now',
                    locale: {
                        format: 'DD/MM/YYYY'
                    },
                })
            });
            $('#txtFromDate').val('');
            $('#txtToDate').val('');
        }

        function OccupancyRooms(occupancyRoomsValue) {
            var rooms = [];
            if (occupancyRoomsValue != null && occupancyRoomsValue.length > 0) {
                for (var index = 0; index < occupancyRoomsValue.length; index++) {
                    var occupancy = {};
                    occupancy.AdultsCount = occupancyRoomsValue[index].AdultsCount;
                    rooms.push(occupancy);
                    occupancy.ChildCount = occupancyRoomsValue[index].ChildCount;
                    occupancy.InfantCount = occupancyRoomsValue[index].InfantCount;
                }
            }
            return rooms;
        }

        function clareBasket() {
            $scope.basket = {};
            $scope.showBasket = false;
        }

        function clearFields() {
            $scope.quoteRefference = '';
            $scope.customerName = '';
        }
    }
})(angular.module('SwordFish'));