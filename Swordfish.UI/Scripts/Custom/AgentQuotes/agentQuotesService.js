﻿(function (app) {
    'use strict'

    //Service Creation
    app.factory('agentQuotesService', agentQuotesService);

    //DI for Service
    agentQuotesService.$inject = ['notificationService', 'apiService'];
    function agentQuotesService(notificationService, apiService) {
        var service = {
            GetAgentQuotes: GetAgentQuotes,
            LoadQuote: LoadQuote,
            Serarch: Serarch
        }

        function GetAgentQuotes(data, GetBaggageSlabsSuccessCB, GetBaggageSlabsFailureCB) {
            apiService.post('../Home/AgentQuotes', data, GetBaggageSlabsSuccessCB, GetBaggageSlabsFailureCB);
        }

        function LoadQuote(data, successCB, errorCB) {
            apiService.post('../Home/LoadQuote', data, successCB, errorCB)
        }

        function Serarch(data, successCB, errorCB) {
            apiService.post('../Home/Search', data, successCB, errorCB)
        }
        return service;
    }

})(angular.module('SwordFish'));