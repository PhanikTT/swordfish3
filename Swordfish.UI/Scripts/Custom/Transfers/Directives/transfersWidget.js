﻿(function (app) {
    'use strict'
    //Directive Creation
    app.directive('transfersWidget', transfersWidget);

    //Directive
    function transfersWidget() {
        return {
            restrict: 'E',
            replace: true,
            templateUrl: './../Scripts/Custom/Transfers/Directives/transfersWidget.html'
        };
    }

})(angular.module("SwordFish"));