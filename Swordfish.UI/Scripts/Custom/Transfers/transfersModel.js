﻿var TransfersResultslist = [];

function loadTransfersResults() {
    TransfersResultslist.push(
    {
        TransfersName: 'Private Minibus',
        TransfersImg: '/Assets/taxi.png',
        TransfersDesc: 'Upon arrival into your chosen airport, your goal is to reach your accommodation as quickly and hassle free as possible. Why not arrive in style by booking one of our private transfers (taxi/minibus). No waiting in public airport taxi queues or tiresome bus transfers, get ahead of the hordes and enjoy your first drink before the masses have left the airport.',
        Transfersprice: '64.45',
        TransfersPesrons: '1-4',
        TransfersBags: '1-4',
        TransfersReturn: 'Return',
        Transferspp: '20',
    });
    TransfersResultslist.push(
    {
        TransfersName: 'Private Transfer',
        TransfersImg: '/Assets/taxi.png',
        TransfersDesc: 'Upon arrival into your chosen airport, your goal is to reach your accommodation as quickly and hassle free as possible. Why not arrive in style by booking one of our private transfers (taxi/minibus). No waiting in public airport taxi queues or tiresome bus transfers, get ahead of the hordes and enjoy your first drink before the masses have left the airport.',
        Transfersprice: '64.45',
        TransfersPesrons: '1-4',
        TransfersBags: '1-4',
        TransfersReturn: 'Return',
        Transferspp: '20',
    });
    TransfersResultslist.push(
    {
        TransfersName: 'Premier Service',
        TransfersImg: '/Assets/taxi.png',
        TransfersDesc: 'Upon arrival into your chosen airport, your goal is to reach your accommodation as quickly and hassle free as possible. Why not arrive in style by booking one of our private transfers (taxi/minibus). No waiting in public airport taxi queues or tiresome bus transfers, get ahead of the hordes and enjoy your first drink before the masses have left the airport.',
        Transfersprice: '64.45',
        TransfersPesrons: '1-4',
        TransfersBags: '1-4',
        TransfersReturn: 'Return',
        Transferspp: '20',
    });
    TransfersResultslist.push(
    {
        TransfersName: 'Wheelchair Adapted Vehicle',
        TransfersImg: '/Assets/taxi.png',
        TransfersDesc: 'Upon arrival into your chosen airport, your goal is to reach your accommodation as quickly and hassle free as possible. Why not arrive in style by booking one of our private transfers (taxi/minibus). No waiting in public airport taxi queues or tiresome bus transfers, get ahead of the hordes and enjoy your first drink before the masses have left the airport.',
        Transfersprice: '64.45',
        TransfersPesrons: '1-4',
        TransfersBags: '1-4',
        TransfersReturn: 'Return',
        Transferspp: '20',
    });
    TransfersResultslist.push(
    {
        TransfersName: 'Private Minibus',
        TransfersImg: '/Assets/taxi.png',
        TransfersDesc: 'Upon arrival into your chosen airport, your goal is to reach your accommodation as quickly and hassle free as possible. Why not arrive in style by booking one of our private transfers (taxi/minibus). No waiting in public airport taxi queues or tiresome bus transfers, get ahead of the hordes and enjoy your first drink before the masses have left the airport.',
        Transfersprice: '64.45',
        TransfersPesrons: '1-4',
        TransfersBags: '1-4',
        TransfersReturn: 'Return',
        Transferspp: '20',
    });
}

loadTransfersResults();