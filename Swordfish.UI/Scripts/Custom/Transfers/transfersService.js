﻿(function (app) {
    'use strict'

    //Service Creation
    app.factory('transfersService', transfersService);

    //DI for Service
    transfersService.$inject = ['notificationService', 'apiService', 'basketService'];
    //Transfers Service
    function transfersService(notificationService, apiService, basketService) {

        var service = {
            getTransfers: getTransfers,
            addTransferstobasket: addTransferstobasket,
            removeTransfers: removeTransfers
        };


        function getTransfers(successCB, errorCB) {
            if (notificationService.transferSearchModel.length > 0) {
                var checkin = notificationService.transferSearchModel[0].checkIn;
                checkin = checkin.substring(0, checkin.indexOf('+'));
                var checkout = notificationService.transferSearchModel[0].checkOut;
                checkout = checkout.substring(0, checkout.indexOf('+'));
                var dataTransfers = {
                    FromType: notificationService.transferSearchModel[0].fromType,
                    FromCode: notificationService.transferSearchModel[0].airportCode,
                    ToType: notificationService.transferSearchModel[0].toType,
                    ToCode: notificationService.transferSearchModel[0].hotelCode,
                    CheckIn: checkin,
                    CheckOut: checkout,
                    AdultCount: notificationService.transferSearchModel[0].noOfAdults,
                    ChildrenCount: notificationService.transferSearchModel[0].noOfChilds,
                    InfantCount: notificationService.transferSearchModel[0].noOfInfants,
                    HotelInfoRequest: { TTICode: notificationService.hotel.TTIcode, SupplierAccommId: notificationService.hotel.HotelCode }
                };
                if (dataTransfers.ToCode == undefined) {
                    $("#loader").hide();
                    notificationService.validationResult = { showPopup: true, errorMsg: ['Please select hotel'], title: 'Alert' };
                }
                apiService.post('./../SearchResults/GetDatafromTaxiTransfer', dataTransfers, successCB, errorCB)
            }
            else {
                $("#loader").hide();
                notificationService.validationResult = { showPopup: true, errorMsg: ['Please select flight(s) and hotel'], title: 'Alert' };
            }
        }

        function addTransferstobasket(addTransferstobasket, transfers) {
            var transfersmodifiedobject = [];
            notificationService.transfers.transfers = addTransferstobasket;

            var productid = addTransferstobasket.general[0].productid;
            var travelling = transfers[0].travelling.products;
            var returning = transfers[0].returning.products;
            for (var travel in travelling) {
                if (travelling[travel].general[0].productid === productid) {
                    travelling[travel].general[0].transfertype = "travelling";
                    travelling[travel].general[0].transferTypeOptionId = 24;
                    transfersmodifiedobject.push(travelling[travel]);
                }

            }
            for (var ret in returning) {
                if (returning[ret].general[0].productid === productid) {
                    returning[ret].general[0].transfertype = "returning";
                    returning[ret].general[0].transferTypeOptionId = 25;
                    transfersmodifiedobject.push(returning[ret]);
                }

            }
            notificationService.transfers.transfersdata = transfersmodifiedobject;
        }

        function removeTransfers() {
            notificationService.transfers = {};
        }
        return service;
    }

})(angular.module('SwordFish'));