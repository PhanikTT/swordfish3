﻿(function (app) {
    'use strict'

    //Controller Creation
    app.controller('transfersCtrl', transfersCtrl);

    app.filter('makeUppercase', function () {
        return function (item) {
            return item.toLowerCase();
        };
    });
    //DI for Controller
    transfersCtrl.$inject = ['$scope', 'transfersService', 'notificationService', 'basketService'];

    //Transfer Controller
    function transfersCtrl($scope, transfersService, notificationService, basketService) {
        $scope.states = {};
        $scope.TransfersStatus = "Please click Get Transfers button to get data.";
        $scope.addTransferstobasket = addTransferstobasket;
        $scope.getTransfers = getTransfers;
        $scope.removeTransfers = removeTransfers;
        $scope.orderBy = "0";
        $scope.transferStates = notificationService.transferStates;
        $scope.PAXCount = { AdultsCount: 0, ChildCount: 0, InfantCount: 0 };
        if (notificationService.searchModel != undefined && notificationService.searchModel != null) {
            angular.forEach(notificationService.searchModel.OccupancyRooms, function (item) {
                $scope.PAXCount.AdultsCount += parseInt(item.AdultsCount)
                $scope.PAXCount.ChildCount += parseInt(item.ChildCount)
                $scope.PAXCount.InfantCount += parseInt(item.InfantCount)
            });
        }
        function getTransfers() {
            $("#loader").show();
            notificationService.transferStates.activeItem = "";
            try {
                transfersService.getTransfers(successCB, errorCB);
            } catch (e) {
                $("#loader").hide();
            }
        }
        function successCB(result) {
            try {
                if (result.data.length != 0) {
                    $scope.TransfersStatus = "";
                    $scope.transfers = result.data;
                    $scope.searchdata = result.data[0].search;
                }
                else {
                    $scope.transfers = "";
                    $scope.TransfersStatus = "No transfers found. Please search with another hotel.";
                }
                $scope.isTransfersExpanded = true;
                $("#loader").hide();
            } catch (e) {
                $("#loader").hide();
            }
        }
        function errorCB(error) {
            $("#loader").hide();
            if (error != null) {
                if (error.status == -1) {
                    notificationService.validationResult = { showPopup: true, errorMsg: ["Some technical error occurred. Please refresh or contact system Administrator..!"], title: 'Transfers: Server Error' };
                }
                else {
                    if (error.message != null && error.message != undefined && error.message != '') {
                        notificationService.validationResult = { showPopup: true, errorMsg: [error.message], title: 'Transfers: Error' };

                    } else {
                        notificationService.validationResult = { showPopup: true, errorMsg: [error.status, error.statusText], title: 'Transfers: Error' };
                    }
                }
            }
        }
        function addTransferstobasket(transfer, transfers) {
            transfersService.addTransferstobasket(transfer, transfers);
        }

        $scope.RefreshTransfers = RefreshTransfers;
        function RefreshTransfers() {
            $scope.TransfersStatus = "Loading Transfers...";
            notificationService.getTransfersForHotel();
            getTransfers();
        }

        $scope.reverse = false;
        $scope.orderByChange = function (result) {
            $scope.reverse = eval(result);
        };

        $scope.description = [];
        $scope.showTransfersDetails = function (idx) {
            var pos = $scope.description.indexOf(idx);
            if (pos == -1) {
                $scope.description.push(idx);
            } else {
                $scope.description.splice(pos, 1);
            }
        };

        $scope.selection = [];
        $scope.selectToggle = function (idx) {
            var pos = $scope.selection.indexOf(idx);
            if (pos == -1) {
                $scope.selection.push(idx);
            } else {
                $scope.selection.splice(pos, 1);
            }
        };
        $scope.$watch(function () { return notificationService.changeSearch; }, function (newValue, oldValue) {
            if (newValue) {
                $scope.transfers = [];
                $scope.searchdata = [];
            }
        }, true);
        function removeTransfers() {
            notificationService.transferStates.activeItem = "";
            transfersService.removeTransfers();
        }
    }

})(angular.module('SwordFish'));