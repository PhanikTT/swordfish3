﻿
app.controller("CustomerCtrl", function ($scope, $http, CustomerService) {
    $scope.createcustomerwrapper = false;

    $scope.AddCustomer = function () {
        if ($scope.CustomerCreateForm.$valid) {
            var CustomerDetail = {
                email_address: $scope.email_address,
                first_name: $scope.first_name,
                last_name: $scope.last_name,
                pass_word: "",
                dob: new Date(),
                created_on: new Date(),
                emailtype: $scope.emailtype,
                phone_number: $scope.phone_number,
                address_line1: $scope.address_line1,
                address_line2: $scope.address_line2,
                city: $scope.city,
                country: $scope.country,
                postcode: $scope.postcode
            };
            CustomerService.AddCustomer(CustomerDetail);
        }
        else {
            $scope.shadow = [];
            $scope.shadow.push('shadow');
        }
    };

    $scope.SearchCustomer = function (SearchCustomer) {
        var SearchCustomer = {
            MembershipNo: $scope.search_membership_no,
            last_name: $scope.search_last_name,
            EmailType: $scope.search_emailtype,
            email_address: $scope.search_email_address,
            phone_number: $scope.search_phone_number,
            Postcode: $scope.search_postcode
        };
        $scope.createcustomerwrapper = false;
        $scope.createcustomerlist = false;
        if (SearchCustomer.MembershipNo == null && SearchCustomer.last_name == null && SearchCustomer.EmailType == null && SearchCustomer.email_address == null && SearchCustomer.phone_number == null && SearchCustomer.Postcode == null) {
            var message = "Please enter at least one field.";
            fnOpenAlert(message);
        }
        else {
            $http({
                method: "post",
                url: '../Customer/SearchCustomer',
                data: JSON.stringify(SearchCustomer),
                dataType: "json"
            }).then(function successCallback(customer) {
                if (customer.data == "Please enter at least one field") {
                    var message = customer.data;
                    fnOpenAlert(message);
                }
                else if (customer.data.length > 0) {
                    $scope.CustomerCount = customer.data.length;
                    $scope.customerData = customer.data;
                    $scope.createcustomerwrapper = false;
                    $scope.createcustomerlist = true;

                }
                else {
                    $scope.CustomerCount = customer.data.length;
                    var message = "No record found enter correct data.";
                    fnOpenAlert(message);
                }
            }, function errorCallback(error) {
                var message = "Call failed" + error.status + "  " + error.statusText;
                fnOpenAlert(message);
            });
        }
    };


    $scope.CreateCustomerClick = function () {
        $scope.createcustomerwrapper = true;
        $scope.createcustomerlist = false;
    };


});