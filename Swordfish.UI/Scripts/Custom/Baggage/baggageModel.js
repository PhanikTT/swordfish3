﻿var BaggageSelectionlist = {
    BasketFlights: {
        Baggage1:
            [{
                Bprice: '£0:00', id: '1', name: "1 x Hand Luggage only, 10kg max  (£0.00)"
            }, {
                Bprice: '£47:00', id: '2', name: "1 x One bag, 20kg max  (£47.00)"
            }, {
                Bprice: '£90:00', id: '3', name: "1 x Two bags, 20kg total  (£90.00)"
            }],
        Baggage2:
            [{
                Bprice: '£0:00', id: '1', name: "1 x Hand Luggage only, 10kg max  (£0.00)"
            }, {
                Bprice: '£47:00', id: '2', name: "1 x One bag, 20kg max  (£47.00)"
            }, {
                Bprice: '£90:00', id: '3', name: "1 x Two bags, 20kg total  (£90.00)"
            }],
        Baggage3:
            [{
                Bprice: '£0:00', id: '1', name: "1 x Hand Luggage only, 10kg max  (£0.00)"
            }, {
                Bprice: '£47:00', id: '2', name: "1 x One bag, 20kg max  (£47.00)"
            }, {
                Bprice: '£90:00', id: '3', name: "1 x Two bags, 20kg total  (£90.00)"
            }],

        Baggage4:
            [{
                Bprice: '£0:00', id: '1', name: "1 x Hand Luggage only, 10kg max  (£0.00)"
            }, {
                Bprice: '£47:00', id: '2', name: "1 x One bag, 20kg max  (£47.00)"
            }, {
                Bprice: '£90:00', id: '3', name: "1 x Two bags, 20kg total  (£90.00)"
            }],
        Baggage5:
            [{
                Bprice: '£0:00', id: '1', name: "1 x Hand Luggage only, 10kg max  (£0.00)"
            }, {
                Bprice: '£47:00', id: '2', name: "1 x One bag, 20kg max  (£47.00)"
            }, {
                Bprice: '£90:00', id: '3', name: "1 x Two bags, 20kg total  (£90.00)"
            }],
        Baggage6:
            [{
                Bprice: '£0:00', id: '1', name: "1 x Hand Luggage only, 10kg max  (£0.00)"
            }, {
                Bprice: '£47:00', id: '2', name: "1 x One bag, 20kg max  (£47.00)"
            }, {
                Bprice: '£90:00', id: '3', name: "1 x Two bags, 20kg total  (£90.00)"
            }],
    }
};