﻿(function (app) {
    'use strict'

    //Controller Creation
    app.controller('baggageCtrl', baggageCtrl);

    //DI for Controller
    baggageCtrl.$inject = ['$scope', "baggageService", "notificationService"];
    var slabs = null;
    //Baggage Controller
    function baggageCtrl($scope, baggageService, notificationService) {
        $scope.baggage = {};
        resetBaggageData();
        $scope.baggage.isTowayOrOutboundBaggageErrorOccured;
        $scope.baggage.towayOrOutboundBaggageErrorMsg = null;
        $scope.baggage.InboundBaggageErrorMsg = null;
        $scope.baggage.isInboundBaggageErrorOccured;
        $scope.baggage.showInBoundBagSlabs = false;
        $scope.baggage.showOutBoundBagSlabs = false;
        $scope.baggage.towayOrOutboundBaggageTextIndex = [];
        $scope.baggage.inboundBaggageTextIndex = [];
        var isPassengersBind = false;
        $scope.$watch(function () { return notificationService.flightsReturn.sesameBasketDetails }, function (newValue) {
            if (newValue != undefined) {
                getBagages();
                $scope.baggage.showOutBoundBagSlabs = true;

            }
            else {
                $scope.baggage.showOutBoundBagSlabs = false;
                $scope.baggage.towayOrOutboundBaggage = {};
                RemoveAllItemsFromBaggageSlabs(notificationService.baggage.selectedRoundTripBaggage);
            }
            changeAllButonsText($scope.baggage.towayOrOutboundBaggageTextIndex);
        });
        $scope.$watch(function () { return notificationService.flightsOneWayOutBound.sesameBasketDetails }, function (newValue) {
            if (newValue != undefined) {
                getBagages();
                changeAllButonsText($scope.baggage.towayOrOutboundBaggageTextIndex);
                $scope.baggage.showOutBoundBagSlabs = true;
            }
            else {
                if (notificationService.flightsReturn.sesameBasketDetails == undefined) {
                    $scope.baggage.showOutBoundBagSlabs = false;
                    $scope.baggage.towayOrOutboundBaggage = {};
                }
                RemoveAllItemsFromBaggageSlabs(notificationService.baggage.OutboundSelectedBaggage);
            }
        });
        $scope.$watch(function () { return notificationService.flightsOneWayInBound.sesameBasketDetails }, function (newValue) {
            if (newValue != undefined) {
                changeAllButonsText($scope.baggage.inboundBaggageTextIndex);
                DisplayInboundBaggages();
            }
            else {
                $scope.baggage.showInBoundBagSlabs = false;
                $scope.baggage.InboundBaggage = {};
                $scope.baggage.showInBoundBagSlabs = false;
                RemoveAllItemsFromBaggageSlabs(notificationService.baggage.inboundSelectedBaggage);
            }
            RemoveAllItemsFromBaggageSlabs(notificationService.baggage.inboundSelectedBaggage);

        });

        //For Text change on the buttons
        $scope.$watch(function () { return notificationService.baggage.selectedRoundTripBaggage }, function (newValue, oldValue) {
            if ((newValue == undefined) || (newValue.length != 0) || (newValue.length == 0 && oldValue != undefined && oldValue.length > 0))//(newValue != undefined && oldValue != undefined) &&
                changeButtonsText(notificationService.baggage.selectedRoundTripBaggage, oldValue, $scope.baggage.towayOrOutboundBaggageTextIndex);
        }, true);

        $scope.$watch(function () { return notificationService.baggage.OutboundSelectedBaggage }, function (newValue, oldValue) {
            if ((newValue == undefined) || (newValue.length != 0) || (newValue.length == 0 && oldValue != undefined && oldValue.length > 0))//(newValue != undefined && oldValue != undefined) && 
                changeButtonsText(notificationService.baggage.OutboundSelectedBaggage, oldValue, $scope.baggage.towayOrOutboundBaggageTextIndex);
        }, true);
        $scope.$watch(function () { return notificationService.baggage.inboundSelectedBaggage }, function (newValue, oldValue) {
            if (newValue != oldValue)
                changeButtonsText(notificationService.baggage.inboundSelectedBaggage, oldValue, $scope.baggage.inboundBaggageTextIndex);
        }, true);


        function changeAllButonsText(data) {
            if (data != undefined && data.length > 0) {
                for (var index = 0; index < data.length; index++) {
                    data[index] = true;
                }
            }
        }

        function changeButtonsText(newValue, oldValue, indexArray) {
            var passengerIndex = -1;
            if (newValue != undefined) {
                if (oldValue == undefined && newValue.length>0) {
                    indexArray[newValue[0].passengerIndex-1] = false;
                }
                else if (newValue != undefined && oldValue != undefined && newValue.length > oldValue.length) {
                    indexArray[newValue[newValue.length - 1].passengerIndex - 1] = false;
                }
                else if (newValue != undefined && oldValue != undefined && newValue.length < oldValue.length) {
                    if (newValue.length == 0) {
                        indexArray[oldValue[0].passengerIndex - 1] = true;
                    }
                    else {
                        var isRemovedBaggageFound = false;
                        for (var index = 0; index < newValue.length; index++) {
                            if (newValue[index].passengerIndex != oldValue[index].passengerIndex) {
                                indexArray[oldValue[index].passengerIndex -1] = true;
                                isRemovedBaggageFound = true;
                                break;
                            }
                        }
                        if (!isRemovedBaggageFound) {
                            indexArray[oldValue[oldValue.length - 1].passengerIndex - 1] = true;
                        }
                    }
                }
                else if(newValue != undefined && oldValue != undefined ) {
                    for (var index = 0; index < newValue.length; index++) {
                        if (newValue[index].passengerIndex == oldValue[index].passengerIndex && newValue[index].baggage.slab[0].Code != oldValue[index].baggage.slab[0].Code) {
                            indexArray[index] = false;
                            break;
                        }
                    }
                }
            }
        }

        //Baggage button text changes;
        $scope.baggageButtonTextChange = function (textArray, index) {
            if (textArray != null && textArray.length > index) {
                textArray[index] = true
            }

        }
        $scope.getnoOfPassengers = function (num) {
            if (num != undefined && !isPassengersBind) {
                $scope.baggage.towayOrOutboundBaggageTextIndex = [];
                $scope.baggage.inboundBaggageTextIndex = [];
                for (var index = 0; index < num; index++) {
                    $scope.baggage.towayOrOutboundBaggageTextIndex.push(true);
                    $scope.baggage.inboundBaggageTextIndex.push(true);
                }
                isPassengersBind = true;
            }
            return new Array(num);
        }

        $scope.AddBaggage = function (holdBagsCode, passengerIndex, typeOfJourney) {
            if (holdBagsCode != null && holdBagsCode != '' && holdBagsCode.indexOf("?") == -1) {
                var selectedItem = {};
                var isNewPassinger = true;
                selectedItem.passengerIndex = passengerIndex;
                notificationService.baggage.isTwoWay = $scope.baggage.isTwoWay;
                if ($scope.baggage.isTwoWay == true) {
                    if ($scope.baggage.selectedRoundTripBaggage.length == 0) {

                        selectedItem.baggage = findSelectedBaggage($scope.baggage.towayOrOutboundBaggage.BoundBaggageSlabs.cartitianSlabs, holdBagsCode, passengerIndex);
                        $scope.baggage.selectedRoundTripBaggage.push(selectedItem);
                        notificationService.baggage.selectedRoundTripBaggage = $scope.baggage.selectedRoundTripBaggage;
                    }
                    else {
                        for (var index = 0; $scope.baggage.selectedRoundTripBaggage.length > index; index++) {
                            if ($scope.baggage.selectedRoundTripBaggage[index].passengerIndex == passengerIndex) {
                                isNewPassinger = false;
                                selectedItem.baggage = findSelectedBaggage($scope.baggage.towayOrOutboundBaggage.BoundBaggageSlabs.cartitianSlabs, holdBagsCode, passengerIndex);
                                $scope.baggage.selectedRoundTripBaggage[index] = selectedItem;
                                notificationService.baggage.selectedRoundTripBaggage = $scope.baggage.selectedRoundTripBaggage;
                            }
                        }
                        if (isNewPassinger == true) {
                            selectedItem.baggage = findSelectedBaggage($scope.baggage.towayOrOutboundBaggage.BoundBaggageSlabs.cartitianSlabs, holdBagsCode, passengerIndex);
                            $scope.baggage.selectedRoundTripBaggage.push(selectedItem);
                            notificationService.baggage.selectedRoundTripBaggage = $scope.baggage.selectedRoundTripBaggage;
                        }

                    }
                }
                else {
                    if (typeOfJourney == "Outbound") {
                        if ($scope.baggage.OutboundSelectedBaggage.length == 0) {
                            selectedItem.baggage = findSelectedBaggage($scope.baggage.towayOrOutboundBaggage.BoundBaggageSlabs.cartitianSlabs, holdBagsCode, passengerIndex);
                            $scope.baggage.OutboundSelectedBaggage.push(selectedItem);
                            notificationService.baggage.OutboundSelectedBaggage = $scope.baggage.OutboundSelectedBaggage;
                        }

                        else {
                            for (var index = 0; $scope.baggage.OutboundSelectedBaggage.length > index; index++) {
                                if ($scope.baggage.OutboundSelectedBaggage[index].passengerIndex == passengerIndex) {
                                    isNewPassinger = false;
                                    selectedItem.baggage = findSelectedBaggage($scope.baggage.towayOrOutboundBaggage.BoundBaggageSlabs.cartitianSlabs, holdBagsCode, passengerIndex);
                                    $scope.baggage.OutboundSelectedBaggage[index] = selectedItem;
                                    notificationService.baggage.OutboundSelectedBaggage = $scope.baggage.OutboundSelectedBaggage;
                                }
                            }
                            if (isNewPassinger == true) {
                                selectedItem.baggage = findSelectedBaggage($scope.baggage.towayOrOutboundBaggage.BoundBaggageSlabs.cartitianSlabs, holdBagsCode, passengerIndex);
                                $scope.baggage.OutboundSelectedBaggage.push(selectedItem);
                                notificationService.baggage.OutboundSelectedBaggage = $scope.baggage.OutboundSelectedBaggage;
                            }
                        }
                    }
                    else if (typeOfJourney == "Inbound") {
                        if ($scope.baggage.inboundSelectedBaggage.length == 0) {
                            selectedItem.baggage = findSelectedBaggage($scope.baggage.InboundBaggage.BoundBaggageSlabs.cartitianSlabs, holdBagsCode, passengerIndex);
                            $scope.baggage.inboundSelectedBaggage.push(selectedItem);
                            notificationService.baggage.inboundSelectedBaggage = $scope.baggage.inboundSelectedBaggage;
                        }

                        else {
                            for (var index = 0; $scope.baggage.inboundSelectedBaggage.length > index; index++) {
                                if ($scope.baggage.inboundSelectedBaggage[index].passengerIndex == passengerIndex) {
                                    isNewPassinger = false;
                                    selectedItem.baggage = findSelectedBaggage($scope.baggage.InboundBaggage.BoundBaggageSlabs.cartitianSlabs, holdBagsCode, passengerIndex);
                                    $scope.baggage.inboundSelectedBaggage[index] = selectedItem;
                                    notificationService.baggage.inboundSelectedBaggage = $scope.baggage.inboundSelectedBaggage;
                                }
                            }
                            if (isNewPassinger == true) {
                                selectedItem.baggage = findSelectedBaggage($scope.baggage.InboundBaggage.BoundBaggageSlabs.cartitianSlabs, holdBagsCode, passengerIndex);
                                $scope.baggage.inboundSelectedBaggage.push(selectedItem);
                                notificationService.baggage.inboundSelectedBaggage = $scope.baggage.inboundSelectedBaggage;
                            }
                        }
                    }
                }
            }
        }

        function GetBaggageSlabsSuccessCBForTwoOrOutbound(result) {
            try {
                $scope.baggage.towayOrOutboundBaggageErrorMsg = '';
                if (result.data != "null") {
                    $scope.baggage.towayOrOutboundBaggage = convertReponseToSlabs(result);
                    if ($scope.baggage.isTwoWay == true) {
                        notificationService.baggage.roundTripBaggageAllSlabs = $scope.baggage.towayOrOutboundBaggage.allSlabs;
                    }
                    else {
                        notificationService.baggage.outboundBaggageAllSlabs = $scope.baggage.towayOrOutboundBaggage.allSlabs;
                    }
                    $scope.baggage.showTwowayorOutBoundBaggageDetails = true;
                    $scope.baggage.isTowayOrOutboundBaggageErrorOccured = false;
                }
                else {
                    $scope.baggage.towayOrOutboundBaggageErrorMsg = "Failed to get baggage information from provider.";
                    $scope.baggage.isTowayOrOutboundBaggageErrorOccured = true;
                    $scope.baggage.showTwowayorOutBoundBaggageDetails = false;
                }
                //$("#loader").hide();
            } catch (e) {
                //$("#loader").hide();
            }
        }

        function GetBaggageSlabsSuccessCBInbound(result) {
            try {
                if (result.data != "null") {
                    $scope.baggage.InboundBaggage = convertReponseToSlabs(result);
                    notificationService.baggage.inboundBaggageAllSlabs = $scope.baggage.InboundBaggage.allSlabs;
                    $scope.baggage.isInboundBaggageErrorOccured = false;
                    $scope.baggage.showInBoundBagSlabs = true;

                }
                else {
                    $scope.baggage.InboundBaggageErrorMsg = "Failed to get baggage information from provider.";
                    $scope.baggage.isInboundBaggageErrorOccured = true;
                    $scope.baggage.showInBoundBagSlabs = false;
                }
                //$("#loader").hide();
            } catch (e) {
                //$("#loader").hide();
            }
        }

        function GetBaggageSlabsFailureCB(error) {

        }


        function getBagages() {
            try {
                $("#loader").show();
                var twowayOrOutboundQuoteID = null;
                $scope.baggage.isRefreshed = true;
                $scope.baggage.towayOrOutboundBaggage = {};
                $scope.baggage.showTwowayorOutBoundBaggageDetails = null;
                $scope.baggage.isTowayOrOutboundBaggageErrorOccured = false;
                var twoWayofOutboundBaggage = {};
                notificationService.getBagsForAirline();
                RemoveAllItemsFromBaggageSlabs($scope.baggage.selectedRoundTripBaggage);
                RemoveAllItemsFromBaggageSlabs($scope.baggage.OutboundSelectedBaggage);
                if (Object.prototype.toString.call(notificationService.baggageSearchModel) === '[object Array]') {
                    $scope.baggage.searchModel = notificationService.baggageSearchModel;
                    $scope.baggage.noOfPassengers = $scope.baggage.searchModel[0].totalPassangers;
                    if ($scope.baggage.searchModel[0].flightType == 1) {
                        twowayOrOutboundQuoteID = $scope.baggage.searchModel[0].returnQuoteId;
                        $scope.baggage.isTwoWay = true;
                        $scope.baggage.towayOrOutBoundDisplayText = "Round trip : " + $scope.baggage.searchModel[0].outBoundFlightFromName + " to " + $scope.baggage.searchModel[0].outBoundFlightToName + " Flying with " + $scope.baggage.searchModel[0].outBoundAirLine;
                        twoWayofOutboundBaggage = angular.copy(notificationService.flightsReturn.sesameBasketDetails);
                        GetBaggageSlabsSuccessCBForTwoOrOutbound(twoWayofOutboundBaggage);
                        $scope.baggage.showInBoundBagSlabs = false;
                        baggage.showOutBoundBagSlabs = true;
                        RemoveAllItemsFromBaggageSlabs($scope.baggage.inboundSelectedBaggage);
                    }
                    else {
                        $scope.baggage.isTwoWay = false;
                        twowayOrOutboundQuoteID = $scope.baggage.searchModel[0].outBoundQuoteId;
                        $scope.baggage.towayOrOutBoundDisplayText = "Outbound : " + $scope.baggage.searchModel[0].outBoundFlightFromName + " to " + $scope.baggage.searchModel[0].outBoundFlightToName + " Flying with " + $scope.baggage.searchModel[0].outBoundAirLine;
                        twoWayofOutboundBaggage = angular.copy(notificationService.flightsOneWayOutBound.sesameBasketDetails);
                        GetBaggageSlabsSuccessCBForTwoOrOutbound(twoWayofOutboundBaggage);
                    }
                    
                }
            } catch (e) {
                //$("#loader").hide();
            }
            $("#loader").hide();
        }

        function DisplayInboundBaggages() {
            try {
                //$("#loader").show();
                $scope.baggage.InboundBaggage = {};
                $scope.baggage.isRefreshed = true;
                $scope.baggage.isInboundBaggageErrorOccured = null;
                var twoWayofOutboundBaggage = angular.copy(notificationService.flightsOneWayInBound.sesameBasketDetails);
                notificationService.getBagsForAirline();
                $scope.baggage.searchModel = notificationService.baggageSearchModel;
                $scope.baggage.noOfPassengers = $scope.baggage.searchModel[0].totalPassangers;
                $scope.baggage.inBoundDisplayText = "Inbound: " + $scope.baggage.searchModel[0].inBoundFlightFromName + " to " + $scope.baggage.searchModel[0].inBoundFlightToName + " Flying with " + $scope.baggage.searchModel[0].inBoundAirLine;
                GetBaggageSlabsSuccessCBInbound(twoWayofOutboundBaggage);
                //$("#loader").hide();
            }
            catch (e) {
                //$("#loader").hide();
            }

        }

        function BindDataToSlabs(slabs) {
            var baggageType = [];
            var baggageWeight = [];
            var sportsEquipment = [];
            var totalSlabs = {};
            var cartitianSlabs = [];

            angular.forEach(slabs, function (slab, key) {
                slab.BuyingPricePence = (parseFloat(slab.BuyingPricePence) / 100).toFixed(2);
                slab.SellingPricePence = (parseFloat(slab.SellingPricePence) / 100).toFixed(2);
                if (slab.Type == "baggage") {
                    baggageType.push(slab)
                }
                else if (slab.Type == "additional_weight") {
                    baggageWeight.push(slab)
                }
                else if (slab.Type == "equipment") {
                    sportsEquipment.push(slab)
                }
            })

            //Cartian of No of Bags and weights
            if (baggageType.length > 0 && baggageWeight.length > 0) {
                for (var baggageIndex = 0; baggageType.length > baggageIndex; baggageIndex++) {
                    for (var baggageWeightIndex = 0; baggageWeight.length > baggageWeightIndex; baggageWeightIndex++) {
                        if (baggageType[baggageIndex].Code != "a3c0c74d2f85349135b9dc482254364d") {
                            var cartitianSlab = {};
                            cartitianSlab.Description = baggageType[baggageIndex].Description + " (" + baggageWeight[baggageWeightIndex].Description + ")"
                            cartitianSlab.Code = baggageType[baggageIndex].Code + "|" + baggageWeight[baggageWeightIndex].Code;
                            cartitianSlab.Type = baggageType[baggageIndex].Type + "|" + baggageWeight[baggageWeightIndex].Type;
                            cartitianSlab.QuantityAvailable = baggageType[baggageIndex].QuantityAvailable + "|" + baggageWeight[baggageWeightIndex].QuantityAvailable;
                            cartitianSlab.QuantityRequired = baggageType[baggageIndex].QuantityRequired + "|" + baggageWeight[baggageWeightIndex].QuantityRequired;
                            cartitianSlab.BuyingPricePence = (parseFloat(baggageType[baggageIndex].BuyingPricePence) + parseFloat(baggageWeight[baggageWeightIndex].BuyingPricePence)).toFixed(2);
                            cartitianSlab.SellingPricePence = (parseFloat(baggageType[baggageIndex].SellingPricePence) + parseFloat(baggageWeight[baggageWeightIndex].SellingPricePence)).toFixed(2);
                            cartitianSlab.Items = baggageType[baggageIndex].Items;
                            cartitianSlabs.push(cartitianSlab);
                        }
                    }

                }
                totalSlabs.cartitianSlabs = cartitianSlabs;
            }
            else {
                totalSlabs.cartitianSlabs = baggageType;
            }
            totalSlabs.Baggage = baggageType;
            totalSlabs.BaggaeWeight = baggageWeight;
            totalSlabs.SportsEquipment = sportsEquipment;
            return totalSlabs;
        }

        function findSelectedBaggage(baggageSlabs, selectedCode, passengerIndex) {
            var selectedSlabWithPassenger = {};
            selectedSlabWithPassenger.slab = [];
            if (typeof (selectedCode) == "object") {
                angular.forEach(selectedCode, function (selctedCodeValue, key) {
                    angular.forEach(baggageSlabs, function (value, key) {
                        if (value.Code == selctedCodeValue) {
                            selectedSlabWithPassenger.slab.push(value);
                        }
                    });
                });
            }
            else if (typeof (selectedCode) == "string") {
                angular.forEach(baggageSlabs, function (value, key) {
                    if (value.Code == selectedCode) {
                        selectedSlabWithPassenger.slab.push(value);
                    }
                });
            }
            return selectedSlabWithPassenger;
        }

        function convertReponseToSlabs(responseResult) {
            var returnValue = {};
            returnValue.ShowNoOfBaggages = true;
            //returnValue.ShowBagWeigths = false;
            returnValue.ShowSportsEquipment = false;
            returnValue.hideTotalSlabs = false;
            if (responseResult.data != "null") {
                slabs = responseResult.BasketComponents[0].ComponentOptions
                if (responseResult.length == 0) {
                    returnValue.hideTotalSlabs = true;
                }
                else {
                    returnValue.BoundBaggageSlabs = BindDataToSlabs(slabs);

                    if (returnValue.BoundBaggageSlabs.BaggaeWeight != null && returnValue.BoundBaggageSlabs.BaggaeWeight.length > 0) {
                        returnValue.ShowBagWeigths = true;
                    }

                    if (returnValue.BoundBaggageSlabs.SportsEquipment != null && returnValue.BoundBaggageSlabs.SportsEquipment.length > 0) {
                        returnValue.ShowSportsEquipment = true;
                    }
                }
                returnValue.basketID = responseResult.BasketID;
                returnValue.componentID = responseResult.BasketComponents[0].ComponentID;
                returnValue.marketingAirLine = responseResult.BasketComponents[0].ComponentFlightInformation[0].Legs[0].MarketingAirline;
                returnValue.allSlabs = slabs;

            }
            return returnValue;
        }

        function RemoveAllItemsFromBaggageSlabs(selectedSlabs) {
            if (typeof (selectedSlabs) == "object") {
                while (selectedSlabs.length) {
                    selectedSlabs.pop();
                }
            }
        }

        $scope.$watch(function () { return notificationService.changeSearch; }, function (newValue, oldValue) {
            if (notificationService.resetHotels) {
                if (newValue) {
                    resetBaggageData();
                }
            }
        }, true);

        $scope.$watch(function () { return notificationService.changeSearch; }, function (newValue, oldValue) {
            if (notificationService.resetHotels) {
                if (newValue) {
                    resetBaggageData();
                }
            }
        });

        function resetBaggageData() {
            $scope.hideTotalSlabs = false;
            $scope.baggage.selectedRoundTripBaggage = [];
            $scope.baggage.OutboundSelectedBaggage = [];
            $scope.baggage.inboundSelectedBaggage = [];
            $scope.baggage.towayOrOutboundBaggage = {};
            $scope.baggage.InboundBaggage = {};
            $scope.baggage.isRefreshed = false;
        }
    }


})(angular.module('SwordFish'));