﻿(function (app) {
    'use strict'

    //Service Creation
    app.factory('baggageService', baggageService);

    //DI for Service
    baggageService.$inject = ['notificationService', 'apiService'];
    function baggageService(notificationService, apiService) {
        var service ={
            GetBaggageSlabs: GetBaggageSlabs
        }

        function GetBaggageSlabs(data, GetBaggageSlabsSuccessCB, GetBaggageSlabsFailureCB) {
            apiService.post('../SearchResults/GetBaggageSlabs', data, GetBaggageSlabsSuccessCB, GetBaggageSlabsFailureCB)
        }
        return service;
    }
   
})(angular.module('SwordFish'));