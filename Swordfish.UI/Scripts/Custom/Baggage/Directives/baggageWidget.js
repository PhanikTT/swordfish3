﻿(function (app) {
    'use strict'

    app.directive('baggageWidget', function () {
        return {
            restrict: 'E',
            replace: true,
            templateUrl: './../Scripts/Custom/Baggage/Directives/baggageWidget.html',
            controller: 'baggageCtrl'
        };
    })

})(angular.module('SwordFish'));