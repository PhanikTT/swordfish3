(function () {
    'use strict'

    angular.module("SwordFish", ['rzModule', 'ui.bootstrap', 'countTo', 'ngMessages', 'daterangepicker', 'timepicker', 'datepicker', 'ngTouch', 'ui.grid', 'ui.grid.pagination', 'jkuri.touchspin', 'sticky', 'ngSanitize', 'ngResource']);

})();