﻿var basketModel = {
    BasketPriceExpiryLimit: '5',
    BasketQuoteRef: 'TXT00001',
    BasketCustomers: {
        Adults: '2',
        Children: '2',
        Infants: '0'
    },
    BasketDates: {
        OutboundDate: '13/05/16',
        InboundDate: '21/06/16'
    },
    hotel: {},
    BasketFlights: {
        OutboundAirline: 'Ryanair',
        OutboundFlightNumber: 'FR 8382',
        OutboundSource: 'STN',
        OutboundDest: 'ALC',
        OutboundDeptTime: '11:20',
        OutboundArrTime: '15:05',

        InboundAirline: 'Ryanair',
        InboundFlightNumber: 'FR 8384',
        InboundSource: 'ALC',
        InboundDest: 'STN',
        InboundDeptTime: '16:20',
        InboundArrTime: '10:05',
        Baggage: [{
            Price:'35',
        },
        {
            Price: '36',
        },
        {
            Price: '35',
        }],
    },
    BasketTeletextCosts: {
        Flights: 0.00,
        Hotel: 0.00,
        Baggage: 0.00,
        Transfers: 0.00,
        Fidelity: 0.00
    }
};
var basketWidgetCtrllist = {
    BasketPriceExpiryLimit: '5',
    BasketQuoteRef: 'TXT00001',
    BasketCustomers: {
        Adults: '2',
        Children: '2',
        Infants: '0'
    },
    BasketDates: {
        OutboundDate: '13/05/2016 15:30',
        InboundDate: '21/06/2016 03:30'
    },
    BasketHotel: {
        HotelName: 'Viva Tropic Aparthotel ',
        HotelCheckinDate: '13/05/2016',
        HotelCheckoutDate: '20/05/2016',
        Nts: '7',
        People: '2A, 0C, 0I',
        HotelResort: 'Alcudia',
        HotelLocation: 'Majorca',
        Room: [{
            BoardType: 'Bed & Breakfast',
            RoomType: 'Twin - Double Room',
            Price: '568',
            RoomPeople: '2A0C0I'
        },
        {
            BoardType: 'Room Only',
            RoomType: 'Twin - Double Room',
            Price: '598',
            RoomPeople: '2A0C1I'
        },
        {
            BoardType: 'Self Catering',
            RoomType: 'Twin - Double Room',
            Price: '553',
            RoomPeople: '2A0C1I'
        }],
    },
    BasketFlights: {
        Outbound: [{
            OutboundAirline: 'Ryanair',
            OutboundFlightNumber: 'FR 8382',
            OutboundSource: 'STN',
            OutboundDest: 'BCN',
            OutboundDeptTime: '22/05/2016 22:35',
            OutboundArrTime: '23/05/2016 01:35',
        }],

        Inbound: [{
            InboundAirline: 'Ryanair',
            InboundFlightNumber: 'FR 8384',
            InboundSource: 'ALC',
            InboundDest: 'BCN',
            InboundDeptTime: '23/05/2016 03:35',
            InboundArrTime: '23/05/2016 06:35',
        }],
        Baggage1:
            [{
                Bprice: '£0', id: '1', name: "1 x Hand Luggage only, 10kg max  (£0.00)"
            }, {
                Bprice: '£47', id: '2', name: "1 x One bag, 20kg max  (£47.00)"
            }, {
                Bprice: '£90', id: '3', name: "1 x Two bags, 20kg total  (£90.00)"
            }],
        Baggage2:
            [{
                Bprice: '£0', id: '1', name: "1 x Hand Luggage only, 10kg max  (£0.00)"
            }, {
                Bprice: '£47', id: '2', name: "1 x One bag, 20kg max  (£47.00)"
            }, {
                Bprice: '£90', id: '3', name: "1 x Two bags, 20kg total  (£90.00)"
            }],
        Baggage3:
            [{
                Bprice: '£0', id: '1', name: "1 x Hand Luggage only, 10kg max  (£0.00)"
            }, {
                Bprice: '£47', id: '2', name: "1 x One bag, 20kg max  (£47.00)"
            }, {
                Bprice: '£90', id: '3', name: "1 x Two bags, 20kg total  (£90.00)"
            }],

        Baggage4:
            [{
                Bprice: '£0', id: '1', name: "1 x Hand Luggage only, 10kg max  (£0.00)"
            }, {
                Bprice: '£47', id: '2', name: "1 x One bag, 20kg max  (£47.00)"
            }, {
                Bprice: '£90', id: '3', name: "1 x Two bags, 20kg total  (£90.00)"
            }],
        Baggage5:
            [{
                Bprice: '£0', id: '1', name: "1 x Hand Luggage only, 10kg max  (£0.00)"
            }, {
                Bprice: '£47', id: '2', name: "1 x One bag, 20kg max  (£47.00)"
            }, {
                Bprice: '£90', id: '3', name: "1 x Two bags, 20kg total  (£90.00)"
            }],
        Baggage6:
            [{
                Bprice: '£0', id: '1', name: "1 x Hand Luggage only, 10kg max  (£0.00)"
            }, {
                Bprice: '£47', id: '2', name: "1 x One bag, 20kg max  (£47.00)"
            }, {
                Bprice: '£90', id: '3', name: "1 x Two bags, 20kg total  (£90.00)"
            }],
    },
    BasketTeletextCosts: {
        OutFlights: '0',
        InFlights: '0',
        Hotel: '0',
        Baggage: '0',
        Transfers: '0',
        Fidelity: '5'
    }
};



