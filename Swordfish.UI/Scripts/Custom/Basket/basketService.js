﻿(function (app) {
    'use strict'

    //Service Creation
    app.factory('basketService', basketService);

    //DI for Service
    basketService.$inject = ['notificationService', 'apiService'];

    //Basket Service
    function basketService(notificationService, apiService) {
        var service = {
            sendSmsAndMail: sendSmsAndMail,
            saveAgentJournal: saveAgentJournal,
            saveBasket : saveBasket
        };

        function sendSmsAndMail(data, successCB, failureCB) {
            apiService.post('../SearchResults/SendSmsAndMail', data, successCB, failureCB);
        }

        function saveAgentJournal(data, successCB, failureCB) {
            apiService.post('../SearchResults/SaveAgentJournal', data, successCB, failureCB);
        }

        function saveBasket(data, successSBCB, failureSBCB) {
            apiService.post('../SearchResults/SaveQuoteData', data, successSBCB, failureSBCB);
        }
        return service;
    }

})(angular.module('SwordFish'));