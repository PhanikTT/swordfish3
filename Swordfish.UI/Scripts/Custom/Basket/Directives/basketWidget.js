﻿(function (app) {
    'use strict'

    app.directive('basketWidget', function () {
        return {
            restrict: 'E',
            replace: true,
            templateUrl: './../Scripts/Custom/Basket/Directives/basketWidget.html',
            controller: 'basketCtrl'
        };
    })

})(angular.module('SwordFish'));