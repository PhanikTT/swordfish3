﻿(function (app) {
    'use strict'

    //Controller Creation
    app.controller('basketCtrl', basketCtrl);

    //DI for Controller
    basketCtrl.$inject = ['$scope', 'notificationService', 'basketService', 'validationService'];


    //Basket Controller
    function basketCtrl($scope, notificationService, basketService, validationService) {
        $scope.isNumber = isNumber;
        $scope.basket = {};
        $scope.basket.searchModel = notificationService.searchModel;
        $scope.basket.userInfo = notificationService.userInfo;
        $scope.basket.marginFlighVal = 0;
        $scope.basket.marginHotelVal = 0;
        $scope.basket.marginExtraVal = 0;
        var Paxobj = notificationService.getPAXCount();
        var PaxCount = Paxobj.AdultsCount + Paxobj.ChildCount;
        $scope.basket.fidelityVal = notificationService.fidelityVal * PaxCount;
        $scope.basket.internalNotes = '';
        var validationErrorMsg = [];
        $scope.basket.isDirty = false;
        $scope.basket.showQuoteRef = false;
        $scope.basket.quoteReference = "";
        $scope.basket.quoteHeaderId = "";
        $scope.basket.isContainBags = false;
        $scope.basket.isBaggageMissing = false;
        //Please uncomment respective scope object and start working
        $scope.$watch(function () { return notificationService.flightsReturn }, function (newValue) {
            $scope.basket.flightsReturn = newValue;
            $scope.basket.isDirty = true;
            $scope.basket.quoteReference = null;
            $scope.basket.quoteHeaderId = null;
            notificationService.hotel.isHotelsSearchButttonNeeded = true;//For Showing GetHotels button
            $scope.basket.isDisableSaveQuote = false;
        });
        $scope.$watch(function () { return notificationService.flightsOneWayOutBound }, function (newValue) {
            $scope.basket.flightsOneWayOutBound = newValue;
            $scope.basket.isDirty = true;
            $scope.basket.quoteReference = null;
            $scope.basket.quoteHeaderId = null;
            notificationService.hotel.isHotelsSearchButttonNeeded = true;//For Showing GetHotels button
            $scope.basket.isDisableSaveQuote = false;
        });
        $scope.$watch(function () { return notificationService.flightsOneWayInBound }, function (newValue) {
            $scope.basket.flightsOneWayInBound = newValue;
            $scope.basket.isDirty = true;
            $scope.basket.quoteReference = null;
            $scope.basket.quoteHeaderId = null;    
            notificationService.hotel.isHotelsSearchButttonNeeded = true;//For Showing GetHotels button
            $scope.basket.isDisableSaveQuote = false;
        });
        $scope.$watch(function () { return notificationService.hotel }, function (newValue, oldValue) {
            $scope.basket.hotel = newValue
            $scope.basket.isDirty = true;
            $scope.basket.quoteReference = null;
            $scope.basket.quoteHeaderId = null;
        });
        $scope.$watch(function () { return notificationService.transfers }, function (newValue, oldValue) {
            $scope.basket.transfers = newValue
            $scope.basket.isDirty = true;
            $scope.basket.quoteReference = null;
            $scope.basket.quoteHeaderId = null;
            $scope.basket.isDisableSaveQuote = false;
        });
        $scope.$watch(function () { return notificationService.transfers.transfersdata }, function (newValue, oldValue) {
            $scope.basket.transfersdata = newValue
            $scope.basket.isDirty = true;
            $scope.basket.quoteReference = null;
            $scope.basket.quoteHeaderId = null;
            $scope.basket.isDisableSaveQuote = false;
        });
        $scope.$watch(function () {
            return notificationService.customer
        }, function (newValue, oldValue) {
            $scope.basket.customer = newValue;
            $scope.basket.isDirty = true;
            $scope.basket.quoteReference = null;
            $scope.basket.quoteHeaderId = null;
            $scope.basket.customer.IsSms = false;
            $scope.basket.customer.IsEmail = false;
            $scope.basket.isDisableSaveQuote = false;
        });
        function IsQuoteSave() {

            if ($scope.basket.isDirty == false && $scope.basket.quoteReference != "" && $scope.basket.quoteHeaderId != "") {
                return true;
            }
            else {
                return false;
            }
        }
        $scope.setDirtyFlag = function () {
            $scope.basket.isDirty = true;
        }
        $scope.saveBooking = function () {

            if (IsQuoteSave()) {
                $scope.isRedirect = false;
                SaveQuoteCheckRedirect();
            }
            else {
                $scope.isRedirect = true;
                $scope.lightBoxSaveBasket();
            }
        }
        $scope.$watch(function () {
            return notificationService.baggage
        }, function (newValue, oldValue) {
            $scope.basket.baggage = newValue;
            var total = 0;
            $scope.basket.isDirty = true;
            $scope.basket.quoteReference = null;
            $scope.basket.quoteHeaderId = null;
            $scope.basket.isDisableSaveQuote = false;
            angular.forEach($scope.basket.baggage.selectedRoundTripBaggage, function (value, key) {
                total = parseFloat(total) + parseFloat(value.baggage.slab[0].SellingPricePence);
            });
            angular.forEach($scope.basket.baggage.OutboundSelectedBaggage, function (value, key) {
                total = parseFloat(total) + parseFloat(value.baggage.slab[0].SellingPricePence);
            });
            angular.forEach($scope.basket.baggage.inboundSelectedBaggage, function (value, key) {
                total = parseFloat(total) + parseFloat(value.baggage.slab[0].SellingPricePence);
            });
            $scope.basket.baggage.totalPrice = total;
            if (($scope.basket.baggage.selectedRoundTripBaggage != null && $scope.basket.baggage.selectedRoundTripBaggage.length > 0) || ($scope.basket.baggage.OutboundSelectedBaggage != null && $scope.basket.baggage.OutboundSelectedBaggage.length > 0) || ($scope.basket.baggage.inboundSelectedBaggage != null && $scope.basket.baggage.inboundSelectedBaggage.length > 0)) {
                $scope.basket.isContainBags = true;
            }
            else {
                $scope.basket.isContainBags = false;
            }
        }, true);

        //Send SMS and Email
        function isNumber(event) {
            return validationService.isNumber(event);
        }
        function SaveQuoteCheckRedirect() {
            $('#QuoteRef').val($scope.basket.quoteReference);
            $('#QuoteHeaderId').val($scope.basket.quoteHeaderId);
            $('#basketForm').submit();
        }
        $scope.sendSmsAndMail = function (customer) {
            try {
                $("#loader").show();
                if ($.isEmptyObject(customer) != true) {
                    if (customer.IsSms == false && customer.IsEmail == false) {
                        $("#loader").hide();
                        notificationService.validationResult = { showPopup: true, errorMsg: ["Please select either SMS or Email check box."], title: 'Validation Result' };
                        return;
                    } else
                        if (customer.IsSms == true && (customer.CustomerMobileNo == null || customer.CustomerMobileNo == 'undefined' || customer.CustomerMobileNo == '')) {
                            $("#loader").hide();
                            notificationService.validationResult = { showPopup: true, errorMsg: ["Enter mobile number."], title: 'Validation Result' };
                            return;
                        }
                        else if (customer.IsSms == true && (!validationService.validateMobile(customer.CustomerMobileNo) || customer.CustomerMobileNo.length<10)) {
                            $("#loader").hide();
                            notificationService.validationResult = { showPopup: true, errorMsg: ["Mobile number is not valid number of digits should be minimum 10."], title: 'Validation Result' };
                            return;
                        }
                        else if (customer.IsEmail == true && (customer.CustomerEmail == null || customer.CustomerEmail == 'undefined' || customer.CustomerEmail == '')) {
                            $("#loader").hide();
                            notificationService.validationResult = { showPopup: true, errorMsg: ["Enter email address."], title: 'Validation Result' };
                            return;
                        } else if (customer.IsEmail == true && !validationService.validateEmail(customer.CustomerEmail)) {
                            $("#loader").hide();
                            notificationService.validationResult = { showPopup: true, errorMsg: ["Email address is not valid."], title: 'Validation Result' };
                            return;
                        }
                    if ($scope.basket.quoteReference == undefined || $scope.basket.quoteReference == "" || $scope.basket.isDirty == true) {
                        $scope.lightBoxSaveBasket();
                    }
                    else {
                       
                        basketService.sendSmsAndMail(JSON.stringify({ "Mobile": customer.CountryCode + customer.CustomerMobileNo, "Email": customer.CustomerEmail, "QuoteRef": $scope.basket.quoteHeaderId, "IsSms": customer.IsSms, "IsEmail": customer.IsEmail }), successCB, failureCB)
                    }
                } else {
                    notificationService.validationResult = { showPopup: true, errorMsg: ["Please select either SMS or Email check box."], title: 'Validation Result' };
                    return;
                }
            }
            catch (e) {
                $("#loader").hide();
            }
        }

        function successCB(result) {
            var res = result.data.split(',');
            $scope.showLightBoxPopup = false;
            if (result.data.split(',')[0] == 'SmsTrue' && result.data.split(',')[1] == 'EmailTrue') {
                $("#loader").hide();
                notificationService.validationResult = { showPopup: true, errorMsg: ["Quote reference id: <b>" + $scope.basket.quoteReference + "</b>", "SMS and Email has been sent successfully."], title: 'Success' };
            } else if (result.data.split(',')[0] == 'SmsTrue' && result.data.split(',')[1] == 'EmailFalse') {
                $("#loader").hide();
                notificationService.validationResult = { showPopup: true, errorMsg: ["Quote reference id: <b>" + $scope.basket.quoteReference + "</b>", "SMS has been sent successfully and Email has been sent failed."], title: 'Success' };
            }
            else if (result.data.split(',')[0] == 'SmsFalse' && result.data.split(',')[1] == 'EmailTrue') {
                $("#loader").hide();
                notificationService.validationResult = { showPopup: true, errorMsg: ["Quote reference id: <b><span class='qrid'>" + $scope.basket.quoteReference + "</b>", "</span> Email has been sent successfully and SMS has been sent failed."], title: 'Success' };

            } else if (result.data.split(',')[0] == 'SmsTrue') {
                $("#loader").hide();
                notificationService.validationResult = { showPopup: true, errorMsg: ["Quote reference id: <b><span class='qrid'>" + $scope.basket.quoteReference + "</b>", "</span> SMS has been sent successfully."], title: 'Success' };
            } else if (result.data.split(',')[1] == 'EmailTrue') {
                $("#loader").hide();
                notificationService.validationResult = { showPopup: true, errorMsg: ["Quote reference id: <b><span class='qrid'>" + $scope.basket.quoteReference + "</b>", "</span> Email has been sent successfully.."], title: 'Success' };
            }
            else if (result.data.split(',')[0] == 'SmsFalse' && result.data.split(',')[1] == 'EmailFalse') {
                $("#loader").hide();
                notificationService.validationResult = { showPopup: true, errorMsg: ["Quote reference id: <b><span class='qrid'>" + $scope.basket.quoteReference + "</b>", "</span> SMS has been sent failed and Email has been sent failed."], title: 'Success' };
            }
            else if (result.data.split(',')[0] == 'SmsFalse') {
                $("#loader").hide();
                notificationService.validationResult = { showPopup: true, errorMsg: ["Quote reference id: <span class='qrid'>" + $scope.basket.quoteReference, "</span> SMS has been sent failed.."], title: 'Success' };
            }
            else if (result.data.split(',')[1] == 'EmailFalse') {
                $("#loader").hide();
                notificationService.validationResult = { showPopup: true, errorMsg: ["Quote reference id: <span class='qrid'>" + $scope.basket.quoteReference, "</span> Email has been sent failed."], title: 'Success' };
            }
        }

        function failureCB(error) {
            $("#loader").hide();
        }

        function successSBCB(result) {
            validationErrorMsg.length = 0;
            try {
                if (result.data != null) {
                    if (result.data.status == "Success") {
                        $scope.basket.isDirty = false;
                        validationErrorMsg.push("Quote has been saved successfully with quote reference id: <span class='qrid'><b>" + result.data.response.customerReferenceCode + "</b></span>");
                        $scope.basket.showQuoteRef = true;
                        $scope.basket.quoteReference = result.data.response.customerReferenceCode;
                        $scope.basket.quoteHeaderId = result.data.response.quoteHeaderId;
                        notificationService.isReSearchQuotes = true;
                        if ($scope.isRedirect) {
                            $("#loader").hide();
                            SaveQuoteCheckRedirect();
                            $scope.showLightBoxPopup = false;
                        }
                        if ($scope.currentContext == 'EmailSms') {
                            basketService.sendSmsAndMail(JSON.stringify({ "Mobile": $scope.basket.customer.CountryCode + $scope.basket.customer.CustomerMobileNo, "Email": $scope.basket.customer.CustomerEmail, "QuoteRef": $scope.basket.quoteHeaderId, "IsSms": $scope.basket.customer.IsSms, "IsEmail": $scope.basket.customer.IsEmail }), successCB, failureCB)
                        } else {
                            $("#loader").hide();
                            $scope.showLightBoxPopup = false;
                            notificationService.validationResult = { showPopup: true, errorMsg: validationErrorMsg, title: 'Success' };

                        }
                    }
                    else {
                        $("#loader").hide();
                        validationErrorMsg = result.data.errorMsg;
                        notificationService.validationResult = { showPopup: true, errorMsg: validationErrorMsg, title: 'Validation Result' };
                    }
                } else {
                    $("#loader").hide();
                    validationErrorMsg.push("Something went wrong");
                    notificationService.validationResult = { showPopup: true, errorMsg: validationErrorMsg, title: 'Failed' };
                }

            }
            catch (e) {
                $("#loader").hide();
            }
        }

        function failureSBCB(error) {
            $("#loader").hide();
        }

        //Save Basket fucntion      
        $scope.saveBasket = function (flag) {
            try {
                $("#loader").show();
                var validationResult = ValidateBasket($scope);
                var validationMes = '';
                if (validationResult.isValidInput == true && $scope.basket.isDirty == true) {
                    $scope.showLightBoxPopup = true;
                    if (flag != undefined && flag != '' && flag == 'book') {
                        $scope.currentContext = 'Book';
                        $("#loader").hide();
                    }
                    else if (flag != undefined && flag != '' && flag == 'quote') {
                        $scope.currentContext = 'SaveQuote';
                        $("#loader").hide();
                    }
                    if (flag != undefined && flag != '' && flag == 'mail') {
                        $scope.currentContext = 'EmailSms';
                        $scope.basket.customer.IsSms = true;
                        $scope.basket.customer.IsEmail = true;
                        $("#loader").hide();
                    }
                }
                else if ($scope.basket.isDirty == false) {
                    if (flag != undefined && flag != '' && flag == 'book') {
                        $scope.currentContext = 'Book';
                        $scope.showLightBoxPopup = true;
                        $("#loader").hide();
                    }
                    else if (flag != undefined && flag != '' && flag == 'quote') {
                        validationResult.errorMes.push("Saved already");
                        notificationService.validationResult = { showPopup: true, errorMsg: validationResult.errorMes, title: 'Validation Result' };
                        $("#loader").hide();
                    }
                    if (flag != undefined && flag != '' && flag == 'mail') {
                        $scope.currentContext = 'EmailSms';
                        $scope.basket.customer.IsSms = true;
                        $scope.basket.customer.IsEmail = true;
                        $scope.showLightBoxPopup = true;
                        $("#loader").hide();
                    }
                }
                else {
                    $("#loader").hide();
                    notificationService.validationResult = { showPopup: true, errorMsg: validationResult.errorMes, title: 'Validation Result' };

                }
                //Priparing baggage object for Lightbox
                $scope.basket.baggage.lightBoxBaggageModel = {};
                $scope.basket.baggage.lightBoxBaggageModel.selectedRoundTripBaggage = [];
                $scope.basket.baggage.lightBoxBaggageModel.OutboundSelectedBaggage = [];
                $scope.basket.baggage.lightBoxBaggageModel.inboundSelectedBaggage = [];
                $scope.basket.showBaggageWarning = false;
                var isBaggageFound = false;
                var baggageTwoWay = false;
                if ($scope.basket.baggage.selectedRoundTripBaggage != null && $scope.basket.baggage.selectedRoundTripBaggage != undefined && $scope.basket.baggage.selectedRoundTripBaggage.length > 0) {
                    for (var index = 0; index < ($scope.basket.searchModel.Adults + $scope.basket.searchModel.Child + $scope.basket.searchModel.Infants) ; index++) {
                        isBaggageFound = false;
                        for (var bagIndex = 0; bagIndex < $scope.basket.baggage.selectedRoundTripBaggage.length; bagIndex++) {
                            if (index + 1 == $scope.basket.baggage.selectedRoundTripBaggage[bagIndex].passengerIndex) {
                                $scope.basket.baggage.lightBoxBaggageModel.selectedRoundTripBaggage.push(angular.copy($scope.basket.baggage.selectedRoundTripBaggage[bagIndex]));
                                isBaggageFound = true;
                                $scope.basket.showBaggageWarning = false;
                            }
                        }
                        if (!isBaggageFound) {
                            var bag = { slab: [{ Description: '<span class="text-danger">Baggage not selected.</span>' }] };
                            $scope.basket.baggage.lightBoxBaggageModel.selectedRoundTripBaggage.push({ passengerIndex: index + 1, baggage: bag });
                            $scope.basket.showBaggageWarning = true;
                        }
                    }
                }
                else {
                    $scope.basket.baggage.selectedRoundTripBaggage = [];
                    for (var index = 0; index < ($scope.basket.searchModel.Adults + $scope.basket.searchModel.Child + $scope.basket.searchModel.Infants) ; index++) {
                        var bag = { slab: [{ Description: '<span class="text-danger">Baggage not selected.</span>' }] };
                        $scope.basket.baggage.lightBoxBaggageModel.selectedRoundTripBaggage.push({ passengerIndex: index + 1, baggage: bag });
                        $scope.basket.showBaggageWarning = true;
                    }
                }
                if ($scope.basket.baggage.OutboundSelectedBaggage != null && $scope.basket.baggage.OutboundSelectedBaggage != undefined && $scope.basket.baggage.OutboundSelectedBaggage.length > 0) {
                    baggageTwoWay = true;
                    for (var index = 0; index < ($scope.basket.searchModel.Adults + $scope.basket.searchModel.Child + $scope.basket.searchModel.Infants) ; index++) {
                        isBaggageFound = false;
                        for (var bagIndex = 0; bagIndex < $scope.basket.baggage.OutboundSelectedBaggage.length; bagIndex++) {
                            if (index + 1 == $scope.basket.baggage.OutboundSelectedBaggage[bagIndex].passengerIndex) {
                                $scope.basket.baggage.lightBoxBaggageModel.OutboundSelectedBaggage.push(angular.copy($scope.basket.baggage.OutboundSelectedBaggage[bagIndex]));
                                isBaggageFound = true;
                                $scope.basket.showBaggageWarning = false;
                            }
                        }
                        if (!isBaggageFound) {
                            var bag = { slab: [{ Description: '<span class="text-danger">Baggage not selected.</span>' }] };
                            $scope.basket.baggage.lightBoxBaggageModel.OutboundSelectedBaggage.push({ passengerIndex: index + 1, baggage: bag });
                            $scope.basket.showBaggageWarning = true;
                        }
                    }
                }
                else {
                    $scope.basket.baggage.OutboundSelectedBaggage = [];
                    for (var index = 0; index < ($scope.basket.searchModel.Adults + $scope.basket.searchModel.Child + $scope.basket.searchModel.Infants) ; index++) {
                        var bag = { slab: [{ Description: '<span class="text-danger">Baggage not selected.</span>' }] };
                        $scope.basket.baggage.lightBoxBaggageModel.OutboundSelectedBaggage.push({ passengerIndex: index + 1, baggage: bag });
                        $scope.basket.showBaggageWarning = true;
                    }
                }
                if ($scope.basket.baggage.inboundSelectedBaggage != null && $scope.basket.baggage.inboundSelectedBaggage != undefined && $scope.basket.baggage.inboundSelectedBaggage.length > 0) {
                    baggageTwoWay = true;
                    for (var index = 0; index < ($scope.basket.searchModel.Adults + $scope.basket.searchModel.Child + $scope.basket.searchModel.Infants) ; index++) {
                        isBaggageFound = false;
                        for (var bagIndex = 0; bagIndex < $scope.basket.baggage.inboundSelectedBaggage.length; bagIndex++) {
                            if (index + 1 == $scope.basket.baggage.inboundSelectedBaggage[bagIndex].passengerIndex) {
                                $scope.basket.baggage.lightBoxBaggageModel.inboundSelectedBaggage.push(angular.copy($scope.basket.baggage.inboundSelectedBaggage[bagIndex]));
                                isBaggageFound = true;
                                $scope.basket.showBaggageWarning = false;
                            }
                        }
                        if (!isBaggageFound) {
                            var bag = { slab: [{ Description: '<span class="text-danger">Baggage not selected.</span>' }] };
                            $scope.basket.baggage.lightBoxBaggageModel.inboundSelectedBaggage.push({ passengerIndex: index + 1, baggage: bag });
                            $scope.basket.showBaggageWarning = true;
                        }
                    }
                }
                else {
                    $scope.basket.baggage.inboundSelectedBaggage = [];
                    for (var index = 0; index < ($scope.basket.searchModel.Adults + $scope.basket.searchModel.Child + $scope.basket.searchModel.Infants) ; index++) {
                        var bag = { slab: [{ Description: '<span class="text-danger">Baggage not selected.</span>' }] };
                        $scope.basket.baggage.lightBoxBaggageModel.inboundSelectedBaggage.push({ passengerIndex: index + 1, baggage: bag });
                        $scope.basket.showBaggageWarning = true;
                    }
                }
                if (!baggageTwoWay && $scope.basket.baggage.selectedRoundTripBaggage != undefined && $scope.basket.baggage.selectedRoundTripBaggage.length < ($scope.basket.searchModel.Adults + $scope.basket.searchModel.Child + $scope.basket.searchModel.Infants)) {
                    $scope.basket.showBaggageWarning = true;
                }
                else if (baggageTwoWay && $scope.basket.baggage.OutboundSelectedBaggage != undefined && $scope.basket.baggage.OutboundSelectedBaggage.length < ($scope.basket.searchModel.Adults + $scope.basket.searchModel.Child + $scope.basket.searchModel.Infants)) {
                    $scope.basket.showBaggageWarning = true;
                }
                else if (baggageTwoWay && $scope.basket.baggage.inboundSelectedBaggage != undefined && $scope.basket.baggage.inboundSelectedBaggage.length < ($scope.basket.searchModel.Adults + $scope.basket.searchModel.Child + $scope.basket.searchModel.Infants)) {
                    $scope.basket.showBaggageWarning = true;
                }
                else {
                    $scope.basket.showBaggageWarning = false;
                }
            }
            catch (e) {
                $("#loader").hide();
            }
        }

        $scope.lightBoxSaveBasket = function () {
            $("#loader").show();
            $scope.basket.isDisableSaveQuote = true;
            basketService.saveBasket({ basketData: $scope.basket }, successSBCB, failureSBCB);
        }

        //Agent Journals
        $scope.saveAgentJournal = function () {
            basketService.saveAgentJournal(JSON.stringify({ "QuoteRef": "XXXXX", "JournalType": "Bag", "JournalNotes": "Inbound-3 paxs selected 2 bags and Outbound-3 paxs selected 2 bags." }), agentSuccessCB, agentFailureCB)
        }

        function agentSuccessCB(response) {
            var result = response.data;
            if (result > 0) {

            }
        }

        function agentFailureCB(error) {

        }

        $scope.DeleteSelectedBaggage = function (passengerIndex, typeOfbaggage) {
            if (typeOfbaggage == "Twoway") {
                for (var index = 0; index < $scope.basket.baggage.selectedRoundTripBaggage.length; index++) {
                    if ($scope.basket.baggage.selectedRoundTripBaggage[index].passengerIndex == passengerIndex) {
                        $scope.basket.baggage.selectedRoundTripBaggage.splice(index, 1);
                    }
                }
                notificationService.baggage.selectedRoundTripBaggage = $scope.basket.baggage.selectedRoundTripBaggage;
            }
            else if (typeOfbaggage == "Outbound") {
                for (var index = 0; index < $scope.basket.baggage.OutboundSelectedBaggage.length; index++) {
                    if ($scope.basket.baggage.OutboundSelectedBaggage[index].passengerIndex == passengerIndex) {
                        $scope.basket.baggage.OutboundSelectedBaggage.splice(index, 1);
                    }
                }
                notificationService.baggage.OutboundSelectedBaggage = $scope.basket.baggage.OutboundSelectedBaggage;
            }
            else if (typeOfbaggage == "Inbound") {
                for (var index = 0; index < $scope.basket.baggage.inboundSelectedBaggage.length; index++) {
                    if ($scope.basket.baggage.inboundSelectedBaggage[index].passengerIndex == passengerIndex) {
                        $scope.basket.baggage.inboundSelectedBaggage.splice(index, 1);
                    }
                }
            }
            notificationService.baggage.inboundSelectedBaggage = $scope.basket.baggage.inboundSelectedBaggage;
        }

        function ValidateBasket(basketModel) {
            var result = {};
            var isValidInput = true;
            var errorMes = [];
            if (basketModel.basket == null) {
                isValidInput = false;
                errorMes.push('Please select flights');
                errorMes.push('Please select hotels');
                errorMes.push('Please select customer');
            }
            else {

                if (!basketModel.basket.flightsReturn.hasOwnProperty('FlightQuoteId') && (!basketModel.basket.flightsOneWayOutBound.hasOwnProperty('FlightQuoteId') || !basketModel.basket.flightsOneWayInBound.hasOwnProperty('FlightQuoteId'))) {

                    if (!basketModel.basket.flightsOneWayOutBound.hasOwnProperty('FlightQuoteId')) {
                        isValidInput = false;
                        errorMes.push('Please select flights');
                    }
                    if (!basketModel.basket.flightsReturn.hasOwnProperty('FlightQuoteId') && (basketModel.basket.flightsOneWayOutBound.hasOwnProperty('FlightQuoteId') || basketModel.basket.flightsOneWayInBound.hasOwnProperty('FlightQuoteId'))) {

                        if (!basketModel.basket.flightsOneWayOutBound.hasOwnProperty('FlightQuoteId')) {
                            isValidInput = false;
                            errorMes.push('Please select Outbound flights');
                        }

                        if (!basketModel.basket.flightsOneWayInBound.hasOwnProperty('FlightQuoteId')) {
                            isValidInput = false;
                            errorMes.push('Please select Inbound flights');
                        }

                    }
                }
                if (!basketModel.basket.hotel.hasOwnProperty('HotelCode')) {

                    isValidInput = false;
                    errorMes.push('Please select Hotels');
                }

                if (!basketModel.basket.customer.hasOwnProperty('CustomerID') || !basketModel.basket.customer.hasOwnProperty('MemberShipNo')) {
                    isValidInput = false;
                    errorMes.push('Please select customer');
                }
                result.isValidInput = isValidInput;
                result.errorMes = errorMes;
                return result;
            }
        }

        $scope.numbersOnly = function (e) {
            if ((e.which >= 48 && e.which <= 57) || e.which == 8) {
                
            }
            else {
                e.preventDefault();
            }
        }
    }
})(angular.module('SwordFish'));