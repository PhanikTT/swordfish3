﻿(function (app, $scope) {
    'use strict'

    //Service Creation
    app.factory('bookingService', bookingService);
    //DI for Service
    bookingService.$inject = ['notificationService','apiService'];
    
    //Basket Service
    function bookingService(notificationService, apiService) {
        var service = {
            getPassengers: getPassengers,            
            saveBook: saveBook,
            findPostcode: findPostcode,
            SavePassengers: SavePassengers,
            GetCardTypes: GetCardTypes,
            GetSelectedCardType: GetSelectedCardType,          
            savePayment: savePayment,
            savePaymentDetails: savePaymentDetails,
            getPaymentDetails: getPaymentDetails,
            SendQuoteAsEmailToLeadPassenger: SendQuoteAsEmailToLeadPassenger
        };

        function getPassengers(passengerDetails, adultCount, childCount, infantCount) {
            var paxCount = adultCount + childCount + infantCount;
            var passengerType = 'Adult';
            var count = 0;
            var isLeadPssenger = false;
            var minDate, maxDate;
            
            for (var i = 0; i < paxCount; i++) {
                var titleList = [];
                isLeadPssenger = false;
                var Title = null;
                if ((i + 1) <= adultCount) {
                    count++;
                    passengerType = 'Adult';
                    titleList.push("Mr");
                    titleList.push("Mrs");
                    titleList.push("Miss");
                    titleList.push("Ms");
                    minDate = new Date("1/1/1930");
                    maxDate = new Date((new Date().getFullYear() - 12),(new Date().getMonth()+1), new Date().getDate() );
                    Title = "Mr";
                    if (i == 0)
                        isLeadPssenger = true;
                }
                else if ((i + 1) <= (adultCount + childCount)) {
                    count++;
                    passengerType = 'Child';
                    titleList.push("Master");
                    titleList.push("Miss");
                    Title = "Master";
                    minDate = new Date((new Date().getFullYear() - 12), new Date().getMonth() , new Date().getDate());
                    maxDate = new Date((new Date().getFullYear() - 2), new Date().getMonth() , new Date().getDate());
                }
                else if ((i + 1) <= (paxCount)) {
                    count++;
                    passengerType = 'Infant';
                    titleList.push("Master");
                    titleList.push("Miss");
                    Title = "Master";
                    minDate = new Date((new Date().getFullYear() - 2), (new Date().getMonth() + 1), new Date().getDate());
                    maxDate = new Date();
                }
                passengerDetails.push({
                    PassengerType: passengerType,
                    Title: Title,
                    FirstName: "",
                    LastName: "",
                    DateOfBirth: "",
                    Count: count,
                    IsLeadPssenger: isLeadPssenger,
                    titleList: titleList,
                    minDate: minDate,
                    maxDate: maxDate
                });
            }
            return passengerDetails;
        }
        function findPostcode(data, successCB, failureCB) {
            apiService.post('../Booking/GetOpenPostCodeData',data, successCB, failureCB);
        }      
        function GetCardTypes(successCB, failureCB) {
            apiService.get('../Booking/GetCardTypes', successCB, failureCB);
        }
        function GetSelectedCardType(data, successCB, failureCB) {
            apiService.post('../Booking/ChangeCardTypePrice',data, successCB, failureCB);
        }
        function SavePassengers(data, successCB, failureCB) {
            apiService.post("../SearchResults/SavePassengers", data, successCB, failureCB);
        }
        function savePayment(data, successPCB, failurePCB) {
            apiService.post('../Booking/SavePayment', data, successPCB, failurePCB);
        }
        function savePaymentDetails(data, successPDCB, failurePDCB) {
            apiService.post('../Booking/SavePayment', data, successPDCB, failurePDCB);
        }
        function getPaymentDetails(data, successGPDCB, failureGPDCB) {
            apiService.post('../Booking/GetPaymentDetails', data, successGPDCB, failureGPDCB);
        }
        function saveBook(data, successBCB, failureBCB) {
            apiService.post('../Booking/SaveBook', data, successBCB, failureBCB);
        }
        function SendQuoteAsEmailToLeadPassenger(data, successCB, failureCB) {
            apiService.post("../Booking/SendQuoteAsEmailToLeadPassenger", data, successCB, failureCB);
        }
        return service;
    }
})(angular.module('SwordFish'));