﻿(function (app) {
    'use strict'

    app.directive('bookingConfirmationWidget', function () {
        return {
            restrict: 'E',
            replace: true,
            templateUrl: './../Scripts/Custom/Booking/Directives/bookingConfirmationwidget.html',
        };
    })

})(angular.module('SwordFish'));