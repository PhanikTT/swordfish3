﻿(function (app) {
    'use strict'

    //Controller Creation
    app.controller('bookingCtrl', bookingCtrl);

    bookingCtrl.$inject = ['$scope', 'bookingService', 'notificationService', 'validationService', 'utilityService', '$sce'];

    //Booking Controller
    function bookingCtrl($scope, bookingService, notificationService, validationService, utilityService, $sce) {
        $scope.manualCustomerAddress = false;
        $scope.customerAddressSelected = false;
        $scope.manualPaymentAddress = false;
        $scope.isPassengerSaveStatus = false;
        var paymentTotal = 0;
        var chat = $.connection.chatHub;
        chat.client.sendMessage = function (name, message) {
            notificationService.validationResult.showPopup = false;
            var quoteHeaderID = name;
            if ($scope.basketquoteobject.quoteHeader.QuoteHeadderId == quoteHeaderID) {
                if (message == "Approved") {
                    var message = "Customer accepted booking details, proceed to booking.";
                }
                else {
                    var message = "Customer rejected booking details, Please review booking details and send the Live Share again.";
                }
                fnOpenAlert(message);
            }
        }
        // Start the connection
        $.connection.hub.start().done(function () {
            chat.server.registerConId($('#displayname').val());
            $("#hdn_clientID").val($.connection.hub.id);
        });
        $scope.ispPay = false;
        $scope.PaymentDetail = notificationService.bookingModel.paymentDetail;
        $scope.PaymentDetails = [];
        var passengerCount = 0;
        $scope.getQuoteModel = function (searchData) {
            $scope.contactDetails = {};
            $("#loader").show();
            $scope.basketquoteobject = searchData;
            $scope.basketquoteobject.CardEasyUrl = $sce.trustAsResourceUrl(searchData.CardEasyEndPoint);
            $scope.bookingModel = bookingModel;
            $scope.bookingModel.selectedAddress = {};
            $scope.GetCardTypes();
            notificationService.userInfo = searchData.UserInfo;
            $scope.bookingModel.isSameAddress = false;
            bookingService.getPassengers($scope.bookingModel.passengerDetails, $scope.basketquoteobject.searchModel.AdultsCount, $scope.basketquoteobject.searchModel.ChildrenCount, $scope.basketquoteobject.searchModel.InfantsCount);
            $scope.bookingModel.passengerDetails[0].Title = $scope.basketquoteobject.quoteHeader.CustomerTitle;
            $scope.bookingModel.passengerDetails[0].FirstName = $scope.basketquoteobject.quoteHeader.CustomerFirstName;
            $scope.bookingModel.passengerDetails[0].LastName = $scope.basketquoteobject.quoteHeader.CustomerLastName;
            $scope.PaymentDetail.firstName = $scope.basketquoteobject.quoteHeader.CustomerFirstName;
            $scope.PaymentDetail.lastName = $scope.basketquoteobject.quoteHeader.CustomerLastName;
            $scope.PaymentDetail.name = $scope.basketquoteobject.quoteHeader.CustomerFirstName + ' ' + $scope.basketquoteobject.quoteHeader.CustomerLastName;
            $scope.contactDetails.Telephone = ($scope.basketquoteobject.quoteHeader.TelephoneNumber == "") ? "0044" : $scope.basketquoteobject.quoteHeader.TelephoneNumber;
            $scope.contactDetails.Mobile = $scope.basketquoteobject.quoteHeader.MobileNumber;
            $scope.contactDetails.Email = $scope.basketquoteobject.quoteHeader.CustomerEmailAddress;
            $scope.contactDetails.PostCode = $scope.basketquoteobject.quoteHeader.PostCode;
            $scope.contactDetails.Address1 = $scope.basketquoteobject.quoteHeader.Address;
            $scope.contactDetails.HouseNumber = $scope.basketquoteobject.quoteHeader.HouseNumber;
            $scope.contactDetails.CountryCode = $scope.basketquoteobject.quoteHeader.CountryCode;
            $scope.PaymentDetail.address = $scope.basketquoteobject.quoteHeader.Address;
            $scope.contactDetails.Address2 = $scope.basketquoteobject.quoteHeader.City;
            $scope.contactDetails.CityTown = $scope.basketquoteobject.quoteHeader.City;
            $scope.PaymentDetail.city = $scope.basketquoteobject.quoteHeader.City;
            $scope.PaymentDetail.country = $scope.basketquoteobject.quoteHeader.Country;
            $scope.PaymentDetail.houseNumber = $scope.basketquoteobject.quoteHeader.HouseNumber;
            $scope.contactDetails.PostCode = $scope.basketquoteobject.quoteHeader.PostCode;
            $scope.PaymentDetail.postCode = $scope.basketquoteobject.quoteHeader.PostCode;
            $scope.bookingModel.dueDaysToTravel = utilityService.dateDiff(moment().format('YYYY-MM-DD'), $scope.basketquoteobject.searchModel.startDate);
            $scope.title = 'Mr';
            $scope.bookingModel.paymentDetail.cost = $scope.basketquoteobject.quoteHeader.GrandTotal;
            $scope.bookingModel.paymentDetail.firstName = '';
            $scope.bookingModel.paymentDetail.lastName = '';
            $scope.bookingModel.paymentDetail.name = '';
            $scope.bookingModel.paymentDetail.address = '';
            $scope.bookingModel.paymentDetail.city = '';
            $scope.bookingModel.paymentDetail.postCode = '';
            $scope.bookingModel.paymentDetail.houseNumber = '';
            $scope.contactDetails.Country = $scope.basketquoteobject.quoteHeader.Country;
            passengerCount = $scope.basketquoteobject.searchModel.AdultsCount + $scope.basketquoteobject.searchModel.ChildrenCount + $scope.basketquoteobject.searchModel.InfantsCount;
            DisplayBaggage();
            bookingValidationRules();
            $("#loader").hide();

        }
        $scope.GetCardTypes = function () {
            bookingService.GetCardTypes(failure, success);
        };
        $scope.GetSelectedCardType = function (cardTypeId, TotalFare) {
            $scope.calculateCradCharges();
            if (cardTypeId != undefined && TotalFare != undefined) {
                bookingService.GetSelectedCardType({ "cardTypeId": cardTypeId, "TotalFare": bookingModel.paymentDetail.cost }, successsct, failuresct);
            } else {
                $scope.bookingModel.paymentDetail.totalCost = "";
                $scope.bookingModel.paymentDetail.cardCharges = "";
                $scope.bookingModel.paymentDetail.paymentRule = "";
            }
        }
        function success(result) {
            if (result.status = 200 && result.data != null) {
                var cardTypes = [];
                $scope.bookingModel.cardTypes = result.data;
                $scope.bookingModel.defaultCardType = "Card Type";
                //$scope.CustomerModel.title = "Mr";
            }
        }
        function failure(error) {
            console.log(error);
            // $scope.CustomerModel.hdnMembershipNo = "";
        }

        $scope.clockticker = function () {
            //function countdown(elementName, minutes, seconds) {
            var element, endTime, hours, mins, msLeft, time;
            function twoDigits(n) {
                return (n <= 9 ? "0" + n : n);
            }
            function updateTimer() {
                msLeft = endTime - (+new Date);
                if (msLeft < 1000) {
                    var data = new Object();
                    element.innerHTML = $scope.basketquoteobject.PageExpireMsg;
                    //window.location = '../Home/DashBoard';                       
                } else {
                    time = new Date(msLeft);
                    hours = time.getUTCHours();
                    mins = time.getUTCMinutes();
                    element.innerHTML = (hours ? hours + ':' + twoDigits(mins) : mins) + ':' + twoDigits(time.getUTCSeconds());
                    setTimeout(updateTimer, time.getUTCMilliseconds() + 500);
                }
            }

            element = document.getElementById("timer");
            endTime = (+new Date) + 1000 * (60 * 30 + 0) + 500;
            updateTimer();



        }
        // clockticker("timer", 1, 5);
        $scope.depositCalculate = function () {
            if ($scope.bookingModel.isDeposit) {

                var dateDiff = utilityService.dateDiff($scope.basketquoteobject.searchModel.startDate, $scope.basketquoteobject.hotelComponent.CheckOutDate);
                if (dateDiff > 0) {
                    $scope.bookingModel.depositMinAmount = (parseFloat($scope.basketquoteobject.quoteHeader.FlightCost) + parseFloat(($scope.basketquoteobject.quoteHeader.HotelCost + $scope.basketquoteobject.quoteHeader.FidelityCost) / dateDiff));
                    $scope.bookingModel.depositMinAmount = parseFloat($scope.bookingModel.depositMinAmount).toFixed(2);
                    $scope.bookingModel.paymentDetail.cost = ($scope.bookingModel.depositMinAmount - parseFloat(paymentTotal)).toFixed(2);
                }
                depositBookingRules();
            }
            else {
                $scope.bookingModel.depositMinAmount = '';
                $scope.bookingModel.paymentDetail.cost = (parseFloat($scope.basketquoteobject.quoteHeader.GrandTotal) - parseFloat(paymentTotal)).toFixed(2);
            }
            $scope.calculateCradCharges();
            $scope.GetSelectedCardType($scope.CardTypeSelected.PaymentTypeID, $scope.bookingModel.paymentDetail.cost);
        }
        function DisplayBaggage() {
            $scope.total = 0;
            $scope.roundTripBags = [];
            $scope.inbound = [];
            $scope.outbound = [];
            $scope.displayBaggageComponent = [];
            if ($scope.basketquoteobject.bagsComponents != undefined && $scope.basketquoteobject.bagsComponents.length > 0) {
                $scope.isContainBags = false;
                $scope.displayBaggageComponent = angular.copy($scope.basketquoteobject.bagsComponents);
                angular.forEach($scope.displayBaggageComponent, function (value, key) {
                    $scope.total += value.SellingPrice;
                });
                if ($scope.basketquoteobject.flightComponentUI.flightType == "Flight Return") {
                    $scope.roundTripBags = AssignBaggsToPassengers($scope.displayBaggageComponent, "Flight Return");
                }
                else {
                    $scope.outbound = AssignBaggsToPassengers($scope.displayBaggageComponent, "Outbound");
                    $scope.inbound = AssignBaggsToPassengers($scope.displayBaggageComponent, "Inbound");
                }
            }
            else {
                $scope.isContainBags = true;
            }
            return $scope.total;
        }

        function AssignBaggsToPassengers(bags, direction) {
            var result = [];

            if (bags.length > 0) {
                for (var passengerIndex = 1; passengerIndex <= passengerCount; passengerIndex++) {
                    for (var bagsIndex = 0; bagsIndex < bags.length; bagsIndex++) {
                        if (bags[bagsIndex].Direction == direction && bags[bagsIndex].PassengerIndex == passengerIndex && bags[bagsIndex].BagsType.toLowerCase() == "baggage") {
                            result.push(bags[bagsIndex]);
                        }
                    }
                    if (result.length != passengerIndex) {
                        result.push({ BagsDescription: "<span class='text-danger'>Baggage not selected.</span>", Direction: direction })
                    }
                }
            }
            return result;
        }
        $scope.getPaymentDetails = function () {
            bookingService.getPaymentDetails({ quoteHeaderId: $scope.basketquoteobject.quoteHeader.QuoteHeadderId }, successGPDCB, failureGPDCB);
        }
        function successGPDCB(result) {
            if (result.status = 200 && result.data != null && result.data.length>0) {
                $scope.PaymentDetails = result.data;
                $scope.bookingModel.isDepositDisabled = true;
                var paymentTotal = $scope.getPaymentTotal();
                if ($scope.bookingModel.isDeposit) {
                    var depositAmount = parseFloat($scope.bookingModel.depositMinAmount);
                    if (parseFloat(paymentTotal) == depositAmount) {
                        $scope.bookingModel.quoteOverride.isPaymentDone = true;
                        notificationService.bookingModel.paymentDetail = {};
                    }
                    else {
                        var pDetails = notificationService.bookingModel.paymentDetail;
                        notificationService.bookingModel.paymentDetail = {};
                        notificationService.bookingModel.paymentDetail.name = pDetails.name;
                        notificationService.bookingModel.paymentDetail.houseNumber = pDetails.houseNumber;
                        notificationService.bookingModel.paymentDetail.address = pDetails.address;
                        notificationService.bookingModel.paymentDetail.city = pDetails.city;
                        notificationService.bookingModel.paymentDetail.country = pDetails.country;
                        notificationService.bookingModel.paymentDetail.postCode = pDetails.postCode;
                        notificationService.bookingModel.paymentDetail.cost = (parseFloat(depositAmount) - parseFloat(paymentTotal)).toFixed(2);
                        $scope.GetSelectedCardType($scope.CardTypeSelected.PaymentTypeID, notificationService.bookingModel.paymentDetail.cost);
                    }
                }
                else {
                    if (parseFloat(paymentTotal) == parseFloat($scope.basketquoteobject.quoteHeader.GrandTotal)) {
                        $scope.bookingModel.quoteOverride.isPaymentDone = true;
                        notificationService.bookingModel.paymentDetail = {};
                    }
                    else {
                        var pDetails = notificationService.bookingModel.paymentDetail;
                        notificationService.bookingModel.paymentDetail = {};
                        notificationService.bookingModel.paymentDetail.name = pDetails.name;
                        notificationService.bookingModel.paymentDetail.houseNumber = pDetails.houseNumber;
                        notificationService.bookingModel.paymentDetail.address = pDetails.address;
                        notificationService.bookingModel.paymentDetail.city = pDetails.city;
                        notificationService.bookingModel.paymentDetail.country = pDetails.country;
                        notificationService.bookingModel.paymentDetail.postCode = pDetails.postCode;
                        notificationService.bookingModel.paymentDetail.cost = (parseFloat($scope.basketquoteobject.quoteHeader.GrandTotal) - parseFloat(paymentTotal)).toFixed(2);
                        $scope.GetSelectedCardType($scope.CardTypeSelected.PaymentTypeID, notificationService.bookingModel.paymentDetail.cost);
                    }
                }
            }
        }
        function failureGPDCB(error) {
            notificationService.validationResult = { showPopup: true, errorMsg: ["Unable to get Payment details. Please try again."], title: 'Payment status.' };
        }
        $scope.addPaymentDetail = function () {
            $("#loader").show();
            var env = notificationService.userInfo.Environment;
            //env = "sqa";
            if (env === 'sandbox' || env === 'dev') {
                var validationResult = ValidatePaymentResponse(notificationService.bookingModel.paymentDetail.cardEasyResponse);
                if (validationResult.isValidInput == true) {
                    var paymentDetails = notificationService.bookingModel.paymentDetail.cardEasyResponse;
                    notificationService.bookingModel.paymentDetail.quoteHeaderId = $scope.basketquoteobject.quoteHeader.QuoteHeadderId;
                    notificationService.bookingModel.paymentDetail.cardNumber = '12345';
                    notificationService.bookingModel.paymentDetail.cardType = $scope.CardTypeSelected.PaymentType;
                    notificationService.bookingModel.paymentDetail.merchantRefrence = returnPaymentValue(paymentDetails.split(/\r\n|\r|\n/g)[0]);
                    notificationService.bookingModel.paymentDetail.transactionId = returnPaymentValue(paymentDetails.split(/\r\n|\r|\n/g)[1]);
                    notificationService.bookingModel.paymentDetail.resultDatetime = returnPaymentValue(paymentDetails.split(/\r\n|\r|\n/g)[2]);
                    notificationService.bookingModel.paymentDetail.processingdb = returnPaymentValue(paymentDetails.split(/\r\n|\r|\n/g)[3]);
                    notificationService.bookingModel.paymentDetail.errormsg = returnPaymentValue(paymentDetails.split(/\r\n|\r|\n/g)[4]);
                    notificationService.bookingModel.paymentDetail.merchantNumber = returnPaymentValue(paymentDetails.split(/\r\n|\r|\n/g)[5]);
                    notificationService.bookingModel.paymentDetail.tid = returnPaymentValue(paymentDetails.split(/\r\n|\r|\n/g)[6]);
                    notificationService.bookingModel.paymentDetail.schemeName = returnPaymentValue(paymentDetails.split(/\r\n|\r|\n/g)[7]);
                    notificationService.bookingModel.paymentDetail.messageNumber = returnPaymentValue(paymentDetails.split(/\r\n|\r|\n/g)[8]);
                    notificationService.bookingModel.paymentDetail.authCode = returnPaymentValue(paymentDetails.split(/\r\n|\r|\n/g)[9]);
                    notificationService.bookingModel.paymentDetail.authMessage = returnPaymentValue(paymentDetails.split(/\r\n|\r|\n/g)[10]);
                    notificationService.bookingModel.paymentDetail.vertel = returnPaymentValue(paymentDetails.split(/\r\n|\r|\n/g)[11]);
                    notificationService.bookingModel.paymentDetail.txnResult = returnPaymentValue(paymentDetails.split(/\r\n|\r|\n/g)[12]);
                    notificationService.bookingModel.paymentDetail.pcavsResult = returnPaymentValue(paymentDetails.split(/\r\n|\r|\n/g)[13]);
                    notificationService.bookingModel.paymentDetail.ad1avsResult = returnPaymentValue(paymentDetails.split(/\r\n|\r|\n/g)[14]);
                    notificationService.bookingModel.paymentDetail.cvcResult = returnPaymentValue(paymentDetails.split(/\r\n|\r|\n/g)[15]);
                    notificationService.bookingModel.paymentDetail.arc = returnPaymentValue(paymentDetails.split(/\r\n|\r|\n/g)[16]);
                    notificationService.bookingModel.paymentDetail.iadarc = returnPaymentValue(paymentDetails.split(/\r\n|\r|\n/g)[17]);
                    notificationService.bookingModel.paymentDetail.iadoad = returnPaymentValue(paymentDetails.split(/\r\n|\r|\n/g)[18]);
                    notificationService.bookingModel.paymentDetail.isd = returnPaymentValue(paymentDetails.split(/\r\n|\r|\n/g)[19]);
                    notificationService.bookingModel.paymentDetail.surcharge = notificationService.bookingModel.paymentDetail.cardCharges;
                    notificationService.bookingModel.paymentDetail.totalprice = notificationService.bookingModel.paymentDetail.totalCost;
                    notificationService.bookingModel.paymentDetail.authorisingentity = returnPaymentValue(paymentDetails.split(/\r\n|\r|\n/g)[20]);
                    notificationService.bookingModel.paymentDetail.depositType = $scope.bookingModel.isDeposit ? 'DEPOSIT' : 'FULL_BALANCE';
                    if ($scope.bookingModel.isDeposit == true)
                    {
                        depositBookingRules();
                    }
                    bookingService.savePayment({ paymentDetail: notificationService.bookingModel.paymentDetail }, successPCB, failurePCB);                    
                    $scope.ispPay = false;
                    $("#loader").hide();
                    $("#syntecGateway").attr("src", $scope.basketquoteobject.CardEasyUrl);
                }
                else {
                    notificationService.validationResult = { showPopup: !validationResult.isValidInput, errorMsg: validationResult.errorMes, title: 'Validation Result' };
                    $("#loader").hide();
                    return;
                }


            }
            else {
                //Get payment details based on quoteHeaderId
                //$scope.PaymentDetails
                $scope.getPaymentDetails();
                $("#loader").hide();
                return;
            }
        }

        function failuresct() {
        }
        function successsct(result) {
            if (result.data != '' && result.data != undefined) {
                var cardtyperesult = result.data;
                $scope.bookingModel.paymentDetail.totalCost = result.data.Price.toFixed(2);
                $scope.bookingModel.paymentDetail.cardCharges = result.data.Description;
                $scope.bookingModel.paymentDetail.paymentRule = result.data.CardChargesDesc;
                $scope.bookingModel.paymentDetail.cardTypeCode = result.data.CardTypeCode;
            }
        }
        function successPCB(result) {
            if (result.data > 0) {
                $scope.bookingModel.isDepositDisabled = true;
                $scope.PaymentDetails.push(notificationService.bookingModel.paymentDetail);
                var paymentTotal = $scope.getPaymentTotal().toFixed(2);
                if ($scope.bookingModel.isDeposit) {
                    var depositAmount = parseFloat($scope.bookingModel.depositMinAmount);
                    if (parseFloat(paymentTotal) == depositAmount) {
                        $scope.bookingModel.quoteOverride.isPaymentDone = true;
                        notificationService.bookingModel.paymentDetail = {};
                    }
                    else {
                        var pDetails = notificationService.bookingModel.paymentDetail;
                        notificationService.bookingModel.paymentDetail = {};
                        notificationService.bookingModel.paymentDetail.name = pDetails.name;
                        notificationService.bookingModel.paymentDetail.houseNumber = pDetails.houseNumber;
                        notificationService.bookingModel.paymentDetail.address = pDetails.address;
                        notificationService.bookingModel.paymentDetail.city = pDetails.city;
                        notificationService.bookingModel.paymentDetail.country = pDetails.country;
                        notificationService.bookingModel.paymentDetail.postCode = pDetails.postCode;
                        notificationService.bookingModel.paymentDetail.cost = (parseFloat(depositAmount) - parseFloat(paymentTotal)).toFixed(2);
                        $scope.GetSelectedCardType($scope.CardTypeSelected.PaymentTypeID, notificationService.bookingModel.paymentDetail.cost);
                    }
                }
                else {
                    if (parseFloat(paymentTotal) == parseFloat($scope.basketquoteobject.quoteHeader.GrandTotal)) {
                        $scope.bookingModel.quoteOverride.isPaymentDone = true;
                        notificationService.bookingModel.paymentDetail = {};
                    }
                    else {
                        var pDetails = notificationService.bookingModel.paymentDetail;
                        notificationService.bookingModel.paymentDetail = {};
                        notificationService.bookingModel.paymentDetail.name = pDetails.name;
                        notificationService.bookingModel.paymentDetail.houseNumber = pDetails.houseNumber;
                        notificationService.bookingModel.paymentDetail.address = pDetails.address;
                        notificationService.bookingModel.paymentDetail.city = pDetails.city;
                        notificationService.bookingModel.paymentDetail.country = pDetails.country;
                        notificationService.bookingModel.paymentDetail.postCode = pDetails.postCode;
                        notificationService.bookingModel.paymentDetail.cost = (parseFloat($scope.basketquoteobject.quoteHeader.GrandTotal) - parseFloat(paymentTotal)).toFixed(2);
                        $scope.GetSelectedCardType($scope.CardTypeSelected.PaymentTypeID, notificationService.bookingModel.paymentDetail.cost);
                    }
                }
            }
        }

        function failurePCB() {

        }

        function returnPaymentValue(obj) {
            if (obj != null && obj != undefined) {
                obj = obj.replace(/:([^&]*)$/, '&' + '$1');
                var splitText = obj.split('&');
                if (splitText != null && splitText.length > 1)
                    return splitText[1].trim();
                else
                    return "";
            }
        }

        $scope.getPaymentTotal = function () {
            var total = 0;
            for (var i = 0; i < $scope.PaymentDetails.length; i++) {
                var payment = $scope.PaymentDetails[i];
                total += parseFloat(payment.cost);
            }
            return total;
        }
        $scope.getGrandPaymentTotal = function () {
            var total = 0;
            for (var i = 0; i < $scope.PaymentDetails.length; i++) {
                var payment = $scope.PaymentDetails[i];
                total += parseFloat(payment.totalprice);
            }
            return total;
        }
        $scope.proceedPayment = function () {
            $("#loader").hide();
            var validationResult = ValidatePayment(notificationService.bookingModel.paymentDetail);
            if (validationResult.isValidInput == true) {
                var env = notificationService.userInfo.Environment;
                var agentId = notificationService.userInfo.SyntechAccountId;
                var amount = notificationService.bookingModel.paymentDetail.cost;              
                var date = new Date();
                var cardValidFrom = ('0' + (date.getMonth() + 1)).slice(-2) + date.getFullYear().toString().substr(2, 2);
                notificationService.bookingModel.paymentDetail.cardValidFrom = cardValidFrom
                var expiryDate = notificationService.bookingModel.paymentDetail.expiryDate;
                var currency = 'GBP';
                var orderNo = $scope.basketquoteobject.quoteHeader.QuoteHeadderId;
                var name = notificationService.bookingModel.paymentDetail.name;
                var houseNo = notificationService.bookingModel.paymentDetail.houseNumber;
                var postCode =  utilityService.numberFromString(notificationService.bookingModel.paymentDetail.postCode);
                //env = "sqa";
                if (env === 'sandbox' || env === 'dev') {
                    $scope.ispPay = true;
                    $("#syntecGateway").attr("src", $scope.basketquoteobject.CardEasyUrl + "?agentid=" + agentId + "&amt=" + amount + "&expirydate=" + expiryDate + "&currency=" + currency + "&ordno=" + orderNo + "&name=" + name + "&avshouse=" + houseNo + "&avspostcode=" + postCode);
                }
                else {
                    $scope.ispPay = false;                    
                    notificationService.bookingModel.paymentDetail.quoteHeaderId = orderNo;
                    notificationService.bookingModel.paymentDetail.depositType = $scope.bookingModel.isDeposit ? 'DEPOSIT' : 'FULL_BALANCE';
                    bookingService.savePaymentDetails({ paymentDetail: notificationService.bookingModel.paymentDetail }, successPDCB, failurePDCB);
                }
            }
            else {
                notificationService.validationResult = { showPopup: !validationResult.isValidInput, errorMsg: validationResult.errorMes, title: 'Validation Result' };
                $("#loader").hide();
                return;
            }
        };

        function successPDCB(result) {
            if (result.data > 0) {
                var agentId = notificationService.userInfo.SyntechAccountId;
                var amount = notificationService.bookingModel.paymentDetail.cost;
                var expiryDate = notificationService.bookingModel.paymentDetail.expiryDate;
                var currency = 'GBP';
                var orderNo = result.data + '-' + $scope.basketquoteobject.quoteHeader.QuoteHeadderId;
                var name = notificationService.bookingModel.paymentDetail.name;
                var houseNo = notificationService.bookingModel.paymentDetail.houseNumber;
                var postCode = utilityService.numberFromString(notificationService.bookingModel.paymentDetail.postCode);
                $("#syntecGateway").attr("src", $scope.basketquoteobject.CardEasyUrl + "?agentid=" + agentId + "&amt=" + amount + "&expirydate=" + expiryDate + "&currency=" + currency + "&ordno=" + orderNo + "&name=" + name + "&avshouse=" + houseNo + "&avspostcode=" + postCode);
            }
        }

        function failurePDCB() {
            notificationService.validationResult = { showPopup: true, errorMsg: ["Failed to save payment details."], title: 'Payment status.' };
        }

        function ValidatePayment(paymentDetail) {
            var result = {};
            var isValidInput = true;
            var errorMes = [];
            if (validationService.isNullOrEmpty(notificationService.bookingModel.paymentDetail.firstName)) {
                isValidInput = false;
                errorMes.push("Please enter firstname.");
            }
            if (validationService.isNullOrEmpty(notificationService.bookingModel.paymentDetail.lastName)) {
                isValidInput = false;
                errorMes.push("Please enter lastname.");
            }
            if (validationService.isNullOrEmpty(notificationService.bookingModel.paymentDetail.name)) {
                isValidInput = false;
                errorMes.push("Please enter card holder name.");
            }
            if (validationService.isNullOrEmpty(notificationService.bookingModel.paymentDetail.houseNumber)) {
                isValidInput = false;
                errorMes.push("Please enter house number.");
            }
            if (validationService.isNullOrEmpty(notificationService.bookingModel.paymentDetail.postCode)) {
                isValidInput = false;
                errorMes.push("Please enter post code.");
            }
            if (validationService.isNullOrEmpty(notificationService.bookingModel.paymentDetail.cost)) {
                isValidInput = false;
                errorMes.push("Please enter cost.");
            }
            if (validationService.isNullOrEmpty(notificationService.bookingModel.paymentDetail.cardCharges)) {
                isValidInput = false;
                errorMes.push("Please enter card Charges.");
            }
            if (validationService.isNullOrEmpty(notificationService.bookingModel.paymentDetail.totalCost)) {
                isValidInput = false;
                errorMes.push("Please enter total Cost.");
            }
            if (validationService.isNullOrEmpty(notificationService.bookingModel.paymentDetail.cardExpiry)) {
                isValidInput = false;
                errorMes.push("Please enter Card Expiry.");
            }
            else if (notificationService.bookingModel.paymentDetail.cardExpiry.length != 6) {
                isValidInput = false;
                errorMes.push(" Card Expiry cannot be less than 6 digits");
            }
            else if (parseInt(notificationService.bookingModel.paymentDetail.cardExpiry.substring(0, 2)) > 12) {
                isValidInput = false;
                errorMes.push("Please enter valid Month.");
            } else {

                var Month = new Date().getUTCMonth().toString();
                var Year = new Date().getUTCFullYear().toString();
                var cardEXpiryYear = notificationService.bookingModel.paymentDetail.cardExpiry.toString().substr(2, 6);
                var cardEXpiryMonth = notificationService.bookingModel.paymentDetail.cardExpiry.toString().substr(0, 2);
                if (parseInt(Year) > parseInt(cardEXpiryYear)) {

                    isValidInput = false;
                    errorMes.push("Invalid Expiry year.");
                } else if (parseInt(Year) == parseInt(cardEXpiryYear) && parseInt(Month) + 1 > parseInt(cardEXpiryMonth)) {
                    isValidInput = false;
                    errorMes.push("Invalid Expiry month.");
                }
                if (isValidInput == true)
                    //notificationService.bookingModel.paymentDetail.cardExpiry = cardEXpiryYear.substring(2, 4) + cardEXpiryMonth;
                notificationService.bookingModel.paymentDetail.expiryDate = cardEXpiryYear.substring(2, 4) + cardEXpiryMonth;
            }

            result.isValidInput = isValidInput;
            result.errorMes = errorMes;
            return result;
        }

        function ValidatePaymentResponse(response) {
            var result = {};
            var isValidInput = true;
            var errorMes = [];
            if (validationService.isNullOrEmpty(response)) {
                isValidInput = false;
                errorMes.push("Please enter card easy response.");
            }
            result.isValidInput = isValidInput;
            result.errorMes = errorMes;
            return result;
        }


        function HoursBetweenDates(startDate, endDate) {
            var one_day = 1000 * 60 * 60;
            var date1_ms = startDate.getTime();
            var date2_ms = endDate.getTime();
            var difference_ms = date2_ms - date1_ms;
            var diffMins = Math.round(((difference_ms % 86400000) % 3600000) / 60000); // minutes
            return Math.round(difference_ms / one_day) + "h" + ":" + diffMins + "m";
        }
        $scope.areaType = "";
        $scope.FindAddress = function (addressCode, areaType) {
            if (areaType === "postcode") {
                $("#loader").show();
                bookingService.findPostcode({ "postCode": addressCode }, SuccessCBForFA, ErrorCBForFA);
            }
            else if (areaType === "payment") {
                $("#loader").show();
                bookingService.findPostcode({ "postCode": addressCode }, SuccessCBForFA, ErrorCBForFA);
            }
            $scope.areaType = areaType;
        }

        $scope.bindAddresData = function (postCodeData, type) {
            console.log(postCodeData);
            if (postCodeData != null && type === "postcode") {
                $scope.manualCustomerAddress = false;
                $scope.contactDetails.Address1 = postCodeData.line_1 + " " + postCodeData.line_2 + " " + postCodeData.line_3;
                $scope.contactDetails.CityTown = postCodeData.post_town;
                $scope.contactDetails.PostCode = postCodeData.postcode;
                $scope.contactDetails.HouseNumber = postCodeData.building_number;
            }
            else if (postCodeData != null && type === "payment") {
                $scope.manualPaymentAddress = false;
                $scope.bookingModel.paymentDetail.address = postCodeData.line_1 + " " + postCodeData.line_2 + " " + postCodeData.line_3;
                $scope.bookingModel.paymentDetail.city = postCodeData.post_town;
                $scope.bookingModel.paymentDetail.houseNumber = postCodeData.building_number;

            }
        }
        $scope.fn = function () {
            console.log(bookingModel.selectedAddress);
        }
        function SuccessCBForFA(result) {
            $("#loader").hide();
            if ($scope.areaType === "postcode") {
                var postCodeList = [];
                $scope.bookingModel.postCodeList = [];
                if (result.status == 200 && result.data != "NotFound" && JSON.parse(result["data"]).result != "Latitude is required") {
                    $scope.bookingModel.selectedAddress = "";
                    $scope.bookingModel.postCodeList = JSON.parse(result["data"]).result;
                    $scope.bookingModel.Error = "";
                }
                else {
                    $scope.bookingModel.Error = "Please enter valid postcode";
                    $scope.bookingModel.postCodeList = [];
                    $scope.contactDetails.Address1 = "";
                    $scope.contactDetails.CityTown = "";
                    $scope.contactDetails.PostCode = "";

                }

            }
            else if ($scope.areaType === "payment") {
                var postCodePaymentList = [];
                $scope.bookingModel.postCodePaymentList = [];
                if (result.status == 200 && result.data != "NotFound" && JSON.parse(result["data"]).result != "Latitude is required") {
                    var postCodePaymentList = [];
                    $scope.bookingModel.selectedAddress = "";
                    $scope.bookingModel.postCodePaymentList = JSON.parse(result["data"]).result;
                    $scope.bookingModel.ErrorPayment = "";
                }
                else {
                    $scope.bookingModel.ErrorPayment = "Please enter valid postcode";
                    $scope.bookingModel.postCodeList = [];
                }
            }

            $scope.areaType = "";
        }
        function ErrorCBForFA(result) {

        }

        $scope.isNumber = function (e) {
            if (!validationService.isNumber(e))
                e.preventDefault();
        }

        $scope.savePassengerDetails = function (successCB, errorCB) {
            try {
                $("#loader").show();
                var validationResult = validatePassengersData();
                if (validationResult.isValid == true) {
                    var passengersDetails = preparePostData($scope.bookingModel.passengerDetails);
                    if (passengersDetails != undefined && passengersDetails.length > 0) {
                        passengersDetails[0].sendOffersDetailsOnEmail = $scope.contactDetails.sendOffersDetailsOnEmail;
                        passengersDetails[0].sendOffersDetailsOnSMS = $scope.contactDetails.sendOffersDetailsOnSMS;
                        passengersDetails[0].sendOffersDetailsOnSMS = $scope.contactDetails.sendOffersDetailsOnSMS;
                        passengersDetails[0].HouseNumber = $scope.contactDetails.HouseNumber;
                        passengersDetails[0].Address1 = $scope.contactDetails.Address1;
                        passengersDetails[0].Address2 = $scope.contactDetails.Address2;
                        passengersDetails[0].Address3 = $scope.contactDetails.Address3;
                        passengersDetails[0].CityTown = $scope.contactDetails.CityTown;
                        passengersDetails[0].Country = $scope.contactDetails.Country;
                        passengersDetails[0].PostCode = $scope.contactDetails.PostCode;
                        passengersDetails[0].Email = $scope.contactDetails.Email;
                        passengersDetails[0].Mobile = $scope.contactDetails.CountryCode + $scope.contactDetails.Mobile;
                        passengersDetails[0].Telephone = ($scope.contactDetails.Telephone == "0044" ? "" : $scope.contactDetails.Telephone);
                        if (successCB != null && errorCB != null)
                            bookingService.SavePassengers(passengersDetails, successCB, errorCB);
                        else
                            bookingService.SavePassengers(passengersDetails, SuccessCBForSP, ErrorCBForSP);
                    }
                }
                else {
                    $("#loader").hide();
                    notificationService.validationResult = { showPopup: !validationResult.isValid, errorMsg: validationResult.errorMsg, title: 'Validation Result' };
                }

            }
            catch (e) {
                $("#loader").hide();
            }
        }
        function SuccessCBForSP(result) {
            $("#loader").hide();
            $scope.isPassengerSaveStatus = true;
            if (result.status == 200 && result.data == "True") {
                notificationService.validationResult = { showPopup: true, errorMsg: ["Live share email has been sent to customer."], title: 'Status' };
            }
        }
        function ErrorCBForSP(result) {

        }

        $scope.SendQuoteAsEmailToLeadPassenger = function () {

            var validationResult = validatePassengersData();
            $("#loader").show();
            if (validationResult.isValid == true) {
                var passengersDetails = preparePostData($scope.bookingModel.passengerDetails);
                if (passengersDetails != undefined && passengersDetails.length > 0) {
                    passengersDetails[0].sendOffersDetailsOnEmail = $scope.contactDetails.sendOffersDetailsOnEmail;
                    passengersDetails[0].sendOffersDetailsOnSMS = $scope.contactDetails.sendOffersDetailsOnSMS;
                    passengersDetails[0].sendOffersDetailsOnSMS = $scope.contactDetails.sendOffersDetailsOnSMS;
                    passengersDetails[0].HouseNumber = $scope.contactDetails.HouseNumber;
                    passengersDetails[0].Address1 = $scope.contactDetails.Address1;
                    passengersDetails[0].Address2 = $scope.contactDetails.Address2;
                    passengersDetails[0].Address3 = $scope.contactDetails.Address3;
                    passengersDetails[0].Country = $scope.contactDetails.Country;
                    passengersDetails[0].PostCode = $scope.contactDetails.PostCode;
                    passengersDetails[0].CityTown = $scope.contactDetails.CityTown;
                    passengersDetails[0].Email = $scope.contactDetails.Email;
                    passengersDetails[0].Mobile = $scope.contactDetails.CountryCode + $scope.contactDetails.Mobile;
                    passengersDetails[0].Telephone = ($scope.contactDetails.Telephone == "0044" ? "" : $scope.contactDetails.Telephone);
                    bookingService.SendQuoteAsEmailToLeadPassenger(passengersDetails, SendQuoteAsEmailToLeadPassengerSuccessCB, SendQuoteAsEmailToLeadPassengerErrorCB);
                }
            }
            else {
                $("#loader").hide();
                notificationService.validationResult = { showPopup: !validationResult.isValid, errorMsg: validationResult.errorMsg, title: 'Validation Result' };
            }

        }
        function SendQuoteAsEmailToLeadPassengerSuccessCB(result) {
            $("#loader").hide();
            $scope.isPassengerSaveStatus = true;
            if (result.data.isEmailSent.split(',')[0] == 'SmsTrue' && result.data.isEmailSent.split(',')[1] == 'EmailTrue' && result.data.isPassengersSaved == true) {
                notificationService.validationResult = { showPopup: true, errorMsg: ["Booking Quote SMS and Email has been sent to the customer successfully."], title: 'Status' };
            }
            else if (result.data.isEmailSent.split(',')[0] == 'SmsTrue' && result.data.isEmailSent.split(',')[1] == 'EmailFalse' && result.data.isPassengersSaved == true) {
                notificationService.validationResult = { showPopup: true, errorMsg: ["Booking Quote SMS has been sent successfully and Email has been sent failed."], title: 'Status' };
            }
            else if (result.data.isEmailSent.split(',')[0] == 'SmsFalse' && result.data.isEmailSent.split(',')[1] == 'EmailTrue' && result.data.isPassengersSaved == true) {
                notificationService.validationResult = { showPopup: true, errorMsg: ["Booking Quote Email has been sent successfully and SMS has been sent failed."], title: 'Status' };
            }
            else if (result.data.isPassengersSaved == false) {
                notificationService.validationResult = { showPopup: true, errorMsg: ["Passenger details could not be saved and hence could not send e-mail."], title: 'Eror' };
            }
            else if (result.data.isEmailSent == false) {
                notificationService.validationResult = { showPopup: true, errorMsg: ["E-mail could not be send because of internal error."], title: 'Eror' };
            }

        }
        function SendQuoteAsEmailToLeadPassengerErrorCB(error) {

        }

        function preparePostData(passengers) {
            var passengerDetails = {};
            var passengersDetails = [];
            angular.forEach(passengers, function (value, key) {
                passengersDetails.push(
                {
                    Title: value.Title,
                    PassengerType: value.PassengerType,
                    FirstName: value.FirstName,
                    LastName: value.LastName,
                    DateOfBirth: (value.DateOfBirth != null && value.DateOfBirth != "") ? value.DateOfBirth.date() + "/" + (value.DateOfBirth.month() + 1) + "/" + value.DateOfBirth.year() : null,
                    SeqNo: value.Count,
                    IsLeadPssenger: value.IsLeadPssenger,
                    sendOffersDetailsOnEmail: null,
                    sendOffersDetailsOnSMS: null,
                    QuoteRef: $scope.basketquoteobject.quoteHeader.CustomerReferenceCode,
                    Address1: null,
                    Address2: null,
                    Address3: null,
                    PostCode: null,
                    HouseNumber:null,
                    QouteHeaderID: $scope.basketquoteobject.quoteHeader.QuoteHeadderId,
                    Email: null,
                    UserID: $scope.basketquoteobject.quoteHeader.agentid
                }
               );
            });
            return passengersDetails;
        }


        function validatePassengersData() {
            var result = {};
            var isValid = true;
            var errorMsg = [];
            angular.forEach($scope.bookingModel.passengerDetails, function (value, key) {
                if (value.Title == "" || value.Title == undefined) {
                    isValid = false;
                    checkMsgISExistOrNot(errorMsg, "Please select title");
                }
                if (value.FirstName == "" || value.FirstName == undefined) {
                    isValid = false;
                    checkMsgISExistOrNot(errorMsg, "Please enter firstname");
                }
                if (value.LastName == "" || value.LastName == undefined) {
                    isValid = false;
                    checkMsgISExistOrNot(errorMsg, "Please enter lastname");
                }
            });
            if ($scope.contactDetails.Address1 == "" || $scope.contactDetails.Address1 == undefined) {
                isValid = false;
                errorMsg.push("Please enter address");
            }
            if ($scope.contactDetails.Email == "" || $scope.contactDetails.Email == undefined) {
                isValid = false;
                errorMsg.push("Please enter email");
            }
            if ($scope.contactDetails.HouseNumber == "" || $scope.contactDetails.HouseNumber == undefined) {
                isValid = false;
                errorMsg.push("Please enter house number.");
            }
            if ($scope.contactDetails.Mobile == "" || $scope.contactDetails.Mobile == undefined) {
                isValid = false;
                errorMsg.push("Please enter mobile number");
            }
            if ($scope.contactDetails.CityTown == "" || $scope.contactDetails.CityTown == undefined) {
                isValid = false;
                errorMsg.push("Please enter city/town");
            }
            else if ($scope.contactDetails.Mobile.length < 10) {
                isValid = false;
                errorMsg.push("Please enter correct mobile number");
            }
            else if ($scope.contactDetails.Mobile.length > 0 && !validationService.validateMobile($scope.contactDetails.Mobile)) {
                isValid = false;
                errorMsg.push("Please enter only numbers in mobile number");
            }
            if ($scope.contactDetails.Telephone != "" && $scope.contactDetails.Telephone != undefined && $scope.contactDetails.Telephone.length < 10 && $scope.contactDetails.Telephone!="0044") {
                isValid = false;
                errorMsg.push("Please enter correct telephone number");
            }
            if ($scope.contactDetails.PostCode == "" || $scope.contactDetails.PostCode == undefined) {
                isValid = false;
                errorMsg.push("Please enter postcode");
            }

            result.isValid = isValid;
            result.errorMsg = errorMsg;
            return result;
        }

        function checkMsgISExistOrNot(array, msg) {
            if (array.indexOf(msg) == -1)
                array.push(msg);
        }
        $scope.saveBook = function () {
            // need to set real time values
            $("#loader").show();
            if ($scope.isPassengerSaveStatus) {
                if ($scope.bookingModel.quoteOverride.isOverride) {
                    var validationResult = validateOverrideData();
                    if (validationResult.isValid == true) {
                        notificationService.bookingModel.quoteOverride.quoteHeaderId = $scope.basketquoteobject.quoteHeader.QuoteHeadderId;
                        notificationService.bookingModel.quoteOverride.totalAmount = $scope.basketquoteobject.quoteHeader.GrandTotal;
                        notificationService.bookingModel.quoteOverride.depositAmount = $scope.bookingModel.depositMinAmount;
                        notificationService.bookingModel.quoteOverride.amountPaid = $scope.getPaymentTotal();
                        if ($scope.bookingModel.isDeposit) {
                            notificationService.bookingModel.quoteOverride.amountDue = (parseFloat(notificationService.bookingModel.quoteOverride.depositAmount) - parseFloat(notificationService.bookingModel.quoteOverride.amountPaid));
                        }
                        else {
                            notificationService.bookingModel.quoteOverride.amountDue = (parseFloat(notificationService.bookingModel.quoteOverride.totalAmount) - parseFloat(notificationService.bookingModel.quoteOverride.amountPaid));
                        }
                        notificationService.bookingModel.quoteOverride.dateOfTravel = $scope.basketquoteobject.flightComponentUI.flightComponentsOutbound[0].DepartureDateTime;
                        notificationService.bookingModel.quoteOverride.agentId = $scope.basketquoteobject.quoteHeader.AgentId;
                        notificationService.bookingModel.quoteOverride.createBy = $scope.basketquoteobject.quoteHeader.AgentId;
                        bookingService.saveBook({ quoteOverride: notificationService.bookingModel.quoteOverride }, successBCB, failureBCB);
                    }
                    else {
                        notificationService.validationResult = { showPopup: !validationResult.isValid, errorMsg: validationResult.errorMsg, title: 'Validation Result' };
                    }
                }
                RedirectToBookingFulfilment();
            }
            else {
                notificationService.validationResult = { showPopup: true, errorMsg: ["Passenger details are not saved. Please perform Live share or Send Quote."], title: 'Validation Result' };
            }
            $("#loader").hide();
        }
        function RedirectToBookingFulfilment() {
            $('#quoteHeaderId').val($scope.basketquoteobject.quoteHeader.QuoteHeadderId);
            $('#bookingFulfilForm').submit();
        }
        function validateOverrideData() {
            var result = {};
            var isValid = true;
            var errorMsg = [];
            if ($scope.bookingModel.quoteOverride.approverId == "" || $scope.bookingModel.quoteOverride.approverId == undefined) {
                isValid = false;
                errorMsg.push("Please enter approver name.");
            }
            if ($scope.bookingModel.quoteOverride.comments == "" || $scope.bookingModel.quoteOverride.comments == undefined) {
                isValid = false;
                errorMsg.push("Please enter comments.");
            }
            result.isValid = isValid;
            result.errorMsg = errorMsg;
            return result;
        }
        function successBCB(result) {
            try {
                if (result.data != null) {
                    if (result.data === 'True') {
                        $("#loader").hide();
                        notificationService.validationResult = { showPopup: true, errorMsg: ["Booking fulfillment."], title: 'Success' };
                    }
                    else {
                        $("#loader").hide();
                        notificationService.validationResult = { showPopup: true, errorMsg: ["Quote overriden fail."], title: 'Fail' };
                    }
                }
                $scope.bookingModel.quoteOverride.isOverride = false;
                $scope.bookingModel.quoteOverride.approverId = '';
                $scope.bookingModel.quoteOverride.comments = '';
            } catch (e) {

            }
        }
        function failureBCB() {

        }
        $scope.setCardCharges = function (paymentRule, price) {
            $scope.bookingModel.paymentDetail.cardCharges = price;
            $scope.bookingModel.paymentDetail.paymentRule = paymentRule;
            if (parseFloat($scope.bookingModel.paymentDetail.cost) > 0) {
                $scope.calculateCradCharges();
            }
        }
        $scope.calculateCradCharges = function () {
            var validationResult = validateCost();
            if (validationResult.isValid == true) {
                if ($scope.bookingModel.paymentDetail.paymentRule == 'PCT') {
                    var charge = ($scope.bookingModel.paymentDetail.cost * $scope.bookingModel.paymentDetail.cardCharges) / 100;
                    $scope.bookingModel.paymentDetail.totalCost = parseFloat($scope.bookingModel.paymentDetail.cost) + parseFloat(charge);
                }
                if ($scope.bookingModel.paymentDetail.paymentRule == 'FRV') {
                    $scope.bookingModel.paymentDetail.totalCost = parseFloat($scope.bookingModel.paymentDetail.cost) + parseFloat($scope.bookingModel.paymentDetail.cardCharges);
                }
            }
            else {
                notificationService.validationResult = { showPopup: !validationResult.isValid, errorMsg: validationResult.errorMsg, title: 'Validation Result' };
            }
        }

        function validateCost() {
            var result = {};
            var isValid = true;
            var errorMsg = [];
            paymentTotal = $scope.getPaymentTotal();
            if (parseFloat($scope.bookingModel.depositMinAmount) > 0) {
                if (parseFloat($scope.bookingModel.paymentDetail.cost) > (parseFloat($scope.bookingModel.depositMinAmount) - parseFloat(paymentTotal)).toFixed(2)) {
                    isValid = false;
                    errorMsg.push("Cost cannot be greater than deposit amount.");
                    $scope.bookingModel.paymentDetail.cost = (parseFloat($scope.bookingModel.depositMinAmount) - parseFloat(paymentTotal)).toFixed(2);
                }
            }
            else {
                if (parseFloat($scope.bookingModel.paymentDetail.cost) > (parseFloat($scope.basketquoteobject.quoteHeader.GrandTotal) - parseFloat(paymentTotal)).toFixed(2)) {
                    isValid = false;
                    errorMsg.push("Cost cannot be greater than total amount.");
                    $scope.bookingModel.paymentDetail.cost =(parseFloat($scope.basketquoteobject.quoteHeader.GrandTotal) - parseFloat(paymentTotal)).toFixed(2);
                }
            }
            result.isValid = isValid;
            result.errorMsg = errorMsg;
            return result;
        }

        $scope.overrideBooking = function () {
            if ($scope.bookingModel.quoteOverride.isOverride) {
                $scope.bookingModel.quoteOverride.isPaymentDone = true;
            }
            else {
                $scope.bookingModel.quoteOverride.isPaymentDone = false;
            }
        }

        $scope.sameAddress = function () {
            if ($scope.bookingModel.isSameAddress) {
                $scope.bookingModel.paymentDetail.firstName = $scope.basketquoteobject.quoteHeader.CustomerFirstName;
                $scope.bookingModel.paymentDetail.lastName = $scope.basketquoteobject.quoteHeader.CustomerLastName;
                $scope.bookingModel.paymentDetail.name = $scope.basketquoteobject.quoteHeader.CustomerFirstName + ' ' + $scope.basketquoteobject.quoteHeader.CustomerLastName;
                $scope.bookingModel.paymentDetail.address = $scope.contactDetails.Address1;
                $scope.bookingModel.paymentDetail.city = $scope.contactDetails.CityTown;
                $scope.bookingModel.paymentDetail.country = $scope.contactDetails.Country;
                $scope.bookingModel.paymentDetail.postCode = $scope.contactDetails.PostCode;
                $scope.bookingModel.paymentDetail.houseNumber = $scope.contactDetails.HouseNumber;

            }
            else {
                $scope.bookingModel.paymentDetail.firstName = '';
                $scope.bookingModel.paymentDetail.lastName = '';
                $scope.bookingModel.paymentDetail.name = '';
                $scope.bookingModel.paymentDetail.address = '';
                $scope.bookingModel.paymentDetail.city = '';
                $scope.bookingModel.paymentDetail.country = $scope.contactDetails.Country;
                $scope.bookingModel.paymentDetail.postCode = '';
                $scope.bookingModel.paymentDetail.houseNumber = '';
            }
        }

        $scope.removeAddressFieldsData = function (type) {
            if (type == "Passenger") {
                $scope.contactDetails.Address1 = "";
                $scope.contactDetails.Address3 = "";
                $scope.contactDetails.CityTown = "";
                $scope.contactDetails.PostCode = "";
                $scope.contactDetails.HouseNumber = "";
            }
            else if (type == "Book") {
                $scope.bookingModel.paymentDetail.houseNumber = "";
                $scope.bookingModel.paymentDetail.address = "";
                $scope.bookingModel.paymentDetail.city = "";
                $scope.bookingModel.paymentDetail.postCode = "";
            }
        }

        function bookingValidationRules() {
            if ($scope.basketquoteobject != null && $scope.basketquoteobject.SesameBookBasket != null) {
                if ((utilityService.dateDiff(moment().format('YYYY-MM-DD'), $scope.basketquoteobject.searchModel.startDate) >= $scope.basketquoteobject.SesameBookBasket.DepositDays) && $scope.basketquoteobject.hotelComponent.hotelComponentsDetails[0].NonRefundable == false) {
                    $scope.bookingModel.isDepositDisabled = false;
                    $scope.bookingModel.isHotelNonRefundable = false;
                    $scope.bookingModel.isDepositDaysSatisfied = true;

                }
                else {
                    $scope.bookingModel.isDepositDisabled = true;
                    if (utilityService.dateDiff(moment().format('YYYY-MM-DD'), $scope.basketquoteobject.searchModel.startDate) >= $scope.basketquoteobject.SesameBookBasket.DepositDays) {
                        $scope.bookingModel.isDepositDaysSatisfied = true;
                    }
                    else {
                        $scope.bookingModel.isDepositDaysSatisfied = false;
                    }

                    if ($scope.basketquoteobject.hotelComponent.hotelComponentsDetails[0].NonRefundable == false) {
                        $scope.bookingModel.isHotelNonRefundable = false;
                    }
                    else {
                        $scope.bookingModel.isHotelNonRefundable = true;
                    }
                }
            }
        }

        function depositBookingRules()
        {
            var minAmount = $scope.bookingModel.depositMinAmount;
            var totalAmount = $scope.basketquoteobject.quoteHeader.GrandTotal;
            var dueAmount = totalAmount - minAmount;
            $scope.bookingModel.depositDueAmount = dueAmount;
            var depositDays = $scope.basketquoteobject.SesameBookBasket.DepositDays;
            var journeyStartDate = $scope.basketquoteobject.searchModel.startDate.split('T')[0];
            var Duedate = new Date(journeyStartDate);
            Duedate.setDate(Duedate.getDate() - depositDays);
            $scope.bookingModel.depositDueDate = Duedate.toISOString().split('T')[0];
            notificationService.bookingModel.paymentDetail.depositDueAmount = $scope.bookingModel.depositDueAmount;
            notificationService.bookingModel.paymentDetail.depositDueDate = $scope.bookingModel.depositDueDate;
        }
    }

})(angular.module('SwordFish'));







