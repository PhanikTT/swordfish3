﻿var sevendatescount = [0];
var fourweekcount;
function GetSevenDaysDataBackWard(datecount) {
   
    $('#chartContainerweek').hide();
    var today = new Date();
    var next = sevendatescount[0] + 7;
    if (sevendatescount[0] == 0) {
        $("#btnlastSevenDaysRight").hide();
    }
    else {
        $("#btnlastSevenDaysRight").show();
    }
    var monthNames = ["January", "February", "March", "April", "May", "June","July", "August", "September", "October", "November", "December"];
    Date.DAYS_IN_MONTH = new Array(31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31)
    sevendatescount[0] = next;
    var nextday = (new Date(today.setDate(today.getDate() - next)));
    var today = new Date()
    var thisYear = today.getFullYear()
    var thisMonth = today.getMonth()
    var thisDay = today.getDate()
    var month = nextday.getMonth();
    var dayOfMonth = nextday.getDate();
    var date = new Date(nextday);
    var lastDay = new Date(date.getFullYear(), date.getMonth() + 1, 0).toString();
    lastDay = lastDay.substring(0, 15);
    var totaldays = new Date(date.getYear(), date.getMonth() + 1, 0).getDate();
    var first = today.getDate();
    var months = monthNames[today.getMonth()];
    var Day1 = 1;
    var Day2 = 2;
    var Day3 = 3;
    var Day4 = 4;
    var Day5 = 5;
    var Day6 = 6;
    var Day7 = 7;
    var dateback = (new Date(nextday.setDate(dayOfMonth)));
    var datebackmatch = (new Date(nextday.setDate(dayOfMonth))).toString().substring(0, 15);
    var matchdays = (new Date(nextday.setDate(dayOfMonth))).getDate();
    if (datebackmatch == lastDay) {
        if (dayOfMonth == 30) {
            dayOfMonth = 30;
            Day1 = 1;
            Day2 = 2;
        }
        else {
            dayOfMonth = 31;
            Day1 = 1;
            Day2 = 2;
        }
       
        var firstday = (new Date(nextday.setDate(Day1 + dayOfMonth))).toString().substring(0, 15);
        dayOfMonth = 1;
        Day2 = 1;
        Day3 = 2;
        Day4 = 3;
        Day5 = 4;
        Day6 = 5;
        Day7 = 6;
    }
    else {
        var firstday = (new Date(nextday.setDate(Day1 + dayOfMonth))).toString().substring(0, 15);
        if (firstday == lastDay) {
            dayOfMonth = 31;
            Day2 = 1;
        }
    }
    var secondday = (new Date(nextday.setDate(Day2 + dayOfMonth))).toString().substring(0, 15);
    if (firstday == lastDay) {
        dayOfMonth = 1;
        Day3 = 1;
        Day4 = 2;
        Day5 = 3;
        Day6 = 4;
        Day7 = 5;

    }
    var thirdday = (new Date(nextday.setDate(Day3 + dayOfMonth))).toString().substring(0, 15);
    if (secondday == lastDay) {
        dayOfMonth = 1;
        Day4 = 1;
        Day5 = 2;
        Day6 = 3;
        Day7 = 4;
    }
    var fourthday = (new Date(nextday.setDate(Day4 + dayOfMonth))).toString().substring(0, 15);
    if (thirdday == lastDay) {
        dayOfMonth = 1;
        Day5 = 1;
        Day6 = 2;
        Day7 = 3;
    }
    var fifthday = (new Date(nextday.setDate(Day5 + dayOfMonth))).toString().substring(0, 15);
    if (fourthday == lastDay) {
        dayOfMonth = 1;
        Day6 = 1;
        Day7 = 2;
    }
    var sixthday = (new Date(nextday.setDate(Day6 + dayOfMonth))).toString().substring(0, 15);
    if (fifthday == lastDay) {
        dayOfMonth = 1;
        Day7 = 1;
    }
    var seventhday = (new Date(nextday.setDate(Day7 + dayOfMonth))).toString().substring(0, 15);
    var actualdate = dateback.toISOString().substring(0, 10);
    var model = {
        TaskText: actualdate
    }
    var day1 = firstday.substring(0, 3);

    var day2 = secondday.substring(0, 3);

    var day3 = thirdday.substring(0, 3);

    var day4 = fourthday.substring(0, 3);

    var day5 = fifthday.substring(0, 3);

    var day6 = sixthday.substring(0, 3);

    var day7 = seventhday.substring(0, 3);

    var Day1 = firstday.substring(0, 10);

    var Day2 = secondday.substring(0, 10);

    var Day3 = thirdday.substring(0, 10);

    var Day4 = fourthday.substring(0, 10);

    var Day5 = fifthday.substring(0, 10);

    var Day6 = sixthday.substring(0, 10);

    var Day7 = seventhday.substring(0, 10);
    Originaldata = [];
    Originaldata.push(day1, day2, day3, day4, day5, day6, day7);

    PushingData = [];
    PushingData.push(Day1, Day2, Day3, Day4, Day5, Day6, Day7);

    CountData = [];
    exactdaycount = [];
    var exactresult;
    exactresultcount = [];
    var count;
    var countdetails = [0, 0, 0, 0, 0, 0, 0];
    var countwithdate = [];
    var modeldata = JSON.stringify(model);
    $.ajax({
        type: "POST",
        crossDomain: true,
        url: '../Account/GetBarChartDetails',
        cache: false,
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        data: modeldata,

        complete: function () {

        },
        success: function (response) {
            if (response != "") {

                for (var i = 0; i < response.length; i++) {
                    var paymentdate = response[i].WeekDate;
                    var paymentexactdate = paymentdate.substring(0, 9);
                    countwithdate.push(paymentexactdate.trim());
                    count = parseInt(response[i].Count);
                    var date = new Date(paymentdate);
                    var days = ['Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday'];
                    var weekday = days[date.getDay()];
                    var exactdate = paymentdate.replace("2015-09-0", "");
                    var exactday = weekday.substring(0, 3)
                    exactdaycount.push(exactday);
                    exactresult = exactday + count;
                    exactresultcount.push(exactresult);
                    CountData.push(count);

                }

                if (response[0] != undefined) {
                    //day1
                    if (exactdaycount[0] == "Mon") {
                        var indexday1 = Originaldata.indexOf("Mon");
                        if (indexday1 !== -1) {
                            countdetails[indexday1] = CountData[0];

                        }
                    }
                    else if (exactdaycount[0] == "Tue") {
                        var indexday2 = Originaldata.indexOf("Tue");
                        if (indexday2 !== -1) {
                            countdetails[indexday2] = CountData[0];

                        }
                    }
                    else if (exactdaycount[0] == "Wed") {
                        var indexday3 = Originaldata.indexOf("Wed");
                        if (indexday3 !== -1) {
                            countdetails[indexday3] = CountData[0];

                        }
                    }
                    else if (exactdaycount[0] == "Thu") {
                        var indexday4 = Originaldata.indexOf("Thu");
                        if (indexday4 !== -1) {
                            countdetails[indexday4] = CountData[0];
                        }
                    }
                    else if (exactdaycount[0] == "Fri") {
                        var indexday5 = Originaldata.indexOf("Fri");
                        if (indexday5 !== -1) {
                            countdetails[indexday5] = CountData[0];

                        }
                    }
                    else if (exactdaycount[0] == "Sat") {
                        var indexday6 = Originaldata.indexOf("Sat");
                        if (indexday6 !== -1) {
                            countdetails[indexday6] = CountData[0];

                        }
                    }
                    else if (exactdaycount[0] == "Sun") {
                        var indexday7 = Originaldata.indexOf("Sun");
                        if (indexday7 !== -1) {
                            countdetails[indexday7] = CountData[0];

                        }
                    }
                }
                if (response[1] != undefined) {
                    //day2
                    if (exactdaycount[1] == "Mon") {
                        var indexday1 = Originaldata.indexOf("Mon");
                        if (indexday1 !== -1) {
                            countdetails[indexday1] = CountData[1];
                        }
                    }
                    else if (exactdaycount[1] == "Tue") {
                        var indexday2 = Originaldata.indexOf("Tue");
                        if (indexday2 !== -1) {
                            countdetails[indexday2] = CountData[1];
                        }
                    }
                    else if (exactdaycount[1] == "Wed") {
                        var indexday3 = Originaldata.indexOf("Wed");
                        if (indexday3 !== -1) {
                            countdetails[indexday3] = CountData[1];
                        }
                    }
                    else if (exactdaycount[1] == "Thu") {
                        var indexday4 = Originaldata.indexOf("Thu");
                        if (indexday4 !== -1) {
                            countdetails[indexday4] = CountData[1];
                        }
                    }
                    else if (exactdaycount[1] == "Fri") {
                        var indexday5 = Originaldata.indexOf("Fri");
                        if (indexday5 !== -1) {
                            countdetails[indexday5] = CountData[1];
                        }
                    }
                    else if (exactdaycount[1] == "Sat") {
                        var indexday6 = Originaldata.indexOf("Sat");
                        if (indexday6 !== -1) {
                            countdetails[indexday6] = CountData[1];
                        }
                    }
                    else if (exactdaycount[1] == "Sun") {
                        var indexday7 = Originaldata.indexOf("Sun");
                        if (indexday7 !== -1) {
                            countdetails[indexday7] = CountData[1];

                        }
                    }
                }
                if (response[2] != undefined) {
                    //day3

                    if (exactdaycount[2] == "Mon") {
                        var indexday1 = Originaldata.indexOf("Mon");
                        if (indexday1 !== -1) {
                            countdetails[indexday1] = CountData[2];
                        }

                    }

                    else if (exactdaycount[2] == "Tue") {
                        var indexday2 = Originaldata.indexOf("Tue");
                        if (indexday2 !== -1) {
                            countdetails[indexday2] = CountData[2];
                        }
                    }
                    else if (exactdaycount[2] == "Wed") {
                        var indexday3 = Originaldata.indexOf("Wed");
                        if (indexday3 !== -1) {
                            countdetails[indexday3] = CountData[2];

                        }
                    }
                    else if (exactdaycount[2] == "Thu") {
                        var indexday4 = Originaldata.indexOf("Thu");
                        if (indexday4 !== -1) {
                            countdetails[indexday4] = CountData[2];

                        }
                    }
                    else if (exactdaycount[2] == "Fri") {
                        var indexday5 = Originaldata.indexOf("Fri");
                        if (indexday5 !== -1)
                            countdetails[indexday5] = CountData[2];

                    }

                    else if (exactdaycount[2] == "Sat") {
                        var indexday6 = Originaldata.indexOf("Sat");
                        if (indexday6 !== -1) {
                            countdetails[indexday6] = CountData[2];

                        }
                    }
                    else if (exactdaycount[2] == "Sun") {
                        var indexday7 = Originaldata.indexOf("Sun");
                        if (indexday7 !== -1) {
                            countdetails[indexday7] = CountData[2];
                        }
                    }
                }

                //day4
                if (response[3] != undefined) {
                    if (exactdaycount[3] == "Mon") {
                        var indexday1 = Originaldata.indexOf("Mon");
                        if (indexday1 !== -1) {
                            countdetails[indexday1] = CountData[3];
                        }

                    }

                    else if (exactdaycount[3] == "Tue") {
                        var indexday2 = Originaldata.indexOf("Tue");
                        if (indexday2 !== -1) {
                            countdetails[indexday2] = CountData[3];
                        }
                    }
                    else if (exactdaycount[3] == "Wed") {
                        var indexday3 = Originaldata.indexOf("Wed");
                        if (indexday3 !== -1) {
                            countdetails[indexday3] = CountData[3];

                        }
                    }
                    else if (exactdaycount[3] == "Thu") {
                        var indexday4 = Originaldata.indexOf("Thu");
                        if (indexday4 !== -1) {
                            countdetails[indexday4] = CountData[3];

                        }
                    }
                    else if (exactdaycount[3] == "Fri") {
                        var indexday5 = Originaldata.indexOf("Fri");
                        if (indexday5 !== -1) {
                            countdetails[indexday5] = CountData[3];

                        }
                    }
                    else if (exactdaycount[3] == "Sat") {
                        var indexday6 = Originaldata.indexOf("Sat");
                        if (indexday6 !== -1) {
                            countdetails[indexday6] = CountData[3];

                        }
                    }
                    else if (exactdaycount[3] == "Sun") {
                        var indexday7 = Originaldata.indexOf("Sun");
                        if (indexday7 !== -1) {
                            countdetails[indexday7] = CountData[3];
                        }
                    }
                }
                if (response[4] != undefined) {
                    //day5
                    if (exactdaycount[4] == "Mon") {
                        var indexday1 = Originaldata.indexOf("Mon");

                        if (indexday1 !== -1) {
                            countdetails[indexday1] = CountData[4];
                        }

                    }

                    else if (exactdaycount[4] == "Tue") {
                        var indexday2 = Originaldata.indexOf("Tue");
                        if (indexday2 !== -1) {
                            countdetails[indexday2] = CountData[4];
                        }
                    }
                    else if (exactdaycount[4] == "Wed") {
                        var indexday3 = Originaldata.indexOf("Wed");
                        if (indexday3 !== -1) {
                            countdetails[indexday3] = CountData[4];

                        }
                    }
                    else if (exactdaycount[4] == "Thu") {
                        var indexday4 = Originaldata.indexOf("Thu");
                        if (indexday4 !== -1) {
                            countdetails[indexday4] = CountData[4];

                        }
                    }
                    else if (exactdaycount[4] == "Fri") {
                        var indexday5 = Originaldata.indexOf("Fri");
                        if (indexday5 !== -1) {

                            countdetails[indexday5] = CountData[4];

                        }
                    }
                    else if (exactdaycount[4] == "Sat") {
                        var indexday6 = Originaldata.indexOf("Sat");
                        if (indexday6 !== -1) {
                            countdetails[indexday6] = CountData[4];

                        }
                    }
                    else if (exactdaycount[4] == "Sun") {
                        var indexday7 = Originaldata.indexOf("Sun");
                        if (indexday7 !== -1) {
                            countdetails[indexday7] = CountData[4];
                        }
                    }
                }
                //day6
                if (response[5] != undefined) {
                    if (exactdaycount[5] == "Mon") {
                        var indexday1 = Originaldata.indexOf("Mon");
                        if (indexday1 !== -1) {
                            countdetails[indexday1] = CountData[5];
                        }

                    }

                    else if (exactdaycount[5] == "Tue") {
                        var indexday2 = Originaldata.indexOf("Tue");
                        if (indexday2 !== -1) {
                            countdetails[indexday2] = CountData[5];
                        }
                    }
                    else if (exactdaycount[5] == "Wed") {
                        var indexday3 = Originaldata.indexOf("Wed");
                        if (indexday3 !== -1) {
                            countdetails[indexday3] = CountData[5];

                        }
                    }
                    else if (exactdaycount[5] == "Thu") {
                        var indexday4 = Originaldata.indexOf("Thu");
                        if (indexday4 !== -1) {
                            countdetails[indexday4] = CountData[5];

                        }
                    }
                    else if (exactdaycount[5] == "Fri") {
                        var indexday5 = Originaldata.indexOf("Fri");
                        if (indexday5 !== -1) {
                            countdetails[indexday5] = CountData[5];

                        }
                    }
                    else if (exactdaycount[5] == "Sat") {
                        var indexday6 = Originaldata.indexOf("Sat");

                        if (indexday6 !== -1) {
                            countdetails[indexday6] = CountData[5];

                        }
                    }
                    else if (exactdaycount[5] == "Sun") {
                        var indexday7 = Originaldata.indexOf("Sun");
                        if (indexday7 !== -1) {
                            countdetails[indexday7] = CountData[5];
                        }
                    }

                }
                if (response[6] != undefined) {
                    //day7
                    if (exactdaycount[6] == "Mon") {
                        var indexday1 = Originaldata.indexOf("Mon");
                        if (indexday1 !== -1) {
                            countdetails[indexday1] = CountData[6];
                        }

                    }

                    else if (exactdaycount[6] == "Tue") {
                        var indexday2 = Originaldata.indexOf("Tue");
                        if (indexday2 !== -1) {
                            countdetails[indexday2] = CountData[6];
                        }
                    }
                    else if (exactdaycount[6] == "Wed") {
                        var indexday3 = Originaldata.indexOf("Wed");
                        if (indexday3 !== -1) {
                            countdetails[indexday3] = CountData[6];

                        }
                    }
                    else if (exactdaycount[6] == "Thu") {
                        var indexday4 = Originaldata.indexOf("Thu");
                        if (indexday4 !== -1) {
                            countdetails[indexday4] = CountData[6];

                        }
                    }
                    else if (exactdaycount[6] == "Fri") {
                        var indexday5 = Originaldata.indexOf("Fri");
                        if (indexday5 !== -1) {
                            countdetails[indexday5] = CountData[6];

                        }
                    }
                    else if (exactdaycount[6] == "Sat") {
                        var indexday6 = Originaldata.indexOf("Sat");
                        if (indexday6 !== -1) {
                            countdetails[indexday6] = CountData[6];

                        }
                    }
                    else if (exactdaycount[6] == "Sun") {
                        var indexday7 = Originaldata.indexOf("Sun");
                        if (indexday7 !== -1) {
                            countdetails[indexday7] = CountData[6];
                        }
                    }
                }

                if (countdetails[0] == 0) {
                    countdetails[0] = null;
                }
                if (countdetails[1] == 0) {
                    countdetails[1] = null;
                }
                if (countdetails[2] == 0) {
                    countdetails[2] = null;
                }
                if (countdetails[3] == 0) {
                    countdetails[3] = null;
                }
                if (countdetails[4] == 0) {
                    countdetails[4] = null;
                }
                if (countdetails[5] == 0) {
                    countdetails[5] = null;
                }
                if (countdetails[6] == 0) {
                    countdetails[6] = null;
                }
                $('#chart_nodata').hide();
                var options = {
                    title: {
                        //text: "Column Chart using jQuery Plugin"
                    },
                    animationEnabled: true,

                    data: [
                    {

                        indexLabelFontColor: "grey",
                        indexLabelFontWeight: "bolder",
                        indexLabelPlacement: "inside",
                        indexLabelFontSize: 25,
                        indexLabelFontFamily: "Lucida Console",
                        indexLabel: "{y}",
                        type: "column", //change it to line, area, bar, pie, etc
                        dataPoints: [

                    { x: 10, y: countdetails[0], label: PushingData[0] },
                    { x: 20, y: countdetails[1], label: PushingData[1] },
                    { x: 30, y: countdetails[2], label: PushingData[2] },
                    { x: 40, y: countdetails[3], label: PushingData[3] },
                    { x: 50, y: countdetails[4], label: PushingData[4] },
                    { x: 60, y: countdetails[5], label: PushingData[5] },
                    { x: 70, y: countdetails[6], label: PushingData[6] },

                        ]

                    }
                    ]
                };

                $("#chartContainerday").CanvasJSChart(options);


            }
            else {
                $('#chart_nodata').show();
                var options = {
                    title: {
                        //text: "Column Chart using jQuery Plugin"
                    },
                    animationEnabled: true,

                    data: [
                    {
                        indexLabelFontColor: "grey",
                        indexLabelFontWeight: "bolder",
                        indexLabelPlacement: "inside",
                        indexLabelFontSize: 25,
                        indexLabelFontFamily: "Lucida Console",
                        indexLabel: "{y}",
                        type: "column", //change it to line, area, bar, pie, etc
                        dataPoints: [

                    { x: 10, y: null, label: PushingData[0] },
                    { x: 20, y: null, label: PushingData[1] },
                    { x: 30, y: null, label: PushingData[2] },
                    { x: 40, y: null, label: PushingData[3] },
                    { x: 50, y: null, label: PushingData[4] },
                    { x: 60, y: null, label: PushingData[5] },
                    { x: 70, y: null, label: PushingData[6] },

                        ]
                    }
                    ]
                };
                $("#chartContainerday").CanvasJSChart(options);
            }



        },

        error: function (xhr, status, error) {
            ShowAjaxRequestError(xhr, status, error);
        }
    });

}
function GetSevenDaysDataForward(datecount) {
    $('#chartContainerweek').hide();
    var today = new Date();
    var next = sevendatescount[0] - 7;
    sevendatescount[0] = next;
    var nextday = (new Date(today.setDate(today.getDate() - next)));


    var monthNames = ["January", "February", "March", "April", "May", "June",
         "July", "August", "September", "October", "November", "December"
    ];

    var today = new Date()
    var thisYear = today.getFullYear()
    var thisMonth = today.getMonth()
    var thisDay = today.getDate()
    var month = nextday.getMonth();
    var dayOfMonth = nextday.getDate();
    var date = new Date(nextday);
    var lastDay = new Date(date.getFullYear(), date.getMonth() + 1, 0).toString();
    lastDay = lastDay.substring(0, 15);
    var first = today.getDate();
    var matchday = today.toString().substring(0, 15);
    var months = monthNames[today.getMonth()];
    var Day1 = 1;
    var Day2 = 2;
    var Day3 = 3;
    var Day4 = 4;
    var Day5 = 5;
    var Day6 = 6;
    var Day7 = 7;
    var datePushDB = (new Date(nextday.setDate(dayOfMonth)));
    var datebackmatch = (new Date(nextday.setDate(dayOfMonth))).toString().substring(0, 15);
    if (datebackmatch == lastDay) {
        if (dayOfMonth == 30) {
            dayOfMonth = 30;
            Day1 = 1;
            Day2 = 2;
        }
        else {
            dayOfMonth = 31;
            Day1 = 1;
            Day2 = 2;
        }

        var firstday = (new Date(nextday.setDate(Day1 + dayOfMonth))).toString().substring(0, 15);
        dayOfMonth = 1;
        Day2 = 1;
        Day3 = 2;
        Day4 = 3;
        Day5 = 4;
        Day6 = 5;
        Day7 = 6;
    }
    else {
        var firstday = (new Date(nextday.setDate(Day1 + dayOfMonth))).toString().substring(0, 15);
        if (firstday == lastDay) {
            dayOfMonth = 31;
            Day2 = 1;
        }
    }
    var secondday = (new Date(nextday.setDate(Day2 + dayOfMonth))).toString().substring(0, 15);
    if (firstday == lastDay) {
        dayOfMonth = 1;
        Day3 = 1;
        Day4 = 2;
        Day5 = 3;
        Day6 = 4;
        Day7 = 5;

    }
    var thirdday = (new Date(nextday.setDate(Day3 + dayOfMonth))).toString().substring(0, 15);
    if (secondday == lastDay) {
        dayOfMonth = 1;
        Day4 = 1;
        Day5 = 2;
        Day6 = 3;
        Day7 = 4;
    }
    var fourthday = (new Date(nextday.setDate(Day4 + dayOfMonth))).toString().substring(0, 15);
    if (thirdday == lastDay) {
        dayOfMonth = 1;
        Day5 = 1;
        Day6 = 2;
        Day7 = 3;
    }
    var fifthday = (new Date(nextday.setDate(Day5 + dayOfMonth))).toString().substring(0, 15);
    if (fourthday == lastDay) {
        dayOfMonth = 1;
        Day6 = 1;
        Day7 = 2;
    }
    var sixthday = (new Date(nextday.setDate(Day6 + dayOfMonth))).toString().substring(0, 15);
    if (fifthday == lastDay) {
        dayOfMonth = 1;
        Day7 = 1;
    }
    var seventhday = (new Date(nextday.setDate(Day7 + dayOfMonth))).toString().substring(0, 15);
    if (matchday == seventhday) {
        $("#btnlastSevenDaysRight").hide();
    }
    else {
        $("#btnlastSevenDaysLeft").show();
    }
    var actualdate = datePushDB.toISOString().substring(0, 10);
    var model = {
        TaskText: actualdate
    }

    var day1 = firstday.substring(0, 3);

    var day2 = secondday.substring(0, 3);

    var day3 = thirdday.substring(0, 3);

    var day4 = fourthday.substring(0, 3);

    var day5 = fifthday.substring(0, 3);

    var day6 = sixthday.substring(0, 3);

    var day7 = seventhday.substring(0, 3);

    var Day1 = firstday.substring(0, 10);

    var Day2 = secondday.substring(0, 10);

    var Day3 = thirdday.substring(0, 10);

    var Day4 = fourthday.substring(0, 10);

    var Day5 = fifthday.substring(0, 10);

    var Day6 = sixthday.substring(0, 10);

    var Day7 = seventhday.substring(0, 10);
    Originaldata = [];
    Originaldata.push(day1, day2, day3, day4, day5, day6, day7);

    PushingData = [];
    PushingData.push(Day1, Day2, Day3, Day4, Day5, Day6, Day7);

    CountData = [];
    exactdaycount = [];
    var exactresult;
    exactresultcount = [];
    var count;
    var countdetails = [0, 0, 0, 0, 0, 0, 0];
    var modeldata = JSON.stringify(model);
    $.ajax({
        type: "POST",
        crossDomain: true,
        url: '../Account/GetBarChartDetails',
        cache: false,
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        data: modeldata,

        complete: function () {
            // $.unblockUI();
        },
        success: function (response) {
            if (response != "") {

                for (var i = 0; i < response.length; i++) {
                    var paymentdate = response[i].WeekDate;
                    var paymentexatdate = paymentdate.substring(0, 10);
                    count = parseInt(response[i].Count);
                    var date = new Date(paymentdate);
                    var days = ['Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday'];
                    var weekday = days[date.getDay()];
                    var exactdate = paymentdate.replace("2015-09-0", "");
                    var exactday = weekday.substring(0, 3)
                    exactdaycount.push(exactday);
                    exactresult = exactday + count;
                    exactresultcount.push(exactresult);
                    CountData.push(count);

                }
                if (response[0] != undefined) {
                    //day1
                    if (exactdaycount[0] == "Mon") {
                        var indexday1 = Originaldata.indexOf("Mon");
                        if (indexday1 !== -1) {
                            countdetails[indexday1] = CountData[0];

                        }
                    }
                    else if (exactdaycount[0] == "Tue") {
                        var indexday2 = Originaldata.indexOf("Tue");
                        if (indexday2 !== -1) {
                            countdetails[indexday2] = CountData[0];

                        }
                    }
                    else if (exactdaycount[0] == "Wed") {
                        var indexday3 = Originaldata.indexOf("Wed");
                        if (indexday3 !== -1) {
                            countdetails[indexday3] = CountData[0];

                        }
                    }
                    else if (exactdaycount[0] == "Thu") {
                        var indexday4 = Originaldata.indexOf("Thu");
                        if (indexday4 !== -1) {
                            countdetails[indexday4] = CountData[0];

                        }
                    }
                    else if (exactdaycount[0] == "Fri") {
                        var indexday5 = Originaldata.indexOf("Fri");
                        if (indexday5 !== -1) {
                            countdetails[indexday5] = CountData[0];

                        }
                    }
                    else if (exactdaycount[0] == "Sat") {
                        var indexday6 = Originaldata.indexOf("Sat");
                        if (indexday6 !== -1) {
                            countdetails[indexday6] = CountData[0];

                        }
                    }
                    else if (exactdaycount[0] == "Sun") {
                        var indexday7 = Originaldata.indexOf("Sun");
                        if (indexday7 !== -1) {
                            countdetails[indexday7] = CountData[0];

                        }
                    }
                }
                if (response[1] != undefined) {
                    //day2
                    if (exactdaycount[1] == "Mon") {
                        var indexday1 = Originaldata.indexOf("Mon");
                        if (indexday1 !== -1) {
                            countdetails[indexday1] = CountData[1];


                        }
                    }
                    else if (exactdaycount[1] == "Tue") {
                        var indexday2 = Originaldata.indexOf("Tue");
                        if (indexday2 !== -1) {
                            countdetails[indexday2] = CountData[1];


                        }
                    }
                    else if (exactdaycount[1] == "Wed") {
                        var indexday3 = Originaldata.indexOf("Wed");
                        if (indexday3 !== -1) {
                            countdetails[indexday3] = CountData[1];

                        }
                    }
                    else if (exactdaycount[1] == "Thu") {
                        var indexday4 = Originaldata.indexOf("Thu");
                        if (indexday4 !== -1) {
                            countdetails[indexday4] = CountData[1];
                        }
                    }
                    else if (exactdaycount[1] == "Fri") {
                        var indexday5 = Originaldata.indexOf("Fri");
                        if (indexday5 !== -1) {
                            countdetails[indexday5] = CountData[1];
                        }
                    }
                    else if (exactdaycount[1] == "Sat") {
                        var indexday6 = Originaldata.indexOf("Sat");
                        if (indexday6 !== -1) {
                            countdetails[indexday6] = CountData[1];
                        }
                    }
                    else if (exactdaycount[1] == "Sun") {
                        var indexday7 = Originaldata.indexOf("Sun");
                        if (indexday7 !== -1) {
                            countdetails[indexday7] = CountData[1];

                        }
                    }
                }
                if (response[2] != undefined) {
                    //day3

                    if (exactdaycount[2] == "Mon") {
                        var indexday1 = Originaldata.indexOf("Mon");
                        if (indexday1 !== -1) {
                            countdetails[indexday1] = CountData[2];
                        }

                    }

                    else if (exactdaycount[2] == "Tue") {
                        var indexday2 = Originaldata.indexOf("Tue");
                        if (indexday2 !== -1) {
                            countdetails[indexday2] = CountData[2];
                        }
                    }
                    else if (exactdaycount[2] == "Wed") {
                        var indexday3 = Originaldata.indexOf("Wed");
                        if (indexday3 !== -1) {
                            countdetails[indexday3] = CountData[2];

                        }
                    }
                    else if (exactdaycount[2] == "Thu") {
                        var indexday4 = Originaldata.indexOf("Thu");
                        if (indexday4 !== -1) {
                            countdetails[indexday4] = CountData[2];

                        }
                    }
                    else if (exactdaycount[2] == "Fri") {
                        var indexday5 = Originaldata.indexOf("Fri");
                        if (indexday5 !== -1) {
                            countdetails[indexday5] = CountData[2];

                        }
                    }
                    else if (exactdaycount[2] == "Sat") {
                        var indexday6 = Originaldata.indexOf("Sat");
                        if (indexday6 !== -1) {
                            countdetails[indexday6] = CountData[2];

                        }
                    }
                    else if (exactdaycount[2] == "Sun") {
                        var indexday7 = Originaldata.indexOf("Sun");
                        if (indexday7 !== -1) {
                            countdetails[indexday7] = CountData[2];
                        }
                    }
                }
                //day4
                if (response[3] != undefined) {
                    if (exactdaycount[3] == "Mon") {
                        var indexday1 = Originaldata.indexOf("Mon");
                        if (indexday1 !== -1) {
                            countdetails[indexday1] = CountData[3];
                        }

                    }
                    else if (exactdaycount[3] == "Tue") {
                        var indexday2 = Originaldata.indexOf("Tue");
                        if (indexday2 !== -1) {
                            countdetails[indexday2] = CountData[3];
                        }
                    }
                    else if (exactdaycount[3] == "Wed") {
                        var indexday3 = Originaldata.indexOf("Wed");
                        if (indexday3 !== -1) {
                            countdetails[indexday3] = CountData[3];

                        }
                    }
                    else if (exactdaycount[3] == "Thu") {
                        var indexday4 = Originaldata.indexOf("Thu");
                        if (indexday4 !== -1) {
                            countdetails[indexday4] = CountData[3];

                        }
                    }
                    else if (exactdaycount[3] == "Fri") {
                        var indexday5 = Originaldata.indexOf("Fri");
                        if (indexday5 !== -1) {
                            countdetails[indexday5] = CountData[3];

                        }
                    }
                    else if (exactdaycount[3] == "Sat") {
                        var indexday6 = Originaldata.indexOf("Sat");
                        if (indexday6 !== -1) {
                            countdetails[indexday6] = CountData[3];

                        }
                    }
                    else if (exactdaycount[3] == "Sun") {
                        var indexday7 = Originaldata.indexOf("Sun");
                        if (indexday7 !== -1) {
                            countdetails[indexday7] = CountData[3];
                        }
                    }
                }
                if (response[4] != undefined) {
                    //day5
                    if (exactdaycount[4] == "Mon") {
                        var indexday1 = Originaldata.indexOf("Mon");
                        if (indexday1 !== -1) {
                            countdetails[indexday1] = CountData[4];
                        }

                    }

                    else if (exactdaycount[4] == "Tue") {
                        var indexday2 = Originaldata.indexOf("Tue");
                        if (indexday2 !== -1) {
                            countdetails[indexday2] = CountData[4];
                        }
                    }
                    else if (exactdaycount[4] == "Wed") {
                        var indexday3 = Originaldata.indexOf("Wed");
                        if (indexday3 !== -1) {
                            countdetails[indexday3] = CountData[4];

                        }
                    }
                    else if (exactdaycount[4] == "Thu") {
                        var indexday4 = Originaldata.indexOf("Thu");
                        if (indexday4 !== -1) {
                            countdetails[indexday4] = CountData[4];

                        }
                    }
                    else if (exactdaycount[4] == "Fri") {
                        var indexday5 = Originaldata.indexOf("Fri");
                        if (indexday5 !== -1) {
                            countdetails[indexday5] = CountData[4];

                        }
                    }
                    else if (exactdaycount[4] == "Sat") {
                        var indexday6 = Originaldata.indexOf("Sat");
                        if (indexday6 !== -1) {
                            countdetails[indexday6] = CountData[4];

                        }
                    }
                    else if (exactdaycount[4] == "Sun") {
                        var indexday7 = Originaldata.indexOf("Sun");
                        if (indexday7 !== -1) {
                            countdetails[indexday7] = CountData[4];
                        }
                    }
                }
                //day6
                if (response[5] != undefined) {
                    if (exactdaycount[5] == "Mon") {
                        var indexday1 = Originaldata.indexOf("Mon");

                        if (indexday1 !== -1) {
                            countdetails[indexday1] = CountData[5];
                        }

                    }

                    else if (exactdaycount[5] == "Tue") {
                        var indexday2 = Originaldata.indexOf("Tue");
                        if (indexday2 !== -1) {
                            countdetails[indexday2] = CountData[5];
                        }
                    }
                    else if (exactdaycount[5] == "Wed") {
                        var indexday3 = Originaldata.indexOf("Wed");
                        if (indexday3 !== -1) {
                            countdetails[indexday3] = CountData[5];

                        }
                    }
                    else if (exactdaycount[5] == "Thu") {
                        var indexday4 = Originaldata.indexOf("Thu");
                        if (indexday4 !== -1) {
                            countdetails[indexday4] = CountData[5];

                        }
                    }
                    else if (exactdaycount[5] == "Fri") {
                        var indexday5 = Originaldata.indexOf("Fri");
                        if (indexday5 !== -1) {
                            countdetails[indexday5] = CountData[5];

                        }
                    }
                    else if (exactdaycount[5] == "Sat") {
                        var indexday6 = Originaldata.indexOf("Sat");
                        if (indexday6 !== -1) {
                            countdetails[indexday6] = CountData[5];

                        }
                    }
                    else if (exactdaycount[5] == "Sun") {
                        var indexday7 = Originaldata.indexOf("Sun");
                        if (indexday7 !== -1) {
                            countdetails[indexday7] = CountData[5];
                        }
                    }

                }
                if (response[6] != undefined) {
                    //day7
                    if (exactdaycount[6] == "Mon") {
                        var indexday1 = Originaldata.indexOf("Mon");
                        if (indexday1 !== -1) {
                            countdetails[indexday1] = CountData[6];
                        }

                    }
                    else if (exactdaycount[6] == "Tue") {
                        var indexday2 = Originaldata.indexOf("Tue");
                        if (indexday2 !== -1) {
                            countdetails[indexday2] = CountData[6];
                        }
                    }
                    else if (exactdaycount[6] == "Wed") {
                        var indexday3 = Originaldata.indexOf("Wed");
                        if (indexday3 !== -1) {
                            countdetails[indexday3] = CountData[6];

                        }
                    }
                    else if (exactdaycount[6] == "Thu") {
                        var indexday4 = Originaldata.indexOf("Thu");
                        if (indexday4 !== -1) {
                            countdetails[indexday4] = CountData[6];

                        }
                    }
                    else if (exactdaycount[6] == "Fri") {
                        var indexday5 = Originaldata.indexOf("Fri");
                        if (indexday5 !== -1) {
                            countdetails[indexday5] = CountData[6];

                        }
                    }
                    else if (exactdaycount[6] == "Sat") {
                        var indexday6 = Originaldata.indexOf("Sat");
                        if (indexday6 !== -1) {
                            countdetails[indexday6] = CountData[6];

                        }
                    }
                    else if (exactdaycount[6] == "Sun") {
                        var indexday7 = Originaldata.indexOf("Sun");
                        if (indexday7 !== -1) {
                            countdetails[indexday7] = CountData[6];
                        }
                    }
                }
                if (countdetails[0] == 0) {
                    countdetails[0] = null;
                }
                if (countdetails[1] == 0) {
                    countdetails[1] = null;
                }
                if (countdetails[2] == 0) {
                    countdetails[2] = null;
                }
                if (countdetails[3] == 0) {
                    countdetails[3] = null;
                }
                if (countdetails[4] == 0) {
                    countdetails[4] = null;
                }
                if (countdetails[5] == 0) {
                    countdetails[5] = null;
                }
                if (countdetails[6] == 0) {
                    countdetails[6] = null;
                }
                $('#chart_nodata').hide();
                var options = {
                    title: {
                        //text: "Column Chart using jQuery Plugin"
                    },
                    animationEnabled: true,

                    data: [
                    {
                        indexLabelFontColor: "grey",
                        indexLabelFontWeight: "bolder",
                        indexLabelPlacement: "inside",
                        indexLabelFontSize: 25,
                        indexLabelFontFamily: "Lucida Console",
                        indexLabel: "{y}",
                        type: "column", //change it to line, area, bar, pie, etc
                        dataPoints: [

                    { x: 10, y: countdetails[0], label: PushingData[0] },
                    { x: 20, y: countdetails[1], label: PushingData[1] },
                    { x: 30, y: countdetails[2], label: PushingData[2] },
                    { x: 40, y: countdetails[3], label: PushingData[3] },
                    { x: 50, y: countdetails[4], label: PushingData[4] },
                    { x: 60, y: countdetails[5], label: PushingData[5] },
                    { x: 70, y: countdetails[6], label: PushingData[6] },

                        ]
                    }
                    ]
                };

                $("#chartContainerday").CanvasJSChart(options);
            }
            else {
                $('#chart_nodata').show();
                var options = {
                    title: {
                        //text: "Column Chart using jQuery Plugin"
                    },
                    animationEnabled: true,

                    data: [
                    {
                        indexLabelFontColor: "grey",
                        indexLabelFontWeight: "bolder",
                        indexLabelPlacement: "inside",
                        indexLabelFontSize: 25,
                        indexLabelFontFamily: "Lucida Console",
                        indexLabel: "{y}",
                        type: "column", //change it to line, area, bar, pie, etc
                        dataPoints: [

                     { x: 10, y: null, label: PushingData[0] },
                    { x: 20, y: null, label: PushingData[1] },
                    { x: 30, y: null, label: PushingData[2] },
                    { x: 40, y: null, label: PushingData[3] },
                    { x: 50, y: null, label: PushingData[4] },
                    { x: 60, y: null, label: PushingData[5] },
                    { x: 70, y: null, label: PushingData[6] },

                        ]
                    }
                    ]
                };
                $("#chartContainerday").CanvasJSChart(options);
            }



        },

        error:function (xhr, status, error){
            ShowAjaxRequestError(xhr, status, error);
        }
    });
}
var date=null;
function GetFourWeeksDataBackward(datecount) {
    $('#chartContainerweek').show();
    var today = new Date();
    var W1 = "Week4";
    var W2 = "Week3";
    var W3 = "Week2";
    var W4 = "Week1";
    if (parseInt(fourweekcount) == 0) {
        $("#btnlastFourWeeksRight").show();
    }
    else {
        $("#btnlastFourWeeksRight").show();
    }
    fourweekcount = parseInt(fourweekcount) + 1;
    var nextday = (new Date(today.setDate(today.getDate() - (parseInt(fourweekcount) * 28))));
    var first = today.getDate();

    var actualdate = nextday.toISOString().substring(0, 10);
    var model = {
        WeekDate: actualdate,
    };
    var modeldata = JSON.stringify(model);
    $.ajax({
        type: "POST",
        crossDomain: true,
        url: '../Account/GetBarChartDetailsforfourWeeks',
        cache: false,
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        data: modeldata,
        complete: function () {

        },
        success: function (response) {
            var myArray;
            var countdetails = [0, 0, 0, 0];

            if (response != "" &&(response[0].Week1!=null || response[0].Week2!=null || response[0].Week3!=null || response[0].Week4!=null)) {
                for (var i = 0; i < response.length; i++) {
                    var Week1 = response[i].Week1;
                    var Week2 = response[i].Week2;
                    var Week3 = response[i].Week3;
                    var Week4 = response[i].Week4;

                }
               
                    if (Week1 != "" && Week1 != null) {
                        countdetails[0] = parseInt(Week1);

                    } else { countdetails[0] = null; }
                    if (Week2 != "" && Week2 != null) {
                        countdetails[1] = parseInt(Week2);

                    } else { countdetails[1] = null; }
                    if (Week3 != "" && Week3 != null) {
                        countdetails[2] = parseInt(Week3);
                    } else { countdetails[2] = null; }
                    if (Week4 != "" && Week4 != null) {
                        countdetails[3] = parseInt(Week4);
                    } else { countdetails[3] = null; }
                $('#chart_nodata').hide();
                var options = {
                    title: {
                        //text: "Column Chart using jQuery Plugin"
                    },
                    animationEnabled: true,

                    data: [
                    {
                        indexLabelFontColor: "grey",
                        indexLabelFontWeight: "bolder",
                        indexLabelPlacement: "inside",
                        indexLabelFontSize: 25,
                        indexLabelFontFamily: "Lucida Console",
                        indexLabel: "{y}",
                        type: "column", //change it to line, area, bar, pie, etc
                        dataPoints: [

                    { x: 1, y: countdetails[0], label: W1 },
                    { x: 2, y: countdetails[1], label: W2 },
                    { x: 3, y: countdetails[2], label: W3 },
                    { x: 4, y: countdetails[3], label: W4 },

                        ]
                    }
                    ]
                };

                $("#chartContainerweek").CanvasJSChart(options);
            }
            else {
                $('#chart_nodata').show();
                var options = {
                    title: {
                        //text: "Column Chart using jQuery Plugin"
                    },
                    animationEnabled: true,

                    data: [
                    {
                        indexLabelFontColor: "grey",
                        indexLabelFontWeight: "bolder",
                        indexLabelPlacement: "inside",
                        indexLabelFontSize: 25,
                        indexLabelFontFamily: "Lucida Console",
                        indexLabel: "{y}",
                        type: "column", //change it to line, area, bar, pie, etc
                        dataPoints: [

                    { x: 1, y: null, label: 'Week1' },
                    { x: 2, y: null, label: 'Week2' },
                    { x: 3, y: null, label: 'Week3' },
                    { x: 4, y: null, label: 'Week4' },

                        ]
                    }
                    ]
                };

                $("#chartContainerweek").CanvasJSChart(options);
            }

        },

        error: function (xhr, status, error) {
            ShowAjaxRequestError(xhr, status, error);
        }
    });

}

function GetFourWeeksDataForward(datecount) {
    $('#chartContainerweek').show();
    var today = new Date();
    var next = fourweekcount -1;
    var first = today.getDate();
    fourweekcount = next;
    if (fourweekcount == 0) {
        $("#btnlastFourWeeksRight").show();
     
    }
    else {
        $("#btnlastFourWeeksRight").show();
    }

    var W1 = "Week4";
    var W2 = "Week3";
    var W3 = "Week2";
    var W4 = "Week1";
    if (next == 0 || parseInt(next) < 0) {
        var nextday = (new Date(today.setDate(today.getDate())));
        fourweekcount = 0;
    }  
    else {
        var nextday = (new Date(today.setDate(today.getDate() - (parseInt(next)*28))));
        
    }
    var actualday = (new Date(nextday.setDate(first)));
    var actualdate = nextday.toISOString().substring(0, 10);
    var model = {
        WeekDate: actualdate,
    };
    var modeldata = JSON.stringify(model);
    $.ajax({
        type: "POST",
        crossDomain: true,
        url: '../Account/GetBarChartDetailsforfourWeeks',
        cache: false,
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        data: modeldata,
        complete: function () {

        },
        success: function (response) {
            var myArray;
            var countdetails = [0, 0, 0, 0];

            if (response != "" && (response[0].Week1!=null || response[0].Week2!=null || response[0].Week3!=null || response[0].Week4!=null)) {

                for (var i = 0; i < response.length; i++) {
                    var Week1 = response[i].Week1;
                    var Week2 = response[i].Week2;
                    var Week3 = response[i].Week3;
                    var Week4 = response[i].Week4;

                }
               
                    if (Week1 != "" && Week1 != null) {
                        countdetails[0] = parseInt(Week1);

                    } else { countdetails[0] = null; }
                    if (Week2 != "" && Week2 != null) {
                        countdetails[1] = parseInt(Week2);

                    } else { countdetails[1] = null; }
                    if (Week3 != "" && Week3 != null) {
                        countdetails[2] = parseInt(Week3);
                    } else { countdetails[2] = null; }
                    if (Week4 != "" && Week4 != null) {
                        countdetails[3] = parseInt(Week4);
                    } else { countdetails[3] = null; }
               
                $('#chart_nodata').hide();
                var options = {
                    title: {
                        //text: "Column Chart using jQuery Plugin"
                    },
                    animationEnabled: true,

                    data: [
                    {
                        indexLabelFontColor: "grey",
                        indexLabelFontWeight: "bolder",
                        indexLabelPlacement: "inside",
                        indexLabelFontSize: 25,
                        indexLabelFontFamily: "Lucida Console",
                        indexLabel: "{y}",
                        type: "column", //change it to line, area, bar, pie, etc
                        dataPoints: [

                    { x: 1, y: countdetails[0], label: 'Week1' },
                    { x: 2, y: countdetails[1], label: 'Week2' },
                    { x: 3, y: countdetails[2], label: 'Week3' },
                    { x: 4, y: countdetails[3], label: 'Week4' },

                        ]
                    }
                    ]
                };

                $("#chartContainerweek").CanvasJSChart(options);
            }
            else {
                $('#chart_nodata').show();
                var options = {
                    title: {
                        //text: "Column Chart using jQuery Plugin"
                    },
                    animationEnabled: true,

                    data: [
                    {
                        indexLabelFontColor: "grey",
                        indexLabelFontWeight: "bolder",
                        indexLabelPlacement: "inside",
                        indexLabelFontSize: 25,
                        indexLabelFontFamily: "Lucida Console",
                        indexLabel: "{y}",
                        type: "column", //change it to line, area, bar, pie, etc
                        dataPoints: [

                    { x: 1, y: null, label: 'Week1' },
                    { x: 2, y: null, label: 'Week2' },
                    { x: 3, y: null, label: 'Week3' },
                    { x: 4, y: null, label: 'Week4' },

                        ]
                    }
                    ]
                };

                $("#chartContainerweek").CanvasJSChart(options);
            }

        },
        error: function (xhr, status, error) {
            ShowAjaxRequestError(xhr, status, error);
        }
    });

}
$(document).on("click", "#btnlastSevenDays", function () {
    $("#btnlastfourweeks").removeClass("purple").addClass("blue");
    $(this).removeClass("blue").addClass("purple");
    $("#chartContainerweek").empty();
    $('#btnlastFourWeeksLeft').attr('id', 'btnlastSevenDaysLeft');
    $('#btnlastFourWeeksRight').attr('id', 'btnlastSevenDaysRight');
    sevendatescount[0] = 0;
    GetSevenDaysDataBackWard(sevendatescount);
});

$(document).on("click", "#btnlastfourweeks", function () {
    $("#btnlastSevenDays").removeClass("purple").addClass("blue");
    $(this).removeClass("blue").addClass("purple");
    $("#chartContainerday").empty();
    $('#btnlastSevenDaysLeft').attr('id', 'btnlastFourWeeksLeft');
    $('#btnlastSevenDaysRight').attr('id', 'btnlastFourWeeksRight');
    fourweekcount = -1;
    GetFourWeeksDataBackward(fourweekcount);
});

$("document").ready(function () {
    var agentsname = $(".usr-name").text();
    var agentsvalue = agentsname.split('@')[0].replace(/[^a-zA-Z 0-9]+/g, '');
    var actualvalue = "''" + agentsvalue + "'" + 's' + "''";
    $('#agentsname').html(actualvalue);
    $('#chart_slide_left').attr('id', 'btnlastSevenDaysLeft');
    $('#chart_slide_right').attr('id', 'btnlastSevenDaysRight');
    GetSevenDaysDataBackWard(sevendatescount);
    AddTodaysAction('Get', "", "DASHBOARD");

});



//document.getElementById('add_button').addEventListener('click', addbutt);
function addbutt() {
    $('.to_do_list_add').css('display', 'block');
}
//To save Action TodoList//
$(document).on("click", "#add_task_button", function () {
    var check = document.getElementById("Task_checkbox");
    if (check.checked && ($('#Task_timer').val() == null || $('#Task_timer').val() == '')) {
        fnOpenAlert("Please select the date");
        return false;
    }
    inserttodaysaction()
    $("#Task_checkbox").attr('checked', false);
    $("#DivTask_timer").hide();
    $('#Task_timer').val('');
    
});

$(document).on("click", ".btnComplete", function () {

    var checkcount = $('input[type="checkbox"]:checked').length;
    if (checkcount == 0) {
        $('.alert-wrapper').show();
        $('.alert-box h4').text('Please check one action to complete');
        return;
    }

    var selectedID = [];
    $(':checkbox[name="checkMr[]"]:checked').each(function () {
        selectedID.push(this.id);
    });

    var TaskPrimaryKeyid = selectedID;
    var reminder = 0;

    var model = {
        TaskPrimaryKeyid: TaskPrimaryKeyid,
        type: '',
        reminder_status: 'complete'
       
    };
    var modeldata = JSON.stringify(model);
    AddTodaysAction('POST', modeldata, 'DASHBOARD');
   
});
function inserttodaysaction() {
    var tasktext = $("#add_task").val();
    if (tasktext == "") {
        $('.alert-wrapper').show();
        $('.alert-box h4').text("Please enter today's action data");
        return;
    }
    else {
        if ($("#Task_checkbox").prop('checked')) {
            var reminderDate = $("#Task_timer").val();
            var reminder = 1;
            
        } else
        {
            var reminder = 0;
            var date = new Date();
            reminderDate: date.getDate();
        };
        var model = {
            TaskText: tasktext,
            type: 'Insert',
            reminder_status: 'new',
            reminder: reminder,
            reminderdate: reminderDate

        };
       
        var modeldata = JSON.stringify(model);
        AddTodaysAction('POST',modeldata, 'DASHBOARD');
    }
}


$(document).on("click", "#btnlastSevenDaysLeft", function () {
    GetSevenDaysDataBackWard(sevendatescount);

});

$(document).on("click", "#btnlastSevenDaysRight", function () {
    GetSevenDaysDataForward(sevendatescount);

});


$(document).on("click", "#btnlastFourWeeksLeft", function () {
    
    GetFourWeeksDataBackward(fourweekcount);

});

$(document).on("click", "#btnlastFourWeeksRight", function () {
    GetFourWeeksDataForward(fourweekcount);

});