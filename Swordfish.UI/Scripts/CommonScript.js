﻿///////////////////////////////////////////////////////////////////////////////////////
//Common functions/functionality across SF applicaiton
////////////////////////////////////////////////////////////////////////////////////////
var data;
var myvar = setInterval(mytimer, 10000);
$(document).ready(function () {
    $("#scrollTask_timer,#Task_timer").keypress(function (e) {
        return false;
    });
});
////////////////////////////////////////////////////////////////////////////////////////
//
////////////////////////////////////////////////////////////////////////////////////////
function SfConfirmationBox(message, OkDelegate, CancelDelegate) {
    $("#dialog-confirm").html(message);
    // Define the Dialog and its properties.		
    $("#dialog-confirm").dialog({
        resizable: false,
        modal: true,
        title: "",
        width: 400,
        buttons: [
            {
                text: "Ok",
                "class": 'btn btn-default grnbtn',
                click: function () {
                    //	OkFunction();		
                    $(this).dialog('close');
                    if (OkDelegate != null) {
                        // function we want to run		
                        // Since its name is being dynamically generated, always ensure your function actually exists		
                        if (typeof (window[OkDelegate]) === "function") {
                            window[OkDelegate]();
                        }
                    }
                    //callback(true, OkFunction);		
                }
            },
            {
                text: "Cancel",
                "class": 'btn btn-default vltbtn',
                click: function () {
                    $(this).dialog('close');
                    if (CancelDelegate != null) {
                        if (typeof (window[CancelDelegate]) === "function") {
                            window[CancelDelegate]();
                        }
                    }

                }
            }
        ]
    });
}
////////////////////////////////////////////////////////////////////////////////////////
//
////////////////////////////////////////////////////////////////////////////////////////
function Confirmation() {
    $.post('../Booking/CheckSession', null, function (data) {
        var ChkSession = data.success;
        if (ChkSession == false) {
            fnOpenAlert("Your session is expired !! \nPlease Re-login again.");
            return false;
        }
        else if (ChkSession != true) {
            fnOpenAlert(ChkSession);
            return false;
        }
    });

    var title = $("#ddlGender option:selected").val();
    var firstName = $("#txtFirstName").val();
    var lastName = $("#txtLastName").val();
    var email = $("#txtEmail").val();
    var mobile = $("#txtMobile").val();
    var telephone = $("#txtTelephone").val();
    var postcode = $("#txtPostCode").val();
    var add1 = $("#txtAddress1").val();
    var add2 = $("#txtAddress2").val();
    var add3 = $("#txtAddress3").val();
    var dob = $("#txtDateOfBirth").val();
    var passportno = $("#txtPassportNo").val();
    var cardType = $("#ddlCardType option:selected").val();
    var nameOnCard = $("#txtNameOnCard").val();
    var cardNumber = $("#txtCardNumber").val();
    var cardExpiryMonth = $("#txtCardExpiryMonth").val();
    var cardExpiryYear = $("#txtCardExpiryYear").val();
    var cvv = $("#txtCVV").val();
    var chkTerms = false;
    $("#chkIsAcceptedTerms").val();
    var LeadPassenger = [];
    LeadPassenger.push({
        Title: title,
        FirstName: firstName,
        MiddleName: '',
        LastName: lastName,
        IsLeadPassenger: 'Y',
        DateOfBirth: dob,
        Nationality: '',
        Gender: '',
        Address1: add1,
        Address2: add2,
        Address3: add3,
        Mobile: mobile,
        Telephone: telephone,
        PostCode: postcode,
        Email: email,
        PassportNumber: passportno,
        Type: 'A'
    });
    var LeadPassengerinfo = JSON.stringify(LeadPassenger);
    var coPassengerinfo = JSON.stringify(coPassengers);
    var clientID = $("#hdn_clientID").val();
    var QID = $("#hdnQuoteReference").val();
    var AgentID = $("#displayname").val();
    if (clientID == "") {
        fnOpenAlert("Your clientid does not exist !! \nPlease Refresh your page & try again.");
        return false;
    }
    var dataObject = { Quoteid: QID, Agent_id: AgentID, Client_id: clientID, LeadPassInfo: LeadPassengerinfo, CoPassInfo: coPassengerinfo }
    $.ajax({
        type: 'POST',
        url: '../Booking/SendtoCustomer',
        contentType: 'application/json; charset=utf-8',
        data: JSON.stringify(dataObject),
        success: function (data) {
            if (data.MailStatus != undefined && data.MailStatus != null && data.MailStatus != '') {
                if (data.MailStatus) {
                    isMailSent = data.MailStatus;
                }
                else {
                    fnOpenAlert("There is problem in sending email to customer.  Please try again.");
                }
            }

            return false;
        },
        error: function (xhr, status, error) {
            ShowAjaxRequestError(xhr, status, error);
        }
    });
}
////////////////////////////////////////////////////////////////////////////////////////
//
////////////////////////////////////////////////////////////////////////////////////////
function passengerConfirm() {
    var chat = $.connection.chatHub;
    var url = $("#hdnUrlRequest").val();
    // Get the user name and store		
    $('#displayname').val('customer');
    // Declare a function server can invoke it		
    chat.client.sendMessage = function (name, message) {
        var encodedName = $('<div />').text(name).html();
        var encodedMsg = $('<div />').text(message).html();
        $('#messages').append('<li>' + encodedName + ':&nbsp;&nbsp;' + encodedMsg + '</li>');
    };
    var CoBrowsid = $("#hdn_browsID").val();
    //Call the method on the server		
    chat.server.sendToSpec($('#displayname').val(), 'Y', $("#hdn_clientID").val());
    var dataObject = { custConf: 'Y', CoBrowsID: CoBrowsid }
    $.ajax({
        type: 'POST',
        url: url,
        data: JSON.stringify(dataObject),
        contentType: 'application/json; charset=utf-8',
        success: function (data) {
            window.close();
        },
        error: function (xhr, status, error) {
            ShowAjaxRequestError(xhr, status, error);
            window.close();
        }
    });
}
///////////////////////////////////////////////////////////////////////////////////////
//
////////////////////////////////////////////////////////////////////////////////////////
function passengerReject() {
    var chat = $.connection.chatHub;
    var url = $("#hdnUrlRequest").val();
    // Get the user name and store		
    $('#displayname').val('customer');
    // Declare a function server can invoke it		
    chat.client.sendMessage = function (name, message) {
        var encodedName = $('<div />').text(name).html();
        var encodedMsg = $('<div />').text(message).html();
        $('#messages').append('<li>' + encodedName + ':&nbsp;&nbsp;' + encodedMsg + '</li>');
    };
    $("#hdn_ConfimID").val($("#hdn_browsID").val());
    //Call the method on the server		
    var CoBrowsid = $("#hdn_browsID").val();
    chat.server.sendToSpec($('#displayname').val(), 'R', $("#hdn_clientID").val());
    var dataObject = { custConf: 'R', CoBrowsID: CoBrowsid }
    $.ajax({
        url: url,
        type: 'POST',
        data: JSON.stringify(dataObject),
        contentType: 'application/json; charset=utf-8',
        success: function (data) {
            window.close();
        },
        error: function (xhr, status, error) {
            ShowAjaxRequestError(xhr, status, error);
            window.close();
        }
    });
}
///////////////////////////////////////////////////////////////////////////////////////
//
////////////////////////////////////////////////////////////////////////////////////////
function ShowAjaxRequestError(jqXHR, status, error) {
    var msg = '';
    if (jqXHR.status == 0) {
        msg = 'Not connect.\n Verify Network.';
    } else if (jqXHR.status == 404) {
        msg = 'Requested page not found. [404]';
    } else if (jqXHR.status == 500) {
        msg = 'Internal Server Error [500].';
    } else if (error == 'parsererror') {
        msg = 'Requested JSON parse failed.';
    } else if (error == 'timeout') {
        msg = 'Time out error.';
    } else if (error == 'abort') {
        msg = 'Ajax request aborted.';
    } else {
        msg = 'Error Occurred\n xhr=' + jqXHR.responseText +'\n status='+status + '\n error=' +error ;
    }

    fnOpenAlert(msg);
}
///////////////////////////////////////////////////////////////////////////////////////
//
////////////////////////////////////////////////////////////////////////////////////////
function fnOpenAlert(message) {
    $("#dialog-confirm").html(message);
    // Define the Dialog and its properties.		
    $("#dialog-confirm").dialog({
        resizable: false,
        modal: true,
        title: "",
        width: 400,
        buttons: [
            {
                text: "Ok",
                "class": 'btn btn-default grnbtn',
                click: function () {
                    $(this).dialog('close');
                }
            }
        ]
    });
}
///////////////////////////////////////////////////////////////////////////////////////
//
////////////////////////////////////////////////////////////////////////////////////////
function AddTodaysAction(Type, Message, Success) {
    var urls = '../Account/InsertUpdateTodaysAction';
    if (Type == 'Get') {
        urls = '../Account/GetTodaysActionData';
    }
    $.ajax({
        url: urls,
        data: Message,
        type: Type,
        dataType: "json",
        contentType: "application/json; charset=utf-8",
        cache: false,
        complete: function () {
        },
        success: function (response) {
            data = null;
            data = response;
            localStorage.clear();
            localStorage["data"] = JSON.stringify(response);
            if (Success == 'DASHBOARD') {
                $('#to_do_list').empty();

                // Testing pull request
                if (response != "") {
                    for (var i = 0; i < response.length; i++) {
                        var len = response[i].TaskText.length;
                        var Tasktext = response[i].TaskText;
                        if (response[i].remindersdate != null) {
                            if (len > 23) {
                                if (response[i].reminder != 0) {
                                    Tasktext = response[i].TaskText.substr(0, 23) + "..." + response[i].remindersdate;

                                } else { Tasktext = response[i].TaskText.substr(0, 23) + "..."; }

                            } else {
                                var space = "&nbsp";
                                while (len <= 30) {
                                    space = space + "&nbsp";
                                    len++;
                                }
                                if (response[i].reminder != 0) {
                                    Tasktext = response[i].TaskText + space + response[i].remindersdate;

                                } else { Tasktext = response[i].TaskText + space; }
                            }
                        }


                        $('.to_do_list_add').css('display', 'none');
                        var mainAdd = document.createElement('div');
                        mainAdd.className = 'checkbox';
                        mainAdd.setAttribute("data-content", response[i].TaskText);
                        mainAdd.setAttribute("data-toggle", "popover");
                        mainAdd.setAttribute("data-placement", "left");
                        mainAdd.onmouseover = MouseOver;
                        mainAdd.onmouseleave = MouseLeave;
                        var Addwrap = document.createElement('div');
                        mainAdd.appendChild(Addwrap);
                        var para = document.createElement('p');
                        para.className = 'add_task_text';
                        //para.innerHTML = document.getElementById('add_task').value;
                        para.innerHTML = Tasktext;
                        //para.title = TaskText;


                        $("#" + response[i].TaskPrimaryKeyid + "span").appendTo(para);
                        Addwrap.appendChild(para);
                        var input = document.createElement('input');
                        input.type = 'checkbox';
                        input.name = 'checkMr[]'
                        //input.value = "option1";
                        //input.value = document.getElementById('add_task').value;
                        input.value = Tasktext;
                        input.id = response[i].TaskPrimaryKeyid;
                        //input.setAttribute(response[i].TaskText, check);
                        Addwrap.appendChild(input);
                        var check = document.createElement('label');
                        Addwrap.appendChild(check);
                        check.setAttribute("for", response[i].TaskPrimaryKeyid);
                        para.innerHTML = input.value;
                        document.getElementById('to_do_list').appendChild(mainAdd);
                    }
                }
                $("#add_task").val("");
                sessionStorage.setItem('data', response);
            } else {
                updatescrollbar(data, 0);
                $("#addTodaytask").val("");
            }
        }, complete: function () {
            if (Type == 'POST')
                AddTodaysAction('Get', "", "OTHER");
        },
        error: function (xhr, status, error) {
            ShowAjaxRequestError(xhr, status, error);
        }
    });
}
///////////////////////////////////////////////////////////////////////////////////////
//
////////////////////////////////////////////////////////////////////////////////////////
function mytimer() {
    updatescrollbar(data, 0);
}
///////////////////////////////////////////////////////////////////////////////////////
//
////////////////////////////////////////////////////////////////////////////////////////
function updatescrollbar(data1, rownumber) {
    var today = new Date();
    var date = today.getDate();
    var d1 = new Date(),
        d2 = new Date(d1);
    d2.setMinutes(d1.getMinutes() + 15);
    var ele = null;
    var sub = null;
    var sub1 = null;
    var sub2 = null;

    if (data == null) {
        if (localStorage["data"] != null) { var data = JSON.parse(localStorage["data"]); } else { var data = AddTodaysAction('Get', "", "OTHER"); }
    }

    if (data != null && data != undefined && data != '') {
        if (data.length > 0) {
            for (var i = rownumber; i < data.length; i++) {

                var reminderdate1 = new Date(data[i].remindersdate);
                if (reminderdate1 <= d2 && data[i].reminder == 1) {
                    var len = data[i].TaskText.length;
                    var taskText = '';
                    if (len > 65) {
                        taskText = data[i].TaskText.substr(0, 65) + '...';
                    } else {
                        taskText = data[i].TaskText;
                    }

                    $('#note-actions').remove();
                    var ele = document.getElementById('actions');
                    if (ele != null) {
                        var subb = document.createElement('div');
                        subb.setAttribute('id', "note-actions")
                        subb.className = "alrt-actions";
                        ele.appendChild(subb);
                        var sub = document.createElement('div');
                        sub.className = "note-actions-wrap";
                        subb.appendChild(sub);
                        var sub1 = document.createElement('span');
                        sub1.className = "icon icon-bell open-slide";
                        sub.appendChild(sub1);
                        sub2 = document.createElement('div');
                        sub2.className = "note-actions-content";
                        sub.appendChild(sub2);
                        sub3 = document.createElement('div');
                        sub3.className = "notes-wrp";
                        sub2.appendChild(sub3);
                        sub4 = document.createElement('h4');
                        sub4.innerHTML = "Action Reminder";
                        sub5 = document.createElement('p');
                        sub5.innerHTML = taskText + "/" + data[i].remindersdate;
                        sub3.appendChild(sub4);
                        sub3.appendChild(sub5);
                        sub6 = document.createElement('div');
                        sub6.className = "btns-note-wrap";
                        sub2.appendChild(sub6);
                        sub7 = document.createElement('a');
                        sub7.className = "btn btn-default next-task";
                        sub7.innerHTML = "Next";
                        sub7.setAttribute("id", i);
                        sub7.onclick = NextEvent;
                        sub6.appendChild(sub7);
                        sub8 = document.createElement('a');
                        sub8.className = "btn btn-default dismiss-task";
                        sub8.innerHTML = "Dismiss";
                        sub8.setAttribute("id", data[i].TaskPrimaryKeyid + "dismiss");
                        sub8.onclick = dynamicdismissEvent;
                        sub6.appendChild(sub8);
                        break;
                    }
                }
            }
        }
    }

    return true;
}
///////////////////////////////////////////////////////////////////////////////////////
//
////////////////////////////////////////////////////////////////////////////////////////

function NextEvent() {
    var id = $(this).attr('id');
    updatescrollbar(data, parseInt(id) + 1);
}
///////////////////////////////////////////////////////////////////////////////////////
//
////////////////////////////////////////////////////////////////////////////////////////
function dynamicdismissEvent() {

    var id = $(this).attr('id');
    data = null;
    localStorage.clear();
    var textId = id.split("dismiss", 1);
    var text = $("#" + textId).html();
    var model = {
        TaskPrimaryKeyid: textId,
        TaskText: text,
        Type: '',
        reminder_status: "dismiss",
        reminder: '1',
        Is_deleted: "0"

    };
    var modeldata = JSON.stringify(model);
    AddTodaysAction('POST', modeldata, "OTHER");
    $('#note-actions').remove();
}
///////////////////////////////////////////////////////////////////////////////////////
//
////////////////////////////////////////////////////////////////////////////////////////
function MouseOver() {
    $(this).popover('show');

}
///////////////////////////////////////////////////////////////////////////////////////
//
////////////////////////////////////////////////////////////////////////////////////////
function MouseLeave() {
    $(this).popover('hide');

}
///////////////////////////////////////////////////////////////////////////////////////
//
////////////////////////////////////////////////////////////////////////////////////////



