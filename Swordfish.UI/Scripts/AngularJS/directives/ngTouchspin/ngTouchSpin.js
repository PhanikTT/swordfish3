angular.module('jkuri.touchspin', [])

.directive('ngTouchSpin', ['$timeout', '$interval', function ($timeout, $interval) {
    'use strict';

    var setScopeValues = function (scope, attrs) {
        scope.min = attrs.min || 0;
        scope.max = attrs.max || 100;
        scope.step = attrs.step || 1;
        scope.prefix = attrs.prefix || undefined;
        scope.postfix = attrs.postfix || undefined;
        scope.decimals = attrs.decimals || 0;
        scope.stepInterval = attrs.stepInterval || 500;
        scope.stepIntervalDelay = attrs.stepIntervalDelay || 500;
        scope.initval = attrs.initval || '';
        scope.val = attrs.min || scope.initval;
    };

    return {
        restrict: 'EA',
        require: '?ngModel',
        scope: true,
        replace: true,
        link: function (scope, element, attrs, ngModel) {
            setScopeValues(scope, attrs);

            var timeout, timer, helper = true, oldval = scope.val, clickStart;
            scope.oldval = scope.val;
            ngModel.$setViewValue(scope.val);

            scope.decrement = function () {
                oldval = scope.val;
                var value = parseFloat(parseFloat(Number(scope.val)) - parseFloat(scope.step)).toFixed(scope.decimals);
                var min = parseFloat(scope.min);
                if (value < min) {
                    value = parseFloat(scope.min).toFixed(scope.decimals);
                    scope.val = value;
                    ngModel.$setViewValue(value);
                    return;
                }

                scope.val = value;
                ngModel.$setViewValue(value);
            };

            scope.increment = function () {
                oldval = scope.val;
                var value = parseFloat(parseFloat(Number(scope.val)) + parseFloat(scope.step)).toFixed(scope.decimals);
                var max = parseFloat(scope.max);
                if (value > max) return;

                scope.val = value;
                ngModel.$setViewValue(value);
            };

            scope.startSpinUp = function () {
                scope.checkValue();
                scope.increment();
                scope.basket.isDisableSaveQuote = false;
                scope.basket.isDirty = true;
                scope.basket.quoteReference = null;
                clickStart = Date.now();
                scope.stopSpin();

                $timeout(function () {
                    timer = $interval(function () {
                        scope.increment();
                    }, scope.stepInterval);
                }, scope.stepIntervalDelay);
            };

            scope.startSpinDown = function () {
                scope.checkValue();
                scope.decrement();
                scope.basket.isDisableSaveQuote = false;
                scope.basket.isDirty = true;
                scope.basket.quoteReference = null;
                clickStart = Date.now();
                var timeout = $timeout(function () {
                    timer = $interval(function () {
                        scope.decrement();
                    }, scope.stepInterval);
                }, scope.stepIntervalDelay);
            };
            scope.stopSpin = function () {
                if (Date.now() - clickStart > scope.stepIntervalDelay) {
                    $timeout.cancel(timeout);
                    $interval.cancel(timer);
                } else {
                    $timeout(function () {
                        $timeout.cancel(timeout);
                        $interval.cancel(timer);
                    }, scope.stepIntervalDelay);
                }
            };

            scope.checkValue = function () {
                var val;
                
                if (scope.val !== '' && !scope.val.match(/^-?(?:\d+|\d*\.\d+)$/i)) {
                    val = oldval !== '' ? parseFloat(oldval).toFixed(scope.decimals) : parseFloat(scope.min).toFixed(scope.decimals);
                    scope.val = val;
                    ngModel.$setViewValue(val);
                }

                if (parseFloat(scope.val) < parseFloat(scope.min) || parseFloat(scope.val) > parseFloat(scope.max)) scope.val = scope.min;
            };

            scope.updateValue = function () {
                scope.checkValue();
                var value = parseFloat(Number(scope.val));
                var min = parseFloat(scope.min);
                var max = parseFloat(scope.max);
                if (value >= min && value <= max) {
                    ngModel.$setViewValue(value);
                    if (scope.oldval != scope.val) {
                        scope.basket.isDirty = true;
                        scope.basket.isDisableSaveQuote = false;
                    }
                    scope.oldval = scope.val;
                }
            };
        },
        template:
		'<div class="input-group">' +
		'  <span class="input-group-btn" ng-show="!verticalButtons">' +
		'    <button class="btn btn-default" ng-mousedown="startSpinDown()" ng-mouseup="stopSpin()"><i class="fa fa-minus"></i></button>' +
		'  </span>' +
		'  <span class="input-group-addon" ng-show="prefix" ng-bind="prefix"></span>' +
		'  <input type="text" ng-model="val" class="form-control" ng-blur="updateValue()">' +
		'  <span class="input-group-addon" ng-show="postfix" ng-bind="postfix"></span>' +
		'  <span class="input-group-btn" ng-show="!verticalButtons">' +
		'    <button class="btn btn-default" ng-mousedown="startSpinUp()" ng-mouseup="stopSpin()"><i class="fa fa-plus"></i></button>' +
		'  </span>' +
		'</div>'
    };

}]);