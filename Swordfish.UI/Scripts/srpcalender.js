﻿var calendarpromises = [];

function LoadingWeekCalender() {
    LoadCalendar("1", '-3');
    LoadCalendar("2", '-2');
    LoadCalendar("3", '-1');
    LoadCalendar("5", '1');
    LoadCalendar("6", '2');
    LoadCalendar("7", '3');

    if (calendarpromises != undefined && calendarpromises != null && calendarpromises != []) {
        $.when.apply(null, calendarpromises).done(function () {
            $(".calendar-loading").hide();
        });
    }
}

function LoadCalendar(callerCnt, forDay) {
    var showPrice = '';
    var showsrpDt = '';

    var request = $.ajax({
        crossDomain: true,
        url: '../Quote/GetWeekData',
        type: 'GET',
        data: { Caller: callerCnt, ForDay: forDay },
        contentType: 'application/json; charset=utf-8',
        dataType: "json",
        cache: false,
        async: true,
        success: function (respnsedt) {
            var list = JSON.parse(respnsedt);
            if (list != null && list != undefined && list != '' && list != []) {
                if (list.length > 0) {
                    showPrice = "<a onclick=ShowSelDtData('" + callerCnt + "','" + list[0].substring(0, 10) + "')>";
                    if (list[1] == "Not Found") {
                        showPrice = showPrice + list[0].substring(0, 10) + "</br>" + list[1] + "";
                    }
                    else {
                        showPrice = showPrice + list[0].substring(0, 10) + "</br> [ " + list[1] + " ]";
                    }                    
                }
            }

            $("#spndt" + callerCnt).html(showPrice);
            $("#spndt" + callerCnt).show();
            $("#Calloader" + callerCnt).hide();
        },
        error: function (xhr, status, error) {
            ShowAjaxRequestError(xhr, status, error);
            $("#SRPCalenDiv").hide();
        }
    });

    calendarpromises.push(request);
}

function ShowSelDtData(Callid, ForDate) {
    $('#loader').show();
    var CallDatafor = { callID: Callid, showDataforDt: ForDate };

    $.ajax({
        url: '../Quote/ShowCalender',
        type: 'POST',
        data: JSON.stringify(CallDatafor),
        contentType: 'application/json; charset=utf-8',
        success: function (data) {
            if (data.Message != undefined && data.Message != '') {
                alert(data.Message);
                $('#loader').hide();
            }
            else if (data.Message == "") {
                alert('Data Not Available!');
            }
            else {
                window.location.href = data.RedirectUrl;
            }
        },
        error: function (xhr, status, error) {
            ShowAjaxRequestError(xhr, status, error);
        }
    });

}



