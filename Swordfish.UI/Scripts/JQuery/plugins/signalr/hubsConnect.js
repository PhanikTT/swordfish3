proxies.chilliHub= this.createHubProxy('chilliHub'); 
    proxies.chilliHub.client = { };
    proxies.chilliHub.server = {
        connect: function () {
            return proxies.transactionHub.invoke.apply(proxies.chilliHub, $.merge(["Connect"], $.makeArray(arguments)));
         }
    };
