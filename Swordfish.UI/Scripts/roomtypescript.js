﻿$(document).ready(function () {
    var fisrtroom = 1;
    var firstroomAdult = 2;
    $('.select_people_ok').css('pointer-events', 'all');
    //var completestring = Number(firstroomAdult) + " People Staying in " + fisrtroom + " Room";
    //$('#txtResult').val(completestring);
    $('#Paxroom').on('change', function () {

        RemoveRoomRows();
        AddRoomRows();
    });

    $(document).on('focus', '.ch_margin', function () {

        var id = $(this).attr('id');
        previousselected = $("#" + id + " option:selected").val();
    });
    $(document).on('focus', '.ch_margin1', function () {

        var id = $(this).attr('id');
        previousselected = $("#" + id + " option:selected").val();

    });
    $(document).on('focus', '.adultclass', function () {

        var id = $(this).attr('id');
        previousselected = $("#" + id + " option:selected").val();

    });
    $(document).on('change', '.adultclass', function () {

        var id = $(this).attr('id');
        var room = id.charAt(id.length - 1);
        if (room == 1) {
            var previous = previousadultvalue1;
        } else if (room == 2) {
            var previous = previousadultvalue2;
        } else if (room == 3) {
            var previous = previousadultvalue3;
        }
        var model = paxinroom(id, previous);


    });
    $(document).on('change', '.ch_margin', function () {
        var id = $(this).attr('id');
        var room = id.charAt(id.length - 1);
        if (room == 1) {
            var previous = previouschildvalue1;
        } else if (room == 2) {
            var previous = previouschildvalue2;
        } else if (room == 3) {
            var previous = previouschildvalue3;
        }
        var model = paxinroom(id, previous);
        Removechild(model.room, model.child,previous);
        AddChild(model.room, id, model.value, previous);
    });
    $(document).on('change', '.ch_margin1', function () {

        var id = $(this).attr('id');
        var room = id.charAt(id.length - 1);
        if (room == 1) {
            var previous = previousinfantvalue1;
        } else if (room == 2) {
            var previous = previousinfantvalue2;
        } else if (room == 3) { var previous = previousinfantvalue3; }
        var model = paxinroom(id, previous);
        RemoveInfants(model.room, model.infants, previous);
        AddInfant(model.room, id, model.value, previous);
    });
});

var previousselected = null;
var previousroomvalue = 1;
var previouschildvalue1 = 0;
var previousinfantvalue1 = 0;
var previousadultvalue1 = 2;
var previouschildvalue2 = 0;
var previousinfantvalue2 = 0;
var previousadultvalue2 = 1;
var previouschildvalue3 = 0;
var previousinfantvalue3 = 0;
var previousadultvalue3 = 1;



function RemoveInfants(room, infants,previous) {
    if (infants == 0) {
        $('#dynamiclabelinfantage' + room).remove();
    } else if (parseInt(previous) > parseInt(infants)) {

        for (var i = parseInt(infants) + 1 ; i <= parseInt(previous) ; i++) {

            $('#dynamicrowInfantage' + i + room).remove();

        }
    }
    return true;
}
function Removechild(room, child, previous) {


    if (child != 0 && previous > child) {

        for (var i = parseInt(child) + 1 ; i <= parseInt(previous) ; i++) {

            $('#dynamicrowchildage' + i + room).remove();

        }
    } else if (child == 0) {
        $('#dynamiclabelchildage' + room).remove();
    }
    return true;
}
function paxinroom(id, change) {

    var room = id.charAt(id.length - 1);
    var value = $('#' + id).val();
    var adults = $("#DynamicAdultrow" + room).val();
    var child = $("#Dynamicchildrow" + room).val();
    var infants = $("#Dynamicinfantrow" + room).val();
    var model =
        {
            adults: adults,
            child: child,
            infants: infants,
            value: value,
            room: room
        };
    return model;
}
function AddRoomRows() {
    var room = $('#Paxroom').val();
    if (parseInt(previousroomvalue) < parseInt(room)) {
        for (var i = parseInt(previousroomvalue) + 1 ; i <= room; i++) {
            if ($('#dynamicroom' + i).length == 0) {

                $('#dynamicroom').append(' <div class="row" id="dynamicroom' + i + '">'
                   + '  <div class="col-lg-12">'
                        + ' <div class="col-lg-2">'
                           + '  <label class="control-label pull-left">Room ' + i + ':</label>'
                        + ' </div>'
                       + '  <div class="col-lg-3" id="DynamicAdultdivrow' + i + '">'
                          + '   <label class="control-label pull-left ">'
                              + '   <select class="form-control col-lg-1 pull-left adultclass" id="DynamicAdultrow' + i + '">'
                                  + '   <option>1</option>'
                                  + '   <option>2</option>'
                                   + '  <option>3</option>'
                                   + '  <option>4</option>'
                                   + '  <option>5</option>'
                               + '  </select>'
                          + '   </label>'
                        + ' </div>'
                        + ' <div class="col-lg-3" id="Dynamicchilddivrow' + i + '">'
                           + '  <label class="control-label pull-left">'
                             + '    <select class="form-control col-lg-1 pull-left ch_margin" id="Dynamicchildrow' + i + '">'
                              + '     <option>0</option>'
                                + '     <option>1</option>'
                                 + '    <option>2</option>'
                                  + '   <option>3</option>'
                                   + '  <option>4</option>'
                               + '  </select>'
                            + ' </label>'
                      + '   </div>'
                        + ' <div class="col-lg-3" id="Dynamicinfantdivrow' + i + '">'
                          + '   <label class="control-label pull-left">'
                               + '  <select class="form-control col-lg-1 pull-left ch_margin1" id="Dynamicinfantrow' + i + '">'
                                + '     <option>0</option>'
                                  + '   <option>1</option>'
                                   + '  <option>2</option>'
                                + ' </select>'
                            + ' </label>'
                       + '  </div>'
                  + '   </div>'
               + '  </div>');
            }
        }
    }
    previousroomvalue = room;
    previousselected = room;
}
function RemoveRoomRows() {

    var room = $('#Paxroom').val();

    if (parseInt(previousroomvalue) > parseInt(room)) {
        for (var i = parseInt(room) + 1 ; i <= previousroomvalue; i++) {

            $('#Dynamicrow' + i).remove();
            $('#dynamicroom' + i).remove();

        }
    }
}
function AddChild(room, id, value) {
    if (room == 1) {
        var previous = previouschildvalue1;
    } else if(room == 2)
    {
        var previous = previouschildvalue2;
    } else if (room == 3) { var previous = previouschildvalue3; }
    if (previous == 0) {
        var labelstring = '<label class="control-label pull-left add_child" id="dynamiclabelchildage' + room + '"></label>';
        $('#Dynamicchilddivrow' + room).append(labelstring);
    }
    var string = '';
    if (previous < value) {
        for (var i = parseInt(previous) + 1 ; i <= value; i++) {
            if ($('#dynamicrowchildage' + i + room).length == 0) {
                string = string + '<select class="form-control col-lg-1 pull-left" id="dynamicrowchildage' + i + room + '">'
                                + '  <option>3yrs</option>'
                                  + '  <option>4yrs</option>'
                               + '   <option>5yrs</option>'
                               + '   <option>6yrs</option>'
                                + '  <option>7yrs</option>'
                                + '  <option>8yrs</option>'
                                + '  <option>9yrs</option>'
                                + '  <option>10yrs</option>'
                                + '  <option>11yrs</option>'
                                + '  <option>12yrs</option>'
                              + '   </select>';
            }

        }
        $('#dynamiclabelchildage' + room).append(string);
    }
    previousvalue(value, room, "child");  
    previousselected = value;
}
function previousvalue(value, room, child)
{
    if (room == 1 && child == 'child') {
        previouschildvalue1 = value;
       
    } else if (room == 2 && child == 'child')
    {
        previouschildvalue2 = value;
       
    } else if (room == 3 && child == 'child')
    {
        previouschildvalue3 = value;
      
    }else if (room == 1 && child == 'infant') {
        previousinfantvalue1 = value;
       
    } else if (room == 2 && child == 'infant') {
        previousinfantvalue2 = value;
       
    } else if (room == 3 && child == 'infant') {
        previousinfantvalue3 = value;
       
    } else if (room == 1 && child == 'adult') {
        previousadultvalue1 = value;
        
    } else if (room == 2 && child == 'adult') {
        previousadultvalue2 = value;
       
    } else if (room == 3 && child == 'adult') {
        previousadultvalue3 = value;
      
    }
}
function AddInfant(room, id, value) {
    if (room == 1) {
        var previous = previousinfantvalue1;
    } else if (room == 2) {
        var previous = previousinfantvalue2;
    } else if (room == 3) { var previous = previousinfantvalue3; }
    var string = '';
    if (previous == 0) {
        var labelstring = '<label class="control-label pull-left add_infant" id="dynamiclabelinfantage' + room + '"></label>';
        $('#Dynamicinfantdivrow' + room).append(labelstring);
    }
    if (previous < value) {
        for (var i = parseInt(previous) + 1 ; i <= value; i++) {
            if ($('#dynamicrowInfantage' + i + room).length == 0) {
                string = string + '<select class="form-control col-lg-1 pull-left infantclass" id="dynamicrowInfantage' + i + room + '">'
                     + '    <option>0yrs</option>'
                               + '  <option>1yrs</option>'
                               + '   <option>2yrs</option>'
                              + '   </select>';
            }

        }
        $('#dynamiclabelinfantage' + room).append(string);
    }
    previousvalue(value, room, "infant");
    previousselected = value;
}

function passengerswidget() {
    $(".spinptrtbtn").toggleClass('expand');
    if ($(".spinptrtbtn").hasClass('expand'))
    { $("#room_pax_wrapper").fadeIn(500); } else { $("#room_pax_wrapper").fadeOut(); }
}
$(document).on('click', '.select_people_ok', function () {
    
    passengerswidget();
    var room = $('#Paxroom').val();
    if (room == 'undefined')
    { room = 1 }
    var Adults = $('#DynamicAdultrow1').val();
    var Child = $('#Dynamicchildrow1').val();
    var Infant = $('#Dynamicinfantrow1').val();

    for (i = 2; i <= parseInt(room) ; i++) {
        var RoomAdults = $('#DynamicAdultrow' + i).val();
        Adults = parseInt(Adults) + parseInt(RoomAdults);
        var RoomChild = $('#Dynamicchildrow' + i).val();
        Child = parseInt(Child) + parseInt(RoomChild);
        var RoomInfant = $('#Dynamicinfantrow' + i).val();
        Infant = parseInt(Infant) + parseInt(RoomInfant);
    }

    var searchType = "";
    var isFlightSelected = $("#btnFlightonly").hasClass("purple");
    if (isFlightSelected == true) {
        searchType = "Flight";
    }

    if (searchType == "Flight") {
        var completestring = Adults + " Adults," + Child + " Child," + Infant + " Infant";
    }
    else {
        var completestring = Adults + " Adults," + (parseInt(Child) + parseInt(Infant)) + " children" + " Staying in " + room + " Room";
    }
    $('#hdnroom').val(room);
    $('#hdnadult').val(Adults);
    $('#hdnchildren').val(Child);
    $('#hdninfant').val(Infant);
    OccupancyRooms = getOccupancyRooms();
    $('#txtResult').val(completestring);
   

});