﻿$(document).ready(function () {
    $('#global-nav-logo').click(function () {
        RedirectNavigationUrl()
    });

    $('#global-nav-addCustomer').click(function () {
        $('ul').children().removeClass('active');
        $(this).addClass('active');
        $("#gridFilter").hide();
        $("#gridQuoteSearch").hide();
        $("#grdCustomer").show();
        RedirectToCustomerProfile();
    });
   


        $('#global-nav-search').click(function () {
        $('ul').children().removeClass('active');
        $(this).addClass('active');

        $("#gridFilter").hide();
        $("#grdCustomer").hide();
        $("#gridQuoteSearch").show();

        $.ajax({
            url: '../Quote/QuoteSearch',
            contentType: 'application/html; charset=utf-8',
            type: 'GET',
            dataType: 'html'
        })
            .success(function (searchResult) {
            $("#gridQuoteSearch").html(searchResult);
            $.ajax({
                url: '../Home/Search',
                contentType: 'application/html; charset=utf-8',
                type: 'GET',
                dataType: 'html'
            }).success(function (searchPage) {
                $("#divSearchPage").html(searchPage);
                $("#divSearchPage").css("display", "inline-block");
                LoadQuoteSearchActions();
            }).error(function (xhr, status, error) {
                ShowAjaxRequestError(xhr, status, error)
            });
        })
            .error(function (xhr, status, error) {
                ShowAjaxRequestError(xhr, status, error)
        });
    });

    $('#global-nav-add').click(function () {
        if ($('#transferpartial').is(':visible') == false) {
            var sourceWidgetvalue = $('#hdnSourceFlightWidget').val();

            if (sourceWidgetvalue == "" || sourceWidgetvalue == null) {
                $('#divAlertBox').show();
                $('.alert-box h4').text("Please select atleast one record.");
                return false;
            }

            if (sourceWidgetvalue != "") {
                $('#global-filter-data-wrapper').hide();
                $('#filter').hide();
                $('#pagination').hide();
                transfersviewModel.leftIconDynamicClick();
            }
        }        
    });

    //$('#global-nav-filters').click(function () {
    //    $("#gridFilter").show();
    //    $("#gridQuoteSearch").hide();
    //    $("#transfers_nav_wrapper").hide();
    //    $("#transferpartial").hide();
    //    $('#global-filter-data-wrapper').show();
    //    $('#filter').show();
    //    $('#pagination').show();
    //    $('#expandBasket').show();

    //    //if (actionType != undefined && actionType != '' && actionType != null) {
    //    //    if (actionType == 'Flight') {
    //    //        $('#expandBasket').hide();
    //    //    }
    //    //}

    //    $('ul').children().removeClass('active');
    //    $(this).addClass('active');
    //});

    $("#headerlogo").click(function () {
        RedirectNavigationUrl();
    });

  

    $('#agentquotesdata').click(function () {
        var urlType = "AgentQuotes"
        var model = {
            UrlType: urlType,
        };
        var modeldata = JSON.stringify(model);
        $.ajax({
            type: "POST",
            crossDomain: true,
            url: '../Quote/RedirectionUrl',
            cache: false,
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            data: modeldata,
            complete: function () {

            },
            success: function (response) {
                if (response != "") {
                    window.location.href = response.RedirectUrl;
                }
            }
        });
    });

    $('#customerquotes').click(function () {
        var urlType = "CustomerQuotes"
        var model = {
            UrlType: urlType,
        };
        var modeldata = JSON.stringify(model);
        $.ajax({
            type: "POST",
            crossDomain: true,
            url: '../Quote/RedirectionUrl',
            cache: false,
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            data: modeldata,
            complete: function () {

            },
            success: function (response) {
                if (response != "") {
                    window.location.href = response.RedirectUrl;
                }
            }
        });
    });

    $('#addCustomer').click(function () {
        RedirectToCustomerProfile();
    });

    function RedirectNavigationUrl() {
        var urlType = "Dashboard"
        var model = {
            UrlType: urlType,
        };

        var modeldata = JSON.stringify(model);

        $.ajax({
            type: "POST",
            crossDomain: true,
            url: '../Quote/RedirectionUrl',
            cache: false,
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            data: modeldata,
            complete: function () {

            },
            success: function (response) {
                if (response != "") {
                    window.location.href = response.RedirectUrl;
                }
            }
        });
    }
    function RedirectToCustomerProfile() {
        var urlType = "AddCustomer"
        var model = {
            UrlType: urlType,
        };
        var modeldata = JSON.stringify(model);
        $.ajax({
            type: "POST",
            crossDomain: true,
            url: '../Quote/RedirectionUrl',
            cache: false,
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            data: modeldata,
            complete: function () {

            },
            success: function (response) {
                if (response != "") {
                    window.location.href = response.RedirectUrl;
                }
            }
        });
    }
});
