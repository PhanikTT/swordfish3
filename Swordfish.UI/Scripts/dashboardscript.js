﻿var transfer = true;
var today = new Date();
var nextday = new Date(today.getFullYear(), today.getMonth(), today.getDate() + 1);
var startdate = new Date();
var enddate = new Date();
var initDays = 14;
var holidayDuration = 7;

startdate.setDate(startdate.getDate() + initDays);
enddate.setDate(enddate.getDate() + (initDays + holidayDuration));

function getOccupancyRooms() {
    var OccupancyRooms = [];
    var rooms = $('#hdnroom').val();
    if (!isNaN(rooms)) {
        var roomsCount = parseInt(rooms);
        for (var i = 0; i < roomsCount; i++) {
            var room = {RoomNo :0, AdultsCount: 0, ChildCount: 0, InfantCount: 0, ChildAge: [], InfantAge: [] };
            var children = 0, Infants = 0
            var adults = $('#DynamicAdultrow' + (i + 1)).val();
            if ($('#hdnchildren').val() > 0)
                children = $('#Dynamicchildrow' + (i + 1)).val();
            if ($('#hdninfant').val() > 0)
                Infants = $('#Dynamicinfantrow' + (i + 1)).val();
            if (!isNaN(adults))
                room.AdultsCount = parseInt(adults);
            if (!isNaN(children))
                room.ChildCount = parseInt(children);
            if (!isNaN(Infants))
                room.InfantCount = parseInt(Infants);
            room.RoomNo = (i + 1);
            if (room.ChildCount > 0) {
                for (var j = 0; j < room.ChildCount; j++) {
                    var childAge = $('#dynamicrowchildage' + (j + 1) + (i + 1)).val();
                    childAge = childAge.match(/[a-zA-Z]+|[0-9]+/g)[0];
                    if (!isNaN(childAge))
                        room.ChildAge.push(parseInt(childAge));
                }
            }
            if (room.InfantCount > 0) {
                for (var j = 0; j < room.InfantCount; j++) {
                    var infantAge = $('#dynamicrowInfantage' + (j + 1) + (i + 1)).val();
                    infantAge = infantAge.match(/[a-zA-Z]+|[0-9]+/g)[0];
                    if (!isNaN(infantAge))
                        room.InfantAge.push(parseInt(infantAge));
                }
            }
            OccupancyRooms.push(room);
        }
    }
    return OccupancyRooms;
}
$(document).ready(function () {
    $('#sel1').val(7);
    $('.close_error_summary').click(function () {
        $('.errror_summery_wrapper1').hide();
    })
    setTimeout(function () {
        $("#alert_close").click(function () {
            $('.alert-wrapper').hide();
        })
        $('.message-field-new').fadeOut("slow", function () {
        })
    }, 2000);
    $(".dropdown-toggle").dropdown();
    $("input[name='txtRooms']").TouchSpin({
        min: 1,
        max: 10
    });
    $("input[name='txtAdults']").TouchSpin({
        min: 1,
        max: 10
    });
    $("input[name='txtChildren']").TouchSpin({
        min: 0,
        max: 10
    });
    $("input[name='txtBags']").TouchSpin({
        min: 0,
        max: 20
    });
    $('#txtCheckIn').daterangepicker({
        // autoUpdateInput: new Date(),
        minDate: nextday,
        startDate: startdate,
        endDate: enddate,

        //  maxDate:ToEndDate,
        separator: ' to ',
        locale: {
            cancelLabel: 'Close',
            format: 'DD-MM-YYYY',
            separator: ' - '
        },
        orientation: "top auto",
        autoclose: true
    });

    var SearchResults = getCookie("SearchResults");

    if (SearchResults != null && SearchResults != '') {
        Searchresults(JSON.parse(SearchResults));
    }
    OccupancyRooms = getOccupancyRooms();
    $("#txtRooms,#txtAdults,#txtChildren").change(function () {
        displayVals();
    });

    $("#btnHolidays").click(function () {
        $('#txtSourceFrom').val('');
        $('#txtDestinationCode').val('');
    });

    $("#btnFlightonly").click(function () {
        $('#txtSourceFrom').val('');
        $('#txtDestinationCode').val('');
    });

    $("#btnHotelonly").click(function () {
        $('#txtSourceFrom').val('');
        $('#txtDestinationCode').val('');
    });

    $("#txtDestinationCode").autocomplete({
        source: function (request, response) {
            $.ajax({
                url: '../Home/GetHotelDestinations',
                type: "GET",
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                data:
                     {
                         searchterm: $('#txtDestinationCode').val(),
                     },
                success: function (data) {
                    response($.map(data, function (item) {
                        return { label: item.dest, value: item.dest, id: item.d };
                    }))
                },
                error: function (xhr, status, error) {
                    ShowAjaxRequestError(xhr, status, error);
                }
            })
        },
        selectFirst: true,
        minLength: 3,
        select: function (event, ui) {
            var selVal = ui.item.id;
            $('#hdnDestinationTo').val(selVal);

            if (selVal == null || selVal == '') {
                $('#hdnDestinationTo').val('');
                $('#ddlHotel').empty();
                var items = [];
                items.push("<option value='0'>--Hotel Name--</option>");
                $("#ddlHotel").html(items.join(' '));
            }
            else {
                $("#ddlHotel").prop('disabled', false);
                $.ajax({
                    crossDomain: true,
                    url: '../Home/GetHotelList',
                    type: "GET",
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    data:
                         {
                             destinationCode: selVal,
                         },
                    success: function (response) {
                        if (response != "") {
                            var items = [];
                            items.push("<option value='0'>--Hotel Name--</option>");
                            $.each(response, function () {
                                items.push("<option value=" + this.SupplierAccommId + ">" + this.Name + "</option>");
                            });
                            $("#ddlHotel").html(items.join(' '));
                            //$("#ddlHotel").val() = response;
                        }
                        else {
                            $('#ddlHotel').empty();
                            var items = [];
                            items.push("<option value='0'>--Hotel Name--</option>");
                            $("#ddlHotel").html(items.join(' '));
                        }
                    },
                    error: function (xhr, status, error) {
                        ShowAjaxRequestError(xhr, status, error);
                    }
                });
            }
        }
    });
    $("#txtSourceFrom").autocomplete({
        source: function (request, response) {
            $.ajax({
                url: '../Home/GetAirportSources',
                type: "GET",
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                data:
                     {
                         searchterm: $('#txtSourceFrom').val(),
                     },
                success: function (data) {
                    response($.map(data, function (item) {
                        return { label: item.desc, value: item.desc, id: item.code };
                    }))
                },
                error: function (xhr, status, error) {
                    ShowAjaxRequestError(xhr, status, error);;
                }
            })
        },
        selectFirst: true,
        minLength: 3,
        select: function (event, ui) {
            var selVal = ui.item.id;
            $('#hdnSourceFrom').val(selVal);
        },
        focus: function (event, ui) {
            if (ui.item.loading) {
                event.preventDefault();
            }
        },
        onHint: function (hint) {
            $('.alert-wrapper').show();
            $('.alert-box h4').text(hint);
            $('#txtSourceFrom').val(hint);
        },
        open: function () {
            $(this).removeClass("ui-corner-all").addClass("ui-corner-top");
        },
        close: function () {
            $(this).removeClass("ui-corner-top").addClass("ui-corner-all");
        }
    });

    $.ui.autocomplete.prototype._renderItem = function (ul, item) {
        var term = this.term.split(' ').join('|');
        var re = new RegExp("(" + term + ")", "gi");
        var t = item.label.replace(re, "<b>$1</b>");
        return $("<li></li>")
           .data("hdnDestinationTo", item)
           .append("<a>" + t + "</a>")
           .appendTo(ul);
    };

    $.ui.autocomplete.prototype._renderItem = function (ul, item) {
        var term = this.term.split(' ').join('|');
        var re = new RegExp("(" + term + ")", "gi");
        var t = item.label.replace(re, "<b>$1</b>");
        return $("<li></li>")
           .data("SourceId", item.d)
           .append("<a>" + t + "</a>")
           .appendTo(ul);
    };

    $("#txtCheckIn").change(function () {
        if ($("#txtCheckIn").val() != null) {
            var checkInDate = $("#txtCheckIn").val().split("-");
            if (checkInDate.length > 3) {
                var checkIn = new Date(checkInDate[2], checkInDate[1] - 1, checkInDate[0]);
                var checkOut = new Date(checkInDate[5], checkInDate[4] - 1, checkInDate[3]);
                var diff = checkOut - checkIn;
                var days = diff / 1000 / 60 / 60 / 24;
                if (days > 0 && days < 31) {
                    $('#sel1').val(days);
                } else if (days == 0) {
                    $('#sel1').val(1);
                }
                else {
                    $('.error_container').html("Please select in between 1 to 30 days/nights");
                    $('.errror_summery_wrapper1').show();
                    $('#sel1').val(0);
                }
            }
        }
    });

    $("#sel1").change(function () {
        if ($("#sel1").val() > 0 && $("#txtCheckIn").val() != null) {
            // var FDate = $.datepicker.formatDate("dd-mm-yy", new Date());
            var checkInDate = $("#txtCheckIn").val().split("-");//FDate.split("-");
            var nights = $("#sel1").val();
            var todayDate = new Date(checkInDate[2], checkInDate[1] - 1, checkInDate[0]);
            var checkOutDate = todayDate.setDate(todayDate.getDate() + parseInt(nights));
            var toDate = $.datepicker.formatDate('dd-mm-yy', new Date(checkOutDate));
            $("#txtCheckIn").val(checkInDate[0] + '-' + checkInDate[1] + '-' + checkInDate[2] + '- ' + toDate);
        }
    });

    $('#testforSyntec').click(function () {
        $('#accept_call_hide_show').toggle();
    });

    $('#btnSearch').click(function () {
        try {
            $("#loader").show();
            var resultType = "";
            var flightWay = "TwoWay";
            // this is we need to changed once full functionality is up//
            var txtResult = $('#txtResult').val();
            var dashboardadultvalue = txtResult.charAt(9);
            var dashboardroomvalue = txtResult.charAt(0);
            var adultvalue = $('#hdnadult').val();
            var ChildValue = $('#hdnchildren').val();
            var InfantValue = $('#hdninfant').val();
            var roomvalue = $('#hdnroom').val();
            var TTSSQuote = null
            var TTSSPrice = null;
            var incomingPhoneNumber = null;
            var customerFirstname = null;
            var customerLastName = null;
            var customerEmail = null;
            if (adultvalue == "" && roomvalue == "") {
                adultvalue = dashboardadultvalue;
                roomvalue = dashboardroomvalue;
            }

            // this is we need to changed once full functionality is up//
            var isHolidaySelected = $("#btnHolidays").hasClass("purple");
            var isFlightSelected = $("#btnFlightonly").hasClass("purple");
            var isHotelSelected = $("#btnHotelonly").hasClass("purple");

            if (isHolidaySelected == true) {
                resultType = "Package";
            }
            else if (isFlightSelected == true) {
                resultType = "Flight";
                if ($("#trip_checkbox_wrapper").find("#optionsRadios1").prop("checked") == true) {
                    flightWay = "OneWay";
                }
                else {
                    flightWay = "TwoWay";
                }
            }
            else if (isHotelSelected == true) {
                resultType = "Hotel";
            }

            if (!onSelectSearch(resultType)) {
                $("#loader").hide();
                return false;
            }
            else {
                var sourceFrom = $("#txtSourceFrom").val();
                var sourceFromId = $("#hdnSourceFrom").val();
                var destinationTo = $("#txtDestinationCode").val();
                var destinationToId = $("#hdnDestinationTo").val();
                var checkInDate = $("#txtCheckIn").val().replace(' ', '').split("-");
                var checkIn = null;
                var checkOut = null;
                if (checkInDate.length > 3) {
                    checkIn = checkInDate[2] + "-" + checkInDate[1] + "-" + checkInDate[0];
                    checkOut = checkInDate[5] + "-" + checkInDate[4] + "-" + checkInDate[3];
                } else {
                    checkIn = checkInDate[2] + "-" + checkInDate[1] + "-" + checkInDate[0];
                    checkOut = checkInDate[2] + "-" + checkInDate[1] + "-" + checkInDate[0];
                }

                var nights = $("#sel1").val();
                // this is we need to changed once full functionality is up//
                var rooms = roomvalue;
                var adults = adultvalue;

                var child = ChildValue;
                var infant = InfantValue;
                // this is we need to changed once full functionality is up//
                //var child = $("#txtChildren").val();
                //var bags = $("#txtBags").val();

                var hotel = $("#ddlHotel option:selected").val();

                if (hotel == "--Hotel Name--" || hotel == "Hotel Name") {
                    hotel = "";
                }
                var hotelName = $("#ddlHotel option:selected").text();
                if (hotelName == "--Hotel Name--" || hotelName == "Hotel Name") {
                    hotelName = "";
                }

                var starrating = $('div.caption span.label-danger').text();
                //if ($("#transfers_yes").prop("checked")) {
                //transfer = true;
                //}
                //if ($("#transfers_no").prop("checked")) {
                //    transfer = false;
                //}
                var BoradTypes = "";
                var BoardtypeAI = $('#boradtypeAI').hasClass("purplelight active");
                if (BoardtypeAI == true) {
                    BoradTypes = "AI" + ",";
                }
                var BoradTypeFB = $('#boradtypeFB').hasClass("purplelight active");
                if (BoradTypeFB == true) {
                    BoradTypes = BoradTypes + "FB" + ",";
                }
                var BoradTypeHB = $('#boradtypeHB').hasClass("purplelight active");
                if (BoradTypeHB == true) {
                    BoradTypes = BoradTypes + "HB" + ",";
                }
                var BoradTypeBB = $('#boradtypeBB').hasClass("purplelight active");
                if (BoradTypeBB == true) {
                    BoradTypes = BoradTypes + "BB" + ",";
                }
                var BoradTypeSC = $('#boradtypeSC').hasClass("purplelight active");
                if (BoradTypeSC == true) {
                    BoradTypes = BoradTypes + "SC" + ",";
                }
                var BoradTypeRO = $('#boradtypeRO').hasClass("purplelight active");
                if (BoradTypeRO == true) {
                    BoradTypes = BoradTypes + "RO" + ",";
                }

                if (BoradTypes != null && BoradTypes != undefined) {
                    var boardtypes = BoradTypes.substring(0, BoradTypes.length - 1).trim();
                }
                if ($("#hdnTypeOfSearch").val() == 'TTSS') {
                    TTSSQuote = $("#hdnTTSSQuoteRef").val();
                    TTSSPrice = $("#hdnTTSSPrice").val();
                }
                else if ($("#hdnTypeOfSearch").val() == 'Sintech') {
                    TTSSQuote = $("#ttsQuoteinfo").val();
                    incomingPhoneNumber = $("#IncustomerPhonenumber").val();
                    customerFirstname = $("#Incustomername").val();
                    customerLastName = $("#IncustomerLastName").val();
                    customerEmail = $("#IncustomerEmail").val();
                }
                //var avg = $("#searchAvg").val();
                var model = {
                    SourceFrom: sourceFrom,
                    SourceFromId: sourceFromId,
                    DestinationTo: destinationTo,
                    DestinationToId: destinationToId,
                    CheckIn: checkIn,
                    CheckOut: checkOut,
                    Nights: nights,
                    Rooms: rooms,
                    Adults: adults,
                    Child: child,
                    //Bags: bags,
                    ResultType: resultType,
                    HotelName: hotel,
                    Hotel: hotelName,
                    Transfer: transfer,
                    //Average: avg,
                    Rating: starrating,
                    FlightWay: flightWay,
                    BoardType: boardtypes,
                    Infants: infant,
                    Infants: infant,
                    TTSSQuoteReference: TTSSQuote,
                    TTSSListedPricePerPerson: TTSSPrice,
                    IncomingPhoneNumber: incomingPhoneNumber,
                    CustomerFirstname: customerFirstname,
                    CustomerLastName: customerLastName,
                    CustomerEmail: customerEmail,
                    OccupancyRooms: OccupancyRooms,
                    BoardTypeAI: BoardtypeAI,
                    BoardTypeFB: BoradTypeFB,
                    BoardTypeHB: BoradTypeHB,
                    BoardTypeBB: BoradTypeBB,
                    BoardTypeSC: BoradTypeSC,
                    BoardTypeRO: BoradTypeRO
                };
                checkandSaveCookie(model);
                $.ajax({
                    url: '../Home/Search',
                    type: 'POST',
                    data: JSON.stringify(model),
                    contentType: 'application/json; charset=utf-8',
                    success: function (data) {
                        if (data.Message != undefined && data.Message != '') {
                            $('.alert-wrapper').show();
                            $('.alert-box h4').text(data.Message);
                        }
                        else if (data.Message == "") {
                            $('.alert-wrapper').show();
                            $('.alert-box h4').text('Data Not Available!');
                        }
                        else {
                            if (data.RedirectUrl != undefined)
                                window.location.href = data.RedirectUrl;
                            else {
                                window.location.href = '/Account/Login';
                            }
                        }
                    },
                    error: function (xhr, status, error) {
                        ShowAjaxRequestError(xhr, status, error);
                    }
                });
            }
        }
        catch (ex) {
            $("#loader").hide();
        }
    });
    
    $("#txtDestinationCode").keyup(function () {
        if ($('#txtDestinationCode').val() != null) {

            if ($("#txtDestinationCode").val() == null || $("#txtDestinationCode").val() == '') {
                $("#ddlHotel").prop('disabled', true).css("display", "hidden");
                $('#hdnDestinationTo').val('');
                $('#ddlHotel').empty();
                var items = [];
                items.push("<option value='0'>--Hotel Name--</option>");
                $("#ddlHotel").html(items.join(' '));
            }
        }
    });

    $('#oneway').click(function () {
        $('#txtCheckIn').daterangepicker({
            singleDatePicker: true,
            minDate: nextday,
            startDate: startdate,
            endDate: enddate,
            locale: {
                cancelLabel: 'Close',
                format: 'DD-MM-YYYY',
                separator: ' - '
            },
        });
    });
    $('#roundtrip').click(function () {
        $('#txtCheckIn').daterangepicker({
            singleDatePicker: false,
            minDate: nextday,
            startDate: startdate,
            endDate: enddate,
            separator: ' to ',
            locale: {
                cancelLabel: 'Close',
                format: 'DD-MM-YYYY',
                separator: ' - '
            },
            orientation: "top auto",
            autoclose: true
        });
    });
});

function setCookie(cname, cvalue) {
    var d = new Date();
    d.setTime(d.getTime() + (1 * 24 * 60 * 60 * 1000));
    var expires = "expires=" + d.toGMTString();
    document.cookie = cname + "=" + JSON.stringify(cvalue) + ";";
}
function Searchresults(SearchData, hotelFound) {
    $("#loader").show();
    if (SearchData != null && SearchData != "") {
        $("#txtSourceFrom").val(SearchData.SourceFrom);
        $('#hdnSourceFrom').val(SearchData.SourceFromId)
        $("#txtDestinationCode").val(SearchData.DestinationTo);
        $("#hdnDestinationTo").val(SearchData.DestinationToId);
        $("#ddlHotel").prop('disabled', false);
        var selVal = SearchData.DestinationToId;
        if (hotelFound === true) {
            $('#ddlHotel').val(SearchData.MasterHotelId).attr("selected", "selected");
            $('#displayResult').hide();
        }
        else {
            $('#ddlHotel').val("0").attr("selected", "selected");
            if (hotelFound === false) {
                $('#displayResult').show();
            }
        }
        $('#hdnadult').val(SearchData.Adults);
        $('#hdnroom').val(SearchData.Rooms);
        $('#hdnchildren').val(SearchData.Child);
        $('#hdninfant').val(SearchData.Infants);
        $('#hdnTTSSQuoteRef').val(SearchData.TTSSQuoteReference);
        $('#hdnTTSSPrice').val(SearchData.TTSSListedPricePerPerson);
        if (SearchData.Rating != null) {
            $('#input-1').rating('update', parseFloat(SearchData.Rating));
            // method chaining 
            var ratingValue = $('#input-1').rating('update', parseFloat(SearchData.Rating)).val();
        }


        if (SearchData.Nights != null && SearchData.Nights != '') {
            $("#sel1").val(SearchData.Nights);
            var StartDate = new Date();
            var d = new Date();
            var EndDate = new Date(new Date().getTime() + (parseInt(SearchData.Nights) * 24 * 60 * 60 * 1000))
        }
        //CHIL-2417 TTSS Search>>Total Nights issue, In SF we are calculating based on the Departure Date. But in Teletext Holidays website, calculating on Arrival Date.
        //because of this ticket I am replacing CheckIn with TTSSQuote.OutboundArrivalDate.
        if (SearchData.TTSSQuote != undefined && SearchData.TTSSQuote.DepartureDate != undefined && SearchData.TTSSQuote.DepartureDate != null && SearchData.TTSSQuote.DepartureDate != '') {
            Checkin = new Date(SearchData.TTSSQuote.DepartureDate.replace(" ", ""));
            if (Checkin >= d)
                if (SearchData.TTSSQuote.OutboundArrivalDate.indexOf("T") > 0)
                    StartDate = new Date(SearchData.TTSSQuote.DepartureDate.substring(0, SearchData.TTSSQuote.DepartureDate.replace(" ", "").indexOf("T")));
                else {
                    StartDate = Checkin;
                }
            if (SearchData.CheckOut != null && SearchData.CheckOut != '') {
                Checkout = new Date(SearchData.CheckOut.replace(" ", ""));
                if (Checkout > d)
                    if (SearchData.CheckOut.replace(" ", "").indexOf("T") > 0)
                        EndDate = new Date(SearchData.CheckOut.substring(0, SearchData.CheckOut.replace(" ", "").indexOf("T")));
                    else {
                        EndDate = Checkout;
                    }
            }
        }
        else {
            if (SearchData.CheckIn != null && SearchData.CheckIn != '') {
                Checkin = new Date(SearchData.CheckIn.replace(" ", ""))
                if (Checkin >= d)
                    if (SearchData.CheckIn.indexOf("T") > 0)
                        StartDate = new Date(SearchData.CheckIn.substring(0, SearchData.CheckIn.replace(" ", "").indexOf("T")));
                    else {
                        StartDate = Checkin;
                    }
            }
            if (SearchData.CheckOut != null && SearchData.CheckOut != '') {
                Checkout = new Date(SearchData.CheckOut.replace(" ", ""));
                if (Checkout > d)
                    if (SearchData.CheckOut.replace(" ", "").indexOf("T") > 0)
                        EndDate = new Date(SearchData.CheckOut.substring(0, SearchData.CheckOut.replace(" ", "").indexOf("T")));
                    else {
                        EndDate = Checkout;
                    }
            }
        }

        $('#Paxroom').val(SearchData.Rooms).attr("selected", "selected");
        RemoveRoomRows();
        if (SearchData.Rooms == 0) {
            SearchData.Rooms = 1;
            //$('#Paxroom').val(SearchData.Rooms).attr("selected", "selected");
            if (SearchData.Adults > 0 && SearchData.Adults <= 5) {

                $("#DynamicAdultrow1").val(SearchData.Adults).attr("selected", "selected");
            }
            if (SearchData.Adults > 5) {
                $("#DynamicAdultrow1").val(5).attr("selected", "selected");


            }
            $("#dynamicroom").remove();

        } else if (parseInt(SearchData.Rooms) >= 1) {
            AddRoomRows();
            // Below changes are made because of the issue CHIL-2540
            for (var roomIndex = 1; roomIndex <= SearchData.Rooms; roomIndex++) {
                $("#DynamicAdultrow" + roomIndex).val(SearchData.OccupancyRooms[roomIndex - 1].AdultsCount).attr("selected", "selected");
            }
            }
            if (SearchData.Child != 0) {
                for (var roomIndex = 1; roomIndex <= SearchData.Rooms; roomIndex++) {
                    $("#Dynamicchildrow" + roomIndex).val(SearchData.OccupancyRooms[roomIndex - 1].ChildCount).attr("selected", "selected");
                    AddChild(roomIndex, "", SearchData.OccupancyRooms[roomIndex - 1].ChildCount);
                }
            }
            else {
                $("#Dynamicchildrow1").val(SearchData.Child).attr("selected", "selected");
                Removechild(SearchData.Rooms, SearchData.Child, previouschildvalue1);
                previouschildvalue1 = 0;
                previouschildvalue2 = 0;
                previouschildvalue3 = 0;
            }
            if (SearchData.Infants != 0) {

                if (SearchData.Child != 0) {
                    for (var roomIndex = 1; roomIndex <= SearchData.Rooms; roomIndex++) {
                        $("#Dynamicinfantrow" + roomIndex).val(SearchData.OccupancyRooms[roomIndex - 1].InfantCount).attr("selected", "selected");
                        AddInfant(roomIndex, "", SearchData.OccupancyRooms[roomIndex - 1].InfantCount)
                    }
            }
            else {
                $("#Dynamicinfantrow1").val(SearchData.Infants).attr("selected", "selected");
                RemoveInfants(SearchData.Rooms, SearchData.Infants, previousinfantvalue1);
                previousinfantvalue1 = 0;
                previousinfantvalue2 = 0;
                previousinfantvalue3 = 0;

            }

        }
        var CompleteString = $("#txtResult").val(SearchData.Rooms + " Rooms, " + SearchData.Adults + " Adults, " + SearchData.Child + " Children, " + SearchData.Infants + " Infants");
        if (SearchData.BoardType != null && SearchData.BoardType != '') {
            var BoardTypes = SearchData.BoardType.split(',');
            $(".purplelight").removeClass("active");
            for (var i = 0; i < BoardTypes.length; i++) {

                switch (BoardTypes[i]) {
                    case 'AI':
                        $('#boradtypeAI').addClass("active");
                        break;
                    case 'FB':
                        $('#boradtypeFB').addClass("active");
                        break;
                    case 'BB':
                        $('#boradtypeBB').addClass("active");
                        break;
                    case 'SC':
                        $('#boradtypeSC').addClass("active");
                        break;
                    case 'HB':
                        $('#boradtypeHB').addClass("active");
                        break;
                    case 'RO':
                        $('#boradtypeRO').addClass("active");
                        break;
                }
            }

        }

        $('#txtCheckIn').daterangepicker({
            singleDatePicker: false,
            minDate: nextday,
            startDate: StartDate,
            endDate: EndDate,
            separator: ' to ',
            locale: {
                cancelLabel: 'Close',
                format: 'DD-MM-YYYY',
                separator: ' - '
            },
            orientation: "top auto",
            autoclose: true
        });


    }
    $("#loader").hide();
    return true;
}

function checkHotelname(response, HotelName) {
    if (response != null && response != "" & HotelName != "") {
        var HotelNameFilters = getAppSettings();
        if (HotelNameFilters != null && HotelNameFilters != "") {
            var FilterArray = HotelNameFilters.split(',');
        }
        for (var i = 0; i < response.length; i++) {
            var DataHotelName = response[i].Name;
            if (HotelName == DataHotelName) {
                HotelName = response[i].SupplierAccommId;
            }

        }
        return HotelName;
    } else { return "--Hotel Name--"; }
}

function getCookie(cname) {

    var name = cname + "=";
    var ca = document.cookie.split(';');
    for (var i = 0; i < ca.length; i++) {
        var c = ca[i];
        while (c.charAt(0) == ' ') c = c.substring(1);
        if (c.indexOf(name) == 0) {
            return c.substring(name.length, c.length);
        }
    }
    return "";
}

function checkandSaveCookie(Searchdata) {
    if (Searchdata != "" && Searchdata != null) {
        var SearchCookie = getCookie("SearchResults");
        if (SearchCookie != "") {
            setCookie("SearchResults", '');
            setCookie("SearchResults", Searchdata);
            return

        } else {

            setCookie("SearchResults", Searchdata);

        }
    }
}
$(document).on("click", "#btnIncomingcall", function () {
    var searchQuote = $('#ttsQuoteinfo').val();
    var InFirstname = $('#Incustomername').val();
    var emailAddress = $('#IncustomerEmail').val();
    var phoneNumber = $('#IncustomerPhonenumber').val();
    var InLastname = $('#IncustomerLastName').val();
    var CustomerAddress = "Hyderabad";
    var IncomingCustomerName = InFirstname + " " + InLastname;
    $("#hdnTypeOfSearch").val("Sintech");
    if ($("#transfers_yes").prop("checked")) {
        transfer = true;
    }
    if ($("#transfers_no").prop("checked")) {
        transfer = false;
    }

    if (emailAddress == "") {
        $('.alert-wrapper').show();
        $('.alert-box h4').text("Please enter the Email Id!");
        return false;
    }
    else {
        $('#loader').show();
        var dataObject = { TTSSQuote: searchQuote, Transfer: transfer, IncomingCustomerName: IncomingCustomerName, EmailAddress: emailAddress }
        $.ajax({
            url: '../Home/Search',
            type: 'POST',
            data: JSON.stringify(dataObject),
            contentType: 'application/json; charset=utf-8',
            success: function (data) {
                if (data.Message != undefined && data.Message != '') {
                    $('#loader').hide();
                    $('.alert-wrapper').show();
                    $('.alert-box h4').text("Please check the quote reference.");
                }
                else {

                    window.location.href = data.RedirectUrl;
                    var customerObject = {
                        QuoteId: searchQuote,
                        FirstName: InFirstname,
                        LastName: InLastname,
                        PhoneNumber: phoneNumber,
                        EmailAddress: emailAddress,
                        Address: CustomerAddress
                    }
                    $.ajax({
                        url: '../Home/InsertIncomingCallData',
                        type: 'POST',
                        data: JSON.stringify(customerObject),
                        contentType: 'application/json; charset=utf-8',
                        success: function (data) {
                            if (data.Message != undefined && data.Message != '') {

                            }

                        },
                        error: function (xhr, status, error) {
                            ShowAjaxRequestError(xhr, status, error);
                        }

                    });
                }
            },
            error: function (xhr, status, error) {
                ShowAjaxRequestError(xhr, status, error);
            }
        });


    }
});


function displayVals() {
    var rooms = $("#txtRooms").val();
    var adults = $("#txtAdults").val();
    var children = $("#txtChildren").val();
    var isFlightSelected = $("#btnFlightonly").hasClass("purple");

    if (isFlightSelected == true) {
        $("#txtResult").val(adults + " Adults, " + children + " Children");
    }
    else {
        $("#txtResult").val(rooms + " Rooms, " + adults + " Adults, " + children + " Children");
    }
}
function btnFlightonly() {
    $("#txtSourceFrom, #txtDestinationCode, #dstnSource, #dstnSource .search-fm,#trip_checkbox_wrapper,#search_but_wrapper,.dashboard_bags").prop('disabled', false).css("display", "inline-block");
    $("#sel1, #hotelName,.search_bordtype,.inputrow #ddlHotel,.inputrow .search_avg_slider").prop('disabled', true).css("display", "none");
    $(".inputrow .search_avg_slider").prop('disabled', true).css("display", "hidden");
    $(".starrtng,#liRooms").css("display", "none");
    $("#txtResult").val("1 Adults,0 Children");
    $("#btnFlightonly").addClass("purple");
    $("#divHomeSearch").addClass("btnflightwrp").removeClass("btnholidayswrp").removeClass("btnhotelwrp");
    $("#btnHolidays, #btnHotelonly").removeClass("purple");
    $('#dynamicchildrow0').hide();
    $('#dynamicinfantrow0').hide();
    $('#dynamicdivrow1').remove();
    $('#dynamicdivrow2').remove();
    $('#dynamicdivrow3').remove();
    $('#dynamicdivrow4').remove();
    $('#displayfirst').html("1");
    $('#childadd0').html("1");
    $('#childtext0').html("0");
    $('#infanttext0').html("0");
    $('#child01').remove();
    $('#child02').remove();
    $('#child03').remove();
    $('#child04').remove();
    $('#infant01').remove();
    $('#infant02').remove();
    $('#infant03').remove();
    $('#infant04').remove();
    $("#peopleandrooms").hide();
    $("#roomallocations").hide();
    $("#howmanyrooms").hide();
    $("#roomsneed").hide();
    $('#txtAdults').val('1');
    $('#txtChildren').val('0');
    $('#txtBags').val('1');
    $("#optionsRadios2").prop("checked", true);
    $("#dvBags").prop('disabled', true).css("display", "block");
    $("#dvAvg").prop('disabled', true).css("display", "none");
}
function btnHotelonly() {
    $(".dropdown-toggle, #sel1,  #dstnSource,  #btnsRow .btn, #txtDestinationCode, #hotelName,.inputrow .search_avg_slider,.search_bordtype,#ddlHotel").prop('disabled', false).css("display", "inline-block");
    $("#txtSourceFrom, #dstnSource .search-fm,#trip_checkbox_wrapper,.dashboard_bags").prop('disabled', true).css("display", "none");
    $(".starrtng, #resultRow, #btnsRow").css("display", "inline-block");
    $("#txtResult").prop('disabled', true).css("display", "inline-block");
    $("#liRooms").css("display", "block");
    $("#btnHotelonly").addClass("purple");
    $("#btnFlightonly, #btnHolidays").removeClass("purple");
    $("#divHomeSearch").addClass("btnhotelwrp").removeClass("btnholidayswrp").removeClass("btnflightwrp");
    $("#ddlHotel").prop('disabled', true).css("display", "hidden");
    $('#hdnDestinationTo').val('');
    $('#childtext0').html("0");
    $('#childtext1').html("0");
    $('#infanttext0').html("0");
    $('#infanttext1').html("0");
    $('#infanttext2').html("0");
    $('#childtext2').html("0");
    $('#child01').remove();
    $('#child02').remove();
    $('#child03').remove();
    $('#child04').remove();
    $('#child11').remove();
    $('#child12').remove();
    $('#child13').remove();
    $('#child14').remove();
    $('#child21').remove();
    $('#child22').remove();
    $('#child23').remove();
    $('#child24').remove();
    $('#infant01').remove();
    $('#infant02').remove();
    $('#infant03').remove();
    $('#infant04').remove();
    $('#infant11').remove();
    $('#infant12').remove();
    $('#infant13').remove();
    $('#infant14').remove();
    $('#infant21').remove();
    $('#infant22').remove();
    $('#infant23').remove();
    $('#infant24').remove();
    $('#dynamicchildrow0').hide();
    $('#dynamicinfantrow0').hide();
    $('#displayfirst').html("1");

    $('#ddlHotel').empty();
    $("#ChildRoom0").show();
    $("#peopleandrooms").show();
    $("#roomallocations").show();
    $("#howmanyrooms").show();
    $("#roomsneed").show();
    $("#txtResult").val("1 Rooms,1 Adults,0 Children");
    var items = [];
    items.push("<option value='0'>--Hotel Name--</option>");
    $("#ddlHotel").html(items.join(' '));
    $('#txtRooms').val('1');
    $('#txtAdults').val('1');
    $('#txtChildren').val('0');
    $('#txtBags').val('1');
    $('#txtCheckIn').daterangepicker({
        singleDatePicker: false,
        minDate: nextday,
        startDate: startdate,
        endDate: enddate,
        separator: ' to ',
        locale: {
            cancelLabel: 'Close',
            format: 'DD-MM-YYYY',
            separator: ' - '
        },
        orientation: "top auto",
        autoclose: true
    });
    $("#dvBags").css("display", "none");
    $("#dvAvg").prop('disabled', true).css("display", "block");
    // $(".dashboard_bags").prop('disabled', true).css("display", "none");
}
function btnHolydays() {

    $("#txtResult, .dropdown-toggle, #sel1, #btnsRow .btn, #hotelName, #txtSourceFrom, #txtDestinationCode, #dstnSource, #dstnSource .search-fm,#ddlHotel").prop('disabled', false).css("display", "inline-block");
    $(".starrtng, #resultRow, #btnsRow,.search_bordtype,#transfers_wrapper,.inputrow .search_avg_slider").css("display", "inline-block");
    $("#trip_checkbox_wrapper").prop('disabled', true).css("display", "none");
    $("#txtResult").prop('disabled', true).css("display", "inline-block");
    $("#liRooms").css("display", "block");
    $("#btnHolidays").addClass("purple");
    $("#btnFlightonly, #btnHotelonly").removeClass("purple");
    $("#divHomeSearch").addClass("btnholidayswrp").removeClass("btnhotelwrp").removeClass("btnflightwrp");
    $("#ddlHotel").prop('disabled', true).css("display", "hidden");
    $('#hdnDestinationTo').val('');
    $('#ddlHotel').empty();
    $("#txtResult").val("1 Rooms,1 Adults,0 Children");
    var items = [];
    $('#childtext0').html("0");
    $('#childtext1').html("0");
    $('#infanttext0').html("0");
    $('#infanttext1').html("0");
    $('#dynamicchildrow0').hide();
    $('#dynamicinfantrow0').hide();
    $('#displayfirst').html("1");
    $('#childadd0').html("1");
    $("#dynamicdivrow2").remove();
    $("#dynamicdivrow1").remove();
    $('#dynamicdivrow3').remove();
    $('#dynamicdivrow4').remove();
    $('#child01').remove();
    $('#child02').remove();
    $('#child03').remove();
    $('#child04').remove();
    $('#child11').remove();
    $('#child12').remove();
    $('#child13').remove();
    $('#child14').remove();
    $('#child21').remove();
    $('#child22').remove();
    $('#child23').remove();
    $('#child24').remove();
    $('#infant01').remove();
    $('#infant02').remove();
    $('#infant03').remove();
    $('#infant04').remove();
    $('#infant11').remove();
    $('#infant12').remove();
    $('#infant13').remove();
    $('#infant14').remove();
    $('#infant21').remove();
    $('#infant22').remove();
    $('#infant23').remove();
    $('#infant24').remove();
    $("#ChildRoom0").show();
    $("#peopleandrooms").show();
    $("#roomallocations").show();
    $("#howmanyrooms").show();
    $("#roomsneed").show();
    items.push("<option value='0'>--Hotel Name--</option>");
    $("#ddlHotel").html(items.join(' '));
    $('#txtRooms').val('1');
    $('#txtAdults').val('1');
    $('#txtChildren').val('0');
    $('#txtBags').val('1');
    $('#txtCheckIn').daterangepicker({
        singleDatePicker: false,
        minDate: nextday,
        startDate: startdate,
        endDate: enddate,
        separator: ' to ',
        locale: {
            cancelLabel: 'Close',
            format: 'DD-MM-YYYY',
            separator: ' - '
        },
        orientation: "top auto",
        autoclose: true
    });
    $("#dvBags").prop('disabled', true).css("display", "block");
    $("#dvAvg").prop('disabled', true).css("display", "block");
    // $(".dashboard_bags").prop('disabled', true).css("display", "block");
}
function onSelectSearch(resultType) {
    var result = true;
    renderHtml = "<ul>";

    if ($('#txtSourceFrom').val().trim().length == 0 && resultType == "Package") {
        renderHtml = renderHtml + '<li class="validateli"><span class="icon icon-warning pull-left"></span><p class="pull-left summary_mesg">Source required.</p></li>';
        result = false;
    }

    if ($('#hdnSourceFrom').val().trim().length == 0 && resultType == "Package") {
        renderHtml = renderHtml + '<li class="validateli"><span class="icon icon-warning pull-left"></span><p class="pull-left summary_mesg">Please select valid source.</p></li>';
        result = false;
    }

    if ($("#txtDestinationCode").val().trim().length == 0 && resultType == "Package") {
        renderHtml = renderHtml + '<li class="validateli"><span class="icon icon-warning pull-left"></span><p class="pull-left summary_mesg">Destination required.</p></li>';
        result = false;
    }
    if ($('#hdnDestinationTo').val().trim().length == 0 && resultType == "Package") {
        renderHtml = renderHtml + '<li class="validateli"><span class="icon icon-warning pull-left"></span><p class="pull-left summary_mesg">Please select valid destination.</p></li>';
        result = false;
    }

    if ($("#txtCheckIn").val().trim().length == 0 && resultType == "Package") {
        renderHtml = renderHtml + '<li class="validateli"><span class="icon icon-warning pull-left"></span><p class="pull-left summary_mesg">Depature date required.</p></li>';
        result = false;
    }

    if ($("#sel1").val() == 0 && resultType == "Package") {
        renderHtml = renderHtml + '<li class="validateli"><span class="icon icon-warning pull-left"></span><p class="pull-left summary_mesg">Nights required.</p></li>';
        result = false;
    }

    if ($('#txtSourceFrom').val().trim().length != 0 && $('#txtSourceFrom').val() == $("#txtDestinationCode").val() && resultType == "Package") {
        renderHtml = renderHtml + '<li class="validateli"><span class="icon icon-warning pull-left"></span><p class="pull-left summary_mesg">Source and Destination should be different.</p></li>';
        result = false;
    }

    if ($('#txtSourceFrom').val().trim().length == 0 && resultType == "Flight") {
        renderHtml = renderHtml + '<li class="validateli"><span class="icon icon-warning pull-left"></span><p class="pull-left summary_mesg">Source required.</p></li>';
        result = false;
    }

    if ($('#hdnSourceFrom').val().trim().length == 0 && resultType == "Flight") {
        renderHtml = renderHtml + '<li class="validateli"><span class="icon icon-warning pull-left"></span><p class="pull-left summary_mesg">Please select valid source.</p></li>';
        result = false;
    }

    if ($("#txtDestinationCode").val().trim().length == 0 && resultType == "Flight") {
        renderHtml = renderHtml + '<li class="validateli"><span class="icon icon-warning pull-left"></span><p class="pull-left summary_mesg">Destination required.</p></li>';
        result = false;
    }
    if ($('#hdnDestinationTo').val().trim().length == 0 && resultType == "Flight") {
        renderHtml = renderHtml + '<li class="validateli"><span class="icon icon-warning pull-left"></span><p class="pull-left summary_mesg">Please select valid destination.</p></li>';
        result = false;
    }

    if ($('#txtSourceFrom').val().trim().length != 0 && $('#txtSourceFrom').val() == $("#txtDestinationCode").val() && resultType == "Flight") {
        renderHtml = renderHtml + '<li class="validateli"><span class="icon icon-warning pull-left"></span><p class="pull-left summary_mesg">Source and Destination should be different.</p></li>';
        result = false;
    }

    if ($('#txtDestinationCode').val().trim().length == 0 && resultType == "Hotel") {
        renderHtml = renderHtml + '<li class="validateli"><span class="icon icon-warning pull-left"></span><p class="pull-left summary_mesg">Destination required.</p></li>';
        result = false;
    }
    if ($('#hdnDestinationTo').val().trim().length == 0 && resultType == "Hotel") {
        renderHtml = renderHtml + '<li class="validateli"><span class="icon icon-warning pull-left"></span><p class="pull-left summary_mesg">Please select valid destination.</p></li>';
        result = false;
    }



    if (result == false) {
        renderHtml = renderHtml + '</ul>';
        $('.error_container').html(renderHtml);
        $('.errror_summery_wrapper1').show();
        onSearch();
    }

    return result;
}
function onSearch() {
    var result = false;
    if ($('#txtSourceFrom').val().trim().length == 0) {
        //$('#txtSourceFrom').addClass('shadow');
        // $("#txtSourceFrom").attr("placeholder", "Please select source").placeholder();

    } else if ($('#hdnDestinationTo').val().trim().length == 0) {
        //  $('#txtDestinationCode').addClass('shadow');
        //  $("#txtDestinationCode").attr("placeholder", "Please select destination").placeholder();
    }
    else if ($("#txtDestinationCode").val().trim().length == 0) {

        $('#txtDestinationCode').focus();
    } else if ($('#hdnDestinationTo').val().trim().length == 0) {

        $('#txtSourceFrom').focus();
    }
    else if ($("#txtCheckIn").val().trim().length == 0) {

        $('#txtCheckIn').focus();
    }
    else if ($("#sel1").val() == 0) {

        $('#sel1').focus();
    }
    else {
        result = true;
    }
    // return result;
}
function checkDate(d, m, y) {
    if ((y.length == 4) && (y % 4 != 0) && (m == 1 || m == 3 || m == 5 || m == 7 || m == 8 || m == 10 || m == 12) && d <= 31) {
        return true;
    } else if ((y.length == 4) && (y % 4 != 0) && (m == 4 || m == 6 || m == 9 || m == 11) && d <= 30) {
        return true;
    } else if ((y.length == 4) && (y % 4 != 0) && (m == 2) && d <= 28) {
        return true;
    } else if ((y.length == 4) && (y % 4 == 0) && (m == 2) && d <= 29) {
        return true;
    }
    else {
        return false;
    }
}


