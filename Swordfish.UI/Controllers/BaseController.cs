﻿using System;
using System.Web.Mvc;
using Swordfish.UI.Helpers;
using Swordfish.UI.Models.Account;

namespace Swordfish.UI.Controllers
{
    public class BaseController : AsyncController
    {
        protected override void OnException(ExceptionContext filterContext)
        {
            Exception ex = filterContext.Exception;
            ErrorLogModel error = new ErrorLogModel();
            error.Message = "Error occurred!!. " + ex.Message.ToString() + " at " + DateTime.Now.ToString();

            SFLogger.logError("", ex);
            filterContext.ExceptionHandled = true;
            var model = new HandleErrorInfo(ex, "Error", "ErrorCall");

            if (ex is SwordfishAjaxException || (ex is AggregateException && ex.InnerException  is SwordfishAjaxException))
            {
                string content = ex.InnerException != null ? ex.InnerException.Message : ex.Message;
                filterContext.Result = new HttpStatusCodeResult(500, content);
            }
            else
            {
                filterContext.Result = new ViewResult()
                {
                    ViewName = "Error",
                    ViewData = new ViewDataDictionary(model)
                };
            }
        }
        protected override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            if (SFConfiguration.GetUserID().ToString() == null)
            {
                RedirectToAction("Login", "Account");
            }
        }
    }
}
