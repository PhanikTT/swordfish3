﻿using Newtonsoft.Json;
using Swordfish.Businessobjects.Easyjet;
using Swordfish.UI.Helpers;
using Swordfish.UI.Models.Easyjet;
using System.Web.Mvc;

namespace Swordfish.UI.Controllers
{
    public class EasyJetController : BaseController
    {
        // GET: EasyJet
        [Authorize]
        public ActionResult Index(string quoteHeaderId, string ComponentTypeOptionId, string easyJetAbort)
        {
            SFLogger.logMessage("EasyJetController->:Index(): started!!.");
            ViewBag.GetURL = HttpContext.Request.Url.ToString().Replace("Index", "GetQuote");
            ViewBag.UpdateURL = HttpContext.Request.Url.ToString().Replace("Index", "Update");
            ViewBag.AbortURL = HttpContext.Request.Url.ToString().Replace("EasyJet", "BookingFulfillment");
            SFLogger.logMessage("EasyJetController->:Index(): Ended!!.");
            return View();
        }
        [Authorize]
        public ActionResult GetQuote(string quoteHeaderId, int ComponentTypeOptionId)
        {
            SFLogger.logMessage("EasyJetController->:GetQuote(): started!!.");
            var data = EasyJetHelper.GetEJQuoteData(quoteHeaderId, ComponentTypeOptionId);
            SFLogger.logMessage("EasyJetController->:GetQuote(): started!!.");
            return Json(data, JsonRequestBehavior.AllowGet);
        }
        [Authorize]
        public ActionResult Update(EasyJetResponse model)
        {
            SFLogger.logMessage("EasyJetController:Update(): started!!.", "Update", JsonConvert.SerializeObject(model));
            EasyJetHelper.SaveEasyJetBooking(model);
            //System.Diagnostics.Process.Start(HttpContext.Request.Url.ToString().Replace("EasyJet", "BookingFulfillment"));
            SFLogger.logMessage("EasyJetController:Update(): end!!.", "Update", "Done");
            return Json(true, JsonRequestBehavior.AllowGet);
        }
    }
}