﻿using System;
using System.Web.Mvc;
using SwordFish.Filters;
using Swordfish.Businessobjects.Common;
using WEXCreateCard = Swordfish.Utilities.WEXCard;
using Swordfish.UI.Helpers;
using Swordfish.UI.Models.Easyjet;
using Swordfish.UI.Models.Booking;
using Swordfish.UI.Helpers.BookingFulfilment;


namespace Swordfish.UI.Controllers
{
    public class BookingFulfillmentController : BaseController
    {

        BookingFulfilmentHelper bookingFulfilment = new BookingFulfilmentHelper();
        // GET: Bookingfulfillment
        public ActionResult Index(string quoteHeaderId, int? componentTypeOptionId, string easyJetAbort)
        {
            ViewBag.buttonText = "Book";
            BookingFulfillmentModel model = new BookingFulfillmentModel();
            model.LoadQuote(quoteHeaderId);
            model.PopulateBookingFulfilmentViewModel();
            return View(model);
        }
        [HttpPost]
        [MultipleButton(Name = "action", Argument = "Index")]
        public ActionResult Index(BookingFulfillmentModel bookingFulfillmentModel)
        {
            try
            {
                ViewBag.buttonText = "Book";
                ViewBag.agentNotes = SFConfiguration.GetAgentNotes();
                bookingFulfillmentModel.LoadQuote(bookingFulfillmentModel.QuoteHeaderId);

                IBookingFulfilment bookingFulfilment = BookingFulfilmentBase.CreateBookingFulfilment(bookingFulfillmentModel);
                bookingFulfilment.FulfilBooking();


                if (bookingFulfillmentModel.IsEasyjetWexcardCreated == true)
                    bookingFulfillmentModel = bookingFulfilment.BookingFullfillmentAccess(bookingFulfillmentModel, ControllerContext);
            }
            catch (Exception ex)
            {
                bookingFulfilment.FillNonEzJetModel(model, model.QuoteHeaderId);
                bookingFulfilment.BookingFailureEmails( model, ControllerContext, "BookingFullfillmentFailure");
            }
            return View(model);
        }
        [HttpPost]
        [MultipleButton(Name = "action", Argument = "Return")]
        public ActionResult Return(BookingFulfillmentModel bookingFulfillmentModel)
        {
            bookingFulfillmentModel.IsFromSubmitted = 1;
            var model = bookingFulfilment.GetBookingFulfillmentModel(bookingFulfillmentModel.QuoteHeaderId);
            model.QuoteHeaderId = bookingFulfillmentModel.QuoteHeaderId;
            model.ComponentTypeOptionId = bookingFulfillmentModel.ComponentTypeOptionId;
            if (model.BookingType.Equals(SFConstants.EasyJetBookingType) || model.BookingType.Equals(SFConstants.HybridBookingType))
            {
                EasyJetHelper.FillEasyJetResponses(model.EasyJetBookingStatus, bookingFulfillmentModel.QuoteHeaderId, bookingFulfillmentModel.ComponentTypeOptionId);
                EasyJetBasketModel easyJetBasketModel = EasyJetHelper.GetEJQuoteData(bookingFulfillmentModel.QuoteHeaderId, SFConstants.QuoteComponent_FlightReturn, true);
                decimal totalAmout = EasyJetHelper.GetTotalAmount(easyJetBasketModel);
                bool IsGBP = true;
                if (easyJetBasketModel != null && !string.IsNullOrEmpty(easyJetBasketModel.currency) && easyJetBasketModel.currency.Equals(SFConstants.EUROCurrencyCode))
                    IsGBP = false;
                //WEX Card Creation
                WEXCreateCard.CreateCardResponse WEXCard = null;
                try
                {

                    WEXCard = bookingFulfilment.CreateWEXCard(model.QuoteHeaderId, totalAmout < 1000 ? 1000 : totalAmout, IsGBP);
                }
                catch (Exception ex)
                {
                    model.ReturnButtonText = "Retry";
                    model.IsEasyjetWexcardCreated = false;
                    SFLogger.logError("Wex card creating failed in Easyjet returns with Quote headerid" + bookingFulfillmentModel.QuoteHeaderId, ex);
                }
                if (WEXCard != null && Convert.ToBoolean(WEXCard.success))
                {
                    return RedirectToAction("Index", "EasyJet", new { quoteHeaderId = model.QuoteHeaderId, ComponentTypeOptionId = SFConstants.QuoteComponent_FlightReturn });
                }
                else
                {
                    model.ReturnButtonText = "Retry";
                }
            }
            return View("Index", model);
        }
        [HttpPost]
        [MultipleButton(Name = "action", Argument = "OutBound")]
        public ActionResult OutBound(BookingFulfillmentModel bookingFulfillmentModel)
        {
            bookingFulfillmentModel.IsFromSubmitted = 1;
            var model = bookingFulfilment.GetBookingFulfillmentModel(bookingFulfillmentModel.quoteHeaderId);
            model.quoteHeaderId = bookingFulfillmentModel.quoteHeaderId;
            model.ComponentTypeOptionId = bookingFulfillmentModel.ComponentTypeOptionId;
            if (model.BookingType.Equals(SFConstants.EasyJetBookingType) || model.BookingType.Equals(SFConstants.HybridBookingType))
            {
                EasyJetHelper.FillEasyJetResponses(model.EasyJetBookingStatus, bookingFulfillmentModel.quoteHeaderId, bookingFulfillmentModel.ComponentTypeOptionId);
                EasyJetBasketModel easyJetBasketModel = EasyJetHelper.GetEJQuoteData(bookingFulfillmentModel.quoteHeaderId, SFConstants.Direction_Outbound, true);
                decimal totalAmout = EasyJetHelper.GetTotalAmount(easyJetBasketModel);
                bool IsGBP = true;
                if (easyJetBasketModel != null && !string.IsNullOrEmpty(easyJetBasketModel.currency) && easyJetBasketModel.currency.Equals(SFConstants.EUROCurrencyCode))
                    IsGBP = false;
                //WEX Card Creation
                WEXCreateCard.CreateCardResponse WEXCard = null;
                try
                {
                    WEXCard = bookingFulfilment.CreateWEXCard(model.quoteHeaderId, totalAmout < 1000 ? 1000 : totalAmout, IsGBP);
                }
                catch (Exception ex)
                {
                    model.OutBoundButtonText = "Retry";
                    model.IsEasyjetWexcardCreated = false;
                    SFLogger.logError("Wex card creating failed in Easyjet OutBound with Quote headerid" + bookingFulfillmentModel.quoteHeaderId, ex);
                }
                if (WEXCard != null && Convert.ToBoolean(WEXCard.success))
                {
                    return RedirectToAction("Index", "EasyJet", new { quoteHeaderId = model.quoteHeaderId, ComponentTypeOptionId = SFConstants.Direction_Outbound });
                }
                else
                {
                    model.OutBoundButtonText = "Retry";
                }
            }
            return View("Index", model);
        }
        [HttpPost]
        [MultipleButton(Name = "action", Argument = "InBound")]
        public ActionResult InBound(BookingFulfillmentModel bookingFulfillmentModel)
        {
            bookingFulfillmentModel.IsFromSubmitted = 1;
            var model = bookingFulfilment.GetBookingFulfillmentModel(bookingFulfillmentModel.quoteHeaderId);
            model.quoteHeaderId = bookingFulfillmentModel.quoteHeaderId;
            model.ComponentTypeOptionId = bookingFulfillmentModel.ComponentTypeOptionId;
            if (model.BookingType.Equals(SFConstants.EasyJetBookingType) || model.BookingType.Equals(SFConstants.HybridBookingType))
            {
                EasyJetHelper.FillEasyJetResponses(model.EasyJetBookingStatus, bookingFulfillmentModel.quoteHeaderId, bookingFulfillmentModel.ComponentTypeOptionId);
                EasyJetBasketModel easyJetBasketModel = EasyJetHelper.GetEJQuoteData(bookingFulfillmentModel.quoteHeaderId, SFConstants.Direction_Inbound, true);
                decimal totalAmout = EasyJetHelper.GetTotalAmount(easyJetBasketModel);
                bool IsGBP = true;
                if (easyJetBasketModel != null && !string.IsNullOrEmpty(easyJetBasketModel.currency) && easyJetBasketModel.currency.Equals(SFConstants.EUROCurrencyCode))
                    IsGBP = false;
                //WEX Card Creation
                WEXCreateCard.CreateCardResponse WEXCard = null;
                try
                {
                    WEXCard = bookingFulfilment.CreateWEXCard(model.quoteHeaderId, totalAmout < 1000 ? 1000 : totalAmout, IsGBP);
                }
                catch (Exception ex)
                {
                    model.InBoundButtonText = "Retry";
                    model.IsEasyjetWexcardCreated = false;
                    SFLogger.logError("Wex card creating failed in Easyjet InBound with Quote headerid" + bookingFulfillmentModel.quoteHeaderId, ex);
                }
                if (WEXCard != null && Convert.ToBoolean(WEXCard.success))
                {
                    return RedirectToAction("Index", "EasyJet", new { quoteHeaderId = model.quoteHeaderId, ComponentTypeOptionId = SFConstants.Direction_Inbound });
                }
                else
                {
                    model.InBoundButtonText = "Retry";
                }
            }
            return View("Index", model);
        }

        [HttpPost]
        public ActionResult PostToTopDog(string agentNotes, string quoteHeaderID)
        {
            string toMailList = string.Empty;
            bookingFulfilment.UpdateAgentNotes(quoteHeaderID, agentNotes);
            BookingFulfillmentModel model= bookingFulfilment.PostToTopDog(quoteHeaderID, ControllerContext);
            ViewBag.agentNotes = agentNotes;
            return PartialView("_TopDogResponse", model);
        }

    }
}