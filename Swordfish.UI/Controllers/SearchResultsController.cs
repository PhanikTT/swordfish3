﻿using System.Collections.Generic;
using System.Threading.Tasks;
using System.Web.Mvc;
using System;
using Swordfish.UI.Helpers;
using Swordfish.Businessobjects.Hotels;
using Swordfish.Businessobjects.Customers;
using Swordfish.Businessobjects.Bookings;
using Swordfish.Businessobjects.Common;
using Swordfish.Components.BookingServices;
using Swordfish.UI.Models.Transfers;
using Swordfish.UI.Models.Account;
using Swordfish.UI.Models.Hotel;
using Swordfish.UI.Models.Flight;
using Swordfish.UI.Models.Basket;
using Swordfish.UI.Models.Booking;
using Swordfish.Businessobjects.Notification;
using System.Web.Script.Serialization;
using Swordfish.Businessobjects.Sesame;

namespace Swordfish.UI.Controllers
{
    public class SearchResultsController : BaseController
    {
        private readonly TravelSAASRestClient travelSAASRestClient;
        public SearchResultsController()
        {
            travelSAASRestClient = new TravelSAASRestClient();
        }
        [Authorize]
        // GET: Search
        public ActionResult Index()
        {
            SFLogger.logMessage("SearchResultsController:Index(): started!!.");
            var searchModel = SFConfiguration.GetDashBSearch();
            ViewBag.UserInfo = SFConfiguration.GetUserInfo();
            ViewBag.FidelityCharges = SFConfiguration.GetFidelityCharges();
            ViewBag.IsAsynchronous = SFConfiguration.GetAsynchronousFlag();
            ViewBag.AjaxCallTime = SFConfiguration.GetAjaxCallTime();
            SFLogger.logMessage("SearchResultsController:Index(): end.");
            return View(searchModel);
        }
        [HttpPost]
        [Authorize]
        public ActionResult GetResort(SearchModel searchModel)
        {
            SFLogger.logMessage("SearchResultsController:GetResort(): started!!.");
            if (searchModel == null)
                throw new ArgumentNullException("SearchModel is null : GetResort");
            if (searchModel.OccupancyRooms == null)
                throw new ArgumentNullException("OccupancyRooms is null in SearchModel : GetResort");
            SFLogger.logMessage("SearchResultsController:GetResort(): end.");
            return Json(SearchResultsHelper.GetResort(searchModel), JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        [Authorize]
        public ActionResult GetMultiRoom(SearchModel searchModel)
        {
            SFLogger.logMessage("SearchResultsController:GetMultiRoom(): started!!.");
            if (searchModel == null)
                throw new ArgumentNullException("SearchModel is null : GetMultiRoom");
            if (searchModel.OccupancyRooms == null)
                throw new ArgumentNullException("OccupancyRooms is null in SearchModel : GetMultiRoom");
            SFLogger.logMessage("SearchResultsController:SearchResultsController:GetMultiRoom(): end.");
            return Json(SearchResultsHelper.GetMultiRoom(searchModel), JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        [Authorize]
        public ActionResult GetAsyncResort(SearchModel searchModel)
        {
            SFLogger.logMessage("SearchResultsController:GetAsyncResort(): started!!.");
            if (searchModel == null)
                throw new ArgumentNullException("SearchModel is null : GetAsyncResort");
            if (searchModel.OccupancyRooms == null)
                throw new ArgumentNullException("OccupancyRooms is null in SearchModel : GetAsyncResort");
            SFLogger.logMessage("SearchResultsController:GetAsyncResort(): end.");
            return Json(SearchResultsHelper.GetAsyncResort(searchModel), JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        [Authorize]
        public ActionResult GetAsyncMultiRoom(SearchModel searchModel)
        {
            SFLogger.logMessage("SearchResultsController:GetAsyncMultiRoom(): started!!.");
            if (searchModel == null)
                throw new ArgumentNullException("SearchModel is null : GetAsyncMultiRoom");
            if (searchModel.OccupancyRooms == null)
                throw new ArgumentNullException("OccupancyRooms is null in SearchModel : GetAsyncMultiRoom");
            SFLogger.logMessage("SearchResultsController:GetAsyncMultiRoom(): end.");
            return Json(SearchResultsHelper.GetAsyncMultiRoom(searchModel), JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        [Authorize]
        public ActionResult GetSearchStatus(AccoSearchStatusRequest accoSearchStatusModel)
        {
            SFLogger.logMessage("SearchResultsController:GetSearchStatus(): started!!.");
            if (accoSearchStatusModel == null)
                throw new ArgumentNullException("accoSearchStatusModel is null : GetSearchStatus");
            SFLogger.logMessage("SearchResultsController:GetSearchStatus(): end!!.");
            return Json(SearchResultsHelper.GetSearchStatus(accoSearchStatusModel), JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        [Authorize]
        public ActionResult GetDeltaResults(AccoDeltaResultsRequest deltaResultsModel)
        {
            SFLogger.logMessage("SearchResultsController:GetDeltaResults(): started!!.");
            if (deltaResultsModel == null)
                throw new ArgumentNullException("deltaResultsModel is null : GetDeltaResults");
            SFLogger.logMessage("SearchResultsController:GetDeltaResults(): end.");
            var serializer = new JavaScriptSerializer();
            serializer.MaxJsonLength = int.MaxValue;
            var result = new ContentResult
            {
                Content = serializer.Serialize(SearchResultsHelper.GetDeltaResults(deltaResultsModel)),
                ContentType = "application/json"
            };
            return result;
        }
        [HttpPost]
        [Authorize]
        public ActionResult GetHotelInfo(HotelInfoRequest hotelInfoRequest)
        {
            SFLogger.logMessage("SearchResultsController:GetHotelInfo(): started!!.");
            HotelInformatoin hotelInformation = new HotelInformatoin();
            if (hotelInformation != null)
                hotelInformation = SearchResultsHelper.GetHotelInfo(hotelInfoRequest);
            SFLogger.logMessage("SearchResultsController:GetHotelInfo(): end.");
            return Json(hotelInformation, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult GetDatafromTaxiTransfer(TransferSearchRequest transferRequest)
        {
            SFLogger.logMessage("SearchResultsController:GetDatafromTaxiTransfer(): started!!.");
            List<Rootobject> transferData = new List<Rootobject>();
            if (transferRequest != null && transferRequest.ToCode != null)
            {
                if (transferRequest != null)
                    transferData = SearchResultsHelper.GetTransfersDataFromService(transferRequest);
            }
            SFLogger.logMessage("SearchResultsController:GetDatafromTaxiTransfer(): end.");
            return Json(transferData, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public JsonResult GetOneWayFlights(SearchModel searchModel)
        {
            SFLogger.logMessage("SearchResultsController:GetOneWayFlights(): started!!.");
            var flightData = new FlightDetails();
            if (searchModel != null)
                flightData = SearchResultsHelper.GetOneWayFlights(searchModel);
            var jsonResult = Json(flightData, JsonRequestBehavior.AllowGet);
            jsonResult.MaxJsonLength = int.MaxValue;
            SFLogger.logMessage("SearchResultsController:GetOneWayFlights(): end.");
            return jsonResult;
        }

        [HttpPost]
        public JsonResult GetReturnFlights(SearchModel searchModel)
        {
            SFLogger.logMessage("SearchResultsController:GetReturnFlights(): started!!.");
            var flightData = new FlightDetails();
            if (searchModel != null)
                flightData = SearchResultsHelper.GetReturnFlights(searchModel);
            var jsonResult = Json(flightData, JsonRequestBehavior.AllowGet);
            jsonResult.MaxJsonLength = int.MaxValue;
            SFLogger.logMessage("SearchResultsController:GetReturnFlights(): end.");
            return jsonResult;
        }
        [HttpPost]
        public JsonResult GetPriceForFlightQuoteId(SesameFlightItinaryPriceRequest flightItinaryPriceRequest, string sourceUrl)
        {
            SFLogger.logMessage("SearchResultsController:GetPriceForFlightQuoteId(): started!!.");
            var basketDetails = SearchResultsHelper.GetFlightItinaryPrice(flightItinaryPriceRequest, sourceUrl);
            SFLogger.logMessage("SearchResultsController:GetPriceForFlightQuoteId(): end.");
            return Json(basketDetails, JsonRequestBehavior.AllowGet);
        }
        public ActionResult CustomerGrid()
        {
            return PartialView();
        }
        public JsonResult GetCustomerDetailsWithQuotes(Customer customer)
        {
            SFLogger.logMessage("SearchResultsController:GetCustomerDetailsWithQuotes(): started!!.");
            var searchResultsHelper = new SearchResultsHelper();
            IEnumerable<CustomerQuotes> customerQuoteDetails = null;
            //var customerDetails = searchResultsHelper.SearchCustomer(customer);
            List<Customer> customerDetails = searchResultsHelper.SearchCustomer(customer).Result;
            if (customerDetails != null && customerDetails.Count > 0)
                customerQuoteDetails = searchResultsHelper.GetCustomerQuotes(customerDetails[0].CustomerId);
            SFLogger.logMessage("SearchResultsController:GetCustomerDetailsWithQuotes(): end.");
            return Json(new { customerDetails = customerDetails, customerQuoteDetails = customerQuoteDetails }, JsonRequestBehavior.AllowGet);
        }
        public async Task<JsonResult> GetCustomersOnly(Customer SearchCustomer)
        {
            SFLogger.logMessage("SearchResultsController:GetCustomersOnly(): started!!.");
            var searchsResultsHelper = new SearchResultsHelper();
            var customerDetails = await System.Threading.Tasks.Task.Run(() => searchsResultsHelper.SearchCustomer(SearchCustomer)).ConfigureAwait(false);
            SFLogger.logMessage("SearchResultsController:GetCustomersOnly(): end.");
            return Json(customerDetails, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetCustomerbyMembershipNo(Customer searchCustomer)
        {
            SFLogger.logMessage("SearchResultsController:GetCustomerbyMembershipNo(): started!!.");
            var searchsResultsHelper = new SearchResultsHelper();
            var customerDetails = searchsResultsHelper.GetCustomerbyMembershipNo(searchCustomer);
            SFLogger.logMessage("SearchResultsController:GetCustomerbyMembershipNo(): end.");
            return Json(customerDetails, JsonRequestBehavior.AllowGet);
        }

        public JsonResult CheckEmailExist(string emaiId, string memberShipNo)
        {
            SFLogger.logMessage("SearchResultsController:CheckEmailExist(): started!!.");

            SearchResultsHelper searchResultsHelper = new SearchResultsHelper();
            string result = searchResultsHelper.CheckEmailExist(emaiId, memberShipNo).Result;
            SFLogger.logMessage("SearchResultsController:CheckEmailExist(): end.");
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        public JsonResult CheckPhoneNumberExist(string phoneNumber, string memberShipNo)
        {
            SFLogger.logMessage("SearchResultsController:CheckEmailExist(): started!!.");

            SearchResultsHelper searchResultsHelper = new SearchResultsHelper();
            string result = searchResultsHelper.CheckMobileNoExist(phoneNumber, memberShipNo).Result;
            SFLogger.logMessage("SearchResultsController:CheckEmailExist(): end.");
            return Json(result, JsonRequestBehavior.AllowGet);
        }
        public ActionResult DisplayCustomerGrid()
        {
            return View();
        }

        public JsonResult SendSmsAndMail(string Mobile, string Email, string QuoteRef, string IsSms, string IsEmail)
        {
            SFLogger.logMessage("SearchResultsController:SendSmsAndMail(): started!!. QuoteReference:" + QuoteRef + ", Email:" + Email + ", Mobile:" + Mobile);
            var notificationHelper = new NotificationHelper();
            if (IsSms == "False")
            {
                Mobile = string.Empty;
            }
            else if (IsEmail == "False")
            {
                Email = string.Empty;
            }
            var result = notificationHelper.SendSmsAndEmail(Mobile, Email, ControllerContext, QuoteRef, Constants.SmsTypes.QuickQuoteSMS.ToString());
            SFLogger.logMessage("SearchResultsController:SendSmsAndMail(): end. QuoteReference:" + QuoteRef + ", Email:" + Email + ", Mobile:" + Mobile);

            return Json(result, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult SaveAgentJournal(string QuoteRef, string JournalType, string JournalNotes)
        {
            SFLogger.logMessage("SearchResultsController:SaveAgentJournal(): started!!.");
            int agentId = 0;
            if (SFConfiguration.GetUserID() > 0)
            {
                agentId = SFConfiguration.GetUserID();
            }
            SearchResultsHelper searchResultsHelper = new SearchResultsHelper();
            int result = searchResultsHelper.SaveAgentJournal(QuoteRef, JournalType, JournalNotes).Result;
            SFLogger.logMessage("SearchResultsController:SaveAgentJournal(): end.");
            return Json(result, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public JsonResult GetBaggageSlabs(BookCreateRequest bookCreateRequest)
        {
            SFLogger.logMessage("SearchResultsController:GetBaggageSlabs(): started!!.");
            var searchResultsHelper = new SearchResultsHelper();
            var result = searchResultsHelper.GetBaggageSlabs(bookCreateRequest);
            SFLogger.logMessage("SearchResultsController:GetBaggageSlabs(): end.");
            return Json(((RestSharp.RestResponseBase)result).Content, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public JsonResult SaveQuoteData(BasketStructure basketData)
        {
            SFLogger.logMessage("SearchResultsController:SaveQuoteData(): started!!.");
            var errorMsg = new List<string>();
            var searchResultsHelper = new SearchResultsHelper();
            string status = string.Empty;
            Dictionary<string, string> result = null;
            if (searchResultsHelper.ValidateQuoteInput(basketData, errorMsg))
            {
                result = searchResultsHelper.SaveQuote(basketData, errorMsg);
                if (result != null && result.Count > 0)
                {
                    BookingService serviceRepository = new BookingService();
                    BookingHelper bookingHelper = new BookingHelper(serviceRepository);
                    if (basketData.flightsReturn != null && !string.IsNullOrEmpty(basketData.flightsReturn.FlightQuoteId))
                    {
                        basketData.flightsReturn.sesameBasketDetails.AgentId = SFConfiguration.GetUserID();
                        basketData.flightsReturn.sesameBasketDetails.QuoteHeaderId = Convert.ToInt32(result["quoteHeaderId"]);
                        basketData.flightsReturn.sesameBasketDetails.BasketComponents[0].Type = SFConstants.SesameBasket_Create;
                        basketData.flightsReturn.sesameBasketDetails.BasketComponents[0].DirectionOptionId = SFConstants.QuoteComponent_FlightReturn;
                        bookingHelper.SaveSesameQuoteForCreateRefreshBook(basketData.flightsReturn.sesameBasketDetails);
                    }
                    else if ((basketData.flightsOneWayOutBound != null && !string.IsNullOrEmpty(basketData.flightsOneWayOutBound.FlightQuoteId))
                        && (basketData.flightsOneWayInBound != null && !string.IsNullOrEmpty(basketData.flightsOneWayInBound.FlightQuoteId)))
                    {
                        basketData.flightsOneWayOutBound.sesameBasketDetails.AgentId = SFConfiguration.GetUserID();
                        basketData.flightsOneWayOutBound.sesameBasketDetails.QuoteHeaderId = Convert.ToInt32(result["quoteHeaderId"]);
                        basketData.flightsOneWayOutBound.sesameBasketDetails.BasketComponents[0].Type = SFConstants.SesameBasket_Create;
                        basketData.flightsOneWayOutBound.sesameBasketDetails.BasketComponents[0].DirectionOptionId = SFConstants.Direction_Outbound;
                        bookingHelper.SaveSesameQuoteForCreateRefreshBook(basketData.flightsOneWayOutBound.sesameBasketDetails);

                        basketData.flightsOneWayInBound.sesameBasketDetails.AgentId = SFConfiguration.GetUserID();
                        basketData.flightsOneWayInBound.sesameBasketDetails.QuoteHeaderId = Convert.ToInt32(result["quoteHeaderId"]);
                        basketData.flightsOneWayInBound.sesameBasketDetails.BasketComponents[0].Type = SFConstants.SesameBasket_Create;
                        basketData.flightsOneWayInBound.sesameBasketDetails.BasketComponents[0].DirectionOptionId = SFConstants.Direction_Inbound;
                        bookingHelper.SaveSesameQuoteForCreateRefreshBook(basketData.flightsOneWayInBound.sesameBasketDetails);
                    }
                }

            }
            if (result != null && result.Count > 0)
            {
                status = ErrorMessages.success;
            }
            else
            {
                status = ErrorMessages.failed;
            }
            SFLogger.logMessage("SearchResultsController:SaveQuoteData(): end.");
            return Json(new { status = status, response = result, errorMsg = errorMsg }, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public ActionResult Booking(string QuoteHeaderId)
        {
            SFLogger.logMessage("SearchResultsController:Booking(): started!!. QuoteHeaderId: " + QuoteHeaderId);
            return RedirectToAction("Index", "Booking", new { QuoteHeaderId = QuoteHeaderId });
        }

        [HttpPost]
        public bool SavePassengers(List<PassengersModel> passengersDetails)
        {           
            var searchResultsHealper = new SearchResultsHelper();
            var commonHelper = new CommonHelper();
            var isSuccess = false;
            var result = searchResultsHealper.SavePassengers(passengersDetails);
            if (result == true && !string.IsNullOrEmpty(passengersDetails[0].QouteHeaderID)) // For success
            {
                var quote = commonHelper.LoadQuoteData(passengersDetails[0].QouteHeaderID);
                var value = Task.Run(() => searchResultsHealper.SaveCoBrowseDetails(Convert.ToInt32(passengersDetails[0].QouteHeaderID), quote.quoteHeader.CustomerReferenceCode, passengersDetails[0].Email, passengersDetails[0].FirstName + " " + passengersDetails[0].LastName, Request.Url.ToString(), ControllerContext)).ConfigureAwait(false);
                isSuccess = value.GetAwaiter().GetResult();
            }
            return isSuccess;
        }
    }
}