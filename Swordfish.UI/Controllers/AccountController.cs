﻿using System;
using System.Web.Mvc;
using System.Threading.Tasks;
using System.Web.Security;
using Swordfish.UI.Helpers;
using Swordfish.Components.MembershipServices;
using Swordfish.Businessobjects.Membership;
using Swordfish.UI.Models.Account;

namespace Swordfish.UI.Controllers
{
    [HandleError]
    public class AccountController : BaseController
    {
        // GET:  Account
        [AllowAnonymous]
        public ActionResult Login(string returnUrl = "")
        {
            ViewBag.ReturnUrl = returnUrl;
            return View();
        }

        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public ActionResult Login(LoginModel login, string returnUrl)
        {
            SFLogger.logMessage("AccountController:Login(): started!!. UserName:" + login.Username);
            TryUpdateModel<LoginModel>(login);
            if (ModelState.IsValid)
            {  
                MembershipService serviceRepository = new MembershipService();
                MembershipHelper memberHelper = new MembershipHelper(serviceRepository);
                var checkUser = memberHelper.ValidateUser(login);

                if (checkUser == 0)
                    login.ErrorMessage = "Error Occured: Please try again.";
                else if (checkUser == 1)
                    login.ErrorMessage = "Username does not exists.";
                else if (checkUser == 2)
                    login.ErrorMessage = "The email and password you entered does not match.";
                else if (checkUser == 3)
                    login.ErrorMessage = "Your account is inactive.  Please contact your administrator.";
                else if (checkUser == 4)
                    login.ErrorMessage = "Your account is lockedout due to failure password attempts.  Please contact your administrator.";
                else if (checkUser == 5)
                {
                    SFLogger.logMessage("AccountController:Login(): Login Success. UserName:" + login.Username.ToString());
                    var userInfo = memberHelper.SetupFormsAuthTicket(login.Username, login.RememberMe);
                    userInfo.Environment = SFConfiguration.GetEnvironment();
                    var UserName = userInfo.FirstName + " " + userInfo.LastName;
                    SFConfiguration.SetUserInfo(userInfo);
                    SFConfiguration.SetUserName(UserName);
                    SFConfiguration.SetUserID(userInfo.UserId);
                    SFConfiguration.SetUserEmail(login.Username);
                    var status = memberHelper.UpdateAgentLoginDate(userInfo.UserId);                                      
                    if (Url.IsLocalUrl(returnUrl) && returnUrl.Length > 1 && returnUrl.StartsWith("/")
                            && !returnUrl.StartsWith("//") && !returnUrl.StartsWith("/\\"))
                    {
                        SFLogger.logMessage("Redirect to " + returnUrl + ", UserName:" + login.Username.ToString());
                        return Redirect(returnUrl);
                    }
                    SFLogger.logMessage("AccountController:Login(): end. UserName:" + login.Username.ToString());
                    return RedirectToAction("DashBoard", "Home");
                }
            }
            SFLogger.logMessage("AccountController:Login(): end. " + login.ErrorMessage.ToString());
            return View(login);
        }        

        [AllowAnonymous]
        public ActionResult RecoverPassword()
        {
            return View();
        }

        // GET: /Account/LogOff
    
        public ActionResult LogOff()
        {
            SFLogger.logMessage("AccountController:LogOff(): started!!.");           
            FormsAuthentication.SignOut();
            Session.Abandon();
            SFConfiguration.RemoveCacheFromRedis();
            SFLogger.logMessage("AccountController:LogOff(): end.");
            return RedirectToAction("Login", "Account");
            // return Redirect(FormsAuthentication.GetRedirectUrl(User.Identity.Name, true));
        }

        [HttpPost]
        [AllowAnonymous]
        public ActionResult RecoverPassword(ForgotPasswordViewModel forgotModel)
        {
            SFLogger.logMessage("AccountController:RecoverPassword(): started!! Sending forgot password request!!. UserName:" + forgotModel.Email);          
            var resultData = string.Empty;
            try
            {
                if (ModelState.IsValid)
                {
                    MembershipService serviceRepository = new MembershipService();
                    MembershipHelper memberHelper = new MembershipHelper(serviceRepository);
                    var checkUser = memberHelper.CheckUserExists(serviceRepository, forgotModel.Email);

                    if (checkUser.Result == 0)
                    {
                        resultData = "Unknown Error: Please try again.";
                    }
                    else if (checkUser.Result == 1)
                    {
                        string token = memberHelper.GeneratePasswordToken();
                        var isEnable = memberHelper.EnableResetPassword(serviceRepository, forgotModel.Email, true);
                        string callbackUrl = HttpContext.Request.Url.Scheme + "://" + HttpContext.Request.Url.Authority + Url.Action("ResetPassword", "Account", new { user = forgotModel.Email, code = token });                       
                      
                        NotificationHelper notificationHelper = new NotificationHelper();
                        bool status = notificationHelper.SendEMailForPasswordRecovery(callbackUrl, forgotModel.Email);
                        forgotModel.ErrorMessage = "Please check your mail to reset password.";
                        forgotModel.ErrorType = "Success";
                        SFLogger.logMessage("AccountController:RecoverPassword(): Please check your mail to reset password.UserName:" + forgotModel.Email);
                    }
                    else
                    {
                        forgotModel.ErrorMessage = "Email does not exists.";
                        forgotModel.ErrorType = "Alert";
                        SFLogger.logMessage("AccountController:RecoverPassword(): Email does not exists. UserName:" + forgotModel.Email);
                    }
                }
            }
            catch (Exception ex)
            {
                forgotModel.ErrorMessage = ex.Message.ToString();
                forgotModel.ErrorType = "Error";
                SFLogger.logError("AccountController:RecoverPassword(): End. UserName:" + forgotModel.Email,ex);
            }
            SFLogger.logMessage("AccountController:RecoverPassword(): End. UserName:" + forgotModel.Email);           
            return View(forgotModel);
        }        

        [AllowAnonymous]
        public async Task<ActionResult> ResetPassword(string user, string code)
        {
            SFLogger.logMessage("AccountController:ResetPassword(): started!!. UserName:" + user);
            ResetPasswordViewModel recoveryModel = new ResetPasswordViewModel();
            if (!string.IsNullOrEmpty(user) && !string.IsNullOrEmpty(code))
            {
                MembershipService serviceRepository = new MembershipService();
                MembershipHelper memberHelper = new MembershipHelper(serviceRepository);
                bool isValidToken = memberHelper.DecriptPasswordToken(code);
                if (isValidToken)
                {
                    recoveryModel.Email = user;

                    //checking whether user already reset his password with this same link                  
                    var isUsed = await memberHelper.CheckResetPasswordEnable(serviceRepository, user);

                    if(!isUsed)
                    {
                        recoveryModel.ErrorMessage = "Token Expired";
                        SFLogger.logError("AccountController:ResetPassword(): End. Token Expired!!, UserName:" + user, null);
                        return View("Error");
                    }
                }
                else
                {
                    recoveryModel.ErrorMessage = "Token Expired.";
                    SFLogger.logError("AccountController:ResetPassword(): End. Token Expired!!, UserName:" + user, null);
                    return View("Error");
                }
            }
            else
            {
                recoveryModel.ErrorMessage = "Error Occured.  Please try again.";
                SFLogger.logError("AccountController:ResetPassword() End. Error Occured.  Please try again.!!, UserName:" + user, null);
                return View("Error");
            }
            SFLogger.logMessage("AccountController:ResetPassword() End. at " + DateTime.Now.ToString() + ", UserName:" + user);
            return View(recoveryModel);
        }

        //
        // POST: /Account/ResetPassword
        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> ResetPassword(ResetPasswordViewModel model)
        {
            SFLogger.logMessage("AccountController:ResetPassword(): started!!., UserName:" + model.Email);
            if (ModelState.IsValid)
            {
                try
                {
                    MembershipService serviceRepository = new MembershipService();
                    MembershipHelper memberHelper = new MembershipHelper(serviceRepository);
                    var checkStatus = await memberHelper.ResetUserPassword(serviceRepository, model);
                    model.Email = string.Empty;

                    if (checkStatus)
                    {
                        model.ErrorMessage = "Your password has been set.";
                        model.ErrorType = "Success";
                        SFLogger.logMessage("AccountController:ResetPassword(): Your password has been set. UserName:" + model.Email);
                    }
                    else
                    {
                        model.ErrorMessage = "Error Occured.  Please try again.";
                        model.ErrorType = "Error";
                        SFLogger.logError("AccountController:ResetPassword():Error Occured.  Please try again. UserName:" + model.Email,null);
                    }
                }
                catch (Exception ex)
                {
                    model.ErrorMessage = ex.Message.ToString();                 
                    model.ErrorType = "Error";
                    SFLogger.logError("AccountController:ResetPassword(): End. UserName:" + model.Email, null);
                }
            }
            SFLogger.logMessage("AccountController:ResetPassword(): end. UserName:" + model.Email);
            return View(model);
        }

        public ActionResult Role()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Role(RoleModel roleModel)
        {
            SFLogger.logMessage("AccountController:Role(): started!!. UserName:" + roleModel.CreatedBy);
            MembershipService serviceRepository = new MembershipService();
            MembershipHelper memberHelper = new MembershipHelper(serviceRepository);
            var checkUser = memberHelper.InsertNewRole(roleModel);
            SFLogger.logMessage("AccountController:Role(): end. UserName:" + roleModel.CreatedBy);
            return View();
        }

        //[SFAuthorize(Roles = "Admin")]
        //[Authorize]
        public ActionResult RolesManagement()
        {
            SFLogger.logMessage("AccountController:RolesManagement(): started!!.");
            MembershipService serviceRepository = new MembershipService();
            MembershipHelper memberHelper = new MembershipHelper(serviceRepository);
            var lstRoles = memberHelper.GetAllRoles();
            SFLogger.logMessage("AccountController:RolesManagement(): end.");
            return View(lstRoles);
        }

        [HttpGet]
        public ActionResult RoleOperations(RoleModel role)
        {
            return PartialView(role);
        }

        [HttpPost]
        public ActionResult RoleManagement(RoleModel role)
        {
            return View();
        }

        [HttpPost]
        public JsonResult GetBarChartDetails(Actiontext todaysaction)
        {
            SFLogger.logMessage("AccountController:GetBarChartDetails(): started!!.");
            MembershipService serviceRepository = new MembershipService();
            MembershipHelper memberHelper = new MembershipHelper(serviceRepository);
            string actualdate = todaysaction.TaskText;
            int UserAdminId = SFConfiguration.GetUserID();
            var listBarChart = memberHelper.GetLast7DaysGraph(actualdate, UserAdminId);
            SFLogger.logMessage("AccountController:GetBarChartDetails(): end.");
            return Json(listBarChart, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult GetBarChartDetailsforfourWeeks(Actiontext todaysaction)
        {
            SFLogger.logMessage("AccountController:GetBarChartDetailsforfourWeeks(): started!!.");
            string actualdate = todaysaction.WeekDate;
            int UserAdminId = SFConfiguration.GetUserID();
            MembershipService serviceRepository = new MembershipService();
            MembershipHelper memberHelper = new MembershipHelper(serviceRepository);
            var listBarChart = memberHelper.GetLastFourWeeksGraph(actualdate, UserAdminId);
            SFLogger.logMessage("AccountController:GetBarChartDetailsforfourWeeks(): end.");
            return Json(listBarChart, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public JsonResult GetTodaysActionData(bool isOnlyTodayAndPastActions = false)
        {
            SFLogger.logMessage("AccountController:GetTodaysActionData(): started!!.");
            int userid = SFConfiguration.GetUserID();
            MembershipService serviceRepository = new MembershipService();
            MembershipHelper memberHelper = new MembershipHelper(serviceRepository);
            var actiondata = memberHelper.GetTodaysActionData(userid, isOnlyTodayAndPastActions);
                    
            SFLogger.logMessage("AccountController:GetTodaysActionData(): end.");
            return Json(actiondata, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult InsertUpdateTodaysAction(Actiontext todaysaction)
        {
            SFLogger.logMessage("AccountController:InsertUpdateTodaysAction(): started!!.");
            TodaysActions action = new TodaysActions();
            action.TaskText = todaysaction.TaskText;
            action.UserId = SFConfiguration.GetUserID();
            var Currentdatetime = DateTime.Now;
            string datetime = Currentdatetime.ToString();
            action.TodaysActionDate = datetime;
            action.TaskId = 1;
            action.TaskPKeyid = todaysaction.TaskPrimaryKeyid;
            action.Type = todaysaction.Type;
            action.remindersdate = todaysaction.reminderdate;
            action.reminder_status = todaysaction.reminder_status;
            action.reminder = todaysaction.reminder;
            action.Is_deleted = todaysaction.Is_deleted;
            MembershipService serviceRepository = new MembershipService();
            var actiondata = serviceRepository.InsertUpdateTodaysAction(action, action.Type);
            SFLogger.logMessage("AccountController:InsertUpdateTodaysAction(): end.");
            return Json(actiondata, JsonRequestBehavior.AllowGet);

        }

        [AllowAnonymous]
        public ActionResult CreateUser()
        {
            return View();
        }

        [HttpPost]
        public ActionResult CreateUser(UserModel model)
        {
            SFLogger.logMessage("AccountController:CreateUser(): started!!.");
            if (ModelState.IsValid)
            {
                MembershipService serviceRepository = new MembershipService();
                MembershipHelper memberHelper = new MembershipHelper(serviceRepository);
                var isCreated = memberHelper.CreateAgent(serviceRepository,model);

                if (isCreated == 1)
                {
                    model = new UserModel();
                    model.ErrorMessage = "User created successfully";
                    SFLogger.logMessage("AccountController:CreateUser(): User created successfully.");
                }
                else
                {
                    model = new UserModel();
                    model.ErrorMessage = "User already exists";
                    SFLogger.logMessage("AccountController:CreateUser(): User already exists.");
                }
            }
            else
                {
                model.ErrorMessage = "Please enter all the fields";
                SFLogger.logMessage("AccountController:CreateUser(): Please enter all the fields.");
                }
            SFLogger.logMessage("AccountController:CreateUser(): end.");
            return View(model);
        } 
    }
}