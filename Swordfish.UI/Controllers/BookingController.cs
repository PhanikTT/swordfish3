﻿using Swordfish.Businessobjects.Payments;
using Swordfish.Businessobjects.Quotes;
using Swordfish.UI.Helpers;
using System.Collections.Generic;
using System.Web.Mvc;
using Swordfish.Components.BookingServices;
using Swordfish.UI.Models.Booking;
using System.Linq;

namespace Swordfish.UI.Controllers
{
    public class BookingController : BaseController
    {
        BookingService serviceRepository = null;
        public BookingController()
        {
            serviceRepository = new BookingService();          
        }
        // GET: Booking
        public ActionResult Index(string guid, string QuoteHeaderId)
        {
            SFLogger.logMessage("BookingController:Index(): started!!. QuoteHeaderId:"+ QuoteHeaderId);
            Quote quoteData = new Quote();
            var coBrowsingHelper = new CoBrowsingHelper();
            quoteData = new CommonHelper().LoadQuoteData(QuoteHeaderId);
            coBrowsingHelper.ConvertBaggageSlabs(quoteData);
            quoteData.PageExpireMsg = SFConfiguration.BookingPageExpireMsg();
            SFLogger.logMessage("BookingController:Index(): end. QuoteHeaderId:" + QuoteHeaderId);          
            return View(quoteData);
        }        
        [HttpPost]
        public JsonResult SavePayment(PaymentDetail paymentDetail)
        {
            SFLogger.logMessage("BookingController:SavePayment(): started.");
            var paymentList = new List<PaymentDetail>();
            paymentDetail.agentId = SFConfiguration.GetUserID();
            paymentDetail.cardNumber = SFConfiguration.GetCCNumber();
            paymentDetail.surcharge = "0";
            paymentList.Add(paymentDetail);
            BookingHelper bookingHelper = new BookingHelper(serviceRepository);
            int status = bookingHelper.SavePaymentDetails(paymentList);
            SFLogger.logMessage("BookingController:SavePayment(): end.");
            return Json(status.ToString(), JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult SaveBook(QuoteOverride quoteOverride)
        {
            SFLogger.logMessage("BookingController:SaveBook(): started.");
            BookingHelper bookHelper = new BookingHelper(serviceRepository);
            bool status = bookHelper.SaveQuoteOverrideDetails(quoteOverride);
            SFLogger.logMessage("BookingController:SaveBook(): end.");
            return Json(status.ToString(), JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public JsonResult SendQuoteAsEmailToLeadPassenger(List<PassengersModel> passengersDetails)
        {
            bool isPassengersSaved = false;
            string  isEmailSent =string.Empty; 
            SFLogger.logMessage("BookingController:SendQuoteAsEmailToLeadPassenger(): started with QuoteHeaderId : " + passengersDetails[0].QouteHeaderID);
            var bookingHelper = new BookingHelper(serviceRepository);
            var searchResultsHelper = new SearchResultsHelper();
            isPassengersSaved = searchResultsHelper.SavePassengers(passengersDetails);
            if (isPassengersSaved)
            {
                isEmailSent = bookingHelper.SendQuoteAsEmailToLeadPassenger(passengersDetails[0].Mobile, passengersDetails[0].QouteHeaderID, passengersDetails[0].Email, ControllerContext);
            }
            
            SFLogger.logMessage("BookingController:SendQuoteAsEmailToLeadPassenger(): end QuoteHeaderId : " + passengersDetails[0].QouteHeaderID);
            return Json(new { isPassengersSaved = isPassengersSaved,isEmailSent= isEmailSent},JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult GetOpenPostCodeData(string postCode)
        {
            SFLogger.logMessage("BookingController:GetOpenPostCodeData(): started.");
            var result = new CommonHelper().getPostCode(postCode);
            SFLogger.logMessage("BookingController:GetOpenPostCodeData(): end.");
            return Json(result, JsonRequestBehavior.AllowGet);
        }
        [HttpGet]
        public JsonResult GetCardTypes()
        {
            SFLogger.logMessage("BookingController:GetCardTypes(): started.");           
            var result = SFConfiguration.GetCardTypes();
            SFLogger.logMessage("BookingController:GetCardTypes(): end.");
            return Json(result, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public JsonResult ChangeCardTypePrice(string cardTypeId, string TotalFare)
        {
            var model = SFConfiguration.ChangeCardTypePrice(cardTypeId, TotalFare);
            return Json(model, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public JsonResult GetPaymentDetails(int quoteHeaderId)
        {
            CommonHelper commonHelper = new CommonHelper();
            List<PaymentDetail> paymnetDetails = commonHelper.GetCompletePaymentDetails(quoteHeaderId);
            foreach(PaymentDetail paymentDetail in paymnetDetails)
            {
                try
                {
                    paymentDetail.totalprice = (System.Convert.ToDecimal(paymentDetail.surcharge) + System.Convert.ToDecimal(paymentDetail.cost)).ToString();
                }
                catch (System.Exception ex)
                {
                    SFLogger.logError("Error Occured at GetPaymentDetails", ex);
                }
            }
            paymnetDetails = paymnetDetails.Where(p => !string.IsNullOrEmpty(p.transactionId)).ToList();
            return Json(paymnetDetails, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        [ValidateInput(false)]
        public ActionResult CardEasyResposne(string status)
        {
            //status = System.IO.File.ReadAllText(@"C:\SwordFish\xml.xml");
            SFLogger.logMessage("BookingController:CardEasyResposne(): started.");
            BookingHelper bookingHelper = new BookingHelper(serviceRepository);
            var paymentDetail = bookingHelper.TransformResponse(status);
            paymentDetail.agentId = SFConfiguration.GetUserID();
            paymentDetail.name = "Test";
            paymentDetail.cardEasyResponse = status;
            int status1 = bookingHelper.SaveCardEasyResponse(paymentDetail);
            //ViewData["CEStatus"] = paymentDetail;
            SFLogger.logMessage("BookingController:CardEasyResposne(): end.");
            return View(paymentDetail);
        }
    }
}