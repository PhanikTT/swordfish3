﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using System.Threading.Tasks;
using Swordfish.Businessobjects.Customers;
using Swordfish.UI.Helpers;
using Swordfish.Components.BookingServices;

namespace Swordfish.UI.Controllers
{
    public class CustomerController : BaseController
    {
        // GET: Customer
        public ActionResult CustomerGrid()
        {
            return PartialView();
        }

        [HttpPost]
        public JsonResult GetTsOptionsTitles()
        {
            SFLogger.logMessage("CustomerController:GetTsOptionsTitles(): started!!.");
            CommonHelper objCommonHelper = new CommonHelper();
            var result = objCommonHelper.GetOptionsForValue(10);
            SFLogger.logMessage("CustomerController:GetTsOptionsTitles(): end.");
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult AddCustomer(Customer customer)
        {
            SFLogger.logMessage("CustomerController:AddCustomer(): started!!.");
            var MembershipNo = GenerateMembershipNo(6); 
            customer.CreatedBy = SFConfiguration.GetUserID().ToString();
            customer.MembershipNo = MembershipNo;
            var result = ValidateCustomer(customer);
            SFLogger.logMessage("CustomerController:AddCustomer(): end.");
            return Json(new
            {
                Message = result,
                Membershipno = Convert.ToString(MembershipNo)
            }, JsonRequestBehavior.AllowGet
           );
        }

        private static string SaveCustomer(BookingService serviceRepository, Customer customer)
        {
            SFLogger.logMessage("CustomerController:SaveCustomer(): started!!.");
            var customerHelper = new CustomerHelper(serviceRepository);
            if (customer.telephone_number.Length == 4 && customer.telephone_number.Equals("0044"))
                customer.telephone_number = string.Empty;
            var result = Task.Run(() => customerHelper.SaveCustomer(customer));
            Task.WhenAll(result);
            SFLogger.logMessage("CustomerController:SaveCustomer(): end.");
            return result.Result;
        }

        public string ValidateCustomer(Customer customer)
        {
            SFLogger.logMessage("CustomerController:ValidateCustomer(): started!!.");
            string result = "";
            try
            {
                if (string.IsNullOrEmpty(customer.first_name))
                {
                    result = "First name should not be empty";
                }
                else if (string.IsNullOrEmpty(customer.last_name))
                {
                    result = "Last name should not be empty";
                }
                else if (string.IsNullOrEmpty(customer.email_address))
                {
                    result = "Email should not be empty";
                }
                else if (string.IsNullOrEmpty(customer.phone_number) || customer.phone_number.Length < 10)
                {
                    result = "Phone Number should not be empty or Phone Number should be minimum 10 digits";
                }
                else if (!string.IsNullOrEmpty(customer.telephone_number.Replace("0044",string.Empty)) && customer.telephone_number.Length < 10 )
                {
                    result = "Telephone Number should be minimum 10 digits";
                }              
                else
                {
                    var serviceRepository = new BookingService();
                    result = SaveCustomer(serviceRepository, customer);
                }
            }
            catch (Exception ex)
            {
                SFLogger.logError("CustomerController:ValidateCustomer(): end," + result ,ex);
                throw ex;
            }
            SFLogger.logMessage("CustomerController:ValidateCustomer(): end," + result);
            return result;
        }


        public static string GenerateMembershipNo(int length)
        {
            SFLogger.logMessage("CustomerController:GenerateMembershipNo(): started!!.");
            const string chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
            var random = new Random();
            SFLogger.logMessage("CustomerController:GenerateMembershipNo(): end.");
            return new string(Enumerable.Repeat(chars, length)
              .Select(s => s[random.Next(s.Length)]).ToArray());
        }

        [HttpPost]
        public JsonResult SearchCustomer(Customer customer)
        {
            SFLogger.logMessage("CustomerController:SearchCustomer(): started!!.");
            List<Customer> result = new List<Customer>();
            string message = "";
            try
            {
                if (customer.MembershipNo == null && customer.last_name == null && customer.EmailType == null && customer.email_address == null && customer.phone_number == null && customer.Postcode == null)
                {
                    message = "Please enter at least one field";
                    SFLogger.logMessage("CustomerController:SearchCustomer():" + message);
                    return Json(message, JsonRequestBehavior.AllowGet);
                }
                else
                {                  
                    var serviceRepository = new BookingService();
                    result = SearchCustomerDetails(serviceRepository, customer).Result;
                }
            }
            catch (Exception ex)
            {
                SFLogger.logError("CustomerController:SearchCustomer(): end.",ex);
                throw ex;
            }
            SFLogger.logMessage("CustomerController:SearchCustomer(): end.");
            return Json(result, JsonRequestBehavior.AllowGet);
        }
        private async Task<List<Customer>> SearchCustomerDetails(BookingService serviceRepository, Customer customer)
        {
            SFLogger.logMessage("CustomerController:SearchCustomerDetails(): started!!.");
            var customerHelper = new CustomerHelper(serviceRepository);
            var result = await Task.Run(() => customerHelper.SearchCustomerData(customer)).ConfigureAwait(false);
            SFLogger.logMessage("CustomerController:SearchCustomerDetails(): end.");
            return result;
        }
        public JsonResult UpdateCustomer(Customer customer)
        {
            SFLogger.logMessage("CustomerController:UpdateCustomer(): started!!.");
            customer.CreatedBy = SFConfiguration.GetUserID().ToString();
            BookingService serviceRepository = new BookingService();
            var result = UpdateCustomerDetails(serviceRepository, customer);
            SFLogger.logMessage("CustomerController:UpdateCustomer(): end!!.");
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        private static string UpdateCustomerDetails(BookingService serviceRepository, Customer customer)
        {
            SFLogger.logMessage("CustomerController:UpdateCustomerDetails(): started!!.");
            CustomerHelper helper = new CustomerHelper(serviceRepository);
            if (customer.telephone_number.Length == 4 && customer.telephone_number.Equals("0044"))
                customer.telephone_number = string.Empty;
            var result = Task.Run(() => helper.UpdateCustomer(customer));
            Task.WhenAll(result);
            SFLogger.logMessage("CustomerController:UpdateCustomerDetails(): end.");
            return result.Result;
        }
    }
}