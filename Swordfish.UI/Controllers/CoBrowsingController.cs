﻿using System;
using System.Web.Mvc;
using Swordfish.Businessobjects.Common;
using Swordfish.UI.Helpers;
using Swordfish.UI.Models.Booking;
namespace Swordfish.UI.Controllers
{
    public class CoBrowsingController : BaseController
    {
        // GET: CoBrowsing
        public ActionResult Index(string guid, int quoteHeaderId)
        {
            var CoBrowseResultModel = new CoBrowseResultModel();
            var coBrowsingHelper = new CoBrowsingHelper();
            var commonHelper = new CommonHelper();
            CoBrowseResultModel.Passengers = commonHelper.GetPassengers(quoteHeaderId.ToString());
            if (CoBrowseResultModel.Passengers[0].LapsDays>= DateTime.Now && CoBrowseResultModel.Passengers[0].StatusOptionId == SFConstants.CobrowsStatus_New)
            {
                CoBrowseResultModel.BasketQuoteObject = coBrowsingHelper.LoadQuoteData(quoteHeaderId.ToString());
                if (CoBrowseResultModel.BasketQuoteObject != null && CoBrowseResultModel.BasketQuoteObject.bagsComponents != null && CoBrowseResultModel.BasketQuoteObject.bagsComponents.Count > 0)
                    coBrowsingHelper.ConvertBaggageSlabs(CoBrowseResultModel.BasketQuoteObject);
            }
            ViewBag.quoteHeaderId = quoteHeaderId;
            ViewBag.guId = guid;
            return View(CoBrowseResultModel);
        }
        [HttpPost]
        public int SaveConformationResponse(int quoteHeaderID, string guId, string responseMsg)
        {
            var coBrowsingHelper = new CoBrowsingHelper();
            var result = coBrowsingHelper.SaveCoBrowseConformationResponce(quoteHeaderID, guId, responseMsg);
            return result.Result;
        }
    }
}