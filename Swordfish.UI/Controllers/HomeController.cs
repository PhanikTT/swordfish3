﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Mvc;
using System.Collections.Concurrent;
using SwordFish.Filters;
using Swordfish.UI.Helpers;
using Swordfish.Businessobjects.Hotels;
using Swordfish.Businessobjects.Customers;
using Swordfish.Components.MembershipServices;
using Swordfish.Components.BookingServices;
using Swordfish.Components.TTSSService;
using Swordfish.Businessobjects.TTSS;
using Swordfish.UI.Models.Account;
using Swordfish.UI.Models.Package;
using Swordfish.Businessobjects.Quotes;

namespace Swordfish.UI.Controllers
{
    [HandleError]
    public class HomeController : BaseController
    {
        [Authorize]
        public ActionResult DashBoard(SearchModel model)
        {
            SFLogger.logMessage("HomeController:DashBoard(): started!!.");
            MembershipService serviceRepository = new MembershipService();
            MembershipHelper memberHelper = new MembershipHelper(serviceRepository);
            memberHelper.ResetSessionID();
            model.MaxRooms = SFConfiguration.GetMaxRooms();
            model.MaxAdultsInRoom = SFConfiguration.GetMaxAultsInRoom();
            model.MaxPaxInRoom = SFConfiguration.GetMaxPaxInRoom();
            model.MaxChildInRoom = SFConfiguration.GetMaxChildInRoom();
            model.MaxInfantInRoom = SFConfiguration.GetMaxInfantInRoom();
            model.MaxPaxPerBooking = SFConfiguration.GetMaxPaxPerBooking();
            int customerID = 1; //TO DO
            int quoteCount = GetQuoteCount(customerID);
            SFConfiguration.SetQuoteCount(quoteCount.ToString());
            SFLogger.logMessage("HomeController:DashBoard(): end.");
            return View(model);
        }

        [AllowAnonymous]
        public JsonResult GetHotelDestinations(string searchterm)
        {
            SFLogger.logMessage("HomeController:GetHotelDestinations(): started!!.");
            Dictionary<string, string> lang = new Dictionary<string, string>();
            //Return from cache 
            var result = MyCacheSource.GetHotelDestinations(searchterm, 10);
            SFLogger.logMessage("HomeController:GetHotelDestinations(): end.");
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        [AllowAnonymous]
        public JsonResult GetAirportSources(string searchterm)
        {
            SFLogger.logMessage("HomeController:GetAirportSources(): started!!.");
            Dictionary<string, string> lang = new Dictionary<string, string>();
            var result = MyCacheSource.GetAirportSources(searchterm, 10);
            SFLogger.logMessage("HomeController:GetAirportSources(): end.");
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult GetAirportsList()
        {
            SFLogger.logMessage("HomeController:GetAirportsList(): started!!.");
            var result = MyCacheSource.GetAirPortsList(10);
            SFLogger.logMessage("HomeController:GetAirportsList(): end.");
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        public string ping(string data)
        {
            return data;
        }

        public ActionResult Search()
        {
            return PartialView();
        }

        [HttpPost]
        [SessionExpire]
        // [Authorize]
        public ActionResult Search(SearchModel model, string TTSSQuote, string transfer, string IncomingCustomerName, string EmailAddress)
        {
            SFLogger.logMessage("HomeController:Search(): started!!.");
            //BookingService serviceRepository = new BookingService();
            string message = string.Empty;
            var redirectUrl = Url.Action("Index", "SearchResults");  //Url.Action("Filter", "Quote", new { basketEnable = "False" });

            SFConfiguration.SetTTSSSearch(false);
            if (!string.IsNullOrEmpty(TTSSQuote))
            {
                SFConfiguration.SetIncomingCutomerName(IncomingCustomerName);
                model = GetQuoteInfo(TTSSQuote.ToUpper());
                model.Transfer = Convert.ToBoolean(transfer);
                //Check if the dates are entered in search screen
                if (string.IsNullOrEmpty(model.CheckIn) && string.IsNullOrEmpty(model.CheckOut))
                {
                    message = "Please check the quote reference.";
                    SFLogger.logMessage("HomeController:Search(): " + message);
                }

            }
            else if (model.ResultType == "Package")
            {
                if (!ModelState.IsValid)
                {
                    return View(model);
                }
            }
            else if (model.ResultType == "Hotel")
            {
                if (!string.IsNullOrEmpty(model.DestinationTo) && !string.IsNullOrEmpty(model.CheckIn))
                {
                    if (model.Adults < 1)
                    {
                        message = "Please select atleast one adult.";
                        SFLogger.logMessage("HomeController:Search(): " + message);
                    }
                    SFConfiguration.SetDashBSearch(model);
                }
                else
                {
                    message = "Please fill the mandatory fields.";
                    SFLogger.logMessage("HomeController:Search(): ");
                }
            }
            else if (model.ResultType == "Flight")
            {
                if (!string.IsNullOrEmpty(model.DestinationTo) && !string.IsNullOrEmpty(model.SourceFrom) && !string.IsNullOrEmpty(model.CheckIn) && !string.IsNullOrEmpty(model.CheckOut))
                {
                }
                else
                {
                    message = "Please fill the mandatory fields.";
                    SFLogger.logMessage("HomeController:Search(): " + message);
                }
            }
            SFConfiguration.SetDashBSearch(model);
            SFLogger.logMessage("HomeController:Search(): end.");
            if (string.IsNullOrEmpty(message) == false)
            {
                return Json(new { Message = message }, JsonRequestBehavior.AllowGet);

            }
            else if (TTSSQuote == null || TTSSQuote == "")
            {
                return Json(new { RedirectUrl = redirectUrl }, JsonRequestBehavior.AllowGet);
            }
            else { return Json(model, JsonRequestBehavior.AllowGet); }
        }

        [NonAction]
        private SearchModel GetQuoteInfo(string TtssQuote)
        {
            SFLogger.logMessage("HomeController:GetQuoteInfo(): started!!. TTSSQuote " + TtssQuote);
            SearchModel searchModel = new SearchModel();
            if (!string.IsNullOrEmpty(TtssQuote))
            {
                TTSSService serviceRepository = new TTSSService();
                string TTSSUrl = string.Format(SFConfiguration.GetSingleTTSSQuote(), TtssQuote, "&");
                TTSSQuote ttssPackage = new TTSSQuote();
                SFConfiguration.SetTTSSQSearch(TtssQuote);
                BookingService BookingRepository = new BookingService();
                QuoteHelper quoteHelper = new QuoteHelper(BookingRepository);
                var searchdata = quoteHelper.getsearchdata(TtssQuote);
                if (searchdata.sourceCode == null)
                {
                    TTSSHelper helper = new TTSSHelper(serviceRepository);
                    var quoteList = Task.Run(() => helper.GetQuote(TTSSUrl));
                    Task.WhenAll(quoteList);
                    ttssPackage = quoteList.Result;

                    SFConfiguration.SetTTSSSearch(true);
                    if (ttssPackage != null)
                    {
                        searchModel.Adults = Convert.ToInt16(ttssPackage.Adults);
                        searchModel.Child = Convert.ToInt16(ttssPackage.Children);
                        searchModel.Bags = 0;
                        searchModel.CheckIn = ttssPackage.DepartureDate;
                        searchModel.CheckOut = ttssPackage.InboundDepartureDate;// .ReturnDate;
                        searchModel.SourceFrom = ttssPackage.FromAirport;
                        searchModel.SourceFromId = ttssPackage.FromAirport;
                        searchModel.DestinationTo = ttssPackage.ToAirport;
                        searchModel.DestinationToId = ttssPackage.ToAirport;
                        searchModel.ResultType = "Package";
                        searchModel.Nights = Convert.ToInt16(ttssPackage.Duration);
                        searchModel.Rating = Convert.ToDecimal(ttssPackage.Rating);
                        searchModel.Hotel = ttssPackage.Accommodation;
                        searchModel.MasterHotelId = ttssPackage.MasterHotelId;
                        searchModel.HotelKey = ttssPackage.HotelKey;
                        searchModel.TTICode = ttssPackage.TTICode;
                        searchModel.TTSSListedPricePerPerson = ttssPackage.Price;
                        searchModel.TTSSQuoteReference = TtssQuote;
                        searchModel.TTSSQuote = ttssPackage;
                        searchModel.Rooms = 1;
                        var occupancyRoom = new Swordfish.UI.Models.Account.Occupancy() { AdultsCount = Convert.ToInt16(ttssPackage.Adults), ChildCount = Convert.ToInt16(ttssPackage.Children) };
                        searchModel.OccupancyRooms.Add(occupancyRoom);
                    }
                }
                else
                {
                    searchModel.Adults = searchdata.AdultsCount;
                    searchModel.Child = searchdata.ChildrenCount;
                    searchModel.Infants = searchdata.InfantsCount;
                    searchModel.Bags = 0;
                    if (searchdata.startDate >= DateTime.Now)
                        searchModel.CheckIn = Convert.ToString(searchdata.startDate);
                    else
                        searchModel.CheckIn = Convert.ToString(DateTime.Now.Date);
                    if (searchdata.endDate >= DateTime.Now)
                        searchModel.CheckOut = Convert.ToString(searchdata.endDate);
                    else
                        searchModel.CheckOut = Convert.ToString(DateTime.Now.AddDays(7).Date);
                    searchModel.SourceFrom = searchdata.sourceName;
                    searchModel.SourceFromId = searchdata.sourceCode;
                    searchModel.DestinationTo = searchdata.destinationName;
                    searchModel.DestinationToId = searchdata.destinationCode;
                    searchModel.ResultType = "Package";
                    TimeSpan nights = searchdata.endDate - searchdata.startDate;
                    searchModel.Nights = int.Parse(nights.TotalDays.ToString());
                    searchModel.Rating = searchdata.starRating;
                    searchModel.Hotel = searchdata.hotelName;
                    searchModel.TTSSQuoteReference = searchdata.TTSSQuoteId;
                }
            }

            return searchModel;
        }

        public JsonResult GetHotelList(string destinationCode)
        {
            SFLogger.logMessage("HomeController:GetHotelList(): started!!.");
            string supplierCode = "HCO";
            BookingService serviceRepository = new BookingService();

            var hotelList = GetDestinationHotels(serviceRepository, supplierCode, destinationCode);
            SFConfiguration.SetHotelList(hotelList);

            SFLogger.logMessage("HomeController:GetHotelList(): end.");
            return Json(hotelList.OrderBy(x => x.Name), JsonRequestBehavior.AllowGet);

        }
        [NonAction]
        public ConcurrentBag<DestinationHotels> GetDestinationHotels(BookingService serviceRepository, string supplierCode, string destinationCode)
        {
            SFLogger.logMessage("HomeController:GetDestinationHotels(): started!!.");
            QuoteHelper helper = new QuoteHelper(serviceRepository);
            var hotelList = Task.Run(() => helper.GetDestinationHotels(supplierCode, destinationCode));
            Task.WhenAll(hotelList);
            SFLogger.logMessage("HomeController:GetDestinationHotels(): end.");
            return hotelList.Result;
        }
        [NonAction]
        private int GetQuoteCount(int CustomerId)
        {
            SFLogger.logMessage("HomeController:GetQuoteCount(): started!!.");
            BookingService serviceRepository = new BookingService();
            //int quoteCount = serviceRepository.GetQuoteCount(CustomerId).Result;
            int quoteCount = 0;
            SFLogger.logMessage("HomeController:GetQuoteCount(): end.");
            return quoteCount;
        }

        [HttpPost]
        public JsonResult InsertIncomingCallData(IncomingCustomer CustomerObject)
        {
            SFLogger.logMessage("HomeController:InsertIncomingCallData(): started!!.");
            int UserId = SFConfiguration.GetUserID();
            CustomerObject.Userid = UserId;
            CustomerObject.LoginDateTime = DateTime.Now.ToString();
            CustomerObject.LogoutDateTime = DateTime.Now.ToString();
            MembershipService serviceRepository = new MembershipService();
            var customerdata = serviceRepository.InsertIncomingCustomerData(CustomerObject);
            if (customerdata != null)
            {
                SFConfiguration.SetCustomerDetails(customerdata);
            }
            SFLogger.logMessage("HomeController:InsertIncomingCallData(): end.");
            return Json(customerdata, JsonRequestBehavior.AllowGet);
        }

        [NonAction]
        private IncomingCustomer InsertIncomingCustomerData(MembershipService serviceRepository, IncomingCustomer CustomerObject)
        {
            SFLogger.logMessage("HomeController:InsertIncomingCustomerData(): started!!.");
            MembershipHelper helper = new MembershipHelper(serviceRepository);
            var quoteRefInfo = Task.Run(() => helper.InsertIncomingCustomerData(CustomerObject));
            Task.WhenAll(quoteRefInfo);
            SFLogger.logMessage("HomeController:InsertIncomingCustomerData(): end. ");
            return quoteRefInfo.Result;
        }

        [HttpPost]
        public JsonResult GetTTSSQuoteInfo(string TtssQuote)
        {
            SFLogger.logMessage("HomeController:GetTTSSQuoteInfo(): started!!. TTSSQuote:" + TtssQuote);
            var ttssPackage = new TTSSQuote();
            if (!string.IsNullOrEmpty(TtssQuote))
            {
                TTSSService serviceRepository = new TTSSService();
                string TTSSUrl = string.Format(SFConfiguration.GetSingleTTSSQuote(), TtssQuote, "&");
                TTSSHelper helper = new TTSSHelper(serviceRepository);
                var quoteList = Task.Run(() => helper.GetQuote(TTSSUrl));
                Task.WhenAll(quoteList);
                ttssPackage = quoteList.Result;
            }
            SFLogger.logMessage("HomeController:GetTTSSQuoteInfo(): end. TTSSQuote:" + TtssQuote);
            return Json(ttssPackage, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult AgentQuotes(string quoteRef, string customerName, string fromDate, string toDate, int agentId, bool isLast24Hours)
        {
            List<AgentQuote> model = new List<AgentQuote>();
            SFLogger.logMessage("HomeController:AgentQuotes(): started!!.");
            MembershipService serviceRepository = new MembershipService();
            MembershipHelper memberHelper = new MembershipHelper(serviceRepository);
            var userInfo = memberHelper.GetUserDetails(serviceRepository, SFConfiguration.GetUserEmail());
            userInfo.Environment = SFConfiguration.GetEnvironment();
            var UserName = userInfo.FirstName + " " + userInfo.LastName;
            BookingService bookServiceRepository = new BookingService();
            var AgentDetails = SFConfiguration.GetUserInfo();
            AgentQuote agentQuote = new AgentQuote();
            agentQuote.AgentName = AgentDetails.FirstName + " " + AgentDetails.LastName;
            agentId = SFConfiguration.GetUserID();
            model = bookServiceRepository.GetAgentQuotes(quoteRef, customerName, fromDate, toDate, agentId, isLast24Hours);
            var modelData = new { model = model, agentQuote = agentQuote.AgentName };
            SFLogger.logMessage("HomeController:AgentQuotes(): end.");
            return Json(modelData, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public string LoadQuote(int quoteHeaderID)
        {
            Quote model = null;
            var coBrowsingHelper = new CoBrowsingHelper();
            SFLogger.logMessage("HomeController:LoadQuote(): started!!.");
            if (quoteHeaderID > 0)
            {
                model = new CommonHelper().LoadQuoteData(Convert.ToString(quoteHeaderID));
                if(model != null)
                coBrowsingHelper.ConvertBaggageSlabs(model);
                model.bagsComponents = model.bagsComponents.OrderBy(x => x.PassengerIndex).ToList();
            }
            SFLogger.logMessage("HomeController:LoadQuote(): end.");
            return Newtonsoft.Json.JsonConvert.SerializeObject(model);
        }

    }
}