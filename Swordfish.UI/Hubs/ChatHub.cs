﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.AspNet.SignalR;
using System.Collections.Concurrent;
using System.Threading.Tasks;
using System.Collections;


namespace Chilli.Hubs
{
    public class ChatHub : Hub
    {
        static ConcurrentDictionary<string, string> dic = new ConcurrentDictionary<string, string>();

        private static Hashtable htUsers_ConIds = new Hashtable(20);
        public void registerConId(string userID)
        {
            try
            {
                bool alreadyExists = false;
                if (htUsers_ConIds.Count == 0)
                {
                    if (userID != null)
                        htUsers_ConIds.Add(userID, Context.ConnectionId);
                }
                else
                {
                    foreach (string key in htUsers_ConIds.Keys)
                    {
                        if (key == userID)
                        {
                            htUsers_ConIds[key] = Context.ConnectionId;
                            alreadyExists = true;
                            break;
                        }
                    }
                    if (!alreadyExists)
                    {
                        if (userID != null)
                           htUsers_ConIds.Add(userID, Context.ConnectionId);
                    }
                }
            }
            catch
            {
                //just to consume error
            }
        }

        public void Send(string name, string message)
        {
           Clients.All.sendMessage(name, message);
        }

        public void SendToSpec(string name, string message, string connID)
        {
            Clients.Client(connID).sendMessage(name, message);
        }

        public void SendToSpecific(string name, string message, string to)
        {
            Clients.Caller.broadcastMessage(name, message);
            Clients.Client(dic[to]).broadcastMessage(name, message);
        }

        public void Notify(string name, string id)
        {
            if (dic.ContainsKey(name))
            {
                Clients.Caller.differentName();
            }
            else
            {
                dic.TryAdd(name, id);
                foreach (KeyValuePair<String, String> entry in dic)
                {
                    Clients.Caller.online(entry.Key);
                }
                Clients.Others.enters(name);
            }
        }

        public override Task OnDisconnected(bool stopCalled=true)
        {
            htUsers_ConIds.Remove(Context.ConnectionId);                   
            return base.OnDisconnected(stopCalled);           
        }

    }
}